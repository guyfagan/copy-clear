<?php
	/********************************************************/
	/* CMS Configuration file								*/
	/********************************************************/

	#Database
	define("__DB_HOST__","127.0.0.1");
	define("__DB_USER__","root");
	define("__DB_PASS__","root");
	define("__DB_NAME__","copyclear");

	#Paths
	define("__SERVERPATH__","/copyclear/html/");
	define("__CMS_PATH__",__SERVERPATH__."admin/");
	define("__DOMAIN__","http://copyclear.ie".__SERVERPATH__);
	define("__UPLOADPATH__","../");

	//charset
	//define("__CMS_CHARSET__","iso-8859-1");//now this is defined in the general settings

	//Encryptions
	define("__ENCRYPTION_PASSKEY__","cmsB055!");

	//Login conf
	define("__LOGIN_TYPE__","cms"); //can be cms, session or cookie

	//Security
	define("__SECURE_CMS__",false);
	define("__FORCE_SSL__",false);

	//Various
	define("__CMS_NAME__","Old Hat CMS");

  ini_set('date.timezone', 'Europe/Dublin');

	//this include all the different versions of all the libraries installed in the cms
	require_once("inc/versions.inc.php");

?>