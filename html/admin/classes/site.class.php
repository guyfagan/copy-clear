<?php
	require_once('ewrite.class.php');

	class site extends ewrite{
		
		public $errors = array();
		public $settings = array();
		protected $meta;
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);		
			$this->module = "your module";
			$this->utils->read_params($this,$params);			
			//Meta init	
			$this->meta = $this->utils->call("meta");		
			//setting the current module in the class
			$this->set_module($this->module);		
			//Set basic settings
			$this->init_settings();
		}//endconstructor
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################		
		
		private function init_settings(){			
			//read the settings from the cms
			$this->settings = $this->utils->get_settings(array('module' => $this->module));			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>