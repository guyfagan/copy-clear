<?php
	#################################################################
	# Export Class - 	last update 08/10/12						#
	#					created on 08/10/12							#
	#################################################################
	
	/* 
	How to use this class
	
	Call the export class	

	$export = $utils->call("export");
	
	Then you just need to call the do_export method to export the file with the data you passed. As an option you can pass the label for the first row 
	$label = array("My Label 1", "My Label 2", [etc..]);
	
	$export->do_export(array("data" => $myarray, "headers" => $label));
	
	*/

	class export{
		protected $utils;//utils class
		public $format;
		protected $data;
		protected $separator;
	
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;			
			$this->utils->read_params($this,$params);	
			$this->format = "csv";
			$this->separator = ',';
		}//end function
		
		protected function get_csv_headers(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($filename)){
				//if nothing passed, generate a filename
				$filename = "export-".date("d-m-Y-H-i");	
			}//end if
			
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$filename.".csv");
			header("Pragma: no-cache");
			header("Expires: 0");
		}//end function
		
		
		public function do_export(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if				
			//get the headers
			$this->get_csv_headers();	
			if(!isset($data) && !is_array($data)){
				return false;	
			}//end  if
			if(!isset($headers)){
				$headers = true;			
			}//end if
			
			$csv = array();
			if(is_bool($headers)){
				$headers_data = $data;
				if($data[0] != NULL){
					$headers_data = $data[0];			
				}//end if
				$csv_headers = array();
				foreach($headers_data as $key => $value){
					array_push($csv_headers,$key);
				}//end foreach			
				array_push($csv,$csv_headers);
			} else if(is_array($headers)){
				array_push($csv,$headers);
			}//end if			
			
			for($i = 0; $i < sizeof($data); $i++){
				array_push($csv,$data[$i]);
			}//end for
			
		
			$outstream = fopen("php://output", "w");
			foreach ($csv as $fields) {
				fputcsv($outstream, $fields);
			}

			fclose($outstream);
		}//end function
	}//end class
?>