<?php

	require_once($_SERVER['DOCUMENT_ROOT'].__CMS_PATH__."inc/twitteroauth/twitteroauth.php");
	
	class twitter{
		
		private $utils;//utils class	
		private $login;
		private $connection;
		private $format;
		private $tweets;
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;	
			$settings = $utils->get_settings();		
			$this->connection = new TwitterOAuth($settings['twitterConsumerKey'], $settings['twitterConsumerSecret'], $settings['twitterUserToken'],$settings['twitterUserSecret']);		
		}//endconstructor		
		
		public function post_tweet($tweet_text){
			 /*$this->connection->request('POST',$this->connection->url('1.1/statuses/update'),array('status' => $tweet_text));			  
			 return $this->connection->response['code'];*/
		}//end function
		
		public function get_last_tweet(){
			$response = $this->connection->get('statuses/user_timeline',array('count' => 1));
			if(!isset($response->errors)){
				return $response[0]->text;
			} else {
				return "Ooops! Apparently Twitter is now working very well!";
			}//end if		
		}//end function
		
		public function get_last_mentions($count = 1){
			$response = $this->connection->get('statuses/mentions_timeline',array('count' => 1));
			if(!isset($response->errors)){
				return $response[0]->text;
			} else {
				return "Ooops! Apparently Twitter is now working very well!";
			}//end if		
		}//end function
		
		public function find_links($text){
			// The Regular Expression filter
			$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
					
			// Check if there is a url in the text
			if(preg_match($reg_exUrl, $text, $url)) {			
				   // make the urls hyper links
				   return preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank">'.$url[0].'</a>', $text);			
			} else {			
				   // if no urls in the text just return the text
				   return $text;
			
			}//end if
		}//end function		
		
		public function find_twitter($text){
			$text = preg_replace("/#([a-z_0-9]+)/i", "<a href=\"http://twitter.com/search/$1\">$0</a>", $text);
			$text = preg_replace("/@([a-z_0-9]+)/i", "<a href=\"http://twitter.com/!#/$1\">$0</a>", $text);		
			
			return $text;
		}//end function
		
		public function find_twitter_hash($text){
			$reg_exTw = "/\ #[a-zA-Z0-9]+\ ?/";
			// Check if there is a url in the text
			if(preg_match($reg_exTw, $text, $url)) {			
			// make the urls hyper links
				$text = preg_replace($reg_exTw, '<a href="https://twitter.com/#!/search/%23'.trim(str_replace("#","",$url[0])).'" target="_blank">'.$url[0].'</a>', $text);
				return $this->find_twitter_hash($text);
			} else {
				return $text;
			}//end if		
		}//end function

	}//end class
?>