<?php
	#################################################################
	# Meta Class -  last update 25/06/13							#
	#				created on 03/10/12								#
	# Updates														#
	# 21/11/12 - Added support for html meta						#	
	# 26/11/12 - Added support for raw meta data					#
	# 22/12/12 - Converted to PDO									#			
	#################################################################
	/*
	
	How to save a meta.
	1) Call the meta class	

	$meta = $this->utils->call("meta");
			
	2) Define the module first, in your script or at the begin of your class
	
	$meta->set_meta_module("your_module_name_or_id");
		
	3) Define the destination id, for example the post_id
	
	$meta->set_meta_id(your_id);
	
	Or you can pass the id in the add_meta method as a paramether, but using the set_meta_id you will save a bit of code if you have to add various meta
	
	4) Add the meta to the databse
	
	$meta->add_meta(array(""))
	*/
	
	class meta{
		protected $utils;//utils class	
		protected $id;	
		protected $module;	
		protected $type;
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;			
			$this->utils->read_params($this,$params);	
			$this->type = 'post';						
		}//endconstructor	
		
		public function set_meta_type($type){
			$this->type = $type;	
		}//end function
		
		public function set_meta_module(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$module = $params[0];
			}//end if
			
			$this->module = $this->utils->get_module($module);
		}//end function
		
		public function set_meta_id(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$id = $params[0];
			}//end if
			if(isset($id) && is_numeric($id)){
				$this->id = $id;
			}//end if
		}//end function
		
		protected function find_meta_fields(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($data)){
				$data = $_POST;
			}//end if
			
			$fields = array();
			foreach($data as $key => $value){
				if(strpos($key,"meta_") !== false){
					array_push($fields,$key);
				}//end if
			}//end foreach
			if(sizeof($fields) > 0){
				return $fields;	
			} else {
				return false;
			}//end if
		}//end function
		
		public function auto_add_meta(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$params = array();
			if(isset($src_data) && is_array($src_data)){
				$params['data'] = $src_data;	
			} else {
				$src_data = $_POST;
			}//end if
			
			if(isset($src_data['deleted_meta'])){
				$delete_meta = explode(',',$src_data['deleted_meta']);					
				foreach($delete_meta as $value){
					$delete_field = str_replace('[]',"",$value);					
					if(!isset($src_data[$delete_field])){
						$src_data[$delete_field] = '';	
					}//end if
				}//end foreach
				$params['data'] = $src_data;	
			}//end if
					
			//find the fields
			$fields = $this->find_meta_fields($params);
			if($fields != false){
				//we have fields, let's check if we have a reference ID as well
				if(isset($id)){
					$this->id = $id;	
				} else if(!isset($id) && !isset($this->id)){
					//to be sure that at least the internal id is set, and if nothing is set, we return false
					return false;
				}//end if
				//set the meta id
				$this->set_meta_id(array("id" => $this->id));
				//now we read the fields array
				for($i = 0; $i < sizeof($fields); $i++){	
					if(isset($avoid) && is_array($avoid)){
						//something to ignore, so we just doublecheck each record
						if(!in_array($fields[$i],$avoid)){
							$this->add_meta(array("label" => $fields[$i], "value" => $src_data[$fields[$i]]));
						}//end if
					} else {
						//nothing to ignore, so add everything
						$this->add_meta(array("label" => $fields[$i], "value" => $src_data[$fields[$i]]));
					}//end if
				}//end for i
				return true;
			} else {
				//nothing found
				return false;	
			}//end if
		}//end function
		
		public function get_meta(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
						
			if(is_numeric($id) && isset($id)){
				$this->id = $id;
				$query = "SELECT * FROM meta
						  WHERE meta_ref_id = ".$this->id."
						  AND meta_type = '".$this->type."'
						  AND meta_solidified = 0";
				if(isset($module)){
					$this->module = $this->utils->get_module($module);	
					$query .= " AND meta_module_id = ".$this->module['module_id'];
				}//end if				
				
				if(isset($search) && is_array($search)){
					$search = implode("','",$search);
					$query .= " AND meta_label IN ('".$search."')";	
				}
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));	
				if($errors === false){
					$num = $result->rowCount();			
					if($num > 0){							
						$data = array();
						while($row = $result->fetch(PDO::FETCH_ASSOC)){
							if(isset($html) && $html === true){
								$data[$row['meta_label']] = htmlentities($row['meta_value']);	
							} else if(isset($raw) && $raw === true){
								$data[$row['meta_label']] = $row['meta_value'];
							} else {
								$data[$row['meta_label']] = stripslashes(html_entity_decode($row['meta_value']));
							}
						}//end while
						return $data;
					} else {
						return false;
					}//endif
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function add_meta(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value_array){
					${$key} = $value_array;
				}//end if
			} else {
				$id = $params[0];
				$label = $params[1];
				$value = $params[2];
			}//end if
			
			if(isset($id)){
				$this->id = $id;	
			}//end if
					
			//sanitize data
			$label = $this->utils->unifystring($label);
			$label = addslashes($label);
			if(!is_array($value)){
				//$value = htmlentities(addslashes($value));
				$value = htmlspecialchars(addslashes($value), ENT_NOQUOTES, "UTF-8");
			} else {
				$value = serialize($value);
			}//end if
		
			//check if the cart id is a numeric value, as expected
			if(is_numeric($this->id) && isset($this->id)){		
					
				//we should check if the meta already exists
				$query = "SELECT meta_id 
						  FROM meta
						  WHERE meta_label = '".$label."'
						  AND meta_ref_id = ".$this->id."
						  AND meta_module_id = ".$this->module['module_id']."
						  AND meta_type = '".$this->type."'
						  AND meta_solidified = 0";
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));	
				if($errors === false){
					$num = $result->rowCount();
					if($num > 0){
						//something found, so get the id and update the field
						$row = $result->fetch(PDO::FETCH_ASSOC);
						$meta_id = $row['meta_id'];
						$update = "UPDATE meta SET 
								   meta_value = '".$value."' 
								   WHERE meta_id = ".$meta_id." 
								   AND meta_ref_id = ".$this->id." 
								   AND meta_module_id = ".$this->module['module_id']."
								   AND meta_type = '".$this->type."'";
						$result = $this->utils->db->query($update);
						$errors = $this->utils->error($result,__LINE__,get_class($this));	
						if($errors === false){
							return true;	
						} else {
							return false;
						}//end if
					} else {						
						//nothing found, so create a new meta record
						$insert = "INSERT INTO meta
								   (meta_ref_id, meta_label, meta_value, meta_module_id, meta_type)
								   VALUES
								   (".$this->id.",'".$label."','".$value."',".$this->module['module_id'].",'".$this->type."')";								 
						$result = $this->utils->db->query($insert);
						$errors = $this->utils->error($result,__LINE__,get_class($this));	
						if($errors === false){
							return true;	
						} else {
							return false;
						}//end if
					}//end if					
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function delete_meta(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if
			
			if(!isset($post_id)){
				return false;	
			}//end if
			
			if(!isset($module)){
				$module = $this->module;	
			}//end if
			
			if(!isset($type)){
				$type = 'post';	
			}//end if
			
			if(!is_numeric($module)){
				$module_data = $this->utils->get_module($module);
				if($module_data === false){
					//return false;	
					$module_id = NULL;
				} else {
					$module_id = $module_data['module_id'];
				}//end if
			} else {
				$module_id = $module;	
			}//end if
			
			$query = "DELETE FROM meta 
					  WHERE meta_ref_id = ".$post_id."					  
					  AND meta_type = '".$type."'";
			if($module_id != NULL && $module_id !== false){
				$query .= " AND meta_module_id = ".$module_id."";	
			}//end if
			if(isset($label)){
				//in case we want to delete a specific label only
				$query .= " AND meta_label = '".$label."'";
			}//end if			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;	
			} else {
				return false;
			}//end if
			
		}//end function
		
		public function solidify_meta_field(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if
			
			if(!isset($module) || !isset($type) || !isset($label) || !isset($static_field) || !isset($prefix)){
				return false;
			}//end if
			
			$module_data = $this->utils->get_module(array('module' => $module));
			if($module_data === false){
				return false;	
			}//end if
			
			$module_name = $module_data['module_name'];
			$module_id = $module_data['module_id'];
			if(!isset($table)){
				$module_data['module_table'];	
			}//end if
			
			//now we run through the meta fields with the label sent
			$query = "SELECT * 
					  FROM meta 
					  WHERE meta_label = '".$label."' 
					  AND meta_solidified = 0 
					  AND meta_module_id = ".$module_id." 
					  AND meta_type = '".$type."'";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){					
					while($row = $result->fetch(PDO::FETCH_ASSOC)){
						$insert = "UPDATE ".$table." SET ".$static_field." = '".$row['meta_value']."' WHERE ".$prefix."_id = ".$row['meta_ref_id'];
						if($table !== "posts"){
							$insert .= " AND ".$prefix."_module_id = ".$row['meta_module_id'];	
						} else {
							$insert .= " AND ".$prefix."_module_id = '".$module_name."'";	
						}
					//	echo $insert.'<br />';
						$result2 = $this->utils->db->query($insert);
						$errors = $this->utils->error($result,__LINE__,get_class($this));
						//var_dump($errors);
						if($errors === false){
							$update = "UPDATE meta SET meta_solidified = 1 WHERE meta_id = ".$row['meta_id'];	
							//echo $update.'<br />';
							$result3 = $this->utils->db->query($update);
							$this->utils->error($result,__LINE__,get_class($this));
						}//end if
					}//end while
				}//end if
				return true;
			} else {
				return false;
			}//end if
			
		}//end function
	}//end class
?>