<?php
	class users_logs{
		
		public $utils;//utils class
		public $t;//prefix before each tablename
		public $uid;//User ID
		public $gid;//Group ID
		public $lt; //defines the login type, sessions or cookies
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->lt = __LOGIN_TYPE__;
			$this->utils->read_params($this,$params);
			$this->module = "users_logs";
		}//endconstructor		
		
		public function log_access(){
			
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$access_granted = "0";
			if(!isset($user_id)){				
				$user_id = "NULL";	
			} else {
				$access_granted = "1";
			}//end if
	
			$iplog = $_SERVER['REMOTE_ADDR'];
			$request = $_REQUEST;
			unset($request['password']);
			$request_data = base64_encode(serialize($request));
			
			$query = "INSERT INTO users_logs 
					  (user_log_ip, user_log_vars, user_log_from, user_log_uid, user_log_access_granted)
					  VALUES
					  ('".$iplog."','".$request_data."','".$_SERVER["REQUEST_URI"]."',".$user_id.",".$access_granted.")";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));
			
			return true;
		}//end function
		
	}//end class
?>