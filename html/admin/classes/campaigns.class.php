<?php
	require_once('ccci.class.php');

	class campaigns extends ccci{
		
		public $errors = array();
		public $settings = array();
		public $prefix = "campaign";
		protected $meta;
		public $id;
		
		//Constructor
		function __construct($utils,$params = array()){	
			parent::__construct($utils,$params);		
			$this->module = "campaigns";
			$this->utils->read_params($this,$params);			
			//Meta init	
			$this->meta = $this->utils->call("meta");		
			//setting the current module in the class
			$this->set_module($this->module);		
			//Set basic settings
			$this->init_settings();
		}//endconstructor
		
		public function get_campaigns(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($encdata)){
				$encdata = unserialize(base64_decode(urldecode($encdata)));					
				if(isset($encdata['token'])){
					$check = $this->check_token($encdata['token']);					
					if($check === false){
						return false;	
					}//end if
				}//end if
				if(isset($encdata['company_id'])){
					$company_id = $encdata['company_id'];	
				}//end if
			}//end if
			
			if(!isset($fields)){
				$fields = "*";
			}//end if
					
			$query = "SELECT ".$fields." FROM campaigns
					  WHERE campaign_temp = 0";
			
			if(isset($search)){
				$search = str_replace(" ","%",$search);
				$query .= " AND campaign_name LIKE '%".$search."%'";	
			}//end if
			
			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND ".$this->prefix."_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND ".$this->prefix."_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if
			
			if(isset($company_id)){
				$query .= " AND campaign_company_id = ".$company_id;	
			}//end if
			
			if(isset($brand_id)){
				$query .= " AND campaign_brand_id = ".$brand_id;	
			}//end if
			
			if(isset($user_id)){
				$query .= " AND campaign_user_id = ".$user_id;	
			}//end if
			
			$query .= " ORDER BY campaign_date DESC";
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit != NULL){
				$query .= " LIMIT ".$limit;
			}//end if	
			
			$this->utils->cache = $query;
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_campaign(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($id)){
				return false;	
			}//end if
			
			$query = "SELECT * FROM campaigns WHERE campaign_id = ".$id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function save_campaign(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($name) || !isset($company_id) || !isset($brand_id)){
				return false;	
			}//end if
			
			if(!isset($temp)){
				$temp = '0';	
			}//end if
			
			if(!isset($status)){
				$status = 'active';	
			}//end if
			
			if(!isset($user_id)){
				$user_id = $this->utils->get_uid();	
			}//end if
			
			$query = "INSERT INTO campaigns
					 (campaign_name, campaign_brand_id, campaign_uid, campaign_company_id, campaign_status, campaign_temp)
					 VALUES
					 (:name, :brand_id, :user_id, :company_id, :status, :temp)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':name', $name, PDO::PARAM_STR);
			$result->bindParam(':brand_id', $brand_id, PDO::PARAM_INT);
			$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);			
			$result->bindParam(':status', $status, PDO::PARAM_STR);
			$result->bindParam(':temp', $temp, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->id = $this->utils->db->lastInsertId();
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################		
		
		private function init_settings(){			
			//read the settings from the cms
			$this->settings = $this->utils->get_settings(array('module' => $this->module));			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>