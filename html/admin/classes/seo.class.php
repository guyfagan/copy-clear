<?php
	require_once('ewrite.class.php');

	class seo extends ewrite{
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
		}//endconstructor	
		
		
		public function check_permalink(){
			$params = func_get_args();		
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$permalink = $params[0];
				$id = $params[1];
				$type = $params[2];
			}//end if
			
			if(!isset($type) || is_null($type)){
				$type = 'post';	
			}//end if
			
			if(!isset($permalink) || is_null($permalink)){
				return false;	
			}//end if
			
			$permalink = $this->clean_link($permalink);
			if($permalink == ""){
				return false;	
			}//end if
			
			$settings = $this->utils->get_settings();
			if(isset($settings['protectedPermalinks'])){
				$protected = explode("\n",$settings['protectedPermalinks']);
				if(is_array($protected) && sizeof($protected) > 0){
					for($i = 0; $i < sizeof($protected); $i++){						
						if($permalink == trim(strtolower($protected[$i]))){
							return false;	
						}//end if
					}//end if
				}//end if
			}//end if
					
			switch($type){
				case "post":
				case "category":
					$data = $this->get_permalink_data(array('type' => "category", 'permalink' => $permalink));
					
					if ($data == false){
						$data = $this->get_permalink_data(array('type' => "post", 'permalink' => $permalink));				
						if ($data == false){
							return true;
						} else {
							if ($id != NULL && $data['post_id'] == $id && $type=="post"){ //the permalink hasn't changed
								return true;
							} else {
								return false;
							}
						}
					} else {
						if ($id != NULL && $data['category_id'] == $id && $type=="category"){ //the permalink hasn't changed
							return true;
						} else {
							return false;
						}
					}
					break;
				default:				
					if(!isset($class) || !isset($method)){					
						return false;	
					} else {
						
						$data = $class->{$method}(array('type' => $type, 'permalink' => $permalink));							
						if($data === false){
							return true;
						} else {
							return false;
						}										
					}//end if
					break;
			}//end switch
		}//end function
		
		public function return_valid_permalink(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($permalink)){
				return false;	
			}//end if
			
			if(!isset($id)){
				$id = NULL;	
			}//end if
			
			if(!isset($type)){
				$type = "post";	
			}//end if
			
			if(trim($permalink) == ""){
				$permalink = "page";	
			}//end if
			
			$suggestion = $permalink;
			$i = 2;
			$options = array('permalink' => $permalink, 'id' => $id, 'type' => $type);		
			if(isset($class)){
				$options['class'] = $class;	
			}//end iff
			if(isset($method)){
				$options['method'] = $method;
			}//end if			
			
			$result = $this->check_permalink($options);				
			while  ($result === false){
				$permalink = $suggestion . '-' . $i;
				$options['permalink'] = $permalink;
				$result = $this->check_permalink($options);				
				$i++;
			}//end if
			
			
			return ($this->clean_link($permalink));
		}//end function
		
		public function clean_link($text){			
			$avoid_chars = array(',','\'','"',';','\\',':','.','?','!','(',')','[',']','{','}','/','//');
			//$text = strtolower(trim($text));	
			$text = str_replace($avoid_chars, "",$text);
			$text = strtolower(iconv('UTF-8','ASCII//TRANSLIT',strtolower($text)));	
			$text = str_replace("@","at",$text);			
			$text = str_replace("&","and",$text);			
			$text = urlencode(utf8_decode($text));	
			$text = str_replace("and%2339",'-',$text);
			$text = str_replace("+","-",$text);	
			$text = str_replace("--","-",$text);	
			return $text;
		}//end function
		
		public function resolve_permalink(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else if(!is_array($params)){
				$permalink = $params[0];
			}//end if
			
			if(!isset($permalink)){
				return false;	
			}//end if
			
			//check if post
			$options = array('permalink' => $permalink);
			
			if(isset($meta)){
				$options['meta'] = $meta;	
			}//end if
			if(isset($tags)){
				$options['tags'] = $tags;	
			}//end if
			if(isset($default_photo)){				
				$options['default_photo'] = $default_photo;	
			}//end if
			
			$options['type'] = 'post';
			$is_post = $this->get_permalink_data($options);
			if ($is_post !=false){
				$is_post['type'] = "post";
				return $is_post;	
			} else {
				//check if category
				$options['type'] = 'category';
				$is_cat = $this->get_permalink_data($options);
				if ($is_cat !=false){
					$is_cat['type'] = "category";
					return $is_cat;	
				} else {
					return false;	
				}//end if
			}//end if
		}//end function
	
		public function get_permalink_data(){	
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else if(!is_array($params)){
				$type = $params[0];
				$permalink = $params[1];
			}//end if	
			
			if(!isset($permalink)){
				return false;	
			}//end if
			
			if(!isset($type)){
				$type = 'post';	
			}//end if
			
			$options = array("permalink" => $permalink);
			
			if(isset($meta)){
				$options['meta'] = $meta;	
			}//end if
			if(isset($tags)){
				$options['tags'] = $tags;	
			}//end if
			if(isset($default_photo)){			
				$options['default_photo'] = $default_photo;	
			}//end if
			
						
			switch($type){
				case "post":				
					$data = $this->get_post($options);					
					break;
				case "category":
					$data = $this->get_category($options);	
					break;
				default:
					$data = false;	
			}//end switch
			
			return $data;
		}//end function
		
		public function simple_clean_link(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else if(!is_array($params)){				
				$permalink = $params[0];
			}//end if	
			
			if(!isset($type)){
				$type = 'link';	
			}//end if
			
			$permalink = urlencode($permalink);
			$result = strtolower(iconv('ISO-8859-1','ASCII//TRANSLIT',$permalink));
			$result = str_replace("+","-",$result);
			if($type == 'meta'){
				$result = str_replace('-','_',$result);	
			}//end if
			return $result;
		}//end function
		
	}//end class
?>