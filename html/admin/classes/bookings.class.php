<?php
	#################################################################
	# Booking Class - 	last update 08/10/12						#
	#					created on 03/10/12							#
	# based on thirdarge booking class								#				
	#################################################################

	require_once('ewrite.class.php');

	class bookings extends ewrite{
	
		protected $id;//Booking ID
		protected $code;//Booking code
		public $errors = array();	
		public $settings;
		protected $meta;
		public $payment_needed;
		protected $subject_mapping;
		public $stats;
		protected $rate_id;
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);	
			$this->module = "bookings";
			$this->utils->read_params($this,$params);
			//Meta init	
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);	
			$this->set_module($this->module);	
			//Set basic settings
			$this->init_settings();
			$this->stats = array();
		}//endconstructor
		
	
		#Create a unique code for the booking, handy to use it as a reference 
		private function create_code($id){
			$tools = $this->utils->call('tools');
			$chars = strlen($id);
			$chars = 8-$chars;
			$this->code = "";
			if($chars > 0){
				for($k = 0; $k < $chars; $k++){
					$this->code .= $tools->get_random_letter();
				}//end for
			}//end if
			$this->code .= $id;
			
			//Update the booking with the code
			$query = "UPDATE bookings SET booking_code = '".$this->code."' WHERE booking_id = ".$id;			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;	
			} else {
				return false;
			}//end if
		}//end function
		
		# Save Booking
		public function save_booking(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			if(isset($id) && is_numeric($id)){
				$this->id = $id;	
			}//end if
			
			if(isset($module)){
				$module_data = $this->utils->get_module(array('module' => $module));
				if($module_data !== false){
					$module_id = $module_data['module_id'];
				}//end if
			}//end if
			
			if(!isset($module_id)){
				$module_id = 'NULL';	
			}//end if
			
			//check if we passed an user ID, if not we set to 0 (zero)
			if(!isset($uid)){
				$uid = 0;
			} else {
				if($uid == 'auto'){
					$uid = $this->utils->get_uid();
				} else {
					$uid = (int)$uid;
				}//end if
			}//end if
			
			if($uid === false){
				$uid = 0;	
			}//end if
			
			$require_details = true;
			//we check if we need to login the user
			if(isset($do_login) && isset($login_username) && isset($login_password)){
				if((bool)$do_login){
					//we call the auth class
					$auth = $this->utils->call('auth');
					$loggedin = $auth->do_login($login_username,$login_password);
					if(!$loggedin){
						array_push($this->errors,"The login details are incorrect");
						return false;
					} else {
						$uid = $auth->uid;	
						$user_details = $auth->get_user(array('id' => $uid,'meta' => true));
						if($user_details !== false){
							$require_details = false;
							$firstname = $user_details['user_firstname'];
							$surname = $user_details['user_surname'];
							$email = $user_details['user_email'];
							$_POST['meta_phone'] = $user_details['meta_phone'];
						}//end if
					}//end if
				}//end if
			}//end if
			
			if($require_details){
				$firstname = filter_var($_POST['firstname'],FILTER_SANITIZE_STRING,FILTER_FLAG_PATH_REQUIRED);
				if($firstname === false || $firstname == ""){
					array_push($this->errors,"Please fill the Firstname field");				
				}//end if
				
				$surname = filter_var($_POST['surname'],FILTER_SANITIZE_STRING,FILTER_FLAG_PATH_REQUIRED);
				if($surname === false || $surname == ""){
					array_push($this->errors,"Please fill the Surname field");				
				}//end if
				
				$email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL,FILTER_FLAG_PATH_REQUIRED);
				if($email === false || $email == ""){
					array_push($this->errors,"The email address you entered is not valid");				
				}//end if
			}//end if
				
			$quantity = filter_var($_POST['quantity'],FILTER_SANITIZE_NUMBER_INT,FILTER_FLAG_PATH_REQUIRED);
			if($quantity === false || $quantity == ""){
				array_push($this->errors,"Please define a quantity");				
			}//end if
					
			if(isset($rate)){
				$rate = filter_var($rate,FILTER_SANITIZE_NUMBER_INT,FILTER_FLAG_PATH_REQUIRED);
				if($rate === false || $rate == ""){
					array_push($this->errors,"Rate passed is invalid");				
				}//end if				
				$rate_data = $this->get_booking_rate(array('id' => $rate));	
			} else {
				$rate = "0";
			}//end if
			
			if(isset($price)){
				$price = filter_var($price,FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_PATH_REQUIRED);
				if($price === false || $price == ""){
					array_push($this->errors,"Please define a price");				
				}//end if
			}//end if
			
			
			if($_POST['status'] != NULL){
				$status = $_POST['status'];
			} else {
				$status = "pending";
			}//end if
			
			//if it's a new booking
			if(!isset($id)){
				//we get the encrypted data
				$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
				if($encdata == false){
					return false;	
				} else {
					//now we check the token sent				
					$token_result = $this->check_token(array("data" => $encdata));
					if($token_result == false){
						array_push($this->errors,"The passed token is wrong.");	
						return false;	
					}//end if
					
					//let's check if we pass the post_id in the encdata
					if(!isset($post_id) && !is_numeric($post_id)){
						if(isset($encdata['post_id']) && is_numeric($encdata['post_id'])){
							//if something found, we set the post id
							$post_id = (int)$encdata['post_id'];	
							$post_data = $this->get_post(array('id' => $post_id,'meta' => true));						
							if($post_data !== false && isset($rate_data)){
								$price = $post_data['meta_'.$rate_data['booking_rate_meta_field']];
							}//end if
						}//end if
					}//end if
				}//end if
			}//end if
			
			
			//here we check if the user choose to create a new account
			if(isset($create_account) && isset($password) && isset($conf_password)){
				if($create_account){
					//now we check the settings if we are allowed to create accounts
					if(isset($this->settings['booking_enable_accounts']) && (bool)$this->settings['booking_enable_accounts']){
						//then we check if we defined a group of users where the account should be put at the end
						if(isset($this->settings['booking_accounts_users_group']) && is_numeric($this->settings['booking_accounts_users_group'])){
							//if we have everything sorted, we proceed to create an account
							$_POST['username'] = $email;
							$_POST['email'] = $email;
							$_POST['firstname'] = $firstname;
							$_POST['surname'] = $surname;
							$_POST['password'] = $password;
							$_POST['conf_password'] = $conf_password;
							$_POST['active'] = 1;
							$_POST['temp'] = '0';
							$_POST['group_id'] = (int)$this->settings['booking_accounts_users_group'];
							//call the users class
							$users = $this->utils->call('users');
							$user_result = $users->new_user();
							if($user_result === false){
								$users_errors = $users->errors;
								if(sizeof($users_errors) > 0){
									foreach($users_errors as $value){
										array_push($this->errors,$value);	
									}//end foreach
								}//end if								
								return false;	
							} else {
								$uid = $users->uid;
								$this->meta->set_meta_type('user');				
								$this->meta->set_meta_id($uid);	
								$this->meta->add_meta(array("label" => "meta_phone", "value" => $_POST['meta_phone']));
							}//end if
						}//end if
					}//end if
				}//end if
			}//end if
			
			
			
			//if we have no errors we proceed
			if(sizeof($this->errors) == 0){
				$single_price = 0;
				if(isset($post_id) && is_numeric($post_id)){
					if(!isset($price)){
						$post_price = $this->get_single_price(array("id" => $post_id));	
						if($post_price !== false){
							$single_price = $post_price;	
						}//end if
					} else {
						$single_price = $price;
					}//end if
				} else {
					$post_id = 0;
				}//end if
				$amount = 0;
				$paid = true;
				$this->payment_needed = false;
				if($single_price > 0){
					$amount = $single_price * $quantity;
					$this->payment_needed = true;
					$paid = false;
				}//end if
								
				if(!isset($id)){
					//now we should everything sorted, so we proceed with the insert
					$query = "INSERT INTO bookings
							  (booking_uid, booking_post_id, booking_module_id, booking_price, booking_amount, booking_firstname, booking_surname, booking_email, 
							  booking_quantity, booking_paid, booking_status, booking_rate)
							  VALUES
							  (".$uid.",".$post_id.",".$module_id.",'".$single_price."','".$amount."', '".$firstname."','".$surname."','".$email."',".$quantity.",
							  ".(int)$paid.",'".$status."',".$rate.")";								 					  
					$result = $this->utils->db->query($query);
					$error = $this->utils->error($result,__LINE__,get_class($this));		  
					if($error === false){
						//if everything went fine, we get the booking id
						$this->id = $this->utils->db->lastInsertId();
						//now that we have the id, we create a booking code
						$this->create_code($this->id);
						//and last thing, we add the meta
						$this->meta->set_meta_type('booking');	
						$this->meta->set_meta_id($this->id);
						$this->meta->add_meta(array("label" => "encdata", "value" => serialize($encdata)));
						$this->meta->auto_add_meta();
						return true;
					} else {
						return false;
					}//end if
				} else {
					//now we should everything sorted, so we proceed with the insert
					$query = "UPDATE bookings SET
							  booking_firstname = '".$firstname."',
							  booking_surname = '".$surname."',
							  booking_email = '".$email."', 
							  booking_quantity = ".$quantity.", 
							  booking_paid = ".(int)$paid.",
							  booking_status = '".$status."'
							  WHERE booking_id = ".$this->id;							
					$result = $this->utils->db->query($query);
					$error = $this->utils->error($result,__LINE__,get_class($this));		  
					if($error === false){								
						//and last thing, we add the meta
						$this->meta->set_meta_type('booking');	
						$this->meta->set_meta_id($this->id);
						$this->meta->add_meta(array("label" => "meta_encdata", "value" => serialize($encdata)));
						$this->meta->auto_add_meta();
						return true;
					} else {
						return false;
					}//end if
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		# Return all the errors saved during the process
		public function get_errors(){
			return $this->errors;	
		}//end function
		
		# Get the bookings
		public function get_bookings(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($sort_by)){
				$sort_order = "booking_date";	
			}//end if
			
			if(!isset($sort_order)){
				$sort_order = "DESC";	
			}//end if
			
			if(!isset($fields)){
				$fields = "bookings.*, UNIX_TIMESTAMP(booking_date) AS booking_date, post_title, booking_rate_label, users.*";	
			}//end if
			
			if(isset($uid)){
				$user_id = $uid;	
			}//end if
			
			$query = "SELECT ".$fields."
					  FROM (bookings)
					  LEFT JOIN posts
					  ON booking_post_id = post_id
					  LEFT JOIN bookings_rates
					  ON booking_rate_id = booking_rate
					  LEFT JOIN users
					  ON booking_uid = user_id
					  WHERE booking_temp = 0";
			if(isset($user_id) && is_numeric($user_id)){
				$query .= " AND booking_uid = ".$user_id;
			}//end if
			
			if(isset($post_id) && is_numeric($post_id)){
				$query .= " AND booking_post_id = ".$post_id;
			}//end if
			
			if(isset($confirmed) && (is_numeric($confirmed) || is_bool($confirmed))){
				$query .= " AND booking_confirmed = ".(int)$confirmed;
			}//end if
			
			if(isset($notified) && (is_numeric($notified) || is_bool($notified))){
				$query .= " AND booking_notification_sent = ".(int)$notified;
			}//end if
					 
			if(isset($status) && is_string($status)){
				$query .= " AND booking_status = '".$status."'";
			}//end if
			
			if(isset($email) && is_string($email)){
				$query .= " AND booking_email = '".trim($email)."'";
			}//end if
					  
			$query .= " ORDER BY booking_date DESC";
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit != NULL){
				$query .= " LIMIT ".$limit;
			}//end if
			
				
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);					
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type('booking');		
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['booking_id']));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						$this->meta->set_meta_type('booking');		
						//if it's an array, we just get the meta we asked for
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['booking_id'],"search" => $meta));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_booking(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			//check if we have the booking id
			if(isset($id) && is_numeric($id)){
				$this->id = $id;	
			} else {
				array_push($this->errors,"No booking ID passed");
				return false;	
			}//end if
			
			if(!isset($fields)){
				$fields = "*, UNIX_TIMESTAMP(booking_date) AS booking_date, UNIX_TIMESTAMP(post_date) AS post_date, booking_rate_label";	
			}//end if
			
			$query = "SELECT ".$fields."
					  FROM (bookings)
					  LEFT JOIN bookings_rates
					  ON booking_rate_id = booking_rate
					  LEFT JOIN posts
					  ON post_id = booking_post_id 
					  WHERE booking_id = ".$this->id;
			if(isset($user_id)){
				$query .= " AND booking_uid =".$user_id;
			}//end if
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);
					/* META SUPPORT */	
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$meta_data = $this->meta->get_meta(array("id" => $this->id));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$meta_data = $this->meta->get_meta(array("id" => $this->id,"search" => $meta));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					}//end if
					/* END META SUPPORT */	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function			
		
		public function get_booking_id(){
			if(isset($this->id) && is_numeric($this->id)){
				return (int)$this->id;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_max_bookable_seats(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($data)){
				return false;	
			}//end if
			
			$seats_fields = explode(",",$this->settings['booking_seats_field']);
			if(is_array($seats_fields)){
				foreach($seats_fields as $value){					
					if(isset($data[$value]) && is_numeric($data[$value])){
						return $data[$value];	
					}//end if
				}//end foreach
			} else {
				return false;	
			}//end if
		}//end function
		
		public function is_sold_out(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($post_id)){
				return false;	
			}//end if
			
			//get post data
			$event = $this->get_post(array('id' => $post_id,'meta' => true));
			
			$booked_seats = $this->get_booked_seats(array('post_id' => $post_id));
			$max_seats = $this->get_max_bookable_seats(array('data' => $event));
			
			$this->stats['booked_seats'] = 0;
			if($booked_seats !== false){
				$this->stats['booked_seats'] = $booked_seats;
			}//end if
			
			if($max_seats !== false && !is_null($max_seats)){
				$this->stats['max_seats'] = $max_seats;	
				if($booked_seats !== false){
					$seats_left = $max_seats - $booked_seats;
					$this->stats['seats_left'] = $seats_left;
				}//end if
			}
			$this->stats['soldout'] = false;
			if(isset($this->stats['max_seats']) && $this->stats['booked_seats'] > 0){
				if($booked_seats >= $max_seats){
					$this->stats['soldout'] = true;
					return true;	
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}
			
		}//end function
		
		public function get_booked_seats(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			//check if we have the booking id
			if(!isset($post_id) && !is_numeric($post_id)){			
				array_push($this->errors,"No post ID passed");
				return false;	
			}//end if
			
			$query = "SELECT SUM(booking_quantity) AS total_quantity
					  FROM (bookings)					  
					  WHERE booking_post_id = ".$post_id."
					  AND booking_status IN ('confirmed','paid')";
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);					 
					return $data['total_quantity'];
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function	
		
		public function set_booking_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($id) || !isset($payment_id)){
				return false;
			}//end if
			
			$query = "UPDATE bookings SET booking_payment_id = ".$payment_id." WHERE booking_id = ".$id;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		# Complete Booking
		public function confirm_booking(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$id = $params[0];			
			}//end if
			if(!isset($id) && !is_numeric($id)){
				return false;	
			} else {
				$this->id = $id;
			}//end if
			
			if(!isset($status)){
				$status = 'confirmed';
			}//end if
			
			$query = "UPDATE bookings SET booking_temp = 0, booking_confirmed = 1, booking_status = '".$status."'";
			if(isset($payment_id) && is_numeric($payment_id)){
				$query .= ", booking_paid = 1, booking_payment_id = ".$payment_id;	
			}//end if
			$query .= " WHERE booking_id = ".$this->id;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));
			if($error == false){
				$this->booking_notification(array("id" => $this->id));
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		
		#Send a notification of the completed booking
		private function booking_notification(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$id = $params[0];			
			}//end if
			if(!isset($id) && !is_numeric($id)){
				return false;	
			} else {
				$this->id = $id;
			}//end if
			
			//Message type, it could be html or text
			if(!isset($type)){
				$type = $this->settings['booking_email_type'];	
			}//end if
			
			if(!isset($force_send)){
				$force_send = false;	
			}//end if
			
			$genSettings = $this->utils->get_settings();
			
			$booking_data = $this->get_booking(array("id" => $this->id));			
			if($booking_data['booking_notification_sent'] == "0" || $force_send == true){		
				$email = $booking_data['booking_email'];				
				
				//$payments = $this->utils->call("payments");
				#$payment_id_ref =$payments->get_payment_code($booking_data['booking_payment_id']);
				
				//create the mailer obj				
				$mailer = $this->utils->call("mailer");
				$mailer->set_sender($this->settings['no_reply_email']);	
				if(isset($this->settings['bcc_email']) && $this->settings['bcc_email'] != ""){
					$mailer->set_bcc($this->settings['bcc_email']);
				}//end if
				if($type == "html"){
					$mailer->defaultFormat = 'text/html';					
				} else {
					$mailer->defaultFormat = 'text/plain';					
				}//end if
								
				if($this->settings['booking_email_subject'] == NULL){
					$subject = "Booking notification";			
				} else {
					$subject = $this->settings['booking_email_subject'];
					$subject = $this->parse_subject_mapping(array("subject" => $subject, "data" => $booking_data));	
				}//end if
						
				if(isset($this->settings['booking_email_signature']) && strlen(trim($this->settings['booking_email_signature'])) > 0){
					$signature = $this->settings['booking_email_signature'];
					if($type == "text"){
						$signature = strip_tags($signature);	
					}					
				}//end if
				
				$options = array("data" => $booking_data, "type" => $type);
				if(isset($signature)){
					$options['signature'] = $signature;	
				}//end if
				$message = $this->get_notification_message($options);
				
				$mailer->set_message(array('message' => $message));
				$mailresult = $mailer->send_mail(array('email' => array($email),'subject' => $subject));
				if($mailresult){
					//update the notification status of the booking		
					$query = "UPDATE bookings SET booking_notification_sent = 1 WHERE booking_id = ".$this->id;
					$result = $this->utils->db->query($query);
					$error = $this->utils->error($result,__LINE__,get_class($this));	
					return true;
				} else {
					return false;	
				}//end if			
				
			} else {
				return false;
			}//end if
		}//end function
		
		public function resubmit_notification_message(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if
			
			$this->booking_notification(array('id' => $id,'force_send' => true));
			return true;
		}//end function
		
		protected function get_notification_message(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$data = $params[0];			
			}//end if
			
			if(!isset($data) && !is_array($data)){
				array_push($this->errors,"No data passed to the notification manager");
				return false;	
			}//end if
		
			if(!isset($type)){
				$type = "html";	
			}//end if
			
			$post_id = $data['booking_post_id'];
			if($post_id != 0){
				$post_data = $this->get_post(array("id" => $post_id));	
			}//end if			
		
			switch($type){
				case "html":
					$genSettings = $this->utils->get_settings();
					$html_message = "<p>Hi ".$data['booking_firstname'].',</p>';
					if($post_data != false){
						$html_message .= "<p>This is your booking summary:</p>";
						//payment details				
						if($data['booking_price'] == 0){
							$html_message .= "<p>".$data['booking_quantity']." x ".$post_data['post_title']."</p>"; 
						} else {
							if($data['booking_rate'] > 0){
								$html_message .= "<p>".$data['booking_quantity']." x ".$post_data['post_title']." @ ".$data['booking_price']." (".$data['booking_rate_label'].") = ".$data['booking_amount']."</p>"; 
							} else {
								$html_message .= "<p>".$data['booking_quantity']." x ".$post_data['post_title']." @ ".$data['booking_price']." = ".$data['booking_amount']."</p>"; 
							}//end if
						}//end if
						$html_message .= "<p>And your booking reference code is <strong>".$data['booking_code']."</strong></p>";
					} else {
						$html_message .= "<p>This email is just to confirm your booking, and your booking reference code is <strong>".$data['booking_code']."</strong></p>";
					}//end if
					
					//now we check if we have to apply a template
					if((bool)$genSettings['enableEmailTemplates'] === true){
						//we get the template from the module settings
						$template = $this->settings['defaultModuleEmailTemplate'];
						if($template != ""){							
							//so if we have a template defined, we process the file
							$tpld = array();//Template Data
							//in this array we pass the data to the html email template
							$tpld['message'] = $html_message;	
							if(isset($signature)){
								$tpld['signature'] = $signature;	
							}//end if
							$html = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
							if($html !== false){
								$html_message = $html;	
							}//end if
						} else {
							if(isset($signature)){
								$html_message .= $signature;
							}//end if
						}//end if
					} else {
						if(isset($signature)){
							$html_message .= $signature;
						}//end if
					}//end if
					$message = $html_message;
				break;
			}//end switch
			return $message;
		}//end function
		
		public function get_single_price(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$id = $params[0];			
			}//end if
			
			//we check if we passed already an array to parse
			if(isset($data) && is_array($data)){
				if($this->settings['field_price'] != NULL){
					if(isset($data[$this->settings['field_price']]) && is_numeric($data[$this->settings['field_price']])){
						return (float)$data[$this->settings['field_price']];	
					} else {
						array_push($this->errors,"Can't find a price for this element");
						return false;	
					}//end if
				} else {
					array_push($this->errors,"Can't find a price for this element");
					return false;	
				}//end if
			} else {
				//if not, we try to find the cost from the posts table, and we also have to check if we set a field_price in the settings 
				//and we passed a valid id
				if($this->settings['field_price'] != NULL && isset($id) && is_numeric($id)){
					$post = $this->get_post(array('id' => $id, 'meta' => true));
					if($post !== false){
						if(isset($post[$this->settings['field_price']])){
							return (float)$post[$this->settings['field_price']];
						} else {
							array_push($this->errors,"Can't find a price for this element");
							return false;	
						}
					} else {
						array_push($this->errors,"Can't find a price for this element");
						return false;	
					}//end if					
				} else {
					array_push($this->errors,"Can't find a price for this element");
					return false;	
				}//end if
			}//end if
			
		}//end if
		
		#################################################################
		# BOOKINGS RATES PART											#		
		#################################################################
		
		public function get_booking_rates(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($sort_order)){
				$sort_order = "ASC";	
			}//end if		
			
			$query = "SELECT *
					  FROM (bookings_rates)
					  ORDER BY booking_rate_label ".$sort_order;
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit != NULL){
				$query .= " LIMIT ".$limit;
			}//end if
			//echo $query;
				
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function delete_booking_rate(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($id)){
				return false;	
			}//end if
			
			if(!is_array($id)){
				$id = array($id);	
			}//end if
			try{
				for($i = 0; $i < sizeof($id); $i++){
					$query = "DELETE FROM bookings_rates WHERE booking_rate_id = ".$id[$i];
					$result = $this->utils->db->query($query);
					$error = $this->utils->error($result,__LINE__,get_class($this));	
					if($error){
						throw new Exception('Cannot delete the data.');
					}//end if
				}//end for i
				return true;
			} catch(Exception $e){
				$this->errors = $e->getMessage();
				return false;	
			}
		}//end function
		
		public function get_booking_rate(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			//check if we have the booking id
			if(isset($id) && is_numeric($id)){
				$this->rate_id = $id;	
			} else {
				array_push($this->errors,"No booking rate ID passed");
				return false;	
			}//end if	
			
			$query = "SELECT *
					  FROM (bookings_rates)					 
					  WHERE booking_rate_id = ".$this->rate_id."
					  LIMIT 1";					 
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);					
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_booking_rate_id(){
			return $this->rate_id;
		}//end function
		
		public function save_booking_rate(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			$data = $_POST;
					
			if($_REQUEST['id'] == NULL){
				//Is a new rate					
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "booking_rate","sql_type" => "INSERT", "method_data" => $data, "module" => $this->module));
				$query = "INSERT INTO bookings_rates ".$sql;				
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$this->rate_id = $this->utils->db->lastInsertId();				
					return true;
				} else {
					return false;	
				}//end if
			} else {
				//Editing an existent rate
				$this->rate_id = (int)$_REQUEST['id'];
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "booking_rate","sql_type" => "UPDATE", "method_data" => $data,"module" => $this->module));				
				$query = "UPDATE bookings_rates SET ".$sql." WHERE booking_rate_id = ".$this->rate_id;	
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					return true;
				} else {
					return false;	
				}//end if
			}//endif	
		}//end function
		
		#################################################################
		# SECURITY PART													#		
		#################################################################
		
		protected function check_token(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			$template = $this->settings['token_template'];
			if(isset($data) && is_array($data)){				
				//find all the elements in the passed data array
				foreach($data as $key => $value){
					$template = str_replace("{#".$key."#}",$value,$template);		
				}//end foreach
				if(defined("__SECRET_KEY__")){
					$template = str_replace("{#SECRET_KEY#}",__SECRET_KEY__,$template);	
				} else {
					$template = str_replace("{#SECRET_KEY#}",$this->settings['secret_key'],$template);	
				}//end if
				
				if(isset($data['token']) && !is_null($data['token'])){
					if($data['token'] == sha1($template)){
						return true;
					} else {
						return false;
					}//end if
				} else {
					return false;	
				}
			} else {
				return false;
			}//end if
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
		
		protected function parse_subject_mapping(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			if(!isset($subject) && !isset($data)){
				return false;	
			}//end if
			$this->subject_mapping = array();	
			$this->subject_mapping['day'] = date("j",$data['post_date']);
			$this->subject_mapping['ordinal'] = date("S",$data['post_date']);
			$this->subject_mapping['month'] = date("F",$data['post_date']);
			$this->subject_mapping['year'] = date("Y",$data['post_date']);
			$this->subject_mapping['title'] = $data["post_title"];
			
			foreach($this->subject_mapping as $key => $value){
				$subject = str_replace('['.$key.']',$value,$subject);
			}//end foreach
			
			return $subject;
		}//end if
		
		private function init_settings(){
			$this->settings = array();
			$settings = $this->utils->get_settings(array('module' => 'bookings'));
			$this->settings['no_reply_email'] = $settings['booking_no_reply'];
			$this->settings['bcc_email'] = 	$settings['booking_bcc_on_notification'];
			$this->settings['field_price'] = $settings['booking_price_field'];	
			$this->settings['token_template'] = "{#timestamp#}:{#post_id#}:{#SECRET_KEY#}";
			$this->settings['secret_key'] = $settings['secret_key'];
			if($settings['booking_token_template'] != ""){			
				$this->settings['token_template'] = $settings['booking_token_template'];
			} 
			//read the settings from the cms
			$cms_settings = $this->utils->get_settings(array('module' => $this->module));
			if($cms_settings !== false){
				//if we have something, merge the two arrays, bear in mind that the cms settings will override the previous settings
				$this->settings = array_merge($this->settings,(array)$cms_settings);	
			}//end if
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
		
	}//end class
?>