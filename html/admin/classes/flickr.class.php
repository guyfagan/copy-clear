<?php
	class flickr{
		
		private $api_key;
		private $api_secret;
		private $user_id;
		private $rest_url;
		private $flickr_params;
		private $flickr_penc;//Encoded parameters
		private $flickr_stats;
		private $obj_data;
		private $photo_id;
		private $photoset_id;
		private $photo_size;
		
		//Constructor
		function __construct(){
			$this->api_key = "d757e5c2c58f83de21bee213a03a2d9d";
			$this->api_secret = "f92c63bf57c84ccb";
			$this->user_id = "24144028@N00";
			$this->rest_url = "http://api.flickr.com/services/rest/?";
			$this->flickr_params = array("api_key" => $this->api_key, "format" => "php_serial");
			$this->flickr_stats = array();
			$this->photo_size = "_s";
		}//endconstructor
		
		private function encode_params($params){
			$this->flickr_penc = array();
			foreach($params as $key => $value){
				$this->flickr_penc[] = urlencode($key).'='.urlencode($value);	
			}//end foreach
		}//end function 
		
		private function get_url_call($params){
			$encdata = $this->encode_params($params);
			$url_call = $this->rest_url.implode("&",$this->flickr_penc);		
			return $url_call;
		}//end function
		
		private function get_flickr_answer($params){
			$url_call = $this->get_url_call($params);
			$answer = @file_get_contents($url_call);
			//unserialize the answer
			$obj = unserialize($answer);
			return $obj;
		}//end function
		
		public function set_photoset_id($photoset_id){
			$this->photoset_id = $photoset_id;
		}//end function
		
		public function set_photo_size($size = NULL){
			if($size == NULL){
				$this->photo_size = NULL;
			} else {
				$this->photo_size = "_".$size;
			}//end if
		}//end function
		
		public function get_photoset_photos(){
			$params = array();
			$params = $this->flickr_params;
			$params['photoset_id'] = $this->photoset_id;	
			$params['method'] = 'flickr.photosets.getPhotos';
			$obj = $this->get_flickr_answer($params);
			if($obj['stat'] == "ok"){
			//	print_r($obj);
			//	echo "<br />------------------------------<br />";
				return $obj;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_latest_photos(){
			$params = array();
			$params = $this->flickr_params;
			$params['user_id'] = $this->flickr_id;	
			$params['per_page'] = $this->per_page;	
			$params['method'] = 'flickr.photos.search';
			$obj = $this->get_flickr_answer($params);
			if($obj['stat'] == "ok"){
			//	print_r($obj);
			//	echo "<br />------------------------------<br />";
				return $obj;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_photoset_list(){
			$params = array();
			$params = $this->flickr_params;
			$params['photoset_id'] = $this->photoset_id;	
			$params['method'] = 'flickr.photosets.getList';
			$obj = $this->get_flickr_answer($params);
			if($obj['stat'] == "ok"){
				return $obj;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_photoset_info($turn_to_array = false){
			$params = array();
			$params = $this->flickr_params;
			$params['photoset_id'] = $this->photoset_id;	
			$params['method'] = 'flickr.photosets.getInfo';
			$obj = $this->get_flickr_answer($params);
			if($obj['stat'] == "ok"){
				//var_dump($obj);
				return $obj;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_array_photoset($size = "s"){
			$obj = $this->get_photoset_photos($this->photoset_id);
			$this->obj_data = $obj;
			if($obj != false){
				$tot_photos = $obj['photoset']['total'];
				$this->flickr_stats['total'] = $tot_photos;
				$this->flickr_stats['default_photo'] = $obj['photoset']['primary'];
				$data = array();
				$basepath = $obj['photoset']['photo'];
				for($i = 0; $i < $tot_photos; $i++){
					$farm = $basepath[$i]['farm'];
					$title = $basepath[$i]['title']['_content'];
					$server = $basepath[$i]['server'];
					$id = $basepath[$i]['id'];
					$owner = $basepath[$i]['owner'];
					$secret = $basepath[$i]['secret'];
					$img_url = "http://farm".$farm.".static.flickr.com/".$server."/".$id."_".$secret.$this->photo_size.".jpg";
					$data[$i] = array();
					$data[$i]['static_image'] = $img_url;
					$data[$i]['owner'] = $owner;
					$data[$i]['title'] = $title;
					$data[$i]['id'] = $id;
					$data[$i]['uri_image'] = "http://www.flickr.com/photos/".$owner."/".$id;
				}//end for
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_default_photoset_image($data = NULL){
			//check if we already asked to the API the information
			if($data != NULL){
				$obj = $this->obj_data;
				if($obj != false){
					$tot_photos = $obj['photoset']['total'];
					$basepath = $obj['photoset']['photo'];
					$found = array();
					for($i = 0; $i < $tot_photos; $i++){
						if($data[$i]['id'] == $obj['photoset']['primary']){
							$found = $data[$i];
						}//end if
					}//end for i
					return $found;
				} else {
					return false;
				}//end if
			} else {
				//if not, ask the missing information to the flickr API
				$this->obj_data = $this->get_photoset_info();
				$obj = $this->obj_data;
				if($obj != false){
					$this->photo_id = $obj['photoset']['primary'];
					$photo_data = $this->get_photo_info();
				
					$farm =$photo_data['photo']['farm'];
					$title = $photo_data['photo']['title']['_content'];
					$server = $photo_data['photo']['server'];
					$id = $this->photo_id;
					$owner = $photo_data['photo']['owner']['nsid'];
					$secret = $photo_data['photo']['secret'];
					$img_url = "http://farm".$farm.".static.flickr.com/".$server."/".$id."_".$secret.$this->photo_size.".jpg";
					$data= array();
					$data['static_image'] = $img_url;
					$data['owner'] = $owner;
					$data['title'] = $title;
					$data['id'] = $id;
					$data['uri_image'] = "http://www.flickr.com/photos/".$owner."/".$id;
					return $data;
				} else {
					return false;
				}//end if
			}//end if
		}//end function
		
		public function get_photoset_id_from_url($url){
			$sets_pos = strpos($url,"/sets/");	
			if($sets_pos !== false){
				$this->photoset_id = substr($url,($sets_pos+6));
				$this->photoset_id = str_replace("/","",$this->photoset_id);
				//echo $this->photoset_id;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_photo_info(){
			$params = array();
			$params = $this->flickr_params;
			$params['photo_id'] = $this->photo_id;	
			$params['method'] = 'flickr.photos.getInfo';
			$obj = $this->get_flickr_answer($params);
			if($obj['stat'] == "ok"){
				return $obj;
			} else {
				return false;	
			}//end if
		}//end if
		
		public function get_recent(){
			$params = array();
			$params = $this->flickr_params;
			$params['photoset_id'] = $this->photoset_id;	
			$params['method'] = 'flickr.people.getPublicPhotos';
			$params['user_id'] = $this->user_id;
			$params['per_page'] = 12;
			$obj = $this->get_flickr_answer($params);
			if($obj['stat'] == "ok"){				
				return $obj;
			} else {
				return false;	
			}//end if
		}//end function
	}//end class
?>