<?php
	require_once('utils.class.php');
	
	class utils_legacy extends utils{

		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->show_addfields = true;
			$this->filters = array();	
		}//endconstructor	
		
		public function show_additional_fields($show_addfields){
			if(is_bool($show_addfields)){
				$this->show_addfields = $show_addfields;
			}//end if
		}//end function	
		
		//this is an interface function, by the new page you can create a new module
		public function save_page($path = "../"){
			$page_name = trim($_REQUEST['page_name']);
			if($page_name == NULL){
				return false;
			}//end if		
			//echo $page_name;
			$module_label = ucwords($page_name);
			$module_name = $this->unifystring(strtolower($page_name));		
			if($_REQUEST['id'] == NULL){
				//Get the last order id
				$query = "SELECT module_order FROM ".$this->t."modules ORDER BY module_order DESC LIMIT 1";
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				$row = @mysql_fetch_array($result);
				$last_order = $row['module_order'];
				$last_order++;
				//Is a new module		
				$query = "INSERT INTO ".$this->t."modules 
						  (module_name, module_label,module_order)
						  VALUES
						  ('".$module_name."','".$module_label."',".$last_order.")";
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				$this->module_id = @mysql_insert_id();		
				//Create the initial permissions for this module
				$this->init_module_permissions();			
			} else {
				$query = "UPDATE ".$this->t."modules SET
						  module_name = '".$module_name."',
						  module_label = '".$module_label."'
						  WHERE module_id = ".$_REQUEST['id'];
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				$this->module_id = $_REQUEST['id'];
			}//end if	
			
			$this->create_default_settings();
			
			//Create the folder under the upload directory
			if(!file_exists($path."upload/".$module_name)){
				mkdir($path."upload/".$module_name,0777);
			}//end if			
			return true;
		}//end function
		
		public function create_permalink_paging_list($range = NULL,$skip_ul_tag = false,$ul_class = "cf",$li_selected_class = "sel"){
			$pages = ceil($this->p_max/$this->p_limit);				
			/*if($pages == 0){
				$pages = 1;
			}//end if*/		
			if($ul_class != NULL){
				$ul_class = ' class="'.$ul_class.'"';
			}
			if($skip_ul_tag == false){
				$html = "<ul".$ul_class.">\n";
			} else {
				$html = "";
			}//end if
			if($range != NULL && is_numeric($range)){
				$just_beginned = false;
				$end_reached = false;
				$lower_limit = $this->p_current;
				$lower_limit -= ceil($range/2);
				//In case the limit is under 0, like in case the selected page is 1, 
				if($lower_limit < 0){
					$just_beginned = true;
					$lower_limit += abs($lower_limit);
				}//end if
				if(($this->p_current + ceil($range/2)) >= $pages){
					$end_reached = true;
					$lower_limit -= ($this->p_current + ceil($range/2)) - ($pages+1);
				}//end if
			}//end if			
			for($i = 1; $i <= $pages; $i++){
				if($range == NULL || ($range != NULL && ($i >= $lower_limit && $i <= ($lower_limit+$range)))){
					//Create the link
					$page_url = "page-".$i.'.html';
					$html .= "<li";
					if($li_selected_class != NULL && $i == $this->p_current){
						$html .= ' class="'.$li_selected_class.'"';
					}//end if
					$html .= "><a href=\"".__BASEPATH__.$_GET['s'].'/'.$page_url."\">";
					if($i == $this->p_current){
						$html .= $i;
					} else {
						$html .= $i;
					}//end if
					$html .= "</a></li>\n";
				}//end if
			}//end for
			if($skip_ul_tag == false){
				$html .= "</ul>\n";
			}//end if
			return $html;
		}//end function
		
		public function create_paging_list($range = NULL,$skip_ul_tag = false,$ul_class = NULL,$li_selected_class = NULL){
			$pages = ceil($this->p_max/$this->p_limit);		
			/*if($pages == 0){
				$pages = 1;
			}//end if*/
	
			$this_page = $_SERVER['PHP_SELF'];
			$this_page = basename($this_page);
			
			//Catch all the GET data and put in the string url
			$url_vars = $this->get_url_vars();
			
			if($ul_class != NULL){
				$ul_class = ' class="'.$ul_class.'"';
			}
			if($skip_ul_tag == false){
				$html = "<ul".$ul_class.">\n";
			} else {
				$html = "";
			}//end if
			if($range != NULL && is_numeric($range)){
				$just_beginned = false;
				$end_reached = false;
				$lower_limit = $this->p_current;
				$lower_limit -= ceil($range/2);
				//In case the limit is under 0, like in case the selected page is 1, 
				if($lower_limit < 0){
					$just_beginned = true;
					$lower_limit += abs($lower_limit);
				}//end if
				if(($this->p_current + ceil($range/2)) >= $pages){
					$end_reached = true;
					$lower_limit -= ($this->p_current + ceil($range/2)) - ($pages+1);
				}//end if
				/*if($just_beginned == false){
					//Create the link
					$page_url = "?page=1";
					$html .= "<li><a href=\"".$this_page.$page_url.$url_vars."\">";			
					$html .= "<span>begin</span>";				
					$html .= "</a></li>\n";
				}//end if	*/			
			}//end if			
			
			for($i = 1; $i <= $pages; $i++){
				if($range == NULL || ($range != NULL && ($i >= $lower_limit && $i <= ($lower_limit+$range)))){
					//Create the link
					$page_url = "?page=".$i;
					$html .= "<li";
					if($li_selected_class != NULL && $i == $this->p_current){
						$html .= ' class="'.$li_selected_class.'"';
					}//end if
					$html .= "><a href=\"".$this_page.$page_url.$url_vars."\">";
					if($i == $this->p_current){
						$html .= "<strong>".$i."</strong>";
					} else {
						$html .= "<span>".$i."</span>";
					}//end if
					$html .= "</a></li>\n";
				}//end if
			}//end for
			/*if($range != NULL && is_numeric($range)){
				if($end_reached == false){
					//Create the link
					$page_url = "?page=".$pages;
					$html .= "<li><a href=\"".$this_page.$page_url.$url_vars."\">";			
					$html .= "<span>end</span>";				
					$html .= "</a></li>\n";
				}//end if
			}//end if*/
			if($skip_ul_tag == false){
				$html .= "</ul>\n";
			}//end if
			return $html;
		}//end function
		
		public function save_module(){
			$hidden_fields = array('action','step','id','submit');
			if($_REQUEST['id'] == NULL){
				//Is a new module		
				$sql = $this->build_sql_fields("module","INSERT","POST",$hidden_fields);				
				$query = "INSERT INTO ".$this->t."modules ".$sql;	
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				$this->module_id = @mysql_insert_id();	
				//Create the initial permissions for this module
				$this->init_module_permissions();			
			} else {
				//Editing an existent module
				$this->module_id = (int)$_REQUEST['id'];
				$sql = $this->build_sql_fields("module","UPDATE","POST",$hidden_fields);				
				$query = "UPDATE ".$this->t."modules SET ".$sql." WHERE module_id = ".$this->module_id;
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));		
			}//endif	
			
			$this->create_default_settings();	
			
			//Create the folder under the upload directory
			if(!file_exists("../upload/".$_REQUEST['name'])){
				mkdir("../upload/".$_REQUEST['name'],0777);
			}//end if	
			return true;
		}//end function
		
		public function create_default_settings(){
			$def_settings = array();
			$def_settings['m_width'] = 200;
			$def_settings['m_height'] = '*';
			$def_settings['t_width'] = 64;
			$def_settings['t_height'] = '*';
			$def_settings['show_categories'] = 1;
			$def_settings['use_date_picker'] = 1;			
			$module_data = $this->get_module($this->module_id);
			$module_name = $module_data['module_name'];
			
			//delete if already exists settings with this module name
			$query = "DELETE FROM ".$this->t."settings  WHERE setting_module = '".$modul_name."'";
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));	
			
			foreach($def_settings as $key => $value){
				if(is_string($value)){
					$query = "INSERT INTO ".$this->t."settings 
							  (setting_field_name, setting_value_char, setting_module)
							  VALUES
							  ('".$key."','".$value."','".$module_name."')";
				} else {
					$query = "INSERT INTO ".$this->t."settings 
							  (setting_field_name, setting_value_int, setting_module)
							  VALUES
							  ('".$key."',".$value.",'".$module_name."')";
				}//end if
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));		
			}//end foreach
		}//end function
		
		public function delete_module($module_id, $delete_files = true){
			$module_data = $this->get_module($module_id);
			$module_id = $module_data['module_id'];
			$module_name = $module_data['module_name'];
			//First, delete all the files
			if($delete_files){
				$this->empty_directory("upload/".$module_name."/files/t/");
				$this->empty_directory("upload/".$module_name."/files/");
				$this->empty_directory("upload/".$module_name."/photos/t/");
				$this->empty_directory("upload/".$module_name."/");
			}//end if
			$query = "DELETE FROM ".$this->t."modules WHERE module_id = ".$module_id;
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));	
			return true;
		}//end function
		
		public function clone_maintable($module_id,$type = "full"){
			$module_data = $this->get_module($module_id);
			$module_id = $module_data['module_id'];
			$module_name = $module_data['module_name'];
			$newtable = $module_data['module_table'];
			if(substr($newtable,-3) == "ies"){
				$prefix = substr($newtable,0,-3);
				$prefix .= "y";
			} else if(substr($newtable,-1) == "s"){
				$prefix = substr($newtable,0,-1);
			} else {
				$prefix = $newtable;
			}//end if
			switch($type){
				case "full":
					$query = "CREATE TABLE `".$newtable."` (
							  `".$prefix."_id` int(11) NOT NULL auto_increment,
							  `".$prefix."_title` text NOT NULL,
							  `".$prefix."_subtitle` tinytext,
							  `".$prefix."_message` text,
							  `".$prefix."_date` timestamp NOT NULL default CURRENT_TIMESTAMP,							  
							  `".$prefix."_lang` int(11) NOT NULL default '1',
							  `".$prefix."_status` tinyint(1) NOT NULL default '1',
							  `".$prefix."_uid` int(11) NOT NULL default '0',
							  `".$prefix."_tags` varchar(255) default NULL,						
							  `".$prefix."_module_id` varchar(50) NOT NULL default '".$module_name."',							 
							  PRIMARY KEY  (`".$prefix."_id`)
							) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
					break;
				case "tiny":
					$query = "CREATE TABLE `".$newtable."` (
							  `".$prefix."_id` int(11) NOT NULL auto_increment,
							  `".$prefix."_title` text NOT NULL,						
							  `".$prefix."_message` text,
							  `".$prefix."_date` timestamp NOT NULL default CURRENT_TIMESTAMP,							  
							  `".$prefix."_lang` int(11) NOT NULL default '1',
							  `".$prefix."_uid` int(11) NOT NULL default '0',									
							  `".$prefix."_module_id` varchar(50) NOT NULL default '".$module_name."',							 
							  PRIMARY KEY  (`".$prefix."_id`)
							) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
					break;
			}//end switch
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));	
		}//end function
		
			public function update_post_field($postID,$fieldName,$fieldValue){
			$query = "UPDATE ".$this->t."posts SET ".$fieldName." = '".$fieldValue."' WHERE post_id = ".$postID;
			$result = @mysql_query($query,$this->db);
			if($this->error($result,__LINE__,get_class($this))){
				return false;
			} else {
				return true;
			}//end if
		}//end if
		
		public function update_category_field($cid,$fieldName,$fieldValue){
			$query = "UPDATE ".$this->t."categories SET ".$fieldName." = '".$fieldValue."' WHERE category_id = ".$cid;
			$result = @mysql_query($query,$this->db);
			if($this->error($result,__LINE__,get_class($this))){
				return false;
			} else {
				return true;
			}//end if
		}//end if
		
		public function field_to_label($field){
			$fu = strpos($field,"_");
			$field = substr($field,$fu,strlen($field));
			$field = str_replace("_"," ",$field);
			$field = ucwords($field);
			return $field;
		}//end function
		
		//check if the file passed has a valid extension
		public function is_file_allowed($filename){		
			//get the general settings
			$gen_settings = $this->get_settings();
			$filename = strtolower($filename);
			//get the file extension
			$file_ext = $this->get_file_ext($filename);		
			if($file_ext != false){
				//get the allowed types
				$allowed = $gen_settings['allowed_filetypes'];
				if(trim($allowed) == NULL){
					//it could means that there are no settings for that, so allow all
					return true;
				} else {
					//get an array with allowed file ext
					$allowed = str_replace(" ","",$allowed);
					$allowed = str_replace("*","",$allowed);
					$allowed = explode(",",$allowed);				
					//check the file extension if it's on the allowed ones
					if(in_array($file_ext,$allowed)){
						return true;
					} else {
						return false;
					}//end if
				}//end if
			} else {
				//if there no file extension, return false
				return false;
			}//end if
		}//end function	
				
		public function get_file_ext($filename){
			$lastdot = strrpos($filename,".");			
			if($lastdot != false){
				$file_ext = substr($filename,$lastdot,strlen($filename));				
				return $file_ext;
			} else {
				return false;
			}//end if
		}//end function
		
		public function set_modules_labels(){
			$modules = $this->get_modules_list();
			for($i = 0; $i < sizeof($modules); $i++){	
				if($modules[$i]['module_name'] != 'generic'){
					$query = "UPDATE ".$this->t."modules 
							  SET module_label = '".$this->strtodb($_POST["module_".$modules[$i]['module_id']])."'
							  WHERE module_id = ".$modules[$i]['module_id'];
					$result = @mysql_query($query,$this->db);
					$this->error($result,__LINE__,get_class($this));	
				}//end if
			}//end for i
			return true;
		}//end function
		
		public function get_message($message,$type = "good"){
			switch($type){
				case "good":
				case "ok":
				default:
					$str = __GOOD_DIV_BEGIN__;
					break;
				case "error":
					$str = __BAD_DIV_BEGIN__;
					break;
				case "alert":
					$str = __ALERT_DIV_BEGIN__;
					break;
			}//end switch
			if(defined($message)){
				$str .= constant($message);
			} else {
				$str .= $message;
			}//end if
			$str .= __DIV_END__;
			return $str;
		}//end function
		
		function print_message($message,$type = "good"){
			echo $this->get_message($message,$type);
		}//end function
		
		public function filter_by_uid($uid){
			$this->uid = $uid;
			$this->filters['user_id'] = $uid;
		}//end function
		
		public function module_has_categories($module){
			if($module == NULL){
				return false;
			}//end if
			if(!is_numeric($module)){
				$module = $this->get_module($module);
				$module = $module['module_id'];
			}//end if
			if($module != NULL){
				$query = "SELECT COUNT(*) AS categories_found FROM ".$this->t."categories WHERE category_module_id = ".$module;
				
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));	
				$row = @mysql_fetch_array($result);
				if($row['categories_found'] == "0"){
					return false;
				} else {
					return true;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function module_has_uncategorized_posts($module){
			$module = $this->get_module($module);			
			$module_name = $module['module_name'];		
			$query = "SELECT COUNT(*) AS found
					  FROM ".$this->t."posts
					  LEFT JOIN ".$this->t."categories_rel 
					  ON post_id = categories_rel_post_id
					  WHERE categories_rel_id IS NULL
				      AND post_module_id = '".$module_name."'";
					 
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$row = @mysql_fetch_array($result);
			$found = $row['found'];
			if($found > 0){
				return $found;
			} else {
				return false;
			}//end if	
		}//end function	
		
		public function get_items($module,$status = NULL,$limit = NULL){		
			$module_data = $this->get_module($module);
			$module_id = $module_data['module_id'];
			$module_name = $module_data['module_name'];
			$query = "(SELECT post_id as id, post_title as label, user_username, module_item_order_order as item_order, post_status as status
					  FROM ".$this->t."posts
					  LEFT JOIN ".$this->t."users
					  ON (".$this->t."users.user_id = ".$this->t."posts.post_uid) 
					  LEFT JOIN ".$this->t."categories_rel 
					  ON post_id = categories_rel_post_id
					  LEFT JOIN ".$this->t."modules_items_order
					  ON (module_item_order_module_id = ".$module_id."
					  AND module_item_order_type = 'post'
					  AND module_item_order_item_id = post_id)
					  WHERE (categories_rel_id IS NULL OR module_item_order_order IS NOT NULL)
					  AND (module_item_order_father_id = 0 OR module_item_order_father_id IS NULL)
					  AND post_temp = 0
				      AND post_module_id = '".$module_name."'";
			if($status != NULL){
				if($status < 2){
					$query .= " AND post_status = ".$status;
				} else {
					$query .= " AND (post_status = 1 OR post_status = 2)";
				}//end if
			}//end if
			$query .= ") ";
			$query .= " UNION
					   (SELECT category_id as id, category_name as label, category_father_id, module_item_order_order as item_order, category_status as status
					   FROM ".$this->t."categories
					   LEFT JOIN ".$this->t."modules_items_order
					   ON (module_item_order_module_id = category_module_id
					   AND module_item_order_type = 'category'
					   AND module_item_order_item_id = category_id)
					   WHERE category_module_id = '".$module_id."'
					   AND category_father_id = 0
					   AND (module_item_order_father_id = 0 OR module_item_order_father_id IS NULL)";
			if($status != NULL){
				if($status < 2){
					$query .= " AND category_status = ".$status;
				} else {
					$query .= " AND (category_status = 1 OR category_status = 2)";
				}//end if
			}//end if
			$query .= ") ORDER BY item_order ASC";
			if($limit != NULL){
				$query .= " LIMIT ".$limit;
			}//end if
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
		
			if($num > 0){							
				$data = $this->get_result_array($result,false);				
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		//explore, show the files inside the passed folder
		public function explore($path = NULL,$script_path = NULL){
			$dir = opendir($_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.$path);
			$data = array();
			$data['dirs'] = array();
			$data['files'] = array();
			if($script_path != NULL){
				$path = $script_path.$path;
			}//end  if
			while(($file = readdir($dir)) !== false){
			//echo "<br />..".$path.$file."<br />";
				if(is_dir($path.$file)){
					if($file != ".." && $file != "." && !in_array($file,$this->protected_dirs)){
						array_push($data['dirs'],$file);						
					}//endif				
				} else {
					if($file != "Thumbs.db" && substr($file,0,1) != "."){
						array_push($data['files'],$file);
					}//endif
				}//endf				
			}//endwhile
			return $data;
		}//endfunction
		
		//public function create_paging_list_google($range = NULL,$skip_ul_tag = false,$ul_class = NULL,$li_selected_class = NULL,$prefix = NULL){
		public function create_paging_list_google(){
			$pages = ceil($this->p_max/$this->p_limit);		
			
			$params = func_get_args();
			if(!is_array($params[0])){
				$range = $params[0];				
				$skip_ul_tag = $params[1];				
				$ul_class = $params[2];				
				$li_selected_class = $params[3];
				$prefix = $params[4];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			/*if($pages == 0){
				$pages = 1;
			}//end if*/
		
			$this_page = $_SERVER['PHP_SELF'];
			$this_page = basename($this_page);
			
			//Catch all the GET data and put in the string url
			$url_vars = $this->get_url_vars();
			
			if($ul_class != NULL){
				$ul_class = ' class="'.$ul_class.'"';
			}
			if($skip_ul_tag == false){
				$html = "<ul".$ul_class.">\n";
			} else {
				$html = "";
			}//end if			
			if($range != NULL && is_numeric($range)){
				$just_beginned = false;
				$end_reached = false;
				$lower_limit = $this->p_current;				
				$lower_limit -= ceil($range/2);
				//In case the limit is under 0, like in case the selected page is 1, 
				if($lower_limit < 0){
					$just_beginned = true;
					$lower_limit += abs($lower_limit);				
				}//end if
				//if($this->p_current > ceil($range/2)){
					$lower_limit = 0;
					$lower_limit = ($this->p_current+1) - $range;
					$upper_limit = ($this->p_current-1) + $range;			
				//} else {
				//	$upper_limit = $lower_limit+$range;				
				//}//end if
				
				if(($this->p_current + ceil($range/2)) >= $pages){
					$end_reached = true;
					//$lower_limit -= ($this->p_current + ceil($range/2)) - ($pages+1);
				}//end if		
				/*if($just_beginned == false){
					//Create the link
					$page_url = "?page=1";
					$html .= "<li><a href=\"".$this_page.$page_url.$url_vars."\">";			
					$html .= "<span>begin</span>";				
					$html .= "</a></li>\n";
				}//end if	*/			
			}//end if			
			for($i = 1; $i <= $pages; $i++){
				if($range == NULL || ($range != NULL && ($i >= $lower_limit && $i <= ($upper_limit)))){
					//Create the link
					$page_url = "?page=".$i;
					$html .= "<li";
					if($li_selected_class != NULL && $i == $this->p_current){
						$html .= ' class="'.$li_selected_class.'"';
					}//end if
					if($prefix != NULL){
						$html .= "><a href=\"".__BASEURL__.$prefix."page".$i."\">";
					} else {
						$html .= "><a href=\"".__BASEURL__.$this_page.$page_url.$url_vars."\">";
					}
					
					if($i == $this->p_current){
						$html .= "<strong>".$i."</strong>";
					} else {
						$html .= "<span>".$i."</span>";
					}//end if
					$html .= "</a></li>\n";
				}//end if
			}//end for
			/*if($range != NULL && is_numeric($range)){
				if($end_reached == false){
					//Create the link
					$page_url = "?page=".$pages;
					$html .= "<li><a href=\"".$this_page.$page_url.$url_vars."\">";			
					$html .= "<span>end</span>";				
					$html .= "</a></li>\n";
				}//end if
			}//end if*/
			if($skip_ul_tag == false){
				$html .= "</ul>\n";
			}//end if
			return $html;
		}//end function
		
		#This function trunk text !- TO IMPROVE
		public function trunktexttoword($txt,$limit = 50){
			$txt = strip_tags($txt);
			if(strlen($txt) > $limit) {  
				$txt = substr($txt, 0, $limit);
				if(strpos($txt, ' ') !== 0 && strpos($txt, ' ') !== FALSE) { 
					if(substr($txt, $limit, 1) != ' ') {  
						$txt = substr($txt, 0, strrpos($limit, ' '));  
       		 		}  
    			}
    			$txt = $txt . ' ...';  
			}  
			return($txt);
		}//end function
		
		#This function trunk text !- TO IMPROVE
		public function trunktextnostrip($txt,$limit = 50){
			if(strlen($txt) > $limit){
				$txt = substr($txt,0,$limit);
				$txt .= "...";
			}//endif
			return($txt);
		}//end function
		
		public function get_breaks($text,$nbr){			
			$textArr = explode("<br />",$text);
			if(sizeof($textArr) > 1){
				$newTextArr = array();
				for($i = 0; $i < sizeof($textArr); $i++){
					if($i < $nbr){
						array_push($newTextArr,$textArr[$i]);
					}//end if
				}//end for
				$newTextArr = implode("<br />",$newTextArr);
				return $newTextArr;
			} else {
				return $text;
			}//end if
		}//end function
		
		public function get_paragraphs($text,$np,$nbr = NULL){
			$textArr = explode("<p>",$text);
			if(sizeof($textArr) > 1){
				$newTextArr = array();
				for($i = 0; $i < sizeof($textArr); $i++){
					if($i < $nbr){
						array_push($newTextArr,$textArr[$i]);
					}//end if
				}//end for
				$newTextArr = implode("<p>",$newTextArr);
				return $newTextArr;
			} else {
				if($nbr == NULL){
					$nbr = $np;
				}//end if
				return $this->get_breaks($text,$nbr);
			}//end if
		}//end function
		
		public function compress_text($text, $size = 30){
			if(strlen($text) > $size){
				$sides = round($size/2);
				$sides -= 2;
				$text = substr($text,0,$sides)."...".substr($text,-($sides));
			}//end if
			return $text;
		}//end function
		
		//Empty the error log
		public function empty_err_log(){
			$query = "TRUNCATE ".$this->t."errors_log";
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));	
		}//end function
		
		public function is_err_log_empty(){
			$query = "SELECT COUNT(*) AS founds FROM ".$this->t."errors_log";			
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));	
			$row = @mysql_fetch_array($result);
			if($row['founds'] == 0){
				return true;
			} else {
				return $row['founds'];
			}//end if
		}//end function
		
		public function analyze_logs(){
			$query = "SELECT SUM";
		}//end function
		
		//return the errors log
		public function get_log($lines = NULL,$filter = NULL){
			$minimum_level = 10;
			if($filter == NULL){
				$query = "SELECT *
						  FROM ".$this->t."errors_log 
						  ORDER BY error_log_date DESC";
			} else {
				$query = "SELECT *, COUNT(*) AS found
						  FROM ".$this->t."errors_log						  
						  GROUP BY error_log_error, error_log_error_page,error_log_line
						  HAVING found > ".$minimum_level."
						  ORDER BY found DESC";
			
			}//end if
			if($lines != NULL){
				$query .= " LIMIT ".$lines;
			}//endif
			if($this->paging_isset() && $lines == NULL){
				$this->set_unpaged_query($query);
				$query .= " LIMIT ".$this->p_start.", ".$this->p_limit;
			}//end if		
				
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));	
			$data = $this->get_result_array($result,false);
			return $data;
		}//end function
		
		public function set_attachment_screenshot($screenshot_filename,$aID){
			$query = "UPDATE ".$this->t."attachments SET attachment_screenshot = '".$screenshot_filename."' WHERE attachment_id = ".$aID;
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			if($screenshot_filename != NULL){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function reset_post_counters($module = NULL){
			if($module != NULL){
				$module_data = $this->get_module($module);
				$module_name = $module_data['module_name'];
			}//end if
			
			$query = "UPDATE ".$this->t."posts SET post_reads = 0";
			if($module != NULL){
				$query .= "WHERE post_module_id = '".$module_name."'";
			}//end if
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
		}//end function
	
		//Merge an additional field to the related main table		
		public function merge_additional_field($id){
			$data = array();
			//First read the information of this addfield
			$info = $this->get_additional_field($id);
			//Read the module info
			$module = $this->get_module($info['addfield_item_module_id']);
			$module_action = $info['addfield_item_module_action'];			
			$main_table = $module['module_table'];
			if(substr($main_table,-1) == "s"){ 
				$field = substr($main_table,0,-1);
			} else {
				$field = $main_table;
			}//end if
			//Check if we are looking for the main table of for a group/category
			switch($module_action){
				case "newcategory":
					$main_table .= "_categories";
					$field .= "_category";
					break;
				case "newgroup":
					$main_table .= "_groups";
					$field .= "_group";
					break;
			}//end switch
			//Build the new field name
			$table_prefix = $field;
			$field .= "_".$info['addfield_item_label'];		
			$data['table'] = $main_table;
			$data['field_name'] = $field;	
			//Create the new field
			$query = "ALTER TABLE `".$this->t.$main_table."` ADD `".$field."` TEXT NULL";
			$result = @mysql_query($query,$this->db);
			if($this->error($result,__LINE__,get_class($this))){
				$data['error'] = "Unable to create the field";
				//return $data;
			}//end if
			//Generate the inserts
			$query = "SELECT * FROM ".$this->t."addfield_data
					  WHERE addfield_data_ref_id = ".$id;
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			$merged = 0;
			while($row = @mysql_fetch_array($result)){
				if($row['addfield_data_int_value'] != NULL){
					$field_value = $row['addfield_data_int_value'];
				} else {
					$field_value = "'".addslashes($row['addfield_data_value'])."'";
				}//end if
				$query_ins = "UPDATE ".$this->t.$main_table." SET ".$field." = ".$field_value." WHERE ".$table_prefix."_id = ".$row['addfield_data_ref_item'];				
				$result_ins = @mysql_query($query_ins,$this->db);
				if($this->error($result,__LINE__,get_class($this)) == false){				
					$merged++;
				}//end if
			}//end while
			
			//Collect the informations
			
			$data['total_rows'] = $num;
			$data['merged_rows'] = $merged;
			//Check if it was merged all the records
			if($num == $merged){
				//Cleaning up
				//Now delete the additional field data
				$query = "DELETE FROM ".$this->t."addfield_data WHERE addfield_data_ref_id = ".$id;
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				//Delete the additional field record
				$query = "DELETE FROM ".$this->t."addfield_items WHERE addfield_item_id = ".$id;
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
			}//end if
			
			return $data;			
		}//end function
	
		//check if a snippet is linked to a post
		public function is_snippet_in_post($snippetID,$postID){
			$query = "SELECT * FROM ".$this->t."snippets_posts_rel
					WHERE snippet_post_rel_sid = '".$snippetID."'
					AND snippet_post_rel_post_id = '".$postID."'";
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		
		//Create a single relation between one snippet and one post if snippetID is a string or numeric, in case of array it 
		//create a multiple relations
		public function create_snippet_post_rel($snippetID,$postID){
			if(is_array($snippetID)){
				//Delete the previous relations
				$query = "DELETE FROM ".$this->t."snippets_posts_rel WHERE snippet_post_rel_post_id = ".$postID;
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				for($i = 0; $i < sizeof($snippetID); $i++){
					$query = "INSERT INTO ".$this->t."snippets_posts_rel
							  (snippet_post_rel_sid, snippet_post_rel_post_id, snippet_post_rel_order)
							  VALUES
							  (".$snippetID[$i].",".$postID.",".$i.")";
					$result = @mysql_query($query,$this->db);
					$this->error($result,__LINE__,get_class($this));
				}//end for
			} else {
				if(!$this->is_snippet_in_post($snippetID,$postID)){				
					$query = "INSERT INTO ".$this->t."snippets_posts_rel
							  (snippet_post_rel_sid, snippet_post_rel_post_id, snippet_post_rel_order)
							  VALUES
							  (".$snippetID.",".$postID.",0)";
				}//end if
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
			}//end if
		}//end function	
		
		/* OLD VERSION */
		//This function save the attachment data in the database
		public function store_attachment(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$upload_class = $params[0];				
				$id = $params[1];			
				$module_id = $params[2];
				if(isset($params[3]) && $params[3] != NULL){
					$extra_data = $params[3];
				}//end if
			}//end if			
			
			//Threat the data as a normal file
			if(is_object($upload_class)){
				$filename = $upload_class->get_filename();
				$mime =  $upload_class->get_mime();
				$title = trim($_REQUEST['file_title']);
				$caption = trim($_REQUEST['caption']);
				$fullpath = $upload_class->get_destination();
				$is_img = $this->is_img($mime);
				$is_file = 1;
				$extra_data = NULL;
				if($is_img){
					$is_img = "1";
					if($this->have_images($id,$module_id)){
						$default_img = "0";
					} else {
						$default_img = "1";
					}//end if
				} else {
					$is_img = "0";
					$default_img = "0";
				}//end if
				//Check if we have to store the exif data
				if(__STORE_EXIF_DATA__){
					//if it's a jpeg file, take the exif
					if($mime == "image/jpeg" || $mime == "image/pjpeg"){
						$exif = $this->get_exif_data($fullpath.$filename);
						$exif = base64_encode(serialize($exif));
					} else {
						$exif = NULL;
					}//end if
				}//end if
			} else {
				//Threat the data as a script allocation or file reference
				if(is_string($upload_class) && sizeof($upload_class) > 0){
					$filename = $upload_class;
					$mime = "file_reference";
				} else {
					$filename = "nofile";
					$mime = "script";
				}//end if
				$title = '';
				$exif = NULL;
				$is_img = "0";
				$default_img = "0";
				$is_file = "0";
			}//end if
			//Look how many files has been uploaded before
			$order = $this->get_num_attachments($module_id,$id,$is_img);
			
			$query = "INSERT INTO ".$this->t."attachments
					  (attachment_title,attachment_filename,attachment_mime,attachment_exif,attachment_ref_id,attachment_ref_module_id,
					  attachment_order,attachment_is_img,attachment_default, attachment_is_file, attachment_data, attachment_caption)
					  VALUES
					  ('".$title."','".$filename."','".$mime."','".$exif."',".$id.",".$module_id.",".$order.",".$is_img.",".$default_img.",".$is_file.",'".$extra_data."','".$caption."')";	
					  echo $query;				
			$result = @mysql_query($query,$this->db);			
			$this->error($result,__LINE__,get_class($this));
			$f_id = @mysql_insert_id();
			return $f_id;
		}//end function
		
		public function create_from_existent_attachment($fid,$pid){
			//First, catch all the information from the original attachment
			$fdata = $this->get_attachment($fid);
			$module = $this->get_module($fdata['attachment_ref_module_id']);
			$module_name = $module['module_name'];		
			$module_id = $module['module_id'];		
			$title = "homepage";
			$filename = $fdata['attachment_filename'];
			$new_filename = time()."_".substr($filename,11);
			//Check if it already exists another image called "homepage"
			$previous_files = $this->get_attachments_by_title('homepage',$module_id,$pid,"1","1");
			if($previous_files != false){
				$this->delete_attachment($module_name,$previous_files[0]['attachment_id']);
			}//end if
			//Copy the file
			//if(copy("../upload/".$module_name."/photos/o/".$filename,"../upload/".$module_name."/photos/o/".$new_filename)){
			if(copy(__UPLOADPATH__."upload/".$module_name."/photos/o/".$filename,__UPLOADPATH__."upload/".$module_name."/photos/o/".$new_filename)){			
				//Copy the sql data
				$hidden_fields = array("attachment_id");
				$fdata["attachment_filename"] = $new_filename;
				$fdata["attachment_default"] = "0";
				$sql = $this->build_sql_fields("","INSERT",$fdata,$hidden_fields);				
				$query = "INSERT INTO ".$this->t."attachments ".$sql;
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				return $this->fid = @mysql_insert_id();
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_attachment($attachmentID){
			$query = "SELECT * FROM ".$this->t."attachments WHERE attachment_id = ".$attachmentID;				
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){		
				$data = $this->get_result_array($result,true);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
				
		private function have_images($id,$module_id){
			$query = "SELECT * FROM ".$this->t."attachments 
					  WHERE attachment_ref_id = ".$id."
					  AND attachment_ref_module_id = ".$module_id."
					  AND attachment_mime IN ('image/jpeg','image/gif','image/png','image/pjpeg')";
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		//this is almost an alias of get_attachments, this is just for retrocompatibility
		public function get_attachments_by_title(){
			$params = func_get_args();
			if(is_array($params[0])){				
				$settings = $params[0];
			} else {
				$src_title = $params[0];
				$module_id = $params[1];
				$ref_id = $params[2];
				$is_img = $params[3];
				$is_file = $params[4];	
				$settings = array("search" => $src_title, "module_id" => $module_id, "post_id" => $ref_id, "is_img" => $is_img, "is_file" => $is_file);	
			}//end if	
			
			return $this->get_attachments($settings);
		}//end function
		
		//return a list with attachments
		//If the third paramether is equal to 1, will show only the images, if it's 0 will show the rest, if null will show both
		public function get_attachments(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {			
				$module_id = $params[0];
				$ref_id = $params[1];
				$type = $params[2];	
				$exclude_def_img = $params[4];
				$show_hidden = $params[5];
			}//end if	
			
			if(isset($module) && !isset($module_id)){
				$module_id = $module;	
			}//end if
			
			if(!is_numeric($module_id) && $module_id != NULL){
				$module_id = $this->get_module($module_id);
				$module_id = $module_id['module_id'];
			}//end if
			
			
			if(!isset($show_hidden)){
				$show_hidden = true;	
			}//end if
			
			if(isset($post_id) && is_numeric($post_id)){
				$ref_id = $post_id;
			}//end if
			
			if(!isset($type)){
				//fallback to previous system
				if(!is_null($is_img)){
					$type = "file";
					if((bool)$is_img){
						$type = "image";
					}//end if			
				} else {
					$type = NULL;	
				}//end if
			}//end if			
				
			$query = "SELECT * FROM ".$this->t."attachments, ".$this->t."modules";
			if(isset($module_id) && !is_null($module_id)){
				$query .= " WHERE attachment_ref_module_id = ".$module_id."
							AND module_id = attachment_ref_module_id";
			} else {
				$query .= " WHERE module_id = attachment_ref_module_id";
			}//end if
			
			if(isset($search) && !is_null($search)){
				if(is_array($search)){
					$search_keys = implode("','",$search);
					$query .= " AND attachment_title IN ('".$search_keys."')";	
				} else if(is_string($search)){
					$query .= " AND attachment_title = '".$search."'";	
				}//end if
			}//end if
			
			if($show_hidden == false){
				$query .= " AND attachment_hidden = 0";
			}//end if
			
			//Check for file protection
			if($this->file_protection){
				$users = $this->call('users');
				$userID = $users->get_uid();
				if(!is_numeric($userID) || is_null($userID)){
					$query .= " AND attachment_protected = 0";
				}//end if
			}//end if
			if($exclude_def_img == true && $is_img){
				$query .= " AND attachment_default = 0";
			}//end if
			if($ref_id != NULL){
				$query .= " AND attachment_ref_id = ".$ref_id;
			}//end if
			if(!is_null($type)){
				$query .= " AND attachment_type = '".$type."'";
			}//end if	
			if($module_id == NULL){
				$query .= " GROUP BY attachment_filename";
			}//end if
			$query .= " ORDER BY attachment_date ASC";
			
			if(isset($limit) && is_numeric($limit)){
				$query .= " LIMIT ".$limit;
			}//end if
		
			$result = @mysql_query($query,$this->db);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = @mysql_num_rows($result);
				if($num > 0){	
					if(isset($limit) && $limit == 1){	
						$data = $this->get_result_array($result,true);
					} else {
						$data = $this->get_result_array($result,false);
					}
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_attachments_by_filetype($src_type, $module_id = NULL,$ref_id = NULL,$is_img = NULL, $is_file = NULL){
			if(!is_numeric($module_id) && $module_id != NULL){
				$module_id = $this->get_module($module_id);
				$module_id = $module_id['module_id'];
			}//end if
			if(is_bool($is_img)){
				if($is_img == true){
					$is_img = "1";
				} else {
					$is_img = "0";
				}//end if
			}//end if
			
			$src_type = str_replace("*","%",$src_type);
			$query = "SELECT * FROM ".$this->t."attachments, ".$this->t."modules";
			if($module_id != NULL){
				$query .= " WHERE attachment_ref_module_id = ".$module_id."
							AND module_id = attachment_ref_module_id";
			} else {
				$query .= " WHERE module_id = attachment_ref_module_id";
			}//end if
			$query .= " AND attachment_filename LIKE '".$src_type."'";
			if($is_file != NULL){
				$query .= " AND attachment_is_file = ".$is_file;
			}//end if			
			if($module_id != NULL && $ref_id != NULL){
				$query .= " AND attachment_ref_id = ".$ref_id;
			}//end if
			if($module_id != NULL && $ref_id != NULL && $is_img != NULL){
				$query .= " AND attachment_is_img = ".$is_img;
			}//end if	
			if($module_id == NULL){
				$query .= " GROUP BY attachment_filename";
			}//end if
			$query .= " ORDER BY attachment_order ASC";

			//This is for the paging			
			if($this->paging_isset()){
				$this->set_unpaged_query($query);
				$query .= " LIMIT ".$this->p_start.", ".$this->p_limit;
			}//end if	
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){		
				$data = $this->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		//simply return the numbers of attachments		
		//If the third paramether is equal to 1, will show only the images, if it's 0 will show the rest, if null will show both
		public function get_num_attachments($module_id = NULL,$ref_id = NULL,$is_img = NULL){
			$query = "SELECT count(*) AS num FROM ".$this->t."attachments";
			if($module_id != NULL){
				$query .= " WHERE attachment_ref_module_id = ".$module_id;
			}//end if
			if($module_id != NULL && $ref_id != NULL){
				$query .= " AND attachment_ref_id = ".$ref_id;
			}//end if
			if($module_id != NULL && $ref_id != NULL && $is_img != NULL){
				$query .= " AND attachment_is_img = ".$is_img;
			}//end if	
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$row = @mysql_fetch_array($result);
			return $row['num'];
		}//end function
		
		//Return all the attachments from a user
		public function get_user_attachments($uid, $module_id = NULL, $excluded_posts = NULL){	
			if(!is_numeric($module_id) && $module_id != NULL){
				$module_id = $this->get_module($module_id);
				$module_id = $module_id['module_id'];
			}//end if	
			//Check if there are some post id to take out from the result
			if($excluded_posts != NULL){
				//If is an array, turn to a string
				if(is_array($excluded_posts)){
					$excluded_posts = implode(",",$excluded_posts);
				}//end if
			}//end if
			$query = "SELECT ".$this->t."attachments.* 
					  FROM (".$this->t."attachments, ".$this->t."posts, ".$this->t."modules)
					  WHERE attachment_ref_id = post_id
					  AND post_module_id = module_name
					  AND module_id = attachment_ref_module_id
					  AND post_uid = ".$uid;
			if($module_id != NULL){
				$query .= " AND module_id = ".$module_id;
			}//end if
			if($excluded_posts != NULL){
				$query .= " AND post_id NOT IN (".$excluded_posts.")";
			}//end if
			$query .= " GROUP BY attachment_filename 
						ORDER BY attachment_filename ASC";
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){		
				$data = $this->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		//Sort the attachment sort
		public function save_attachment_sorting($module_id,$id,$attachments_ids){
			if(!is_numeric($module_id) && $module_id != NULL){
				$module_id = $this->get_module($module_id);
				$module_id = $module_id['module_id'];
			}//end if			
			$fid = $attachments_ids;			
			if(is_array($fid) && sizeof($fid) > 0){
				for($i = 0; $i < sizeof($fid); $i++){
					$query = "UPDATE ".$this->t."attachments 
							  SET attachment_order = ".$i."
							  WHERE attachment_ref_id = ".$id."
							  AND attachment_ref_module_id = ".$module_id." 
							  AND attachment_id = ".$fid[$i];						
					$result = @mysql_query($query,$this->db);
					$this->error($result,__LINE__,get_class($this));	
				}//end for
				return true;
			} else {
				return false;
			}//end if		
		}//end function
		
		private function empty_directory($path){
			$full_path = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.$path;
			$dir = opendir($full_path);						
			//$data['files'] = array();		
			if(file_exists("../".$path)){
				while(($file = readdir($dir)) !== false){										
					if(is_dir($full_path.$file) == false){								
						if($file != "Thumbs.db" && substr($file,0,1) != "."){
							//array_push($data['files'],$file);
							unlink($full_path.$file);
						}//endif
					} else if(is_dir($path.$file)){
						rmdir($full_path.$file);
						echo "dir removed";
					}//endf				
				}//endwhile
				return true;
			} else {
				echo "doesn't exists";
				return false;
			}//end if
		}//end function
		
		public function clone_attachment($fid,$newid){
			if(is_array($newid)){
				for($i = 0; $i < sizeof($newid); $i++){
					$o = $this->get_attachment($fid);//original
					$o["attachment_ref_id"] = $newid[$i];
					$hidden_fields = array("attachment_id");
					$sql = $this->build_sql_fields("","INSERT",$o,$hidden_fields);				
					$query = "INSERT INTO ".$this->t."attachments ".$sql;
					$result = @mysql_query($query,$this->db);
					$this->error($result,__LINE__,get_class($this));					
				}//end for
			} else {
				$o = $this->get_attachment($fid);//original
				$o["attachment_ref_id"] = $newid;
				$hidden_fields = array("attachment_id");
				$sql = $this->build_sql_fields("","INSERT",$o,$hidden_fields);				
				$query = "INSERT INTO ".$this->t."attachments ".$sql;
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				$this->fid = @mysql_insert_id();
			}//end if	
		}//end function
		
		public function clone_attachments($farray,$newid){
			if(is_array($farray) && sizeof($farray) > 0){
				for($i = 0; $i < sizeof($farray); $i++){
					$this->clone_attachment($farray[$i],$newid);
				}//end for
			}//end for
		}//end function
		
		public function move_attachments($module,$farray,$newid){
			//First clone the attachments to the new id(s)
			$this->clone_attachments($farray,$newid);
			//And then delete the attachment from this id
			$this->delete_attachment($module,$farray);
		}//end function
		
		//This function define the image to be a default img
		public function set_default_image($fid,$id,$module_id){
			//Reset previous default, if exists
			$query = "UPDATE ".$this->t."attachments SET attachment_default = 0 
					  WHERE attachment_ref_id = ".$id."
					  AND attachment_ref_module_id = ".$module_id;					
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			//now set the new default image
			$query = "UPDATE ".$this->t."attachments SET attachment_default = 1 
					  WHERE attachment_ref_id = ".$id."
					  AND attachment_ref_module_id = ".$module_id."
					  AND attachment_id = ".$fid;								
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
		}//end function
		
		public function set_hidden_attachment($fid,$id,$module_id){
			//Check the old status
			$data = $this->get_attachment($fid);
			$hidden = $data['attachment_hidden'];
			if($hidden == 0){
				$new_hidden = 1;
			} else {
				$new_hidden = 0;
			}//end if
			//Set the new status
			$query = "UPDATE ".$this->t."attachments SET attachment_hidden = ".$new_hidden." 
					  WHERE attachment_ref_id = ".$id."
					  AND attachment_ref_module_id = ".$module_id."
					  AND attachment_id = ".$fid;				
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			//return the new status
			return $new_hidden;
		}//end function
		
		public function set_protected_attachment($fid,$id,$module_id){
			//Check the old status
			$data = $this->get_attachment($fid);
			$hidden = $data['attachment_protected'];
			if($hidden == 0){
				$new_status = 1;
			} else {
				$new_status = 0;
			}//end if
			//Set the new status
			$query = "UPDATE ".$this->t."attachments SET attachment_protected = ".$new_status." 
					  WHERE attachment_ref_id = ".$id."
					  AND attachment_ref_module_id = ".$module_id."
					  AND attachment_id = ".$fid;				
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			//return the new status
			return $new_status;
		}//end function
		
		public function get_default_image($id,$module = NULL){
			if($id == NULL){
				return false;	
			}//end if
			$module_id = $this->get_module($module);
			$module_id = $module_id['module_id'];
			$query = "SELECT * FROM ".$this->t."attachments 
					  WHERE attachment_default = 1 
					  AND attachment_mime IN ('image/jpeg','image/gif','image/png','image/pjpeg')
					  AND attachment_ref_id = ".$id;
			if($module != false){
				$query .= " AND attachment_ref_module_id = ".$module_id;
			}//end if	
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){			
				$data = $this->get_result_array($result,true);
				return $data;
			} else {
				//If there is any default image, try to take the first image
				$query = "SELECT * 
						  FROM ".$this->t."attachments 
						  WHERE attachment_mime IN ('image/jpeg','image/gif','image/png','image/pjpeg') 
						  AND attachment_ref_id = ".$id;
				if($module != false){
					$query .= " AND attachment_ref_module_id = ".$module_id;
				}//end if
				$query .= " LIMIT 1";
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
				$num = @mysql_num_rows($result);
				if($num > 0){			
					$data = $this->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			}//end if
		}//end function
				
		public function set_attachment_title($title,$aID){
			$query = "UPDATE ".$this->t."attachments SET attachment_title = '".$title."' WHERE attachment_id = ".$aID;
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			if($title != NULL){
				return true;
			} else {
				return false;
			}//end if
		}//end if
		
		public function set_attachment_caption($caption,$aID){
			$query = "UPDATE ".$this->t."attachments SET attachment_caption = '".$caption."' WHERE attachment_id = ".$aID;
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			if($title != NULL){
				return true;
			} else {
				return false;
			}//end if
		}//end if
		
		public function enable_file_protection($file_protection = false){
			$this->file_protection = $file_protection;
		}//end function
		
		public function replace_attachment($upClass,$fid,$uploadpath = __UPLOADPATH__){
			//First read the information
			$data = $this->get_attachment($fid);
			//now read the information from upload class
			$mime = $upClass->get_mime();
			$filename = $upClass->get_filename();
			//Delete the old file
			$module_id = $data['attachment_ref_module_id'];
			$module = $this->get_module($module_id);
			$folder = $module['module_name'];
			$isImg = $data['attachment_is_img'];
			if($isImg == "1"){
				@unlink($uploadpath."upload/".$folder."/photos/o/".$data['attachment_filename']);
				$this->delete_related_thumbs($fid,$uploadpath);
			} else {
				@unlink($uploadpath."upload/".$folder."/files/".$data['attachment_filename']);
		
			}//end if
			//Update the filename on the db
			$query = "UPDATE ".$this->t."attachments 
					  SET attachment_filename = '".$filename."',
					  attachment_mime = '".$mime."'
					  WHERE attachment_id = ".$fid;
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			return true;
		}//end function
		
		public function delete_related_thumbs($fid,$uploadpath = __UPLOADPATH__){
			$data = $this->get_attachment($fid);
			$module_id = $data['attachment_ref_module_id'];
			$module = $this->get_module($module_id);
			$folder = $module['module_name'];
			$image_formats = explode(",",__IMAGE_FORMATS_LIST__);
			for($i = 0; $i < sizeof($image_formats); $i++){
				if(file_exists($uploadpath."upload/".$folder."/photos/".$image_formats[$i]."/".$data['attachment_filename'])){
					@unlink($uploadpath."upload/".$folder."/photos/".$image_formats[$i]."/".$data['attachment_filename']);
				}//end if
			}//end for i
			return true;
		}//end function
		
		public function check_default_image_exists($pid,$module = NULL){
			if(!is_numeric($module)){
				$module_data = $this->get_module($module);
				$module = $module_data['module_id'];
			}//end if
			$query = "SELECT * 
			          FROM ".$this->t."attachments
					  WHERE attachment_default = 1
					  AND attachment_is_img = 1
					  AND attachment_ref_id = ".$pid;
			if($module != false){
				$query .= " AND attachment_ref_module_id = ".$module;
			}//end if		
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);			
			if($num == 0){
				//If I cannot find a default image, get all the other image
				$files = $this->get_attachments($module,$pid,1,1);
				//If there are other images, take the first one and mark as default
				if($files != false){
					$update = "UPDATE ".$this->t."attachments SET attachment_default = 1 WHERE attachment_id = ".$files[0]['attachment_id'];
					$result = @mysql_query($update,$this->db);
					$this->error($result,__LINE__,get_class($this));
				}//end if
				return true;
			} else {
				return true;
			}//end if
		}//end function
			
		public function check_download_permission($filename, $force_protection = false, $strict_users_group_id = NULL, $module = NULL, $single_user = NULL){
			$query = "SELECT * FROM ".$this->t."attachments WHERE attachment_filename = '".$filename."'";
			if($module != NULL){
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];
				$query .= " AND attachment_ref_module_id = ".$module_id;
			}//end if
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num == 1){
				$row = @mysql_fetch_array($result);
				//check if the file is protected or not
				if($row['attachment_protected'] == 1 || $force_protection == true){			
					$users = $this->call("users");
					$userID = $users->get_uid();	
					//check if is a system user				
					if($userID != false){					
						$pass_user = true;
						$userData = $users->get_user($userID);
						//check if only the owner can download this file
						if($single_user != NULL && is_numeric($single_user)){	
							if($single_user == $userID){	
								$pass_user = true;
							} else {	
								$pass_user = false;
							}//end if
						}//end if
						//check if is restricted to a group
						if($strict_users_group_id != NULL){
							if($userData['user_group_id'] == $strict_users_group_id){
								$pass_user = true;
							} else {
								$pass_user = false;
							}//end if	
						}//end if	
						return $pass_user;
					} else {
						return false;
					}//end if
				} else {
					return true;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		//this function return the exif data of a jpeg
		public function get_exif_data($Img){
			$this->Img = $Img;
			$exif = exif_read_data($this->Img, 0, true);
			$exif_data = array();
			
			foreach ($exif as $key => $section) {
			   foreach ($section as $name => $val) {
				  // echo "$key.$name: $val<br />\n";
				   switch($name){
				   		case "FileDateTime":
							$exif_data['filedataunix'] = $val;
						break;
						case "FileSize":
							$exif_data['filesize'] = $val;
						break;
						case "MimeType":
							$exif_data['mime'] = $val;
						break;
						case "Height":
							$exif_data['height'] = $val;
						break;
						case "Width":
							$exif_data['width'] = $val;
						break;
						case "ApertureFNumber":
							$exif_data['aperture'] = $val;
						break;
						case "Model":
							$exif_data['camera'] = $val;
						break;
						case "Artist":
							$exif_data['artist'] = $val;
						break;
						case "ExposureTime":
							$exif_data['exposure_time'] = $val;
						break;
						case "ISOSpeedRatings":
							$exif_data['iso'] = $val;
						break;
						case "Flash":
							$exif_data['flash'] = $val;
						break;
						case "FocalLength":
							$exif_data['focal_length'] = $val;
						break;
				   }//endswitch
			   }//endforeach
			}//endforeach
			return $exif_data;
		}//endfunction
		
		
		public function merge_var_to_const($constant,$var){
			$constant = str_replace("%string%",$var,$constant);
			return $constant;
		}//end if
		
		//clean a string from empty spaces
		public function clean_spaces($txt){
			$txt = str_replace(" ","",$txt);
			//clear from double commas
			$txt = str_replace(",,",",",$txt);
			return $txt;
		}//end function
		
		public function create_headers($csslist,$script_list){
			$code = $this->create_css_includes($csslist);
			$code .= $this->create_js_includes($script_list);
			return $code;
		}//end function
		
		private function create_css_includes($csslist){
			$csslist = explode(",",$csslist);
			$css_base = __SERVERPATH__."admin/css/";
			for($i = 0; $i < sizeof($csslist); $i++){
				$css = $csslist[$i];
				$code .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"".__SERVERPATH__."admin/css/".$css."\" />\n";
			}//end for
			return $code;
		}//end function
		
		private function create_js_includes($script_list){
			$scripts = explode(",",$script_list);
			$script_base = __SERVERPATH__."admin/includes/js/";
			for($i = 0; $i < sizeof($scripts); $i++){
				$jsscript = $scripts[$i];
				switch($jsscript){
					case "jquery":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/".__JQUERY_VERSION__."/jquery-".__JQUERY_VERSION__.".pack.js\"></script>\n";
						$line .= "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/interface/".__INTERFACE_VERSION__."/interface.js\"></script>\n";
						$code .= $line;
						break;
					case "tiny_mce":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."tiny_mce/".__TINYMCE_VERSION__."/tiny_mce.js\"></script>\n";
						$code .= $line;
						break;
					case "lightbox":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.lightbox.js\"></script>\n";
						$line .= "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.color.packed.js\"></script>\n";
						$code .= $line;
						break;
					case "autocomplete":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.autocomplete.pack.js\"></script>\n";
						$code .= $line;
						break;
					case "jquery-ui":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery-ui-1.7.1.custom.min.js\"></script>\n";
						$code .= $line;
						break;						
					case "swfobject":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."swfobject/".__SWFOBJECT_VERSION__."/swfobject.js\"></script>\n";
						$code .= $line;
						break;
					case "date_picker":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/date.js\"></script>\n";
						$line .= "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.datePicker2.js\"></script>\n";
						$code .= $line;
						break;
					default:
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base.$jsscript."\"></script>\n";
						$code .= $line;
				}//end switch
			}//end for
			return $code;
		}//end function
		
		//check if the passed filename is a shared file
		public function is_shared_attachment($filename){
			$query = "SELECT COUNT(*) as files FROM ".$this->t."attachments WHERE attachment_filename = '".$filename."'";		
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$row = @mysql_fetch_array($result);
			if($row['files'] > 1){
				//In this case exists more than one currencies for this file, so it's shared
				return true;
			} else {
				//It's unique
				return false;
			}//end if
		}//end function
		
		public function delete_attachment($module,$attachmentID){
			$aID = $attachmentID;//Just a short name
			if(is_array($aID)){
				for($j = 0; $j < sizeof($aID); $j++){
					//First read the data of the attachment
					$attachData = $this->get_attachment($aID[$j]);
					//Check if the file is shared or not	
					if($this->is_shared_attachment($attachData['attachment_filename']) == false && $attachData['attachment_is_file'] == 1){			
						//If it's not shared, I'm going to delete all the currencies
						$isImg = $attachData['attachment_is_img'];				
						$formats = explode(",",__IMAGE_FORMATS_LIST__);
						//add the 'o' format
						array_push($formats,"o");
						//Create the path
						$path = $_SERVER['DOCUMENT_ROOT']."/".__SERVERPATH__."/upload/";
						if($isImg == 1){
						//It's an image attachment
							$path .= $module."/photos/";
							$path = $this->clean_slashes($path);
							//erase all the files
							for($i = 0; $i < sizeof($formats); $i++){					
								if(file_exists($path.$formats[$i]."/".$attachData['attachment_filename'])){							
									if(!unlink($path.$formats[$i]."/".$attachData['attachment_filename'])){
										return false;
									}//end if					
								}//end if
							}//end for				
						} else {
						//It's a normal attachment
							$path .= $module."/files/";
							$path = $this->clean_slashes($path);				
							if(file_exists($path.$attachData['attachment_filename'])){	
								if(!unlink($path.$attachData['attachment_filename'])){
									return false;
								}//end if
							}//end if
							//Check if there is a thumbnail
							if($attachData['attachment_screenshot'] != NULL){
								if(file_exists($path."t/".$attachData['attachment_screenshot'])){	
									if(!unlink($path."t/".$attachData['attachment_screenshot'])){
										return false;
									}//end if
								}//end if								
							}//end if
						}//end if
					}//end if
					
					//Delete the record from database
					$query = "DELETE FROM ".$this->t."attachments WHERE attachment_id = ".$aID[$j];
					$result = @mysql_query($query,$this->db);
					$this->error($result,__LINE__,get_class($this));
				}//end for
			} else {
				//First read the data of the attachment
				$attachData = $this->get_attachment($aID);				
				//Check if the file is shared or not					
				if($this->is_shared_attachment($attachData['attachment_filename']) == false && $attachData['attachment_is_file'] == 1){			
					//If it's not shared, I'm going to delete all the currencies
					$isImg = $attachData['attachment_is_img'];				
					$formats = explode(",",__IMAGE_FORMATS_LIST__);
					//add the 'o' format
					array_push($formats,"o");
					//Create the path
					$path = $_SERVER['DOCUMENT_ROOT']."/".__SERVERPATH__."/upload/";
					if($isImg == 1){
					//It's an image attachment
						$path .= $module."/photos/";
						$path = $this->clean_slashes($path);
						//erase all the files
						for($i = 0; $i < sizeof($formats); $i++){					
							if(file_exists($path.$formats[$i]."/".$attachData['attachment_filename'])){							
								if(!unlink($path.$formats[$i]."/".$attachData['attachment_filename'])){
									return false;
								}//end if					
							}//end if
						}//end for				
					} else {
					//It's a normal attachment
						$path .= $module."/files/";
						$path = $this->clean_slashes($path);				
						if(file_exists($path.$attachData['attachment_filename'])){	
							if(!unlink($path.$attachData['attachment_filename'])){
								return false;
							}//end if
						}//end if
						//Check if there is a thumbnail
						if($attachData['attachment_screenshot'] != NULL){
							if(file_exists($path."t/".$attachData['attachment_screenshot'])){	
								if(!unlink($path."t/".$attachData['attachment_screenshot'])){
									return false;
								}//end if
							}//end if								
						}//end if
					}//end if
				}//end if
				
				//Delete the record from database
				$query = "DELETE FROM ".$this->t."attachments WHERE attachment_id = ".$aID;
				$result = @mysql_query($query,$this->db);
				$this->error($result,__LINE__,get_class($this));
			}//end if			
			return true;
		}//end function
		
		//This function save the attachment data in the database
		public function store_attachment(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$upload_class = $params[0];				
				$id = $params[1];			
				$module_id = $params[2];				
			}//end if			
		
			$filename = $upload_class->get_filename();
			$mime =  $upload_class->get_mime();
			if(!isset($title)){
				//fallback
				$title = trim($_REQUEST['file_title']);
			}//end if
			if(!isset($caption)){
				//fallback
				$caption = trim($_REQUEST['caption']);
			}//end if
			
			$type = "file";
			$is_img = $this->is_img($mime);
			if($is_img){
				$type = "image";
			}//end if
		
			if($is_img){	
				if($this->have_images($id,$module_id)){
					$default_img = "0";
				} else {
					$default_img = "1";
				}//end if
			} else {
				$default_img = "0";
			}//end if	
			
			$query = "INSERT INTO ".$this->t."attachments
					  (attachment_title,attachment_filename,attachment_mime,attachment_ref_id,attachment_ref_module_id,
					  attachment_type,attachment_default, attachment_caption)
					  VALUES
					  ('".$title."','".$filename."','".$mime."',".$id.",".$module_id.",'".$type."',".$default_img.",'".$caption."')";	
					  echo $query;				
			$result = @mysql_query($query,$this->db);			
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->file_id = @mysql_insert_id();
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function store_static_attachment(){
			$params = func_get_args();
			$filename = $params[0];				
			$id = $params[1];			
			$module_id = $params[2];
			if(isset($params[3]) && $params[3] != NULL){
				$extra_data = $params[3];
			}//end if
			
			$image = $this->call("image");
			$image->init_id3("includes/lib/getid3.php");
			$fileinfo = $image->get_id3("../upload/static/".$filename);
			if($fileinfo != false){
				if(is_array($fileinfo['video']) && !is_null($fileinfo['video'])){					
					$extra_data = array();
					$extra_data['width'] = $fileinfo['video']['resolution_x'];
					$extra_data['height'] = $fileinfo['video']['resolution_y'];	
					$extra_data = base64_encode(serialize($extra_data));
				}//end if
			}//end if
			$mime = $this->get_mimetype($filename);
			if(!isset($title)){
				//fallback
				$title = trim($_REQUEST['file_title']);
			}//end if
			if(!isset($caption)){
				//fallback
				$caption = trim($_REQUEST['caption']);
			}//end if
		
			$type = "file";
			
			$order = $this->get_num_attachments($module_id,$id,$is_img);
			
			$query = "INSERT INTO ".$this->t."attachments
					  (attachment_title,attachment_filename,attachment_mime,attachment_ref_id,attachment_ref_module_id,
					  attachment_order,attachment_type,attachment_default,attachment_caption, attachment_is_static)
					  VALUES
					  ('".$title."','".$filename."','".$mime."',".$id.",".$module_id.",".$order.",'".$type."',".$default_img."','".$caption."',1)";					
			$result = @mysql_query($query,$this->db);			
			$this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->file_id = @mysql_insert_id();
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		/* DEPRECATED METHODS */
		/*
		public function get_tags($module = NULL,$limit = NULL){			
			$query = "SELECT tag_tag FROM ".$this->t."tags";
			if($module != NULL){
				//take the module  id
				$module = $this->get_module($module);
				$module_id = $module['module_id'];
				$query .= " WHERE tag_idmodule = '".$module_id."'";
			}//end if
			$query .= " ORDER BY tag_tag ASC";
			$result = $this->db->query($query);
			$this->error($result,__LINE__,get_class($this));	
			$num = $result->rowCount();
			if($num > 0){							
				$data = $this->get_result_array($result,false);							
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function get_post_tags($id,$module){
			//take the module  id
			$module = $this->get_module($module);
			$module_name = $module['module_name'];
			
			$query = "SELECT post_tags FROM ".$this->t."posts WHERE post_id = ".$id." AND post_module_id = '".$module_name."'";
			$result = $this->db->query($query);
			$this->error($result,__LINE__,get_class($this));	
			$num = $result->rowCount();
			if($num > 0){							
				$data = $this->get_result_array($result,true);							
				return $data['post_tags'];
			} else {
				return false;
			}//endif
		}//end function
				
		public function add_tags($tags,$id,$module){
			$tags = str_replace(", ",",",$tags);
			$tags = str_replace(" ,",",",$tags);
			if(substr($tags,-1) == ","){
				$tags = substr($tags,0,-1);	
			}//end if
			if(substr($tags,0,1) == ","){
				$tags = substr($tags,1);	
			}//end if
			$tags = explode(",",$tags);
		
			for($i = 0; $i < sizeof($tags); $i++){
				if($tags[$i] != ""){
					if($i == 0){
						$tagsList .= $this->unifystring($tags[$i]);
					} else {
						$tagsList .= ",".$this->unifystring($tags[$i]);
					}//end if
				}//end if
			}//end for	
			
			$current_tags = $this->get_post_tags($id,$module);
			//Now, we have to delete the previous tags, just for do something clean
			$tags_to_del = explode(",",$current_tags);
			
			for($i = 0; $i < sizeof($tags_to_del); $i++){
				$this->delete_tag($tags_to_del[$i],$module);
			}//end if
			$tagsList = trim($tagsList);
			if(substr($tagsList,-1) == ","){
				$tagsList = substr($tagsList,1,strlen($tagsList));
			}//end if
			
			$query = "UPDATE ".$this->t."posts SET post_tags = '".$tagsList."' WHERE post_id = ".$id." AND post_module_id = '".$module."'";
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));				
			if($errors === false){
				$this->store_tags($tagsList,$module);
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function delete_tag($tag,$module){
			//take the module  id
			$module = $this->get_module($module);
			$module_id = $module['module_id'];
			
			$query = "SELECT * FROM ".$this->t."tags 
					  WHERE tag_tag = '".$tag."'
					  AND tag_idmodule = ".$module_id;							 
			$result = $this->db->query($query);
			$this->error($result,__LINE__,get_class($this));
			$num = $result->rowCount();
			if($num > 0){
				//tag already exists
				$row = $result->fetch(PDO::FETCH_ASSOC);
				$count = $row['tag_count'];
				if($count > 1){
					$count--;
					$query = "UPDATE ".$this->t."tags SET tag_count = ".$count."
							  WHERE tag_id = ".$row['tag_id'];							
				} else {
					$query = "DELETE FROM ".$this->t."tags WHERE tag_id = ".$row['tag_id'];
				}//end if
				$result = $this->db->query($query);
				$errors = $this->error($result,__LINE__,get_class($this));
				if($errors === false){
					return true;
				} else {
					return false;
				}//end if
			}//end if
		}//end function
		
		public function delete_tags($tags,$module){
			//take the module  id
			$module = $this->get_module($module);
			$module_id = $module['module_id'];
			//Clean from spaces
			$tags = str_replace(", ",",",$tags);
			$tags = str_replace(" ,",",",$tags);
			//clean from last comma
			if(substr($tags,-1) == ","){
				$tags = substr($tags,0,strlen($tags)-1);
			}//end if	
			//create the array	
			$tags = explode(",",$tags);
			if(sizeof($tags) > 0){
				for($i = 0; $i < sizeof($tags); $i++){
					//clean from  other chars
					$tags[$i] = $this->unifystring($tags[$i]);
					//check if the tag already exists in the db
					$query = "SELECT * FROM ".$this->t."tags 
							  WHERE tag_tag = '".$tags[$i]."'
							  AND tag_idmodule = ".$module_id;							 
					$result = $this->db->query($query);
					$this->error($result,__LINE__,get_class($this));
					$num = $result->rowCount();
					if($num > 0){
						//tag already exists
						$row = $result->fetch(PDO::FETCH_ASSOC);
						$count = $row['tag_count'];
						if($count > 1){
							$count--;
							$query = "UPDATE ".$this->t."tags SET tag_count = ".$count."
									  WHERE tag_id = ".$row['tag_id'];							
						} else {
							$query = "DELETE FROM ".$this->t."tags WHERE tag_id = ".$row['tag_id'];
						}//end if
						$result = $this->db->query($query);
						$errors = $this->error($result,__LINE__,get_class($this));						
					}//end if
				}//end for	
			}//end if
			return true;
		}//end function
		
		public function store_tags($tags,$module){
			$tags = trim($tags);
			//take the module  id
			$module = $this->get_module($module);
			$module_id = $module['module_id'];
			//Clean from spaces
			$tags = str_replace(", ",",",$tags);
			$tags = str_replace(" ,",",",$tags);
			//clean from last comma
			if(substr($tags,-1) == ","){
				$tags = substr($tags,-1);
			}//end if			
			//create the array	
			$tags = explode(",",$tags);
			if(sizeof($tags) > 0){
				for($i = 0; $i < sizeof($tags); $i++){
					//clean from  other chars
					$tags[$i] = $this->unifystring($tags[$i]);
					//check if the tag already exists in the db
					$query = "SELECT * FROM ".$this->t."tags 
							  WHERE tag_tag = '".$tags[$i]."'
							  AND tag_idmodule = ".$module_id;							 
					$result = $this->db->query($query);
					$this->error($result,__LINE__,get_class($this));
					$num = $result->rowCount();
					if($num > 0){
						//tag already exists
						$row = $result->fetch(PDO::FETCH_ASSOC);
						$count = $row['tag_count'];
						$count++;
						$query = "UPDATE ".$this->t."tags SET tag_count = ".$count."
								  WHERE tag_id = ".$row['tag_id'];
						$result = $this->db->query($query);
						$this->error($result,__LINE__,get_class($this));
					} else{
						if($tags[$i] != NULL){
							//tag not exists
							$query = "INSERT INTO ".$this->t."tags 
									  (tag_tag, tag_count, tag_idmodule)
									  VALUES
									  ('".$tags[$i]."',1,".$module_id.")";
							$result = $this->db->query($query);
							$this->error($result,__LINE__,get_class($this));
						}//end if
					}//end if
				}//end if
			}//end for
			return true;
		}//end function
		*/
	}//end class
?>