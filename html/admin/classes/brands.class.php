<?php
	require_once('ccci.class.php');

	class brands extends ccci{

		public $id;
		public $owner_id;
		public $prefix = 'brand';

		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->utils->read_params($this,$params);
			$this->set_module("brands");
			//Meta init
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);
		}//endconstructor

		public function get_brands(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($fields)){
				$fields = "*";
			}//end if

			if(!isset($show_legacy)){
				$show_legacy = false;
			}//end if

			$query = "SELECT ".$fields." FROM (brands, brands_owners)
					  LEFT JOIN brands_owners_rel
					  ON brand_owner_rel_owner_id = brand_owner_id
					  LEFT JOIN brands_categories
					  ON brands.brand_category_id = brands_categories.brand_category_id";
			if(isset($company_id)){
				$query .= " LEFT JOIN brands_rel
					  		ON (brand_rel_brand_id = brand_id AND brand_rel_company_id = ".$company_id.")";
			}//end if
			$query .= " WHERE brand_owner_id =  brand_advertiser_id
					  AND brand_temp = 0";

			if(isset($search)){
				$search = str_replace(" ","%",$search);
				$query .= " AND brand_name LIKE '%".$search."%'";
			}//end if

			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND ".$this->prefix."_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND ".$this->prefix."_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if

			if($show_legacy == false && isset($company_id)){
				$query .= " AND brand_rel_active = 1";
			}//end if

			if(isset($brand_owner_id)){
				$query .= " AND brand_advertiser_id = ".$brand_owner_id;
			}//end if

			if(isset($company_id)){
				$query .= " AND brand_owner_rel_company_id = ".$company_id." AND brand_rel_brand_id IS NOT NULL";
			}//end if

			if(isset($user_id)){
				$query .= " AND brand_owner_rel_uid = ".$user_id;
			}//end if

			$query .= " GROUP BY brand_id
						ORDER BY brand_name ASC";

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if

			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_brands_owners(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($fields)){
				$fields = "*";
			}//end if

			if(!isset($show_legacy)){
				$show_legacy = false;
			}//end if

			if(!isset($show_inactive)){
				$show_inactive = false;
			}//end if

			//temporary change of the prefix
			$default_prefix = $this->prefix;
			$this->prefix = 'brand_owner';

			$query = "SELECT ".$fields." FROM brands_owners
					  LEFT JOIN brands_owners_rel
					  ON brand_owner_rel_owner_id = brand_owner_id
					  WHERE brand_owner_name IS NOT NULL";

			if(isset($search)){
				$search = str_replace(" ","%",$search);
				$query .= " AND brand_owner_name LIKE '%".$search."%'";
			}//end if

			if($show_inactive == false){
				$query .= " AND brand_owner_active = 1";
			}//end if

			if($show_legacy == false){
				$query .= " AND brand_owner_rel_active = 1";
			}//end if

			if(isset($company_id)){
				$query .= " AND brand_owner_rel_company_id = ".$company_id;
			}//end if

			if(isset($user_id)){
				$query .= " AND brand_owner_rel_uid = ".$user_id;
			}//end if

			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND ".$this->prefix."_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND ".$this->prefix."_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if

			$query .= " GROUP BY brand_owner_id
						ORDER BY brand_owner_name ASC";

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if

			//reset the prefix
			$this->prefix = $default_prefix;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_brand(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			$query = "SELECT * FROM brands, brands_categories
					  WHERE brands.brand_category_id = brands_categories.brand_category_id
					  AND brand_id = ".$id;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_brands_categories(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			$query = "SELECT * FROM brands_categories ORDER BY brand_category_name ASC";

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_brands_report(){
  		$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			$query = "SELECT brand_name, COUNT(ad_submission_id) AS submissions
                FROM (brands, ads_submissions, users, companies)
                WHERE ad_submission_brand_owner_id = ".$brand_owner_id."
                AND brand_id = ad_submission_brand_id
                AND ad_submission_uid = user_id
                AND ad_submission_company_id = company_id
                AND user_active = 1
                AND ad_submission_temp = 0
                AND company_status = 'active'
                AND company_temp = 0
                AND brand_temp = 0
                AND ad_submission_status IN ('final-approval','interim-approval','pending','not-valid','not-approved','sm-received')";

		  if(isset($from_month)){
				$query .= " AND MONTH(ad_submission_date_replied) >= ".$from_month;
			}//end if

			if(isset($to_month)){
				$query .= " AND MONTH(ad_submission_date_replied) <= ".$to_month;
			}//end if

			if(isset($from_year)){
				$query .= " AND YEAR(ad_submission_date_replied) >= ".$from_year;
			}//end if

			if(isset($to_year)){
				$query .= " AND YEAR(ad_submission_date_replied) <= ".$to_year;
			}//end if

			if(isset($year)){
				$query .= " AND YEAR(ad_submission_date_replied) = ".$year;
			}//end if

			$query .= " GROUP BY brand_id
                  ORDER BY brand_name ASC";

      $result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_brand_category(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($category_id)){
				return false;
			}//end if

			$query = "SELECT * FROM brands_categories WHERE brand_category_id = ".$category_id." LIMIT 1";

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function save_brand(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($name)){
				return false;
			}//end if

			if(!isset($temp)){
				$temp = '0';
			}//end if

			if(!isset($active)){
				$active = '0';
			}//end if

			if(!isset($user_id)){
				$user_id = '0';
			}//end if

			if(!isset($category_id)){
				$category_id = '0';
			}//end if

			if(!isset($billing_name)){
				$billing_name = "";
			}//end if

			if(!isset($billing_email)){
				$billing_email = "";
			}//end if

			if(!isset($owner_id)){
				$owner_id = 1;
			}//end if

			$query = "INSERT INTO brands
					 (brand_name, brand_active, brand_category_id, brand_advertiser_id, brand_billing_name, brand_billing_email, brand_temp)
					 VALUES
					 (:name, :active, :category_id, :owner_id, :billing_name, :billing_email, :temp)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':name', $name, PDO::PARAM_STR);
			$result->bindParam(':active', $active, PDO::PARAM_INT);
			$result->bindParam(':category_id', $category_id, PDO::PARAM_INT);
			$result->bindParam(':owner_id', $owner_id, PDO::PARAM_INT);
			$result->bindParam(':billing_name', $billing_name, PDO::PARAM_STR);
			$result->bindParam(':billing_email', $billing_email, PDO::PARAM_STR);
			$result->bindParam(':temp', $temp, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->id = $this->utils->db->lastInsertId();
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function save_brand_owner(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($name)){
				return false;
			}//end if

			if(!isset($active)){
				$active = '1';
			}//end if

			if(!isset($user_id)){
				$user_id = 1;//default to the Admin
			}//end if

			if(!isset($company_id)){
				$company_id = '0';//default
			}//end if

			$query = "INSERT INTO brands_owners
					  (brand_owner_name, brand_owner_uid, brand_owner_active)
					  VALUES
					  (:name, :user_id, :active)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':name', $name, PDO::PARAM_STR);
			$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':active', $active, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->owner_id = $this->utils->db->lastInsertId();
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function save_brand_rel(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($brand_id) || !isset($company_id) || !isset($user_id)){
				return false;
			}//end if

			if(!is_array($brand_id)){
				$brand_id = array($brand_id);
			}//end if

			for($i = 0; $i < sizeof($brand_id); $i++){
				$query = "INSERT INTO brands_rel
						  (brand_rel_brand_id, brand_rel_uid, brand_rel_company_id)
						  VALUES
						  (:brand_id, :user_id, :company_id)";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':brand_id', $brand_id[$i], PDO::PARAM_INT);
				$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
				$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
			}//end for
			return true;
		}//end function

		public function save_brand_owner_rel(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($owner_id) || !isset($company_id) || !isset($user_id)){
				return false;
			}//end if

			if(!is_array($owner_id)){
				$owner_id = array($owner_id);
			}//end if

			for($i = 0; $i < sizeof($owner_id); $i++){
				$query = "INSERT INTO brands_owners_rel
						  (brand_owner_rel_owner_id, brand_owner_rel_uid, brand_owner_rel_company_id)
						  VALUES
						  (:owner_id, :user_id, :company_id)";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':owner_id', $owner_id[$i], PDO::PARAM_INT);
				$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
				$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
			}//end for
			return true;
		}//end function

		public function update_brand_associations(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($company_id) || !isset($_POST['brand']) || !isset($_POST['brand_owner'])){
				return false;
			}//end if

			$user_id = $this->utils->get_uid();
			$brands_sent = $_POST['brand'];
			$brands_owners_sent = $_POST['brand_owner'];
			//brands need to be an array
			if(!is_array($brands_sent) || !is_array($brands_owners_sent)){
				return false;
			}//end if
			$this->utils->db->beginTransaction();

			try{
				//first we reset the current relationships and we set everything to inactive
				$query = "UPDATE brands_rel SET brand_rel_active = 0 WHERE brand_rel_company_id = ".$company_id;
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors !== false){
					throw new PDOException('Cannot reset the brands associations');
				}//end if
				//we do the same for the brand owners
				$query = "UPDATE brands_owners_rel SET brand_owner_rel_active = 0 WHERE brand_owner_rel_company_id = ".$company_id;
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors !== false){
					throw new PDOException('Cannot reset the brands owners associations');
				}//end if

				//now we set to active only the passed brands & brands owners
				for($i = 0; $i < sizeof($brands_sent); $i++){
					$query = "UPDATE brands_rel SET brand_rel_active = 1 WHERE brand_rel_company_id = ".$company_id." AND brand_rel_id = ".$brands_sent[$i];
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors !== false){
						throw new PDOException('Cannot update the brand_rel id: '.$brands_sent[$i]);
					}//end if
				}//end for i

				//now we set to active only the passed brands & brands owners
				for($i = 0; $i < sizeof($brands_owners_sent); $i++){
					$query = "UPDATE brands_owners_rel SET brand_owner_rel_active = 1 WHERE brand_owner_rel_company_id = ".$company_id." AND brand_owner_rel_id = ".$brands_owners_sent[$i];
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors !== false){
						throw new PDOException('Cannot update the brand_owner_rel id: '.$brands_owners_sent[$i]);
					}//end if
				}//end for i
				$this->utils->db->commit();
				return true;
			} catch(PDOException $ex){
				$this->utils->db->rollback();
				$this->errors = $ex->getMessage();
				return false;
			}//end try
		}//end function

		public function update_brand_details(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			$query = "UPDATE brands SET
					  brand_billing_name = :billing_name,
					  brand_billing_email = :billing_email,
					  brand_category_id = :category_id
					  WHERE brand_id = :id";
			if(!is_array($id)){
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':billing_name', $_POST['company_brand_name'], PDO::PARAM_STR);
				$result->bindParam(':billing_email', $_POST['company_brand_email'], PDO::PARAM_STR);
				$result->bindParam(':category_id', $_POST['company_brand_category_id'], PDO::PARAM_INT);
				$result->bindParam(':id', $id, PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
			} else {
				for($i = 0; $i < sizeof($id); $i++){
					$result = $this->utils->db->prepare($query);
					$result->bindParam(':billing_name', $_POST['company_brand_name'][$i], PDO::PARAM_STR);
					$result->bindParam(':billing_email', $_POST['company_brand_email'][$i], PDO::PARAM_STR);
					$result->bindParam(':category_id', $_POST['company_brand_category_id'][$i], PDO::PARAM_INT);
					$result->bindParam(':id', $id[$i], PDO::PARAM_INT);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));
				}//end for i
			}//end if

			return true;

		}//end function
	}//end class
?>