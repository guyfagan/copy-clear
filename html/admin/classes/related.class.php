<?php
	#################################################################
	# Related Class -  last update 22/12/12							#
	#				   created in 2012								#
	# Updates														#
	# 22/12/12 - Converted to PDO									#			
	#################################################################
	
	require_once('ewrite.class.php');

	class related extends ewrite{
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);		
			$this->module = "related";
		
		}//endconstructor
		
		public function get_related_posts($module_id,$pid){
			if(is_null($module_id) || !is_numeric($pid)){
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];
			}//end if
			
			$query = "SELECT posts.*,related_posts.related_post_id, related_posts.related_post_uid,
					  category_name, category_id
					  FROM (related_posts, posts, modules, modules_items_order)
					  LEFT JOIN categories
					  ON category_id = module_item_order_father_id
					  WHERE related_post_dest_post_id = ".$pid." 
					  AND related_post_dest_module_id = ".$module_id." 
					  AND related_post_source_post_id = post_id
					  AND post_module_id = module_name
					  AND related_post_source_module_id = module_id
					  AND module_item_order_item_id = post_id
					  AND module_item_order_type = 'post'
					  GROUP BY post_id
					  ORDER BY related_post_order ASC";				
	
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function create_relation($src_pid, $src_module_id,$dest_pid, $dest_module_id){
			$user_id = $this->utils->get_uid();
			if($user_id == false){
				return false;
			}//end if
			
			//get the order counting the previous related posts for the current one
			$order = $this->get_related_post_order($dest_pid, $dest_module_id);
			
			//insert the data in the database
			$query = "INSERT INTO related_posts
					  (related_post_source_post_id, related_post_source_module_id, related_post_dest_post_id, related_post_dest_module_id, related_post_uid, related_post_order)
					  VALUES
					  (".$src_pid.",".$src_module_id.",".$dest_pid.",".$dest_module_id.",".$user_id.",".$order.")";
					
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		private function get_related_post_order($dest_pid, $dest_module_id){
			$query = "SELECT COUNT(*) AS total FROM related_posts WHERE related_post_dest_post_id = ".$dest_pid." AND related_post_dest_module_id = ".$dest_module_id;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error === false){
				$num = $result->rowCount();
				if($num > 0){							
					$row = $result->fetch(PDO::FETCH_ASSOC);
					$order = $row['total'];
					$order++;
					return $order;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
				
		public function delete_relation($rid){
			if(is_numeric($rid)){
				$query = "DELETE FROM related_posts WHERE related_post_id = ".$rid;
				$result = $this->utils->db->query($query);
				$error = $this->utils->error($result,__LINE__,get_class($this));	
				if($error === false){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
				
		public function save_related_sorting($dest_post_id, $dest_module_id, $related_ids){
			$ri = $related_ids;
			if(is_array($ri) && sizeof($ri) > 0){
				for($i = 0; $i < sizeof($ri); $i++){					
					$query = "UPDATE ".$this->t."related_posts 
							  SET related_post_order = ".$i."
							  WHERE related_post_id = ".$ri[$i];
					$result = $this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));					
				}//end for
				return true;
			} else {
				return false;
			}//end if		
		}//end function
	}//end class
?>