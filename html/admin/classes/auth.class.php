<?php
	require_once("users.class.php");
	
	class auth extends users{
	
		private $bothways;
		public $gid; 
		protected $settings;
		protected $cookie_expiry;
	
		//Constructor
		function __construct($utils){
			if(class_exists("users")){
				parent::__construct($utils);
				$this->init_settings();			
			}//end if
			
			$this->bothways = false;
		}//endconstructor
		
		public function enable_bothways($bothways){
			if(is_bool($bothways)){
				$this->bothways = $bothways;
			}//end if
		}//end function
		
		public function set_login_type($lt = "cookie"){
			$this->lt = $lt;
		}//end function
		
		public function allow_groups($gid){
			$this->gid = $gid;
		}//end function
		
		public function is_inactive_user($username,$password){
			$sha1_password = sha1($password);
			//Execute the query
			$query = "SELECT user_id, HEX(user_id) AS uidhex, HEX(user_username) AS unamehex
					  FROM (users, users_groups_rel, users_groups)
					  WHERE users_groups_rel.users_groups_rel_uid = users.user_id
					  AND users_groups_rel.users_groups_rel_gid = users_groups.users_group_id";
			if($this->bothways == true){
				$query .= " AND (TRIM(user_username) = '".$username."' OR TRIM(user_email) = '".$username."')";
			} else {
				$query .= " AND ".$u_field." = '".$username."'";
			}//end if
			$query .= " AND user_active = 0
					  AND user_password = '".$sha1_password."'";			
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));
			if($error == false){
				$num = $result->rowCount();
				if($num == 1){
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		//login
		public function do_login($username,$password,$use_mail = false){
			//if the user access is the email, use this field for authenticate
			if($use_mail == true){
				$u_field = "user_email";
			} else {
				$u_field = "user_username";
			}//end if			
			//Crypting the password
			$sha1_password = sha1($password);
			//Execute the query
			$query = "SELECT user_id, HEX(user_id) AS uidhex, HEX(user_username) AS unamehex
					  FROM (users, users_groups_rel, users_groups)
					  WHERE users_groups_rel.users_groups_rel_uid = users.user_id
					  AND users_groups_rel.users_groups_rel_gid = users_groups.users_group_id";
			if($this->bothways == true){
				$query .= " AND (user_username = '".$username."' OR user_email = '".$username."')";
			} else {
				$query .= " AND ".$u_field." = '".$username."'";
			}//end if
			$query .= " AND user_active = 1
					  AND user_password = '".$sha1_password."'";
			if($this->gid != NULL){
				if(is_array($this->gid)){
					$query .= " AND users_group_id IN (".implode(",",$this->gid).")";
				} else {
					$query .= " AND users_group_id = ".$this->gid;
				}//end if
			}//end if		
			
			$result = $this->utils->db->query($query);
			$this->utils->error($result,__LINE__,get_class($this));
			$num = $result->rowCount();
			//If an user is found, do the rest			
			if($num == 1){				
				$row = $result->fetch(PDO::FETCH_ASSOC);
				$this->uid = $row['user_id'];					
				$secure_id = sha1($row['uidhex'].$row['unamehex']);					
				if($this->lt == "cookie"){
					setcookie("uid",$this->uid,$this->cookie_expiry,"/");
					setcookie("secure_id",$secure_id,$this->cookie_expiry,"/");			
					return true;
				} else if($this->lt == "session"){
					$_SESSION['uid'] = $this->uid;
					$_SESSION['secure_id'] = $secure_id;
					return true;
				} else {
					return false;
				}//endif
			} else {				
				if($this->lt == "cookie"){
					setcookie("uid","",time()-3600,"/");
					setcookie("secure_id","",time()-3600,"/");
					return false;
				} else if($this->lt == "session"){
					$_SESSION['uid'] = NULL;
					unset($_SESSION['uid']);
					$_SESSION['secure_id'] = NULL;
					unset($_SESSION['secure_id']);
					return false;
				} else {
					return false;
				}//endif				
				return false;
			}//end if
		}//end function	
	
		
		public function do_logout(){	
			//we delete both cookies and sessions
			setcookie("uid","",time()-(3600*24),"/");
			setcookie("secure_id","",time()-(3600*24),"/");	
			$_SESSION['uid'] = NULL;
			$_SESSION['secure_id'] = NULL;
			unset($_SESSION['uid']);
			unset($_SESSION['secure_id']);			
		}//end function
		
		public function remember_user($remember_boole){
			if($remember_boole == "1"){				
				setcookie("oh_username_field",$_POST['username'],time()+((3600*24)*14),"/");
				setcookie("oh_password_field",$_POST['password'],time()+((3600*24)*14),"/");
			} else {
				setcookie("oh_username_field","",time()-(3600*24),"/");
				setcookie("oh_password_field","",time()-(3600*24),"/");		
			}//end if
		}//end if
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
		
		protected function init_settings(){
			$this->settings = $this->utils->get_settings(array('module' => $this->module));		
			if($this->lt == "cms"){
				if(isset($this->settings['login_type'])){
					$accepted_types = array('cookie','session');
					if(in_array($this->settings['login_type'], $accepted_types)){
						$this->lt = $this->settings['login_type'];	
					} else {
						//fallback to cookie
						$this->lt = 'cookie';
					}//end if
				} else {
					//fallback to cookie
					$this->lt = 'cookie';	
				}//end if
			}//end if
			
			$this->cookie_expiry = time()+((3600*24)*31);
			if(isset($this->settings['cookie_expiry']) && is_numeric(($this->settings['cookie_expiry'])) && (int)$this->settings['cookie_expiry'] > 0){
				$this->cookie_expiry = time()+(int)$this->settings['cookie_expiry'];				
			}//end if
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//endclass
?>