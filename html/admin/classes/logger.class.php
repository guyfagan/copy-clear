<?php
	#################################################################
	# Logger Class - 	last update 18/12/12						#
	#					created in 2012								#
	# This is a functional class for the Old Hat CMS, originally	#	
	# created for a dead branch of the cms called "cerberus"		#
	#																#
	#################################################################

	class logger{
		protected $utils;//utils class	
		protected $flood_threshold;	
		protected $api_key = NULL;
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;		
			$this->utils->read_params($this,$params);		
			$this->flood_threshold = 20;		
		}//endconstructor
		
		public function set_api_key($api_key){
			$this->api_key = $api_key;
		}//end function
		
		public function is_spammer(){
			//get the parameters passed in the function
			$params = func_get_args();			
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){				
					${$key} = addslashes($value);
				}//end if				
			}//end if
			
			if(!isset($task)){
				$task = "spammer.logged";
			}//end if
			
			if(!isset($ip)){
				$ip = $_SERVER['REMOTE_ADDR'];	
			}//end if
						
			$query = "SELECT COUNT(*) AS spam_counter FROM system_logs
					  WHERE system_log_type = '".$task."'
					  AND system_log_ip = '".$ip."'";					  
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){					
					return true;
				} else {
					return false;
				}//end if				
			} else {
				return false;
			}//end if
			
		}//end function
		
		public function blacklist_ip($ip){
			$query = "INSERT INTO blacklist 
					  (blacklist_ip)
					  VALUES
					  (:ip)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':ip', $ip, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){				
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_blacklist(){
			//get the parameters passed in the function
			$params = func_get_args();			
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){				
					${$key} = addslashes($value);
				}//end if				
			}//end if
			
			$query = "SELECT *, UNIX_TIMESTAMP(blacklist_date) AS blacklist_date FROM blacklist ORDER by blacklist_date DESC";
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;					
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function is_blacklisted($ip){
			$query = "SELECT * FROM blacklist WHERE blacklist_ip LIKE :ip";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':ip', $ip, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){				
				$num = $result->rowCount();
				if($num > 0){
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function empty_blacklist(){
			//get the parameters passed in the function
			$params = func_get_args();			
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){				
					${$key} = addslashes($value);
				}//end if				
			}//end if
			$query = "TRUNCATE blacklist";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		/*
		With this function we can check if an user is flooding a specific task (like spammers), it returns true if the count of tasks is greater than the thresold
		*/
		public function check_flood(){
			//get the parameters passed in the function
			$params = func_get_args();			
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){				
					${$key} = addslashes($value);
				}//end if				
			}//end if
			
			if(!isset($task)){
				return false;	
			}//end if
			
			if(!isset($ip)){
				$ip = $_SERVER['REMOTE_ADDR'];	
			}//end if
			
			$settings = $this->utils->get_settings();
			//checking the whitelist
			if(isset($settings['whitelistIPs'])){
				$whitelist = $settings['whitelistIPs'];
				$whitelist = str_replace(" ","",$whitelist);
				$whitelist = explode(',',$whitelist);
				if(in_array($ip,$whitelist)){
					return false;	
				}//end if
			}//end if
			
			if(isset($threshold)){
				$this->flood_threshold = $threshold;	
			}//end if
			
			if(!isset($days)){
				$days = 7;	
			}//end if
			
			$query = "SELECT COUNT(*) AS attempts FROM system_logs
					  WHERE system_log_type = '".$task."'
					  AND system_log_ip = '".$ip."'
					  AND DATE(system_log_date) > CURDATE() - INTERVAL ".(int)$days." DAY";					  
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){					
					$row = $result->fetch(PDO::FETCH_ASSOC);					
					if($row['attempts'] >= $this->flood_threshold){
						$this->blacklist_ip($ip);
						return true;	
					} else {
						return false;	
					}//end if
				} else {
					return false;
				}//end if				
			} else {
				return false;
			}//end if
			
		}//end function
		
		public function get_logs(){
			$params = func_get_args();			
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){				
					${$key} = addslashes($value);
				}//end if				
			}//end if
			
			if(!isset($full)){
				$full = false;
			}//end if
			
			$select = 'SELECT system_logs.*, UNIX_TIMESTAMP(system_log_date) AS system_log_date, post_title, user_username, module_label';
			if($full === false){
				$select = 'SELECT system_log_type, system_log_ip, system_log_id, UNIX_TIMESTAMP(system_log_date) AS system_log_date, 
						   post_title, user_username, module_label';
			}//end if
			
			$query = $select." FROM (system_logs)
					  LEFT JOIN users
					  ON system_log_uid = user_id
					  LEFT JOIN modules
					  ON system_log_module_id = module_id
					  LEFT JOIN posts
					  ON system_log_post_id = post_id
					  ORDER BY system_log_date DESC";
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;					
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function empty_logs(){
			//get the parameters passed in the function
			$params = func_get_args();			
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){				
					${$key} = addslashes($value);
				}//end if				
			}//end if
			$query = "TRUNCATE system_logs";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function log_event(){
			//get the parameters passed in the function
			$params = func_get_args();			
			if(is_array($params[0]) && !is_null($params[0])){
				foreach($params[0] as $key => $value){				
					${$key} = addslashes($value);
				}//end if				
			}//end if
			
			//get the user id, if there is one or it has not passed to the method previously
			if(!isset($user_id)){			
				$user_id = $this->utils->get_uid();
				if($user_id === false){
					$user_id = "NULL";
				}//end if
			}//end if
			//getting the user ip address
			$ip_address = $_SERVER['REMOTE_ADDR'];
			//get the type
			if(!isset($type) && is_null($type)){
				$type = "general";
			}//end if
			//get the message
			if(!isset($message) && is_null($message)){
				$message = "";
			}//end if
			
			$api_key = "";
			if(!is_null($this->api_key)){
				$api_key = $this->api_key;
			}//end if
			
			//get the module id, if we have one
			if(!isset($module_id) || is_null($module_id)){
				$module_id = "0";
			} else if(!is_numeric($module_id) && !is_null($module_id)){
				$module_data = $this->utils->get_module($module_id);
				if($module_data !== false){
					$module_id = $module_data['module_id'];
				} else {
					//if I can't find any modules, just pass null
					$module_id = "0";
				}//end if
			}//end if
			if($module_id == NULL){
				$module_id = "0";
			}//end if
			//get the post id, if we have one
			if(!isset($post_id) && is_null($post_id)){
				$post_id = "NULL";
			}//end if			
			//get the post id, if we have one
			if(!isset($login_area) && is_null($login_area)){
				$login_area = "private";
			}//end if
			
			//clean old logs
			$this->logs_autoclean();
			
			$query = "INSERT INTO system_logs
					  (system_log_ip, system_log_type, system_log_message, system_log_module_id, system_log_uid,system_log_post_id,system_log_area, system_log_api_key)
					  VALUES
					  (:ip,:type,:message,:module_id,:user_id,:post_id,:login_area, :api_key)";			  
			$result = $this->utils->db->prepare($query);	
			$result->bindParam(':ip', $ip_address, PDO::PARAM_STR);
			$result->bindParam(':type', $type, PDO::PARAM_STR);
			$result->bindParam(':message', $type, PDO::PARAM_STR);
			$result->bindParam(':api_key', $api_key, PDO::PARAM_STR);
			$result->bindParam(':module_id', $module_id);
			$result->bindParam(':user_id', $user_id);
			$result->bindParam(':post_id', $post_id);
			$result->bindParam(':login_area', $login_area);
			$result->execute();	
			$errors = $this->utils->error($result,__LINE__,get_class($this));			
			if($errors === false){				
				return true;
			} else {
				return false;
			}//end if	 	
		}//end function
		
		public function logs_autoclean(){
			$settings = $this->utils->get_settings();		
			if(isset($settings['deleteOldLogsDays'])){
				if(is_numeric($settings['deleteOldLogsDays']) && (int)$settings['deleteOldLogsDays'] > 0){
					$query = "DELETE FROM system_logs WHERE system_log_date < CURDATE()";
					if((int)$settings['deleteOldLogsDays'] > 1){
						$query .= " - INTERVAL ".(int)$settings['deleteOldLogsDays']." DAY";						
					}//end if
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors === false){
						//optimize the table
						$query = "OPTIMIZE TABLE system_logs";
						$result = $this->utils->db->query($query);
						$errors = $this->utils->error($result,__LINE__,get_class($this));
						return true;
					} else {
						return false;	
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
	}//end class
?>