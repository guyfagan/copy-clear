<?php
	#################################################################
	# Posts Class - 	last update 12/11/12						#
	#					created 12/11/12							#
	# This is a core class for the Old Hat CMS						#	
	#																#		
	#################################################################
	
	class posts{
		protected $utils;//utils class
		protected $uid;//userid
		protected $id;//post id	
		protected $category_id;//category id	
		public $module; //module	
		protected $settings;
		protected $meta;
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;		
			$this->module = "posts";			
			$this->utils->read_params($this,$params);	
			$module_data = $this->utils->get_module($this->module);
			$this->module_id = $module_data['module_id'];	
			//Meta init	
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);		
		}//endconstructor
		
		#################################################################
		# POSTS PART													#		
		#################################################################
		
		public function get_posts(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
		}//end function
		
		public function get_post(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
		}//end function
		
		#################################################################
		# CATEGORIES PART												#		
		#################################################################
		
		public function get_categories(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
		}//end function
		
		public function get_category(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################		
		
		
		private function init_settings(){
			$this->settings = array();
			/*
			$this->settings['no_reply_email'] = "no-reply@".__DOMAIN_NAME__;
			$this->settings['bcc_email'] = 	"developer@oldhat.ie";
			$this->settings['token_template'] = "{#timestamp#}:{#post_id#}:{#SECRET_KEY#}";
			*/
			//read the settings from the cms
			$cms_settings = $this->utils->get_settings($this->module);
			if($cms_settings !== false){
				//if we have something, merge the two arrays, bear in mind that the cms settings will override the previous settings
				$this->settings = array_merge($this->settings,(array)$cms_settings);	
			}//end if
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>