<?php
	class vimeo{
	
		public $utils;//utils class
		public $t;//prefix before each tablename
		private $api_path;
		private $format;
	
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;			
			$this->utils->read_params($this,$params);
			$this->format = "json";
			$this->api_path = "http://vimeo.com/api/v2/video/";
		}//endconstructor			
		
		public function get_video_id($url_video){
			$url_video = str_replace("#","",$url_video);
			$url_video = explode("/",$url_video);
			$vimeo_id = $url_video[sizeof($url_video)-1];		
			return $vimeo_id;
		}//end function
		
		public function get_video_data($url_video){
			$url_video = trim($url_video);
			$vimeo_id = $this->get_video_id($url_video);
			$vimeo = @curl_init();
			//echo $this->api_path.$url;
			@curl_setopt($vimeo, CURLOPT_URL, $this->api_path.$vimeo_id.".".$this->format);					
			@curl_setopt($vimeo, CURLOPT_RETURNTRANSFER, true);
			@curl_setopt($vimeo, CURLOPT_HEADER, false); 
			@curl_setopt($vimeo, CURLOPT_FOLLOWLOCATION, true); 			
			$vimeo_data = @curl_exec($vimeo);		
			$data = json_decode($vimeo_data,true);
			@curl_close($vimeo);
			
			return $data;
		}//end function
		
		public function get_video_thumbnails($url_video){
			$video_data = $this->get_video_data($url_video);			
			$photos = array();
			$photos['small'] = $video_data[0]['thumbnail_small'];
			$photos['medium'] = $video_data[0]['thumbnail_medium'];
			$photos['large'] = $video_data[0]['thumbnail_large'];
			return $photos;
		}//end function
		
		public function get_video_iframe(){
			$params = func_get_args();
			if(is_array($params[0])){			
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($url)){
				return false;	
			}//end if			
			
			if(!isset($ssl)){
				$ssl = false;
			}//end if
			
			$base_url = 'http://player.vimeo.com/video/';
			if($ssl){
				$base_url = 'https://player.vimeo.com/video/';	
			}//end if
			
			if(!isset($width)){
				$width = 560;//defaults	
			}//end if
			if(!isset($height)){
				$height = 315;//defaults
			}//end if
			
			$video_id = $this->get_video_id($url);
			
			if(isset($clean)){
				if($clean == true){	
					$video_id .= '?title=0&portrait=0&byline=0';
				}//end if
			}//end if
			
			$iframe = '<iframe width="'.$width.'" height="'.$height.'" src="'.$base_url.$video_id.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
			return $iframe;
		}//end function
	}//end class
?>