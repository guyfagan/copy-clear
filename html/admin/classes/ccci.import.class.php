<?php
	require_once('import.class.php');

	class ccci_import extends import{
		
		public $errors = array();
		public $settings = array();
		protected $meta;
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);		
			$this->utils->read_params($this,$params);			
			//Meta init	
			$this->meta = $this->utils->call("meta");			
			//Set basic settings
			$this->init_settings();
		}//endconstructor
		
		public function do_import($what = 'users'){
			$options = array();
			$options['host'] = __DB_HOST__;
			$options['dbname'] = 'ccci_old_4';
			$options['user'] = __DB_USER__;
			$options['pass'] = __DB_PASS__;
			
			$connected = $this->create_secondary_db_conn($options);
			if($connected){	
				try{
					switch($what){
						case 'users':
							$companies_imported = $this->import_companies();
							if($companies_imported){
								$result = $this->import_users();
								return $result;
							}//end if
						break;
						case 'brands':
							$result = $this->import_brands();
							return $result;				
						break;
						case 'advertisers':
							$result = $this->import_advertisers();
							return $result;				
						break;
						case 'campaigns':
							$result = $this->import_campaigns();
							return $result;				
						break;
						case 'comments':
							$result = $this->import_comments();
							return $result;				
						break;
						case 'submissions':
							$result = $this->import_submissions();
							return $result;				
						break;
						case 'ads':
							$result = $this->import_ads();
							return $result;				
						break;
						case 'codes':
							$result = $this->import_codes();
							return $result;				
						break;
						case 'relations':
							$result = $this->generate_brand_owner_rel();
							if($result){
								$result = $this->generate_brand_rel();
								return $result;
							} else {
								return false;	
							}//end if							
						default:
							return false;
					}
				} catch(Exception $ex){				
					$this->errors = $ex->getMessage();
					return false;	
				}//end try	
			}//end if
		}//end function
		
		protected function import_brands(){
			//read the brands
			$query = "SELECT * FROM brand";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];										
							$insert = "INSERT INTO brands
									   (brand_id, brand_name, brand_active, brand_category_id, brand_advertiser_id, brand_old_advertiser_id, brand_old_user_id, brand_temp) 
									   VALUES
									   (".$row['brand_no'].", '".addslashes($row['brand_name'])."', ".$row['brand_active'].", '".$row['category_no']."',".$row['advertiser_no'].", ".$row['advertiser_no'].", '".$row['user_no']."', 0)";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));									
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_ads(){
			//read the ads
			$query = "SELECT * FROM ad";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];
							
							$new_user_id = $this->get_new_uid(array('user_id' => $row['user_no']));	
							if($new_user_id === false){
								$new_user_id = "1";
							}//end if
							
							$status = 'inactive';
							if($row['ad_active'] == 1){
								$status = 'active';
							}//end if
																	
							$insert = "INSERT INTO ads
									   (ad_id, ad_title, ad_date, ad_brand_id, 
									   ad_brand_owner_id, ad_campaign_id, ad_company_id,
									   ad_uid, ad_medium_id, ad_status, ad_last_update, ad_old_contact_name, ad_temp) 
									   VALUES
									   (".$row['ad_no'].",'".addslashes($row['ad_title'])."', '".$row['last_activity']."', ".$row['brand_no'].", 
									   ".$row['advertiser_no'].", ".$row['campaign_no'].", ".$row['client_no']."
									   , ".$new_user_id.", ".$row['medium_no'].",'".$status."','".$row['last_activity']."','".addslashes($row['ad_contact'])."', 0)";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));									
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_campaigns(){
			//read the brands
			$query = "SELECT * FROM campaign";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];									
							$status = 'active';
							if($row['campaign_active'] == '0'){
								$status = 'inactive';	
							}//end if
							$insert = "INSERT INTO campaigns
									   (campaign_id, campaign_name, campaign_brand_id, campaign_uid, campaign_company_id, campaign_status, campaign_old_contact, campaign_temp) 
									   VALUES
									   (".$row['campaign_no'].", '".addslashes($row['campaign_name'])."', ".$row['brand_no'].", NULL, ".$row['client_no'].",'".$status."', '".addslashes($row['campaign_contact'])."',0)";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));									
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_codes(){
			//read the brands
			$query = "SELECT * FROM approval_code WHERE code_no > 0";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];								
							
							$insert = "INSERT INTO codes
									   (code_id, code_code, code_text) 
									   VALUES
									   (".$row['code_no'].", '".addslashes($row['code_ref'])."', '".addslashes($row['code_body'])."')";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));									
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_comments(){
			//read the brands
			$query = "SELECT * FROM comm";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];									
							$status = strtolower($row['comm_status']);		
							if($status == 'created'){
								$status = 'sent';	
							}//end if
							
							$new_user_id = $this->get_new_uid(array('user_id' => $row['user_no']));	
							if($new_user_id === false){
								$new_user_id = "1";
							}//end if
							
							$insert = "INSERT INTO messages
									   (message_id, message_date, message_sender_uid, message_ref_id, message_module_id, message_status, message_message) 
									   VALUES
									   (".$row['comm_no'].", '".$row['date_created']."', ".$new_user_id.", ".$row['sub_no'].",13,'".$status."', '".addslashes($row['content'])."')";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));									
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_submissions(){
			$tools = $this->utils->call('tools');
			$mediaSettings = $this->utils->get_settings(array('module' => 'media'));
			$amazonPath = 'https://'.$mediaSettings['mediaS3Region'].'.amazonaws.com/'.$mediaSettings['mediaS3DestinationBucket'];
			//read the submissions
			$query = "SELECT * FROM submission";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];									
							$status = str_replace(" ","-",strtolower($row['approval_status']));		
							switch($status){
								case "not-decided":
									$status = "new";
									break;										
							}
							
							switch($row['approval_sought']){
								case "Final Approval":
									$approval_sought = "final";
									break;
								case "Interim Approval":
									$approval_sought = 'interim';
									break;
							}
							
							$old_status = strtolower($row['status']);
							
							$new_user_id = $this->get_new_uid(array('user_id' => $row['user_no']));	
							if($new_user_id === false){
								$new_user_id = "1";
							}//end if
							
							$origin = 'ireland';
							if($row['overseas'] == 1){
								$origin = 'elsewhere';	
							}//end if
							
							$insert = "INSERT INTO ads_submissions
									   (ad_submission_id, ad_submission_date, ad_submission_title, ad_submission_comments, ad_submission_file_details, 
									   ad_submission_ref, ad_submission_brand_id, ad_submission_brand_owner_id, ad_submission_campaign_id, ad_submission_company_id,
									   ad_submission_uid, ad_submission_ad_id, ad_submission_ad_medium_id, ad_submission_status, ad_submission_support,
									   ad_submission_codes, ad_submission_old_status, ad_submission_approval_sought, ad_submission_approval_code,
									   ad_submission_origin, ad_submission_ccci_feedback, ad_submission_user_feedback, ad_submission_old_email, 
									   ad_submission_old_phone, ad_submission_old_mobile, ad_submission_old_date_received, ad_submission_date_replied, ad_submission_temp) 
									   VALUES
									   (".$row['sub_no'].", DATE('".$row['date_created']."'), '".addslashes($row['sub_details'])."', '".addslashes($row['sub_comments'])."', '',
									    '".addslashes($row['our_ref'])."',".$row['brand_no'].", ".$row['advertiser_no'].", ".$row['campaign_no'].", ".$row['client_no'].", 
										".$new_user_id.", ".$row['ad_no'].", ".$row['medium_no'].",'".$status."', ".$row['support_material'].",
										'".addslashes($row['code_array'])."','".addslashes($old_status)."','".$approval_sought."','".addslashes($row['approval_no'])."',
										'".$origin."', '".addslashes($row['ccci_feedback'])."','".addslashes($row['user_feedback'])."','".addslashes($row['email_address'])."',
										'".addslashes($row['phone_no'])."','".addslashes($row['mobile_no'])."','".addslashes($row['date_received'])."','".addslashes($row['date_replied'])."', 0)";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));	
							if($errors === false){								
								$submission_id = $this->utils->db->lastInsertId();
								//now that we have the submission id, we need to create the attachment
								$filename = $row['file_name'];
								$url = $amazonPath.addslashes($row['file_path']);
								$mime = $tools->get_mime_from_filename($filename);
								if($row['link_only'] == 1){
									$url = addslashes($row['url_link']);
								}//end if
								$url = str_replace(" ","+",$url);
								$this->utils->attach_url(array(
									'url' => $url,
									'attachment_post_type' => 'submission',
									'post_id' => $submission_id,
									'module' => 13,//ads
									'mime' => $mime
								));
							}//end if
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function get_new_uid(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($user_id)){
				return false;	
			}//end if
			
			$query = "SELECT user_id FROM users WHERE user_old_id = ".$user_id." LIMIT 1";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);
					if($data !== false){
						return $data['user_id'];
					} else {
						return false;	
					}//end if
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_advertisers(){
			//read the brands
			$query = "SELECT * FROM advertiser WHERE advertiser_no >= 1";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];	
							$new_user_id = $this->get_new_uid(array('user_id' => $row['user_no']));	
							if($new_user_id === false){
								$new_user_id = "1";
							}//end if
							$insert = "INSERT INTO brands_owners
									   (brand_owner_id, brand_owner_name, brand_owner_uid, brand_owner_active, brand_owner_old_user_id) 
									   VALUES
									   (".$row['advertiser_no'].", '".addslashes($row['advertiser_name'])."', ".$new_user_id.", ".$row['advertiser_active'].", ".$row['user_no'].")";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));									
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_companies(){
			//read the companies
			$query = "SELECT * FROM client WHERE client_no >= 1";
			$result = $this->db2->query($query);		
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];
							$brand_owner = false;
							if($row['client_type'] == "Advertiser"){
								$brand_owner = true;	
							}//end if
							$brand_owner = (int)$brand_owner;				
							$insert = "INSERT INTO companies
									   (company_id, company_name, company_brand_owner, company_type_id, company_contact_name, 
									   company_contact_email, company_contact_phone) 
									   VALUES
									   (".$row['client_no'].", '".addslashes($row['client_name'])."', $brand_owner, ".$row['client_type_no'].", '".addslashes($row['contact_name'])."', '".addslashes($row['primary_email'])."', '".addslashes($row['client_phone_no'])."')";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));									
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function import_users(){	
			$users = $this->utils->call('users');
		
			$this->meta->set_meta_type('user');	
			$this->meta->set_meta_module('users');					
			//read the users
			$query = "SELECT * FROM user";
			$result = $this->db2->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$row = $data[$i];
							$password = sha1($row['password']);
							$insert = "INSERT INTO users 
									   (user_username, user_firstname, user_password, user_surname, user_active, user_email, user_type, user_old_id, user_company_id)
									   VALUES
									   (:username, :firstname, :password, :surname, :active, :email, :type, :old_id, :company_id)";
							$result = $this->utils->db->prepare($insert);
							$result->bindParam(':username', $row['email_address'], PDO::PARAM_STR);
							$result->bindParam(':firstname', $row['first_name'], PDO::PARAM_STR);
							$result->bindParam(':surname', $row['last_name'], PDO::PARAM_STR);
							$result->bindParam(':password', $password, PDO::PARAM_STR);
							$result->bindParam(':active', $row['activated'], PDO::PARAM_INT);
							$result->bindParam(':email', $row['email_address'], PDO::PARAM_STR);
							$result->bindParam(':type', $row['client_type'], PDO::PARAM_STR);
							$result->bindParam(':old_id', $row['user_no'], PDO::PARAM_INT);
							$result->bindParam(':company_id', $row['client_no'], PDO::PARAM_INT);
							$result->execute();
							$errors = $this->utils->error($result,__LINE__,get_class($this));		
							if($errors === false){			   
								//add non important fields on the meta table
								$user_id = $this->utils->db->lastInsertId();										
								$this->meta->set_meta_id($user_id);	
								$this->meta->add_meta(array('label' => 'meta_fullname','value' => $row['full_name']));
								$this->meta->add_meta(array('label' => 'meta_phone','value' => $row['phone_no']));
								$this->meta->add_meta(array('label' => 'meta_mobile','value' => $row['mobile_no']));
								$this->meta->add_meta(array('label' => 'meta_trained','value' => $row['trained']));
								if($row['initial_client_name'] != ""){
									$this->meta->add_meta(array('label' => 'meta_initial_name','value' => $row['initial_client_name']));
								}//end if
																
								$users->create_user_group_relation($user_id,4);
							}//end if
						}//end for i
						return true;
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function generate_brand_owner_rel(){
			$query = "SELECT ad_company_id,ad_brand_owner_id, ad_uid FROM ads WHERE ad_company_id > 0 GROUP BY ad_brand_id, ad_company_id";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$insert = "INSERT INTO brands_owners_rel
									   (brand_owner_rel_owner_id, brand_owner_rel_uid, brand_owner_rel_company_id)
									   VALUES
									   (".$data[$i]['ad_brand_owner_id'].",".$data[$i]['ad_uid'].",".$data[$i]['ad_company_id'].")";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));
						}//end for i
						return true;
					} else {
						return false;	
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function generate_brand_rel(){
			$query = "SELECT ad_company_id,ad_brand_id, ad_uid FROM ads WHERE ad_company_id > 0 GROUP BY ad_brand_id, ad_company_id";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$insert = "INSERT INTO brands_rel
									   (brand_rel_brand_id, brand_rel_uid, brand_rel_company_id)
									   VALUES
									   (".$data[$i]['ad_brand_id'].",".$data[$i]['ad_uid'].",".$data[$i]['ad_company_id'].")";
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));
						}//end for i
						return true;
					} else {
						return false;	
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
	}//end class
?>
		