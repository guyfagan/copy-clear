<?php
	class api_keys{
	
		private $utils;//utils class
		private $t;//prefix before each tablename
		private $api_key;
		private $key_id;//key id		
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;	
			$this->utils->read_params($this,$params);
		}//endconstructor
		
		public function generate($api_key_descr = "", $api_key_email = "", $api_key_domain = ""){
			if($_REQUEST['id'] == NULL){
				$this->api_key = $this->new_key();
				if($this->api_key !== false){
					//save the key
					$query = "INSERT INTO api_keys 
							  (api_key_sha1, api_key_descr, api_key_email, api_key_domain_allowed)
							  VALUES
							  ('".$this->api_key."','".$api_key_descr."','".$api_key_email."','".$api_key_domain."')";
					$result = $this->utils->db->query($query);
					$error = $this->utils->error($result,__LINE__,get_class($this));	
					if($error == false){
						return true;
					} else {
						return false;
					}//end if
				} else {
					return false;
				}//end if
			} else {
				$query = "UPDATE api_keys SET 
						  api_key_descr = '".$api_key_descr."',
						  api_key_email = '".$api_key_email."',
						  api_key_domain_allowed = '".$api_key_domain."'
						  WHERE api_key_id = ".$_REQUEST['id'];
				$result = $this->utils->db->query($query);
				$error = $this->utils->error($result,__LINE__,get_class($this));	
				if($error == false){
					return true;
				} else {
					return false;
				}//end if
			}//end if
		}//end function		
		
		private function new_key(){
			//generate a random key
			$clear_key = rand(1000000,9999999);
			$enc_key = sha1($clear_key);
			//now check if the key exists on the db
			$query = "SELECT * FROM api_keys WHERE api_key_sha1 = '".$enc_key."'";
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				//if the key exists, regenerate another until it finds a free key
				if($num != 0){
					return $this->new_key();
				} else {
					return $enc_key;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_key(){
			return $this->api_key;
		}//end function
		
		public function check_key($api_key){
			$server_name = $_SERVER["SERVER_NAME"];	
			
			//get the request uri
			$request_uri = $_SERVER['HTTP_REFERER'];
			if($request_uri != NULL){
				//clean from http://
				$request_uri = str_replace("http://","",$request_uri);
				//get rid of the subfolders
				$request_uri = explode("/",$request_uri);
				$request_uri = $request_uri[0];
				$server_name = $request_uri;
			}//end if
			
			//removes the www.
			$server_name = str_replace("www.","",$server_name);
			
			$query = "SELECT * FROM api_keys 
					  WHERE api_key_sha1 = '".$api_key."'
					  AND (api_key_domain_allowed = '*' OR api_key_domain_allowed LIKE '%".$server_name."%')";
					
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				if($num == 1){
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_keys(){
			$query = "SELECT * FROM api_keys ORDER BY api_key_id ASC";
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);				
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_api($key_id){
			$query = "SELECT * FROM api_keys WHERE api_key_id = ".$key_id;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);				
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function delete_key($key_id){
			$query = "DELETE FROM api_keys WHERE api_key_id = ".$key_id;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function print_result($data,$format = "json"){
			switch($format){
				case "json":
				default:
					$result = json_encode($data);
					break;
				case "text":
					$result = $data;
					break;
				case "xml":
					$result = $data;
					break;
			}//end switch
			
			return $result;
		}//end function
	}//end class
?>