<?php
	#################################################################
	# Legacy Class - last update 16/10/12							#
	#																#
	# This class contains old methods that are not used on the main #
	# ewrite class anymore, mostly old stuff or personalizations.   #	
	# Use it only if you need retrocompatibility with old projects  #
	#################################################################
	
	require_once('ewrite.class.php');

	class legacy_ewrite extends ewrite{
		
		public $show_addfields;
		public $filters;
		public $maintable;//to be completed
		public $field_prefix;//to be completed
		public $search_key;
		public $search_fields;
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->show_addfields = true;
			$this->filters = array();	
		}//endconstructor	
		
		
		public function search_posts($key,$params = NULL){		
			if($params != NULL && is_array($params)){
				foreach($params as $pkey => $value){
					${$pkey} = $value;
				}//end if
			}//end if
			
			
			if($struct == NULL){
				$struct = array();
				$struct['posts'] = array();
				$struct['posts']['id'] = "post_id";
				$struct['posts']['title'] = "post_title";
				$struct['posts']['message'] = "post_message";
				$struct['posts']['permalink'] = "post_permalink";
				$struct['posts']['module_id'] = "post_module_id";
				$struct['posts']['prefix'] = "post";
				$struct['posts']['enable_status'] = true;
				$struct['posts']['status'] = 1;
			} else {
				$struct = unserialize($struct);
			}//end if
				
			if($key == NULL){
				return false;
			}//end if
			
			$key = trim($key);
			
			//Build the queries
			$queries = array();
			$i = 0;
			foreach($struct as $table => $fields){
				$queries[$i] = "SELECT ";
				$fields_list = array();
				foreach($struct[$table] as $definition => $value){
					if($definition != "prefix" && $definition != "enable_status" && $definition != 'module_id' && $definition != 'id' && $definition != 'status'){
						array_push($fields_list,$value);
						$queries[$i] .= $value." AS ".$definition.",";
					} else if($definition == "id") {
						$id_field = $value." AS ".$definition;
					}//end if
				}
				//clean the last comma
				$queries[$i] = substr($queries[$i],0,-1);
				$queries[$i] .= ", MATCH(".implode(",",$fields_list).") AGAINST ('".$key."' IN BOOLEAN MODE) AS total";
				$queries[$i] .= ", ".$id_field;
				$queries[$i] .= ", categories.*, module_name AS folder, module_label"; 
				$queries[$i] .= " FROM (".$table.", modules";
				if($struct[$table]['prefix'] == "attachment"){		
					$queries[$i] .= ", posts";
				}//end if
				$queries[$i] .= ")
								 LEFT JOIN modules_items_order
								 ON (module_item_order_item_id = post_id AND module_item_order_type = 'post')
								 LEFT JOIN categories
								 ON (category_id = module_item_order_father_id)";				
				$queries[$i] .= " WHERE (".$struct[$table]['module_id']." = module_name OR ".$struct[$table]['module_id']." = module_id)";								 
				$queries[$i] .= " AND (";
				for($j = 0; $j < sizeof($fields_list); $j++){
					if($j > 0){
						$queries[$i] .= " OR ";
					}
					$queries[$i] .= " MATCH(".$fields_list[$j].") AGAINST ('".$key."' IN BOOLEAN MODE)";
				}//end for j
				$queries[$i] .= ")";

				if($struct[$table]['prefix'] == "attachment"){
					$queries[$i] .= " AND attachment_protected = 0 AND ".$module_table.".".$post_field." = ".$table.".attachment_ref_id";										
				}//end if
				
				if(isset($exclude_module)){
					if(is_array($exclude_module)){
						$exclude_module_list = implode(",",$exclude_module);
						$exclude_module_list = str_replace(",","','",$exclude_module_list);
						$exclude_module_list = "'".$exclude_module_list."'";
						$queries[$i] .= " AND module_name NOT IN(".$exclude_module_list.")";
					} else {
						$queries[$i] .= " AND module_name != ".$exclude_module;
					}//end if
				}//end if
				
				if($struct[$table]['enable_status'] != NULL && $struct[$table]['enable_status'] == true && $struct[$table]['status'] != NULL){
					$queries[$i] .= " AND ".$struct[$table]['prefix']."_status = ".$struct[$table]['status'];
				}//end if
					
				$i++;
			}//end foreach
		
			if(sizeof($queries) > 1){
				for($k = 0; $k < sizeof($queries); $k++){
					$queries[$k] = "(".$queries[$k].")";
				}//end for
				$query = implode(" UNION ",$queries);
			} else {
				$query = $queries[0];
			}
			
			$query .= " ORDER BY total DESC";
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if			
			//echo $query;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);				
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function clean_search(){
			$this->search_key = NULL;
			$this->search_fields = NULL;			
		}//end function				
				
		public function search_post($search_key, $search_fields = array('post_title','post_message')){			
			$this->search_key = $search_key;
			$this->search_fields = $search_fields;			
		}//end function
		
		public function process_search(){				
			if($this->search_key != NULL){
				$this->search_key = str_replace(" ","%",$this->search_key);
				$sql = " AND (";
				for($i = 0; $i < sizeof($this->search_fields); $i++){
					if($i > 0){
						$sql .= " OR ";
					}//end if
					$sql .= $this->search_fields[$i]." LIKE '%".$this->search_key."%'";
				}//end for
				$sql .= ") ";						
				return $sql;
			} else {
				return false;
			}//end if
		}//end function
		
		public function give_num_posts($user_id = NULL,$module = NULL){
			if(is_null($module)){
				$module = $this->module;
			}//end if
			$query = "SELECT COUNT(*) as posts_count
					  FROM (posts)					 			  
					  WHERE post_module_id = '".$module."'";
			if(!is_null($user_id) && is_numeric($user_id)){
				$query .= " AND post_uid = ".$user_id;
			}//end if			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$row = $result->fetch(PDO::FETCH_ASSOC);
					return $row['posts_count'];
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_search_score($key,$founds){
			$key = explode(" ",$key);
			$perc = intval($founds / sizeof($key) * 100) . "%"; 
			return $perc;
		}//end function
		
		//Function to be finished
		public function get_post_path(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if			
			
		}//end function
		
		private function find_file_thumbnail($path,$filename){
			$ext_pos = strrpos($filename,".");
			//Take out the extension from the original filename
			$filename = substr($filename,0,$ext_pos);		
			if(file_exists($path.$filename.".jpg")){
				return $filename.".jpg";
			}//end if
			if(file_exists($path.$filename.".gif")){
				return $filename.".gif";
			}//end if
			if(file_exists($path.$filename.".png")){
				return $filename.".png";
			}//end if
			if(file_exists($path.$filename.".jpeg")){
				return $filename.".jpeg";
			}//end if
			return false;			
		}//end function
		
		public function get_post_subpages($id){
			$query = "SELECT * FROM posts WHERE post_father_id = ".$id;
			$result = $this->utils->db->query($query);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = $result->rowCount();
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);						
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function delete_post_tag($pid,$tag){
			$data = $this->get_post($pid);
			$tags = $data['post_tags'];
			$tags = explode(",",$tags);
			$removed = false;
			for($i = 0; $i < sizeof($tags); $i++){
				if($tags[$i] == $tag){
					$removed = true;
					$tags[$i] = NULL;
				}//end if
			}//end for i
			$tags = implode(",",$tags);
			//clean for double commas if exists
			$tags = str_replace(",,",",",$tags);
			//check if exists a comma at the end of the string
			if(substr($tags,-1) == ","){
				$tags = substr($tags,0,-1);	
			}//end if
			//Update the post with the new taglist
			$this->utils->update_post_field($pid,"post_tags",$tags);
			
			//Delete the tag from the main tags table
			$this->utils->delete_tag($tag,$data['post_module']);
		}//end function
		
		public function show_additional_fields($show_addfields){
			if(is_bool($show_addfields)){
				$this->show_addfields = $show_addfields;
			}//end if
		}//end function		
		
		public function create_basic_module_folders($module){
			return false;
			if(is_numeric($module)){
				$module_data = $this->get_module($module);
				$module = $module_data['module_name'];
			}//end if
			if(!file_exists("../upload/".$module)){
				mkdir("../upload/".$module,0777);
				@chmod("../upload/".$module,0777);
			}//end if
			if(!file_exists("../upload/".$module."/files/")){
				mkdir("../upload/".$module."/files/",0777);
				@chmod("../upload/".$module."/files/",0777);
			}//end if
			if(!file_exists("../upload/".$module."/photos/")){
				mkdir("../upload/".$module."/photos/",0777);
				@chmod("../upload/".$module."/photos/",0777);
			}//end if
			//create the sub folders
			if(!file_exists("../upload/".$module."/photos/o/")){
				mkdir("../upload/".$module."/photos/o/",0777);
				@chmod("../upload/".$module."/photos/o/",0777);
			}//end if
			$img_formats = explode(",",__IMAGE_FORMATS_LIST__);
			for($k = 0; $k < sizeof($img_formats); $k++){
				if(!file_exists("../upload/".$module."/photos/".$img_formats[$k]."/")){
					mkdir("../upload/".$module."/photos/".$img_formats[$k]."/",0777);
					@chmod("../upload/".$module."/photos/".$img_formats[$k]."/",0777);
				}//end if
			}//end for
		}//end function		
		
		public function synch_attachments($pid, $old_module_id, $new_module_id){
			$module_data = $this->utils->get_module($old_module_id);
			$module_id = $module_data['module_id'];
			
			$new_module_data = $this->utils->get_module($new_module_id);
			$new_module_name = $new_module_data['module_name'];
			$new_module_id = $new_module_data['module_id'];
			//Check if the new module has the right folders
			$this->utils->create_basic_module_folders($new_module_name);
			//Move the files			
			$files = $this->utils->get_attachments($module_id,$pid,NULL,1);	
			if($files != false){
				$img_formats = explode(",",__IMAGE_FORMATS_LIST__);
				for($i = 0; $i < sizeof($files); $i++){
					$mime = $files[$i]['attachment_mime'];
					$filename = $files[$i]['attachment_filename'];
					if($this->utils->is_img($mime)){
						@chmod("../upload/".$old_module_id."/photos/o/".$filename,0777);
						@rename("../upload/".$old_module_id."/photos/o/".$filename,"../upload/".$new_module_name."/photos/o/".$filename);
						for($k = 0; $k < sizeof($img_formats); $k++){
							@chmod("../upload/".$old_module_id."/photos/".$img_formats[$k]."/".$filename,0777);
							@rename("../upload/".$old_module_id."/photos/".$img_formats[$k]."/".$filename,"../upload/".$new_module_name."/photos/".$img_formats[$k]."/".$filename);
						}//end for k
					} else {
						@chmod("../upload/".$old_module_id."/files/".$filename,0777);
						@rename("../upload/".$old_module_id."/files/".$filename,"../upload/".$new_module_name."/files/".$filename);
						//Copy the thumbnail if exists
						$thumb = $this->find_file_thumbnail("../upload/".$old_module_id."/files/t/",$filename);
						if($thumb != false){
							@chmod("upload/".$old_module_id."/files/t/".$thumb,0777);
							@rename("upload/".$old_module_id."/files/t/".$thumb,"../upload/".$new_module_name."/files/t/".$thumb);
						}//end if
					}//end if
				}//end for i 
			}//end if		
			$query = "UPDATE attachments SET attachment_ref_module_id = ".$new_module_id."
					  WHERE attachment_ref_module_id = ".$module_id."
					  AND attachment_ref_id = ".$pid;
			$result = $this->utils->db->query($query);
			$this->utils->error($result,__LINE__,get_class($this));	
		}//end function
		
		public function delete_post_module_item_relation($pid){
			$query = "DELETE FROM modules_items_order 
					  WHERE module_item_order_module_id = ".$this->module_id."
					  AND  module_item_order_type = 'post'
					  AND  module_item_order_item_id = ".$pid;
			$result = $this->utils->db->query($query);
			$errors == $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function		
		
		public function get_posts(){			
			$params = func_get_args();
			if(!is_array($params[0])){
				$cid = $params[0];				
				$sort_by = $params[1];
				
				$sort_order = $params[2];
				
				$status = $params[3];
				$deleted = $params[4];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $this->module_id;
				$module_name = $this->module;	
			}//end if
			
			//backward compatibility
			if(isset($userid)){
				$uid = $userid;	
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "post_date";
			}//end if
			
			if($sort_order == NULL){			
				$this->settings = $this->utils->get_settings($module_name);				
				$sort_order = $this->settings['list_order'];
			}//end if						
		
			$query = "SELECT ".$this->t."posts.*, ".$this->t."users.user_username, ".$this->t."users.user_id, UNIX_TIMESTAMP(".$this->t."posts.post_date) AS post_date, UNIX_TIMESTAMP(".$this->t."posts.post_date_end) AS post_date_end, module_name AS folder";			
			if($cid != NULL){
				$query .= ", ".$this->t."categories.*, ".$this->t."categories_rel.*";
			}//end if
			$query .= " FROM (".$this->t."posts, ".$this->t."modules";
			if($use_system_order == true){
				$query .= ", ".$this->t."modules_items_order";
			}//end if
			$query .= ")
					  LEFT JOIN ".$this->t."users
					  ON (".$this->t."users.user_id = ".$this->t."posts.post_uid) ";
			if($cid != NULL){
				$query .= "LEFT JOIN ".$this->t."categories_rel
					  ON ".$this->t."categories_rel.categories_rel_post_id = ".$this->t."posts.post_id
					  LEFT JOIN ".$this->t."categories
					  ON (".$this->t."categories.category_id = ".$this->t."categories_rel.categories_rel_category_id) ";
			}//end if
			$query .= "WHERE post_module_id = module_name
					   AND post_temp = 0";
			if(!isset($all_modules)){
				if($cid != NULL){
					$query .= " AND category_module_id = ".$module_id."";
				} else {
					$query .= " AND post_module_id = '".$module_name."'";		
				}//end if	
			}//end if  
			/*
			if($module_group_id != NULL && is_numeric($module_group_id)){
				$query .= "";
			}//end if
			*/
			if($use_system_order == true){
				$query .= " AND module_item_order_type = 'post'
							AND module_item_order_item_id = post_id
							AND module_item_order_module_id = module_id";
			}//end if
			
			if($month != NULL && is_numeric($month)){
				$query .= " AND MONTH(post_date) = ".$month;
			}//end if
			
			if($year != NULL && is_numeric($year)){
				$query .= " AND YEAR(post_date) = ".$year;
			}//end if
			
			if($day != NULL && is_numeric($day)){
				$query .= " AND DAY(post_date) = ".$day;
			}//end if
			
			if($uid != NULL && isset($uid)){
				$query .= " AND user_id = ".$uid;
			}//end if
			if($tag != NULL && is_string($tag)){
				$query .= " AND post_tags LIKE '%".$tag."%'";
			}//end if		
			if($this->search_key != NULL){
				$sql = $this->process_search();
				if($sql != false){
					$query .= $sql;					
				}//endif
			}//end if		
			//check the status of the posts
			if(isset($status) && (is_bool($status) || is_numeric($status))){
				$query .= " AND post_status = ".(int)$status;
			}//end if	
			if(isset($temp) && (is_numeric($temp) || is_bool($temp))){
				$query .= " AND post_temp = ".(int)$temp;
			}//end if	
			/*if($cid != NULL && $status != NULL && is_numeric($status)){
				$query .= " AND category_status = ".$status;
			}//end if*/
			//check if the posts is deleted or not	
			if(isset($deleted) && (is_numeric($deleted) || is_bool($deleted))){
				$query .= " WHERE post_deleted = ".(int)$deleted;
			}//end if
			
			if($cid != NULL){
				$query .= " AND (category_id = ".$cid;
				if(isset($show_subelements) && $show_subelements == true){
					$query .= " OR category_father_id = ".$cid;
				}//end if
				$query .= ")";
			}//end if
			
			if(sizeof($this->filters) > 0){
				$query .= $this->process_filters(false);
			}//end if
			
			if($use_system_order == NULL || $use_system_order == false){
				$query .= " GROUP BY ".$this->t."posts.post_id						
						    ORDER BY ".$sort_by." ".$sort_order;			
			} else {
				$query .= " GROUP BY ".$this->t."posts.post_id						
					    	ORDER BY module_item_order_order ".$sort_order;	
			}					
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if			
						
			$result = @mysql_query($query,$this->utils->db);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = @mysql_num_rows($result);
	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);				
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function filter_by_uid($uid){
			$this->uid = $uid;
			$this->filters['user_id'] = $uid;
		}//end function
		
		public function set_filter($key,$value){
			if(is_numeric($value)){
				$this->filters[$key] = $value;
			} else if($value != NULL){
				$this->filters[$key] = "'".$value."'";
			} else {
				$this->filters[$key] = NULL;
			}//end if
		}//end function
		
		public function process_filters($init_where = false){
			$c = 0;
			$sql = "";
			foreach($this->filters as $key => $value){
				if($c == 0 && $init_where == true){
					$sql .= "WHERE ";
				} else {
					$sql .= " AND ";
				}//end if
				if($value != NULL && $value != ""){
					$sql .= $key." = ".$value;
				} else {
					$sql .= $key." IS NULL";
				}//end if
			}//end foreach
			return $sql;
		}//end function
		
		public function get_post_images($pid,$exclude_def_img = true){
			$data = $this->utils->get_attachments($this->module,$pid,1,1,$exclude_def_img, false);
			return $data;
		}//end function
		
		public function get_post_attacched_items($pid){
			$data = $this->utils->get_attachments($this->module,$pid,NULL,"0");
			return $data;
		}//end functino
		
		public function get_post_files($pid){
			$data = $this->utils->get_attachments($this->module,$pid,"0",1);
			return $data;
		}//end function
		
		public function get_post_files_by_title($pid,$title,$is_img = NULL){		
			if(is_bool($is_img)){
				if($is_img == true){
					$is_img = "1";
				} else {
					$is_img = "0";
				}//end if
			}//end if
			$query = "SELECT * FROM ".$this->t."attachments, ".$this->t."modules
					  WHERE module_id = attachment_ref_module_id
					  AND attachment_ref_id = ".$pid."
					  AND attachment_title LIKE '%".$title."%'";	
			if($ref_id != NULL && $is_img != NULL){
				$query .= " AND attachment_is_img = ".$is_img;
			}//end if	
		
			$query .= " ORDER BY attachment_order ASC";
	
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){		
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function clone_post(){	
			$params = func_get_args();
			if(!is_array($params[0])){
				$oID = $params[0];				
				$newCatID = $params[1];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
				if($id != NULL){
					$oID = $id;
				}//end if
				if($newcategory != NULL){
					$newCatID = $newcategory;
				} else {
					$newCatID = NULL;
				}
			}//end if
			/* STEPS TO DO
			*	1. clone the post	
			*	2. clone the attachments
			*	3. assign the new categories
			*/		
			//First clone the post record
			$query = "SELECT * FROM ".$this->t."posts WHERE post_id = ".$oID;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$table_structure = @mysql_num_fields($result);	
			$row = @mysql_fetch_array($result);	
			//Read the structure and create the insert query			
			$fields = array();
			$values = array();
			for($j = 0; $j < $table_structure; $j++){
				$type = mysql_field_type($result,$j);
				$name = mysql_field_name($result,$j);
				if($name != "post_date" &&$name != "post_id" && $row[$name] != NULL){
					array_push($fields,$name);
					if($type != "int"){
						array_push($values,"'".$row[$name]."'");
					} else {
						array_push($values,$row[$name]);
					}//end if
				}//end if
			}//end for
			//Insert the new cloned post
			$query = "INSERT INTO ".$this->t."posts
					  (".implode(",",$fields).")
					  VALUES
					  (".implode(",",$values).")";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$this->pid = @mysql_insert_id();
			
			//If a different language is set, update the post data
			if($language != NULL){
				$this->utils->update_post_field($this->pid,"post_lang",$language);
			}//end if
			
			//Clone the attachments
			$post_files = $this->utils->get_attachments($this->module_id,$oID);
		
			$cloned_post_files = array();
			for($i = 0; $i < sizeof($post_files); $i++){
				array_push($cloned_post_files,$post_files[$i]['attachment_id']);
			}//end for
			$this->utils->clone_attachments($cloned_post_files,$this->pid);
			//Create or clone categories relations
			if($newCatID != NULL){
				//Create the new categories associations
				$this->create_categories_relation($this->pid,$newCatID);
			} else {
				//If no categories are passed, copy from the original post
				$categories = $this->get_post_categories($oID);
				$newCatID = array();
				for($i = 0; $i < sizeof($categories); $i++){
					array_push($newCatID,$categories[$i]['category_id']);
				}//end for
				$this->create_categories_relation($this->pid,$newCatID);
			}//end if
		}//end function
		
		public function update_post_dependencies($id = NULL,$type = "post"){
			//Post update
			if($type == "post"){
				if($id == NULL){
					$id = $this->pid;
				}//end if
				$post_data = $this->get_post($id);			
				$module_id = $post_data['post_module_id'];
				$subpages = $this->get_post_subpages($id);
				//all read the subpages for this post
				if($subpages != false){
					//if exists, update all the module id
					for($i = 0; $i < sizeof($subpages); $i++){
						$this->utils->update_post_field($id,"post_module_id",$module_id);
						$this->synch_attachments($subpages[$i]['post_id'],$subpages[$i]['post_module_id'],$module_id);
					}//end for
				}//end if
			}//end if
			
			//Post category update
			
			if($type == "category"){
				if($id == NULL){
					$id = $this->cid;
				}//end if
				$cat_data = $this->get_category($id);		
				if($_REQUEST['module_id'] == NULL){	
					$module_id = $cat_data['category_module_id'];
				} else {
					$module_id = $_REQUEST['module_id'];
				}//end if
				$module_data = $this->utils->get_module($module_id);
				$module_name = $module_data['module_name'];
				//Check for the correlated posts
				$params = array("cid" => $id);
				$posts = $this->get_posts($params);				
				//For each post I update the module id
				
				if($posts != false){
					for($j = 0; $j < sizeof($posts); $j++){
						//$this->utils->update_post_field($posts[$j]['post_id'],"post_module_id",$module_name);
						$this->synch_attachments($posts[$j]['post_id'],$posts[$j]['post_module_id'],$module_id);
					}//end for
				}//end if	
				
				$this->set_category_father_id($id);
				$subcats = $this->get_categories();
				//check if exists at least one sub category
				if($subcats != false){
					//Update the module of the selected category
					$query = "UPDATE ".$this->t."categories SET category_module_id = ".$module_id." WHERE category_father_id = ".$id;
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));
					//Now update all the elements under this category
					for($i = 0; $i < sizeof($subcats); $i++){						
						$cid = $subcats[$i]['category_id'];		
						$params = array("cid" => $cid);
						$posts = $this->get_posts($params);
						
						//For each post I update the module id
						if($posts != false){
							for($j = 0; $j < sizeof($posts); $j++){
								//$this->utils->update_post_field($posts[$j]['post_id'],"post_module_id",$module_name);
								$this->synch_attachments($posts[$j]['post_id'],$posts[$j]['post_module_id'],$module_id);
							}//end for
						}//end if			
					}//end for
				}//end if
				
			}//end if
			
			//Post category update
			if($type == "group"){
				if($id == NULL){
					$id = $this->gid;
				}//end if
				$group_data = $this->get_group($id);					
				$module_id = $group_data['group_module_id'];	
				$module_data = $this->utils->get_module($module_id);
				$module_name = $module_data['module_name'];			
				//Check for the main categories
				$cats = $this->get_categories(true,"category_id","ASC",$id);
				//if the group has categories			
				if($cats != false){
					$query = "UPDATE ".$this->t."categories SET category_module_id = ".$module_id." WHERE category_gid = ".$id;
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));
					for($k = 0; $k < sizeof($cats); $k++){
						//Update the posts for this category
						$params = array("cid" => $cats[$k]['category_id']);
						$catposts = $this->get_posts($params);
						//For each post I update the module id
						if($catposts != false){
							for($j = 0; $j < sizeof($catposts); $j++){
								$this->utils->update_post_field($catposts[$j]['post_id'],"post_module_id",$module_name);
								$this->synch_attachments($catposts[$j]['post_id'],$catposts[$j]['post_module_id'],$module_id);
							}//end for
						}//end if	
						
						//Now do the same for the subcategories
						$this->set_category_father_id($cats[$k]['category_id']);
						$subcats = $this->get_categories(false,"category_id","ASC",$id,$module_id);												
						//check if exists at least one sub category
						if($subcats != false){
							//Update the module of the selected category
							$query = "UPDATE ".$this->t."categories SET category_module_id = ".$module_id." WHERE category_father_id = ".$cats[$k]['category_id'];					
							$result = @mysql_query($query,$this->utils->db);
							$this->utils->error($result,__LINE__,get_class($this));
							//Now update all the elements under this category
							for($i = 0; $i < sizeof($subcats); $i++){						
								$cid = $subcats[$i]['category_id'];		
								$params = array("cid" => $cid);
								$posts = $this->get_posts($params);							
								//For each post I update the module id
								if($posts != false){
									for($j = 0; $j < sizeof($posts); $j++){
										$this->utils->update_post_field($posts[$j]['post_id'],"post_module_id",$module_name);
										$this->synch_attachments($posts[$j]['post_id'],$posts[$j]['post_module_id'],$module_id);
									}//end for
								}//end if			
							}//end for
						}//end if
					}//end for
				}//end if
			}//end if
		}//end function
		
		public function get_users_posts($sort_by = "post_date", $sort_order = "DESC",$multiply_per_category = false){
			$query = "SELECT ".$this->t."posts.* , COUNT(post_id) AS posts_found, ".$this->t."users.user_username, ".$this->t."users.user_id";
			if($multiply_per_category == false){
				$query .= " FROM (".$this->t."posts)";
			} else {
				$query .= " FROM (".$this->t."posts, ".$this->t."categories_rel)";
			}//end if
			$query .= " LEFT JOIN ".$this->t."users
					  ON (".$this->t."users.user_id = ".$this->t."posts.post_uid)
					  WHERE post_module_id = '".$this->module."' AND post_temp = 0";
			if($multiply_per_category == true){		  
				$query .= " AND categories_rel_post_id = post_id";
			}//end if
			$query .= " GROUP BY post_uid
					  ORDER BY ".$sort_by." ".$sort_order;	
		
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);					
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function get_parent_category($cid){
			$child = $this->get_category($cid);
			$parent_id = $child['category_father_id'];
			if($parent_id != NULL){
				$data = $this->get_category($parent_id);				
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_uncategorized_posts(){
			$params = func_get_args();
			foreach($params[0] as $key => $value){
				${$key} = $value;
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "post_date";
			}//end if
			if($sort_order == NULL){
				$sort_order = "DESC";
			}//end if			
			
			$query = "SELECT ".$this->t."posts.*, ".$this->t."users.user_username, ".$this->t."users.user_id, UNIX_TIMESTAMP(".$this->t."posts.post_date) AS post_date, UNIX_TIMESTAMP(".$this->t."posts.post_date_end) AS post_date_end
					  FROM ".$this->t."posts
					  LEFT JOIN ".$this->t."users
					  ON (".$this->t."users.user_id = ".$this->t."posts.post_uid) 
					  LEFT JOIN ".$this->t."categories_rel 
					  ON post_id = categories_rel_post_id
					  WHERE categories_rel_id IS NULL
				      AND post_module_id = '".$this->module."'";
			$query .= " GROUP BY ".$this->t."posts.post_id						
						ORDER BY ".$sort_by." ".$sort_order;					
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);					
				return $data;
			} else {
				return false;
			}//endif
		}//end function		
		
		public function get_groups(){
			$module_id = $this->utils->get_module($this->module);
			$module_id = $module_id['module_id'];
			$query = "SELECT * FROM ".$this->t."groups WHERE group_module_id = ".$module_id;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){			
				$data = $this->utils->get_result_array($result,false);			
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_group($gid){
			$module_id = $this->utils->get_module($this->module);
			$module_id = $module_id['module_id'];
			$query = "SELECT * FROM ".$this->t."groups WHERE group_id = ".$gid;		
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){			
				$data = $this->utils->get_result_array($result,true);			
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function new_group(){
			$module_id = $this->utils->get_module($this->module);
			$module_id = $module_id['module_id'];
			//$_POST['module_id'] = $module_id;
			//These fields will be omitted 
			$hidden_fields = array('action','submit','id');
			if($_REQUEST['id'] == NULL){
				//create the insert sql script
				$sql = $this->utils->build_sql_fields("group","INSERT","POST",$hidden_fields);			
				$query = "INSERT INTO ".$this->t."groups ".$sql;		
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
			} else {
				//create the insert sql script
				$sql = $this->utils->build_sql_fields("group","UPDATE","POST",$hidden_fields);			
				$query = "UPDATE ".$this->t."groups SET ".$sql." WHERE group_id = ".$_REQUEST['id'];			
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));				
				$this->update_post_dependencies($_REQUEST['id'],"group");
			}//end if
			return true;
		}//end function
		
		public function delete_group($gid){
			//Delete the group
			$query = "DELETE FROM ".$this->t."groups WHERE group_id = ".$gid;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));
		}//end function
		
		///////////////////////////////
		// START MOVE CAT / POST METHODS by Séon Gleeson
		
		//Give the list of the categories for this module
		public function get_categories_by_module($mid){
			$query = "SELECT category_id, category_name, module_name FROM ".$this->t."categories 
							LEFT JOIN modules ON (module_id = category_module_id)";
			if(is_numeric($mid)){
				$query .= " WHERE category_module_id = ".$mid;
			} else {
				$query .= " WHERE module_name = '".$mid."'";
			}//end if
			$query .= " AND category_father_id = 0 ";
			$query .= " ORDER BY category_name ASC";		
			$result = @mysql_query($query,$this->utils->db);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error !== false){
				$num = @mysql_num_rows($result);
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function

		//Give the list of the subcategories for this category
		public function get_categories_by_parent($cid){
			if($cid != NULL){
				$query = "SELECT category_id, category_name FROM ".$this->t."categories 
						  WHERE category_father_id = ".$cid." 
						  ORDER BY category_name ASC";						  
				$result = @mysql_query($query,$this->utils->db);
				$error = $this->utils->error($result,__LINE__,get_class($this));	
				if($error !== false){
					$num = @mysql_num_rows($result);
					if($num > 0){
						$data = $this->utils->get_result_array($result,false);
						return $data;
					} else {
						return false;	
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		//Give the list of the posts for this category
		public function get_posts_by_parent($cid){
			if($cid != NULL){
				$query = "SELECT post_id, post_title FROM ".$this->t."categories 
						  LEFT JOIN categories_rel ON (categories_rel_category_id = category_id)
						  LEFT JOIN posts ON (categories_rel_post_id = post_id)
						  WHERE categories_rel_category_id = ".$cid."";	
				$result = @mysql_query($query,$this->utils->db);
				$error = $this->utils->error($result,__LINE__,get_class($this));	
				if($error !== false){
					$num = @mysql_num_rows($result);
					if($num > 0){
						$data = $this->utils->get_result_array($result,false);
						return $data;
					} else {
						return false;	
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function	
		
		//Move the post to new category and/or module
		public function move_post($pid,$cid,$mid=false){
			$modmove = false;		
			$query = "SELECT post_module_id FROM posts 
					  WHERE post_id = ".$pid;
			$result = @mysql_query($query,$this->utils->db);
			
			if($this->utils->error($result,__LINE__,get_class($this))){
				return false;
			} else {
			
				$data = $this->utils->get_result_array($result,false);
				
				
				$cur_module = $data[0]['post_module_id'];
	
				if(is_numeric($mid)){
					$module_data = $this->utils->get_module($mid);
					$module_name = $module_data['module_name'];
					$query = "UPDATE ".$this->t."posts
							  SET post_module_id = '".$module_name."'
							  WHERE post_id = ".$pid."";
					if($module_name != $cur_module){$modmove=true;}
				
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));
				
				}// end if
				
				if(is_numeric($cid)){
					$query = "UPDATE ".$this->t."modules_items_order
							  SET module_item_order_module_id = '".$mid."'
							  , module_item_order_father_id = '".$cid."'
							  WHERE module_item_order_item_id = ".$pid."";
				
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));	
				
				}// end if
				
				if(is_numeric($cid)){
					//check if there is any relationship on the category_rel
					$query = "SELECT * FROM ".$this->t."categories_rel WHERE categories_rel_post_id = ".$pid;
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));
					$num = @mysql_num_rows($result);
					if($num > 0){
						$query = "UPDATE ".$this->t."categories_rel
								  SET categories_rel_category_id = '".$cid."'
								  WHERE categories_rel_post_id = ".$pid;
					} else {
						$query = "INSERT INTO ".$this->t."categories_rel
								  (categories_rel_category_id, categories_rel_post_id)
								  VALUES
								  (".$cid.",".$pid.")";
					}//end if
	
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));
				
				}// end if
				
				if($modmove){
					$this->synch_attachments($pid,$cur_module,$mid);
					$this->move_post_sidebars($pid,$mid);
					$this->move_post_comments($pid,$mid);
				}// end if
				
				return true;
			}

		}//end function

		public function move_post_sidebars($pid,$mid){
				$query = "UPDATE sidebars SET
						sidebar_module_id = '".$mid."'
			 			WHERE sidebar_post_id = ".$pid."
					 ";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));		
		}//end function		
		
		public function move_post_comments($pid,$mid){		
				$query = "UPDATE comments SET
						comment_module_id = '".$mid."'
			 			WHERE comment_post_id = ".$pid."
					 ";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
		}//end function
		
		public function move_category($id, $cid = "0", $mid = false){	

			$query = "UPDATE categories SET
					  category_father_id = '".$cid."'";
			if($mid != false){
				$query .=", category_module_id = '".$mid."'";
			}//end if
			$query .= " WHERE category_id = ".$id."";
			
			$result = @mysql_query($query,$this->utils->db);
			
			if($this->utils->error($result,__LINE__,get_class($this)) === true){
				return false;
			} else if ($id != $cid) {			
		
				$query = "UPDATE ".$this->t."modules_items_order
						  SET module_item_order_father_id = ".$cid;
				if($mid != false){
					$query .=", module_item_order_module_id = ".$mid;
				}//end if	  
				$query .= " WHERE module_item_order_item_id = ".$id." AND module_item_order_type = 'category'";
				//echo $query;
				
				$result = mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));		
			
				
				// get all subcategories
				$subcats = $this->get_categories_by_parent($id);
				
				if($subcats!=false){
					for($si=0;$si<sizeof($subcats);$si++){
						$this->move_category($subcats[$si]['category_id'],false,$mid);
					}
				}// end if
				
				$posts = $this->get_posts_by_parent($id);
				
				if($posts!=false){
					for($pi=0;$pi<sizeof($posts);$pi++){
						$this->move_post($posts[$pi]['post_id'],$id,$mid);
					}
				}// end if
				
				return true;		
				
			} else {
			
				return false;
				
				echo "<p>You can't move a Category to itself</p>";
			
			}// end if
		
		}
		// END MOVE CAT / POST METHODS
		////////////////////////////////////
	}//end class