<?php
	#################################################################
	# View Class - 	last update 04/01/13							#
	#																#
	# This is a core class for the Old Hat CMS						#	
	#																#
	# 11/12/12 - update the build_page method to accept action but	#
	#			 no module as well as the other options				#
	# 24/12/12 - improved the breadcrumbs functionality				#
	# 04/01/13 - fixed bug on the breadcrumb						#
	#################################################################

	class view{
		//Variables
		public $utils;					
		public $action;//Action variable
		public $module;//module
		public $config;
		public $settings;
		public $module_settings;
		public $subnav;
		public $errors;
		public $page;
		public $dev_mode = false;
		protected $module_data;
		protected $default_action;
		public $user_permissions;
		
		//constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->module = $_REQUEST['m'];
			$this->action = $_REQUEST['a'];	
			$this->get_default_action();	
			$this->init_settings();		
			if(defined('__DEFAULT_ACTION__')){
				$this->default_action = __DEFAULT_ACTION__;
			} else {
				$this->default_action = 'generic';
			}//end if		
		}//end function	
		
		public function set_module($module){
			$this->module = $module;
			$this->utils->set_module($module);	
			$this->module_settings = $this->utils->get_settings(array("module_id" =>$module));	
			$this->get_default_action();
		}//end function	
		
		public function load_module_config(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($module)){
				$this->module = $module;
			}//end if
			
			if(is_null($this->module)){
				return false;	
			}//end if
			
			if(!isset($path)){
				$path = "";	
			}
									
			if(file_exists($path."modules/".$this->module."/config.php")){				
				require_once($path."modules/".$this->module."/config.php");
			} else if(file_exists($path."modules/".$this->default_action."/config.php")){				
				require_once($path."modules/".$this->default_action."/config.php");
			} else if(file_exists($path."modules/generic/config.php")){				
				require_once($path."modules/generic/config.php");
			}//end if
	
			if(isset($config)){
				$this->config = $config;	
			}//end if
			return true;
		}//end function	
		
		public function set_user_permissions($permissions){
			$this->user_permissions = $permissions;
		}//end function
		
		protected function get_default_action(){
			if(file_exists("modules/".$this->module."/config.php")){				
				require_once("modules/".$this->module."/config.php");
			} else if(file_exists("modules/".$this->default_action."/config.php")){				
				require_once("modules/".$this->default_action."/config.php");
			} else if(file_exists("modules/generic/config.php")){				
				require_once("modules/generic/config.php");
			}//end if
			
			if(isset($config)){
				$this->config = $config;	
			}//end if
						
			if(isset($this->config['alias'])){
				$this->default_action = $this->config['alias'];
			}//end if
								
			if(!is_null($this->module) && is_null($this->action)){
				if(isset($config) && !is_null($config['default_action'])){
					$this->action = $config['default_action'];				
				} else {
					$this->action = "list";
				}//end if
			}//end if				
			//also we store the module data	
			if(isset($this->module)){
				$this->module_data = $this->utils->get_module($this->module);	
			}//end if
			
			if(isset($config['subnav'])){
				$this->subnav[$this->module_data['module_name']] = $config['subnav'];
			}//end if
			
		}//end function
		
		public function build_page(){
			//echo $this->module." - ".$this->action;
			//force all actions containing .edit to redirect to .new
			if(strpos($this->action,".edit") !== false){
				$this->action = str_replace(".edit",".new",$this->action);	
			}//end if
			if(file_exists("modules/".$this->module."/".$this->action.".php") && !is_null($this->module)){				
				return "modules/".$this->module."/".$this->action.".php";
			} else if(!is_null($this->module) && !is_null($this->action)){
				if(file_exists("modules/".$this->default_action."/".$this->action.".php")){					
					return "modules/".$this->default_action."/".$this->action.".php";
				} else if(file_exists("modules/generic/".$this->action.".php")){					
					return "modules/generic/".$this->action.".php";
				} else {
					return false;
				}//end if
			} else if(is_null($this->module) && !is_null($this->action)){
				if(file_exists("modules/main/".$this->action.".php")){
					return "modules/main/".$this->action.".php";
				} else {
					return false;
				}//end if
			} else if(is_null($this->module) && is_null($this->action)){
				//if nothing is passed we fallback to the dashboard
				if(file_exists("modules/main/dashboard.php")){
					return "modules/main/dashboard.php";
				} else {
					return false;
				}//end if
			}//end if			 
		}//end function
		
		public function autoload_js(){
			if(file_exists("modules/".$this->module."/autoload.php")){				
				@require_once("modules/".$this->module."/autoload.php");
			} else if(file_exists("modules/".$this->default_action."/autoload.php") && !is_null($this->module)){
				@require_once("modules/".$this->default_action."/autoload.php");
			} else if(file_exists("modules/generic/autoload.php") && !is_null($this->module)){
				@require_once("modules/generic/autoload.php");
			} else if(file_exists("modules/main/autoload.php") && is_null($this->module)){
				@require_once("modules/main/autoload.php");
			}//end if
			if(isset($js_libs_autoload)){
				$this->load_js_libraries($js_libs_autoload);
			} else {
				$this->load_js_libraries();
			}//end if
		}//end function
		
		public function autolaod_css(){
			
		}//end function
		
		protected function load_js_libraries($array_libraries = NULL){
			//check if for the current action is specified one or more libraries to load				
			if(is_array($array_libraries)){
				if(isset($array_libraries[$this->action])){
					if(is_array($array_libraries[$this->action])){
						foreach($array_libraries[$this->action] as $value){
							if(!is_array($value)){
								if(strpos($value,"ckeditor") === false){
									echo "\n<script src=\"".__CMS_PATH__.$value."?v=".__CMS_VERSION__."\"></script>";
								} else {
									echo "\n<script src=\"".__CMS_PATH__.$value."\"></script>";
								}//end if
							} else {
								switch($value['type']){
									case "css":
										echo "\n<link rel=\"stylesheet\" type=\"text/css\" href=\"".__CMS_PATH__.$value['path']."?v=".__CMS_VERSION__."\" />";
										break;
									case "js":
										if(strpos($value['path'],"ckeditor") === false){
											echo "\n<script src=\"".__CMS_PATH__.$value['path']."?v=".__CMS_VERSION__."\"";
										} else {
											echo "\n<script src=\"".__CMS_PATH__.$value['path']."\"";
										}//end if
										if(isset($value['charset'])){
											echo ' charset="'.$value['charset'].'"';	
										}//end if
										echo "></script>";
										break;	
									case "ext-js":										
										echo "\n<script src=\"".$value['path']."\"";										
										if(isset($value['charset'])){
											echo ' charset="'.$value['charset'].'"';	
										}//end if
										echo "></script>";
										break;	
								}//end switch
							}//end if
							
						}//endforeach
					}//end if
				}//end if
			}//end if
			
			//try to find the behaviours			
			if(file_exists("modules/".$this->module."/js/behaviours.js")){				
				echo '<script src="'.__CMS_PATH__.'modules/'.$this->module.'/js/behaviours.js?v='.__CMS_VERSION__.'"></script>';
			} else if(file_exists("modules/".$this->default_action."/js/behaviours.js") && !is_null($this->module)){
				echo '<script src="'.__CMS_PATH__.'modules/'.$this->default_action.'/js/behaviours.js?v='.__CMS_VERSION__.'"></script>';
			} else if(file_exists("modules/generic/js/behaviours.js") && !is_null($this->module)){
				echo '<script src="'.__CMS_PATH__.'modules/generic/js/behaviours.js?v='.__CMS_VERSION__.'"></script>';
			} else if(file_exists("modules/main/js/behaviours.js") && is_null($this->module)){
				echo '<script src="'.__CMS_PATH__.'modules/main/js/behaviours.js?v='.__CMS_VERSION__.'"></script>';
			}//end if
		}//end function
		
		#########################################
		# SEARCH	 							#
		#########################################
		
		public function get_search(){
			require_once("inc/searchform.inc.php");
		}//end function
		
		#########################################
		# BREADCRUMBS 							#
		#########################################
		
		public function get_breadcrumbs(){
			$html = '<ul class="breadcrumb">
   					 <li><a href="'.__CMS_PATH__.'dashboard/">Home</a> <span class="divider">/</span></li>';				
			//Add the module to the breadcrumb			
			if(!is_null($this->module)){
				if(is_null($this->action)){
					$html .= '<li class="active">'.$this->module_data['module_label'].'</li>';    	
				} else if(count($this->module_data) > 1){
					if(!is_null($this->action)){
						$html .= '<li><a href="'.__CMS_PATH__.$this->module_data['module_name'].'/'.$this->config['default_action'].'/">'.$this->module_data['module_label'].'</a> <span class="divider">/</span></li>';
					} else {
						$html .= '<li><a href="'.__CMS_PATH__.$this->module_data['module_name'].'/">'.$this->module_data['module_label'].'</a> <span class="divider">/</span></li>';
					}
				}//end if
			}//end if
			//check if we have custom code for the breadcrumb
			if(!isset($this->config['breadcrumb'][$this->action])){
				if(isset($_REQUEST['parent_id']) && !isset($_REQUEST['id'])){							
					if($this->action == "list"){					
						$categories = $this->get_directions(array("category_id" => $_REQUEST['parent_id']));
					} else {						
						$categories = $this->get_directions(array("category_id" => $_REQUEST['parent_id']));
					}//end if
					if($categories !== false && sizeof($categories) > 0){
						for($i = (sizeof($categories)-1); $i >= 0; $i--){
							$html .= '<li><a href="'.__CMS_PATH__.$this->module_data['module_name'].'/list/'.$categories[$i]['category_id'].'/">'.$categories[$i]['category_name'].'</a> <span class="divider">/</span></li>';
						}//end for
					}//end if
				}//end if
							
				if(isset($_REQUEST['id'])){					
					if($this->action == "list"){						
						$categories = $this->get_directions(array("category_id" => $_REQUEST['id']));											
					} else if(strpos($this->action,'categor') !== false){
						$categories = $this->get_directions(array('category_id' => $_REQUEST['id'],'is_category' => true));	
					} else {
						$categories = $this->get_directions(array("id" => $_REQUEST['id']));
					}//end if
					if($categories !== false && sizeof($categories) > 0){
						for($i = (sizeof($categories)-1); $i >= 0; $i--){
							$html .= '<li><a href="'.__CMS_PATH__.$this->module_data['module_name'].'/list/'.$categories[$i]['category_id'].'/">'.$categories[$i]['category_name'].'</a> <span class="divider">/</span></li>';
						}//end for
					}//end if
				}//end if
			} else {
				$obj_name = $this->config['breadcrumb'][$this->action]['class'];
				$method_name = $this->config['breadcrumb'][$this->action]['method'];
				if(!isset($this->config['breadcrumb'][$this->action]['options'])){
					$html .= $obj_name->$method_name();
				} else {
					$html .= $obj_name->$method_name($this->config['breadcrumb'][$this->action]['options']);
				}
			}
			if(!is_null($this->action)){
				$action_path = str_replace(".","/",$this->action);
				$action_label = explode(".",$this->action);
				$action_label = ucwords($action_label[1]." ".$action_label[0]);
				if(isset($_REQUEST['id']) || isset($_REQUEST['category_id'])){
					//$html .= '<li><a href="'.__CMS_PATH__.$this->module_data['module_name'].'/'.$action_path.'/">'.$action_label.'</a> <span class="divider">/</span></li>';
					$html .= '<li class="active">'.$action_label.'<span class="divider">/</span></li>';
				} else {
					$html .= '<li class="active">'.$action_label.'</li>';
				}
			}//end if
			$html .= '</ul>';
			return $html;
		}//end function
		
		public function print_breadcrumbs(){
			echo $this->get_breadcrumbs();
		}//end function
		
		public function get_directions(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($is_category)){
				$is_category = false;	
			}
			if($is_category === false){
				if(strpos($this->action,"category") !== false){
					$category_id = $id;			
				}//end if
			}//end if
			$crumbs = array();			
			
			//get first parent path
			if(isset($id) || isset($category_id)){
				//we have a post
				if(isset($id)){
					$result = $this->get_north_pole(array("id" => $id));					
				} else {
					//we have a category
					//we get the category title
					$query = "SELECT * FROM categories WHERE category_id = :category_id";					
					$result = $this->utils->db->prepare($query);
					$result->bindValue(":category_id", $category_id, PDO::PARAM_INT);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));	
					if($errors === false){
						$num = $result->rowCount();
						if($num > 0){
							$row = $result->fetch(PDO::FETCH_ASSOC);
							array_push($crumbs,$row);
						} else {
							return false;
						}//end if
					} else {
						return false;
					}//end if
					$result = $this->get_north_pole(array("category_id" => $category_id));
				}//end if
				if($result !== false && is_array($result)){
					array_push($crumbs,$result);
					$i = 0;
					while($result !== false){
						$result = $this->get_north_pole(array("category_id" => $crumbs[$i]['category_id']));
						if($result !== false){
							$i++;
							$crumbs[$i] = $result;
						}//end if						
					}//end while
				}//end if
				return $crumbs;
			}//end if
		}//end function
		
		private function get_north_pole(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!is_array($this->module_data)){
				return false;	
			}//end if
			
			if(isset($id)){
				//we received a post ID	
				try{
					$query = "SELECT category_name, category_id 
							  FROM modules_items_order, categories 
							  WHERE module_item_order_item_id=:post_id 
							  AND module_item_order_type = 'post' 
							  AND module_item_order_module_id=:module_id
							  AND category_id = module_item_order_father_id
							  AND module_item_order_father_id != 0
							  LIMIT 1";
					$result = $this->utils->db->prepare($query);
					$result->bindValue(":post_id", $id, PDO::PARAM_INT);
					$result->bindValue(":module_id", $this->module_data['module_id'], PDO::PARAM_INT);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));	
					if($errors === false){
						$num = $result->rowCount();
						if($num > 0){
							$row = $result->fetch(PDO::FETCH_ASSOC);
							return $row;
						} else {
							return false;
						}//end if
					} else {
						return false;	
					}//end if
				} catch (PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end if
			}//end if
			
			if(isset($category_id)){
				//we received a category ID	
				try{
					$query = "SELECT category_name, category_id 
							  FROM modules_items_order, categories 
							  WHERE module_item_order_item_id = :category_id 
							  AND module_item_order_type = 'category' 
							  AND module_item_order_module_id = :module_id
							  AND category_id = module_item_order_father_id
							  AND module_item_order_father_id != 0
							  LIMIT 1";
					$result = $this->utils->db->prepare($query);
					$result->bindValue(':category_id', $category_id, PDO::PARAM_INT);
					$result->bindValue(':module_id', $this->module_data['module_id'], PDO::PARAM_INT);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));	
					if($errors === false){
						$num = $result->rowCount();
						if($num > 0){
							$row = $result->fetch(PDO::FETCH_ASSOC);
							return $row;
						} else {
							return false;
						}//end if
					} else {
						return false;	
					}//end if
				} catch (PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end if
			}//end if
			
			return false;
		}//end function
		
		public function get_module_settings(){
			$settings = $this->utils->get_settings(array("module_id" => $this->module));
			return $settings;
		}//end function
		
		public function get_category_explorer(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			require_once("inc/components/categories/categories.explorer.php");
		}//end function
		
		#################################################################
		# WIDGETS PART													#		
		#################################################################
		
		public function widgets(){
			//first we get the full list of modules
			$modules = $this->utils->get_modules(array('show_all' => true,'show_hidden' => true));
			$widgets = array();
			if($modules !== false){
				//now we look for widgets in each module folder
				for($i = 0; $i < sizeof($modules); $i++){
					$can_see = true;
					
					if(!is_null($this->user_permissions) && is_array($this->user_permissions)){
						if($this->user_permissions[$modules[$i]['module_name']] <= 0){
							$can_see = false;	
						}//end if					
					}//end if
					//now we check if we have the widgets directory in the module
					if(file_exists("modules/".$modules[$i]['module_name']."/widgets/") && $can_see){						
						$path = __CMS_PATH__."modules/".$modules[$i]['module_name']."/widgets/";
						$path = str_replace('//','/',$path);
						$abs_path = $_SERVER['DOCUMENT_ROOT'].$path;
						$files = scandir($abs_path);
						$result = array();
						$avoid_files = array('.','..','.AppleDouble','.DS_Store');				
						for($x = 0; $x < sizeof($files); $x++){					
							if(file_exists($abs_path.$files[$x]) && !is_dir($abs_path.$files[$x]) && !in_array($files[$x],$avoid_files) && strpos($files[$x],'default') === false){	
								$data = array();
								$data['module'] = $modules[$i]['module_name'];
								$data['file'] = $files[$x];
								array_push($widgets,$data);
							}//end if
						}//end for i
					}//end if
				}//end for i				
				
			
				
				if(sizeof($widgets) > 0){
					echo '<div class="row-fluid dashboard-widgets">';					
					for($z = 0; $z < sizeof($widgets); $z++){	
						$zz = $z+1;					
						echo '<div class="span6">';
						echo '<div class="dashboard-widget clearfix">';						
						include("modules/".$widgets[$z]['module']."/widgets/".$widgets[$z]['file']);
						echo '</div><!-- /.dashboard-widget -->';
						echo '</div><!-- /.span6 -->';
						if($zz%2 == 0 && $z > 0){
							echo '</div><br /><div class="row-fluid">';	
						}
					}//end for i
					echo '</div>';
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		#################################################################
		# PUBLIC PART													#		
		#################################################################
		
		public static function permalink(){
			return $_GET['permalink'];
		}//end function
		
		public function get_permalink(){
			return $_GET['permalink'];
		}//end function
		
		public static function section(){
			return $_GET['section'];
		}//end function
		
		public function get_section(){
			return $_GET['section'];
		}//end function
		
		public function get_section_data(){
			$module_data = $this->utils->get_module(array('module' => $_GET['section']));
			if($module_data === false){
				$module_data = $this->utils->get_module(array('module' => $this->page['module']));
				return $module_data;
			} else {
				return $module_data;	
			}//end if
		}//end function
		
		protected function path_exists($file){
			if(isset($this->settings['templatesPath'])){
				$paths = $this->settings['templatesPath'];
				$paths = explode(',',$paths);
				foreach($paths as $path){
					$path = trim($path);
					if(file_exists($_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.$path.$file)){
						return true;	
					}//end if
				}//end foreach
				return false;
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function url_interceptor(){
			
			$seo = $this->utils->call('seo');
				
			$section = $_GET['section'];
			$permalink = $_GET['permalink'];					
		
			if(isset($this->settings['protectedPermalinks']) && $this->settings['protectedPermalinks'] != ""){
				$protected = explode("\n",$this->settings['protectedPermalinks']);		
			}//end if	
			
			if(isset($_GET['permalink']) && !isset($_GET['section'])){
				$page = $seo->resolve_permalink(array('permalink' => $permalink,'meta' => true,'default_photo' => true));
				if($page === false){
					//now we check if we want to access to a hard coded template	
					if($this->path_exists($permalink.'.php')){
						$page = array();
						$page['type'] = 'hardcode';
						$page['template'] = $permalink.'.php';
					} else {
						return false;
					}//end if			
				}//end if
			} else if(!isset($_GET['permalink']) && isset($_GET['section'])){
				$skip_section_ctrl = false;				
				if(isset($protected)){
					if(is_array($protected) && sizeof($protected) > 0){
						for($i = 0; $i < sizeof($protected); $i++){						
							if($_GET['section'] == trim(strtolower($protected[$i]))){
								$page = array();
								$page['type'] = 'hardcode';
								$page['template'] = $_GET['section'].'.php';
								$skip_section_ctrl = true;
							}//end if
						}//end if					
					}//end if
				} else {					
					if($this->path_exists($_GET['section'].'.php')){
						$page = array();
						$page['type'] = 'hardcode';
						$page['template'] = $_GET['section'].'.php';
						$skip_section_ctrl = true;
					}//end if
				}//end if
				if($skip_section_ctrl === false){
					$ewrite = $this->utils->call('ewrite',array('module' => $section));					
					$intro = $ewrite->get_intro(array('module' => $section,'force' => true));
					if($intro !== false){
						$permalink = $intro[0]['post_permalink'];
						$page = $seo->resolve_permalink(array('permalink' => $permalink,'meta' => true,'default_photo' => true));
						if($page == false){				
							return false;
						}//end if
					} else {
						return false;
					}//end if
				}
			} else if(isset($_GET['permalink']) && isset($_GET['section'])){		
				$page = $seo->resolve_permalink(array('permalink' => $permalink,'meta' => true,'default_photo' => true));	
				if($page === false){								
					//now we check if we want to access to a hard coded template	
					if($this->path_exists($_GET['section'].'.'.$_GET['permalink'].'.php')){	
						$page['type'] = 'hardcode';
						$page['template'] = $_GET['section'].'.'.$_GET['permalink'].'.php';
					} else if($this->path_exists($_GET['section'].'.php')){			
						$page['type'] = 'hardcode';
						$page['template'] = $_GET['section'].'.php';
					} else {
						return false;
					}//end if	
				}//end if
			}//end if
			
			switch($page['type']){
				case "post":
					$page['module'] = $page['post_module_id'];
				break;
				case "category":
					$page['module'] = $page['module_name'];
				break;	
			}
			$this->page = $page;
			return true;
		}//end function
		
		public function load_page(){
			$page_loaded = $this->url_interceptor();
			if(!$page_loaded && !$this->dev_mode){
				header('location: '.__SERVERPATH__);
				die();		
			}//end if
		}//end function
		
		public function load_template(){			
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($template_path)){
				$template_path = 'inc/templates/';
			}//end if			
			
			$dev_mode = $this->dev_mode;
			
			if(!isset($this->page)){
				if(!$dev_mode){
					header('location: '.__SERVERPATH__);
				}//end if
			}//end if
			
			$page = $this->page;
			$template = false;		
			
			switch($page['type']){
                case "post":				
                    if($page['post_template'] !== 'default'){	
                        if(file_exists($template_path.$page['post_template'])){
                            $template = $template_path.$page['post_template'];
                        }//end if
                    } else {
                        $template = $template_path.'default.php';
                    }//end if
                    break;
                case "category":				
                    if($page['category_template'] !== 'default'){	
                        if(file_exists($template_path.$page['category_template'])){
                            $template = $template_path.$page['category_template'];
                        }//end if
                    } else {
                        $template = $template_path.'default_category.php';
                    }
                    break;
                case "hardcode":				
                    if(file_exists($template_path.$page['template'])){
                        $template = $template_path.$page['template'];						
                    } else {
	                    if(!$dev_mode){
							header('location: '.__SERVERPATH__);
						}//end if
                    }//end if
                    break;
            }//end switch
			return $template;
		}//end function
		
		public static function get_backgrounds(){
			$path = __CMS_PATH__."gr/backgrounds/";
			$path = str_replace('//','/',$path);
			$abs_path = $_SERVER['DOCUMENT_ROOT'].$path;
			$files = scandir($abs_path);
			if($files !== false){
				$photos = array();
				$avoid_files = array('.','..','.AppleDouble','.DS_Store');				
				for($x = 0; $x < sizeof($files); $x++){	
					if(file_exists($abs_path.$files[$x]) && !is_dir($abs_path.$files[$x]) && !in_array($files[$x],$avoid_files)){
						array_push($photos,"gr/backgrounds/".$files[$x]);
					}//end if
				}//end for i
				if(sizeof($photos) > 0){
					return $photos;	
				} else {
					return false;
				}
			} else {
				return false;
			}//end if
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
		
		private function init_settings(){
			$this->settings = $this->utils->get_settings();						
			//we also add the media settings in the general settings
			$media_settings = $this->utils->get_settings(array('module' => 'media'));
			$this->settings['media'] = $media_settings;
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>