<?php
	#################################################################
	# Utils Class - 	last update 15/03/13						#
	#					created in 2008								#
	# This is a core class for the Old Hat CMS						#	
	#																#
	# 14/11/12 - Fixed a problem with the sql insert of a timestamp #
	# 21/11/12 - Edited the build_sql_fields method, added the      #
	#			 mysql_escape_string								#
	# -------- IMPORTANT!	--------------------------------------- #	
	# From 06/12/12 this class fork to a new branch of development  #
	# and it will be no more compatible with old version of the cms #
	# so DO NOT USE IT on previous version of the cms				#
	# ------------------------------------------------------------- #
	#																#
	# 06/12/12 - Removed old functions								#	
	# 09/12/12 - Update get_attachments, parameters like is_img or  #	
	# 			 is_file are not accepted anymore, just use the 	#
	#			 the type parameter									#
	# 10/12/12 - Removed all the methods regarding the attachments  #
	#			 as it's going to be rewritten from scratch         #
	# 11/12/12 - Rebuild basic attachments methods, removed more 	#
	#			 old methods										#	
	# 14/12/12 - Made some improvements in the methods related to   #
	#			 attachments										#
	# 16/12/12 - Cleaned the code and moved some methods to tools   #
	# 17/12/12 - Create the candybox element						#
	# 18/12/12 - Replaced some deprecated php functions				#
	# 21/12/12 - Started convertion to PDO							#
	# 14/03/12 - Fixed several bugs									#
	# 15/03/13 - Added function check_attachment_post_ownership     #
	# From now on, all the edits will be on bitbucket				#
	#################################################################

	//Catch eventually different path
	if($_GET['curpath'] != NULL && !isset($curpath)){
		$curpath = base64_decode(urldecode($_GET['curpath']));
	} else if(!isset($curpath)){
		$curpath = NULL;
	}//end if
	
	require_once('core.class.php');
	
	class utils extends core{
	
		public $p_start; //Paging start element
		public $p_limit; //Elements showed per page
		public $p_max; //Number of the total elements sent for paging
		public $p_current; //Current page
		private $mail_id; //Mail ID
		public $module_id;
		private $protected_dirs;
		protected $cur_path;
		public $p_stats;
		public $file_protection;
		protected $passKey;
		protected $file_id;
		public $cache;
		public $candybox; //this is an array accessible externally to pass any kind of variables
		public $errors = array();
		
		//Constructor
		function __construct($cur_path = NULL){
			parent::__construct($cur_path);			
			$this->protected_dirs = array('admin','classes','content','include','css');
			$this->cur_path = $cur_path;
			$this->file_protection = false;
			$this->passKey = __ENCRYPTION_PASSKEY__;
			$this->candybox = array();
		}//endconstructor
				
		#####################################################
		# MODULES PART - BEGIN								#
		#####################################################		
		
		public function get_modules_groups(){
			$query = "SELECT * FROM modules_groups";
			$result = @$this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function save_modules_group(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			if(!isset($id) && isset($_REQUEST['id'])){
				$id = $_REQUEST['id']; 	
			}//end if
			
			if(!isset($name)){
				return false;	
			}//end if
			
			if(!isset($hidden) || is_null($hidden)){
				$hidden = "0";
			}//end if
			
			if(isset($id) && !is_null($id) && is_numeric($id)){
				//update a module
				try{
					//is a new module
					$query = "UPDATE modules_groups SET 
							  module_group_name = :name,							 
							  module_group_hidden = :hidden
							  WHERE module_group_id = :id";							
					$result = $this->db->prepare($query);
					$result->bindParam(':name', $name, PDO::PARAM_STR);
					$result->bindParam(':hidden', $hidden, PDO::PARAM_INT);				
					$result->bindParam(':id', $id, PDO::PARAM_INT);
					$result->execute();
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors === false){
						$data = array();						
						$data['module_group_id'] = $id;						
						return $data;
					} else {
						return false;
					}//end if
				} catch(PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end if
			} else {
				try{
					//is a new module
					$query = "INSERT INTO modules_groups
							  (module_group_name, module_group_hidden)
							  VALUES
							  (:name, :hidden)";							
					$result = $this->db->prepare($query);
					$result->bindParam(':name', $name, PDO::PARAM_STR);
					$result->bindParam(':hidden', $hidden, PDO::PARAM_INT);					
					$result->execute();
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors === false){
						$data = array();
						$id = $this->db->lastInsertId();
						$data['module_group_id'] = $id;						
						return $data;
					} else {
						return false;
					}//end if
				} catch(PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end if
			}//end if
		}//end function
		
		public function save_modules_group_sorting($post_ids){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($group_id) || !isset($ids)){
				return false;	
			}//end if
			
			if(!is_array($ids)){
				return false;	
			}//end if
			
			if(is_array($ids) && sizeof($ids) > 0){
				for($i = 0; $i < sizeof($ids); $i++){
					$query = "UPDATE ".$this->t."modules 
							  SET module_order = ".$i."
							  WHERE module_id = ".$ids[$i]."
							  AND module_gid = ".$group_id;
					$result = $this->db->query($query);
					$this->error($result,__LINE__,get_class($this));	
				}//end for
				return true;
			} else {
				return false;
			}//end if		
		}//end function		
		
		public function get_modules_group(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$id = $params[0];				
			}//end if
			
			$query = " SELECT * FROM modules_groups WHERE module_group_id = ".$id;			
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		//Alias of get_modules(), use the other function, this will be deprecated
		public function get_modules_list($group = NULL,$show_hidden = false){
			return $this->get_modules($group,$show_hidden);
		}//end function
		
		//Give the list of the actived modules
		public function get_modules(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$group_id = $params[0];
				$show_hidden = $params[1];
			}//end if
			
			if(!isset($show_hidden) || is_null($show_hidden)){
				$show_hidden = false;
			}//end if
			
			$query = "SELECT * FROM modules";
			if(isset($public) && is_bool($public)){
				$query .= " LEFT JOIN settings
							ON setting_module_id = module_id";
			}//end if
			if(isset($show_all) && (bool)$show_all == true){
				$query .= " WHERE module_name != ''";
			} else {
				$query .= " WHERE module_name != 'generic'";
			}
			if(isset($group_id) && is_numeric($group_id)){
				$query .= " AND module_gid = ".$group_id;
			}//end if
			if($show_hidden == false){
				$query .= " AND module_hidden = 0";
			}//end if		
			if(isset($public) && is_bool($public)){
				$query .= " AND setting_label = 'module-status'";
				if($public){
					$query .= " AND setting_value = 'public'";	
				} else {
					$query .= " AND setting_value = 'private'";
				}//end query
			}//end if
			
			if(isset($standard) && $standard == true){
				$query .= " AND module_table = 'posts'";
			}//end if
				
			$query .= " ORDER BY module_order ASC, module_id ASC";	
		//	echo $query;		
			$result = @$this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		//Alias of get_module(), use the other function, this will be deprecated
		public function get_module_data($module){
			return $this->get_module($module);
		}//end function
		
		public function get_module(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$module = $params[0];
			}//end if
			$query = "SELECT * FROM modules WHERE ";
			//Check if the module is a string
			if(!is_numeric($module)){
				//It's a string
				$query .= "module_name = '".$module."'";
			} else {
				//It's a numer
				$query .= "module_id = ".$module."";
			}//end if
			$query .= " LIMIT 1";			
			$result = $this->db->query($query);
			$error = $this->error($result,__LINE__,get_class($this));	
			if($error === false){
				$num = $result->rowCount();	
				if($num > 0){		
					$data = $this->get_result_array($result,true);
					$field_name = $data['module_table'];
					if(substr($field_name,-3) == "ies"){
						$field_name = substr($field_name,0,-3);
						$field_name .= "y";
						$data['module_field'] = $field_name;
					} else if(substr($field_name,-1) == "s"){
						$field_name = substr($field_name,0,-1);
						$data['module_field'] = $field_name;
					} else {
						$data['module_field'] = $data['module_table'];
					}//end if
					return $data;
				} else {
					return false;	
				}
			} else {
				return false;	
			}//end if
		}//end function
		
		public function save_module(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			if(!isset($module_id) && isset($_REQUEST['id'])){
				$module_id = $_REQUEST['id']; 	
			}//end if
			
			if(!isset($module_label) || !isset($group_id)){
				return false;	
			}//end if
			
			if(!isset($module_name)){
				$module_name = urlencode($module_label);
				$module_name = strtolower(iconv('ISO-8859-1','ASCII//TRANSLIT',$module_name));
				$module_name = str_replace("+","-",$module_name);
			}//end if
			
			if(!isset($module_table)){
				$module_table = "posts";	
			}//end if
			
			if(isset($module_id) && !is_null($module_id) && is_numeric($module_id)){
				//update a module
				try{
					//is a new module
					$query = "UPDATE modules SET 
							  module_label = :module_label,
							  module_table = :module_table, 
							  module_gid = :module_gid
							  WHERE module_id = :module_id";							
					$result = $this->db->prepare($query);
					$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
					$result->bindParam(':module_label', $module_label, PDO::PARAM_STR);
					$result->bindParam(':module_table', $module_table, PDO::PARAM_STR);
					$result->bindParam(':module_gid', $group_id, PDO::PARAM_INT);
					$result->execute();
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors === false){
						$data = array();
						$this->module_id = $module_id;
						$data['module_id'] = $this->module_id;						
						return $data;
					} else {
						return false;
					}//end if
				} catch(PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end if
			} else {
				try{
					//is a new module
					$query = "INSERT INTO modules
							  (module_name, module_label, module_table, module_gid)
							  VALUES
							  (:module_name, :module_label, :module_table, :module_gid)";							
					$result = $this->db->prepare($query);
					$result->bindParam(':module_name', $module_name, PDO::PARAM_STR);
					$result->bindParam(':module_label', $module_label, PDO::PARAM_STR);
					$result->bindParam(':module_table', $module_table, PDO::PARAM_STR);
					$result->bindParam(':module_gid', $group_id, PDO::PARAM_INT);
					$result->execute();
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors === false){
						$data = array();
						$this->module_id = $this->db->lastInsertId();
						$data['module_id'] = $this->module_id;
						//now that we have saved the module, we create the default permissions for the admin groups
						$users = $this->call("users");
						$users->set_default_module_permissions(array("module_id" => $this->module_id));
						$this->set_default_module_settings(array("module_id" => $this->module_id));
						return $data;
					} else {
						return false;
					}//end if
				} catch(PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end if
			}//end if
		}//end function
		
		protected function set_default_module_settings(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($module_id)){
				return false;
			}//end if
			
			$defaults = array();
			$defaults['module-status'] = 'public';
			$defaults['list_view_type'] = 'large';
			$defaults['list_view_mode'] = 'default';
			$defaults['list_order'] = 'DESC';
			$defaults['list_system'] = 'natural';
			
			foreach($defaults as $key => $value){
				$query = "INSERT INTO settings
						  (setting_label, setting_value,setting_module_id)
						  VALUES
						  ('".$key."','".addslashes($value)."','".$module_id."')";
				$result = @$this->db->query($query);
				$this->error($result,__LINE__,get_class($this));					
			}//end foreach
			return true;
			
		}//end function
		
		#####################################################
		# MODULES PART - END								#
		#####################################################
				
				
		public function set_paging($p_start,$p_limit){					
			$this->p_start = $p_start;
			$this->p_limit = $p_limit;	
			$this->p_stats['start'] = $this->p_start;				
		}//end function
		
		public function unset_paging(){
			$this->p_start = NULL;
			$this->p_limit = NULL;	
			$this->p_stats = NULL;	
		}//end function
		
		public function paging_isset(){		
			if($this->p_limit != NULL){
				return true;				
			} else {
				return false;
			}//end if
		}//end functiton
	
		public function get_uid(){
			if($_COOKIE['uid'] != NULL){
				return $_COOKIE['uid'];
			} else if($_SESSION['uid'] != NULL){
				return $_SESSION['uid'];				
			} else {
				return false;
			}//end if
		}//end function
		
		//This function initialize the pagination
		public function create_paging($current_page = 1,$p_limit = 20){
			$this->unset_paging();
			if($current_page == NULL){
				$current_page = 1;
			}//end if
			$this->p_stats = array();
			$this->p_stats['current_page'] = $current_page;
			$this->p_current = $current_page;
			//$p_limit = 20;
			$j = $current_page-1;
			$start = $p_limit * $j;
			$this->set_paging($start,$p_limit);
		}//end function
		
		public function set_unpaged_query($query,$avoid_left_joins = true,$keep_grouping = false,$force_union_query = false){
			//The original query will be clean from useless code for improve the speed of the result
			//Replace the select with a count				
			$union_query = true;			
			if(substr($query,0,1) != "("){				
				$union_query = false;	
				$query_from = strpos($query,"FROM");
				$query_from = substr($query,$query_from);
				$query = "SELECT COUNT(*) AS record_founds ".$query_from;
			} else {			
				$query = preg_replace('/SELECT (.*?) FROM/s', 'SELECT COUNT(*) AS record_founds FROM', $query);				
				$union_query = true;	
			}//end if
			if($force_union_query == true){
				$union_query = true;	
			}//end if
		
			if($avoid_left_joins == true){
				//Take out the left join, because can cause some f**king problems
				$begin_LJ = strpos($query,"LEFT JOIN");
				$end_LJ = strpos($query,"WHERE");
				if($begin_LJ !== false){
					$query = substr($query,0,$begin_LJ)." ".substr($query,$end_LJ);
				}//end if
			}//end if
		
			//Take out the group by and order by, see above for the reason
			$query_group = strpos($query,"ORDER BY");
			if($query_group !== false){
				$query = substr($query,0,$query_group);	
			}//end if
			if($keep_grouping == false){
				//Take out the group by and order by, see above for the reason
				if($begin_LJ !== false){
					$query_group = strpos($query,"GROUP BY");
					if($query_group !== false){
						$query = substr($query,0,$query_group);	
					}//end if
				}//end if
			}//end if
			
			if($union_query){
				$query = 'SELECT SUM(record_founds) AS record_founds FROM ('.$query.') AS union_query';	
			}//end if
			$this->cache = $query;	
			//execute the new query
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors === false){
				//if($union_query == false){
					$row = $result->fetch(PDO::FETCH_ASSOC);				
					$this->p_max = $row['record_founds'];
				/*} else {
					$num = $result->rowCount();
					$this->p_max = $num;
				}//end if	*/		
				$this->p_stats['founds'] = $this->p_max;
				$pages = ceil($this->p_max/$this->p_limit);	
				$this->p_stats['pages'] = $pages;	
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_paging_stats(){
			return $this->p_stats;
		}//end function
		
		//Give the list of the actived modules
		public function get_pages(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			$query = "SELECT * FROM ".$this->t."modules 
					  WHERE module_name != 'generic'
				      AND module_hidden = 0 
					  AND module_public = 1";
			if(isset($gid)){
				$query .= " AND module_gid = ".$gid;
			}//end if
			$query .= " ORDER BY module_order ASC, module_id ASC";		
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function		
								
		public function get_num_pages(){
			$pages = ceil($this->p_max/$this->p_limit);	
			return $pages;
		}//end function
		
		public function get_paging(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$pages = ceil($this->p_max/$this->p_limit);		
	
			$this_page = $_SERVER['PHP_SELF'];
			$this_page = basename($this_page);			
			
			//Catch all the GET data and put in the string url
			$url_vars = $this->get_url_vars();
			
			if(isset($post_vars) && $post_vars === true){
				$url_vars .= $this->get_post_vars();
			}//end if
			
			if(isset($cms)){
				if($cms){
					$vars = $_REQUEST;
					$module = $vars['m'];
					unset($vars['m']);
					$action = $vars['a'];
					unset($vars['a']);
					if(isset($vars['page'])){
						unset($vars['page']);	
					}//end if
					$action = str_replace(".","/",$action);
					$url_vars = "&".http_build_query($vars);
					$this_page = __CMS_PATH__.$module."/".$action."/";
				}//end if
			}//end if
			
			$result = array();	
		
			if($range != NULL && is_numeric($range)){
				$just_beginned = false;
				$end_reached = false;
				$lower_limit = $this->p_current;
				$lower_limit -= floor($range/2);						
				//In case the limit is under 0, like in case the selected page is 1, 
				if($lower_limit < 0){
					$just_beginned = true;
					$lower_limit += abs($lower_limit);
					//$lower_limit--;					
				}//end if
				if(($this->p_current + ceil($range/2)) > $pages){
					$end_reached = true;
					$lower_limit--;	
					$lower_limit -= ($this->p_current + floor($range/2)) - ($pages+1);
				}//end if					
			}//end if		
			
			if(!isset($_GET['page'])){
				$lower_limit = 0;	
			}
						
			$x = 0;
			for($i = 1; $i <= $pages; $i++){
				if(!isset($range) || (isset($range) && ($i >= $lower_limit && $i <= ($lower_limit+$range)))){
					//Create the link
					$result[$x] = array();
					$page_url = "?page=".$i;
					$result[$x]['page'] = $i;					
					$result[$x]['url'] = $this_page.$page_url.$url_vars;
					$result[$x]['url_clean'] = $page_url.$url_vars;
					if($i == $this->p_current){
						$result[$x]['current'] = true;
					} else {
						$result[$x]['current'] = false;
					}//end if	
					$x++;			
				}//end if
			}//end for			
			
			return $result;
		}//end function			
		
		public function get_url_vars(){
			$url_vars = "";
			foreach($_GET as $key => $value){
				if($key != "page"){				
					$url_vars .= "&amp;".$key."=".$value;	
				}//end if		
			}//end foreach
			return $url_vars;
		}//end function
		
		public function get_post_vars(){
			$url_vars = "";
			foreach($_REQUEST as $key => $value){
				if($key != "page"){				
					$url_vars .= "&amp;".$key."=".$value;	
				}//end if		
			}//end foreach
			return $url_vars;
		}//end function
		
		//Convert a string in the format dd/mm/YYYY to Ymd
		public function convert_to_db_date($dateString){
			$newdate = explode("/",$dateString);
			$newdate = $newdate[2]."-".$newdate[1]."-".$newdate[0]." 01:00:00";	
			return $newdate;
		}//end function		
		
		//Page means the page number, per_page means the number of elements will be showed in the page
		public function set_page($page,$per_page = __ELEMENTS_PER_PAGE__){
			$this->p_start = $page * $per_page;
			$this->p_limit = $per_page;
		}//end function
		
		public function call($classname, $params = array()){
			if($classname != NULL){	
				//check if the classname is ewrite
				if($classname == "ewrite"){
					//now check if exists an customized extended version for the ewrite class					
					if(file_exists($this->cur_path."classes/ewrite.ext.class.php")){								
						//if true, overwrite the ewrite using the extended version
						$classname = "ewrite_ext";
					}//end if
				}//end if	
				if($classname == "users"){
					//now check if exists an customized extended version for the ewrite class					
					if(file_exists($this->cur_path."classes/users.ext.class.php")){								
						//if true, overwrite the ewrite using the extended version
						$classname = "users_ext";
					}//end if
				}//end if
				//include the file		
				$classfile = $classname;
				//$classfile = str_replace("public_","public.",$classfile); //Transform public_classname in public.classname	
				$classfile = str_replace("_",".",$classfile);
				require_once($classfile.".class.php");
				
				if(class_exists($classname)){
					if(sizeof($params) != 0){
						return new $classname($this,$params);
					} else {
						return new $classname($this);
					}//end if					
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function					
		
		#This function trunk text !- TO IMPROVE
		public function trunktext($txt,$limit = 50){
			$txt = strip_tags($txt);
			if(strlen($txt) > $limit){
				$txt = substr($txt,0,$limit);
				$txt .= "...";
			}//endif
			return($txt);
		}//end function
		
		public function hidden_dump($string){
			echo '<!-- ';
			var_dump($string);
			echo ' -->';
		}//end if
	
		public function strtodb($txt) {	
			/*$badchr = array(
			"\\xe2\\x80\\xa6",	 // ellipsis
			"\\xe2\\x80\\x93",	 // long dash
			"\\xe2\\x80\\x94",	 // long dash
			"\\xe2\\x80\\x98",	 // single quote opening
			"\\xe2\\x80\\x99",	 // single quote closing
			"\\xe2\\x80\\x9c",	 // double quote opening
			"\\xe2\\x80\\x9d",	 // double quote closing
			"\\xe2\\x80\\xa2"	 // dot used for bullet points
			);
			
			$goodchr = array('...',	'-','-','\'','\'','"','"','*');	
			for($i = 0; $i < sizeof($badchr); $i++){
				$txt = str_replace($badchr[$i], $goodchr[$i],$txt);
			}//end for	*/
			/*$txt = str_replace("<p>&#160;</p>\n","<br />",$txt);
			$txt = str_replace("í","'",$txt);*/
			
			$txt = addslashes(htmlentities($txt));
			/*$txt = str_replace("&rsquo;","'",$txt); 
			$txt = str_replace("&lsquo;","'",$txt);*/

			return $txt;
		}//end function
		
		public function dbtostr($txt) {
			$txt = html_entity_decode(stripslashes($txt));
			return $txt;
		}//end function
				
		//This function unify a string from spaces, special chars, etc...
		public function unifystring($string){
			$string = strtr($string,"éËÈáÁóÓÍíúÚ","eeeaaooiiuu");
			$string = str_replace("'","",$string);
			$string = str_replace("\"","",$string);
			$string = str_replace("‡","a",$string);
			//$string = str_replace("Ë","e",$string);
			//$string = str_replace("È","e",$string);
			$string = str_replace(" ","_",$string);			
			$string = str_replace("’","",$string);
			//$string = str_replace("Ú","u",$string);
			//$string = str_replace("ú","u",$string);
			$string = str_replace("˘","u",$string);
			//$string = str_replace("é","e",$string);
			//$string = str_replace("É","e",$string);
			$string = str_replace("?","",$string);
			$string = str_replace("!","",$string);
			$string = str_replace(",","",$string);
			$string = str_replace("%","",$string);
			$string = str_replace("&","and",$string);
			$string = str_replace("(","",$string);
			$string = str_replace(")","",$string);
			$string = str_replace("\\","",$string);
			$string = str_replace("/","",$string);
			$string = str_replace("__","_",$string);
			//$string = str_replace("á","a",$string);
			//$string = str_replace("Á","a",$string);
			//$string = str_replace("Í","i",$string);
			//$string = str_replace("í","o",$string);
			$string = str_replace("+","",$string);
			$string = str_replace("*","",$string);
			//$string = str_replace("-","",$string);
			$string = str_replace("[","",$string);
			$string = str_replace("]","",$string);
			$string = str_replace("{","",$string);
			$string = str_replace("}","",$string);
			//$string = str_replace("ó","o",$string);
			//$string = str_replace("Ó","o",$string);
			return $string;
		}//end function		
		
		protected function fixAscii($string) {
			$map = array(
				'33' => '!', '34' => '"', '35' => '#', '36' => '$', '37' => '%', '38' => '&', '39' => "'", '40' => '(', '41' => ')', '42' => '*', 
				'43' => '+', '44' => ',', '45' => '-', '46' => '.', '47' => '/', '48' => '0', '49' => '1', '50' => '2', '51' => '3', '52' => '4', 
				'53' => '5', '54' => '6', '55' => '7', '56' => '8', '57' => '9', '58' => ':', '59' => ';', '60' => '<', '61' => '=', '62' => '>', 
				'63' => '?', '64' => '@', '65' => 'A', '66' => 'B', '67' => 'C', '68' => 'D', '69' => 'E', '70' => 'F', '71' => 'G', '72' => 'H', 
				'73' => 'I', '74' => 'J', '75' => 'K', '76' => 'L', '77' => 'M', '78' => 'N', '79' => 'O', '80' => 'P', '81' => 'Q', '82' => 'R', 
				'83' => 'S', '84' => 'T', '85' => 'U', '86' => 'V', '87' => 'W', '88' => 'X', '89' => 'Y', '90' => 'Z', '91' => '[', '92' => '\\', 
				'93' => ']', '94' => '^', '95' => '_', '96' => '`', '97' => 'a', '98' => 'b', '99' => 'c', '100'=> 'd', '101'=> 'e', '102'=> 'f', 
				'103'=> 'g', '104'=> 'h', '105'=> 'i', '106'=> 'j', '107'=> 'k', '108'=> 'l', '109'=> 'm', '110'=> 'n', '111'=> 'o', '112'=> 'p', 
				'113'=> 'q', '114'=> 'r', '115'=> 's', '116'=> 't', '117'=> 'u', '118'=> 'v', '119'=> 'w', '120'=> 'x', '121'=> 'y', '122'=> 'z', 
				'123'=> '{', '124'=> '|', '125'=> '}', '126'=> '~', '127'=> ' ', '128'=> '&#8364;', '129'=> ' ', '130'=> ',', '131'=> ' ', '132'=> '"', 
				'133'=> '.', '134'=> ' ', '135'=> ' ', '136'=> '^', '137'=> ' ', '138'=> ' ', '139'=> '<', '140'=> ' ', '141'=> ' ', '142'=> ' ', 
				'143'=> ' ', '144'=> ' ', '145'=> "'", '146'=> "'", '147'=> '"', '148'=> '"', '149'=> '.', '150'=> '-', '151'=> '-', '152'=> '~', 
				'153'=> ' ', '154'=> ' ', '155'=> '>', '156'=> ' ', '157'=> ' ', '158'=> ' ', '159'=> ' ', '160'=> ' ', '161'=> '¡', '162'=> '¢', 
				'163'=> '£', '164'=> '¤', '165'=> '¥', '166'=> '¦', '167'=> '§', '168'=> '¨', '169'=> '©', '170'=> 'ª', '171'=> '«', '172'=> '¬', 
				'173'=> '­', '174'=> '®', '175'=> '¯', '176'=> '°', '177'=> '±', '178'=> '²', '179'=> '³', '180'=> '´', '181'=> 'µ', '182'=> '¶', 
				'183'=> '·', '184'=> '¸', '185'=> '¹', '186'=> 'º', '187'=> '»', '188'=> '¼', '189'=> '½', '190'=> '¾', '191'=> '¿', '192'=> 'À', 
				'193'=> 'Á', '194'=> 'Â', '195'=> 'Ã', '196'=> 'Ä', '197'=> 'Å', '198'=> 'Æ', '199'=> 'Ç', '200'=> 'È', '201'=> 'É', '202'=> 'Ê', 
				'203'=> 'Ë', '204'=> 'Ì', '205'=> 'Í', '206'=> 'Î', '207'=> 'Ï', '208'=> 'Ð', '209'=> 'Ñ', '210'=> 'Ò', '211'=> 'Ó', '212'=> 'Ô', 
				'213'=> 'Õ', '214'=> 'Ö', '215'=> '×', '216'=> 'Ø', '217'=> 'Ù', '218'=> 'Ú', '219'=> 'Û', '220'=> 'Ü', '221'=> 'Ý', '222'=> 'Þ', 
				'223'=> 'ß', '224'=> 'à', '225'=> 'á', '226'=> 'â', '227'=> 'ã', '228'=> 'ä', '229'=> 'å', '230'=> 'æ', '231'=> 'ç', '232'=> 'è', 
				'233'=> 'é', '234'=> 'ê', '235'=> 'ë', '236'=> 'ì', '237'=> 'í', '238'=> 'î', '239'=> 'ï', '240'=> 'ð', '241'=> 'ñ', '242'=> 'ò', 
				'243'=> 'ó', '244'=> 'ô', '245'=> 'õ', '246'=> 'ö', '247'=> '÷', '248'=> 'ø', '249'=> 'ù', '250'=> 'ú', '251'=> 'û', '252'=> 'ü', 
				'253'=> 'ý', '254'=> 'þ', '255'=> 'ÿ'
			);
		
			$search = Array();
			$replace = Array();
		
			foreach ($map as $s => $r) {
				$search[] = chr((int)$s);
				$replace[] = $r;
			}//end foreach
		
			return str_replace($search, $replace, $string); 
		}//end function
				
		public function build_sql_fields(){
			$params = func_get_args();		
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$table_prefix = $params[0];
				$sql_type = $params[1];
				if(is_null($sql_type)){
					$sql_type = "INSERT";	
				}//end if
				$method_data = $params[2];
				if(is_null($method_data)){
					$method_data = "POST";	
				}//end if				
				$hidden_fields = $params[3];
				if(is_null($hidden_fields) && !is_array($hidden_fields)){
					$hidden_fields = array();
				}//end if
			}//end if	
			
			if(!isset($hidden_fields)){
				$hidden_fields = array();	
			}//end if
			
			$params = NULL;
			if(isset($module)){		
				if(is_numeric($module)){
					$module_data = $this->get_module($module);
					if($module_data !== false){
						$module = $module_data['module_name'];	
					} else {
						return false;	
					}//end if
				}//end if
					
				$options = array("module" => $module,"field_prefix" => $table_prefix, "show_all" => true);
				if(isset($module_table)){
					$options['module_table'] = $module_table;	
				}//end if
				$table_fields = $this->get_table_fields($options);
							
				array_walk($table_fields,array($this,'clean_prefix'),$table_prefix);						
				$params = array("compare_table" => $table_fields);
			}//end if
			
			if(!is_array($method_data)){
				if($method_data == "POST"){
					$data = $this->read_post_data($params);
				}//end if
				if($method_data == "REQUEST"){
					$data = $this->read_request_data($params);
				}//end if
				if($method_data == "GET"){
					$data = $this->read_get_data($params);
				}//end if
			} else {
				//in this case e read directly an array
				$data = $method_data;
			}//end  if	
			//Check which kind of sql string I have to build
			switch($sql_type){
				case "INSERT":
					//In case of an INSERT do that:
					$fields = "(";
					$values = " VALUES (";
					foreach($data as $key => $value){
						if(!in_array($key,$hidden_fields)){
							if($table_prefix != NULL){
								$fields .= $table_prefix."_".$key.", ";
							} else {
								$fields .= $key.", ";
							}//end if
							//check if is a password field, in case I crypt with sha1
							if($key == "password"){
								$value = sha1($value);
							}//end if
							$date_find = strpos($key,'date');
							if(is_numeric($date_find) && $value == ""){					
								$value = "NULL";	
							}//end if
							/*
							$value = str_replace("<br /><br /><br /><br />","<br /><br />",html_entity_decode($value));
							$value = htmlentities($value);
							$value = str_replace(chr(13),'',$value);
							$value = str_replace(chr(10),'',$value);
							$value = utf8_encode($value);
							$value = $this->fixAscii($value);*/
							$value = htmlspecialchars($value, ENT_NOQUOTES, "UTF-8");
							if($value != "NULL"){							
								$values .= $this->db->quote($value).", ";								
							} else {
								$values .= "NULL, ";
							}//end if
						}//end if
					}//end foreach
					$fields = substr($fields,0,strlen($fields)-2);
					$fields .= ")";
					$values = substr($values,0,strlen($values)-2);
					$values .= ")";
					$sql = $fields.$values;
					break;
				case "UPDATE":
					foreach($data as $key => $value){										
						if(!in_array($key,$hidden_fields) && $key != "uid"){
							/*$value = str_replace("<br /><br /><br /><br />","<br /><br />",html_entity_decode($value));
							$value = htmlentities($value);
							$value = str_replace(chr(13),'',$value);
							$value = str_replace(chr(10),'',$value);
							$value = utf8_encode($value);
							$value = $this->fixAscii($value);			*/
							$value = htmlspecialchars($value, ENT_NOQUOTES, "UTF-8");		
							$date_find = strpos($key,'date');
							if(is_numeric($date_find) && $value == ""){					
								$sql .= $table_prefix."_".$key." = NULL,";								
							} else {							
								$sql .= $table_prefix."_".$key." = ".$this->db->quote($value).",";
							}//end if
						}//endif
					}//end foreach
					$sql = substr($sql,0,strlen($sql)-1);
					break;
			}//end switch
				//echo $sql;
				return $sql;
		}//end function
		
		public function get_table_fields(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$module = $params[0];
				$field_prefix = $params[1];
				if(is_null($field_prefix)){
					$field_prefix = "post";	
				}//end if
			}//end if				
			if(!isset($module_table)){
				$module = $this->get_module($module);	
				$module_table = $module['module_table'];			
			}//end if
					
			$query = "DESCRIBE ".$module_table;	
											
			$result = $this->db->query($query);
			$error = $this->error($result,__LINE__,get_class($this));
			if($error === false){
				if(!isset($show_all) || (bool)$show_all == false){
					$fields_to_avoid = array($field_prefix."_id",$field_prefix."_title",$field_prefix."_subtitle",$field_prefix."_message",$field_prefix."_abstract",$field_prefix."_module_id",$field_prefix."_date_expire",$field_prefix."_date_update",$field_prefix."_tags");
				} else {
					$fields_to_avoid = array();	
				}
				$num = $result->rowCount();
				if($num > 0){
					$data = array();
					while($row = $result->fetch(PDO::FETCH_ASSOC)){					
						if(!in_array($row['Field'],$fields_to_avoid)){
							if($row['Type'] == "time" || $row['Type'] == "text" || strpos($row['Type'],"varchar") !== false || strpos($row['Type'],"mediumtext") !== false || strpos($row['Type'],"float") !== false || $row['Type'] == "timestamp" || strpos($row['Type'],"int") !== false){
								array_push($data,$row['Field']);
	
							}//end if
						}//end if
					}//end while				
					if(sizeof($data) > 0){
						return $data;
					} else {
						return false;
					}//end if
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		private function read_post_data(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if			
			if(isset($compare_table)){
				$data = array();
				foreach($_POST as $key => $value){				
					if($key != "checkboxes_field_list" && in_array($key,$compare_table)){			
						$data[$key] = $value;
					}//end if
				}//end foreach
			} else {
				$data = array();
				foreach($_POST as $key => $value){				
					if($key != "checkboxes_field_list"){			
						$data[$key] = $value;
					}//end if
				}//end foreach
			}//end if
			//Retrieve all the submitted checkboxes
			$empty_checkboxes = unserialize(base64_decode(urlencode($_POST["checkboxes_field_list"])));
			if(sizeof($empty_checkboxes) > 0 && is_array($empty_checkboxes)){
				//Read all the fields found
				for($i = 0; $i < sizeof($empty_checkboxes); $i++){
					//check for the empty fields							
					if(!array_key_exists($empty_checkboxes[$i],$_POST)){
						$data[$empty_checkboxes[$i]] = "0";						
					}//end if
				}//end for
			}//end if				
			return $data;
		}//end function
		
		private function read_request_data(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			$data = array();
			foreach($_REQUEST as $key => $value){
				if($key != "checkboxes_field_list"){			
					$data[$key] = $value;
				}//end if
			}//end foreach
			
			//Retrieve all the submitted checkboxes
			$empty_checkboxes = unserialize(base64_decode(urlencode($_REQUEST["checkboxes_field_list"])));
			if(sizeof($empty_checkboxes) > 0 && is_array($empty_checkboxes)){
				//Read all the fields found
				for($i = 0; $i < sizeof($empty_checkboxes); $i++){
					//check for the empty fields							
					if(!array_key_exists($empty_checkboxes[$i],$_REQUEST)){
						$data[$empty_checkboxes[$i]] = "0";						
					}//end if
				}//end for
			}//end if
			
			return $data;
		}//end function
		
		private function read_get_data(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			$data = array();
			foreach($_GET as $key => $value){
				$data[$key] = $value;
			}//end foreach
			return $data;
		}//end function
		
		public function clean_prefix(&$item, $key, $prefix){
			$item = substr($item,strlen($prefix)+1);					
		}//end if
		
		private function init_module_permissions(){
			//Create the permission for the admins
			////First, check if the administrator group exists, it usually have the id eq to 1, so find that
			$query = "SELECT COUNT(*) admin_found FROM users_groups WHERE users_group_id = 1";
			$result = $this->db->query($query);
			$this->error($result,__LINE__,get_class($this));
			$row = $result->fetch(PDO::FETCH_ASSOC);
			if($row['admin_found'] > 0){
				//admin group found
				$query = "INSERT INTO ".$this->t."users_permissions
						  (user_permission_gid, user_permission_module_id, user_permission_permission)
						  VALUES
						  (1,".$this->module_id.",7)";
				$result = @$this->db->query($query);
				$this->error($result,__LINE__,get_class($this));
			}//end if
			//Now check if the user that has added the new page is an administrator or not, if not, give the full control to his group
			////Retrieving user id
			$users = $this->call('users');
			$uid = $users->get_uid();
			if($uid == NULL){
				return false;
			}//end if
			////Get the user data			
			$user_data = $users->get_user($uid);
			$Ugid = $user_data['user_group_id'];//User Group ID
			if($Ugid != 1){
				//The group is part of another group, so we need to create the permissions for his group
				$query = "INSERT INTO ".$this->t."users_permissions
						  (user_permission_gid, user_permission_module_id, user_permission_permission)
						  VALUES
						  (".$Ugid.",".$this->module_id.",7)";
				$result = @$this->db->query($query);
				$this->error($result,__LINE__,get_class($this));
			}//end if
		}//end function
			
		
		public function delete_module($module_id){
			if(!is_numeric($module_id)){
				return false;
			}//end if
			$query = "DELETE FROM ".$this->t."modules WHERE module_id = ".$module_id;
			$result = @$this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
				
		public function get_field_prefix_from_table($table){
			if(substr($table,-3) == "ies"){
				$prefix = substr($table,0,-3);
				$prefix .= "y";
			} else if(substr($table,-1) == "s"){
				$prefix = substr($table,0,-1);
			} else {
				$prefix = $table;
			}//end if
			return $prefix;
		}//end if
				
		
		
		#################################################################
		# ATTACHMENTS PART												#		
		#################################################################
		
		public function get_attachment_id(){
			return $this->file_id;
		}//end if
		
		public function get_attachments(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(isset($module)){
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];	
				$module_name = $module_data['module_name'];
			}//end if	
					
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if
			
			if(!isset($check_isolation)){
				$check_isolation = false;	
			}
			
			$isolate = false;
			if($check_isolation){
				$mediaSettings = $this->get_settings(array('module' => 'media'));				
				if($mediaSettings !== false){
					if((bool)$mediaSettings['mediaGalleryIsolateUsers']){
						$isolate = true;	
						$isolation_type = 'hard';
						if($mediaSettings['mediaGalleryIsolatationType'] != ""){
							$isolation_type = $mediaSettings['mediaGalleryIsolatationType'];
						}
					}//end if
				}//end if
			}//end if
			
			if($isolate){	
				switch($isolation_type){
					case 'hard':
					default:
						$query = "SELECT attachments.*, IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name, UNIX_TIMESTAMP(attachment_date) AS attachment_date  
								  FROM (attachments, attachments_posts, posts)						  	
								  WHERE attachment_type IS NOT NULL 
								  AND attachment_post_file_id = attachment_id
								  AND post_id = attachment_post_post_id
								  AND (post_uid = ".$this->get_uid()." OR post_uid IS NULL)";
						 break;
					case 'soft':
						$query = "SELECT attachments.*, IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name, UNIX_TIMESTAMP(attachment_date) AS attachment_date  
								  FROM (attachments)								
								  LEFT JOIN attachments_posts
								  ON attachment_id = attachment_post_file_id
								  LEFT JOIN posts
								  ON attachment_post_post_id = post_id								  	
								  WHERE attachment_type IS NOT NULL
								  AND (post_uid = ".$this->get_uid()." OR post_uid IS NULL)";
						break;
				}//end switch
			} else {
				$query = "SELECT *, IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name, UNIX_TIMESTAMP(attachment_date) AS attachment_date  
						  FROM attachments
						  LEFT JOIN attachments_posts
						  ON attachment_post_file_id =  attachment_id				
						  WHERE attachment_type IS NOT NULL";
			}//end if
			if(isset($post_id)){
				$query .= " AND attachment_post_post_id = ".$post_id;	
			}//end if
			if($attachment_post_type !== "all"){
				$query .= " AND attachment_post_type = '".$attachment_post_type."'";
			}//end if
			if(isset($module_id)){
				$query .= " AND attachment_post_module_id = ".$module_id;
			}//end if
			//check if we pass other params
			if(isset($role)){
				if(!is_array($role)){
					$query .= " AND attachment_post_role = '".$role."'";	
				} else {
					$query .= " AND attachment_post_role IN ('".implode("','",$role)."')";	
				}//end if
			}//end if
			if(isset($status)){
				$query .= " AND attachment_post_status = '".$status."'";	
			}//end if
			if(isset($type)){
				if(!is_array($type)){
					$query .= " AND attachment_type = '".$type."'";	
				} else {
					$query .= " AND attachment_type IN ('".implode("','",$type)."')";	
				}//end if
			}//end if
			
			//ignore list
			if(isset($ignore)){
				if(is_array($ignore)){
					foreach($ignore as $key => $value){
						switch($key){
							case 'role':
								if(!is_array($value)){
									$query .= " AND attachment_post_role != '".$value."'";
								} else {
									$query .= " AND attachment_post_role NOT IN ('".implode("','",$value)."')";
								}//end if
								break;	
							case 'type':
								if(!is_array($value)){
									$query .= " AND attachment_type != '".$value."'";
								} else {
									$query .= " AND attachment_type NOT IN ('".implode("','",$value)."')";
								}//end if
								break;	
						}
					}//end foreach
				}//end if
			}//end if
			
			if(isset($search) && !is_null($search)){
				if(is_array($search)){
					$search_keys = implode("','",$search);
					$query .= " AND (attachment_post_title IN ('".$search_keys."') OR attachment_filename IN ('".$search_keys."'))";	
				} else if(is_string($search)){
					$query .= " AND (attachment_post_title LIKE '%".$search."%' OR attachment_filename LIKE '%".$search."%')";	
				}//end if
			}//end if
			if(!isset($sort_by)){
				//$sort_by = "attachment_date";	
				$sort_by = "attachment_date";	
			}//end if
			if(!isset($sort_order)){
				$sort_order = "DESC";	
			}//end if
			
			if(isset($post_id)){
				$query .= " GROUP BY attachment_id ORDER BY attachment_post_order ASC, ".$sort_by." ".$sort_order;
			} else {
				$query .= " GROUP BY attachment_id ORDER BY ".$sort_by." ".$sort_order;
			}//end if
			
			if(!isset($index)){
				if(isset($support_pagination) && $support_pagination){
					//This is for the paging			
					if($this->paging_isset()){
						$this->set_unpaged_query($query,false);
						$query .= " LIMIT ".$this->p_start.", ".$this->p_limit;				
					}//end if		
				}//end if
				
				if(isset($limit)){
					$query .= " LIMIT ".$limit;	
				}//end if
			} else {
				$query .= " LIMIT ".$index.",1";
			}//end if
			
			//$this->hidden_dump($query);
			//echo $query;
			$result = @$this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){	
					if(isset($limit) && $limit == 1){	
						$data = $this->get_result_array($result,true);
					} else {
						$data = $this->get_result_array($result,false);
					}//end if
					/* META SUPPORT */	
					$meta = $this->call("meta");
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					$meta->set_meta_type('attachment');
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $meta->get_meta(array("id" => $data[$i]['attachment_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $meta->get_meta(array("id" => $data[$i]['attachment_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */						
					if(isset($filesize) && $filesize === true && isset($upload_path)){
						//call the tools class
						$tools = $this->call("tools");
						if(isset($limit) && $limit == 1){
							$folder = "photos/o";
							if($data['attachment_type'] != "image"){
								$folder = "files";	
							}
							$data['fullpath'] = $upload_path."upload/".$folder."/".$data['attachment_filename'];
							if($data['attachment_size'] == 0){
								$data['filesize'] = $tools->get_round_file_size(@filesize($upload_path."upload/".$folder."/".$data['attachment_filename']));
							} else {
								$data['filesize'] = $tools->get_round_file_size($data['attachment_size']);
							}
						} else {
							for($i = 0; $i < sizeof($data); $i++){
								$folder = "photos/o";
								if($data[$i]['attachment_type'] != "image"){
									$folder = "files";	
								}
								$data[$i]['fullpath'] = $upload_path."upload/".$folder."/".$data[$i]['attachment_filename'];
								if($data[$i]['attachment_size'] == 0){
									$data[$i]['filesize'] = $tools->get_round_file_size(@filesize($upload_path."upload/".$folder."/".$data[$i]['attachment_filename']));
								} else {
									$data[$i]['filesize'] = $tools->get_round_file_size($data[$i]['attachment_size']);
								}
							}//end for i
						}//end if
					}//end if
					
					if(isset($transcodes)){
						if($transcodes == true){							
							if(isset($limit) && $limit == 1){
								$transcodes = $this->get_transcodes(array('file_id' => $data['attachment_id']));
								if($transcodes !== false){
									$data['transcodes'] = $transcodes;
								}//end if
							} else {
								for($i = 0; $i < sizeof($data); $i++){
									$transcodes = $this->get_transcodes(array('file_id' => $data[$i]['attachment_id']));
									if($transcodes !== false){
										$data[$i]['transcodes'] = $transcodes;
									}//end if
								}//end for i
							}//end if
						}//end if
					}//end if
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_attachment(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//we need both the file_id and post_id to retrieve the attachment information
			if(!isset($file_id)){
				return false;
			}//end if
			
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if
			
			//in case we pass the post_id and the module_id as long with the file_id, we retrieve the full informations
			if(isset($post_id) && isset($module)){
				$query = "SELECT *, IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name, UNIX_TIMESTAMP(attachment_date) AS attachment_date
						  FROM attachments, attachments_posts
						  WHERE attachment_post_file_id =  attachment_id
						  AND attachment_id = ".$file_id."
						  AND attachment_post_type = '".$attachment_post_type."'
						  AND attachment_post_post_id = ".$post_id;
				if(isset($module)){
					$module_data = $this->get_module($module);
					$module_id = $module_data['module_id'];					
					$query .= " AND attachment_post_module_id = ".$module_id;
				}//end if
				
				if(isset($role)){
					$query .= " AND attachment_post_role = '".$role."'";	
				}//end if
			} else {
				//in case we only pass the file_id, we get the minimal information we can get with the attachments table
				$query = "SELECT *, IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name, UNIX_TIMESTAMP(attachment_date) AS attachment_date  
						  FROM attachments
						  WHERE attachment_id = ".$file_id."";
			}//end if
			
			$query .= " LIMIT 1";		
			
			
			$result = @$this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){		
					$data = $this->get_result_array($result,true);
					if(isset($get_posts) && (bool)$get_posts == true){
						$posts = $this->get_attachment_posts(array("file_id" => $file_id));
						$data['posts'] = $posts;						
					}//end if
					if(isset($get_categories) && (bool)$get_categories == true){
						$categories = $this->get_attachment_posts(array("file_id" => $file_id,'attachment_post_type' => 'category'));
						$data['categories'] = $categories;						
					}//end if
					if(isset($filesize) && $filesize === true && isset($upload_path)){
						//call the tools class
						$tools = $this->call("tools");
						
						$folder = "photos/o";
						if($data['attachment_type'] != "image"){
							$folder = "files";	
						}
						$data['fullpath'] = $upload_path."upload/".$folder."/".$data['attachment_filename'];
						$data['filesize'] = $tools->get_round_file_size(@filesize($upload_path."upload/".$folder."/".$data['attachment_filename']));
						
					}//end if
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function get_attachment_posts(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//we need both the file_id and post_id to retrieve the attachment information
			if(!isset($file_id)){
				return false;
			}//end if
			
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if
			
			switch($attachment_post_type){
				case "post":
					$query = "SELECT post_title, post_id, module_id, module_name, attachment_post_id
							  FROM (attachments_posts, modules)
							  LEFT JOIN posts
							  ON (attachment_post_post_id = post_id AND attachment_post_type = 'post')					
							  WHERE attachment_post_file_id = ".$file_id."
							  AND attachment_post_module_id = module_id
							  AND post_id IS NOT NULL";	
				break;
				case "category":
					$query = "SELECT category_name, category_id, module_id, module_name, attachment_post_id
							  FROM (attachments_posts, modules)
							  LEFT JOIN categories
							  ON (attachment_post_post_id = category_id AND attachment_post_type = 'category')					
							  WHERE attachment_post_file_id = ".$file_id."
							  AND attachment_post_module_id = module_id
							  AND category_id IS NOT NULL";	
				break;
			}//end switch
			$result = @$this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){		
					$data = $this->get_result_array($result,false);				
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		//This function save the attachment data in the database
		public function save_attachment(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least the upload class istance
			if(!isset($upload_class) && !isset($filename)){
				return false;	
			}//end if
			
			if(isset($post_id) && !isset($attachment_post_type)){
				//default
				$attachment_post_type = 'post';	
			}//end if
			
			if(isset($upload_class)){
				$filename = $upload_class->get_filename();
				if(!isset($mime)){
					$mime =  $upload_class->get_mime();
				}//end if
			} else if(!isset($upload_class) && isset($filename)){
				$tools = $this->call('tools');
				if(!isset($mime)){
					$mime = $tools->get_mime_from_filename($filename);
				}//end if
			}//end if
			
			//forcing to get a proper mime
			if(strpos($mime,"unknown") !== false){
				$tools = $this->call('tools');
				$mime = $tools->get_mime_from_filename($filename);
			}//end if
			
			if(!isset($type)){	
				$type = $this->get_file_type(array("mime" => $mime));
				if($type == false || $mime == 'application/force-download'){
					$type = $this->get_file_type(array("filename" => $filename));
				}//end if
				if($type == false){
					$type = "file";
				}//end if
			}//end if	
			
			if(!isset($size)){	
				$size = "0";
			}//end if		
			
			$size = (int)$size;	
		
			$query = "INSERT INTO ".$this->t."attachments
					  (attachment_filename,attachment_mime, attachment_type, attachment_size)
					  VALUES
					  ('".$filename."','".$mime."','".$type."', '".$size."')";		
			$result = $this->db->query($query);		
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->file_id = $this->db->lastInsertId();
				//check if we passed the post_id and the module, so we can create a relation with a specific post
				if(isset($post_id) && isset($module)){
					//so now we have saved the attachment basic info in the database, the next step it will be to associate this file to a post
					$params = array("file_id" => $this->file_id, "post_id" => $post_id, "module" => $module);
					if(isset($title)){
						$params['title'] = $title;
					}//end if
					if(isset($caption)){
						$params['caption'] = $caption;
					}//end if
					if(isset($attachment_post_type)){
						$params['attachment_post_type'] = $attachment_post_type;	
					}//end if
					if(isset($upload_path)){
						$params['upload_path'] = $upload_path;	
					}//end if
					if(isset($role)){
						$params['role'] = $role;
					}//end if
					$this->add_attachment_to_post($params);
				}//end if
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function save_post_attachments_sorting(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($post_id) || !isset($ids) || !isset($module)){
				return false;
			}//end if
		
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if
		
			$module_data = $this->get_module($module);
			$module_id = $module_data['module_id'];		
			
			if(is_array($ids)){
				for($i = 0; $i < sizeof($ids); $i++){
					$query = "UPDATE attachments_posts SET 
							  attachment_post_order = ".$i."
							  WHERE attachment_post_file_id = ".$ids[$i]."
							  AND attachment_post_post_id = ".$post_id."
							  AND attachment_post_module_id = ".$module_id."
							  AND attachment_post_type = '".$attachment_post_type."'";
					$result = $this->db->query($query);		
					$errors = $this->error($result,__LINE__,get_class($this));
				}//end for i
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		//create a link between a post and an attachment
		public function add_attachment_to_post(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least these three vars passed
			if(!isset($file_id) && !isset($post_id) && !isset($module)){
				return false;	
			}//end if
			
			if(!is_numeric($module)){
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];	
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $module;	
			}//end if
			
			//get the settings for media
			$mediaSettings = $this->get_settings(array('module' => 'media'));
						
			//default values
			if(!isset($title)){
				$title = "";	
			}//end if
			if(!isset($caption)){
				$caption = "";	
			}//end if			
			if(!isset($role)){
				$role = "generic";	
				if(isset($mediaSettings['mediaDefaultRole']) && !is_null($mediaSettings['mediaDefaultRole'])){
					$role = $mediaSettings['mediaDefaultRole'];	
				}//end if
			}//end if
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if		
			if(!isset($status)){
				//the status can be published | hidden | protected
				$status = "published";	
			}//end if
			
			$query = "INSERT INTO attachments_posts
					  (attachment_post_file_id, attachment_post_post_id, attachment_post_module_id, attachment_post_role, attachment_post_title, attachment_post_caption, attachment_post_status, attachment_post_type)
					  VALUES
					  (".$file_id.",".$post_id.",".$module_id.",'".$role."',".$this->db->quote($title).",".$this->db->quote($caption).",'".$status."','".$attachment_post_type."')";
			$this->cache = $query;		  
			$result = $this->db->query($query);			
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){	
				$check_options = array(
					'file_id' => $file_id,
					'post_id' => $post_id,
					'module' => $module_id
				);
				if(isset($upload_path)){
					$check_options['upload_path'] = $upload_path;
				}//end if
				$this->check_missing_formats($check_options);			
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function load_S3(){
			require_once($_SERVER['DOCUMENT_ROOT'].__CMS_PATH__."inc/S3/S3.php");	
		}//end function
		
		public function load_S3_ET(){
			require_once($_SERVER['DOCUMENT_ROOT'].__CMS_PATH__."inc/elasticTranscoder/ElasticTranscoder.php");	
		}//end function
		
		public function post_upload_video_actions(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least the file_id
			if(!isset($file_id)){
				return false;	
			}//end if
			
			//get the file info
			$file = $this->get_attachment(array('file_id' => $file_id));
			if($file === false){
				return false;	
			}//end if
			
			if(!isset($upload_path)){
				$upload_path = "../../";	
			}//end if
			
			if($file['attachment_type'] == "image"){
				$upload_path .= 'upload/photos/o/';
			} else {
				$upload_path .= 'upload/files/';
			}//end if
			
			$mediaSettings = $this->get_settings(array('module' => 'media'));
			if($mediaSettings !== false){
				if((bool)$mediaSettings['mediaS3ForceCopy'] === true){
					//we need to check if we have the S3 credentials
					$accessKey = $mediaSettings['mediaS3AccessKey'];
					$secretKey = $mediaSettings['mediaS3SecretKey'];
					$bucket = $mediaSettings['mediaS3DestinationBucket'];
					if(!is_null($accessKey) && !is_null($secretKey) && !is_null($bucket)){
						//if we have the credentials, we load the S3 Libraries
						$this->load_S3();
						
						$s3 = new S3($accessKey, $secretKey);
						$result = $s3->putObject(S3::inputFile($upload_path.$file['attachment_filename'], false), $bucket, $file['attachment_filename'], S3::ACL_PUBLIC_READ);
						if($result == true){
							//if the file has been uploaded successfully on S3, we check if we need to remove the original file
							if($mediaSettings['mediaS3RemoveOriginal'] == 1){
								//now we need to remove the original file
								try{
									$url = 'https://'.$mediaSettings['mediaS3Region'].'.amazonaws.com/'.$bucket.'/'.$file['attachment_filename'];
									//now we transform the current attachment to a remote attachment
									$query = "UPDATE attachments SET 
											  attachment_type = 'url',
											  attachment_url = '".$url."',
											  attachment_external_service = 'amazonS3'
											  WHERE attachment_id = ".$file['attachment_id'];
									$result = $this->db->query($query);
									$errors = $this->error($result,__LINE__,get_class($this));
									if($errors === false){				
										unlink($upload_path.$file['attachment_filename']);
										return true;
									} else {
										throw new PDOException("Cannot update the file");
									}//end if
								} catch(PDOException $ex){
									$this->errors = $ex->getMessage();
									return false;	
								}//end try
							}//end if
							return true;
						} else {
							return false;	
						}
					}//end if
				} else {
					//nothing to do, return true
					return true;	
				}//end if				
			} else {
				//we can't read the settings
				return false;
			}//end if	
		}//end function
		
		private function check_missing_formats(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least these three vars passed
			if(!isset($file_id) && !isset($post_id) && !isset($module)){
				return false;	
			}//end if
			
			if(!is_numeric($module)){
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];	
				$module_name = $module_data['module_name'];	
			} else {
				$module_id = $module;	
			}//end if
			
			if(!isset($upload_path)){
				$uplaod_path = 	"../../upload/";
			}
			
			$file_data = $this->get_attachment(array('file_id' => $file_id));
			if($file_data !== false){
				if($file_data['attachment_type'] == 'image'){
					$formats = $this->get_attachments_formats(array('module' => $module));
					if($formats !== false){
						$image = $this->call('image');
						for($i = 0; $i < sizeof($formats); $i++){
							if(!file_exists(__SERVER_PATH__.'upload/photos/'.$formats[$i]['attachment_setting_folder'].'/'.$file_data['attachment_filename'])){
								$params = array(
									"file_id" => $file_id,
									"format_id" => $formats[$i]['attachment_setting_id'],
									"module" => $module_id,
									"post_id" => $post_id,
									"upload_path" => $upload_path
								);							
								$image->create_new_size($params);	
							}//end if
						}//end for i
						return true;
					} else {
						return false;
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		//this function simply check if the file_id passed is owned by the post_id passed
		public function check_attachment_post_ownership(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least these three vars passed
			if(!isset($post_id) || !isset($module) || !isset($file_id)){
				return false;	
			}//end if	
			
			if(!is_numeric($module)){
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];	
			} else {
				$module_id = $module;	
			}//end if
			
			$query = "SELECT * FROM attachments_posts
					  WHERE attachment_post_post_id = ".$post_id."
					  AND attachment_post_module_id = ".$module_id."
					  AND attachment_post_file_id = ".$file_id;
			$result = $this->db->query($query);			
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){				
				$num = $result->rowCount();
				if($num > 0){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_attachment_post_id(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least these vars passed
			if(!isset($post_id) || !isset($module) || !isset($file_id)){
				return false;	
			}//end if	
			
			if(!is_numeric($module)){
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];	
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $module;	
			}//end if
			
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';
			}//end if
			
			try{
				$query = "SELECT attachment_post_id FROM attachments_posts
						  WHERE attachment_post_post_id = :post_id
						  AND attachment_post_module_id = :module_id
						  AND attachment_post_type = :attachment_post_type
						  AND attachment_post_file_id = :file_id
						  LIMIT 1";
				$result = $this->db->prepare($query);
				$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);	
				$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
				$result->bindParam(':file_id', $file_id, PDO::PARAM_STR);
				$result->bindParam(':attachment_post_type',$attachment_post_type,PDO::PARAM_STR);
				$result->execute();
				$errors = $this->error($result,__LINE__,get_class($this));
				if($errors === false){
					$num = $result->rowCount();
					if($num > 0){
						$row = $result->fetch(PDO::FETCH_ASSOC);			
						return $row['attachment_post_id'];
					} else {
						return false;	
					}//end if					
				} else {
					return false;	
				}//end if
			} catch(PDOException $ex){
				$this->errors = $ex->getMessage();
				return false;	
			}//end try	
						
		}//end function
		
		public function update_post_attachment(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least these vars passed
			if(!isset($post_file_id) || !isset($post_id) || !isset($module) || !isset($field) || !isset($file_id)){
				return false;	
			}//end if	
						
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';
			}//end if
						
			if(!is_numeric($module)){
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];	
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $module;	
			}//end if
			
			//get current file data
			$file_options = array('file_id' => $file_id, 'module' => $module, 'post_id' => $post_id,'attachment_post_type' => $attachment_post_type);			
			$file_data = $this->get_attachment($file_options);		
			
			$current_role = $file_data['attachment_post_role'];
			$current_type = $file_data['attachment_type'];
							
			$prefix = 'attachment_post_';
			if(!is_array($field)){			
				//if it's just one field		
				$field = $prefix.$field;
				//begin transaction
				$this->db->beginTransaction();
				try{					
					//check if we want this field unique
					if($unique === true && isset($unique_default)){						
						$query = "SELECT * FROM attachments_posts, attachments
								  WHERE attachment_post_post_id = :post_id
								  AND attachment_id = attachment_post_file_id
								  AND attachment_post_module_id = :module_id
								  AND attachment_post_type = :attachment_post_type
								  AND ".$field." = :field_value";								  
						if($field == 'attachment_post_role'){
							$query .= " AND attachment_type = :type";	
							if($field_value == 'default-image'){
								$query .= " AND attachment_post_role = 'default-image'";	
							}//end if
						}//end if		
						$result = $this->db->prepare($query);
						$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);	
						$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
						$result->bindParam(':field_value', $field_value, PDO::PARAM_STR);
						$result->bindParam(':attachment_post_type',$attachment_post_type,PDO::PARAM_STR);
						if($field == 'attachment_post_role'){
							$result->bindParam(':type',$current_type,PDO::PARAM_STR);
						}//end if
						$result->execute();
						$errors = $this->error($result,__LINE__,get_class($this));
						if($errors === false){
							$num = $result->rowCount();
							if($num > 0){
								//if we found something, we reset those records to the default value passed
								$query = "UPDATE attachments_posts, attachments 
										  SET ".$field." = :field_value 
										  WHERE attachment_post_post_id = :post_id
										  AND attachment_id = attachment_post_file_id
										  AND attachment_post_type = :attachment_post_type
										  AND attachment_post_module_id = :module_id";
								if($field == 'attachment_post_role'){
									$query .= " AND attachment_type = :type";	
									if($field_value == 'default-image'){
										$query .= " AND attachment_post_role = 'default-image'";	
										$unique_default = $current_role;
									}//end if
								}//end if				
								$result = $this->db->prepare($query);
								$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);				
								$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
								$result->bindParam(':field_value', $unique_default, PDO::PARAM_STR);
								$result->bindParam(':attachment_post_type',$attachment_post_type,PDO::PARAM_STR);
								if($field == 'attachment_post_role'){
									$result->bindParam(':type',$current_type,PDO::PARAM_STR);
								}//end if
								$result->execute();
								$errors = $this->error($result,__LINE__,get_class($this));				
							}//end if
						}//end if
					}//end if
					
					$query = "UPDATE attachments_posts 
							  SET ".$field." = :field_value 
							  WHERE attachment_post_id = :id 
							  AND attachment_post_file_id = :file_id
							  AND attachment_post_post_id = :post_id
							  AND attachment_post_type = :attachment_post_type
							  AND attachment_post_module_id = :module_id";				
					$result = $this->db->prepare($query);
					$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);
					$result->bindParam(':file_id', $file_id, PDO::PARAM_INT);
					$result->bindParam(':id', $post_file_id, PDO::PARAM_INT);
					$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
					$result->bindParam(':attachment_post_type',$attachment_post_type,PDO::PARAM_STR);
					$result->bindParam(':field_value', $field_value, PDO::PARAM_STR);
					$result->execute();
					//echo $result->queryString;
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors === false){
						$this->db->commit();
						return true;
					} else {
						return false;
					}//end if
				} catch(PDOException $ex){
					$this->db->rollback();
					$this->errors = $ex->getMessage();
					return false;	
				}//end try	
			} else if(is_array($field)){
				//if we send an array of fields, then we execute this code
				try{
					$query = "UPDATE attachments_posts 
							  SET ";
					$i = 1;
					foreach($field as $key => $value){
						$query .= " attachment_post_".$key." = ".$this->db->quote($value);
						if($i < (sizeof($field))){
							$query .= ", ";	
						}
						$i++;
					}//end for i
							 // .$field." = :field_value 
					$query .= " WHERE attachment_post_id = :id 
							  AND attachment_post_file_id = :file_id
							  AND attachment_post_post_id = :post_id
							  AND attachment_post_type = :attachment_post_type
							  AND attachment_post_module_id = :module_id";		
					
					$result = $this->db->prepare($query);	 					
					$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);
					$result->bindParam(':file_id', $file_id, PDO::PARAM_INT);
					$result->bindParam(':id', $post_file_id, PDO::PARAM_INT);
					$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
					$result->bindParam(':attachment_post_type',$attachment_post_type,PDO::PARAM_STR);
					$result->execute();
					//echo $result->queryString;
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors === false){
						return true;
					} else {
						return false;
					}//end if
				} catch(PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end try	
			}//end if
		}//end function
		
		public function transcode_video(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least the url address
			if(!isset($file_id)){
				return false;	
			}//end if	
			
			$file_data = $this->get_attachment(array('file_id' => $file_id));
			if($file_data === false){
				return false;	
			}//end if
			
			$mediaSettings = $this->get_settings(array('module' => 'media'));
			if($mediaSettings !== false){				
				//we need to check if we have the S3 credentials
				$accessKey = $mediaSettings['mediaS3AccessKey'];
				$secretKey = $mediaSettings['mediaS3SecretKey'];
				
				if(!is_null($accessKey) && !is_null($secretKey)){
					$this->load_S3_ET();
					$awsRegion = $mediaSettings['mediaS3Region'];
					$awsRegion = str_replace('s3-','',$awsRegion);					
					$et = new AWS_ET($accessKey, $secretKey, $awsRegion);
					
					$pipelineId = $mediaSettings['mediaS3Pipeline'];
					if($pipelineId == ""){
						return false;	
					}//end if
					
					//get the presets 
					$presets = unserialize($mediaSettings['mediaS3ElasticPreset']);
					if(is_array($presets)){
						for($i = 0; $i < sizeof($presets); $i++){
							
							$preset_id = $presets[$i];
							$preset_data = $et->readPreset($presets[$i]);
							$preset_data = $preset_data['Preset'];
							$preset_ext = $preset_data['Container'];
							$preset_prefix = explode("-",$preset_id);
							$preset_prefix = $preset_prefix[1];
						
							$src_file = $file_data['attachment_filename'];
							$lastdot = strrpos($src_file,'.');
							$dest_file = substr($src_file,0,$lastdot);
							$thumb = $dest_file.'-'.$preset_prefix.'-{count}';
							$dest_file .= '-'.$preset_prefix.'.'.$preset_ext;
							
							$input = array('Key' => $src_file);
							$output = array(
							  'Key' => $dest_file,							  
							  'PresetId' => $preset_id,
							  'ThumbnailPattern' => 'thumbs/'.$thumb
							 );
							
							$outputFolder = $mediaSettings['mediaS3TranscodedDestFolder'];
							
							$result = AWS_ET::createJob($input, array($output), $pipelineId,$outputFolder);
							
							if($result) {
								//var_dump($result['Job']);
								$url = "https://s3-".$awsRegion.".amazonaws.com/".$mediaSettings['mediaS3DestinationBucket']."/".$outputFolder.$dest_file;
								$url_thumbs = "https://s3-".$awsRegion.".amazonaws.com/".$mediaSettings['mediaS3DestinationBucket']."/".$outputFolder."thumbs/".$thumb.".png"; 
								$url_thumbs = str_replace("{count}","00001",$url_thumbs);
							 	//echo 'New job ID: ' . $result['Job']['Id'];
								$query = "INSERT INTO attachments_transcodes
										  (attachment_transcode_file_id, attachment_transcode_preset_id, attachment_transcode_filetype, 
										  attachment_transcode_filename, attachment_transcode_uri, attachment_transcode_job_id, 
										  attachment_transcode_job_response, attachment_transcode_thumbnail)
										  VALUES
										  (".$file_id.",'".$preset_id."','".$preset_ext."','".$dest_file."','".$url."',
										  '".$result['Job']['Id']."','".addslashes(json_encode($result))."','".$url_thumbs."')";
								$query_result = $this->db->query($query);
								$errors = $this->error($query_result,__LINE__,get_class($this));			
							} else {
								 //echo AWS_ET::getErrorMsg();	
								 return false;							
							}//end if
							
							//now we set the file as transcoded
							$query = "UPDATE attachments SET attachment_video_transcoded = 1 WHERE attachment_id = ".$file_id;
							$result = $this->db->query($query);
							$errors = $this->error($result,__LINE__,get_class($this));	
						}//end for i
						return true;
					}//end if
				}//end if
			}//end if
		}//end function
		
		public function get_transcodes(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least the url address
			if(!isset($file_id)){
				return false;	
			}//end if	
			
			$query = "SELECT * FROM attachments_transcodes WHERE attachment_transcode_file_id = :file_id";
			$result = $this->db->prepare($query);
			$result->bindParam(':file_id', $file_id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->error($result,__LINE__,get_class($this));		
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->get_result_array($result,false);	
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		//This function save the attachment data in the database
		public function attach_url(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least the url address
			if(!isset($url)){
				return false;	
			}//end if			
			
			if(!isset($type)){				
				$type = "url";				
			}//end if
			
			if(!isset($size)){	
				$size = "0";
			}//end if	
			
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if
			
			//get the service
			$service = $this->get_url_service_name($url);	
			//get the thumbnail
			$thumbnail = $this->get_url_thumbnail(array("url" => $url, "service" => $service));
			$filename = "Imported from ".ucwords($service); 
			
			if(!isset($mime)){
				$mime = 'unknown';	
			}//end if
	
			switch($service){
				default:
					$filename = $url;
					if(!isset($role)){						
						$role = "website";
					}//end if
					$type = 'url-website';
					break;
				case "flickr":
					if(!isset($role)){
						$role = "generic";
					}//end if
					break;
				case "vimeo":
					//if we are importing a video from vimeo, we get the info
					$vimeo = $this->call("vimeo");
					$vimeo_data = $vimeo->get_video_data($url);					
					if($vimeo_data !== false){
						$filename = $vimeo_data[0]['title'];	
					}//end if
					if(!isset($role)){
						$role = "video";
					}//end if
					break;
				case "youtube":
					//if we are importing a video from youtube, we get the info
					$youtube = $this->call("youtube");
					$youtube_data = $youtube->get_video_data($url);
					if($youtube_data !== false){
						$filename = $youtube_data['title'];
					}//end if
					if(!isset($role)){
						$role = "video";	
					}//end if
					break;
				case "amazonS3":
					//if we are importing a file from Amazon
					$url_array = explode('/',$url);
					$filename = array_pop($url_array);
					if(!isset($role)){										
						$role = "amazon-file";
					}//end if
					break;
			}//end switch
			
			$title = $filename;	
			$size = (int)$size;
		
			$query = "INSERT INTO ".$this->t."attachments
					  (attachment_filename, attachment_url, attachment_mime, attachment_size, attachment_type, attachment_external_service, attachment_external_thumb)
					  VALUES
					  (".$this->db->quote($filename).",'".$url."','".$mime."','".$size."','".$type."','".$service."','".$thumbnail."')";	
					 // echo $query;									  			
			$result = $this->db->query($query);		
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->file_id = $this->db->lastInsertId();
				//check if we passed the post_id and the module, so we can create a relation with a specific post
				if(isset($post_id) && isset($module)){
					//so now we have saved the attachment basic info in the database, the next step it will be to associate this file to a post
					$params = array("file_id" => $this->file_id, "post_id" => $post_id, "module" => $module);
					if(isset($title)){
						$params['title'] = $title;
					}//end if
					if(isset($caption)){
						$params['caption'] = $caption;
					}//end if
					if(isset($role)){
						$params['role'] = $role;	
					}//end if					
					if(isset($attachment_post_type)){
						$params['attachment_post_type'] = $attachment_post_type;	
					}//end if
					$this->add_attachment_to_post($params);
				}//end if
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		protected function get_url_service_name($url){
			//strip the http:// out
			$url = str_replace("http://","",$url);
			//strip the http:// out
			$url = str_replace("https://","",$url);
			//strip the www. out
			$url = str_replace("www.","",$url);
			//now we should have a link like youtube.com/watch...etc
			$url = explode("/",$url);
			$url = $url[0];//at this point we should have just youtube.com
			//get rid of the first level domain
			$lastdot = strrpos($url,".");
			if($lastdot !== false){
				$url = substr($url,0,$lastdot);
				//do it again to remove third level domains
				$lastdot = strrpos($url,".");
				$url = substr($url,$lastdot,strlen($url));
			}//end if
			
			switch($url){
				case (strpos($url,"youtube") !== false):
					$service = "youtube";
					break;
				case (strpos($url,"flickr") !== false):
					$service = "flickr";
					break;
				case (strpos($url,"vimeo") !== false):
					$service = "vimeo";
					break;
				case (strpos($url,"facebook") !== false):
					$service = "facebook";
					break;
				case (strpos($url,"amazonaws") !== false):
					$service = "amazonS3";
					break;
				default:
					$service = $url;
			}//end switch
			
			return $service;
		}//end function
		
		protected function get_url_thumbnail(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least the url address
			if(!isset($url)){
				return false;	
			}//end if			
			
			if(!isset($service)){
				$service = $this->get_url_service_name($url);
			}//end if
			
			switch($service){
				case "flickr":
					$thumb_uri = str_replace("_b.jpg","_m.jpg",$url);
					return $thumb_uri;
					break;
				case "vimeo":
					$vimeo = $this->call("vimeo");
					$vimeo_data = $vimeo->get_video_data($url);	
					if($vimeo_data !== false){
						$thumb_uri = $vimeo_data[0]['thumbnail_medium'];
						return $thumb_uri;
					} else {
						return NULL;	
					}//end if
					break;
				case "youtube":
					$youtube = $this->call("youtube");
					$youtube_data = $youtube->get_video_data($url);
					if($youtube_data !== false){
						$thumb_uri = $youtube_data['thumbnail_large'];
						return $thumb_uri;
					} else {
						return NULL;
					}//end if
					break;
				default:
					return NULL;	
			}//end switch
		}//end function
		
		public function is_attachment_shared(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//we need to pass the file_id 
			if(!isset($file_id)){
				return false;	
			}//end if
			
			$query = "SELECT * FROM attachments_posts WHERE attachment_post_file_id = ".$file_id;
			$result = @$this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 1){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function delete_attachment(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//we need to pass the file_id 
			if(!isset($file_id)){
				return false;	
			}//end if
			
			if(!isset($upload_path)){
				$upload_path = "../../upload/";	
			}//end if
			
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if
			
			$meta = $this->call('meta');
			
			//first thing we need to retrieve the file attachment basic info
			$options = array("file_id" => $file_id);
			if(isset($post_id) && isset($module)){	
				$module_data = $this->get_module($module);
				$module_id = $module_data['module_id'];							
				$options['post_id'] = $post_id;
				$options['module'] = $module;
				$options['attachment_post_type'] = $attachment_post_type;
			}//end if
			$file_data = $this->get_attachment($options);
			if($file_data !== false){
				//now we have to check if the file is shared or not
				$is_shared = $this->is_attachment_shared(array('file_id' => $file_id));
				if(!$is_shared){
					//it's  not shared, so we can delete as we are sure that the file is not used anywhere else
					//get the filename
					$filename = $file_data['attachment_filename'];
					//get the type
					$file_type = $file_data['attachment_type'];
					//if we have to delete an image, we need to remove all the copies of the file
					switch($file_type){
						case "image":
							//get the formats
							$formats = $this->get_attachments_formats();
							if($formats !== false){
								for($i = 0; $i < sizeof($formats); $i++){
									if(file_exists($upload_path."photos/".$formats[$i]['attachment_setting_folder']."/".$filename)){
										@unlink($upload_path."photos/".$formats[$i]['attachment_setting_folder']."/".$filename);	
									}//end if
								}//end for i
							}//end if					
							//remove the original image as well
							if(file_exists($upload_path."photos/o/".$filename)){
								@unlink($upload_path."photos/o/".$filename);	
							}//end if
							break;
						case "file":
							if(file_exists($upload_path."files/".$filename)){
								@unlink($upload_path."files/".$filename);	
							}//end if
							break;
						case "url":
							$this->delete_remote_file(array('file_data' => $file_data));
							break;
						default:
							//do nothing						
					}//end switch
					
					//Great, now the files are gone, so we just need to delete the references in the database
					//First delete any relation in the attachments_posts
					$query = "DELETE FROM attachments_posts WHERE attachment_post_file_id = ".$file_id;
					$result = $this->db->query($query);			
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors === false){
						//everything went fine so far, so last thing to remove is the main record in the attachments table
						$query = "DELETE FROM attachments WHERE attachment_id = ".$file_id;
						$result = $this->db->query($query);			
						$errors = $this->error($result,__LINE__,get_class($this));
						if($errors === false){
							//Delete the related meta
							$meta->delete_meta(array('post_id' => $file_id, 'type' => 'attachment'));
							//all done!
							return true;
						} else {
							//something in the last step went wrong
							return false;
						}//end if
					} else {
						//we deleted the files but for some reasons not the records in the database
						return false;
					}//end if
				} else {
					//the file is shared with someone else, so we just remote the relation between the file and that 
					//specific post_id, if a post_id is passed
					if(isset($post_id)){
						$query = "DELETE FROM attachments_posts 
								  WHERE attachment_post_file_id = ".$file_id." 
								  AND attachment_post_post_id = ".$post_id."
								  AND attachment_post_module_id = ".$module_id;
						$result = $this->db->query($query);			
						$errors = $this->error($result,__LINE__,get_class($this));
						if($errors === false){
							//Delete the related meta
							$meta->delete_meta(array('post_id' => $file_id, 'type' => 'attachment'));
							return true;
						} else {
							return false;	
						}//end if
					} else {
						return false;
					}//end if
				}//end if
			} else {
				//if nothing is found
				return false;
			}//end if
		}//end function
		
		public function delete_remote_file(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($file_data) || $file_data === false){
				return false;	
			}//end if
			
			$mediaSettings = $this->get_settings(array('module' => 'media'));
			if($mediaSettings == false){
				return false;
			}//end if
			
			switch($file_data['attachment_external_service']){
				case "amazonS3":
					if($mediaSettings['mediaS3DontRemoveFile'] != 1){
						$accessKey = $mediaSettings['mediaS3AccessKey'];
						$secretKey = $mediaSettings['mediaS3SecretKey'];
						$bucket = $mediaSettings['mediaS3DestinationBucket'];
						if(!is_null($accessKey) && !is_null($secretKey) && !is_null($bucket)){
							//if we have the credentials, we load the S3 Libraries
							$this->load_S3();						
							$s3 = new S3($accessKey, $secretKey);	
							$s3->deleteObject($bucket, $file_data['attachment_filename']);				
						}//end if
					}//end if
					break;
				default:
					//do nothing
			}//end switch
			return true;
		}//end function
		
		//set a value in the attachments_posts table, parameters to pass are attachment_post_id, label and value
		public function update_attachment_field(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//be sure that we have at least these three vars passed
			if(!isset($attachment_post_id) && !isset($label) && !isset($value)){
				return false;	
			}//end if
			
			$query = "UPDATE attachments_posts SET ".$label." = '".$value."' WHERE  attachment_post_id = ".$attachment_post_id;
			$result = $this->db->query($query);			
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_attachments_formats(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(isset($module)){
				$module_settings = $this->get_settings(array("module_id" =>$module));
				if(!is_null($module_settings['moduleDefaultImageFormats'])){
					$extra_formats = $module_settings['moduleDefaultImageFormats'];				
				}//end if
			}//end if
				
			$query = "SELECT * FROM attachments_settings";
			if(isset($default)){
				$query .= " WHERE attachment_setting_default = ".(int)$default;
				if(isset($extra_formats) && $extra_formats != ""){
					$query .= " OR attachment_setting_id IN (".$extra_formats.")";					
				}//end if
			} else {
				if(isset($extra_formats) && $extra_formats != ""){
					$query .= " WHERE attachment_setting_id IN (".$extra_formats.")";					
				}//end if
			}//end if
			$query .= " ORDER BY attachment_setting_id ASC";		
		
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){						
					$data = $this->get_result_array($result,false);					
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_attachment_format(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($id)){
				return false;	
			}//end if
			
			$query = "SELECT * FROM attachments_settings
					  WHERE attachment_setting_id = ".$id."
					  ORDER BY attachment_setting_id ASC
					  LIMIT 1";
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){						
					$data = $this->get_result_array($result,true);					
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function save_attachment_format(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			$default = $_POST['attachment_default'];
			if(is_null($default)){
				$default = "0";	
			}//end if
			
			$width = (int)$_POST['attachment_width'];
			$height = (int)$_POST['attachment_height'];
			$processing = "crop";
			if($width == 0 || $height  == 0){
				$processing = "scale";	
			}//end if
			
			if(!is_writable($_SERVER["DOCUMENT_ROOT"].__SERVERPATH__."upload/photos/")){			
				return false;	
			}//end if
			
			if(!isset($id)){
				//new format
				$query = "INSERT INTO attachments_settings
						  (attachment_setting_label, attachment_setting_folder, attachment_setting_width, attachment_setting_height, 
						  attachment_setting_default, attachment_setting_processing)
						  VALUES
						  (:attachment_label,:attachment_folder,:attachment_width,:attachment_height, :default, :processing)";
			} else {
				//update a format
				$query = "UPDATE attachments_settings SET 
						  attachment_setting_label = :attachment_label,						 
						  attachment_setting_width = :attachment_width,
						  attachment_setting_height = :attachment_height,
						  attachment_setting_default = :default,
						  attachment_setting_processing = :processing
						  WHERE attachment_setting_id = :id";
			}//end if
			$result = $this->db->prepare($query);
			$result->bindParam(':attachment_label', $_POST['attachment_label'], PDO::PARAM_STR);
			$result->bindParam(':attachment_width', $_POST['attachment_width'], PDO::PARAM_STR);
			$result->bindParam(':attachment_height', $_POST['attachment_height'], PDO::PARAM_STR);			
			$result->bindParam(':default', $default, PDO::PARAM_INT);
			$result->bindParam(':processing', $processing, PDO::PARAM_STR);
			if(!isset($id)){
				$result->bindParam(':attachment_folder', $_POST['attachment_folder'], PDO::PARAM_STR);
			} else {
				$result->bindParam(':id', $id, PDO::PARAM_STR);
			}//end if
			$result->execute();
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){		
				//if everything is ok we get the ID and we create the folder
				if(isset($id)){
					$this->candybox['attachment_setting_id'] = $id;
				} else {
					$this->candybox['attachment_setting_id'] = $this->db->lastInsertId();						
					if(!file_exists($_SERVER["DOCUMENT_ROOT"].__SERVERPATH__."upload/photos/".trim($_POST['attachment_folder']))){
						mkdir($_SERVER["DOCUMENT_ROOT"].__SERVERPATH__."upload/photos/".trim($_POST['attachment_folder']),0777);						
					}//end if					
				}//end if
				
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function delete_attachment_format(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if
			
			$query = "DELETE FROM attachments_settings WHERE attachment_setting_id = ".$id;
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));
			if($errors === false){		
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_file_type(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if
			
			//mime or filename has to be passed, if none passed return false
			if(!isset($mime) && !isset($filename)){
				return false;
			}//end if
			
			//if we passed the mime, we get the file type from it
			if(isset($mime)){
				switch($mime){
					case "image/jpeg":
					case "image/gif":
					case "image/png":
					case "image/pjpeg":
						$type = "image";
						break;
					case "application/pdf":
					case "application/msword":
					case "application/rtf":
					case "text/plain":					
						$type = "document";
						break;
					case "video/quicktime":
					case "video/mpeg":
					case "video/mp4":
					case "video/ogg":
					case "video/webm":
					case "video/x-flv":
					case "video/x-ms-wmv":
					case "video/x-matroska":
						$type = "video";
						break;
					case "audio/mp3":
					case "audio/mpeg":
					case "audio/ogg":
					case "audio/aac":
					case "audio/wav":
						$type = "audio";
						break;
					case "unknown":
						return false;
						break;
					default:
						$type = "file";
				}//end switch
			}//end if
			
			if(isset($filename)){
				$ext = $this->get_file_extension($filename);
				$ext = strtolower($ext);
				switch($ext){
					case "jpg":
					case "gif":
					case "png":
					case "jpeg":
						$type = "image";						
						break;
					case "pdf":
					case "doc":
					case "docx":
					case "rtf":
					case "txt":
						$type = "document";
						break;
					case "mp4":
					case "mkv":
					case "mov":
						$type = "video";
						break;
					case "mp3":
					case "ogg":
					case "aac":
					case "wav":
						$type = "audio";
						break;
					default:
						$type = "file";
				}//end switch
			}//end if
			
				
			if(!isset($type)){
				$type = false;	
			}//end if
			return $type;
		}//end function
		
		//return true if the mime passed is for image
		public function is_img($mime){
			$isImg = false;
			switch($mime){
				case "image/jpeg":
				case "image/gif":
				case "image/png":
				case "image/pjpeg":
					$isImg = true;
					break;
				case "application/pdf":
				case "text/plain":
				case "application/x-shockwave-flash":
				case "image/psd":
				case "image/tiff":
					$isImg = false;
					break;
				default:
					$isImg = false;
			}//end if
			return $isImg;
		}//end function
				
		public function get_file_extension($file) {
			/*$array = explode('.',$file);
			return array_pop($array);*/			
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			return $ext;
		}//end function		
		
		public function remove_slashes($address){
			$address = str_replace("/","",$address);
			$address = str_replace("\\","",$address);
			return $address;
		}//end function
		
		public function clean_slashes($path){
			$path = str_replace("//","/",$path);
			$path = str_replace("\\\\","\\",$path);
			return $path;
		}//end function	
									
		
		/* MAIL RECORDING FUNCTIONALITY */
		public function delete_logged_emails(){
			$query = "TRUNCATE mail_log";
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors == false){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_logged_email(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if
			
			$query = "SELECT *, UNIX_TIMESTAMP(mail_log_date) AS mail_log_date 
					  FROM mail_log 
					  WHERE mail_log_id = :id";			
			$result = $this->db->prepare($query);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->get_result_array($result,true);	
					return $data;					
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_logged_emails(){
			$query = "SELECT *, UNIX_TIMESTAMP(mail_log_date) AS mail_log_date FROM mail_log ORDER BY mail_log_date DESC";
			
			//This is for the paging			
			if($this->paging_isset()){
				$this->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->p_start.", ".$this->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if				
			$result = $this->db->query($query);
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->get_result_array($result,false);	
					return $data;					
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function store_email(){
			$params = func_get_args();
			if(is_array($params[0])){	
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			$query = "INSERT INTO mail_log 
					  (mail_log_sender, mail_log_recipient, mail_log_subject, mail_log_text,mail_log_headers)
					  VALUES
					  (:sender,:recipient,:subject,:message,:headers)";		
			$result = $this->db->prepare($query);			
			$result->bindParam(':sender', $sender, PDO::PARAM_STR);
			$result->bindParam(':recipient', $recipient, PDO::PARAM_STR);
			$result->bindParam(':subject', $subject, PDO::PARAM_STR);
			$result->bindParam(':message', $message, PDO::PARAM_STR);			
			$result->bindParam(':headers', $headers, PDO::PARAM_STR);	
			$result->execute();
			$this->mail_id = $this->db->lastInsertId();
			if($this->error($result,__LINE__,get_class($this))){
				return false;
			} else {
				return true;
			}//end if
		}//end function
		
		public function mark_mail_as_sent(){
			$query = "UPDATE mail_log SET mail_log_sent = 1 WHERE mail_log_id = ".$this->mail_id;
			$result = $this->db->query($query);
			if($this->error($result,__LINE__,get_class($this))){
				return false;
			} else {
				return true;
			}//end if
		}//end if
		
		public function get_facebook_sdk(){
			$settings = $this->view->settings;
			if(isset($settings['facebookSDKCode']) && strlen($settings['facebookSDKCode']) > 5){
				echo $settings['facebookSDKCode'];	
			}//end if
		}
		
		public function get_google_analytics(){
			$settings = $this->get_settings();
			if(isset($settings['googleAnalyticsCode']) && strlen($settings['googleAnalyticsCode']) > 5){
				echo $settings['googleAnalyticsCode'];	
			}//end if
		}//end if
		
		#####################################################
		# TEMPLATES PART - BEGIN							#
		#####################################################
		
		public function get_templates(){
			$params = func_get_args();
			if(is_array($params[0])){	
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($type)){
				$type = 'html';	
			}//end if
			
			switch($type){
				case "html":
				default:
					$enabler = 'enableTemplates';
					$source_path = 'templatesPath';
					break;
				case "email":
					$enabler = 'enableEmailTemplates';
					$source_path = 'emailTemplatesPath';
					break;
			}//end switch
			
			$settings = $this->get_settings();
			if($settings[$enabler] == 1){
				$folders = explode(",",trim($settings[$source_path]));			
				if(is_array($folders)){
					$result = array();
					$avoid_files = array('.','..','.AppleDouble','.DS_Store');
					for($y = 0; $y < sizeof($folders); $y++){						
						$path = __SERVERPATH__.trim($folders[$y]);
						$path = str_replace('//','/',$path);
						$abs_path = $_SERVER['DOCUMENT_ROOT'].$path;	
								
						if(file_exists($abs_path)){
							$files = scandir($abs_path);
						
							for($i = 0; $i < sizeof($files); $i++){					
								if(file_exists($abs_path.$files[$i]) && !is_dir($abs_path.$files[$i]) && !in_array($files[$i],$avoid_files) && strpos($files[$i],'default') === false){	
									$file_info = array();
									$file_info['file'] = $files[$i];	
									$file_info['folder'] = $folders[$y];	
									$ignore_template = false;							
									if(function_exists('file_get_contents')){
										$file_text = file_get_contents($abs_path.$files[$i],NULL,NULL,0,1000);
										if($file_text !== false){
											$text = explode("\n",$file_text);
											for($x = 0; $x < 3; $x++){
												$line = trim(html_entity_decode($text[$x]));
												if(strpos(strtolower($text[$x]),'#template name:') !== false){
													$template_name = substr(trim($text[$x]),strlen("#Template Name:"));
													$file_info['label'] = $template_name;
												}
												if(strpos(strtolower($text[$x]),'#allowed groups:') !== false){
													$allowed_groups = substr(trim($text[$x]),strlen("#Allowed Groups:"));
													$file_info['allowed_groups'] = explode(',',trim($allowed_groups));
												}
												if(strpos(strtolower($text[$x]),'#ignore template') !== false){
													$ignore_template = true;	
												}
											}//end for i
											
											if(!isset($file_info['label'])){
												$file_info['label'] = $files[$i];
											}//end if
										} else {
											$file_info['label'] = $files[$i];
										}//end if
										
									}//end if
									if($ignore_template === false){
										array_push($result, $file_info);
									}//end if
								}//end if
							}//end if
						}//end for i
					}//end for y
				} else {
					return false;
				}//end if
				if(sizeof($result) > 0){
					return $result;	
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_folder_files(){
			$params = func_get_args();
			if(is_array($params[0])){	
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($path)){
				return false;	
			}//end if					
		
			$path = __SERVERPATH__.$path;
			$path = str_replace('//','/',$path);
			$abs_path = $_SERVER['DOCUMENT_ROOT'].$path;
			
			if(file_exists($abs_path)){
				$files = scandir($abs_path);
				$result = array();
				$avoid_files = array('.','..','.AppleDouble','.DS_Store','autoload.php','config.php','settings.php');
				for($i = 0; $i < sizeof($files); $i++){					
					if(file_exists($abs_path.$files[$i]) && !is_dir($abs_path.$files[$i]) && !in_array($files[$i],$avoid_files) && strpos($files[$i],'default') === false){						
						if(!isset($filter)){
							array_push($result, $files[$i]);
						} else {
							if(preg_match("/".$filter."/",$files[$i])){
								array_push($result, $files[$i]);
							}//end if
						}//end if
					}//end if
				}//end for i
				if(sizeof($result) > 0){
					return $result;	
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function parse_text(){
			$params = func_get_args();
			if(is_array($params[0])){	
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($data) || !isset($text)){
				return false;	
			}//end if
			
			foreach($data as $key => $value){
				$text = str_replace("{#".$key."#}",$value,$text);	
			}//end foreach
			return $text;
		}//end function
		
		public function parse_mail_template(){
			$params = func_get_args();
			if(is_array($params[0])){	
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($data) || !isset($template)){
				return false;	
			}//end if
			
			$settings = $this->get_settings();
			if((bool)$settings['enableEmailTemplates'] === true){				
				$path = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.$settings['emailTemplatesPath'];
				$path = str_replace('//','/',$path);						
				if(file_exists($path.$template)){
					if(function_exists('file_get_contents')){
						$html = file_get_contents($path.$template);
						if(is_array($data)){
							foreach($data as $key => $value){
								$html = str_replace("{#".$key."#}",$value,$html);
							}//end for i	
						}//end if
						return $html;
					}//end if
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		#####################################################
		# TEMPLATES PART - END								#
		#####################################################
		
	}//endclasss
	
	$utils = new utils($curpath);
?>