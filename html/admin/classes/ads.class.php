<?php
	require_once('ccci.class.php');

	class ads extends ccci{

		public $errors = array();
		public $status = array();
		public $settings = array();
		public $prefix = "campaign";
		protected $meta;
		public $id;
		public $submission_id;
		public $submission_group_id;

		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->module = "ads";
			$this->utils->read_params($this,$params);
			//Meta init
			$this->meta = $this->utils->call("meta");
			//setting the current module in the class
			$this->set_module($this->module);
			//Set basic settings
			$this->init_settings();
		}//endconstructor

		public function get_ads(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(isset($encdata)){
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				if(isset($encdata['token'])){
					$check = $this->check_token($encdata['token']);
					if($check === false){
						return false;
					}//end if
				}//end if
				if(isset($encdata['company_id'])){
					$company_id = $encdata['company_id'];
				}//end if
			}//end if

			if(!isset($order_by_submission)){
				if(!isset($fields)){
					$fields = "ads.*,  campaign_name, brand_name";
				}//end if

				$query = "SELECT ".$fields." FROM ads, campaigns, brands
						  WHERE ad_temp = 0
						  AND ad_brand_id = brand_id
						  AND ad_campaign_id = campaign_id";
			} else {
				if(!isset($fields)){
					$fields = "ads.*, ads_submissions.ad_submission_id, campaign_name, brand_name";
				}//end if

				$query = "SELECT ".$fields." FROM ads, campaigns, brands, ads_submissions
						  WHERE ad_temp = 0
						  AND ad_brand_id = brand_id
						  AND ad_submission_ad_id = ad_id
						  AND ad_submission_temp = 0
						  AND ad_campaign_id = campaign_id";
			}
			if(isset($search)){
				$search = str_replace(" ","%",$search);
				$query .= " AND ad_title LIKE '%".$search."%'";
			}//end if

			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND ".$this->prefix."_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND ".$this->prefix."_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if

			if(isset($company_id)){
				$query .= " AND ad_company_id = ".$company_id;
			}//end if

			if(isset($brand_id)){
				$query .= " AND ad_brand_id = ".$brand_id;
			}//end if

			if(isset($brand_owner_id)){
				$query .= " AND ad_brand_owner_id = ".$brand_owner_id;
			}//end if

			if(isset($campaign_id)){
				$query .= " AND ad_campaign_id = ".$campaign_id;
			}//end if

			if(isset($user_id)){
				$query .= " AND ad_uid = ".$user_id;
			}//end if

			if(isset($order_by_submission)){
				if(isset($days)){
					$query .= " AND (ad_submission_date_replied >= CURDATE() - INTERVAL ".$days." DAY)";
				}//end if
			}//end if

			if(isset($order_by_submission)){
				$query .= " GROUP BY ad_id
					   ORDER BY ads_submissions.ad_submission_date DESC";
			}
			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if
			//echo $query;
			//$this->utils->cache = str_replace(array("\n","\t"),'',$query);

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_ad(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			$query = "SELECT * FROM ads WHERE ad_id = ".$id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function save_ad(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($title) || !isset($company_id) || !isset($brand_id) || !isset($brand_owner_id) || !isset($campaign_id) || !isset($medium_id)){
				return false;
			}//end if

			if(!isset($temp)){
				$temp = '0';
			}//end if

			if(!isset($status)){
				$status = 'active';
			}//end if

			if(!isset($user_id)){
				$user_id = $this->utils->get_uid();
			}//end if

			$query = "INSERT INTO ads
					 (ad_title, ad_brand_id, ad_brand_owner_id, ad_campaign_id, ad_company_id, ad_uid, ad_medium_id, ad_status, ad_temp)
					 VALUES
					 (:title, :brand_id, :brand_owner_id, :campaign_id, :company_id, :user_id, :medium_id, :status, :temp)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':title', $title, PDO::PARAM_STR);
			$result->bindParam(':brand_id', $brand_id, PDO::PARAM_INT);
			$result->bindParam(':brand_owner_id', $brand_owner_id, PDO::PARAM_INT);
			$result->bindParam(':campaign_id', $campaign_id, PDO::PARAM_INT);
			$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);
			$result->bindParam(':medium_id', $medium_id, PDO::PARAM_INT);
			$result->bindParam(':status', $status, PDO::PARAM_STR);
			$result->bindParam(':temp', $temp, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->id = $this->utils->db->lastInsertId();
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function save_submission(){
			$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
			if(!is_array($encdata)){
				return false;
			}//end if

			if(!isset($encdata['company_id']) || !isset($encdata['user_id'])){
				return false;
			}//end if
			//data from Encdata
			$company_id = $encdata['company_id'];
			$user_id = $encdata['user_id'];
			//data from POST
			$brand_owner_id = $_POST['brand_owner'];
			$brand_id = $_POST['brand'];
			$campaign_id = $_POST['campaign'];
			$ad_id = $_POST['ad'];
			$support = $_POST['supporting_material'];
			if(is_null($support)){
				$support = "0";
			}
			$temp = 1;
			$status = 'draft';

			//get the medium from the ad
			$ad_data = $this->get_ad(array('id' => $ad_id));
			if($ad_data !== false){
				$medium_id = $ad_data['ad_medium_id'];
			} else {
				return false;
			}//end if

			$query = "INSERT INTO ads_submissions
					  (ad_submission_brand_id, ad_submission_brand_owner_id, ad_submission_campaign_id, ad_submission_company_id,
					  ad_submission_uid, ad_submission_ad_id, ad_submission_ad_medium_id, ad_submission_status, ad_submission_support, ad_submission_temp,
					  ad_submission_date_replied)
					  VALUES
					  (:brand_id, :brand_owner_id, :campaign_id, :company_id,
					  :user_id, :ad_id, :medium_id, :status, :support, :temp,
					  NOW())";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':brand_id', $brand_id, PDO::PARAM_INT);
			$result->bindParam(':brand_owner_id', $brand_owner_id, PDO::PARAM_INT);
			$result->bindParam(':campaign_id', $campaign_id, PDO::PARAM_INT);
			$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);
			$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':ad_id', $ad_id, PDO::PARAM_INT);
			$result->bindParam(':medium_id', $medium_id, PDO::PARAM_INT);
			$result->bindParam(':support', $support, PDO::PARAM_INT);
			$result->bindParam(':status', $status, PDO::PARAM_STR);
			$result->bindParam(':temp', $temp, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->submission_id = $this->utils->db->lastInsertId();
				$this->create_submission_group();
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function update_submission(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			$notes = $_POST['notes'];
			$status = $_POST['status'];
			$codes = $_POST['codes'];

			$this->log_activity(array('value' => $status,'ref_id' => $id));

			$query = "UPDATE ads_submissions SET
					  ad_submission_codes = :codes,
					  ad_submission_status = :status,
					  ad_submission_notes = :notes,
					  ad_submission_date_replied = NOW(),
					  ad_submission_date_viewed = NULL
					  WHERE ad_submission_id = :id";

			$result = $this->utils->db->prepare($query);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->bindParam(':codes', $codes, PDO::PARAM_STR);
			$result->bindParam(':status', $status, PDO::PARAM_INT);
			$result->bindParam(':notes', $notes, PDO::PARAM_STR);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->submission_id = $id;
				if($status == 'final-approval'){
					$this->create_approval_code();
				}//end if
				if($status !== "viewed"){
					$this->notify_submission_status_change();
				}// end if

				if(strlen(trim($_POST['comment'])) > 2){
					$message = trim($_POST['comment']);
					$messages = $this->utils->call('messages');
					$options = array('ref_id' => $id,'message' => $message,'status' => 'sent','module' => $this->module);
					$sent = $messages->send_message($options);
					if($sent){
						$message_data = $messages->get_message(array('id' => $messages->message_id));
						$this->status = $message_data;
					}//end if
				}//end if
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function create_approval_code(){
			//get last approval code
			$last_code = $this->get_last_approval_code();
			if($last_code !== false){
				$last_code++;
				$date = date('dMY');
				$approval_code = $date.'/'.$last_code;

				$query = "UPDATE ads_submissions SET ad_submission_approval_code = '".$approval_code."' WHERE ad_submission_id = ".$this->submission_id;
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		protected function get_last_approval_code(){
			$query = "SELECT MAX(SUBSTRING(ad_submission_approval_code,11)) AS last_code FROM ads_submissions LIMIT 1";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return (int)$data['last_code'];
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function mark_submission_as_viewed(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			if(!isset($admin)){
				$admin = false;
			}//end if

			if($admin === false){
				$query = "UPDATE ads_submissions SET ad_submission_date_viewed = NOW() WHERE ad_submission_id = :id";
			} else {
				$user_id = $this->utils->get_uid();
				if($user_id === false){
					return false;
				}//end if
				$query = "UPDATE ads_submissions SET ad_submission_viewed_by_admin_date = NOW(), ad_submission_viewed_by_admin = ".$user_id.", ad_submission_status = 'viewed' WHERE ad_submission_id = :id  AND ad_submission_status = 'new'";
			}//end if
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end if

		public function search_submission(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($year_only) || (int)$year_only == 0){
				if(isset($from_date)){
					$from_date = date("Y-m-d H:i:01",strtotime($from_date));
				}//end if

				if(isset($to_date)){
					$to_date = date("Y-m-d H:i:01",strtotime($to_date));
				}//end if
			}

			$query = "SELECT ads_submissions.*, brand_name, brand_owner_name, campaign_name, ad_medium_name, ad_title,
				      UNIX_TIMESTAMP(ad_submission_date) AS create_date, UNIX_TIMESTAMP(ad_submission_date_replied) AS reply_date,
					  SUBSTRING(attachment_filename,14) AS attachment_friendly_name, attachments.*, company_name
					  FROM ads_submissions, brands, brands_owners, campaigns, ads, ads_medium, attachments, attachments_posts, companies
					  WHERE brand_id = ad_submission_brand_id
					  AND ad_submission_temp = 0
					  AND ad_submission_company_id = company_id
					  AND brand_owner_id = ad_submission_brand_owner_id
					  AND campaign_id = ad_submission_campaign_id
					  AND ads_medium.ad_medium_id = ad_submission_ad_medium_id
					  AND ad_id = ad_submission_ad_id
					  AND attachment_id = attachment_post_file_id
					  AND attachment_post_post_id = ad_submission_id
					  AND attachment_post_type = 'submission'";
			if(isset($ad_id)){
				$query .= " AND ad_id = ".$ad_id;
			}//end if

			if(isset($search)){
				//$search = str_replace(" ","%",$search);
				/*$query .= " AND (ad_submission_title LIKE '%".$search."%'
								OR ad_title LIKE '%".$search."%'
								OR brand_name LIKE '%".$search."%'
								OR company_name LIKE '%".$search."%' )";	*/
				$query .= " AND MATCH(ad_submission_title, ad_title, brand_name, company_name) AGAINST ('".$search."' IN BOOLEAN MODE)";
			}

			if(isset($brand_id)){
				if(!is_array($brand_id)){
					$query .= " AND ad_submission_brand_id = ".$brand_id;
				} else {
					$brand_id = implode("','",$brand_id);
					$query .= " AND ad_submission_brand_id IN ('".$brand_id."')";
				}//end if
			}//end if

			if(isset($medium_id)){
				if(!is_array($medium_id)){
					$query .= " AND ads_medium.ad_medium_id = ".$medium_id;
				} else {
					$medium_id = implode("','",$medium_id);
					$query .= " AND ads_medium.ad_medium_id IN ('".$medium_id."')";
				}//end if
			}//end if

			if(isset($origin)){
				if(!is_array($origin)){
					$query .= " AND ad_submission_origin = ".$origin;
				} else {
					$origin = implode("','",$origin);
					$query .= " AND ad_submission_origin IN ('".$origin."')";
				}//end if
			}//end if

			if(isset($brand_category_id)){
				if(!is_array($brand_category_id)){
					$query .= " AND brand_category_id = ".$brand_category_id;
				} else {
					$brand_category_id = implode("','",$brand_category_id);
					$query .= " AND brand_category_id IN ('".$brand_category_id."')";
				}//end if
			}//end if

			if(isset($company_id)){
				if(!is_array($company_id)){
					$query .= " AND ad_submission_company_id = ".$company_id;
				} else {
					$company_id = implode("','",$company_id);
					$query .= " AND ad_submission_company_id IN ('".$company_id."')";
				}//end if
			}//end if

			if(isset($company_type_id)){
				if(!is_array($company_type_id)){
					$query .= " AND company_type_id = ".$company_type_id;
				} else {
					$company_type_id = implode("','",$company_type_id);
					$query .= " AND company_type_id IN ('".$company_type_id."')";
				}//end if
			}//end if

			if(isset($status)){
				if(is_array($status)){
					$status = implode("','",$status);
					$query .= " AND ad_submission_status IN ('".$status."')";
				} else {
					$query .= " AND ad_submission_status = '".$status."'";
				}//end if
			}//end if

			if(isset($approval)){
				if(is_array($approval)){
					$approval = implode("','",$approval);
					$query .= " AND ad_submission_approval_sought IN ('".$approval."')";
				} else {
					$query .= " AND ad_submission_approval_sought = '".$approval."'";
				}//end if
			}//end if

			if(isset($ignore_status)){
				if(is_array($ignore_status)){
					$ignore_status = implode("','",$ignore_status);
					$query .= " AND ad_submission_status NOT IN ('".$ignore_status."')";
				} else {
					$query .= " AND ad_submission_status != '".$ignore_status."'";
				}//end if
			}//end if

			if(isset($viewed)){
				if(is_bool($viewed)){
					if($viewed === true){
						$query .= " AND ad_submission_date_viewed IS NOT NULL";
					} else {
						$query .= " AND ad_submission_date_viewed IS NULL";
					}//end if
				}//end if
			}//end if

			if(isset($days)){
				$query .= " AND (ad_submission_date_replied >= CURDATE() - INTERVAL ".$days." DAY)";
			}//end if

			if(!isset($year_only) || (int)$year_only == 0){
				if(isset($from_date)){
					$query .= " AND ad_submission_date_replied >= '".$from_date."'";
				}//end if

				if(isset($to_date)){
					$query .= " AND ad_submission_date_replied <= ('".$to_date."' + INTERVAL 1 MONTH)";
				}//end if
			} else {
				if(isset($from_date)){
					$query .= " AND YEAR(ad_submission_date_replied) >= '".$from_date."'";
				}//end if

				if(isset($to_date)){
					$query .= " AND YEAR(ad_submission_date_replied) <= '".$to_date."'";
				}//end if
			}

			$query .= " ORDER BY ad_submission_date_replied DESC";

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if

			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function search_report(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($ignore_status)){
				$ignore_status = array('pending','sm-received');
			}// end if

			if(!isset($year_only) || (int)$year_only == 0){
				if(isset($from_date)){
					$from_date = date("Y-m-d H:i:01",strtotime($from_date));
				}//end if

				if(isset($to_date)){
					$to_date = date("Y-m-d H:i:01",strtotime($to_date));
				}//end if
			}

			$query = "SELECT COUNT(*) AS found
					  FROM ads_submissions, brands, brands_owners, campaigns, ads, ads_medium, attachments, attachments_posts, companies
					  WHERE brand_id = ad_submission_brand_id
					  AND ad_submission_temp = 0
					  AND ad_submission_company_id = company_id
					  AND brand_owner_id = ad_submission_brand_owner_id
					  AND campaign_id = ad_submission_campaign_id
					  AND ads_medium.ad_medium_id = ad_submission_ad_medium_id
					  AND ad_id = ad_submission_ad_id
					  AND attachment_id = attachment_post_file_id
					  AND attachment_post_post_id = ad_submission_id
					  AND attachment_post_type = 'submission'";
			if(isset($ad_id)){
				$query .= " AND ad_id = ".$ad_id;
			}//end if

			if(isset($search)){
				//$search = str_replace(" ","%",$search);
				/*$query .= " AND (ad_submission_title LIKE '%".$search."%'
								OR ad_title LIKE '%".$search."%'
								OR brand_name LIKE '%".$search."%'
								OR company_name LIKE '%".$search."%' )";	*/
				$query .= " AND MATCH(ad_submission_title, ad_title, brand_name, company_name) AGAINST ('".$search."' IN BOOLEAN MODE)";
			}

			if(isset($brand_id)){
				if(!is_array($brand_id)){
					$query .= " AND ad_submission_brand_id = ".$brand_id;
				} else {
					$brand_id = implode("','",$brand_id);
					$query .= " AND ad_submission_brand_id IN ('".$brand_id."')";
				}//end if
			}//end if

			if(isset($medium_id)){
				if(!is_array($medium_id)){
					$query .= " AND ads_medium.ad_medium_id = ".$medium_id;
				} else {
					$medium_id = implode("','",$medium_id);
					$query .= " AND ads_medium.ad_medium_id IN ('".$medium_id."')";
				}//end if
			}//end if

			if(isset($origin)){
				if(!is_array($origin)){
					$query .= " AND ad_submission_origin = ".$origin;
				} else {
					$origin = implode("','",$origin);
					$query .= " AND ad_submission_origin IN ('".$origin."')";
				}//end if
			}//end if

			if(isset($brand_category_id)){
				if(!is_array($brand_category_id)){
					$query .= " AND brand_category_id = ".$brand_category_id;
				} else {
					$brand_category_id = implode("','",$brand_category_id);
					$query .= " AND brand_category_id IN ('".$brand_category_id."')";
				}//end if
			}//end if

			if(isset($company_id)){
				if(!is_array($company_id)){
					$query .= " AND ad_submission_company_id = ".$company_id;
				} else {
					$company_id = implode("','",$company_id);
					$query .= " AND ad_submission_company_id IN ('".$company_id."')";
				}//end if
			}//end if

			if(isset($company_type_id)){
				if(!is_array($company_type_id)){
					$query .= " AND company_type_id = ".$company_type_id;
				} else {
					$company_type_id = implode("','",$company_type_id);
					$query .= " AND company_type_id IN ('".$company_type_id."')";
				}//end if
			}//end if

			if(isset($status)){
				if(is_array($status)){
					$status = implode("','",$status);
					$query .= " AND ad_submission_status IN ('".$status."')";
				} else {
					$query .= " AND ad_submission_status = '".$status."'";
				}//end if
			}//end if

			if(isset($approval)){
				if(is_array($approval)){
					$approval = implode("','",$approval);
					$query .= " AND ad_submission_approval_sought IN ('".$approval."')";
				} else {
					$query .= " AND ad_submission_approval_sought = '".$approval."'";
				}//end if
			}//end if

			if(isset($ignore_status)){
				if(is_array($ignore_status)){
					$ignore_status = implode("','",$ignore_status);
					$query .= " AND ad_submission_status NOT IN ('".$ignore_status."')";
				} else {
					$query .= " AND ad_submission_status != '".$ignore_status."'";
				}//end if
			}//end if

			if(isset($viewed)){
				if(is_bool($viewed)){
					if($viewed === true){
						$query .= " AND ad_submission_date_viewed IS NOT NULL";
					} else {
						$query .= " AND ad_submission_date_viewed IS NULL";
					}//end if
				}//end if
			}//end if

			if(isset($days)){
				$query .= " AND (ad_submission_date_replied >= CURDATE() - INTERVAL ".$days." DAY)";
			}//end if

			if(!isset($year_only) || (int)$year_only == 0){
				if(isset($from_date)){
					$query .= " AND ad_submission_date_replied >= '".$from_date."'";
				}//end if

				if(isset($to_date)){
					$query .= " AND ad_submission_date_replied <= ('".$to_date."' + INTERVAL 1 MONTH)";
				}//end if
			} else {
				if(isset($from_date)){
					$query .= " AND YEAR(ad_submission_date_replied) >= '".$from_date."'";
				}//end if

				if(isset($to_date)){
					$query .= " AND YEAR(ad_submission_date_replied) <= '".$to_date."'";
				}//end if
			}//end if

			if(isset($from_month)){
				$query .= " AND MONTH(ad_submission_date_replied) >= ".$from_month;
			}//end if

			if(isset($to_month)){
				$query .= " AND MONTH(ad_submission_date_replied) <= ".$to_month;
			}//end if

			if(isset($from_year)){
				$query .= " AND YEAR(ad_submission_date_replied) >= ".$from_year;
			}//end if

			if(isset($to_year)){
				$query .= " AND YEAR(ad_submission_date_replied) <= ".$to_year;
			}//end if

			$query .= " ORDER BY ad_submission_date_replied DESC";


			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_chart(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			//$ignore_status = array('pending','sm-received');

			$query = "SELECT COUNT(*) AS found, YEAR(ad_submission_date_replied) AS year
					  FROM ads_submissions, brands, brands_owners, campaigns, ads, ads_medium, attachments, attachments_posts, companies
					  WHERE brand_id = ad_submission_brand_id
					  AND ad_submission_temp = 0
					  AND ad_submission_company_id = company_id
					  AND brand_owner_id = ad_submission_brand_owner_id
					  AND campaign_id = ad_submission_campaign_id
					  AND ads_medium.ad_medium_id = ad_submission_ad_medium_id
					  AND ad_id = ad_submission_ad_id
					  AND attachment_id = attachment_post_file_id
					  AND attachment_post_post_id = ad_submission_id
					  AND attachment_post_type = 'submission'";
			if(isset($ad_id)){
				$query .= " AND ad_id = ".$ad_id;
			}//end if

			if(isset($search)){
				//$search = str_replace(" ","%",$search);
				/*$query .= " AND (ad_submission_title LIKE '%".$search."%'
								OR ad_title LIKE '%".$search."%'
								OR brand_name LIKE '%".$search."%'
								OR company_name LIKE '%".$search."%' )";	*/
				$query .= " AND MATCH(ad_submission_title, ad_title, brand_name, company_name) AGAINST ('".$search."' IN BOOLEAN MODE)";
			}

			if(isset($brand_id)){
				if(!is_array($brand_id)){
					$query .= " AND ad_submission_brand_id = ".$brand_id;
				} else {
					$brand_id = implode("','",$brand_id);
					$query .= " AND ad_submission_brand_id IN ('".$brand_id."')";
				}//end if
			}//end if

			if(isset($medium_id)){
				if(!is_array($medium_id)){
					$query .= " AND ads_medium.ad_medium_id = ".$medium_id;
				} else {
					$medium_id = implode("','",$medium_id);
					$query .= " AND ads_medium.ad_medium_id IN ('".$medium_id."')";
				}//end if
			}//end if

			if(isset($origin)){
				if(!is_array($origin)){
					$query .= " AND ad_submission_origin = '".$origin."'";
				} else {
					$origin = implode("','",$origin);
					$query .= " AND ad_submission_origin IN ('".$origin."')";
				}//end if
			}//end if

			if(isset($brand_category_id)){
				if(!is_array($brand_category_id)){
					$query .= " AND brand_category_id = ".$brand_category_id;
				} else {
					$brand_category_id = implode("','",$brand_category_id);
					$query .= " AND brand_category_id IN ('".$brand_category_id."')";
				}//end if
			}//end if

			if(isset($company_type_id)){
				if(!is_array($company_type_id)){
					$query .= " AND company_type_id = ".$company_type_id;
				} else {
					$company_type_id = implode("','",$company_type_id);
					$query .= " AND company_type_id IN ('".$company_type_id."')";
				}//end if
			}//end if

			if(isset($status)){
				if(is_array($status)){
					$status = implode("','",$status);
					$query .= " AND ad_submission_status IN ('".$status."')";
				} else {
					$query .= " AND ad_submission_status = '".$status."'";
				}//end if
			}//end if

			if(isset($ignore_status)){
				if(is_array($ignore_status)){
					$ignore_status = implode("','",$ignore_status);
					$query .= " AND ad_submission_status NOT IN ('".$ignore_status."')";
				} else {
					$query .= " AND ad_submission_status != '".$ignore_status."'";
				}//end if
			}//end if

			if(isset($from_month)){
				$query .= " AND MONTH(ad_submission_date_replied) >= ".$from_month;
			}//end if

			if(isset($to_month)){
				$query .= " AND MONTH(ad_submission_date_replied) <= ".$to_month;
			}//end if

			if(isset($from_year)){
				$query .= " AND YEAR(ad_submission_date_replied) >= ".$from_year;
			}//end if

			if(isset($to_year)){
				$query .= " AND YEAR(ad_submission_date_replied) <= ".$to_year;
			}//end if

			if(isset($year)){
				$query .= " AND YEAR(ad_submission_date_replied) = ".$year;
			}//end if

			$query .= " GROUP BY YEAR(ad_submission_date_replied)
						ORDER BY ad_submission_date_replied DESC";


			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_submissions(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($sort_by)){
				$sort_by = 'ad_submission_date_replied';
			}

			if(!isset($sort_order)){
				$sort_order = 'DESC';
			}//end if

			$query = "SELECT ads_submissions.*, brand_name, brand_owner_name, campaign_name, ad_medium_name, ad_title,
				      UNIX_TIMESTAMP(ad_submission_date) AS create_date, UNIX_TIMESTAMP(ad_submission_date_replied) AS reply_date,
					  IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name,
					  attachments.attachment_filename, attachments.attachment_size , company_name
					  FROM ads_submissions, brands, brands_owners, campaigns, ads, ads_medium, attachments, attachments_posts, companies
					  WHERE brand_id = ad_submission_brand_id
					  AND ad_submission_temp = 0
					  AND ad_submission_company_id = company_id
					  AND brand_owner_id = ad_submission_brand_owner_id
					  AND campaign_id = ad_submission_campaign_id
					  AND ads_medium.ad_medium_id = ad_submission_ad_medium_id
					  AND ad_id = ad_submission_ad_id
					  AND attachment_id = attachment_post_file_id
					  AND attachment_post_post_id = ad_submission_id
					  AND attachment_post_type = 'submission'";
			if(isset($ad_id)){
				$query .= " AND ad_id = ".$ad_id;
			}//end if

			if(isset($company_id)){
				$query .= " AND ad_submission_company_id = ".$company_id;
			}//end if

			if(isset($status)){
				if(is_array($status)){
					$status = implode("','",$status);
					$query .= " AND ad_submission_status IN ('".$status."')";
				} else {
					$query .= " AND ad_submission_status = '".$status."'";
				}//end if
			}//end if

			if(isset($ignore_status)){
				if(is_array($ignore_status)){
					$ignore_status = implode("','",$ignore_status);
					$query .= " AND ad_submission_status NOT IN ('".$ignore_status."')";
				} else {
					$query .= " AND ad_submission_status != '".$ignore_status."'";
				}//end if
			}//end if

			if(isset($viewed)){
				if(is_bool($viewed)){
					if($viewed === true){
						$query .= " AND ad_submission_date_viewed IS NOT NULL";
					} else {
						$query .= " AND ad_submission_date_viewed IS NULL";
					}//end if
				}//end if
			}//end if

			if(isset($current_week)){
				$query .= " AND IF(ad_submission_status IN ('new','viewed','pending') ,YEARWEEK(ad_submission_date_replied) > (YEARWEEK(CURDATE()) - 60) , YEARWEEK(ad_submission_date_replied) = YEARWEEK(CURDATE())) ";
				//$query .= " AND YEARWEEK(ad_submission_date_replied) = YEARWEEK(CURDATE()) ";
			}

			if(isset($days)){
				$query .= " AND (ad_submission_date_replied >= CURDATE() - INTERVAL ".$days." DAY)";
			}//end if

			$query .= " ORDER BY ".$sort_by." ".$sort_order;

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if

			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_submissions_in_conversation(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($sort_order)){
				$sort_order = 'DESC';
			}//end if

			$query = "SELECT ads_submissions.ad_submission_id, ads_submissions.ad_submission_date, ads_submissions.ad_submission_title, ads_submissions.ad_submission_status, brand_name, brand_owner_name, campaign_name, ad_medium_name, ad_title,
				      UNIX_TIMESTAMP(ad_submission_date) AS create_date, UNIX_TIMESTAMP(ad_submission_date_replied) AS reply_date,
					  IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name,
					  attachments.attachment_filename, attachments.attachment_size, company_name
					  FROM ads_submissions, brands, brands_owners, campaigns, ads, ads_medium, attachments, attachments_posts, companies, messages
					  WHERE brand_id = ad_submission_brand_id
					  AND ad_submission_temp = 0
					  AND ad_submission_company_id = company_id
					  AND brand_owner_id = ad_submission_brand_owner_id
					  AND campaign_id = ad_submission_campaign_id
					  AND ads_medium.ad_medium_id = ad_submission_ad_medium_id
					  AND ad_id = ad_submission_ad_id
					  AND attachments.attachment_id = attachments_posts.attachment_post_file_id
					  AND attachments_posts.attachment_post_post_id = ad_submission_id
					  AND attachments_posts.attachment_post_type = 'submission'
					  AND (message_date >= ad_submission_date_viewed OR ad_submission_date_viewed IS NULL)
					  AND message_ref_id = ad_submission_id";
			if(isset($ad_id)){
				$query .= " AND ad_id = ".$ad_id;
			}//end if

			if(isset($company_id)){
				$query .= " AND ad_submission_company_id = ".$company_id;
			}//end if

			if(isset($status)){
				if(is_array($status)){
					$status = implode("','",$status);
					$query .= " AND ad_submission_status IN ('".$status."')";
				} else {
					$query .= " AND ad_submission_status = '".$status."'";
				}//end if
			}//end if

			if(isset($ignore_status)){
				if(is_array($ignore_status)){
					$ignore_status = implode("','",$ignore_status);
					$query .= " AND ad_submission_status NOT IN ('".$ignore_status."')";
				} else {
					$query .= " AND ad_submission_status != '".$ignore_status."'";
				}//end if
			}//end if

			if(isset($viewed)){
				if(is_bool($viewed)){
					if($viewed === true){
						$query .= " AND ad_submission_date_viewed IS NOT NULL";
					} else {
						$query .= " AND ad_submission_date_viewed IS NULL";
					}//end if
				}//end if
			}//end if

			if(isset($days)){
				$query .= " AND (ad_submission_date_replied >= CURDATE() - INTERVAL ".$days." DAY)
							AND (message_date >= CURDATE() - INTERVAL ".$days." DAY)";
			}//end if

			$query .= " GROUP BY ad_submission_id
						ORDER BY ad_submission_date_replied ".$sort_order;

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if

			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_submissions_by_user_comments(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($sort_order)){
				$sort_order = 'DESC';
			}//end if

			$query = "SELECT ads_submissions.ad_submission_id, ads_submissions.ad_submission_date, ads_submissions.ad_submission_title, ads_submissions.ad_submission_status, brand_name, brand_owner_name, campaign_name, ad_medium_name, ad_title,
				      UNIX_TIMESTAMP(ad_submission_date) AS create_date, UNIX_TIMESTAMP(ad_submission_date_replied) AS reply_date,
					  IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS attachment_friendly_name,
					  attachments.attachment_filename, attachments.attachment_size, company_name
					  FROM ads_submissions, brands, brands_owners, campaigns, ads, ads_medium, attachments, attachments_posts, companies, messages, users, users_groups_rel
					  WHERE brand_id = ad_submission_brand_id
					  AND ad_submission_temp = 0
						AND messages.message_sender_uid = users.user_id
						AND users_groups_rel.users_groups_rel_gid = 4
						AND users_groups_rel.users_groups_rel_uid = users.user_id
					  AND ad_submission_company_id = company_id
					  AND brand_owner_id = ad_submission_brand_owner_id
					  AND campaign_id = ad_submission_campaign_id
					  AND ads_medium.ad_medium_id = ad_submission_ad_medium_id
					  AND ad_id = ad_submission_ad_id
					  AND attachments.attachment_id = attachments_posts.attachment_post_file_id
					  AND attachments_posts.attachment_post_post_id = ad_submission_id
					  AND attachments_posts.attachment_post_type = 'submission'
					  AND (message_date >= ad_submission_date_viewed OR ad_submission_date_viewed IS NULL)
					  AND message_ref_id = ad_submission_id";
			if(isset($ad_id)){
				$query .= " AND ad_id = ".$ad_id;
			}//end if


			if(isset($company_id)){
				$query .= " AND ad_submission_company_id = ".$company_id;
			}//end if

			if(isset($status)){
				if(is_array($status)){
					$status = implode("','",$status);
					$query .= " AND ad_submission_status IN ('".$status."')";
				} else {
					$query .= " AND ad_submission_status = '".$status."'";
				}//end if
			}//end if

			if(isset($ignore_status)){
				if(is_array($ignore_status)){
					$ignore_status = implode("','",$ignore_status);
					$query .= " AND ad_submission_status NOT IN ('".$ignore_status."')";
				} else {
					$query .= " AND ad_submission_status != '".$ignore_status."'";
				}//end if
			}//end if

			if(isset($viewed)){
				if(is_bool($viewed)){
					if($viewed === true){
						$query .= " AND ad_submission_date_viewed IS NOT NULL";
					} else {
						$query .= " AND ad_submission_date_viewed IS NULL";
					}//end if
				}//end if
			}//end if

			if(isset($days)){
				$query .= " AND (ad_submission_date_replied >= CURDATE() - INTERVAL ".$days." DAY)
							AND (message_date >= CURDATE() - INTERVAL ".$days." DAY)";
			}//end if

			$query .= " GROUP BY ad_submission_id
						ORDER BY ad_submission_date_replied ".$sort_order;

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if

			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_submission(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			if(!isset($detailed)){
				$detailed = false;
			}//end if

			if($detailed === false){
				$query = "SELECT *,UNIX_TIMESTAMP(ad_submission_date) AS create_date FROM ads_submissions WHERE ad_submission_id = ".$id." LIMIT 1";
			} else {
				$query = "SELECT  ads_submissions.*, brand_name, brand_owner_name, campaign_name, ad_medium_name, ad_title,
						  UNIX_TIMESTAMP(ad_submission_date) AS create_date, UNIX_TIMESTAMP(ad_submission_date_replied) AS reply_date,
						  SUBSTRING(attachment_filename,14) AS attachment_friendly_name, attachments.*
						  FROM ads_submissions, brands, brands_owners, campaigns, ads, ads_medium, attachments, attachments_posts
						  WHERE ad_submission_id = ".$id."
						  AND brand_id = ad_submission_brand_id
						  AND brand_owner_id = ad_submission_brand_owner_id
						  AND campaign_id = ad_submission_campaign_id
						  AND ads_medium.ad_medium_id = ad_submission_ad_medium_id
						  AND ad_id = ad_submission_ad_id
						  AND attachment_id = attachment_post_file_id
						  AND attachment_post_post_id = ad_submission_id
						  AND attachment_post_type = 'submission'
						  LIMIT 1";
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function create_submission_group(){
			if(is_null($this->submission_id)){
				return false;
			}//end if
			$query = "INSERT INTO ads_submissions_groups
					  (ad_submission_group_submission_id, ad_submission_group_source)
					  VALUES
					  (:submission_id,1)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':submission_id', $this->submission_id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->submission_group_id = $this->utils->db->lastInsertId();
				$query = "UPDATE ads_submissions_groups SET ad_submission_group_key = ".$this->submission_group_id." WHERE ad_submission_group_id = ".$this->submission_group_id;
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function get_submission_group_items(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($group_id)){
				return false;
			}//end if

			if(!isset($fields)){
				$fields = "*";
			}//end if

			$query = "SELECT ".$fields."
				      FROM ads_submissions, ads_submissions_groups, attachments, attachments_posts
					  WHERE ad_submission_group_key = ".$group_id."
					  AND ad_submission_id = attachment_post_post_id
					  AND attachment_post_type = 'submission'
					  AND attachment_id = attachment_post_file_id
					  AND ad_submission_id = ad_submission_group_submission_id";
			$this->utils->cache = $query;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_encdata(){
			$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
			if(!is_array($encdata)){
				return false;
			}//end if
			$encdata['submission_id'] = $this->submission_id;
			$encdata['submission_group_id'] = $this->submission_group_id;
			$encdata = urlencode(base64_encode(serialize($encdata)));
			return $encdata;
		}//end function

		public function save_submission_labels(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($group_id)){
				return false;
			}//end if

			$submission_id = $_POST['submission_id'];
			$submission_label = $_POST['submission_label'];
			$submission_comment = $_POST['submission_comment'];

			if(!is_array($submission_id) || !is_array($submission_label)){
				return false;
			}//end if

			for($i = 0; $i < sizeof($submission_id); $i++){
				$query = "UPDATE ads_submissions SET ad_submission_title = :label, ad_submission_file_details = :comment WHERE ad_submission_id = :id";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':label', $submission_label[$i], PDO::PARAM_STR);
				$result->bindParam(':comment', $submission_comment[$i], PDO::PARAM_STR);
				$result->bindParam(':id', $submission_id[$i], PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
			}//end for i

			return true;
		}//end function

		public function save_submission_details(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($group_id)){
				return false;
			}//end if

			$approval_sought = $_POST['approval_sought'];
			$origin = $_POST['origin'];
			$comments = $_POST['comments'];

			$list = $this->get_submission_group_items(array('fields' => 'ad_submission_id AS id','group_id' => $group_id));
			if($list !== false){
				for($i = 0; $i < sizeof($list); $i++){
					$query = "UPDATE ads_submissions SET
							  ad_submission_approval_sought = :approval,
							  ad_submission_origin = :origin,
							  ad_submission_comments = :comments
							  WHERE ad_submission_id = :id";
					$result = $this->utils->db->prepare($query);
					$result->bindParam(':approval', $approval_sought, PDO::PARAM_STR);
					$result->bindParam(':origin', $origin, PDO::PARAM_STR);
					$result->bindParam(':comments', $comments, PDO::PARAM_STR);
					$result->bindParam(':id', $list[$i]['id'], PDO::PARAM_INT);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));
				}//end for i
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function delete_submission(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id) || !isset($group_id)){
				return false;
			}//end if

			$user_id = $this->utils->get_uid();

			//now we get the submission details before delete anything
			$data = $this->get_submission(array('id' => $id));
			if($data !== false){
				//now we get the submission user_id
				if($data['ad_submission_uid'] === $user_id){
					$this->utils->db->beginTransaction();
					try{
						//We delete the submission
						$query = "DELETE FROM ads_submissions WHERE ad_submission_id = ".$id." AND ad_submission_uid = ".$user_id;
						$result = $this->utils->db->query($query);
						$errors = $this->utils->error($result,__LINE__,get_class($this));

						$query = "DELETE FROM ads_submissions_groups WHERE ad_submission_group_submission_id = ".$id." AND ad_submission_group_key = ".$group_id;
						$result = $this->utils->db->query($query);
						$errors = $this->utils->error($result,__LINE__,get_class($this));

						$this->reorganise_submission_group(array('submission_id' => $id, 'group_id' => $group_id));

						######################
						# HAS TO BE FINISHED #
						######################

						$this->utils->db->commit();
					} catch(PDOException $ex){
						$this->utils->db->rollback();
						array_push($this->errors,$ex->getMessage());
						return false;
					}//end try
					return true;
				} else {
					//if the user ids do not match, we return false
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		//with this method we have to confirm anything was set as temporary for this submission
		public function complete_submission(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($group_id)){
				return false;
			}//end if

			$list = $this->get_submission_group_items(array('group_id' => $group_id));
			if($list !== false){
				$this->utils->db->beginTransaction();
				try{
					//first we set the submission out of temporary
					for($i = 0; $i < sizeof($list); $i++){
						$query = "UPDATE ads_submissions SET ad_submission_temp = 0, ad_submission_status = 'new' WHERE ad_submission_id = ".$list[$i]['ad_submission_id'];
						$result = $this->utils->db->query($query);
						$errors = $this->utils->error($result,__LINE__,get_class($this));
					}//end for i

					//now we need to take one of the submission for this group and check if every element, brand, ad, etc... are set out of temp mode
					$submission = $list[0];
					$brand_id = $submission['ad_submission_brand_id'];
					$brand_owner_id = $submission['ad_submission_brand_owner_id'];
					$campaign_id = $submission['ad_submission_campaign_id'];
					$ad_id = $submission['ad_submission_ad_id'];

					//we check the brand
					$query = "UPDATE brands SET brand_temp = 0 WHERE brand_id = ".$brand_id;
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));

					//now the brand owner
					$query = "UPDATE brands_owners SET brand_owner_active = 1 WHERE brand_owner_id = ".$brand_owner_id;
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));

					//the campaigns as well
					$query = "UPDATE campaigns SET campaign_temp = 0 WHERE campaign_id = ".$campaign_id;
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));

					//and final is the ad
					$query = "UPDATE ads SET ad_temp = 0 WHERE ad_id = ".$ad_id;
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));

					//now we notify the submission
					$this->notify_submissions(array('group_id' => $group_id));

					$this->utils->db->commit();
				} catch(PDOException $ex){
					$this->utils->db->rollback();
					array_push($this->errors,$ex->getMessage());
					return false;
				}//end try

				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function clone_submission(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id) || !isset($group_id)){
				return false;
			}//end if

			$source = $this->get_submission(array('id' => $id));
			if($source !== false){
				$query = "INSERT INTO ads_submissions
					  (ad_submission_brand_id, ad_submission_brand_owner_id, ad_submission_campaign_id, ad_submission_company_id,
					  ad_submission_uid, ad_submission_ad_id, ad_submission_ad_medium_id, ad_submission_status, ad_submission_support, ad_submission_temp,
					  ad_submission_date_replied)
					  VALUES
					  (:brand_id, :brand_owner_id, :campaign_id, :company_id,
					  :user_id, :ad_id, :medium_id, :status, :support, :temp,
					  NOW())";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':brand_id', $source['ad_submission_brand_id'], PDO::PARAM_INT);
				$result->bindParam(':brand_owner_id', $source['ad_submission_brand_owner_id'], PDO::PARAM_INT);
				$result->bindParam(':campaign_id', $source['ad_submission_campaign_id'], PDO::PARAM_INT);
				$result->bindParam(':company_id', $source['ad_submission_company_id'], PDO::PARAM_INT);
				$result->bindParam(':user_id', $source['ad_submission_uid'], PDO::PARAM_INT);
				$result->bindParam(':ad_id', $source['ad_submission_ad_id'], PDO::PARAM_INT);
				$result->bindParam(':medium_id', $source['ad_submission_ad_medium_id'], PDO::PARAM_INT);
				$result->bindParam(':support', $source['ad_submission_support'], PDO::PARAM_INT);
				$result->bindParam(':status', $source['ad_submission_status'], PDO::PARAM_STR);
				$result->bindParam(':temp', $source['ad_submission_temp'], PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$this->submission_id = $this->utils->db->lastInsertId();
					$this->add_submission_to_group(array('group_id' => $group_id, 'id' => $this->submission_id));
					return true;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function notify_submission_status_change(){
			//we get the submission details
			$submission_data = $this->get_submission(array('id' => $this->submission_id));
			if($submission_data == false){
				return false;
			}//end if

			//we get the user details
			$users = $this->utils->call('users');
			$user_data = $users->get_user(array('id' => $submission_data['ad_submission_uid']));

			$company_id = $user_data['user_company_id'];
			$companies = $this->utils->call('companies');
			$company_data = $companies->get_company(array('id' => $company_id));

			//create the mailer obj
			$mailer = $this->utils->call("mailer");

			//we read the general settings
			$genSettings = $this->utils->get_settings();
			$no_reply = $this->settings['submission_no_reply'];
			if($no_reply == ""){
				$no_reply = $genSettings['noReply'];
			}//end if

			$type = $this->settings['submission_email_type'];
			if($type == "html"){
				$mailer->defaultFormat = 'text/html';
			} else {
				$mailer->defaultFormat = 'text/plain';
			}//end if

			$email = $user_data['user_email'];
			$mailer->set_sender($no_reply);

			$title = trim($submission_data['ad_submission_title']);
			if($title == ""){
				$title = $submission_data['attachment_filename'];
			}//end if

			$tpld = array();
			$encdata = urlencode(base64_encode($submission_data['ad_submission_id']));
			$tpld['encdata'] = $encdata;
			$tpld['user_firstname'] = $user_data['user_firstname'];
			$tpld['user_surname'] = $user_data['user_surname'];
			$tpld['company_name'] = $company_data['company_name'];
			$tpld['submission_title'] = $title;
			$tpld['submission_date'] = date("jS F Y",$submission_data['create_date']);
			$tpld['message'] = $this->utils->parse_text(array('data' => $tpld,'text' => $this->settings['submission_status_email_message']));

			$subject = $this->settings['submission_status_email_subject'];
			$subject = $this->utils->parse_text(array('data' => $tpld,'text' => $subject));

			if($this->settings['submission_status_change_template'] != "" && $type == "html"){
				$template = $this->settings['submission_status_change_template'];
				if($submission_data['ad_submission_status'] == 'final-approval'){
					$template = $this->settings['submission_status_finalapprove_template'];
					$tpld['approval_code'] = $submission_data['ad_submission_approval_code'];
				}
				$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
			} else {
				$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));
			}//end if

			$mailer->set_message(array('message' => $message));
			$mailresult = $mailer->send_mail(array('email' => array($email),'subject' => $subject));
			if(!$mailresult){
				return false;
			}//end if
		}//end function

		public function notify_new_comment(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			if($this->settings['notify_new_comments'] == 1){

				//we get the submission details
				$submission_data = $this->get_submission(array('id' => $id,'detailed' => true));
				if($submission_data == false){
					return false;
				}//end if

				//create the mailer obj
				$mailer = $this->utils->call("mailer");

				//we read the general settings
				$genSettings = $this->utils->get_settings();
				$no_reply = $this->settings['submission_no_reply'];
				if($no_reply == ""){
					$no_reply = $genSettings['noReply'];
				}//end if

				$type = $this->settings['submission_email_type'];
				if($type == "html"){
					$mailer->defaultFormat = 'text/html';
				} else {
					$mailer->defaultFormat = 'text/plain';
				}//end if

				$notifyUsersGroup = $this->settings['notify_new_comments_users'];

				$users = $this->utils->call('users');
				$users_list = $users->get_users(array('group_id' => $notifyUsersGroup));
				if($users_list === false){
					return false;
				}//end if


				$mailer->set_sender($no_reply);

				$title = trim($submission_data['ad_submission_title']);
				if($title == ""){
					$title = $submission_data['attachment_filename'];
				}//end if

				for($i = 0; $i < sizeof($users_list); $i++){
					$tpld = array();
					$encdata = urlencode(base64_encode($submission_data['ad_submission_id']));
					$tpld['encdata'] = $encdata;
					$tpld['user_firstname'] = $users_list[$i]['user_firstname'];
					$tpld['user_surname'] = $users_list[$i]['user_surname'];
					$tpld['submission_title'] = $title;
					$tpld['submission_date'] = date("jS F Y",$submission_data['create_date']);
					$tpld['message'] = $this->utils->parse_text(array('data' => $tpld,'text' => $this->settings['notify_new_comments_email_message']));

					$subject = $this->settings['notify_new_comments_email_subject'];
					$subject = $this->utils->parse_text(array('data' => $tpld,'text' => $subject));

					if($this->settings['notify_new_comment_email_template'] != "" && $type == "html"){
						$template = $this->settings['notify_new_comment_email_template'];
						$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
					} else {
						$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));
					}//end if

					$mailer->set_message(array('message' => $message));
					$mailresult = $mailer->send_mail(array('email' => $users_list[$i]['user_email'],'subject' => $subject));
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function notify_submissions(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($group_id)){
				return false;
			}//end if

			//we get the user details
			$users = $this->utils->call('users');
			$user_data = $users->get_user();

			$company_id = $user_data['user_company_id'];
			$companies = $this->utils->call('companies');
			$company_data = $companies->get_company(array('id' => $company_id));

			$campaigns = $this->utils->call('campaigns');

			//we get the list of submissions
			$list = $this->get_submission_group_items(array('group_id' => $group_id));
			if($list === false){
				return false;
			}//end if

			//create the mailer obj
			$mailer = $this->utils->call("mailer");

			//we read the general settings
			$genSettings = $this->utils->get_settings();
			$no_reply = $this->settings['submission_no_reply'];
			if($no_reply == ""){
				$no_reply = $genSettings['noReply'];
			}//end if

			$type = $this->settings['submission_email_type'];
			if($type == "html"){
				$mailer->defaultFormat = 'text/html';
			} else {
				$mailer->defaultFormat = 'text/plain';
			}//end if

			//now we need to send an email to the designed admin
			if($this->settings['submission_notification_email'] != ""){
				$email = $this->settings['submission_notification_email'];
				$mailer->set_sender($no_reply);
				if(isset($this->settings['submission_bcc_on_notification']) && $this->settings['submission_bcc_on_notification'] != ""){
					$mailer->set_bcc($this->settings['submission_bcc_on_notification']);
				}//end if

				for($i = 0; $i < sizeof($list); $i++){

					$subject = $this->settings['submission_email_subject'];
					$subject = $this->utils->parse_text(array('data' => $user_data,'text' => $subject));

					//get the campaign name for the current submission
					$campaign_data = $campaigns->get_campaign(array('id' => $list[$i]['ad_submission_campaign_id']));

					$title = trim($list[$i]['ad_submission_title']);
					if($title == ""){
						$title = $list[$i]['attachment_filename'];
					}//end if

					$tpld = array();
					$tpld['message'] = $this->utils->parse_text(array('data' => $user_data,'text' => $this->settings['submission_email_message']));
					$encdata = urlencode(base64_encode($list[$i]['ad_submission_id']));
					$tpld['encdata'] = $encdata;
					$tpld['submission_title'] = $title;
					$tpld['company_name'] = $company_data['company_name'];
					$tpld['campaign_name'] = $campaign_data['campaign_name'];

					if($this->settings['submission_template'] != "" && $type == "html"){
						$template = $this->settings['submission_template'];
						$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
					} else {
						$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));;
					}//end if

					$mailer->set_message(array('message' => $message));
					$mailresult = $mailer->send_mail(array('email' => array($email),'subject' => $subject));
				}//end for i
			}//end if

			//check if we have to notify the user as well
			if($this->settings['submission_notify_user'] == 1){
				$email = $user_data['user_email'];
				$mailer->set_sender($no_reply);

				for($i = 0; $i < sizeof($list); $i++){
					$subject = $this->settings['submission_user_email_subject'];
					$subject = $this->utils->parse_text(array('data' => $user_data,'text' => $subject));

					//get the campaign name for the current submission
					$campaign_data = $campaigns->get_campaign(array('id' => $list[$i]['ad_submission_campaign_id']));

					$title = trim($list[$i]['ad_submission_title']);
					if($title == ""){
						$title = $list[$i]['attachment_filename'];
					}//end if

					$tpld = array();
					$tpld['message'] = $this->utils->parse_text(array('data' => $user_data,'text' => $this->settings['submission_user_email_message']));
					$encdata = urlencode(base64_encode($list[$i]['ad_submission_id']));
					$tpld['encdata'] = $encdata;
					$tpld['submission_title'] = $title;
					$tpld['company_name'] = $company_data['company_name'];
					$tpld['campaign_name'] = $campaign_data['campaign_name'];

					if($this->settings['submission_user_template'] != "" && $type == "html"){
						$template = $this->settings['submission_user_template'];
						$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
					} else {
						$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));;
					}//end if

					$mailer->set_message(array('message' => $message));
					$mailresult = $mailer->send_mail(array('email' => array($email),'subject' => $subject));
				}//end for i
			}//end if
			return true;

		}//end function

		public function count_new_messages(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($status)){
				$status = 'conversation';
			}//end if

			$query = "SELECT COUNT(message_id) AS new_messages
					  FROM messages, ads_submissions
					  WHERE ad_submission_temp = 0
					  AND ad_submission_status = '".$status."'
					  AND message_date >= ad_submission_date_viewed
					  AND message_ref_id = ad_submission_id
					  AND (ad_submission_date_replied >= CURDATE() - INTERVAL 7 DAY)";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data['new_messages'];
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function reorganise_files(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($group_id) || !isset($source)){
				return false;
			}//end if

			//ok, first thing is to get the current files from the source
			$files = $this->utils->get_attachments(array(
				'post_id' => $source,
				'module' => $this->module,
				'sort_by' => 'attachment_post_id',
				'sort_order' => 'ASC',
				'attachment_post_type' => 'submission'
			));
			//now we count the files and we loop them if more than one files is found
			if($files > 1){
				//we loop the files and we skip the first one
				for($i = 1; $i < sizeof($files); $i++){
					//for each file we clone the source submission
					$this->clone_submission(array('id' => $source, 'group_id' => $group_id));
					$submission_id = $this->submission_id;
					//now that we have the submission, we can associate the current file to it
					$query = "UPDATE attachments_posts
							  SET attachment_post_post_id = ".$submission_id."
							  WHERE attachment_post_id = ".$files[$i]['attachment_post_id']."
							  AND attachment_post_file_id = ".$files[$i]['attachment_id']."
							  AND attachment_post_type = 'submission'";
					$this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
				}//end for i
				return true;
			} else {
				//if we have only one file, there is no need to duplicate anything and is good as it is
				return true;
			}//end if

		}//end function

		public function add_submission_to_group(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($group_id) || !isset($id)){
				return false;
			}//end if

			$query = "INSERT INTO ads_submissions_groups
					  (ad_submission_group_key, ad_submission_group_submission_id)
					  VALUES
					  (:group_id, :id)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':group_id', $group_id, PDO::PARAM_INT);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function log_activity(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $param_value){
					${$key} = $param_value;
				}//end if
			}//end if

			if(!isset($value) || !isset($ref_id)){
				return false;
			}//end if

			$user_id = $this->utils->get_uid();
			if($user_id === false){
				return false;
			}//end if

			if(!isset($type)){
				$type = 'status-change';
			}//end if

			$query = "INSERT INTO ads_submissions_logs
					  (ad_submission_log_uid, ad_submission_log_type, ad_submission_log_value, ad_submission_log_ref_id)
					  VALUES
					  (:user_id, :type, :value, :ref_id)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':type', $type, PDO::PARAM_STR);
			$result->bindParam(':value', $value, PDO::PARAM_STR);
			$result->bindParam(':ref_id', $ref_id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end  function

		public function get_codes(){
			$query = "SELECT * FROM codes ORDER by code_id ASC";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_code(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			$query = "SELECT * FROM codes WHERE code_id = ".$id." LIMIT 1";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		#################################################################
		# SETTINGS PART													#
		#################################################################

		private function init_settings(){
			//read the settings from the cms
			$this->settings = $this->utils->get_settings(array('module' => $this->module));
			return true;
		}//end function

		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function

		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>
