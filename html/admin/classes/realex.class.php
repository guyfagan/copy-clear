<?php
	require_once('payments.class.php');

	class realex extends payments{		
	
		private $merchantid;
		private $secret;
		private $account;
		private $currency;
		private $URL;
		private $mode;
		private $response;
		public $xml_result;
		private $secure_enabled;
	
		//Constructor
		function __construct($utils,$params = array()){	
			$this->utils = $utils;			
			$this->utils->read_params($this,$params);				
			$this->URL = "https://epage.payandshop.com/epage-remote.cgi";
			$this->module = 'payments';
			$this->init_settings();
		}//endconstructor
		
		private function read_card_information($src = NULL){
			$data = array();			
			if($src == NULL){
				$src = $_POST;
			}//end if
			$data = $src;						
			return $data;
		}//end function
		
		public function enable_secure_transfer($secure){
			$this->secure_enabled = $secure;
		}//end function
		
		public function realex_payment($src_data = NULL){
			//read the POST data
			$data = $this->read_card_information($src_data);			
			// The Timestamp is created here and used in the digital signature
			$timestamp = strftime("%Y%m%d%H%M%S");
			mt_srand((double)microtime()*1000000);
			// Order ID -  You can use any alphanumeric combination for the orderid.Although each transaction must have a unique orderid.
			$orderid = $timestamp."-".mt_rand(1, 999);
			// This section of code creates the md5hash that is needed
			$amount = $data['amount'];
			$merchantid = $this->merchantid;
			$currency = $this->currency;
			$cardnumber = $data['cardnumber'];
			$secret = $this->secret;
			$tmp = "$timestamp.$merchantid.$orderid.$amount.$currency.$cardnumber";	
			$md5hash = md5($tmp);			
			$tmp = "$md5hash.$secret";
			$md5hash = md5($tmp);
		
			
			//A number of variables are needed to generate the request xml that is send to Realex Payments.
			$xml = "<request type='auth' timestamp='".$timestamp."'>
				<merchantid>".$this->merchantid."</merchantid>	
				<account>".$this->account."</account>
				<orderid>".$orderid."</orderid>
				<amount currency='".$this->currency."'>".$data['amount']."</amount>
				<card> 
					<number>".$data['cardnumber']."</number>
					<expdate>".$data['expdate']."</expdate>
					<type>".$data['cardtype']."</type> 
					<chname>".$data['cardname']."</chname> 
					<cvn>
						<number>".$data['cv2']."</number>
						<presind>1</presind>
					</cvn>
				</card> 
				<autosettle flag='1'/>
				<md5hash>".$md5hash."</md5hash>
				<tssinfo>
					<address type=\"billing\">
						 <country>ie</country>
					</address>
				</tssinfo>
			</request>";
					
			//echo "<pre>".$xml."</pre>";	
			
			$ch = curl_init();    
			curl_setopt($ch, CURLOPT_URL, $this->URL );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_USERAGENT, "payandshop.com php version 0.9"); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		
			if($this->secure_enabled == true){
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // this line makes it work under https 	
			}//end if
			$this->response = curl_exec ($ch);     
			curl_close ($ch); 
			
			$this->response= eregi_replace ( "[[:space:]]+", " ", $this->response);
			$this->response = eregi_replace ( "[\n\r]", "", $this->response );	
				
			$this->xmlData = new SimpleXMLElement($this->response);
			foreach($this->xmlData as $attribute => $value){
				$this->xml_result[$attribute] = (string)$value;
			} 
			
			if($this->xml_result['result'] == "00"){
				return true;
			} else {
				return $this->xml_result['result'];
			}//end if
		}//end function
		
		public function get_xml_result(){
			return $this->response;
		}//end function
		
		public function get_array_result(){
			return $this->xml_result;
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
		
		private function init_settings(){
			$this->settings = $this->utils->get_settings(array('module' => $this->module));	
			$this->merchantid = $this->settings['paymentMerchantID'];
			$this->secret = $this->settings['paymentSecret'];		
			$this->account = $this->settings['paymentAccount'];	
			$this->currency = $this->settings['paymentCurrency'];
			$this->secure_enabled = (bool)$this->settings['paymentSecureServer'];
			$this->mode = $this->settings['realexMode'];
			if($this->mode == 'test'){
				$this->secret = "secret";
				$this->account = "internettest";
			}//end if			
			return true;
		}//end function
		
	}//end class
?>