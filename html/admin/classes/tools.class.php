<?php
	#################################################################
	# Tools Class - 	last update 22/12/12						#
	#					created in 16/12/12							#
	# This class contains useful but not essential methods that 	#
	# used to be in utils.class.php. It has been created mostly for	#
	# shrink down the size of utils class							#
	#																#
	# Updates														#
	# 22/12/12 - Converted to PDO									#	
	# 15/03/13 - Not anymore a child of utils						#
	#################################################################
		
	class tools{
		
		protected $utils;//utils class
		public $errors = array();
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->utils->read_params($this,$params);	
		}//endconstructor
		
		##################################################
		# Methods inherited from extended ewrite classes #
		##################################################
		
		public function google_title($title,$add_ext = true){
			$title = trim($title);
			$title = html_entity_decode($title);
			$title = str_replace("&","and",$title);
			$title = str_replace(array("\xE2\x80\x99","\xE2\x80\x98"),'\'',$title);
			$title = urlencode($title);	
			$title = $this->unifystring($title);
			$title = str_replace("_","-",$title);
			$title = str_replace("---","-",$title);
			$title = str_replace("--","-",$title);
			$title = strtolower($title);
			//$title = preg_replace("/([0-9])/i","-",$title);
			if($add_ext == true){
				$title .= ".html";
			}//end if
			return $title;
		}//end function	
		
		public function find_links($text){
			// The Regular Expression filter
			$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
					
			// Check if there is a url in the text
			if(preg_match($reg_exUrl, $text, $url)) {			
				   // make the urls hyper links
				   return preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank">'.$url[0].'</a>', $text);			
			} else {			
				   // if no urls in the text just return the text
				   return $text;
			
			}//end if
		}//end function		
		
		######################################
		# Methods inherited from utils class #
		######################################
		
		//Check if in the address the last char is a slash
		public function check_slash($path){
			$lastchar = substr($path,-1,1);
			if($lastchar != "/"){
				$path .= "/";
			}//endif
			return $path;
		}//end function
		
		
		
		//This method check if in the passed address there is the http://
		public function check_http($address){
			$begin = substr($address,0,7);
			if($begin != "http://"){
				$address = "http://".$address;
			}//endif
			return $address;
		}//end function
		
		public function hide_http($address){	
			$begin = substr($address,0,7);
			if($begin == "http://"){
				$address = substr($address,7);
			}//endif
			return $address;
		}//end function
		
		public function friendly_link($address){
			$address = $this->hide_http($address);
			//check for the last slash
			if(substr($address,-1) == "/"){
				$address = substr($address,0,-1);
			}//end if
			return $address;
		}//end function
		
		public function array_cut($key_search,$array){
			$data = array();
			foreach($array as $key => $value){
				if($key_search != $key){
					$data[$key] = $value;
				}//end if
			}//end foreach
			return $data;
		}//end function
		
		//assoc_field = for a multidimensional array, means the associative field to glue
		//simpe_associative = for a simple, but associative array
		public function array_to_stringlist($array,$assoc_field = NULL,$simple_associative = false, $string_glue = NULL){		
			if($string_glue == NULL){			
				$string_glue = ", ";
			}
			//check if is an associative array
			if($assoc_field != NULL){
				for($i = 0; $i < sizeof($array); $i++){
					$str .= $array[$i][$assoc_field].$string_glue;
				}//end for
			} else if($simple_associative == true){
				foreach($array as $value){
					$str .= $value.$string_glue;
				}//end foreach
			} else {
				//It's a normal monodimensional array
				$str = implode($string_glue,$array);				
			}//end if
			//if the string end with ", " cut these two chars
			$end_string = substr($str,-2);
			if($end_string == $string_glue){
				//remove the last two chars
				$str = substr($str,0,strlen($str)-2);
			}//end if
			return $str;
		}//end function
		
		public function get_filetype($mime){
			switch($mime){
				case "image/jpeg":
				case "image/gif":
				case "image/png":
				case "image/pjpeg":
					return "Image";
					break;
				case "application/octet-stream":
				case "application/octet-st":
					return "Multimedia File";
					break;
				case "application/pdf":
					return "PDF";
					break;
				case "application/zip":
					return "Zipped file";
					break;
				case "application/msword":
					return "Word Document";
					break;
				case "application/vnd.ms-excel":
				case "application/vnd.ms-e":
					return "Excel Document";
					break;
				case "application/vnd.ms-p":
				case "application/vnd.ms-powerpoint":
					return "Powerpoint Document";
					break;
				case "audio/mpeg":
				case "audio/x-ms-wma":
					return "Audio";
					break;
				case "text/plain":
					return "Text file";
					break;		
				case "video/mpeg":
				case "video/mp4":
				case "video/quicktime":
				case "video/x-ms-wmv":
					return "Video";
					break;			
				default:
					return $mime;
			}//end switch
		}//end function
		
		//convert a date formatted like dd/mm/YYYY in a unix timestamp
		public function get_unixtimestamp_from_date($dateString){
			$newdate = explode("/",$dateString);
			$newdate = mktime(1,0,0,$newdate[1],$newdate[0],$newdate[2]);
			return $newdate;
		}//end function
		
		public function get_date_from_unixtimestamp($unixtimestamp){
			return date("Y-m-d H:i:s",$unixtimestamp);				
		}//endfunction	
		
		
		#################################################################
		# POST REVISIONS METHODS - BEGIN								#		
		#################################################################
		
		public function get_post_revisions(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($module_id)){				
				return false;	
			}//end if
			
		
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);		
				$module_id = $module_data['module_id'];
			}//end if
			
			$single_user = false;
			if(isset($user_id) && is_numeric($user_id)){
				$single_user = true;
			}//end if			
	
			$fields = "UNIX_TIMESTAMP(post_revision_date) AS post_revision_date, post_revision_id ";
			if(isset($full_details) && (bool)$full_details == true && is_bool($full_details)){
				//it a boolean value is passed, it will show all the fields
				$fields = "*,UNIX_TIMESTAMP(post_revision_date) AS post_revision_date";	
			}//end if
			
			if(isset($sql_fields) && is_string($sql_fields)){
				//if it's a string value, we pass the string straight to the sql
				$fields = $sql_fields;
			} else if(isset($sql_fields) && is_array($sql_fields)){
				//we passed an array with the fields, so from it we create the fields list
				$fields = implode(", ",$sql_fields);
			}//end if
			
			//I'll get only the date and the revision_id
			$query = "SELECT ".$fields."
					  FROM posts_revisions
					  WHERE post_revision_post_id = :post_id
					  AND post_revision_module_id = :module_id";		
			if($single_user){
				$query .= " AND post_revision_user_id = :user_id";
			}//end if
			$query .= " ORDER BY post_revision_date DESC";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);			
			$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
			if($single_user){
				$result->bindParam(':user_id', $tag, PDO::PARAM_INT);
			}//end if
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_post_revision(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($module_id) || !isset($id)){				
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];
			}//end if
			
			$single_user = false;
			if(isset($user_id) && is_numeric($user_id)){
				$single_user = true;
			}//end if
			
			$fields = "UNIX_TIMESTAMP(post_revision_date) AS post_revision_date, post_revision_id ";
			if(isset($full_details) && (bool)$full_details == true && is_bool($full_details)){
				//it a boolean value is passed, it will show all the fields
				$fields = "*, UNIX_TIMESTAMP(post_revision_date) AS post_revision_date";	
			}//end if
			
			if(isset($sql_fields) && is_string($sql_fields)){
				//if it's a string value, we pass the string straight to the sql
				$fields = $sql_fields;
			} else if(isset($sql_fields) && is_array($sql_fields)){
				//we passed an array with the fields, so from it we create the fields list
				$fields = implode(", ",$sql_fields);
			}//end if
			
			//I'll get only the date and the revision_id
			$query = "SELECT ".$fields."
					  FROM posts_revisions
					  WHERE post_revision_post_id = :post_id
					  AND post_revision_module_id = :module_id
					  AND post_revision_id = :id";			
			if($single_user){
				$query .= " AND post_revision_user_id = :user_id";
			}//end if						
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);	
			$result->bindParam(':id', $id, PDO::PARAM_INT);			
			$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
			if($single_user){
				$result->bindParam(':user_id', $tag, PDO::PARAM_INT);
			}//end if
			$result->execute();
			
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);						
					//we check if in the results we have the post_revision_data field					
					if(isset($data['post_revision_data'])){
						//if yes, we go through the array and we decrypt the data									
						$data['post_revision_data'] = unserialize(base64_decode($data['post_revision_data']));						
					}//end if
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		#################################################################
		# POST REVISIONS METHODS - END									#		
		#################################################################
		
		#################################################################
		# TAGS METHODS - BEGIN											#		
		#################################################################
		
		public function get_tags(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($module_id)){				
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];	
			}//end if
			
			$query = "SELECT * 
					  FROM tags
					  WHERE tag_post_id = ".$post_id."
					  AND tag_module_id = ".$module_id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_tags_cloud(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
	
			if(isset($module_id)){
				if(!is_numeric($module_id)){
					$module_data = $this->utils->get_module($module_id);
					$module_id = $module_data['module_id'];	
				}//end if
			}//end if
			
			$query = "SELECT COUNT(tag_id) AS tag_number, tag_tag AS tag
					  FROM tags";
			if(isset($module_id)){
				$query .= " WHERE tag_module_id = ".$module_id;
			}//end if
			$query .= " GROUP BY tag_tag";
			if(isset($limit) && is_numeric($limit)){
				$query .= " LIMIT ".$limit;	
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function add_tags(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($module_id) || !isset($tags)){				
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];	
			}//end if
			
			//split the tags string
			$tags = explode(",",$tags);
			$tags_done = array();			
			if(is_array($tags) && sizeof($tags) > 0){
				for($i = 0; $i < sizeof($tags); $i++){
					$tag = trim($tags[$i]);
					$tag = urlencode($tag);
					$tag = strtolower(iconv('ISO-8859-1','ASCII//TRANSLIT',$tag));
					$tag = str_replace("+","-",$tag);
					//now we check if this tag already exists for this post
					$already_set = $this->post_tag_already_set(array(
						"post_id" => $post_id,
						"module_id" => $module_id,
						"tag" => $tag
					));					
					if(!$already_set && $tag != ""){
						//it's not set, so we add it to the database
						try{
							$query = "INSERT INTO tags
									  (tag_post_id, tag_tag, tag_module_id)
									  VALUES
									  (:post_id, :tag, :module_id)";
							$result = $this->utils->db->prepare($query);
							$result->bindParam(':post_id', $post_id, PDO::PARAM_INT);
							$result->bindParam(':tag', $tag, PDO::PARAM_STR);
							$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
							$result->execute();
							$errors = $this->utils->error($result,__LINE__,get_class($this));
							if($errors === false){
								array_push($tags_done,$tag);
							}//end if
						} catch(PDOException $ex){
							$this->errors = $ex->getMessage();
							return false;	
						}//end if
					}//end if
				}//end for i
				return $tags_done;
			} else {
				return false;
			}//end if
		}//end function
		
		protected function post_tag_already_set(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($module_id) || !isset($tag)){
				return false;	
			}//end if
			
			$query = "SELECT * 
					  FROM tags 
					  WHERE tag_post_id = ".$post_id."
					  AND tag_tag = '".trim($tag)."'
					  AND tag_module_id = ".$module_id;					 
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function delete_tag(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($module_id) || !isset($tag)){
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];	
			}//end if
			
			$query = "DELETE FROM tags 
					  WHERE tag_post_id = ".$post_id."
					  AND tag_tag = '".trim($tag)."'
					  AND tag_module_id = ".$module_id;									 
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function delete_post_tags(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($module_id)){
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];	
			}//end if
			
			$query = "DELETE FROM tags WHERE tag_post_id = ".$post_id." AND tag_module_id = ".$module_id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		
		#################################################################
		# TAGS METHODS - END											#		
		#################################################################
		
		public function date_to_unix($date){
			$date = str_replace('/', '-', $date);
			return strtotime($date);
		}//end function
		
		public function format_date($date,$length = 'full'){
			if($date != NULL){
				if($length == 'full'){
					return date("d/m/Y H:i",$date);
				} else {
					return date("d/m/Y",$date);
				}//endif			
			} else {
				if($length == 'full'){
					return "--/--/---- --:--";
				} else {
					return "--/--/----";
				}//end if
			}//end if
		}//end function
		
		public function get_random_password(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($length)){
				$length = 10;	
				$password = '';
				for($i = 0; $i < $length; $i++){
					$char = $this->get_random_alphanum();
					$randCase = rand(0,1);					
					if($randCase == 0){		
						$char = strtoupper($char);							
					}
					$password .= $char;
				}//end for
			}//end if
			return $password;
		}//end function
		
		public function get_random_letter(){
			$AtoZ = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
			$chars = sizeof($AtoZ);
			$chars--;
			$rand = rand(0,$chars);
			return $AtoZ[$rand];
		}//end function
		
		public function get_random_alphanum(){
			$AtoZ = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9");
			$chars = sizeof($AtoZ);
			$chars--;
			$rand = rand(0,$chars);
			return $AtoZ[$rand];
		}//end if
		
		public function change_array_prefix($array,$newprefix,$oldprefix){
			$data = array();
			if(is_array($array[0])){
				for($i = 0; $i < sizeof($array); $i++){		
					$data[$i] = array();
					foreach($array[$i] as $key => $value){
					//echo $key;
						$newkey = str_replace($oldprefix,$newprefix,$key);
						//echo $newkey;
						$data[$i][$newkey] = $value;
					}//endforeach	
				}//end for	
			} else {
				foreach($array as $key => $value){				
					$newkey = str_replace($oldprefix,$newprefix,$key);				
					$data[$newkey] = $value;
				}//endforeach	
			}//end if
			return $data;
		}//end for
		
		#################################################################
		# SITEMAP GENERATOR PART										#		
		#################################################################
		
		public function get_sitemap(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$sitemap = array();
			
			//if we do not pass the module, it means that we start from the root
			if(!isset($module)){
				$options = array("show_hidden" => false);
				if(isset($group_id)){
					$options['group_id'] = (int)$group_id;	
				}//end if
				if(isset($public)){
					$options['public'] = (bool)$public;	
				}//end if
				$modules = $this->utils->get_modules($options);
				if($modules !== false){
					for($i = 0; $i < sizeof($modules); $i++){
						$sitemap[$modules[$i]['module_name']] =  array();
					}//end for i
				} else {
					return false;
				}//end if
			} else {
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
				$module_name = $module_data['module_name'];
				$sitemap[$module_name] = array();
			}//end if
			
			$ewrite = $this->utils->call("ewrite");
			$items_options = array();
			if(isset($status)){
				$items_options['status'] = $status;	
			}//end if
			foreach($sitemap as $key => $value){
				//now for each module we retrive the items
				$ewrite->set_module($key);
				$items = $ewrite->get_items($items_options);
				if($items !== false){
					for($i = 0; $i < sizeof($items); $i++){
						$sitemap[$key][$i] = array();
						$sitemap[$key][$i]['label'] = $items[$i]['label'];
						$sitemap[$key][$i]['permalink'] = $items[$i]['permalink'];
						$sitemap[$key][$i]['id'] = $items[$i]['id'];
						$sitemap[$key][$i]['is_category'] = (bool)$items[$i]['is_category'];
						if((bool)$sitemap[$key][$i]['is_category']){
							//is a category, so we look inside
							$branch_options = array(
													"module" => $key,
													"category_id" => $items[$i]['id'],
													"class_obj" => $ewrite													
													);
							if(isset($status)){
								$branch_options['status'] = $status;	
							}//end if
							$sitemap[$key][$i]['content'] = $this->get_sitemap_branch($branch_options);
							
						}
					}//end for i
				}//end if
			}//end foreach
			
			return $sitemap;
		}//end function
		
		protected function get_sitemap_branch(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$branch = array();
			
			if(!isset($category_id) || !isset($module) || !isset($class_obj)){
				return false;
			}//end if
			
			$items_options = array("category_id" => $category_id);
			if(isset($status)){
				$items_options['status'] = $status;	
			}//end if
			$items = $class_obj->get_items($items_options);
			
			if($items !== false){
				for($i = 0; $i < sizeof($items); $i++){
					$branch[$i] = array();
					$branch[$i]['label'] = $items[$i]['label'];
					$branch[$i]['permalink'] = $items[$i]['permalink'];
					$branch[$i]['id'] = $items[$i]['id'];
					$branch[$i]['is_category'] = (bool)$items[$i]['is_category'];
					if((bool)$branch[$i]['is_category']){
						$subbranch_options = array("category_id" => $items[$i]['id'],"class_obj" => $class_obj,"module" => $module);
						if(isset($status)){
							$subbranch_options['status'] = $status;
						}//end if
						$branch[$i]['content'] = $this->get_sitemap_branch($subbranch_options);
					}//end if
				}//end for i					
			}//end if
			return $branch;
		}//end function
		
		//this function return a valid sitemap xml
		public function get_google_sitemap(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$options = array("public" => true,"status" => 1);
			if(isset($module)){
				$options['module'] = $module;	
			}//end if
			
			//first thing we get the sitemap of the site, and as is for google we show only the modules marked as public and categories or posts marked as public
			$sitemap = $this->get_sitemap($options);
			//now we need to translate this into and xml document
			$xml = new DomDocument('1.0', 'utf-8'); 
			$xml->formatOutput = true; 
			
			$urlset = $xml->createElement('urlset'); 
			$urlset->appendChild(
				new DomAttr('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9')
			);
			$xml->appendChild($urlset);
			
			foreach($sitemap as $key => $value){
				//Creating single url node
				$url = $xml->createElement('url'); 
			
				//Filling node with entry info
				$url->appendChild( $xml->createElement('loc', __DOMAIN__.$key.'/') ); 
				//$url->appendChild( $lastmod = $xml->createElement('lastmod', date('Y-m-d\TH:i\T\Z\D ',time())) ); 
				$url->appendChild( $changefreq = $xml->createElement('changefreq', 'daily') ); 
				$url->appendChild( $priority = $xml->createElement('priority', '1.0') ); 
				
				// append url to urlset node
				$urlset->appendChild($url);
				
				//now we check what's inside each module
				
				for($i = 0; $i < sizeof($value); $i++){
					//Creating single url node
					$url = $xml->createElement('url'); 
					
					if($value[$i]['is_category']){
						//Filling node with entry info
						$url->appendChild( $xml->createElement('loc', __DOMAIN__.$key.'/'.$value[$i]['permalink'].'/') ); 
						//$url->appendChild( $lastmod = $xml->createElement('lastmod', date('Y-m-d\TH:i\T\Z\D ',time())) ); 
						$url->appendChild( $changefreq = $xml->createElement('changefreq', 'daily') ); 
						$url->appendChild( $priority = $xml->createElement('priority', '0.8') ); 
						// append url to urlset node
						$urlset->appendChild($url);
						if(is_array($value[$i]['content'])){							
							$branch = $this->get_google_sitemap_branch(array("content" => $value[$i]['content'],"xml" => $xml, "urlset" => $urlset,"module" => $key));
							/*if($branch !== false){
								$urlset->appendChild($branch);
							}//end if*/
						}//end if
					} else {
						//Filling node with entry info
						$url->appendChild( $xml->createElement('loc', __DOMAIN__.$key.'/'.$value[$i]['permalink'].'.html') ); 
						//$url->appendChild( $lastmod = $xml->createElement('lastmod', date('Y-m-d\TH:i\T\Z\D ',time())) ); 
						$url->appendChild( $changefreq = $xml->createElement('changefreq', 'daily') ); 
						$url->appendChild( $priority = $xml->createElement('priority', '0.5') ); 
						// append url to urlset node
						$urlset->appendChild($url);
					}//end if
					
				}//end for i				
			}//end foreach
			
			if(isset($save_file)){
				$xml_result = $xml->save($_SERVER["DOCUMENT_ROOT"].__SERVERPATH__.$save_file);
				return true;
			} else {
				$xml_result = $xml->saveXML();
				return $xml_result;
			}//end if			
		}//end function
		
		protected function get_google_sitemap_branch(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if			
			
			if(!isset($content) || !isset($xml) || !isset($urlset) || !isset($module)){
				return false;
			}//end if
			
			for($i = 0; $i < sizeof($content); $i++){
				//Creating single url node
				$url = $xml->createElement('url'); 
				
				if($content[$i]['is_category']){
					//Filling node with entry info
					$url->appendChild( $xml->createElement('loc', __DOMAIN__.$module.'/'.$content[$i]['permalink'].'/') ); 
					//$url->appendChild( $lastmod = $xml->createElement('lastmod', date('Y-m-d\TH:i\T\Z\D ',time())) ); 
					$url->appendChild( $changefreq = $xml->createElement('changefreq', 'daily') ); 
					$url->appendChild( $priority = $xml->createElement('priority', '0.8') ); 
					// append url to urlset node
					$urlset->appendChild($url);
					if(is_array($content[$i]['content'])){
						$branch = $this->get_google_sitemap_branch(array("content" => $content[$i]['content'],"xml" => $xml, "urlset" => $urlset));						
					}//end if
				} else {
					//Filling node with entry info
					$url->appendChild( $xml->createElement('loc', __DOMAIN__.$module.'/'.$content[$i]['permalink'].'.html') ); 
					//$url->appendChild( $lastmod = $xml->createElement('lastmod', date('Y-m-d\TH:i\T\Z\D ',time())) ); 
					$url->appendChild( $changefreq = $xml->createElement('changefreq', 'daily') ); 
					$url->appendChild( $priority = $xml->createElement('priority', '0.5') ); 
					// append url to urlset node
					$urlset->appendChild($url);
				}//end if
			}//end for i
			
		}//end function
		
		#################################################################
		# XML UTILITIES PART											#		
		#################################################################
		
		public function array_to_xml($array, $rootElement = NULL, $xml = NULL) {	
			$_xml = $xml;			
			if ($_xml === null) {
				$_xml = new SimpleXMLElement($rootElement !== NULL ? $rootElement : '<root/>');
			}			
			foreach ($array as $k => $v) {
				if (is_array($v)) { //nested array
					$this->array_to_xml($v, $k, $_xml->addChild($k));
				} else {
					$_xml->addChild($k, $v);
				}
			}			 
			return $_xml->asXML();			
		}//end function
		
		
		#################################################################
		# LANGUAGE PART													#		
		#################################################################
		
		public function get_languages($status = NULL){
			$query = "SELECT * FROM ".$this->t."languages";
			if($status != NULL && is_numeric($status)){
				$query .= " WHERE language_enabled = ".$status;
			}//end if
			$query .= " ORDER BY language_order ASC";
			$result = $this->db->query($query);
			$this->error($result,__LINE__,get_class($this));	
			$num = $result->rowCount();
			if($num > 0){							
				$data = $this->get_result_array($result,false);	
				return $data;
			} else {
				return false;
			}//end if
		}//end function		
		
		public function get_language($id){
			if(is_numeric($id)){
				$query = "SELECT * FROM ".$this->t."languages WHERE language_id = ".$id;
			} else {
				$query = "SELECT * FROM ".$this->t."languages WHERE language_tld = '".$id."'";
			}//end if
			
			$result = $this->db->query($query);
			$this->error($result,__LINE__,get_class($this));	
			$num = $result->rowCount();
			if($num > 0){							
				$data = $this->get_result_array($result,true);	
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function save_language_sorting($post_ids){
			$pi = $post_ids;
			if(is_array($pi) && sizeof($pi) > 0){
				for($i = 0; $i < sizeof($pi); $i++){		
					$query = "UPDATE ".$this->t."languages 
							  SET language_order = ".$i."
							  WHERE language_id = ".$pi[$i];
					$this->db->query($query);
					$this->error($result,__LINE__,get_class($this));	
				}//end for
				return true;
			} else {
				return false;
			}//end if	
		}//end function
		
		public function save_language(){
			$hidden_fields = array('action','step','submit');
			$_POST['flag_image'] = $_POST['tld'].".gif";
			if($_POST['enabled'] == NULL){
				$_POST['enabled'] = "0";
			}//end if
			if($_POST['content_redirect'] == NULL){
				$_POST['content_redirect'] = "NULL";
			}//end if
			if($_REQUEST['id'] == NULL){
				$sql = $this->build_sql_fields("language","INSERT","POST",$hidden_fields);				
				$query = "INSERT INTO ".$this->t."languages ".$sql;	
				$result = $this->db->query($query);
				$this->error($result,__LINE__,get_class($this));
				$lid = $this->db->lastInsertId();
			} else {
				$lid = (int)$_REQUEST['id'];
				$sql = $this->build_sql_fields("language","UPDATE","POST",$hidden_fields);				
				$query = "UPDATE ".$this->t."languages SET ".$sql." WHERE language_id = ".$lid;				
				$result = $this->db->query($query);
				$this->error($result,__LINE__,get_class($this));
			}//end if
		}//end function
		
		private function get_mimetype($value = "") {		
			$ct = array();			
			$ct['htm'] = 'text/html';		
			$ct['html'] = 'text/html';		
			$ct['txt'] = 'text/plain';
			$ct['asc'] = 'text/plain';
			$ct['bmp'] = 'image/bmp';
			$ct['gif'] = 'image/gif';
			$ct['jpeg'] = 'image/jpeg';
			$ct['jpg'] = 'image/jpeg';
			$ct['jpe'] = 'image/jpeg';
			$ct['png'] = 'image/png';
			$ct['ico'] = 'image/vnd.microsoft.icon';
			$ct['mpeg'] = 'video/mpeg';
			$ct['mpg'] = 'video/mpeg';
			$ct['mpe'] = 'video/mpeg';
			$ct['flv'] = 'video/mpeg';
			$ct['qt'] = 'video/quicktime';
			$ct['mov'] = 'video/quicktime';
			$ct['avi'] = 'video/x-msvideo';
			$ct['wmv'] = 'video/x-ms-wmv';
			$ct['mp2'] = 'audio/mpeg';
			$ct['mp3'] = 'audio/mpeg';
			$ct['rm'] = 'audio/x-pn-realaudio';
			$ct['ram'] = 'audio/x-pn-realaudio';
			$ct['rpm'] = 'audio/x-pn-realaudio-plugin';
			$ct['ra'] = 'audio/x-realaudio';
			$ct['wav'] = 'audio/x-wav';
			$ct['css'] = 'text/css';
			$ct['zip'] = 'application/zip';
			$ct['pdf'] = 'application/pdf';
			$ct['doc'] = 'application/msword';
			$ct['bin'] = 'application/octet-stream';
			$ct['exe'] = 'application/octet-stream';
			$ct['class']= 'application/octet-stream';
			$ct['dll'] = 'application/octet-stream';
			$ct['xls'] = 'application/vnd.ms-excel';
			$ct['ppt'] = 'application/vnd.ms-powerpoint';
			$ct['wbxml']= 'application/vnd.wap.wbxml';
			$ct['wmlc'] = 'application/vnd.wap.wmlc';
			$ct['wmlsc']= 'application/vnd.wap.wmlscriptc';
			$ct['dvi'] = 'application/x-dvi';
			$ct['spl'] = 'application/x-futuresplash';
			$ct['gtar'] = 'application/x-gtar';
			$ct['gzip'] = 'application/x-gzip';
			$ct['js'] = 'application/x-javascript';
			$ct['swf'] = 'application/x-shockwave-flash';
			$ct['tar'] = 'application/x-tar';
			$ct['xhtml']= 'application/xhtml+xml';
			$ct['au'] = 'audio/basic';
			$ct['snd'] = 'audio/basic';
			$ct['midi'] = 'audio/midi';
			$ct['mid'] = 'audio/midi';
			$ct['m3u'] = 'audio/x-mpegurl';
			$ct['tiff'] = 'image/tiff';
			$ct['tif'] = 'image/tiff';
			$ct['rtf'] = 'text/rtf';
			$ct['wml'] = 'text/vnd.wap.wml';
			$ct['wmls'] = 'text/vnd.wap.wmlscript';
			$ct['xsl'] = 'text/xml';
			$ct['xml'] = 'text/xml';
			
			$extension = $this->utils->get_file_extension($value);
			
			if (!$type = $ct[strtolower($extension)]) {
				$type = 'text/html';
			}//end if
			
			return $type;

		}//end function
		
		public function remove_shouting($string){
			/*$string = strtolower(trim($string));
			$new_string = "";
			$strings = explode(".",$string);
			for($i = 0; $i < sizeof($strings); $i++){
				if(trim($strings[$i]) != ""){
					$new_string .= ucfirst(trim($strings[$i])).". ";
				}//end if
			}//end for
			if(substr($new_string,-2) == ". "){
				$new_string = substr($new_string,0,-2);
			}
			return $new_string;	*/
			$sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
			$new_string = '';
			foreach ($sentences as $key => $sentence) {
				$new_string .= ($key & 1) == 0?
					ucfirst(strtolower(trim($sentence))) :
					$sentence.' ';
			}
			return trim($new_string); 
		}//end function
		
		public function count_post_attachments(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($module_id) || !isset($post_id)){
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				if($module_data !== false){
					$module_id = $module_data['module_id'];
				} else {
					return false;	
				}//end if
			}//end if
						
			$query = "SELECT COUNT(*) AS found FROM attachments_posts WHERE attachment_post_post_id = ".$post_id." AND attachment_post_module_id = ".$module_id;
			$result = @$this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$row = $result->fetch(PDO::FETCH_ASSOC);			
				return $row['found'];
			} else {
				return false;	
			}
		}//end function
		
		public function get_mime_from_file($filename){
			$mime = false;
			if(function_exists('mime_content_type')){
				$mime = mime_content_type($filename);
			} else if(function_exists('finfo_open')){
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$mime = finfo_file($finfo, $filename);
				finfo_close($finfo);
			}//end if
			return $mime;
		}//end function
		
		public function get_mime_from_filename($filename){
			// MIME types array
			$mimeTypes = array(
				"323"       => "text/h323",
				"acx"       => "application/internet-property-stream",
				"ai"        => "application/postscript",
				"aif"       => "audio/x-aiff",
				"aifc"      => "audio/x-aiff",
				"aiff"      => "audio/x-aiff",
				"asf"       => "video/x-ms-asf",
				"asr"       => "video/x-ms-asf",
				"asx"       => "video/x-ms-asf",
				"au"        => "audio/basic",
				"avi"       => "video/x-msvideo",
				"axs"       => "application/olescript",
				"bas"       => "text/plain",
				"bcpio"     => "application/x-bcpio",
				"bin"       => "application/octet-stream",
				"bmp"       => "image/bmp",
				"c"         => "text/plain",
				"cat"       => "application/vnd.ms-pkiseccat",
				"cdf"       => "application/x-cdf",
				"cer"       => "application/x-x509-ca-cert",
				"class"     => "application/octet-stream",
				"clp"       => "application/x-msclip",
				"cmx"       => "image/x-cmx",
				"cod"       => "image/cis-cod",
				"cpio"      => "application/x-cpio",
				"crd"       => "application/x-mscardfile",
				"crl"       => "application/pkix-crl",
				"crt"       => "application/x-x509-ca-cert",
				"csh"       => "application/x-csh",
				"css"       => "text/css",
				"dcr"       => "application/x-director",
				"der"       => "application/x-x509-ca-cert",
				"dir"       => "application/x-director",
				"dll"       => "application/x-msdownload",
				"dms"       => "application/octet-stream",
				"doc"       => "application/msword",
				"dot"       => "application/msword",
				"dvi"       => "application/x-dvi",
				"dxr"       => "application/x-director",
				"eps"       => "application/postscript",
				"etx"       => "text/x-setext",
				"evy"       => "application/envoy",
				"exe"       => "application/octet-stream",
				"fif"       => "application/fractals",
				"flr"       => "x-world/x-vrml",
				"gif"       => "image/gif",
				"gtar"      => "application/x-gtar",
				"gz"        => "application/x-gzip",
				"h"         => "text/plain",
				"hdf"       => "application/x-hdf",
				"hlp"       => "application/winhlp",
				"hqx"       => "application/mac-binhex40",
				"hta"       => "application/hta",
				"htc"       => "text/x-component",
				"htm"       => "text/html",
				"html"      => "text/html",
				"htt"       => "text/webviewhtml",
				"ico"       => "image/x-icon",
				"ief"       => "image/ief",
				"iii"       => "application/x-iphone",
				"ins"       => "application/x-internet-signup",
				"isp"       => "application/x-internet-signup",
				"jfif"      => "image/pipeg",
				"jpe"       => "image/jpeg",
				"jpeg"      => "image/jpeg",
				"jpg"       => "image/jpeg",
				"js"        => "application/x-javascript",
				"latex"     => "application/x-latex",
				"lha"       => "application/octet-stream",
				"lsf"       => "video/x-la-asf",
				"lsx"       => "video/x-la-asf",
				"lzh"       => "application/octet-stream",
				"m13"       => "application/x-msmediaview",
				"m14"       => "application/x-msmediaview",
				"m3u"       => "audio/x-mpegurl",
				"man"       => "application/x-troff-man",
				"mdb"       => "application/x-msaccess",
				"me"        => "application/x-troff-me",
				"mht"       => "message/rfc822",
				"mhtml"     => "message/rfc822",
				"mid"       => "audio/mid",
				"mny"       => "application/x-msmoney",
				"mov"       => "video/quicktime",
				"movie"     => "video/x-sgi-movie",
				"mp2"       => "video/mpeg",
				"mp3"       => "audio/mp3",
				"mp4"       => "video/mp4",
				"mpa"       => "video/mpeg",
				"mpe"       => "video/mpeg",
				"mpeg"      => "video/mpeg",
				"mpg"       => "video/mpeg",
				"mpp"       => "application/vnd.ms-project",
				"mpv2"      => "video/mpeg",
				"ms"        => "application/x-troff-ms",
				"mvb"       => "application/x-msmediaview",
				"nws"       => "message/rfc822",
				"oda"       => "application/oda",
				"p10"       => "application/pkcs10",
				"p12"       => "application/x-pkcs12",
				"p7b"       => "application/x-pkcs7-certificates",
				"p7c"       => "application/x-pkcs7-mime",
				"p7m"       => "application/x-pkcs7-mime",
				"p7r"       => "application/x-pkcs7-certreqresp",
				"p7s"       => "application/x-pkcs7-signature",
				"pbm"       => "image/x-portable-bitmap",
				"pdf"       => "application/pdf",
				"pfx"       => "application/x-pkcs12",
				"pgm"       => "image/x-portable-graymap",
				"pko"       => "application/ynd.ms-pkipko",
				"pma"       => "application/x-perfmon",
				"pmc"       => "application/x-perfmon",
				"pml"       => "application/x-perfmon",
				"pmr"       => "application/x-perfmon",
				"pmw"       => "application/x-perfmon",
				"pnm"       => "image/x-portable-anymap",
				"pot"       => "application/vnd.ms-powerpoint",
				"ppm"       => "image/x-portable-pixmap",
				"pps"       => "application/vnd.ms-powerpoint",
				"ppt"       => "application/vnd.ms-powerpoint",
				"prf"       => "application/pics-rules",
				"ps"        => "application/postscript",
				"pub"       => "application/x-mspublisher",
				"qt"        => "video/quicktime",
				"ra"        => "audio/x-pn-realaudio",
				"ram"       => "audio/x-pn-realaudio",
				"ras"       => "image/x-cmu-raster",
				"rgb"       => "image/x-rgb",
				"rmi"       => "audio/mid",
				"roff"      => "application/x-troff",
				"rtf"       => "application/rtf",
				"rtx"       => "text/richtext",
				"scd"       => "application/x-msschedule",
				"sct"       => "text/scriptlet",
				"setpay"    => "application/set-payment-initiation",
				"setreg"    => "application/set-registration-initiation",
				"sh"        => "application/x-sh",
				"shar"      => "application/x-shar",
				"sit"       => "application/x-stuffit",
				"snd"       => "audio/basic",
				"spc"       => "application/x-pkcs7-certificates",
				"spl"       => "application/futuresplash",
				"src"       => "application/x-wais-source",
				"sst"       => "application/vnd.ms-pkicertstore",
				"stl"       => "application/vnd.ms-pkistl",
				"stm"       => "text/html",
				"svg"       => "image/svg+xml",
				"sv4cpio"   => "application/x-sv4cpio",
				"sv4crc"    => "application/x-sv4crc",
				"t"         => "application/x-troff",
				"tar"       => "application/x-tar",
				"tcl"       => "application/x-tcl",
				"tex"       => "application/x-tex",
				"texi"      => "application/x-texinfo",
				"texinfo"   => "application/x-texinfo",
				"tgz"       => "application/x-compressed",
				"tif"       => "image/tiff",
				"tiff"      => "image/tiff",
				"tr"        => "application/x-troff",
				"trm"       => "application/x-msterminal",
				"tsv"       => "text/tab-separated-values",
				"txt"       => "text/plain",
				"uls"       => "text/iuls",
				"ustar"     => "application/x-ustar",
				"vcf"       => "text/x-vcard",
				"vrml"      => "x-world/x-vrml",
				"wav"       => "audio/x-wav",
				"wcm"       => "application/vnd.ms-works",
				"wdb"       => "application/vnd.ms-works",
				"wks"       => "application/vnd.ms-works",
				"wmf"       => "application/x-msmetafile",
				"wps"       => "application/vnd.ms-works",
				"wri"       => "application/x-mswrite",
				"wrl"       => "x-world/x-vrml",
				"wrz"       => "x-world/x-vrml",
				"xaf"       => "x-world/x-vrml",
				"xbm"       => "image/x-xbitmap",
				"xla"       => "application/vnd.ms-excel",
				"xlc"       => "application/vnd.ms-excel",
				"xlm"       => "application/vnd.ms-excel",
				"xls"       => "application/vnd.ms-excel",
				"xlsx"      => "vnd.ms-excel",
				"xlt"       => "application/vnd.ms-excel",
				"xlw"       => "application/vnd.ms-excel",
				"xof"       => "x-world/x-vrml",
				"xpm"       => "image/x-xpixmap",
				"xwd"       => "image/x-xwindowdump",
				"z"         => "application/x-compress",
				"zip"       => "application/zip"
			);
			
			$ext = $this->utils->get_file_extension($filename);
			$mime = $mimeTypes[$ext];
			return $mime;
			
		}//end function
		
		public function get_round_file_size($size){
			switch($size){
				case $size <= 1024:
					return $size." bytes";
					break;
				case ($size > 1024 && $size <= (1024*1024)):
					return round($size/1024,1)." Kb";
					break;
				case ($size > (1024*1024) && $size <= (1024*1024*1024)):
					return round($size/(1024*1024),1)." Mb";
					break;
				case ($size > (1024*1024*1024) && $size <= (1024*1024*1024*1024)):
					return round($size/(1024*1024*1024),1)." Gb";
					break;
				default:
					return $size." bytes";
			}//endswitch
		}//endfunction
		
		public function is_audio($mime){
			$isAudio = false;
			switch($mime){
				case "audio/mpeg":
				case "audio/mp3":
					$isAudio = true;
					break;			
				default:
					$isAudio = false;
			}//end if
			return $isAudio;
		}//end function
		
		public function aasort($array, $key) {
			$sorter=array();
			$ret=array();
			reset($array);
			foreach ($array as $ii => $va) {
				$sorter[$ii]=$va[$key];
			}
			asort($sorter);
			foreach ($sorter as $ii => $va) {
				$ret[$ii]=$array[$ii];
			}
			$array=$ret;
			return $array;
		}//end function
		
		/* UTF-8 ENCODING/DECODING */
		
		public function array_utf8_decode_recursive($dat){ 
			if (is_string($dat)) {
        	    return utf8_decode($dat);
        	}
			if (is_object($dat)) {
				$ovs= get_object_vars($dat);
				$new=$dat;
				foreach ($ovs as $k =>$v)    {
					$new->$k=$this->array_utf8_decode_recursive($new->$k);
				}
				return $new;
			}
			
			if (!is_array($dat)) return $dat;
			$ret = array();
			foreach($dat as $i=>$d) $ret[$i] = $this->array_utf8_decode_recursive($d);
			return $ret;
        } 
		
		public function array_utf8_encode_recursive($dat){ 
			if (is_string($dat)) {
				return utf8_encode($dat);
			  }//end if
			  if (is_object($dat)) {
				$ovs= get_object_vars($dat);
				$new=$dat;
				foreach ($ovs as $k =>$v)    {
					$new->$k=$this->array_utf8_encode_recursive($new->$k);
				}
				return $new;
			  }//end if
			 
			  if (!is_array($dat)) return $dat;
			  $ret = array();
			  foreach($dat as $i=>$d) $ret[$i] = $this->array_utf8_encode_recursive($d);
			  return $ret;
		}//end function
		
		/* ENCRYPT/DECRYPT Methods */
		
		public function encrypt_string($string){
			$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->passKey), $string, MCRYPT_MODE_CBC, md5(md5($this->passKey))));
			return $encrypted;
		}//end function
		
		public function decrypt_string($encrypted_string){
			$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->passKey), base64_decode($encrypted_string), MCRYPT_MODE_CBC, md5(md5($this->passKey))), "\0");
			return $decrypted;
		}//end function
		
		public function encrypt($txt){
			$encrypted = base64_encode(serialize($txt));
			return $encrypted;
		}//end function
		
		public function decrypt($encrypted){
			$txt = unserialize(base64_decode($encrypted));
			return $txt;
		}//end function
		
		public function get_table_size($table){
			$query = 'SELECT table_name AS "name", 
					round(((data_length + index_length) / 1024 / 1024), 2) "size" 
					FROM information_schema.TABLES 
					WHERE table_schema = "'.__DB_NAME__.'"
					 AND table_name = "'.$table.'"';
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$row = $result->fetch(PDO::FETCH_ASSOC);
					return $row['size'];
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function array_to_json($array){
			if(phpversion() >= "5.2.0"){
				return json_encode($array);
			} else {
				$json = "{";		
				foreach($array as $key => $value){
					$json .= '"'.$key.'" : "'.$value.'",'; 
				}//end foreach
				$json = substr($json,0,-1);	
				$json .= "}";
			}
			return $json;
		}//end function		
		
		#################################################################
		# DATABASE UPDATER PART											#		
		#################################################################
		
		public function update_database(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($file_log)){
				return false;
			}//end if
			
			if(!isset($skip_errors)){
				$skip_errors = false;	
			}//end if
			
			try{
				$log = file_get_contents($file_log);
				if($log !== false){
					$log = explode("\n",$log);
					if(is_array($log)){	
						if($skip_errors){
							$this->utils->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
						}//end if
						//now we check the database version in the settings
						$settings = $this->utils->get_settings();						
						if(isset($settings['db_version'])){
							$version = $settings['db_version'];
						} else {
							//if it's not isset	we put a very initial one, back on the 1st october 2013
							$version = "3.0.2.131001";
							//and we save it in the database
							$query = "INSERT INTO settings 
									  (setting_label, setting_value, setting_module_id)
									  VALUES
									  ('db_version','".$version."',0)";
							$result = $this->utils->db->query($query);
							$errors = $this->utils->error($result,__LINE__,get_class($this));	
						}//end if
										
						foreach($log as $line){
							$sql = explode("]=",$line);
							$sql_version = trim(str_replace("[","",$sql[0]));
							$sql = $sql[1];								
							if($sql_version	> $version){							
								$result = $this->utils->db->query($sql);
								$errors = $this->utils->error($result,__LINE__,get_class($this));	
								if($errors !== false && !$skip_errors){
									throw new Exception("Couldn't execute this query > ".$sql);
								}//end if
								
							}//end if
						}//end foreach
						
						//once everything is done, we updated the database with the latest version
						$query = "UPDATE settings SET
								  setting_value = '".__CMS_VERSION__."'
								  WHERE setting_label = 'db_version'
								  AND setting_module_id = 0";
						$result = $this->utils->db->query($query);
						$errors = $this->utils->error($result,__LINE__,get_class($this));	
					} else {
						throw new Exception("File is empty");
					}
				} else {
					throw new Exception("Cannot read the file");
				}//end if
				return true;
			} catch(Exception $e){
				array_push($this->errors,$e->getMessage());
				return false;	
			}
		}//end function
		
		#################################################################
		# IMPORT FILES INTO A TABLE										#		
		#################################################################
		
		//In this function you will need a few things, the file to import (a csv file), a mapping of the fields, and the table to store the data
		public function import_into_table(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($map) || !isset($file) || !isset($table)){
				return false;	
			}//end if
			
			if(!isset($ignore_titles)){
				$ignore_titles = false;	
			}//end if
			
			if(!isset($separator)){
				$separator = ";";	
			}//end if
			
			if(!is_array($map)){
				return false;	
			}//end if
			
			$handle = fopen($file, "r");
			$data = fread($handle,filesize($file));
			$data = explode("\n",$data);
			
			if(isset($ignore_titles)){
				array_shift($data);
			}//end if
			/*
			$fields = $map;			
			array_walk($fields,array($this,'add_column'));
			*/			
			for($i = 0; $i < sizeof($data); $i++){
				
				$line = explode($separator,$data[$i]);
				foreach($line as $key => $value){
					$line[$key] = addslashes(trim(str_replace('"','',$value)));	
				}
				
				$query = "INSERT INTO ".$table."
						  (".implode(",",$map).")
						  VALUES
						  ('";
				$query .= implode("','",$line);
				$query .= "')";
				//echo $query;
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));				
			}//end for i			
			fclose($handle);
			return true;
		}//end function
		/*
		private function add_column(&$field){
			$field = ":".$field;
			return($field);
		}//end function
		*/
		#################################################################
		# LIST OF COUNTRIES PART										#		
		#################################################################
		
		public static function get_countries() {
			return array(	
				'AF' => 'Afghanistan',
				'AX' => 'Åland Islands',
				'AL' => 'Albania',
				'DZ' => 'Algeria',
				'AS' => 'American Samoa',
				'AD' => 'Andorra',
				'AO' => 'Angola',
				'AI' => 'Anguilla',
				'AQ' => 'Antarctica',
				'AG' => 'Antigua and Barbuda',
				'AR' => 'Argentina',
				'AM' => 'Armenia',
				'AW' => 'Aruba',
				'AU' => 'Australia',
				'AT' => 'Austria',
				'AZ' => 'Azerbaijan',
				'BS' => 'Bahamas',
				'BH' => 'Bahrain',
				'BD' => 'Bangladesh',
				'BB' => 'Barbados',
				'BY' => 'Belarus',
				'BE' => 'Belgium',
				'BZ' => 'Belize',
				'BJ' => 'Benin',
				'BM' => 'Bermuda',
				'BT' => 'Bhutan',
				'BO' => 'Bolivia',
				'BA' => 'Bosnia and Herzegovina',
				'BW' => 'Botswana',
				'BV' => 'Bouvet Island',
				'BR' => 'Brazil',
				'IO' => 'British Indian Ocean Territory',
				'BN' => 'Brunei Darussalam',
				'BG' => 'Bulgaria',
				'BF' => 'Burkina Faso',
				'BI' => 'Burundi',
				'KH' => 'Cambodia',
				'CM' => 'Cameroon',
				'CA' => 'Canada',
				'CV' => 'Cape Verde',
				'KY' => 'Cayman Islands',
				'CF' => 'Central African Republic',
				'TD' => 'Chad',
				'CL' => 'Chile',
				'CN' => 'China',
				'CX' => 'Christmas Island',
				'CC' => 'Cocos (Keeling) Islands',
				'CO' => 'Colombia',
				'KM' => 'Comoros',
				'CG' => 'Congo',
				'CD' => 'Congo, the Democratic Republic of the',
				'CK' => 'Cook Islands',
				'CR' => 'Costa Rica',
				'HR' => 'Croatia',
				'CU' => 'Cuba',
				'CY' => 'Cyprus',
				'CZ' => 'Czech Republic',
				'CI' => 'Côte d\'Ivoire',
				'DK' => 'Denmark',
				'DJ' => 'Djibouti',
				'DM' => 'Dominica',
				'DO' => 'Dominican Republic',
				'EC' => 'Ecuador',
				'EG' => 'Egypt',
				'SV' => 'El Salvador',
				'GQ' => 'Equatorial Guinea',
				'ER' => 'Eritrea',
				'EE' => 'Estonia',
				'ET' => 'Ethiopia',
				'FK' => 'Falkland Islands (Malvinas)',
				'FO' => 'Faroe Islands',
				'FJ' => 'Fiji',
				'FI' => 'Finland',
				'FR' => 'France',
				'GF' => 'French Guiana',
				'PF' => 'French Polynesia',
				'TF' => 'French Southern Territories',
				'GA' => 'Gabon',
				'GM' => 'Gambia',
				'GE' => 'Georgia',
				'DE' => 'Germany',
				'GH' => 'Ghana',
				'GI' => 'Gibraltar',
				'GR' => 'Greece',
				'GL' => 'Greenland',
				'GD' => 'Grenada',
				'GP' => 'Guadeloupe',
				'GU' => 'Guam',
				'GT' => 'Guatemala',
				'GG' => 'Guernsey',
				'GN' => 'Guinea',
				'GW' => 'Guinea-Bissau',
				'GY' => 'Guyana',
				'HT' => 'Haiti',
				'HM' => 'Heard Island and McDonald Islands',
				'VA' => 'Holy See (Vatican City State)',
				'HN' => 'Honduras',
				'HK' => 'Hong Kong',
				'HU' => 'Hungary',
				'IS' => 'Iceland',
				'IN' => 'India',
				'ID' => 'Indonesia',
				'IR' => 'Iran, Islamic Republic of',
				'IQ' => 'Iraq',	
				'IE' => 'Ireland',	
				'IM' => 'Isle of Man',
				'IL' => 'Israel',
				'IT' => 'Italy',
				'JM' => 'Jamaica',
				'JP' => 'Japan',
				'JE' => 'Jersey',
				'JO' => 'Jordan',
				'KZ' => 'Kazakhstan',
				'KE' => 'Kenya',
				'KI' => 'Kiribati',
				'KP' => 'Korea, Democratic People\'s Republic of',
				'KR' => 'Korea, Republic of',
				'KW' => 'Kuwait',
				'KG' => 'Kyrgyzstan',
				'LA' => 'Lao People\'s Democratic Republic',
				'LV' => 'Latvia',
				'LB' => 'Lebanon',
				'LS' => 'Lesotho',
				'LR' => 'Liberia',
				'LY' => 'Libyan Arab Jamahiriya',
				'LI' => 'Liechtenstein',
				'LT' => 'Lithuania',
				'LU' => 'Luxembourg',
				'MO' => 'Macao',
				'MK' => 'Macedonia, the former Yugoslav Republic of',
				'MG' => 'Madagascar',
				'MW' => 'Malawi',
				'MY' => 'Malaysia',
				'MV' => 'Maldives',
				'ML' => 'Mali',
				'MT' => 'Malta',
				'MH' => 'Marshall Islands',
				'MQ' => 'Martinique',
				'MR' => 'Mauritania',
				'MU' => 'Mauritius',
				'YT' => 'Mayotte',
				'MX' => 'Mexico',
				'FM' => 'Micronesia, Federated States of',
				'MD' => 'Moldova, Republic of',
				'MC' => 'Monaco',
				'MN' => 'Mongolia',
				'ME' => 'Montenegro',
				'MS' => 'Montserrat',
				'MA' => 'Morocco',
				'MZ' => 'Mozambique',
				'MM' => 'Myanmar',
				'NA' => 'Namibia',
				'NR' => 'Nauru',
				'NP' => 'Nepal',
				'NL' => 'Netherlands',
				'AN' => 'Netherlands Antilles',
				'NC' => 'New Caledonia',
				'NZ' => 'New Zealand',
				'NI' => 'Nicaragua',
				'NE' => 'Niger',
				'NG' => 'Nigeria',
				'NU' => 'Niue',
				'NF' => 'Norfolk Island',
				'MP' => 'Northern Mariana Islands',
				'NO' => 'Norway',
				'OM' => 'Oman',
				'PK' => 'Pakistan',
				'PW' => 'Palau',
				'PS' => 'Palestinian Territory, Occupied',
				'PA' => 'Panama',
				'PG' => 'Papua New Guinea',
				'PY' => 'Paraguay',
				'PE' => 'Peru',
				'PH' => 'Philippines',
				'PN' => 'Pitcairn',
				'PL' => 'Poland',
				'PT' => 'Portugal',
				'PR' => 'Puerto Rico',
				'QA' => 'Qatar',
				'RE' => 'Reunion ﻿Réunion',
				'RO' => 'Romania',
				'RU' => 'Russian Federation',
				'RW' => 'Rwanda',
				'BL' => 'Saint Barthélemy',
				'SH' => 'Saint Helena',
				'KN' => 'Saint Kitts and Nevis',
				'LC' => 'Saint Lucia',
				'MF' => 'Saint Martin (French part)',
				'PM' => 'Saint Pierre and Miquelon',
				'VC' => 'Saint Vincent and the Grenadines',
				'WS' => 'Samoa',
				'SM' => 'San Marino',
				'ST' => 'Sao Tome and Principe',
				'SA' => 'Saudi Arabia',
				'SN' => 'Senegal',
				'RS' => 'Serbia',
				'SC' => 'Seychelles',
				'SL' => 'Sierra Leone',
				'SG' => 'Singapore',
				'SK' => 'Slovakia',
				'SI' => 'Slovenia',
				'SB' => 'Solomon Islands',
				'SO' => 'Somalia',
				'ZA' => 'South Africa',
				'GS' => 'South Georgia and the South Sandwich Islands',
				'ES' => 'Spain',
				'LK' => 'Sri Lanka',
				'SD' => 'Sudan',
				'SR' => 'Suriname',
				'SJ' => 'Svalbard and Jan Mayen',
				'SZ' => 'Swaziland',
				'SE' => 'Sweden',
				'CH' => 'Switzerland',
				'SY' => 'Syrian Arab Republic',
				'TW' => 'Taiwan',
				'TJ' => 'Tajikistan',
				'TZ' => 'Tanzania, United Republic of',
				'TH' => 'Thailand',
				'TL' => 'Timor-Leste',
				'TG' => 'Togo',
				'TK' => 'Tokelau',
				'TO' => 'Tonga',
				'TT' => 'Trinidad and Tobago',
				'TN' => 'Tunisia',
				'TR' => 'Turkey',
				'TM' => 'Turkmenistan',
				'TC' => 'Turks and Caicos Islands',
				'TV' => 'Tuvalu',
				'UG' => 'Uganda',
				'UA' => 'Ukraine',
				'AE' => 'United Arab Emirates',	
				'GB' => 'United Kingdom',							
				'US' => 'United States',
				'UM' => 'United States Minor Outlying Islands',
				'UY' => 'Uruguay',
				'UZ' => 'Uzbekistan',
				'VU' => 'Vanuatu',
				'VE' => 'Venezuela, Bolivarian Republic of',
				'VN' => 'Viet Nam',
				'VG' => 'Virgin Islands, British',
				'VI' => 'Virgin Islands, U.S.',
				'WF' => 'Wallis and Futuna',
				'EH' => 'Western Sahara',
				'YE' => 'Yemen',
				'ZM' => 'Zambia',
				'ZW' => 'Zimbabwe'
			);
        }//end function
		
        public static function get_country($code) {
			$countries = self::get_countries();
			return $countries[$code];
        }//end function		
		
		public static function get_months(){
			$months = array();
			$months[0] = "January";
			$months[1] = "February";
			$months[2] = "March";
			$months[3] = "April";
			$months[4] = "May";
			$months[5] = "June";
			$months[6] = "July";
			$months[7] = "August";
			$months[8] = "September";
			$months[9] = "October";
			$months[10] = "November";
			$months[11] = "December";
			return $months;			 
		}//end function
		
		public static function file_get_php_classes($filepath) {
			$php_code = file_get_contents($filepath);			
			$classes = tools::get_php_classes($php_code);
			return $classes;
		}//end function
		
		public static function get_php_classes($php_code) {
			$classes = array();
			$tokens = token_get_all($php_code);
			$count = count($tokens);
			for ($i = 2; $i < $count; $i++) {
				if (   $tokens[$i - 2][0] == T_CLASS
					&& $tokens[$i - 1][0] == T_WHITESPACE
					&& $tokens[$i][0] == T_STRING) {
					
					$class_name = $tokens[$i][1];
					$classes[] = $class_name;
				}//end if
			}//end for i
			return $classes;
		}//end function
	}//end class
?>