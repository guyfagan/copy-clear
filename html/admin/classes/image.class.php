<?php
	#################################################################
	# Image Class - 	last update 14/12/12						#
	#					created in 13/12/12 						#
	# This is a core class for the Old Hat CMS						#	
	#																#
	#################################################################

	class image{
		
		public $utils;
		public $src_file;
		public $format;
		public $photo;
		public $image_quality;
		public $upload_path;
		
		//Constructor
		function __construct($utils,$params = array()){		
			$this->utils = $utils;	
			$this->photo = array();
			$this->image_quality = 100;
			$this->upload_path = __UPLOADPATH__;
		}//endconstructor
		
		public function set_crop($array){
			$this->photo['crop'] = array();
			if(!isset($array['scale'])){
				$array['scale'] = 1;	
			}//end if
			$this->photo['crop']['x1'] = $array['x1']*$array['scale'];
			$this->photo['crop']['y1'] = $array['y1']*$array['scale'];
			$this->photo['crop']['x2'] = $array['x2']*$array['scale'];
			$this->photo['crop']['y2'] = $array['y2']*$array['scale'];
			$this->photo['crop']['width'] = $array['width']*$array['scale'];
			$this->photo['crop']['height'] = $array['height']*$array['scale'];
			
		}//end function
		
		public function create_new_size(){	
			//check if imagetypes exists in the php configuration
			if (!function_exists("imagetypes")) {	
				return false;
			}//endif	
				
			$params = func_get_args();			
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($attachment_post_type)){
				$attachment_post_type = 'post';	
			}//end if
			
			//if it's passed, we override the upload path
			if(isset($upload_path)){
				$this->upload_path = $upload_path;	
			}//end if
			
			if(!isset($file_id) && !isset($format_id) && !isset($post_id)){
				return false;	
			} else {
				//we retrieve all the info about the attachment
				$src_data = $this->utils->get_attachment(array("post_id" => $post_id,"file_id" => $file_id,'attachment_post_type' => $attachment_post_type));	
				if($src_data === false){
					return false;	
				}//end if
				//store the info of the src file in the photo array
				$this->photo['src'] = $src_data;
				$this->photo['new'] = array(); //this is the array where we store the info for the new image
				//now we retrieve all the info about this format
				$this->format = $this->utils->get_attachment_format(array("id" => $format_id));
				if($this->format === false){
					return false;	
				}//end if
				//get the format folder
				$dest_folder = $this->format['attachment_setting_folder'];
				$dest_path = $this->upload_path."photos/".$dest_folder."/";
				//src file
				//in case the source file is not defined we just fallback to the default one, which is supposed to be in the o (Original) folder
				if(is_null($this->src_file)){
					$this->src_file = $this->upload_path."photos/o/".$this->photo['src']['attachment_filename'];
				}//end if
				//get the size of the original image
				if(!isset($this->photo['crop'])){
					$image_size = getimagesize($this->src_file);
					$this->photo['src']['width'] = $image_size[0];
					$this->photo['src']['height'] = $image_size[1];
				} else {
					$this->photo['src']['width'] = $this->photo['crop']['width'];
					$this->photo['src']['height'] = $this->photo['crop']['height'];
				}
				//destination file
				$dest_file = $dest_path.$this->photo['src']['attachment_filename'];
				$this->photo['new']['path'] = $dest_path;
				$this->photo['new']['filename'] = $this->photo['src']['attachment_filename'];				
				//get the file extension
				$image_ext = substr(strtolower($this->photo['src']['attachment_filename']),-3);
				$this->photo['ext'] = $image_ext;
				//get the width and height from the new format settings
				$new_width = (int)$this->format['attachment_setting_width'];
				$new_height = (int)$this->format['attachment_setting_height'];
				$this->photo['image_processing'] = strtolower($this->format['attachment_setting_processing']);
				//get the aspect ratio for x and y values
				$this->photo['x_ratio'] = $this->photo['src']['width']/$this->photo['src']['height'];
				$this->photo['y_ratio'] = $this->photo['src']['height']/$this->photo['src']['width'];
				//now we check which image processing we use			
				if($this->photo['image_processing'] == "scale"){
					//scale
					if(!isset($this->photo['crop'])){
						$this->photo['new']['start_x'] = 0;
						$this->photo['new']['start_y'] = 0;
					} else {
						$this->photo['new']['start_x'] = $this->photo['crop']['x1'];;
						$this->photo['new']['start_y'] = $this->photo['crop']['y1'];;
					}
					//in case one of the sizes is equal to 0, we execute this code	
					if($new_width == 0){
						//we scale the width in proportion of the height
						$this->photo['new']['width'] = floor($new_height*$this->photo['x_ratio']);
						$this->photo['new']['height'] = $new_height;
					} else {
						//we scale the height in proportion of the width
						$this->photo['new']['width'] = $new_width;
						$this->photo['new']['height'] = floor($new_width*$this->photo['y_ratio']);
					}//end if
				} else {
					//crop	
					if(!isset($this->photo['crop'])){
						$this->photo['new']['start_x'] = 0;
						$this->photo['new']['start_y'] = 0;
						if($this->photo['src']['width'] > $this->photo['src']['height']){
							$this->photo['new']['start_x'] = ceil(($this->photo['src']['width'] - $this->photo['src']['height'])/2); 
						} else if($this->photo['src']['width'] < $this->photo['src']['height']){
							$this->photo['new']['start_y'] = ceil(($this->photo['src']['height'] - $this->photo['src']['width'])/2);
						}//end if
					} else {
						$this->photo['new']['start_x'] = $this->photo['crop']['x1'];;
						$this->photo['new']['start_y'] = $this->photo['crop']['y1'];;
					}//end if
					
					
					$orig_size_ratio = $this->photo['src']['width']/$this->photo['src']['height'];
					$new_size_ratio = $new_width/$new_height;			
					
					if(!isset($this->photo['crop'])){
						//the code below works mostly by accident, don't ask me why it works, but it works
						if($new_size_ratio > $orig_size_ratio){
							//the new pictures is larger than the original one
							$orig_new_height = $this->photo['src']['width']/$new_size_ratio; 
							$this->photo['new']['start_x'] = 0;						
							$this->photo['new']['start_y'] = -(ceil(($orig_new_height-$this->photo['src']['height'])/2));					
							$this->photo['src']['height'] = $orig_new_height;
						} else if($new_size_ratio <= $orig_size_ratio){
							//the new picture is heigher than the origina one
							$orig_new_width = $this->photo['src']['height']*$new_size_ratio; 
							$this->photo['new']['start_y'] = 0;
							$this->photo['new']['start_x'] = -(ceil(($orig_new_width-$this->photo['src']['width'])/2));
							$this->photo['src']['width'] = $orig_new_width;
						}//end if						
					}//end if				
					
					if($new_width > $new_height){
						//it's landscape
						$this->photo['new']['type'] = 'landscape';						
					} else if($new_width < $new_height){
						//it's portrait					
						$this->photo['new']['type'] = 'portrait';
					} else {
						//it's a square	
						$this->photo['new']['type'] = 'square';						
						$this->photo['src']['width'] = $this->photo['src']['height'];
					}//end if
					$this->photo['new']['width'] = $new_width;
					$this->photo['new']['height'] = $new_height;	
				}//end if
			
				//after we collected all the info, we create the new image
				$result = $this->create_image();
				if($result !== false){
					return $this->photo;
				} else {
					return false;	
				}//end if
			
			}//end if
		}//end function
		
		public function check_if_resize_image_needed(){
			$params = func_get_args();			
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($file_id)){
				return false;	
			}//end if
			
			//if it's passed, we override the upload path
			if(isset($upload_path)){
				$this->upload_path = $upload_path;	
			}//end if
						
			$file_data = $this->utils->get_attachment(array('file_id' => $file_id));
			if($file_data !== false){		
				//we check if the current file is an image
				if($file_data['attachment_type'] == "image"){
					$filename = $file_data['attachment_filename'];
					//we get the media settings
					$media_settings = $this->utils->get_settings(array('module' => 'media'));				
					if($media_settings !== false){						
						if((bool)$media_settings['resizeLargeImages'] && is_numeric($media_settings['resizeImagesWidth']) && is_numeric($media_settings['resizeImagesHeight']) && isset($media_settings['resizeImagesFormat'])){						
							//we have to be sure that the file exists at this stage
							//echo $this->upload_path.$filename;
							if(file_exists($this->upload_path."photos/o/".$filename)){								
								//we get the image size
								$image_size = getimagesize($this->upload_path."photos/o/".$filename);
								if(is_array($image_size)){
									if($image_size[0] > $media_settings['resizeImagesWidth'] || $image_size[1] > $media_settings['resizeImagesHeight']){									
										$params = array('format_id' => $media_settings['resizeImagesFormat'], 'filename' => $filename);
										$this->resize_original_image($params);										
										return true;
									} else {
										return false;	
									}//end if
								} else {
									return false;
								}//end if
							} else {
								return false;
							}//end if
						} else {
							return false;
						}//end if
					} else {
						return false;	
					}//end if
				} else {
					return false;
				}//end if	
			} else {
				return false;	
			}//end if
		}//end function
		
		public function resize_original_image(){	
			//check if imagetypes exists in the php configuration
			if (!function_exists("imagetypes")) {	
				return false;
			}//endif	
				
			$params = func_get_args();			
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			//if it's passed, we override the upload path
			if(isset($upload_path)){
				$this->upload_path = $upload_path;	
			}//end if
			
			if(!isset($filename) && !isset($format_id)){
				return false;	
			} else {				
				//store the info of the src file in the photo array
				$this->photo['src'] = array('attachment_filename' => $filename);
				$this->photo['new'] = array(); //this is the array where we store the info for the new image
				//now we retrieve all the info about this format
				$this->format = $this->utils->get_attachment_format(array("id" => $format_id));
				if($this->format === false){
					return false;	
				}//end if
				//get the format folder
				$dest_folder = $this->format['attachment_setting_folder'];
				$dest_path = $this->upload_path."photos/o/";
				//src file
				//in case the source file is not defined we just fallback to the default one, which is supposed to be in the o (Original) folder
				if(is_null($this->src_file)){
					rename($this->upload_path."photos/o/".$this->photo['src']['attachment_filename'],$this->upload_path."photos/o/old.".$this->photo['src']['attachment_filename']);
					$this->src_file = $this->upload_path."photos/o/old.".$this->photo['src']['attachment_filename'];
				}//end if
				
				//get the size of the original image
				if(!isset($this->photo['crop'])){
					$image_size = getimagesize($this->src_file);
					$this->photo['src']['width'] = $image_size[0];
					$this->photo['src']['height'] = $image_size[1];
				} else {
					$this->photo['src']['width'] = $this->photo['crop']['width'];
					$this->photo['src']['height'] = $this->photo['crop']['height'];
				}
				//destination file
				$dest_file = $dest_path.$this->photo['src']['attachment_filename'];
				$this->photo['new']['path'] = $dest_path;
				$this->photo['new']['filename'] = $this->photo['src']['attachment_filename'];				
				//get the file extension
				$image_ext = substr(strtolower($this->photo['src']['attachment_filename']),-3);
				$this->photo['ext'] = $image_ext;
				//get the width and height from the new format settings
				$new_width = (int)$this->format['attachment_setting_width'];
				$new_height = (int)$this->format['attachment_setting_height'];
				$this->photo['image_processing'] = strtolower($this->format['attachment_setting_processing']);
				//get the aspect ratio for x and y values
				$this->photo['x_ratio'] = $this->photo['src']['width']/$this->photo['src']['height'];
				$this->photo['y_ratio'] = $this->photo['src']['height']/$this->photo['src']['width'];
				//now we check which image processing we use			
				if($this->photo['image_processing'] == "scale"){
					//scale
					if(!isset($this->photo['crop'])){
						$this->photo['new']['start_x'] = 0;
						$this->photo['new']['start_y'] = 0;
					} else {
						$this->photo['new']['start_x'] = $this->photo['crop']['x1'];;
						$this->photo['new']['start_y'] = $this->photo['crop']['y1'];;
					}
					//in case one of the sizes is equal to 0, we execute this code	
					if($new_width == 0){
						//we scale the width in proportion of the height
						$this->photo['new']['width'] = floor($new_height*$this->photo['x_ratio']);
						$this->photo['new']['height'] = $new_height;
					} else {
						//we scale the height in proportion of the width
						$this->photo['new']['width'] = $new_width;
						$this->photo['new']['height'] = floor($new_width*$this->photo['y_ratio']);
					}//end if
				} else {
					//crop	
					if(!isset($this->photo['crop'])){
						$this->photo['new']['start_x'] = 0;
						$this->photo['new']['start_y'] = 0;
						if($this->photo['src']['width'] > $this->photo['src']['height']){
							$this->photo['new']['start_x'] = ceil(($this->photo['src']['width'] - $this->photo['src']['height'])/2); 
						} else if($this->photo['src']['width'] < $this->photo['src']['height']){
							$this->photo['new']['start_y'] = ceil(($this->photo['src']['height'] - $this->photo['src']['width'])/2);
						}//end if
					} else {
						$this->photo['new']['start_x'] = $this->photo['crop']['x1'];;
						$this->photo['new']['start_y'] = $this->photo['crop']['y1'];;
					}//end if
					
					
					$orig_size_ratio = $this->photo['src']['width']/$this->photo['src']['height'];
					$new_size_ratio = $new_width/$new_height;			
					
					if(!isset($this->photo['crop'])){
						//the code below works mostly by accident, don't ask me why it works, but it works
						if($new_size_ratio > $orig_size_ratio){
							//the new pictures is larger than the original one
							$orig_new_height = $this->photo['src']['width']/$new_size_ratio; 
							$this->photo['new']['start_x'] = 0;						
							$this->photo['new']['start_y'] = -(ceil(($orig_new_height-$this->photo['src']['height'])/2));					
							$this->photo['src']['height'] = $orig_new_height;
						} else if($new_size_ratio <= $orig_size_ratio){
							//the new picture is heigher than the origina one
							$orig_new_width = $this->photo['src']['height']*$new_size_ratio; 
							$this->photo['new']['start_y'] = 0;
							$this->photo['new']['start_x'] = -(ceil(($orig_new_width-$this->photo['src']['width'])/2));
							$this->photo['src']['width'] = $orig_new_width;
						}//end if						
					}//end if				
					
					if($new_width > $new_height){
						//it's landscape
						$this->photo['new']['type'] = 'landscape';						
					} else if($new_width < $new_height){
						//it's portrait					
						$this->photo['new']['type'] = 'portrait';
					} else {
						//it's a square	
						$this->photo['new']['type'] = 'square';						
						$this->photo['src']['width'] = $this->photo['src']['height'];
					}//end if
					$this->photo['new']['width'] = $new_width;
					$this->photo['new']['height'] = $new_height;	
				}//end if
			
				//after we collected all the info, we create the new image
				$result = $this->create_image();
				if($result !== false){
					//remote the old photo
					unlink($this->upload_path."photos/o/old.".$this->photo['src']['attachment_filename']);
					return $this->photo;
				} else {
					return false;	
				}//end if
			
			}//end if
		}//end function
		
		protected function create_image(){
			switch($this->photo['ext']){
				default:
				case "jpg":
					$result = $this->create_jpg();
					break;
				case "gif":
					$result = $this->create_gif();
					break;
				case "png":
					$result = $this->create_png();
					break;
			}//endswitch	
				
			imagedestroy($this->photo['src_img_obj']);
			$this->photo['src_img_obj'] = NULL;
			imagedestroy($this->photo['new_img_obj']);
			$this->photo['new_img_obj'] = NULL;
			
			return $result;
		}//end function
		
		protected function create_jpg(){
			//create the jpg
			$this->photo['src_img_obj'] = @imagecreatefromjpeg($this->src_file);
			//create
			$this->photo['new_img_obj'] = @imagecreatetruecolor($this->photo['new']['width'],$this->photo['new']['height']);
			//copy the original image in the new image
			imagecopyresampled(
				$this->photo['new_img_obj'],
				$this->photo['src_img_obj'],
				0,//destination starting x
				0,//destination starting y
				$this->photo['new']['start_x'],//src starting x
				$this->photo['new']['start_y'],//src starting y
				$this->photo['new']['width'],
				$this->photo['new']['height'],
				$this->photo['src']['width'],
				$this->photo['src']['height']
				);
			//now save the new physical image		
			if(imagejpeg($this->photo['new_img_obj'],$this->photo['new']['path'].$this->photo['new']['filename'],$this->image_quality)){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function create_gif(){
			//create the jpg
			$this->photo['src_img_obj'] = @imagecreatefromgif($this->src_file);
			//create
			$this->photo['new_img_obj'] = @imagecreatetruecolor($this->photo['new']['width'],$this->photo['new']['height']);
			//preserve transparency
			imagecolortransparent($this->photo['new_img_obj'], imagecolorallocatealpha($this->photo['new_img_obj'], 0, 0, 0, 127));
			imagealphablending($this->photo['new_img_obj'], false);
			imagesavealpha($this->photo['new_img_obj'], true);	
			//copy the original image in the new image
			imagecopyresampled(
				$this->photo['new_img_obj'],
				$this->photo['src_img_obj'],
				0,//destination starting x
				0,//destination starting y
				$this->photo['new']['start_x'],//src starting x
				$this->photo['new']['start_y'],//src starting y
				$this->photo['new']['width'],
				$this->photo['new']['height'],
				$this->photo['src']['width'],
				$this->photo['src']['height']
				);
			//now save the new physical image		
			if(imagegif($this->photo['new_img_obj'],$this->photo['new']['path'].$this->photo['new']['filename'])){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function create_png(){
		//create the jpg
			$this->photo['src_img_obj'] = @imagecreatefrompng($this->src_file);
			if($this->photo['src_img_obj'] === false){
				return false;	
			}
			//create
			$this->photo['new_img_obj'] = @imagecreatetruecolor($this->photo['new']['width'],$this->photo['new']['height']);
			if($this->photo['new_img_obj'] === false){
				return false;	
			}//end if
			//preserve transparency
			imagecolortransparent($this->photo['new_img_obj'], imagecolorallocatealpha($this->photo['new_img_obj'], 0, 0, 0, 127));
			imagealphablending($this->photo['new_img_obj'], false);
			imagesavealpha($this->photo['new_img_obj'], true);
			//copy the original image in the new image
			imagecopyresampled(
				$this->photo['new_img_obj'],
				$this->photo['src_img_obj'],
				0,//destination starting x
				0,//destination starting y
				$this->photo['new']['start_x'],//src starting x
				$this->photo['new']['start_y'],//src starting y
				$this->photo['new']['width'],
				$this->photo['new']['height'],
				$this->photo['src']['width'],
				$this->photo['src']['height']
				);
			//now save the new physical image		
			if(imagepng($this->photo['new_img_obj'],$this->photo['new']['path'].$this->photo['new']['filename'])){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		#This function is ported from the old cms, needs to be converted ASAP
		public function rotate_image($image_file,$degrees){
			$this->image_file = $image_file;
			
			$image_ext = substr(strtolower($this->image_file),-3);		
			switch($image_ext){
				case "jpg":
					$source = imagecreatefromjpeg($image_file);
					$rotate = imagerotate($source, $degrees, 0);
					imagejpeg($rotate,$this->image_file,$this->image_quality);
					imagedestroy($source);
					break;
				case "gif":
					$source = imagecreatefromgif($image_file);
					$rotate = imagerotate($source, $degrees, 0);
					imagegif($rotate,$this->image_file);
					imagedestroy($source);
					break;
				case "png":
					$source = imagecreatefrompng($image_file);
					$rotate = imagerotate($source, $degrees, 0);
					imagepng($rotate,$this->image_file);
					imagedestroy($source);
					break;
				default:
					$source = imagecreatefromjpeg($image_file);
					$rotate = imagerotate($source, $degrees, 0);
					imagejpeg($rotate,$this->image_file,$this->image_quality);
					imagedestroy($source);
			}//endswitch
			return true;
		}//end function
	}//end class
?>