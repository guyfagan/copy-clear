<?php
	class messages{

		public $message_id;
		protected $utils;//utils class
		public $errors = array();
		public $settings = array();
		protected $meta;

		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->utils->read_params($this,$params);
			//Meta init
			$this->meta = $this->utils->call("meta");
			//Set basic settings
			$this->init_settings();
		}//endconstructor

		public function get_messages(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			try{
				$query = "SELECT *, UNIX_TIMESTAMP(message_date) AS message_date
						  FROM messages
						  WHERE message_privacy IS NOT NULL";
				if(isset($from)){
					$from = (int)$from;
					$query .= " AND message_sender_uid = :from";
				}//end if
				if(isset($to)){
					$to = (int)$to;
					$query .= " AND message_receiver_uid = :to";
				}//end if
				if(isset($ref_id)){
					$query .= " AND message_ref_id = :ref_id";
				}//end if
				if(isset($module_id)){
					if(!is_numeric($module_id)){
						$module_data = $this->utils->get_module($module_id);
						if($module_data === false){
							throw new PDOException("The module ID passed is invalid");
						} else {
							$module_id = $module_data['module_id'];
						}//end if
					}//end if
					$query .= " AND message_module_id = :module_id";
				}//end if
				if(isset($privacy)){
					$query .= " AND message_privacy = :privacy";
				}//end if
				if(isset($status)){
					$query .= " AND message_status = :status";
				}//end if
				if(isset($type)){
					$query .= " AND message_type = :type";
				}//end if
				$query .= " ORDER BY message_date DESC";

				//This is for the paging
				if($this->utils->paging_isset()){
					$this->utils->set_unpaged_query($query,false);
					$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
				}//end if

				if(isset($limit)){
					$query .= " LIMIT :limit";
				}//end if


				$result = $this->utils->db->prepare($query);
				if(isset($from)){
					$result->bindParam(':from', $from);
				}//end if
				if(isset($to)){
					$result->bindParam(':to', $to);
				}//end if
				if(isset($ref_id)){
					$result->bindParam(':ref_id', $ref_id);
				}//end if
				if(isset($module_id)){
					$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
				}//end if
				if(isset($privacy)){
					$result->bindParam(':privacy', $privacy, PDO::PARAM_STR);
				}//end if
				if(isset($type)){
					$result->bindParam(':type', $type, PDO::PARAM_STR);
				}//end if
				if(isset($status)){
					$result->bindParam(':status', $status, PDO::PARAM_STR);
				}//end if
				if(isset($limit)){
					$result->bindParam(':limit', $limit, PDO::PARAM_INT);
				}//end if
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$num = $result->rowCount();
					if($num > 0){
						$data = $this->utils->get_result_array($result,false);
						if($data !== false){
							$users = $this->utils->call('users');
							for($i = 0; $i < sizeof($data); $i++){
								$sender_data = $users->get_user(array('id' => $data[$i]['message_sender_uid']));
								if($sender_data !== false){
									$data[$i]['sender_username'] = $sender_data['user_username'];
									$data[$i]['sender_firstname'] = $sender_data['user_firstname'];
									$data[$i]['sender_surname'] = $sender_data['user_surname'];
									$data[$i]['sender_email'] = $sender_data['user_email'];
									$data[$i]['sender_group_id'] = $sender_data['group_id'];
									$data[$i]['sender_is_admin'] = $sender_data['users_group_admin'];
								}//end if
								$receiver_data = $users->get_user(array('id' => $data[$i]['message_receiver_uid']));
								if($receiver_data !== false){
									$data[$i]['receiver_username'] = $receiver['user_username'];
									$data[$i]['receiver_firstname'] = $receiver['user_firstname'];
									$data[$i]['receiver_surname'] = $receiver['user_surname'];
									$data[$i]['receiver_email'] = $receiver['user_email'];
									$data[$i]['receiver_group_id'] = $receiver['group_id'];
									$data[$i]['receiver_is_admin'] = $receiver['users_group_admin'];
								}//end if
							}//end for i
						}//end if
						return $data;
					} else {
						return false;
					}//end if
				} else {
					throw new PDOException("Cannot read the messages");
				}//end if
			} catch(PDOException $ex){
				array_push($this->errors,$ex->getMessage());
				return false;
			}//end if
		}//end function

		public function get_latest_messages(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if



			$query = "SELECT messages.*, ads_submissions.ad_submission_id,
								ads_submissions.ad_submission_title,
								users.user_firstname, users.user_surname, users.user_email,
								companies.company_name, ads_submissions.ad_submission_status,
								UNIX_TIMESTAMP(message_date) AS message_date
								FROM messages, ads_submissions, users, companies, users_groups_rel
					  		WHERE (messages.message_date >= DATE(ads_submissions.ad_submission_date_viewed) OR ads_submissions.ad_submission_date_viewed IS NULL)
								AND users.user_id = users_groups_rel.users_groups_rel_uid
								AND users_groups_rel.users_groups_rel_gid = 4
							  AND messages.message_ref_id = ads_submissions.ad_submission_id
								AND users.user_company_id = companies.company_id
								AND users.user_id = messages.message_sender_uid";
			if(isset($status)){
				if(is_array($status)){
					$status = implode("','",$status);
					$query .= " AND ad_submission_status IN ('".$status."')";
				} else {
					$query .= " AND ad_submission_status = '".$status."'";
				}//end if
			}//end if

			if(isset($ignore_status)){
				if(is_array($ignore_status)){
					$ignore_status = implode("','",$ignore_status);
					$query .= " AND ad_submission_status NOT IN ('".$ignore_status."')";
				} else {
					$query .= " AND ad_submission_status != '".$ignore_status."'";
				}//end if
			}//end if

			if(isset($viewed)){
				if(is_bool($viewed)){
					if($viewed === true){
						$query .= " AND ad_submission_date_viewed IS NOT NULL";
					} else {
						$query .= " AND ad_submission_date_viewed IS NULL";
					}//end if
				}//end if
			}//end if
			if(isset($days)){
				$query .= " AND (ad_submission_date_replied >= CURDATE() - INTERVAL ".$days." DAY)
							AND (message_date >= CURDATE() - INTERVAL ".$days." DAY)";
			}//end if
			$query .= " GROUP BY ad_submission_id
								ORDER BY ad_submission_date_replied ".$sort_order;
								//echo $query;

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if
			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if

		}// end function

		public function get_message(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			try{
				if(!isset($id)){
					throw new PDOException("Cannot read the message, invalid ID");
				}//end if

				$query = "SELECT * FROM messages
						  WHERE message_id = :id";
				if(isset($from)){
					$query .= " AND message_sender_uid = :from";
				}//end if
				if(isset($to)){
					$query .= " AND message_receiver_uid = :to";
				}//end if
				if(isset($ref_id)){
					$query .= " AND message_ref_id = :ref_id";
				}//end if
				if(isset($module_id)){
					if(!is_numeric($module_id)){
						$module_data = $this->utils->get_module($module_id);
						if($module_data === false){
							throw new PDOException("The module ID passed is invalid");
						} else {
							$module_id = $module_data['module_id'];
						}//end if
					}//end if
					$query .= " AND message_module_id = :module_id";
				}//end if
				if(isset($privacy)){
					$query .= " AND message_privacy = :privacy";
				}//end if
				if(isset($status)){
					$query .= " AND message_status = :status";
				}//end if
				if(isset($type)){
					$query .= " AND message_type = :type";
				}//end if
				$query .= " ORDER BY message_date DESC
							LIMIT 1";


				$result = $this->utils->db->prepare($query);
				$result->bindParam(':id', $id, PDO::PARAM_INT);
				if(isset($from)){
					$result->bindParam(':from', $from, PDO::PARAM_INT);
				}//end if
				if(isset($to)){
					$result->bindParam(':to', $to, PDO::PARAM_INT);
				}//end if
				if(isset($ref_id)){
					$result->bindParam(':ref_id', $ref_id, PDO::PARAM_INT);
				}//end if
				if(isset($module_id)){
					$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
				}//end if
				if(isset($privacy)){
					$result->bindParam(':privacy', $privacy, PDO::PARAM_STR);
				}//end if
				if(isset($type)){
					$result->bindParam(':type', $type, PDO::PARAM_STR);
				}//end if
				if(isset($status)){
					$result->bindParam(':status', $status, PDO::PARAM_STR);
				}//end if

				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$num = $result->rowCount();
					if($num > 0){
						$data = $this->utils->get_result_array($result,true);
						return $data;
					} else {
						return false;
					}//end if
				} else {
					throw new PDOException("Cannot read the message");
				}//end if
			} catch(PDOException $ex){
				array_push($this->errors,$ex->getMessage());
				return false;
			}//end if
		}//end function

		public function send_message(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($from)){
				$from = $this->utils->get_uid();
				if($from === false){
					array_push($this->errors,"You need to specify who is sending the message");
				}//end if
			}//end if

			if(!isset($to)){
				$to = "0";
			}//end if

			if(!isset($message)){
				array_push($this->errors,"Missing compulsory fields");
				return false;
			}//end if

			if(!isset($subject)){
				$subject = "";
			}//end if

			if(isset($module)){
				if(!is_numeric($module)){
					$module_data = $this->utils->get_module($module);
					$module_id = $module_data['module_id'];
				}//end if
			}//end if

			$fields = array();
			$fields['subject'] = $subject;
			$fields['message'] = $message;
			$fields['sender_uid'] = $from;
			$fields['receiver_uid'] = $to;

			if(isset($status)){
				$fields['status'] = $status;
			}//end if

			if(isset($privacy)){
				$fields['privacy'] = $privacy;
			}//end if

			if(isset($type)){
				$fields['type'] = $type;
			}//end if

			if(isset($ref_id)){
				$fields['ref_id'] = $ref_id;
			}//end if

			if(isset($module_id)){
				$fields['module_id'] = $module_id;
			}//end if

			$sql = $this->utils->build_sql_fields(array("table_prefix" => "message","sql_type" => "INSERT", "method_data" => $fields));
			$query = "INSERT INTO messages ".$sql;
		//	echo $query;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->message_id = $this->utils->db->lastInsertId();
				return true;
			} else {
				return false;
			}//end if
		}//end function

		public function set_message_status(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($message_id) || !isset($status)){
				array_push($this->errors,'No message ID or Status sent');
				return false;
			}//end if

			if($this->check_message_receiver(array('message_id' => $message_id))){
				$query = "UPDATE messages SET message_status = :status WHERE message_id = :message_id";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':status', $status, PDO::PARAM_STR);
				$result->bindParam(':message_id', $message_id, PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					return true;
				} else {
					array_push($this->errors,'You can\'t edit this message');
					return false;
				}
			} else {
				array_push($this->errors,'You can\'t edit this message');
				return false;
			}//end if

		}//end function

		public function check_message_receiver(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($message_id)){
				array_push($this->errors,'No message ID sent');
				return false;
			}//end if

			if(!isset($user_id)){
				$user_id = $this->utils->get_uid();
				if($user_id === false){
					array_push($this->errors,'No user ID sent');
					return false;
				}//end if
			}//end if

			try{
				$query = "SELECT message_receiver_uid FROM messages WHERE message_receiver_uid = :user_id AND message_id = :message_id";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
				$result->bindParam(':message_id', $message_id, PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$num = $result->rowCount();
					if($num > 0){
						return true;
					} else {
						return false;
					}//end if
				} else {
					throw new PDOException("Cannot find the message");
				}//end if
			} catch(PDOException $ex){
				array_push($this->errors,$ex->getMessage());
				return false;
			}//end if
		}//end function

		#################################################################
		# SETTINGS PART													#
		#################################################################

		private function init_settings(){
			//read the settings from the cms
			$this->settings = $this->utils->get_settings(array('module' => $this->module));
			return true;
		}//end function

		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function

		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>
