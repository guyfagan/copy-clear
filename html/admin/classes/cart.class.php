<?php
	require_once('ewrite.class.php');

	class cart extends ewrite{
	
		protected $cartID; //Cart ID
		protected $sid;//session ID
		protected $moneyMaker;
		protected $cartPrefix;
		protected $email_notification_reply;
		protected $domain;
		public $stats;
		public $errors;
		protected $settings;
		protected $cart_storage;
		public $vat;
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);			
			$this->module = "cart";
			$this->utils->read_params($this,$params);			
			$module_data = $this->utils->get_module($this->module);
			$this->set_module($this->module);
			$this->module_id = $module_data['module_id'];
			//Meta init	
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);	
			//Set basic settings
			$this->init_settings();	
			//cart vars
			$this->create_session_id();
			$this->stats = array();
			$this->stats['total'] = 0;
			$this->vat = 0.23;
			$this->domain = "oldhat.ie";
			$this->no_reply_email = "no-reply@oldhat.ie";
			$this->errors = array();
		}//endconstructor
		
		protected function create_session_id(){
			if($this->settings['cart_unique_id_base'] == 'session_id'){
				$this->sid = session_id();
				if($this->utils->get_uid() !== false){
					$this->sid .= '-'.sha1($this->utils->get_uid());
				}//end if
			} else {
				$this->sid = 'cart-'.sha1($this->utils->get_uid());
			}//end if
		}//end function
		
		public function create_cart(){
			//clean old carts
			$this->autoclean_carts();			
			if($this->utils->get_uid() !== false){
				$uid = $this->utils->get_uid();	
			} else {
				$uid = 0;	
			}//end if
			$query = "INSERT INTO carts
					  (cart_session_id,cart_uid, cart_uuid)
					  VALUES
					  ('".$this->sid."',".$uid.", UUID())";
					  
					 // echo $query;
			$result = $this->utils->db->query($query);				
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$this->cartID = $this->utils->db->lastInsertId();	
				$this->create_code();
				if($this->cart_storage == "session"){
					$_SESSION[$this->cartPrefix.'cart_id'] = base64_encode(serialize($this->cartID));
				} else {
					$delay = (int)$this->settings['cart_cookie_expiry'];
					setcookie($this->cartPrefix.'cart_id',base64_encode(serialize($this->cartID)),time()+$delay,"/");
				}
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		protected function autoclean_carts(){
			$query = "DELETE FROM carts WHERE cart_date < CURDATE() - INTERVAL 6 DAY AND cart_paid = 0";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;	
			}//end if	
		}//end function
		
		public function check_cart_consistency(){
			if($this->cart_storage == "session"){
				$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
			} else {
				$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
			}//end if
		
 			if($cartID != NULL){
				$cartID = unserialize(base64_decode($cartID));
				$cartID = (int)$cartID;
				$query = "SELECT * FROM carts WHERE cart_paid = 0 AND cart_id = ".$cartID." AND cart_session_id = '".$this->sid."'";
			
				$result = $this->utils->db->query($query);				
				$errors = $this->utils->error($result,__LINE__,get_class($this));	
				if($errors === false){
					$num = $result->rowCount();	
					if($num > 0){
						return true;
					} else {
						if($this->cart_storage == "session"){
							$_SESSION[$this->cartPrefix.'cart_id'] = NULL;
							unset($_SESSION[$this->cartPrefix.'cart_id']);
						} else {
							setcookie($this->cartPrefix.'cart_id',"",time()-(3600*24),"/");
						}//end if
						return false;	
					}//end if
				} else {
					if($this->cart_storage == "session"){
						$_SESSION[$this->cartPrefix.'cart_id'] = NULL;
						unset($_SESSION[$this->cartPrefix.'cart_id']);
					} else {
						setcookie($this->cartPrefix.'cart_id',"",time()-(3600*24),"/");
					}//end if			
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function 
		
		public function is_cart_empty(){
			$has_items = $this->cart_has_items();
			if($has_items === true){
				return false;	
			} else {
				return true;
			}//end if
		}//end function
		
		protected function cart_has_items(){
			if($this->cart_storage == "session"){
				$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
			} else {
				$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
			}//end if
			if($cartID !== NULL){
				$this->cartID = unserialize(base64_decode($cartID));
				$query = "SELECT COUNT(cart_item_id) AS items_found 
						  FROM carts_items, carts 
						  WHERE cart_item_cart_id = cart_id
						  AND cart_paid = 0
						  AND cart_id = ".$this->cartID;
				$result = $this->utils->db->query($query);				
				$errors = $this->utils->error($result,__LINE__,get_class($this));	
				if($errors === false){
					$num = $result->rowCount();	
					if($num > 0){
						$row = $result->fetch(PDO::FETCH_ASSOC);
						if($row['items_found'] > 0){
							$this->stats['cart_items_num'] = $row['items_found'];
							return true;
						} else {
							return false;
						}//end if
					} else {
						return false;	
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}
		
		public function cart_exists(){
			if($this->cart_storage == "session"){
				$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
			} else {
				$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
			}//end if
			if($cartID !== NULL){
				$this->cartID = unserialize(base64_decode($cartID));
				if(is_numeric($this->cartID)){
					return true;
				} else {
					return false;
				}//end if
			} else {
				$this->create_cart();
				return false;
			}//end if
		}//end function
		
		public function get_cartID(){
			if(is_null($this->cartID)){
				if($this->cart_storage == "session"){
					$this->cartID = unserialize(base64_decode($_SESSION[$this->cartPrefix.'cart_id']));
				} else {
					$this->cartID = unserialize(base64_decode($_COOKIE[$this->cartPrefix.'cart_id']));	
				}//end if	
			}//end if
			return $this->cartID;
		}//end function
		
		private function create_code(){
			if(is_null($this->cartID)){
				return false;	
			}//end if
			$tools = $this->utils->call('tools');
			$chars = 6;
			$this->code = "";
			if($chars > 0){
				for($k = 0; $k < $chars; $k++){
					$this->code .= $tools->get_random_alphanum();
				}//end for
			}//end if	
			
			//Update the payment with the code
			$query = "UPDATE carts SET cart_code = '".$this->code."' WHERE cart_id = ".$this->cartID;
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;	
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_user_carts(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$user_id = $this->utils->get_uid();
			if($user_id === false){
				return false;
			}//end if
			if(!is_array($params[0])){		
				$params[0] = array();	
			}//end if
			$params[0]['user_id'] = $user_id;
			
			return $this->get_carts($params[0]);
			
		}//end function
		
		public function get_carts(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($paid)){
				$paid = 1;	
			}//end if
			
			$query = "SELECT carts.*, UNIX_TIMESTAMP(cart_date) AS cart_date, user_firstname, user_surname, user_email
					  FROM carts
					  LEFT JOIN payments
					  ON payment_id = cart_payment_id
					  LEFT JOIN users
					  ON user_id = cart_uid
					  WHERE cart_paid = $paid";
			if(isset($user_id)){
				$query .= " AND cart_uid = ".$user_id;	
			}//end if
			$query .= " ORDER BY cart_date DESC";
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit != NULL){
				$query .= " LIMIT ".$limit;
			}//end if	
			
			$result = $this->utils->db->query($query);				
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();			
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_cart_details(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($cart_id)){
				if(is_null($this->cartID)){
					return false;	
				}
			} else {
				$this->cartID = $cart_id;	
			}//end if
			
			$query = "SELECT *, UNIX_TIMESTAMP(cart_date) AS cart_date, user_email AS email
			    	  FROM carts 
					  LEFT JOIN payments
					  ON (cart_payment_id = payment_id AND cart_payment_id > 0)
					  LEFT JOIN users
					  ON (cart_uid = user_id)
					  WHERE cart_id = ".$this->cartID." 
					  LIMIT 1";
			$result = $this->utils->db->query($query);				
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();			
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function is_item_in_cart(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($post_id)){
				return false;	
			}//end if
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				if($module_data !== false){
					$module_id = $module_data['module_id'];					
				}//end if
			}//end if
			
			if(!isset($module_id)){
				$module_id = 0;
			}//end if
			
			//get current cartID
			$cart_id = $this->get_cartID();
			
			$query = "SELECT cart_id 
					  FROM carts, carts_items 
					  WHERE cart_id = cart_item_cart_id
					  AND cart_item_post_id = ".$post_id."
					  AND cart_item_module_id = ".$module_id."
					  AND (cart_paid = 1 ";
			if(!is_null($cart_id) && $cart_id !== false){
				$query .= " OR cart_paid = 0 AND cart_id = ".$cart_id;
			}//end if
			$query .= ")";
			$result = $this->utils->db->query($query);				
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();			
				if($num > 0){							
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_cart(){	
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($cart_id)){
				$this->cartID = $cart_id;	
			}//end if
			
			if(isset($paid)){
				if(is_bool($paid)){					
					if($paid === true){
						$paid = "1";	
					} else {
						$paid = "0";
					}//end if
				}//end if
			} else {
				$paid = "0";	
			}//end if
			
			if(!isset($internal)){
				$cart_exists = $this->check_cart_consistency();	
				
				if($cart_exists){
					if($this->cart_storage == "session"){
						$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
					} else {
						$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
					}//end if
					if($cartID != NULL){
						$this->cartID = unserialize(base64_decode($cartID));						
					} else {
						return false;	
					}//end if		
				} else {
					return false;	
				}//end if
			}//end if				
			if(is_numeric($this->cartID)){
				$query = "SELECT * 
						  FROM carts_items, carts, posts
						  WHERE cart_id = cart_item_cart_id
						  AND cart_item_post_id = post_id";
				if(isset($paid)){
					$query .= " AND cart_paid = ".$paid;
				}//end if
				if(isset($module)){
					$module_data = $this->utils->get_module($module);
					$module_name = $module_data['module_name'];
					$query .= " AND post_module_id = '".$module_name."'";
				}//end if
				if(!isset($internal)){
					$query .= " AND cart_session_id = '".$this->sid."'";
				}//end if
				$query .= " AND cart_id = ".$this->cartID;	
				if(isset($user_id)){
					$query .= " AND cart_uid = ".$user_id;
				}//end if
																	 
				$result = $this->utils->db->query($query);				
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$num = $result->rowCount();			
					if($num > 0){							
						$data = $this->utils->get_result_array($result,false);	
						$this->set_cart_figures($data);		
						/* META SUPPORT */	
						if(!isset($html)){
							$html = false;
						}//end if
						//if we pass the value meta as a boolean, we get all the meta into the final data array
						if(isset($meta) && !is_array($meta) && (bool)$meta == true){
							$this->meta->set_meta_type("cart_item");
							//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
							if($data != false){
								for($i = 0; $i < sizeof($data); $i++){
									$meta_data = $this->meta->get_meta(array("id" => $data[$i]['cart_item_id'], "html" => (bool)$html));
									if($meta_data != false){
										$data[$i] = array_merge($data[$i],$meta_data);
									}//end if
								}//end for i
							}//end if
						} else if(isset($meta) && is_array($meta)){
							//if it's an array, we just get the meta we asked for
							$this->meta->set_meta_type("cart_item");
							if($data != false){							
								for($i = 0; $i < sizeof($data); $i++){
									$meta_data = $this->meta->get_meta(array("id" => $data[$i]['cart_item_id'],"search" => $meta, "html" => (bool)$html));								
									if($meta_data != false){
										$data[$i] = array_merge($data[$i],$meta_data);
									}//end if
								}//end for i
							}//end if
						}//end if	
						/* END META SUPPORT */	
						/* ATTACHMENT SUPPORT */
						if(isset($default_photo) && $default_photo === true){
							if($data != false){							
								for($i = 0; $i < sizeof($data); $i++){
									$photo = $this->utils->get_attachments(array('post_id' => $data[$i]['post_id'],
																		   'role' => 'default-image', 
																		   'module_id' => $this->module_id, 
																		   'limit' => 1, 
																		   'status' => 'published',
																		   'type' => 'image'));															   
									if($photo !== false){
										$data[$i]['photo'] = $photo;
									}//end if
								}//end if
							}//end if
						}//end if 								
						return $data;
					} else {
						return false;
					}//endif
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		protected function set_cart_figures($result){			
			if($result !== false && is_array($result)){
				for($i = 0; $i < sizeof($result); $i++){
					$item_vat_perc = $result[$i]['cart_item_vat']/100;
					$item_price = $result[$i]['cart_item_quantity'] * $result[$i]['cart_item_price'];
					$this->stats['vat'] += $item_price*$item_vat_perc;
					$this->stats['total'] += $item_price;
				}//end for i
				return $this->stats;
			} else {
				return false;	
			}//end if
		}//end function 
		
		public function get_cart_figures(){
			return $this->stats;	
		}//end function
		
		public function get_cart_total(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($cart_id)){
				if($this->cart_storage == "session"){
					$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
				} else {
					$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
				}//end if
				
				if($cartID != NULL){
					$this->cartID = unserialize(base64_decode($cartID));				
				} else if(isset($cart_id)){
					$this->cartID = $cart_id;				
				} else {
					return false;
				}//end if
			} else {
				$this->cartID = $cart_id;
			}//end if
			
			if(is_numeric($this->cartID)){
				$query = "SELECT SUM(cart_item_quantity*cart_item_price) AS total, 
						  SUM(cart_item_quantity*cart_item_price*(cart_item_vat/100)) AS vat
						  FROM carts_items 
						  WHERE cart_item_cart_id = ".$this->cartID;					
				$result = $this->utils->db->query($query);				
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$num = $result->rowCount();			
					if($num > 0){							
						$data = $this->utils->get_result_array($result,true);																						
						return $data;						
					} else {
						return false;
					}//endif
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function update_cart(){
			if($this->cart_storage == "session"){
				$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
			} else {
				$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
			}//end if
			if($cartID != NULL){
				$this->cartID = unserialize(base64_decode($cartID));
				if(is_numeric($this->cartID)){
					$quantity = $_POST['quantity'];		
					$ids = $_POST['id'];	
					//check if the ids is an array
					if(is_array($ids)){
						for($i = 0; $i < sizeof($ids); $i++){
							$id = unserialize(base64_decode($ids[$i]));
							$id = $id['item_id'];
							if($quantity[$i] == "0"){
								//delete the element from the cart	
								$query = "DELETE FROM carts_items WHERE cart_item_id = ".$id." AND cart_item_cart_id = ".$this->cartID;
								$result = $this->utils->db->query($query);				
								$errors = $this->utils->error($result,__LINE__,get_class($this));
							} else {
								$query = "UPDATE carts_items SET cart_item_quantity = ".$quantity[$i]." WHERE cart_item_id = ".$id." AND cart_item_cart_id = ".$this->cartID;
								$result = $this->utils->db->query($query);				
								$errors = $this->utils->error($result,__LINE__,get_class($this));	
							}//end if
							//echo $query."<br />";							
						}//end for
						return true;
					} else {
						return false;	
					}//end if
				} else {
					return false;
				}//end if
			} else {				
				return false;
			}//end if
		}//end function
		
		public function delete_cart(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				return false;
			}//end if
			
			$this->cartID = $id;
			
			try{
				$query = "DELETE FROM carts_items WHERE cart_item_cart_id = :cart_id";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':cart_id', $this->cartID, PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$query = "DELETE FROM carts WHERE cart_id = :cart_id";
					$result = $this->utils->db->prepare($query);
					$result->bindParam(':cart_id', $this->cartID, PDO::PARAM_INT);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors === false){
						return true;
					} else {
						throw new Exception('Cannot delete the cart');
					}
				} else {
					throw new Exception('Cannot delete the cart');
				}//end if				
			} catch(PDOException $e){
				array_push($this->errors, $e->getMessage());
				return false;	
			}//end try
		}//end function
		
		public function add_to_cart(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				$id = $_POST['id'];	
			}//end if
			
			if(is_null($id)){
				return false;
			}//end if
			
			if(!isset($quantity)){
				$quantity = $_POST['quantity'];	
			}//end if
			
			if(is_null($quantity)){
				$quantity = 1;
			}//end if
			
			if(!isset($price)){
				$price = $_POST['price'];	
			}//end if
			
			if(is_null($price)){
				$price = 0;
			}//end if
			
			if(!isset($vat)){
				$vat = $this->vat*100;	
			}//end if		
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				if($module_data !== false){
					$module_id = $module_data['module_id'];					
				}//end if
			}//end if
			
			if(!isset($module_id)){
				$module_id = 0;
			}
			
			if(!isset($auto_update_quantity)){
				$auto_update_quantity = false;	
			}//end if
			
			if($this->cart_storage == "session"){
				$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
			} else {
				$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
			}//end if
			
			$this->cartID = unserialize(base64_decode($cartID));
		
			if(is_numeric($this->cartID)){					
				//check if quantity is an array
				if(is_array($quantity)){
					//we check each of the quantity fields submitted and  we check if the quantity for each one is bigger than 0
					for($i = 0; $i < sizeof($quantity); $i++){
						if($quantity[$i] > 0){
							//retrieve the id related
							$post_id = $id[$i];
							if($auto_update_quantity){
								//now we check if this cart has already this item
								$query = "SELECT * FROM carts_items WHERE cart_item_post_id = ".$post_id." AND cart_item_module_id = ".$module_id." AND cart_item_cart_id = ".$this->cartID;
								$result = $this->utils->db->query($query);				
								$errors = $this->utils->error($result,__LINE__,get_class($this));
								if($errors === false){
									$num = $result->rowCount();	
									if($num == 1){
										//it already exists, so we just update the quantity
										$row = $result->fetch(PDO::FETCH_ASSOC);
										$item_id = $row['cart_item_id'];
										$item_quantity = $quantity[$i] + $row['cart_item_quantity'];
										//update the quantity in the database
										$query = "UPDATE carts_items SET cart_item_quantity = ".$item_quantity." WHERE cart_item_id = ".$item_id;
										$result = $this->utils->db->query($query);				
										$errors = $this->utils->error($result,__LINE__,get_class($this));
									} else {									
										//new item, so create the record
										$query = "INSERT INTO carts_items
												  (cart_item_cart_id, cart_item_post_id, cart_item_module_id, cart_item_quantity, cart_item_price, cart_item_vat)
												  VALUES
												  (".$this->cartID.",".$id.",".$module_id.",".$quantity[$i].",'".$price[$i]."','".$vat."')";
										$result = $this->utils->db->query($query);				
										$errors = $this->utils->error($result,__LINE__,get_class($this));
									}//end if
								}//end if
							} else {
								$query = "INSERT INTO carts_items
										  (cart_item_cart_id, cart_item_post_id, cart_item_module_id, cart_item_quantity, cart_item_price, cart_item_vat)
										  VALUES
										  (".$this->cartID.",".$id.",".$module_id.",".$quantity[$i].",'".$price[$i]."','".$vat."')";
								$result = $this->utils->db->query($query);				
								$errors = $this->utils->error($result,__LINE__,get_class($this));
							}//end if
						}//end if
					}//end for i
					return true;
				} else if(is_numeric($quantity)){
					//we check each of the quantity fields submitted and  we check if the quantity for each one is bigger than 0					
					if($quantity > 0){
						$post_id = $id;
						if($auto_update_quantity){
							//now we check if this cart has already this item
							$query = "SELECT * FROM carts_items WHERE cart_item_post_id = ".$post_id." AND cart_item_module_id = ".$module_id." AND cart_item_cart_id = ".$this->cartID;							
							$result = $this->utils->db->query($query);				
							$errors = $this->utils->error($result,__LINE__,get_class($this));
							if($errors === false){
								$num = $result->rowCount();	
								if($num == 1){
									//it already exists, so we just update the quantity
									$row = $result->fetch(PDO::FETCH_ASSOC);
									$item_id = $row['cart_item_id'];
									$item_quantity = $quantity + $row['cart_item_quantity'];
									//update the quantity in the database
									$query = "UPDATE carts_items SET cart_item_quantity = ".$item_quantity." WHERE cart_item_id = ".$item_id;
									$result = $this->utils->db->query($query);				
									$errors = $this->utils->error($result,__LINE__,get_class($this));
								} else {														
									//new item, so create the record
									$query = "INSERT INTO carts_items
											  (cart_item_cart_id, cart_item_post_id, cart_item_module_id, cart_item_quantity, cart_item_price, cart_item_vat)
											  VALUES
											  (".$this->cartID.",".$post_id.",".$module_id.",".$quantity.",'".$price."','".$vat."')";
									$result = $this->utils->db->query($query);				
									$errors = $this->utils->error($result,__LINE__,get_class($this));
									if($errors === false){
										$item_id = $this->utils->db->lastInsertId();
									}//end if	
								}//end if								
							}//end if
						} else {
							$query = "INSERT INTO carts_items
									  (cart_item_cart_id, cart_item_post_id, cart_item_module_id, cart_item_quantity, cart_item_price, cart_item_vat)
									  VALUES
									  (".$this->cartID.",".$post_id.",".$module_id.",".$quantity.",'".$price."','".$vat."')";
							$result = $this->utils->db->query($query);				
							$errors = $this->utils->error($result,__LINE__,get_class($this));
							if($errors === false){
								$item_id = $this->utils->db->lastInsertId();
							}//end if	
						}//end if					
						if(isset($item_id)){
							/* META SUPPORT */	
							$this->meta->set_meta_type('cart_item');		
							$this->meta->set_meta_id($item_id);	
							$table_fields = $this->utils->get_table_fields(array("module" => $this->module,"field_prefix" => "cart_item","module_table" => "carts_items", "show_all" => true));			
							array_walk($table_fields,array($this->utils,'clean_prefix'),$table_prefix);		
							$this->meta->auto_add_meta(array("avoid" => $table_fields));
							/* END META SUPPORT */	
						}//end if
					}//end if					
					return true;	
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function delete_item_from_cart(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id) || !is_numeric($id)){
				return false;	
			}//end if						
			
			if(!isset($cart_id)){
				if($this->cart_storage == "session"){
					$cartID = $_SESSION[$this->cartPrefix.'cart_id'];
				} else {
					$cartID = $_COOKIE[$this->cartPrefix.'cart_id'];	
				}//end if
				$this->cartID = unserialize(base64_decode($cartID));
			} else {
				$this->cartID = $cart_id;	
			}
			
			if(!is_null($this->cartID)){				
				if(is_numeric($this->cartID)){
					if(is_numeric($id)){
						$query = "DELETE carts_items.* 
								  FROM carts_items, carts
								  WHERE cart_item_id = ".$id."
								  AND cart_item_cart_id = cart_id
								  AND cart_id = ".$this->cartID."
								  AND cart_paid = 0";
								//  echo $query;
						$result = $this->utils->db->query($query);				
						$errors = $this->utils->error($result,__LINE__,get_class($this));
						if($errors === false){
							return true;
						} else {
							return false;
						}//end if
					} else {
						return false;	
					}//end if	
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}
		}//end function
		
		public function edit_item_in_cart(){
			
		}//end function
		
		public function set_cart_status(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($cart_id) || !isset($status)){
				return false;	
			}//end if
		}//end function
		
		public function mark_cart_as_paid(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($cart_id)){
				return false;	
			}//end if
			
			if(!isset($payment_id)){
				$payment_id = 0;
			}//end if
			
			if(!isset($status)){
				$status = 'paid';
			}//end if	
			
			if(!isset($result)){
				$result = NULL;
			}//end if
			
			if(is_numeric($cart_id)){
				$this->cartID = $cart_id;
				
				if($result == NULL){
					$result = $_POST;	
				}//end if
				
				switch($this->moneyMaker){
					case "paypal":
						$query = "UPDATE carts 
							  SET cart_paid = 1,
							  cart_ipn_date = NOW(),
							  cart_payment_id = ".$payment_id.",
							  cart_payment_response = '".base64_encode(serialize($result))."',
							  cart_status = '".$status."'
							  WHERE cart_id = ".$this->cartID;
						break;
					case "realex":
					case "sagepay":
						$query = "UPDATE carts 
								  SET cart_paid = 1,
								  cart_payment_id = ".$payment_id.",
								  cart_payment_response = '".base64_encode(serialize($result))."',
							      cart_status = '".$status."'
								  WHERE cart_id = ".$this->cartID;
						break;
					case "none":
					default:
						$query = "UPDATE carts 
								  SET cart_paid = 1,
								  cart_payment_id = ".$payment_id.",
								  cart_payment_response = '',
							  	  cart_status = '".$status."'
								  WHERE cart_id = ".$this->cartID;				
				
				}//end switch
				
				//echo $query;
				
				$result = $this->utils->db->query($query);				
				$errors = $this->utils->error($result,__LINE__,get_class($this));	
				if($errors === false){
					//clean the session
					$this->clear_cart_session();
					//notify the purchase
					//$this->notify_purchase();
					return true;
				} else {
					return false;
				}//end if	
			} else {
				return false;
			}//end if
		}//end function
		
		public function clear_cart_session(){
			$_SESSION[$this->cartPrefix.'cart_id'] = NULL;
			unset($_SESSION[$this->cartPrefix.'cart_id']);
			setcookie($this->cartPrefix.'cart_id',"",time()-(3600*24),"/");
			unset($_COOKIE[$this->cartPrefix.'cart_id']);
			return true;
		}//end function
		
		
		/************************************************************************/
		/* NOTIFICATIONS METHODS												*/
		/************************************************************************/
		
	
		protected function notify_purchase(){
			//check if we have a numeric cartID
			if(is_numeric($this->cartID)){
				//get the cart
				$cart_data = $this->get_cart(array("internal" => true,"paid" => true,'meta' => true));				
				if($cart_data != false){	
					//I just need to check in the first element if we already sent an email						
					if($cart_data[0]['cart_purchase_notified'] != "1"){																	
						$figures = $this->get_cart_figures();	
						$html = "";
						//check if we have meta about this cart
						if(isset($cart_data['meta_email'])){
							$html .= "Hi ".$cart_data['meta_firstname'].' '.$cart_data['meta_surname'].',<br />';	
							//check if we have an email to send
							if($cart_data['meta_email'] != NULL){
								$email_to = $cart_data['meta_email'];
							} else {
								//if not fallback to the notification email
								$email_to = $this->email_notification_reply;
							}//end if
						} else {
							//if not fallback to the notification email
							$email_to = $this->email_notification_reply;
						}//end if
						
						$html .= '<p>This email is just to confirm that we received your order, which it consists of:';
						for($i = 0; $i < sizeof($cart_data); $i++){							
							$html .= '<br />'.$cart_data[$i]['cart_item_quantity'].' x '.$cart_data[$i]['post_title'].' @ <strong>'.$cart_data[$i]['cart_item_price'].'</strong>';
						}//end for i
						$html .= '</p>';
						$html .= '<p>Total paid: <strong>'.$figures['total'].'</strong></p>';
						
						//get the payment reference
						if(is_numeric($cart_data[0]['cart_payment_id']) && $cart_data[0]['cart_payment_id'] > 0){
							$payments = $this->utils->call("payments");
							$payment_data = $payments->get_payment($cart_data[0]['cart_payment_id']);
							if($payment_data != false){
								$html .= '<p>Your payment reference ID is: <strong>'.$payment_data['payment_code'].'</strong></p>';
							}//end if
						}//end if
						
						$mailer = $this->utils->call("mailer");
						$mailer->set_sender($this->no_reply_email);			
						$mailer->set_bcc($this->email_notification_reply);
						$mailer->set_message(array('type' => 'text/html', 'message' => $html));
						$subject = "Your purchase summary from ".$this->domain;
						$mailresult = $mailer->send_mail(array('email' => array($email_to),'subject' => $subject));
						if($mailresult){							
							$query = "UPDATE carts SET cart_purchase_notified = 1 WHERE cart_id = ".$this->cartID;							
							$result = $this->utils->db->query($query);				
							$errors = $this->utils->error($result,__LINE__,get_class($this));
							if($errors === false){
								return true;		
							} else {
								return false;	
							}//end if
						}//end if 	
					} else {
						return false;
					}//end if
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		/************************************************************************/
		/* END NOTIFICATIONS METHODS											*/
		/************************************************************************/
		
		public function get_countries(){
			$tools = $this->utils->call('tools');
			$countries = tools::get_countries();		
			return $countries;
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
		
		protected function init_settings(){
			$this->settings = $this->utils->get_settings(array('module' => $this->module));		
			$this->cartPrefix = $this->settings['session_prefix'];
			$this->moneyMaker = $this->settings['credit_system'];
			$this->cart_storage = $this->settings['cart_storage_type'];
			$this->email_notification_reply = $this->settings['cart_notification_email'];
			$this->no_reply_email = $this->settings['cart_noreply_email'];			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>