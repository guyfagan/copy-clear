<?php
	#################################################################
	# Payments Class - 	last update 05/10/12						#
	#					created on 03/10/12							#
	# based on tyres2go payment class								#				
	#################################################################

	require_once('ewrite.class.php');

	class payments extends ewrite{

		protected $pay_id;//Payment id	
		protected $code;
		private $paymentType;
		private $paymentSystem;
		public $module; //module
		protected $meta;
		protected $settings;
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);	
			$this->module = "payments";
			$this->utils->read_params($this,$params);			
			$module_data = $this->utils->get_module($this->module);
			$this->force_module($this->module);
			$this->module_id = $module_data['module_id'];	
			$this->paymentType = "form";
			$this->paymentSystem = "realex";	
			$this->paid = false;
			//Meta init	
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);	
			//Set basic settings
			$this->init_settings();	
		}//endconstructor
		
		public function get_payments(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($sort_by)){
				$sort_by = "payment_date";
			}//end if
			if(!isset($sort_order)){
				$sort_order = "DESC";
			}//end if
			
			$query = "SELECT payments.*, UNIX_TIMESTAMP(payment_date) AS payment_date, user_username, user_firstname, user_surname, module_name, module_label
					  FROM payments
					  LEFT JOIN modules
					  ON module_id = payment_module_id
					  LEFT JOIN users
					  ON user_id = payment_uid
					  WHERE payment_id > 0";
			if(isset($user_id)){
				$query .= " AND payment_uid = ".$user_id;	
			}//end if
			if(isset($post_id)){
				$query .= " AND payment_rel_id = ".$post_id;	
			}//end if
			$query .= " GROUP BY payment_id						
						ORDER BY ".$sort_by." ".$sort_order;							
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($this->limit_result != NULL){
				$query .= " LIMIT ".$this->limit_result;
			}//end if	
											
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);				
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;	
			}
		}//end function
		
		public function get_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($pay_id)){
				return false;	
			}//end if
			
			$this->pay_id = $pay_id;
			
			$query = "SELECT payments.*, UNIX_TIMESTAMP(payment_date) AS payment_date, user_firstname, user_surname, user_email, module_label, module_name
					  FROM payments 
					  LEFT JOIN users
					  ON user_id = payment_uid		
					  LEFT JOIN modules
					  ON module_id = payment_module_id			 
					  WHERE payment_id = ".$pay_id;
			if(isset($user_id)){
				$query .= " AND payment_uid = ".$user_id;	
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;	
					}//end if
					if(!isset($raw)){
						$raw = false;	
					}//end if
					$this->meta->set_meta_type('payment');
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$meta_data = $this->meta->get_meta(array("id" => $pay_id, "html" => (bool)$html, "raw" => (bool)$raw));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for					
						$meta_data = $this->meta->get_meta(array("id" => $pay_id,"search" => $meta, "html" => (bool)$html, "raw" => (bool)$raw));				
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					}//end if
					/* END META SUPPORT */				
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;	
			}//end if
		}//endfunction		
		
		/*
		private function create_code($pay_id){
			$chars = strlen($pay_id);
			$chars = 6-$chars;
			$this->code = "";
			if($chars > 0){
				for($k = 0; $k < $chars; $k++){
					$this->code .= $this->utils->get_random_letter();
				}//end for
			}//end if
			$this->code .= $pay_id;
			
			//Update the payment with the code
			$query = "UPDATE payments SET payment_code = '".$this->code."' WHERE payment_id = ".$pay_id;
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;	
			} else {
				return false;
			}//end if
		}//end function
		*/
		
		public function do_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($method)){
				$method = 'credit_card';
			}//end if
			
			$options = array();			
			if(isset($params)){
				$options = $params[0];	
			}//end if
			
			if(isset($description)){
				$options['description'] = $description;	
			}//end if
			
			if(isset($status)){
				$options['status'] = $status;	
			} else {
				$options['status'] = 'pending';	
			}//end if
			
			if(isset($user_id)){
				$data['user_id'] = (int)$user_id;	
			}//end if
			
			
			
			switch($method){
				case "cash":							
					return $this->do_cash_payment($options);					
					break;
				case "custom":
				default:
					$options['method'] = $method;
					return $this->do_custom_payment($options);
					break;
				case "credit_card":
					switch($this->paymentSystem){			
						case "sagepay":
							return $this->do_sagepay_payment($options);
							break;
						case "realex":
							return $this->do_realex_payment($options);
							break;
					}//end switch
					break;	
			}//end switch
			
		}//end function
		
		/* CASH PAYMENT */
		
		public function do_cash_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
		
			if(!isset($rel_id) || !isset($encdata)){
				return false;	
			}//end if
						
			$data = array();
			$data['rel_id'] = $rel_id;
			$data['system'] = 'none';
			$data['method'] = 'cash';
	
			if(isset($user_id)){
				$data['uid'] = $user_id;	
			}//end if
	
			if(isset($module_id)){
				$data['module_id'] = $module_id;
			}//end if
				
			if(!isset($amount)){
				if(isset($encdata['amount'])){
					$data['amount'] = $encdata['amount'];
				} else {
					$data['amount'] = 0;	
				}//end if
			} else {
				$data['amount'] = $amount;
			}//end if
			
			//Check if this user push twice or more the buttons or just refreshed the page
			
			$page_refresh = $this->avoid_page_refresh($data,'cash');
			if($page_refresh != false){
				$this->pay_id = $page_refresh;
				return true;
			}//end if
			
			$sql = $this->utils->build_sql_fields(array('table_prefix' => "payment","sql_type" => "INSERT", "method_data" => $data));
			//Record the first informations
			$query = "INSERT INTO ".$this->t."payments ".$sql;	
			//echo $query;
					
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->pay_id = $this->utils->db->lastInsertId();	
				$this->create_payment_code($this->pay_id);	
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		/* CUSTOM PAYMENT */
		public function do_custom_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
		
			if(!isset($rel_id) || !isset($encdata)){
				return false;	
			}//end if
						
			$data = array();
			$data['rel_id'] = $rel_id;
			$data['system'] = 'none';
			$data['method'] = $method;			
	
			if(isset($user_id)){
				$data['uid'] = $user_id;	
			}//end if
	
			if(isset($module_id)){
				$data['module_id'] = $module_id;
			}//end if
			
			if(isset($description)){
				$data['description'] = $description;	
			}//end if
				
			if(!isset($amount)){
				if(isset($encdata['amount'])){
					$data['amount'] = $encdata['amount'];
				} else {
					$data['amount'] = 0;	
				}//end if
			} else {
				$data['amount'] = $amount;
			}//end if
			
			//Check if this user push twice or more the buttons or just refreshed the page
			
			$page_refresh = $this->avoid_page_refresh($data,'cash');
			if($page_refresh != false){
				$this->pay_id = $page_refresh;
				return true;
			}//end if
			
			$sql = $this->utils->build_sql_fields(array('table_prefix' => "payment","sql_type" => "INSERT", "method_data" => $data));
			//Record the first informations
			$query = "INSERT INTO ".$this->t."payments ".$sql;	
			//echo $query;
					
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->pay_id = $this->utils->db->lastInsertId();	
				$this->create_payment_code($this->pay_id);	
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		/* CREDIT CARD PAYMENTS */
		/*
		public function do_cc_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($options)){
				return false;	
			}//end if
			
		}//end function
		*/
		
		/* SAGEPAY PART */
		
		//PD means Payment Description, it's just a text to describe what the payment is for
		public function do_sagepay_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($rel_id) || !isset($encdata)){
				return false;	
			}//end if
						
			$data = array();
			$data['rel_id'] = $rel_id;
			$data['system'] = 'sagepay';
			$data['method'] = 'credit_card';
			
			#### FIELDS FOR FORM MODE ####
			/*
			if($pd != NULL){
				$data['description'] = $pd;
			}//end if					
			$data['sagepay_authno'] = $sagepay_result['TxAuthNo'];
			$data['sagepay_status'] = $sagepay_result['Status'];
			$data['sagepay_lastdigits'] = $sagepay_result['Last4Digits'];
			$data['sagepay_vid'] = $sagepay_result['VPSTxId'];
			$data['sagepay_vendortxcode'] = $sagepay_result['VendorTxCode'];
			*/
			
			if(isset($response)){
				$data['response'] = $response;	
			}//end if
			
			if(isset($module_id)){
				$data['module_id'] = $module_id;
			}//end if
			
			if(isset($status)){
				$data['status'] = $status;	
			}//end if
			
			if(isset($user_id)){
				$data['uid'] = $user_id;	
			}//end if
			
			if(isset($description)){
				$data['description'] = $description;	
			}//end if
			
			$data['billing_name'] = $encdata['cardname'];
			$data['card_expiry'] = $encdata['expdate'];
			$data['card'] = $encdata['cardtype'];
			
			if(!isset($amount)){
				if(isset($encdata['amount'])){
					$data['amount'] = $encdata['amount'];
				} else {
					$data['amount'] = 0;	
				}//end if
			} else {
				$data['amount'] = $amount;
			}//end if
			
			//Check if this user push twice or more the buttons or just refreshed the page
			$page_refresh = $this->avoid_page_refresh($data,'credit_card');
			if($page_refresh != false){
				$this->pay_id = $page_refresh;
				return true;
			}//end if
			
			$sql = $this->utils->build_sql_fields(array('table_prefix' => "payment","sql_type" => "INSERT", "method_data" => $data));
			//Record the first informations
			$query = "INSERT INTO ".$this->t."payments ".$sql;							
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->pay_id = $this->utils->db->lastInsertId();	
				$this->create_payment_code($this->pay_id);	
				return true;
			} else {
				return false;	
			}//end if
		}//end function		
		
		/* END SAGEPAY PART */
		
		/* REALEX PART */
		
		public function do_realex_payment(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($rel_id) || !isset($encdata)){
				return false;	
			}//end if
						
			$data = array();
			$data['rel_id'] = $rel_id;
			$data['system'] = 'realex';
			$data['method'] = 'credit_card';
			
			if(isset($response)){
				$data['response'] = $response;	
			}//end if
			
			if(isset($module_id)){
				$data['module_id'] = $module_id;
			}//end if
			
			if(isset($status)){
				$data['status'] = $status;	
			}//end if
			
			if(isset($user_id)){
				$data['uid'] = $user_id;	
			}//end if
			
			if(isset($description)){
				$data['description'] = $description;	
			}//end if
			
			$data['billing_name'] = $encdata['cardname'];
			$data['card_expiry'] = $encdata['expdate'];
			$data['card'] = $encdata['cardtype'];
			
			if($this->settings['realexMode'] == 'test'){
				$data['test_mode'] = "1";	
			}//end if
			
			if(!isset($amount)){
				if(isset($encdata['amount'])){
					$data['amount'] = $encdata['amount'];
				} else {
					$data['amount'] = 0;	
				}//end if
			} else {
				$data['amount'] = $amount;
			}//end if
			
			//Check if this user push twice or more the buttons or just refreshed the page
			$page_refresh = $this->avoid_page_refresh($data,'credit_card');
			if($page_refresh != false){
				$this->pay_id = $page_refresh;
				return true;
			}//end if
			
			$sql = $this->utils->build_sql_fields(array('table_prefix' => "payment","sql_type" => "INSERT", "method_data" => $data));
			//Record the first informations
			$query = "INSERT INTO ".$this->t."payments ".$sql;	
			//echo $query;
					
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->pay_id = $this->utils->db->lastInsertId();	
				$this->create_payment_code($this->pay_id);	
				return true;
			} else {
				return false;	
			}//end if
			
		}//end function
				
		/* END REALEX PART */
		
		public function log_response($response,$id = NULL,$section = NULL,$encdata = NULL){
			switch($this->paymentSystem){
				case "sagepay":
					if($id == NULL){
						$id = "0";	
					}//end if					
					$query = "INSERT INTO sagepay_logs
							  (sagepay_log_response,sagepay_log_pid, sagepay_log_module, sagepay_log_encdata)
							  VALUES
							  ('".serialize($response)."',".$id.",'".$section."','".$encdata."')";
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));	
					if($errors === false){
						return true;
					} else {
						return false;
					}//end if
					break;
				default:
					return true;
			}//end switch
		}//end function
		
		private function avoid_page_refresh($data,$method){			
			//$userid = $data['uid'];				
			if($method == "credit_card"){
				switch($this->paymentSystem){
					case "sagepay":
						/*$lastdigits = $data['sagepay_lastdigits'];
						$sage_vid = $data['sagepay_vid'];						
						$query = "SELECT * FROM payments 
								  WHERE DATE(payment_date) > (CURDATE()-1)
								  AND payment_sagepay_lastdigits = '".$lastdigits."'
								  AND payment_sagepay_vid = '".$sage_vid."'";						
						$result = $this->utils->db->query($query);
						$errors = $this->utils->error($result,__LINE__,get_class($this));	
						if($errors === false){
							$num = $result->rowCount();
							if($num > 0){
								$row = $result->fetch(PDO::FETCH_ASSOC);
								return $row['payment_id'];
							} else {
								return false;
							}//end if
						} else {
							return false;	
						}*/
						return false;
						break;
				}
			} else {
				//it's a cheque payment
				return false;
			}//end if		
		}//end function
		
		public function get_paid_status(){
			return $this->paid;
		}//end function
		
		public function get_payment_id(){
			return $this->pay_id;
		}//end function
		
		public function get_payment_code($pay_id = NULL){			
			if($pay_id == NULL){
				return $this->code;
			} else {
				$data = $this->get_payment(array('id' => $pay_id));				
				return $data['payment_code'];
			}//end if
		}//end function
		
		public function update_payment_status(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($pay_id)){
				$pay_id = $this->pay_id;	
			}//end if
			
			if(is_null($pay_id) || !isset($status)){
				return false;
			}//end if
			
			$query = "UPDATE payments SET payment_status = '".$status."' WHERE payment_id = ".$pay_id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){			
				$this->update_payment_field(array('field' => 'payment_paid', 'field_value' => 1, 'pay_id' => $pay_id));
				return true;
			} else {
				return false;	
			}//end if
		}//end function
				
		public function update_payment_field(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($pay_id)){
				$pay_id = $this->pay_id;	
			}//end if
			
			if(is_null($pay_id) || !isset($field) || !isset($field_value)){
				return false;
			}//end if
			
			$query = "UPDATE payments SET ".$field." = '".$field_value."' WHERE payment_id = ".$pay_id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){			
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		/*
		public function store_realex_payment($rel_id,$description = "standard payment"){
			$query = "INSERT INTO payments 
					  (payment_amount, payment_description, payment_rel_id,payment_paid, payment_realex_order_id, payment_realex_authcode, payment_realex_md5hash, payment_realex_timestamp, payment_realex_message)
					  VALUES
					  ('".($_POST['AMOUNT']/100)."','".$description."',".$rel_id.",1,'".$_POST['ORDER_ID']."','".$_POST['AUTHCODE']."','".$_POST['MD5HASH']."','".$_POST['TIMESTAMP']."','".$_POST['MESSAGE']."')";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){				
				$this->pay_id = $this->utils->db->lastInsertId();	
				$this->create_payment_code($this->pay_id);		
				return true;	
			} else {
				return false;	
			}//end if
		}//end function
		*/
		
		private function create_payment_code($pay_id){
			$tools = $this->utils->call('tools');
			$chars = strlen($pay_id);
			$chars = 6-$chars;
			$this->code = "";
			if($chars > 0){
				for($k = 0; $k < $chars; $k++){
					$this->code .= $tools->get_random_letter();
				}//end for
			}//end if
			$this->code .= $pay_id;
			
			//Update the booking with the code
			$query = "UPDATE payments SET payment_code = '".$this->code."' WHERE payment_id = ".$pay_id;
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		#################################################################
		# STATS PART													#		
		#################################################################
		
		public function get_stats(){			
			$stats = $this->get_stats_payments();
			return $stats;
		}//end function
		
		protected function get_stats_payments(){
			/*$query = "SELECT COUNT(*) AS total, 
					  COUNT(CASE WHEN `payment_status` = 'paid' THEN 1 END) AS paid, 
					  COUNT(CASE WHEN `payment_status` = 'denied' THEN 1 END) AS unpaid
					  FROM payments, modules
					  WHERE payment_status = 'paid'
					  AND payment_module_id = module_id";
					  */
			$query = "SELECT COUNT(module_id) AS total, module_label
					  FROM payments, modules
					  WHERE payment_status = 'paid'
					  AND payment_module_id = module_id
					  GROUP BY module_id";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function check_ssl(){
			if((bool)$this->settings['paymentSecureServer']){
				if (!isset($_SERVER['HTTPS'])) {
				   header('Location: https://'.$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
				}
			}//end if
		}//end function
		
		public function uncheck_ssl(){
			if((bool)$this->settings['paymentSecureServer']){
				if (isset($_SERVER['HTTPS'])) {
				   header('Location: http://'.$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
				}
			}//end if
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
		
		private function init_settings(){
			$this->settings = $this->utils->get_settings(array('module' => $this->module));		
			$this->paymentSystem = $this->settings['paymentSystem'];		
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
		
		
	}//end class
?>
		