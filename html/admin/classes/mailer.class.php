<?php
	#################################################################
	# Mailer Class -  last update 30/04/13							#
	#				  created on 30/04/13							#
	#																#
	# This class will take care of all the email sent with the CMS  #		
	#################################################################
	
	//load SwiftMailer
	require_once $_SERVER['DOCUMENT_ROOT'].__CMS_PATH__.'inc/swift/lib/swift_required.php';
	
	class mailer{
		protected $utils;//utils class
		protected $transport;	
		protected $mailer;	
		protected $headers;
		protected $swiftMessage;
		protected $mailMessage;
		protected $mailFormats;
		protected $mailAttachments;
		protected $settings;
		public $defaultFormat;
		public $failures;
		public $sendFrom;	
		public $cc;
		public $bcc;	
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;			
			$this->utils->read_params($this,$params);				
			$this->mailMessage = array();
			$this->mailAttachments = array();				
			$this->init_settings();
		}//endconstructor	
		
		public function set_sender($sendFrom){
			if(!is_array($sendFrom)){
				$this->sendFrom = array($sendFrom);
			} else {
				$this->sendFrom = $sendFrom;	
			}//end if
		}//end function
		
		public function set_cc($cc){
			if(!is_array($sendFrom)){
				$this->cc = array($cc);
			} else {
				$this->cc = $cc;	
			}//end if
		}//end function
		
		public function set_bcc($bcc){
			if(!is_array($bcc)){
				$this->bcc = array($bcc);
			} else {
				$this->bcc = $bcc;	
			}//end if
		}//end function
		
		private function swift_init(){
			// Create the Transport			
			if(isset($this->settings['mailDelivery']) && $this->settings['mailDelivery'] == 'mandrill'){
				if(!is_null($this->settings['mandrillUsername']) && !is_null($this->settings['mandrillApiKey'])){
					$this->transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
					$this->transport->setUsername($this->settings['mandrillUsername']);
					$this->transport->setPassword($this->settings['mandrillApiKey']);
				} else {
					$$this->transport = Swift_MailTransport::newInstance();
				}//end if
			} else {			
				$this->transport = Swift_MailTransport::newInstance();
			}//end if
			// Create the Mailer using your created Transport
			$this->mailer = Swift_Mailer::newInstance($this->transport);
			
			// Create the message
			$this->swiftMessage = Swift_Message::newInstance();							
				
			$this->headers = $this->swiftMessage->getHeaders();
			$this->headers->addTextHeader('Message-ID', "<".time()." TheSystem@".$_SERVER['SERVER_NAME'].">");
			$this->headers->addTextHeader('X-Mailer', "PHP v".phpversion());
			
		}//end function
		
		public function add_attachment($filepath){
			array_push($this->mailAttachments,$filepath);
		}//end function
		
		public function set_template(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($template)){
				return false;
			}//end if
			
			$filename = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.$template;
			$handle = fopen($filename, "r");
			//get the html
			$html = fread($handle, filesize($filename));
			fclose($handle);
			//now we replace the template tags
			if(isset($message)){
				//first, the message
				$html = str_replace("{#message#}",$message,$html);
			}//end if
			//now we check for tags
			if(isset($tags)){
				foreach($tags as $key => $value){
					$html = str_replace('{#'.$key.'#}',$value,$html);
				}//end foreach
			}//end if		
			
			$this->set_message(array('message' => $html));
		}//end function
		
		public function set_message(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			//this function accept two parameters, one is the message the other one is the type, which can be 'text/html' or 'text/plain'
			
			if(!isset($message)){
				return false;	
			}//end if
			
			if(!isset($type)){
				$type = $this->defaultFormat;
			}//end if			
			if(isset($message)){
				$this->mailMessage[$type] = $message;					
			}//end if
		}//end function
		
		public function send_mail(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($subject) || !isset($email)){
				return false;	
			}//end if
			
			//check if the email is an array
			if(!is_array($email)){
				//if not lets turn it into an array
				$email = array($email);	
			}//end if
		
			if($this->settings['testEnvironmentEnabled'] == 1 && trim($this->settings['testEnvironmentEnabled']) != ""){
				//override the email in case the test environment is enabled
				$email = explode(',',$this->settings['testEnvironmentNotificationsEmail']);
			}//end if
			
			$this->swift_init();			
			
			// Give the message a subject
			$this->swiftMessage->setSubject($subject);			
			// Set the From address with an associative array
			$this->swiftMessage->setFrom($this->sendFrom);	
			// Set the To addresses with an associative array
			$this->swiftMessage->setTo($email);	
			if(isset($cc)){
				if(!is_array($cc)){
					$cc = array($cc);	
				}//end if
				$this->swiftMessage->setCc($cc);
			} else if(!is_null($this->cc)){
				$this->swiftMessage->setCc($this->cc);			
			}//end if
			
			if(isset($bcc)){
				if(!is_array($bcc)){
					$cc = array($bcc);	
				}//end if
				$this->swiftMessage->setBcc($bcc);
			} else if(!is_null($this->bcc)){
				$this->swiftMessage->setBcc($this->bcc);
			}//end if
			
			$body = array_shift($this->mailMessage);
			
			$this->swiftMessage->setBody($body,$this->defaultFormat);	
			
			if(sizeof($this->mailMessage) > 0){
				foreach($this->mailMessage as $type => $message){						
					// And optionally an alternative body				
					$this->swiftMessage->addPart($message, $type);
				}//end foreach
			}//end if
			
			if(sizeof($this->mailAttachments) > 0){
				for($i = 0; $i < sizeof($this->mailAttachments); $i++){
					$this->swiftMessage->attach(Swift_Attachment::fromPath($this->mailAttachments[$i]));
				}
			}
			
			$this->utils->store_email(array("sender" => $this->sendFrom[0],"recipient" => serialize($email),"subject" => $subject,"message" => serialize($body), "headers" => serialize($this->headers)));	
			$recipients = $this->mailer->send($this->swiftMessage,$failures);
			if($recipients){
				$this->utils->mark_mail_as_sent();
				return true;
			} else {
				$this->failures = $failures;
				return false;
			}//end if
	
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
				
		private function init_settings(){
			$this->settings = array();			
			//read the settings from the cms
			$cms_settings = $this->utils->get_settings();
			if(!isset($cms_settings['sendFrom'])){
				$this->sendFrom = array('no-reply@oldhat.ie');
			} else {
				$this->sendFrom = explode(",",$cms_settings['sendFrom']);
			}//end if
							
			if(!isset($cms_settings['defaultFormat'])){
				$this->defaultFormat = 'text/html';	
			} else {
				$this->defaultFormat = $cms_settings['defaultFormat'];
			}//end if
			if($cms_settings !== false){
				//if we have something, merge the two arrays, bear in mind that the cms settings will override the previous settings
				$this->settings = array_merge($this->settings,(array)$cms_settings);	
			}//end if
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>