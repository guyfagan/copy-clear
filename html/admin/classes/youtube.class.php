<?php
	class youtube{
	
		public $utils;//utils class
		public $t;//prefix before each tablename
		private $api_path;
	
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;			
			$this->utils->read_params($this,$params);
			$this->api_path = "http://gdata.youtube.com/feeds/api/videos/";
		}//endconstructor	
		
		public function get_video_data($url_video){
			$url = trim($url_video);
			$url = $this->get_video_id($url_video);			
			$yt = @curl_init();
			//echo $this->api_path.$url;
			@curl_setopt($yt, CURLOPT_URL, $this->api_path.$url."?v=2&alt=json");		
			//@curl_setopt($yt, CURLOPT_URL, $this->api_path.$url);		
			@curl_setopt($yt, CURLOPT_RETURNTRANSFER, TRUE);
			$yti = @curl_exec($yt);		
			$data = json_decode($yti,true);
			@curl_close($yt);
			
			$result = array();
			/*$result['thumbnail_large'] = $data['feed']['entry'][0]['media'.'$'.'group']['media$thumbnail'][0]['url'];
			$result['video_url'] = $data['feed']['entry'][0]['link'][0]['href'];
			$result['title'] = $data['feed']['entry'][0]['title']['$t'];*/		
			$result['thumbnail_large'] = $data['entry']['media'.'$'.'group']['media$thumbnail'][0]['url'];
			$result['image_medium'] = $data['entry']['media'.'$'.'group']['media$thumbnail'][1]['url'];
			$result['image_large'] = $data['entry']['media'.'$'.'group']['media$thumbnail'][3]['url'];
			$result['video_url'] = $data['entry']['link'][0]['href'];
			$result['title'] = $data['entry']['title']['$t'];			
			
			return $result;
		}//end function

		public function get_video_id($url_video){
			$url_video = str_replace("#!","", $url_video);
			/*
			$url_video = explode("?",$url_video);
			$url_vars = $url_video[1];
			$url_vars = explode("&",$url_vars);
			$video_id = explode("=",$url_vars[0]);
			$video_id = $video_id[1];
			*/
			//better way to get the video ID from youtube
			$url_vars = parse_url($url_video);		
			$url_vars = parse_str($url_vars['query'],$output);	
			$video_id = $output['v'];
			return $video_id;
		}//end function
		
		public function get_video_thumbnails($url_video){
			$video_data = $this->get_video_data($url_video);				
			$photos = array();
			$photos['small'] = $video_data['thumbnail_large'];
			$photos['medium'] = $video_data['image_medium'];
			$photos['large'] = $video_data['image_large'];
			return $photos;
		}//end function
		
		public function get_video_iframe(){
			$params = func_get_args();
			if(is_array($params[0])){			
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($url)){
				return false;	
			}//end if
			
			if(!isset($ssl)){
				$ssl = false;
			}//end if
			
			$base_url = 'http://www.youtube.com/embed/';
			if($ssl){
				$base_url = 'https://www.youtube.com/embed/';	
			}//end if
			
			if(!isset($width)){
				$width = 560;//youtube defaults	
			}//end if
			if(!isset($height)){
				$height = 315;//youtube defaults
			}//end if
			
			$video_id = $this->get_video_id($url);
			$iframe = '<iframe width="'.$width.'" height="'.$height.'" src="'.$base_url.$video_id.'?rel=0" frameborder="0" allowfullscreen></iframe>';
			return $iframe;
		}//end function
	}//end class
?>