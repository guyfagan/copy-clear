<?php
	class instagram{
		
		protected $utils;//utils class
		public $errors = array();
		public $settings = array();
		private $client_id;
		private $client_secret;
		private $user_id;
		public $format = "json";
		public $api_uri = 'https://api.instagram.com/v1/';
		
		//Constructor
		function __construct($utils,$params = array()){	
			$this->utils = $utils;			
			//Set basic settings
			$this->init_settings();
		}//endconstructor
		
		public function get_user_recent_media(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$path = $this->api_uri.'users/'.$this->user_id.'/media/recent/?client_id='.$this->client_id;
			
			if(isset($limit)){
				$path .= '&count='.$limit;	
			}//end if
			
			$result = $this->read_request(array('url' => $path));
			if($result !== false){
				$photos = $result['data'];
				if(is_array($photos)){
					$data = array();
					for($i = 0; $i < sizeof($photos); $i++){
						$photo = array();
						$photo['type'] = $photos[$i]['type'];
						$photo['link'] = $photos[$i]['link'];
						$photo['thumbnail'] = $photos[$i]['images']['thumbnail']['url'];
						$photo['standard'] = $photos[$i]['images']['standard_resolution']['url'];
						$photo['caption'] = $photos[$i]['caption']['text'];
						if($photo['type'] == 'video'){
							$photo['video'] = array();
							$photo['low_resolution'] = $photos[$i]['videos']['low_resolution']['url'];
							$photo['standard_resolution'] = $photos[$i]['videos']['standard_resolution']['url'];
						}//end if
						array_push($data,$photo);
					}//end for i
					return $data;
				} else {
					return false;
				}//end if				
			} else {
				return false;	
			}//end if
		}//end function
		
		private function read_request(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($url)){
				return false;
			}//end if
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);					
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, false); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 			
			$result = curl_exec($ch);				
			curl_close($ch);
			switch($this->format){
				case "json":
					$result = json_decode($result,true);
					break;	
			}
			return $result;
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################		
		
		private function init_settings(){			
			//read the settings from the cms
			$this->settings = $this->utils->view->settings;
			$this->client_id = $this->settings['instagramClientID'];
			$this->client_secret = $this->settings['instagramClientSecret'];
			$this->user_id = $this->settings['instagramUserID'];			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>