<?php
	class badwords{
	
		public $utils;//utils class
		public $t;//prefix before each tablename	
	
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;			
			$this->utils->read_params($this,$params);
		}//endconstructor
		
		private function get_bad_words(){
			$query = "SELECT * FROM badwords";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = array();
				while($row = @mysql_fetch_array($result)){
					array_push($data,"/(\s)".$row['badword_word']."/");
				}//endwhile					
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function check_bad_words($text){
			$bad_list = $this->get_bad_words();		
			//check if there are any bad words
			if($bad_list != false){
				$count = 0;
				$text = strtolower($text);
				$text_filtered = preg_replace($bad_list,"@@@@",$text,-1,$count);			
				if($count > 0){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function bad_words_import($csv_file, $erase_previous = false){
			$filepath = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__."content/import/".$csv_file;		
			if($erase_previous == true){
				$query = "TRUNCATE TABLE badwords";
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));	
			}//end if
			$content = file_get_contents($filepath);
			$content = explode("\n",$content);
			for($i = 0; $i < sizeof($content); $i++){
				$query = "INSERT INTO badwords (badword_word) VALUES ('".$content[$i]."')";
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));	
			}//end for i
		}//end function
	}//end class
?>