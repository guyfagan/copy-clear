<?php
	class view{
		//Variables
		public $utils;
		public $t;//prefix before each tablename
		public $uid;//userid
		public $lang;//default language
		private $action;//Action variable
		private $module;//module
		private $cat_path;
		private $cat_path_counter;
		private $default_action;
		private $extra_js_code;
		
		//constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->lang = __LANGUAGE__;
			$this->t = __TABLE_PREFIX__;
			$this->action = $_REQUEST['action'];	
			$this->extra_js_code = NULL;
		}//end function
		
		public function set_module($module){
			$this->module = $module;
			$this->utils->set_module($module);
			$this->get_default_action();
		}//end function
		
		public function get_action_menu(){
			$menu_inc = "includes/view/menus/".$this->module.".menu.php";
			if(!file_exists("includes/view/menus/".$this->module.".menu.php")){
				$menu_inc = "includes/view/menus/generic.menu.php";
			}//end if
			return $menu_inc;
		}//end if
		
		public function add_extra_js($customJS,$module,$action){
			if(is_array($customJS[$module])){
				if(!is_null($customJS[$module][$action])){
					$this->extra_js_code = $customJS[$module][$action];
				}//end if
			}//end if
		}//end function
		
		private function get_default_action(){
			$default_action = "list";			
			if($this->module != NULL && $this->module  != 'users'){
				$settings = $this->utils->get_settings($this->module);
				if($settings['first_view'] == NULL){
					$has_categories = $this->utils->module_has_categories($this->module);
									
					if($settings['show_categories'] == 1 && $has_categories == true){
						$default_action = "pages";
					}//end if
				} else {
					$default_action = $settings['first_view'];
				}//end if
			}//end if
				
			if($this->module == "users"){
				$default_action = "groups";
			}//end if
			
			$this->default_action = $default_action;
			
			//$this->action = $_REQUEST['action'];
			if($this->action == NULL){
				$this->action = $default_action;
			}//end if
		}//end function
		
		public function get_module_attachments_extra(){
			if(file_exists("includes/view/common/attachments_extras/".$this->module.".buttons.php")){
				$filepath = "includes/view/common/attachments_extras/".$this->module.".buttons.php";
				return $filepath;
			} else {
				return false;
			}//end if
		}//end function
		
		public function build_page(){			
			if(file_exists("includes/view/".$this->module.".".$this->action.".php")){
				return "includes/view/".$this->module.".".$this->action.".php";
			} else {
				if(file_exists("includes/view/generic.".$this->action.".php")){
					return "includes/view/generic.".$this->action.".php";
				} else {
					return false;
				}//end if
			}//end if			 
		}//end function
		
		public function create_headers($csslist,$script_list){
			$code = $this->create_css_includes($csslist);
			if($this->extra_js_code != NULL){
				$script_list .= ",".$this->extra_js_code;
			}//end if
			$code .= $this->create_js_includes($script_list);
			return $code;
		}//end function
		
		private function create_css_includes($csslist){
			$csslist = explode(",",$csslist);
			$css_base = __SERVERPATH__."admin/css/";
			for($i = 0; $i < sizeof($csslist); $i++){
				$css = $csslist[$i];
				$code .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"".__SERVERPATH__."admin/css/".$css."\" />\n";
			}//end for
			return $code;
		}//end function
		
		private function create_js_includes($script_list){
			$scripts = explode(",",$script_list);
			$script_base = __SERVERPATH__."admin/includes/js/";
			for($i = 0; $i < sizeof($scripts); $i++){
				$jsscript = $scripts[$i];
				switch($jsscript){
					case "jquery":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/".__JQUERY_VERSION__."/jquery-".__JQUERY_VERSION__.".min.js\"></script>\n";
						/*$line .= "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/interface/".__INTERFACE_VERSION__."/interface.js\"></script>\n";*/
						$code .= $line;
						break;
					/*case "tiny_mce":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."tiny_mce/".__TINYMCE_VERSION__."/tiny_mce.js\"></script>\n";
						$code .= $line;
						break;*/
					case "lightbox":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.lightbox.js\"></script>\n";
						$line .= "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.color.packed.js\"></script>\n";
						$code .= $line;
						break;
					case "autocomplete":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.autocomplete.pack.js\"></script>\n";
						$code .= $line;
						break;
					case "jquery-ui":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery-ui-1.8.23.custom.min.js\"></script>\n";
						$code .= $line;
						break;						
					case "swfobject":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."swfobject/".__SWFOBJECT_VERSION__."/swfobject.js\"></script>\n";
						$code .= $line;
						break;
					/*case "date_picker":
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/date.js\"></script>\n";
						$line .= "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base."jquery/plug-ins/jquery.datePicker2.js\"></script>\n";
						$code .= $line;
						break;*/
					default:
						$line = "<script language=\"javascript\" type=\"text/javascript\" src=\"".$script_base.$jsscript."\"></script>\n";
						$code .= $line;
				}//end switch
			}//end for
			return $code;
		}//end function
		
		public function build_table($struct, $caption){
		//to finish
		}//end function
		
		public function show_steps(){
			if($_REQUEST['action'] == NULL 
				&& $_REQUEST['category'] == NULL 
				&& $this->module != "users"
				&& $this->module != "homepage"
				&& $this->module != "comments"
				){
				//echo "<p class=\"bigfont\">Choose the sub-category from the list below, into which you want to place an article.</p>";
			}//end if
		}//end function
		
		private function create_group_path($class_parser,$link_counter){
			$data = array();
			$html = NULL;
			$gid = $_REQUEST['gid'];
			if($gid != NULL){
				$html = "<li><a href=\"do.php?module=".$this->module."&amp;action=groups\">".__GROUPS__."</a></li>\n";
				$link_counter++;
				$group_name = $class_parser->get_group($gid);			
				switch($this->module){
					case "users":
						$group_name = $group_name['users_group_name'];
						break;
					default:
						$group_name = $group_name['group_name'];
				}//end switch
				$html .= "<li><a href=\"do.php?module=".$this->module."&amp;action=list&amp;gid=".$gid."\">".$group_name."</a></li>\n";
				$link_counter++;
			}//end if
			$data['html'] = $html;
			$data['links'] = $link_counter;
			return $data;
		}//end function
		
		private function create_category_path($class_parser,$link_counter){
			$data = array();
			$html = NULL;
			$category = $_REQUEST['category'];
			
			if($this->default_action == "pages"){
				$cat_list_action = "pages";
			} else {
				$cat_list_action = "categories";
			}//end if
									
			if($category != NULL){
				if($category != "uncategorized"){						
					$category_data = $class_parser->get_category($category);
					if($category_data['category_father_id'] != 0){
						/*$f_category_data = $class_parser->get_category($category_data['category_father_id']);
						$f_category_name = $f_category_data['category_name'];
						$html .= "<li><a href=\"do.php?module=".$this->module."&amp;action=pages&amp;category=".$category_data['category_father_id']."\">".$f_category_name."</a></li>\n";
						$link_counter++;*/
						$this->build_categories_path($class_parser,$category);
						$html .= $this->cat_path;
						$link_counter += $this->cat_path_counter;
					}//end if
					$category_name = $category_data['category_name'];
					$html .= "<li><a href=\"do.php?module=".$this->module."&amp;action=".$cat_list_action."&amp;category=".$category."\">".$category_name."</a></li>\n";
					$link_counter++;
				} else {
					
					$html = "<li><a href=\"do.php?module=".$this->module."&amp;action=".$cat_list_action."\">".__CATEGORIES__."</a></li>\n";
					$link_counter++;					
					$html .= "<li><a href=\"do.php?module=".$this->module."&amp;action=".$cat_list_action."&amp;category=".$category."\">Uncategorized</a></li>\n";
					$link_counter++;
				}//end if
			}//end if
			$data['html'] = $html;
			$data['links'] = $link_counter;
			return $data;
		}//end function
		
		private function build_categories_path($class_parser,$category){
			$this->cat_path = "";
			$this->cat_path_counter = 0;
			$this->build_category_path($class_parser,$category);
		}//end function
		
		private function build_category_path($class_parser,$category){				
			//check if exists a sub category with this id
			$category_data = $class_parser->get_category($category);
			if($this->default_action == "pages"){
				$cat_list_action = "pages";
			} else {
				$cat_list_action = "categories";
			}//end if
			if($category_data['category_father_id'] != "0"){
				$f_category = $class_parser->get_category($category_data['category_father_id']);
				$f_category_name = $f_category['category_name'];
				$this->cat_path = "<li><a href=\"do.php?module=".$this->module."&amp;action=".$cat_list_action."&amp;category=".$category_data['category_father_id']."\">".$f_category_name."</a></li>\n".$this->cat_path;
				$this->cat_path_counter++;
				$this->build_category_path($class_parser,$category_data['category_father_id']);
				return $code;
			} else {		
				return false;
			}//end if
		}//end function
		
		public function get_page_options(){
			$html = "";
			//Add settings
			$usersClass = $this->utils->call("users");
			$udata = $usersClass->get_user();
			$gid = $udata['user_group_id'];
			if($gid == 1){
				$html .= "<ul class=\"toright\">\n";
				$html .= "<li class=\"settings-btn\"><a href=\"do.php?module=".$this->module."&amp;action=settings\">".__SETTINGS__."</a></li>\n";
				$html .= "</ul>\n";	
			}//end if
			//Additional functions
			/*$html .= "<ul class=\"toright\">\n";
			$content = "";
			$category = $_REQUEST['category'];
			switch($this->action){
				case "categories":
				case "groups":
				case "pages":
					$content = "<li><a href=\"do.php?module=".$this->module."&amp;action=list\">Show all items</a></li>\n";
					break;
				case "list":
					if($category != NULL){
						$content = "<li><a href=\"do.php?module=".$this->module."&amp;action=list\">Show all items</a></li>\n";
					}//end if
					break;
			}//end switch
			$html .= $content;
			$html .= "</ul>\n";	*/				
			if($html != NULL){
				return $html;
			} else {
				return NULL;
			}//end if
		}//end function
		
		public function get_sent_variables($prefix = "&amp;"){
			//read the get
			$link = $prefix;
			$avoided_vars = array("action","id","module","step");
			$i = 0;
			foreach($_GET as $key => $value){	
				if(!in_array($key,$avoided_vars)){	
					if($value != NULL){				
						if($i != 0){
							$link .= "&amp;".$key."=".$value;
						} else {
							$link .= $key."=".$value;					
						}//end if
						$i++;
					}//end if
				}//end if				
			}//end foreach
			if(substr($link,-5) == "&amp;"){
				$link = substr($link,0,-5);
			}
			return $link;
		}//end function
		
		public function get_breadcrumbs(){
			//Check if exists a class for this module
			$params = array("module" => $module);			
			if(file_exists("classes/".$this->module.".class.php")){			
				${$this->module} = $this->utils->call($this->module,$params);				
			} else {
				${$this->module} = $this->utils->call("ewrite",$params);
			}//end if
			$module_data = $this->utils->get_module($this->module);
			$module_table = $module_data['module_table'];
			$module_field = $module_data['module_field'];
			
			$settings = $this->utils->get_settings($this->module);
			if($settings['first_view'] == NULL){
				$has_categories = $this->utils->module_has_categories($this->module);
								
				if($settings['show_categories'] == 1 && $has_categories == true){
					$default_action = "pages";
				}//end if
			} else {
				$default_action = $settings['first_view'];
			}//end if
			
			//Init the list 		
			$html = "<ul>\n";
			//Fill with crumbs
			//Show the current module
			$html .= "<li><a href=\"do.php?module=".$this->module."&amp;action=".$default_action."\">".$module_data['module_label']."</a></li>\n";
			//Count the links 
			$links = 1;
			//echo $this->action;
			switch($this->action){
				//List of elements
				case "list":
					if($this->module != 'users'){					
						$cats = $this->create_category_path(${$this->module},$links);
						$html .= $cats['html'];
						$links = $cats['links'];
					} else {
						$groups = $this->create_group_path(${$this->module},$links);
						$html .= $groups['html'];
						$links = $groups['links'];
					}//end if					
					break;
				//Pages view
				case "pages":
				case "select":
				case "newcategory":
				case "getcategory":
				case "sortmodule":
				case "delete":				
					$cats = $this->create_category_path(${$this->module},$links);
					$html .= $cats['html'];
					$links = $cats['links'];
					break;
				case "activationlist":
					$html .= "<li><a href=\"do.php?module=".$this->module."&amp;action=activationlist\">".__ACTIVATION_LIST__."</a></li>\n";
					$links++;
					break;
				//View of an element
				case "view":	
				case "preview":			
				case "addfile":
				case "makehomepageimg":
				case "delfile":
					$cats = $this->create_category_path(${$this->module},$links);
					$html .= $cats['html'];
					$links = $cats['links'];
					//Add the item title in the links
					$id = $_REQUEST['id'];
					if(method_exists(${$this->module},"get_post") && !method_exists(${$this->module},"get_cms_headers")){
						$post = ${$this->module}->get_post($id);															
						$post_title = $post[$module_field.'_title'];
						$post_title = $this->utils->trunktext($post_title,50);
					} else if(method_exists(${$this->module},"get_user_data") && !method_exists(${$this->module},"get_cms_headers")){						
						$post = ${$this->module}->get_user_data($id);
						$post_title = $this->utils->trunktext($post[$module_field.'_username'],50);
					} else if(method_exists(${$this->module},"get_cms_headers")){					
						$post = ${$this->module}->get_cms_headers(array("id" => $id));
						$post_title = $post['label'];
					}//end if
					if($post_title == NULL){
						$post_title = "<em>View</em>";
					}//end if
					$html .= "<li class=\"post_title\"><a href=\"do.php?module=".$this->module."&amp;action=view&amp;category=".$_REQUEST['category']."&amp;id=".$id."\">".$post_title."</a></li>\n";
					$links++;
					if($this->action == "addfile"){
						$html .= "<li><a href=\"#\">".__UPLOAD_NEW_LEGEND__."</a></li>";
						$links++;
					}//end if
					break;
				case "get":		
				case "publish":			
					$cats = $this->create_category_path(${$this->module},$links);
					$html .= $cats['html'];
					$links = $cats['links'];
					//Add the item title in the links
					$id = $_REQUEST['id'];	
					if(method_exists(${$this->module},"get_post")){
						$post = ${$this->module}->get_post($id);
						$post_title = $this->utils->trunktext($post[$module_field.'_title'],50);
					} else if(method_exists(${$this->module},"get_user_data")){
						$post = ${$this->module}->get_user_data($id);
						$post_title = $this->utils->trunktext($post[$module_field.'_username'],50);
					}//end if
					if($post_title == NULL){
						$post_title = "<em>untitled</em>";
					}//end if
					$html .= "<li class=\"post_title\"><a href=\"do.php?module=".$this->module."&amp;action=view&amp;category=".$_REQUEST['category']."&amp;id=".$id."\">".$post_title."</a></li>\n";
					$links++;
					if($this->action == "get"){
						$html .= "<li>Saved</li>\n";
					} else {
						$html .= "<li>Published</li>\n";
					}//end if
					$links++;					
					break;					
				case "new":	
					if($this->module == "users"){
						$groups = $this->create_group_path(${$this->module},$links);
						$html .= $groups['html'];
						$links = $groups['links'];
					} else {
						$cats = $this->create_category_path(${$this->module},$links);
						$html .= $cats['html'];
						$links = $cats['links'];
					}//end if
					
					//Add the item title in the links
					$id = $_REQUEST['id'];						
					if($id == NULL){
						$html .= "<li>New</li>\n";
						$links++;
					} else {
						if(method_exists(${$this->module},"get_post") && !method_exists(${$this->module},"get_cms_headers")){
							$post = ${$this->module}->get_post($id);															
							$post_title = $post[$module_field.'_title'];
							$post_title = $this->utils->trunktext($post_title,50);
						} else if(method_exists(${$this->module},"get_user_data") && !method_exists(${$this->module},"get_cms_headers")){						
							$post = ${$this->module}->get_user_data($id);
							$post_title = $this->utils->trunktext($post[$module_field.'_username'],50);
						} else if(method_exists(${$this->module},"get_cms_headers")){					
							$post = ${$this->module}->get_cms_headers(array("id" => $id));
							$post_title = $post['label'];
						}//end if
						if($post_title == NULL){
							$post_title = "<em>untitled</em>";
						}//end if
						$html .= "<li class=\"post_title\"><a href=\"do.php?module=".$this->module."&amp;action=view&amp;category=".$_REQUEST['category']."&amp;id=".$id."\">".$post_title."</a></li>\n";
						$links++;
						$html .= "<li>Edit</li>\n";
						$links++;
					}//end if
					break;
				default:				
					if(method_exists(${$this->module},"view_".$this->action)){					
						$html .= ${$this->module}->{"view_".$this->action}();
						$links++;
					}//end if
			}//end switch
			//Closing the list
			$html .= "</ul>\n";
			if($links > 0){
				return $html;
			} else {
				return NULL;
			}//end if
		}//end function
		
		public function show_message(){
			$message_code = $_REQUEST['message'];
			$message_encryption = $_REQUEST['menc'];
			$_REQUEST['message'] = NULL;
			if($message_code != NULL){
				if($message_encryption == NULL){
					switch($message_code){
						case "postpublished":
							$message = "Post successfully published";
							break;
						case "categorycreated":
							$message = "Category successfully created/modified";
							break;
						case "postssorted":
							$message = "Posts successfully sorted";
							break;
						case "categoriessorted":
							$message = "Categories successfully sorted";
							break;
						case "itemssorted":
							$message = "Items successfully sorted";
							break;
						case "postdeleted":
							$message = "Post successfully deleted";
							break;
						case "categorydeleted":
							$message = "Category successfully deleted";
							break;
						case "fileuploaded":
							$message = "File successfully uploaded";
							break;
						case "filedeleted":
							$message = "File successfully deleted";
							break;
						case "itemsaved":
							$message = "Item successfully saved";
							break;
						case "settingssaved":
							$message = __SETTINGS_SAVED__;
							break;
					}//end switch
				} else {
					$message = base64_decode(urldecode($message_code));
				}//end if
				$msg = "<div id=\"sys_message\">".$this->utils->get_message($message,"ok")."</div>";
				$msg .= '<script language="javascript" type="text/javascript">
							$(document).ready(function(){
								setTimeout(function(){ $(\'#sys_message\').fadeOut(\'slow\'); }, 3000);
							});					
						</script>';
				echo $msg;
			}//end if
		}//end function 
		
		public function print_breadcrumbs(){
			$html = "";
			$content = $this->get_breadcrumbs();
			$content .= $this->get_page_options();
			if($content != ""){
				$html = "<div id=\"breadcrumbs\">\n";
				$html .= $content;
				$html .= "</div>\n";
			}//end if
			return $html;
		}//end function
	}//endclass
?>