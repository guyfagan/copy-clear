<?php
	require_once('ewrite.class.php');

	class ewrite_ext extends ewrite{
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
		}//endconstructor	
		
		/*
		//Return the data of the id post passed
		public function get_post(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$pid = $params[0];
				$this->pid = $pid;	
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$query = "SELECT ".$this->t."posts.*, ".$this->t."users.user_username, users.user_email, UNIX_TIMESTAMP(".$this->t."posts.post_date) AS post_date, UNIX_TIMESTAMP(".$this->t."posts.post_date_end) AS post_date_end, module_name AS folder
					  FROM (".$this->t."posts, ".$this->t."modules)
					  LEFT JOIN ".$this->t."users
					  ON ".$this->t."users.user_id = ".$this->t."posts.post_uid
					  WHERE module_name = post_module_id ";	
			if(isset($pid)){
				if(is_numeric($pid)){
					$query .= " AND post_id = ".$pid;
				} else {
					return false;	
				}//end if
			}//end if
			
			if(isset($permalink)){
				$permalink = mysql_real_escape_string($permalink);
				$query .= " AND post_permalink = '".$permalink."'";
			}//end if
			
			$query .= " LIMIT 1";
			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);	
				//Read the post categories
				////This may fix a problem, in case, take it out
				$this->module = $data['folder'];
				$this->pid = $data['post_id'];				
				$post_cats = $this->get_post_categories($this->pid);	
				if($post_cats !== false){
					$cids = array();
					for($i = 0; $i < sizeof($post_cats); $i++){
						array_push($cids,$post_cats[$i]['category_id']);
					}//end for
					$data['category_id'] = base64_encode(serialize($cids));
				} else {
					$data['category_id'] = NULL;			
				}//end if
				
	
				//These three fields are made only for the post editing
				$data['post_date_day'] = date("j",$data['post_date']);
				$data['post_date_month'] = date("n",$data['post_date']);	
				$data['post_date_year'] = date("Y",$data['post_date']);				
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		//Return the name of the category passed
		public function get_category(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$cid = $params[0];	
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			$query = "SELECT ".$this->t."categories.*, ".$this->t."modules.* 
					  FROM ".$this->t."categories, ".$this->t."modules 
					  WHERE category_module_id = module_id ";
			if(isset($cid)){
				if(is_numeric($cid)){
					$query .= " AND category_id = ".$cid;	
				} else {
					return false;	
				}//end if
			}
			
			if(isset($permalink)){
				$permalink = mysql_real_escape_string($permalink);
				$query .= " AND category_permalink = '".$permalink."'";
			}
			
			$query .= " LIMIT 1";
			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){				
				$data = $this->utils->get_result_array($result,true);			
				return $data;
			} else {
				return false;
			}//end if	
		}//end function
		*/
	}//end class