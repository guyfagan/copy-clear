<?php
	class notes{
		private $utils;//utils class
		private $t;//prefix before each tablename
		private $nid;//note id
		private $uid;//userid
		private $pid;//post id		
		public $module; //module
		public $module_id; //Module id number		
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->module = "ewrite";			
			$this->utils->read_params($this,$params);	
			$module_data = $this->utils->get_module($this->module);
			$this->module_id = $module_data['module_id'];		
		}//endconstructor	
		
		public function get_post_notes($id,$module){
			if(!is_numeric($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
			} else {
				$module_id = $module;
			}//end if
			$query = "SELECT * FROM  notes WHERE note_ref_id = ".$id." AND note_module_id = ".$module_id." ORDER BY note_id DESC";
			$result = @mysql_query($query,$this->utils->db);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = @mysql_num_rows($result);
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_note_id(){
			return $this->nid;
		}//end function
		
		public function get_note($nid){
			$query = "SELECT * FROM notes WHERE note_id = ".$nid;
			$result = @mysql_query($query,$this->utils->db);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = @mysql_num_rows($result);
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function save_note($id,$module,$message){
			if(!is_numeric($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
			} else {
				$module_id = $module;
			}//end if
			$user_id = $this->utils->get_uid();
			$message = $this->utils->strtodb($message);
			if($_REQUEST['nid'] == NULL){
				$query = "INSERT INTO notes
						  (note_message, note_ref_id, note_module_id, note_uid)
						  VALUES
						  ('".$message."',".$id.",".$module_id.",".$user_id.")";
			} else {
				$query = "UPDATE notes SET note_message = '".$message."' WHERE note_id = ".$_REQUEST['nid'];			
			}//end if
					
			$result = @mysql_query($query,$this->utils->db);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$this->nid = @mysql_insert_id();
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function delete_note($nid){
			$query = "DELETE FROM notes WHERE note_id = ".$nid;
			$result = @mysql_query($query,$this->utils->db);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
	}//end class
?>