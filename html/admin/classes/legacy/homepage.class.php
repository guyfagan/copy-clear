<?php
	class homepage{
	
		private $utils;//utils class
		private $t;//prefix before each tablename
		private $uid;//userid
		private $pid;//post id	
		private $cid;//category id	
		private $fcid;//father category id
		private $gid;//group id
		private $reg_errors_data;
		private $site_address;
		public $module; //module
	
		//Constructor
		function __construct($utils,$params = array()){			
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->module = "ewrite";
			$this->utils->read_params($this,$params);			
			$module_data = $this->utils->get_module($this->module);
			//$this->force_module($this->module);
			$this->module_id = $module_data['module_id'];
			$this->site_address = "http://www.thirdageireland.ie/";
		}//endconstructor
		
		public function get_sections(){
			//query database for section names and display them
			$query = "SELECT * FROM sections";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);				
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);							
				return $data;
			} else {
				return false;
			}//endif	
		}//end function
		
		public function get_section_name(){
			return $this->get_sections();
		}//end function
		
		public function get_section($id){
			$query = "SELECT * FROM sections WHERE section_id = ".$id;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);				
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);							
				return $data;
			} else {
				return false;
			}//endif	
		}//end function
		
		public function get_category($id){
			$data = $this->get_section($id);
			$data = $this->utils->change_array_prefix($data,"category_","section_");
			return $data;
		}//end function
		
		public function get_categories(){
			$data = $this->get_sections();
			$data = $this->utils->change_array_prefix($data,"category_","section_");
			return $data;
		}//end function
		
		public function get_section_posts($id,$lang_id = NULL){
			if($lang_id != NULL){
				$lang = $this->utils->get_language($lang_id);
				$lang_id = $lang['language_id'];
			}//end if
			$query = "SELECT posts.post_id, posts.post_title, posts.post_subtitle, posts.post_module_id, 
					  posts.post_lang, categories_rel_category_id, posts.post_abstract
					  FROM (posts, sections_posts) 
					  LEFT JOIN categories_rel
					  ON posts.post_id = categories_rel.categories_rel_post_id
					  WHERE posts.post_id = sections_posts.post_id 
					  AND sections_posts.section_id = ".$id;
			if($lang_id != NULL){
				$query .= " AND posts.post_lang = ".$lang_id;
			}
			$query .= " GROUP BY posts.post_id
						ORDER BY sections_posts.post_order ASC";
	
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);				
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);							
				return $data;
			} else {
				return false;
			}//endif	
		}//end function
		
		public function delete_section_posts($section_id,$lang_id = NULL){
			$query = "DELETE sections_posts.* FROM sections_posts, posts 
					  WHERE sections_posts.section_id = ".$section_id."
					  AND sections_posts.post_id = posts.post_id";
			if($lang_id != NULL){
				$query .= " AND posts.post_lang = ".$lang_id;
			}//end if
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
		}//end function
		
		public function get_available_items(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$section_id = $params[0];				
				$exclude = $params[1];				
				$limit = $params[2];	
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			if($section_id != NULL){
				$section_data = $this->get_section($section_id);
				//get the section categories
				if($section_data['section_categories'] != NULL){
					$section_categories = unserialize($section_data['section_categories']);
				}//end if
				//get the modules and clean it from doublequotes
				$section_modules = str_replace('"','',$section_data['section_module_allow']);
				//turn it into an array
				$section_modules_list = explode(",",$section_modules);
				if(in_array("users",$section_modules_list)){
					if(!isset($output)){
						$output = array();
						$output['id'] = 'user_id';
						$output['label'] = 'user_firstname';
					}//end if
	
					//we need to read the data from the users_table
					$query = "SELECT ".$output['id']." AS id, ".$output['label']." AS label
							  FROM users, users_groups_rel
							  WHERE user_id = users_groups_rel_uid";
							if(isset($section_categories) && is_array($section_categories)){
								$query .= " AND users_groups_rel_gid IN (".implode(",",$section_categories).")";
							}//end if
							if ($exclude != "") { 
								$query.= " AND user_id NOT IN(".$exclude.")"; 
							}//end if
						  	if(isset($limit) && $limit != NULL){
								$query .= " LIMIT ".$limit;
						  	}//end if
				} else {
					if(!isset($output)){
						$output = array();
						$output['id'] = 'post_id';
						$output['label'] = 'post_title';
					}//end if
					
					//we need to read the data from the users_table
					$query = "SELECT ".$output['id']." AS id, ".$output['label']." AS label
							  FROM posts, modules_items_order
							  WHERE post_id = module_item_order_item_id";
					if(isset($section_categories) && is_array($section_categories)){
						$query .= " AND module_item_order_father_id IN (".implode(",",$section_categories).")";
					}//end if
					if ($exclude != "") { 
						$query.= " AND post_id NOT IN(".$exclude.")"; 
					}//end if
					if(isset($allowed)){
						$query .= " post_module_id IN(".$section_modules.") ";	
					}
					if(isset($limit) && $limit != NULL){
						$query .= " LIMIT ".$limit;
					}//end if
	
				}//end if
				
				echo $query;
				
				$result = @mysql_query($query,$this->utils->db);
				$errors = $this->utils->error($result,__LINE__,get_class($this));	
				if($errors === false){
					$num = @mysql_num_rows($result);				
					if($num > 0){							
						$data = $this->utils->get_result_array($result,false);							
						return $data;
					} else {
						return false;
					}//endif	
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_available_posts(){
			
			$params = func_get_args();
			if(!is_array($params[0])){
				$allowed = $params[0];					
				$exclude = $params[1];				
				$limit = $params[2];	
				$sortby = $params[3];
				$lang_id = $params[4];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			if($section_id != NULL){
				$section_data = $this->get_section($section_id);
				//get the section categories
				if($section_data['section_categories'] != NULL){
					$section_categories = unserialize($section_data['section_categories']);
				}//end if		
			}//end if
						
			if($lang_id != NULL){
				$lang = $this->utils->get_language($lang_id);
				$lang_id = $lang['language_id'];
			}//end if
			$query = "SELECT post_id, post_title, post_module_id, post_lang 
					  FROM posts,modules_items_order 
					  WHERE post_module_id IN(".$allowed.")
					  AND post_id = module_item_order_item_id ";
			if ($exclude != "") { 
				$query.= " AND post_id NOT IN(".$exclude.")"; 
			}//end if			
			if(isset($section_categories) && is_array($section_categories)){
				$query .= " AND module_item_order_father_id IN (".implode(",",$section_categories).")";
			}//end if
			if($lang_id != NULL){
				$query .= " AND post_lang = ".$lang_id;
			}//end if
			$query .= " AND post_status = 1";
			if($sortby != NULL){ 
				$query .= " ORDER BY ".$sortby; 
			}//end if
			if($limit != NULL){
				$query .= " LIMIT ".$limit; 
			}//end if
			
			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);				
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);							
				return $data;
			} else {
				return false;
			}//endif	
		}//end function
		
		public function get_section_posts_string($id){
			$query = "SELECT * FROM sections_posts WHERE section_id = ".$id;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){
				$data = "";
				while($row = @mysql_fetch_array($result)){
					$data .= $row['post_id'].",";
				}//end while
				if(substr($data,-1) == ","){
					$data = substr($data,0,-1);
				}//endif
				return $data;
			} else {
				return NULL;
			}//end if			
		}//end function
		
		public function save_sorting(){
			$id = $_GET['id'];
			$lang = $_GET['lang'];
			if($lang != NULL){
				$lang = $this->utils->get_language($lang);
				$lang_id = $lang['language_id'];
			}//end if
			
			$count = sizeof($_GET['listItem']);
			$items = $_GET['listItem'];
			$delete = $this->delete_section_posts($id,$lang_id);		
			if(sizeof($items) > 0){
				$i = 0;
				foreach ($_GET['listItem'] as $position => $item){
					$query = "INSERT INTO sections_posts (section_id, post_id,post_order) VALUES (".$id.",".$item.",".$i.")";					
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));	
					$i++;
				}//end foreach
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function has_hp_image($module,$pid){
			if(!is_numeric($module)){
				$md = $this->utils->get_module($module);
				$module = $md['module_id'];
			}//end if		
			$hp_label = "homepage";
			$query = "SELECT * FROM attachments 
					  WHERE attachment_title = '".$hp_label."'
					  AND attachment_ref_id = ".$pid."
					  AND attachment_ref_module_id = ".$module;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num == "0"){
				return false;
			} else {
				return true;
			}//end if	
		}//end function 
	}//end class
?>