<?php
	require_once('users.class.php');	

	class users_ext extends users{
		
		public $htpasswords;
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->htpasswords = array();		
		}//endconstructor
		
		public function new_user(){
			if($_REQUEST['id'] == NULL){
				//Check first if the username is already used
				if($this->user_exists($_POST['username']) == true){
					return false;
				}//end if
				//Check if the email is already used
				if($this->email_exists($_POST['email']) == true){
					return false;
				}//end if	
			}//end if		
			//Check if the password are identical
			if($_POST['password'] != $_POST['conf_password']){
				return false;
			}//end if			
			
			if($this->save_user()){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function save_user(){	
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				$id = $_REQUEST['id'];	
			}//end if
			
			//These fields will be omitted 
			$hidden_fields = array('action','a','step','submit','id','conf_password','group_id');
			//$hidden_fields = $this->utils->merge_addfields_to_hiddens($this->module,'new',$hidden_fields);
			$_POST['active'] = 1;
			//create the insert sql script
			if($id == NULL){
				$sql = $this->utils->build_sql_fields("user","INSERT","POST",$hidden_fields);
				$query = "INSERT INTO ".$this->t."users ".$sql;	
			} else {
				$this->uid = $id;
				array_push($hidden_fields,"password");
				$sql = $this->utils->build_sql_fields("user","UPDATE","POST",$hidden_fields);
				if($_POST['password'] != NULL){
					$sql .= ", user_password = '".sha1($_POST['password'])."'";
				}//end if
				$query = "UPDATE ".$this->t."users SET ".$sql." WHERE ".$this->t."users.user_id = ".$this->uid;
			}//end if				
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));
			if($id == NULL){				
				$this->uid = @mysql_insert_id();
			}//end if			
			//Create relation between groups and user
			if($_POST['group_id'] != NULL){
				$this->create_user_group_relation($this->uid,$_POST['group_id']);
			}//end if
			
			/* META SUPPORT */			
			$this->meta->set_meta_id($this->uid);	
			$table_fields = $this->utils->get_table_fields(array("module" => $this->module,"field_prefix" => "user", "show_all" => true));			
			array_walk($table_fields,array($this->utils,'clean_prefix'),$table_prefix);		
			$this->meta->auto_add_meta(array("avoid" => $table_fields));
			//add the raw encoded password
			if($_POST['password'] != NULL){
				$meta_password = base64_encode(sha1($_POST['password'],true));
				$this->meta->add_meta(array("label" => "user_sha1_raw_pass", "value" => $meta_password));
			}//end if
			/* END META SUPPORT */
			if($this->settings['htpasswd_enabled'] == 1 && trim($this->settings['htpasswd_folders']) != NULL){
				$ht_folders = $this->settings['htpasswd_folders'];
				$ht_folders = explode("\n",trim($this->settings['htpasswd_folders']));				
				$this->set_htpasswords(array("path" => $ht_folders));
				$this->update_htpasswords();
			}//end if
			return true;
		}//end function
		
		public function update_htpasswords(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			if(defined("__SERVERPATH__") && !isset($basepath)){
				$basepath = __SERVERPATH__;			
			}//end if
			
			//get the users
			$params = array();
			if(isset($gid)){
				$params['gid'] = $gid;	
			}//end if
			$params['active'] = true;
			$params['meta'] = true;
			$users_list = $this->get_users($params);
			if($users_list === false){
				return false;	
			}//end if
			
			if(sizeof($this->htpasswords) > 0){
				foreach($this->htpasswords as $value){
					if($value != ""){
						$htpasswd = "../".trim($value)."/.htpasswd";					
						$file = fopen($htpasswd,"w+");
						$file_content = "";
						for($i = 0; $i < sizeof($users_list); $i++){
							if($users_list[$i]['user_sha1_raw_pass'] != NULL){
								$file_content .= $users_list[$i]['user_username'].":{SHA}".$users_list[$i]['user_sha1_raw_pass']."\n";
							}//end if
						}//end for i				
						fwrite($file,$file_content);
						fclose($file);
					}//end if
				}//end foreach
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function set_htpasswords(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(isset($path)){
				if(is_string($path)){
					array_push($this->htpasswords,$path);
					return true;
				} else if(is_array($path)){
					$this->htpasswords = $path;
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
	}//end class
?>