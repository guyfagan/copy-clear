<?php
	class xmlforms{
		
		protected $utils;//utils class	
		protected $xmlfile; //xml file for form schema
		protected $file_to_include; //Is an array, contains all the files should be included in the result page
		protected $field_prefix; //in edit mode it used for the beginning of the fields name
		protected $data_src;//It's an array where are stored all the data in edit mode
		protected $exec_func;//in case it should be executed a function
		protected $avoid_field_list; //a list to fields to avoid
		protected $current_module;
		protected $module;
		protected $xmlData;
		protected $curret_nodename;
		protected $date_format;
		protected $date_fields;
			
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;		
			$this->utils->read_params($this,$params);
			$this->avoid_field_list = array();
			$this->date_format = "d/m/Y";
			$this->date_fields = array();			
		}//endconstructor	
		
		public function set_module($cm){
			$this->current_module = $cm;	
		}//end function
		
		private function find_current_module(){
			if(isset($GLOBALS['module'])){
				$module_id = $this->utils->get_module($GLOBALS['module']);
				$this->module = $module_id['module_id'];				
			}//end if
		}//end function
		
		public function set_date_fields($dates_array){
			$this->date_fields = $dates_array;
		}//end if
		
		public function set_data_src($data_src, $field_prefix,$avoid_field_list = NULL){
			$this->data_src = $data_src;
			$this->avoid_field_list = $avoid_field_list;
			if(!is_array($this->avoid_field_list)){
				$this->avoid_field_list = explode(",",$this->avoid_field_list);
			}//end if
			$this->field_prefix = $field_prefix;
		}//end function
		
		public function build_xmlfields($xmlfile){	
			$this->find_current_module();		
			$this->xmlfile = $xmlfile;
			$xmlFileData = file_get_contents($this->xmlfile);
			$this->xmlData = new SimpleXMLElement($xmlFileData);
			$this->file_to_include = array();
			$checkbox_list = array();
			
			foreach($this->xmlData->xmlform->children() as $node){	
				$nodename = $node->getName();
				$this->current_nodename;
				switch($nodename){
					case "startfieldset":
						$fieldset_html = "<fieldset";
						foreach($node->attributes() as $attribute => $value){	
							switch($attribute){
								case "legend":
									if(defined($value)){
										$legend_label_txt = constant($value);
									} else {
										$legend_label_txt = $value;
									}//end if
									$legend_html = "<legend>".$legend_label_txt."</legend>\n";
									break;
								case "id":
									$fieldset_html .= " id=\"".$value."\"";
									break;
								case "class":
									$fieldset_html .= " class=\"".$value."\"";
									break;
							}//end switch
						}//end foreach
						$fieldset_html = $fieldset_html.">\n".$legend_html;
						$xmlresult .= $fieldset_html;
						break;
					case "startdiv":
						$div_html = "<div";
						foreach($node->attributes() as $attribute => $value){	
							switch($attribute){
								case "id":
									$div_html .= " id=\"".$value."\"";
									break;
							}//end switch
						}//end foreach
						$div_html = $div_html.">\n";
						$xmlresult .= $div_html;
						break;
					case "div":
						$div_html = "<div";
						foreach($node->attributes() as $attribute => $value){	
							switch($attribute){
								case "id":
									$div_html .= " id=\"".$value."\"";
									break;
								case "class":
									$div_html .= " class=\"".$value."\"";
									break;
							}//end switch
						}//end foreach
						if(defined($node[0])){
							$node_content = constant($node[0]);
						} else {
							$node_content = $node[0];
						}//end if
						$div_html = $div_html.">".$node_content."</div>\n";
						$xmlresult .= $div_html;
						break;
					case "import":
						$url = $node->attributes();	
						array_push($this->file_to_include,'includes/view/forms/'.$url);				
						break;
					case "endfieldset":
						$xmlresult .= "</fieldset>\n";
						break;
					case "enddiv":
						$xmlresult .= "</div>\n";
						break;
					default:
						$label = array();
						$optional = array();
						$this->exec_func = false;
						//Read all the attributes		
						foreach($node->attributes() as $attribute => $value){			
							switch($attribute){
								case "type":
									$type = $value;
									break;							
								case "label":
									$label['name'] = $value;
									break;
								case "labelposition":
									$label['position'] = $value;
									break;
								case "labelclass":
									$label['class'] = $value;
									break;
								case "labelhidden":
									$label['hidden'] = $value;
									break;
								case "labelid":
									$label['id'] = $value;
									break;
								case "query":
									$params = (string)$value;													
									break;
								case "function":
									$params = (string)$value;
									$this->exec_func = true;
									break;
								default:
									$optional[$attribute] = $value;
							}//endswitch
						}//end foreach	
						
						//check if exists the data source array					
						if(sizeof($this->data_src) > 0){							
							//check if the name of the node is in the source data array
							$key = $this->field_prefix.$nodename;																
							if(array_key_exists($key,$this->data_src) && $type != "hidden" && $type != "checkbox" && $type != "radio" && !in_array($nodename,$this->avoid_field_list)){										
								//echo $this->data_src[$key];								
								$optional['value'] = $this->data_src[$key];								
								//echo $optional['value'];
							} else if(array_key_exists($nodename,$this->data_src) && $type != "hidden" && $type != "checkbox" && $type != "radio" && !in_array($nodename,$this->avoid_field_list)){								
								$optional['value'] = $this->data_src[$nodename];	
							}//end if
						}//end if
				
						if(!isset($params) && ($type != "checkbox")){
							$params = $node;
						}//end if	
						
						if($type == "checkbox"){
							array_push($checkbox_list,$nodename);
						}//end if			
						
						$xmlresult .= $this->build_input($type,$nodename,$label,$optional,$params);
						unset($params);
				}//end switch
			}//end foreach
			if(sizeof($checkbox_list) > 0){
				$hidden_data = "<input type=\"hidden\" name=\"checkboxes_field_list\" id=\"checkboxes_field_list\" value=\"".urlencode(base64_encode(serialize($checkbox_list)))."\" />\n"; 
				$xmlresult = $hidden_data.$xmlresult;
			}//end if
			return $xmlresult;
		}//end function
		
		public function get_imported_files(){
			return $this->file_to_include;
		}//end function
		
		private function build_input($type, $name, $label = array(), $optional = array(), $param = NULL){
			$start_label = "\n<label for=\"".$name."\"";
			if($label['class'] != NULL){
				$start_label .= " class=\"".$label['class']."\"";
			}//end if
			if($label['id'] != NULL){
				$start_label .= " id=\"".$label['id']."\"";
			}//end if
			if($label['name'] != NULL){
				if(defined($label['name'])){
					$field_label = constant($label['name']);
				} else {
					$field_label = $label['name'];
				}//end if
			} else {
				$field_label = $name;
			}//endif
			if($type != "checkbox" && $type != "radio"){
				$start_label .= ">\n<span class=\"field_title\">".$field_label."</span>\n";
			} else {
				$start_label .= ">\n";
				$end_label = "<span class=\"field_title\">".$field_label."</span>\n";
			}//endif
			$end_label .= "</label>\n";
			
			//check which type of field
			switch($type){
				case "text":
				case "text[]":
				case "password":
				case "file":
				case "file[]":
					if($type == "file[]"){
						$input_name = $name."[]";	
						if(sizeof($optional) > 0){
							if($optional['id'] != NULL){
								$input_id = $optional['id'];
								$optional = $this->utils->array_cut('id',$optional);	
							} else {
								$input_id = $name;
							}//end if
						}//end if		
						$type = "file";			
					} else if($type == "text[]"){
						$input_name = $name."[]";	
						if(sizeof($optional) > 0){
							if($optional['id'] != NULL){
								$input_id = $optional['id'];
								$optional = $this->utils->array_cut('id',$optional);	
							} else {
								$input_id = $name;
							}//end if
						}//end if		
						$type = "text";	
					} else {
						$input_id = $name;
						$input_name = $name;
						/* Why I did the code below?*/
						/*if(is_numeric($optional['value'])){
							$optional['value'] = NULL;
						}//end if	*/				
					}//end if		
					if(substr($name,0,4) == "date" || in_array($name,$this->date_fields)){
						if($optional['value'] != NULL && strlen($optional['value']) > 2){
							$optional['value'] = date($this->date_format,$optional['value']);
						} else {
							$optional['value'] = NULL;
						}//end if						
					}//end if
					$input = "<input type=\"".$type."\" name=\"".$input_name."\" id=\"".$input_id."\"";	
					if($type == "password"){
						$optional['value'] = NULL;
					}//end if				
					if(sizeof($optional) > 0){
						$input .= $this->read_optional_attributes($optional);
					}//end if	
					$input .= " />\n";
					$input = $this->apply_label($type,$label,$input,$start_label,$end_label);
					break;
				case "textarea":
					$input = "<textarea name=\"".$name."\" id=\"".$name."\"";
					if(sizeof($optional) > 0){					
						$value_saved = $optional['value'];
						$optional = $this->utils->array_cut('value',$optional);						
						$input .= $this->read_optional_attributes($optional);
					} else {
						$value_saved = "";
					}//end if					
					$input .= ">".$value_saved."</textarea>\n";
					$input = $this->apply_label($type,$label,$input,$start_label,$end_label);
					break;
				case "select":
				case "select_forced":				
					$input = "<select name=\"".$name."\" id=\"".$name."\">\n";
											
					if(sizeof($optional) > 0){
						$value_saved = $optional['value'];						
						$optional = $this->utils->array_cut('value',$optional);										
						$input .= $this->read_optional_attributes($optional);					
					}//end if	
					
					//If the passed param is an object with xml structure inside, execute that code
					if(is_object($param) && $param != NULL){					
						//in this case there are subnodes that forms the options data								
						$i = 0;								
						foreach($param->option as $value){
							$input .= "<option value=\"".$param->option[$i]->attributes()."\"";
							if(isset($value_saved) && $param->option[$i]->attributes() == $value_saved){
								$input.= " selected=\"selected\"";
							}//endif
							//
							if(defined($value)){
								$optvalue = constant($value);
							} else {
								$optvalue = $value;
							}//end if
							$input .= ">".$optvalue. "</option>\n";
							$i++;
						}//end foreach
					//If the param is a string execute that code	
					} else if(is_string($param) && $param != NULL){						
						//If the select allow null values, add a null option
						if($type == "select"){
							$input .= "<option value=\"\">".__SELECT_CHOOSE__. "</option>\n";
						}//end if	
						if($type == "select"){
							$input .= '<option value=""></option>';
						}//end if							
						if($this->exec_func == false){
							//echo $value_saved."a <br />";
							//execute a query
							//That means that I've used the attribute "query" for executing a query
							if(isset($value_saved) && $value_saved != NULL){													
								$input .= $this->options_from_query($param,$value_saved,$name);
							} else {
								$input .= $this->options_from_query($param,NULL,$name);
							}//end if
						} else {					
							//Execute a function
							if(isset($value_saved) && $value_saved != 0){
								$input .= $this->{$param}($value_saved);
							} else {
								$input .= $this->{$param}();
							}//end if
						}//end if
					}//end if						
					$input .= "</select>\n";	
					if(($label['hidden'] != NULL && $label['hidden'] == false) || $label['hidden'] == NULL){		
						$input = $this->apply_label($type,$label,$input,$start_label,$end_label);
					}//end if			
					break;
				case "hidden":
					$input = "<input type=\"hidden\" name=\"".$name."\" id=\"".$name."\"";
					//var_dump($optional);
					if(sizeof($optional) > 0){
						$input .= $this->read_optional_attributes($optional);
					}//end if	
					$input .= " />\n";
					break;
				case "submit":
					$str_value = (string)$param;
					if(defined($str_value)){
						$field_label = constant($str_value);
					} else {
						$field_label = $str_value;
					}//end if					
					$input = "<input type=\"submit\" name=\"".$name."\" id=\"".$name."\" value=\"".$field_label."\"";
					if(sizeof($optional) > 0){
						$input .= $this->read_optional_attributes($optional);
					}//end if	
					$input .= " />\n";					
					break;
				case "checkbox":	
				case "radio":		
					if(sizeof($optional) > 0){
						$value_saved = $optional['value'];
						//Delete value from the optional array 
						$optional = $this->utils->array_cut('value',$optional);
					} else {
						if(is_array($this->data_src) && sizeof($this->data_src)){	
							if(array_key_exists($this->field_prefix.$name,$this->data_src)){
								$key_name = $this->field_prefix.$name;
							} else {
								$key_name = $name;
							}//end if
							$value_saved = $this->data_src[$key_name];	
						} else {								
							$value_saved = NULL;		
						}//end if
					}//end if					
					
					//var_dump($this->data_src);
					//echo "value saved is: ".$value_saved." - ".$this->data_src[$this->field_prefix.$name]."<br />";								
					//echo $this->field_prefix.$name;
					if(is_object($param) && $param != NULL){								
						//in this case there are subnodes that forms the checkboxes										
						foreach($param->checkbox as $value){							
							if($type == "checkbox"){
								$input .= "<input type=\"checkbox\" value=\"".$value."\" name=\"".$name."[]\" id=\"".$name."_".$i."\"";
								if($value_saved == $value){
									$input .= " checked=\"checked\"";
								}//end if
								$input .= " />\n";
							} else {
								$input .= "<input type=\"radio\" value=\"".$value."\" name=\"".$name."\" id=\"".$name."_".$i."\"";
								if($value_saved == $value){
									$input .= " checked=\"checked\"";
								}//end if
								$input .= " />\n";
							}//end if
							$input .= $this->apply_label($type,$label,$input,$start_label,$end_label);					
							$i++;
						}//end foreach
						
					} else if(is_string($param) && $param != NULL){														
						//That means that I've used the attribute "query" for executing a query
						$input .= $this->checkboxes_from_query($type,$name,$param,$value_saved,$label);
					} else {								
						$input_code = "<input type=\"".$type."\" name=\"".$name."\" id=\"".$name."\" value=\"".$value_saved."\"";
						if(sizeof($optional) > 0){
							$input_code .= $this->read_optional_attributes($optional);
						}//end if							
						if($value_saved == $this->data_src[$this->field_prefix.$name]){
							$input_code .= " checked=\"checked\"";
						}//end if
						$input_code .= " />\n";
						$input .= $this->apply_label($type,$label,$input_code,$start_label,$end_label);	
					}//end if					
					break;
			}//end switch			
			return $input;
		}//end function
		
		private function apply_label($type, $label = array(), $input, $start_label, $end_label){
			//check where put the label
			if($label['position'] == NULL){
				$label['position'] = 'inside';
			}//end if
			if($type != "hidden" && $type != "submit"){
				switch($label['position']){
					case "inside":
						$input = $start_label.$input.$end_label;
						break;
					case "outside":
						$input = $start_label.$end_label.$input;
						break;
					default:
						$input = $start_label.$input.$end_label;
				}//endswitch
			}//end if
			return $input;
		}//end function
		
		private function read_optional_attributes($opt_array){
			foreach($opt_array as $key => $value){				
				if($key != "default" && $value != NULL){					
					$string .= " ".$key."=\"".$value."\"";
				}//end if
			}//endfor
			return $string;
		}//end function
		
		private function create_days($value_saved = NULL){		
			$current = date("j",time());
			for($i = 1; $i <= 31; $i++){
				$html .= "<option value=\"".$i."\"";
				if($current == $i && $value_saved == NULL){
					$html .= " selected=\"selected\"";
				} else if($value_saved != NULL && $i == $value_saved){
					$html .= " selected=\"selected\"";
				}//end if
				$html .= ">".$i."</option>\n";
			}//end for
			return $html;
		}//end function
		
		private function create_months($value_saved = NULL){
			$months = explode(",",__MONTHS_LIST__);
			$current = date("n",time());
			$current--;
			if($value_saved != NULL){
				$value_saved--;
			}//end if
			for($i = 0; $i < 12; $i++){
				$html .= "<option value=\"".$i."\"";
				if($current == $i && $value_saved == NULL){
					$html .= " selected=\"selected\"";
				} else if($value_saved != NULL && $i == $value_saved){
					$html .= " selected=\"selected\"";
				}//end if
				$html .= ">".$months[$i]."</option>\n";
			}//end for
			return $html;
		}//end function
		
		private function create_years($value_saved = NULL){	
			$current = date("Y",time());
			for($i = 2006; $i <= 2020; $i++){
				$html .= "<option value=\"".$i."\"";
				if($current == $i && $value_saved == NULL){
					$html .= " selected=\"selected\"";
				} else if($value_saved != NULL && $i == $value_saved){
					$html .= " selected=\"selected\"";
				}//end if
				$html .= ">".$i."</option>\n";
			}//end for
			return $html;
		}//end function
		
		private function get_root_pages($value_saved = NULL){
			$files = $this->utils->explore("/");
			$files = $files['files'];
			for($i = 0; $i < sizeof($files); $i++){
				$html .= "<option value=\"".$files[$i]."\"";
				if($value_saved != NULL && $files[$i] == $value_saved){
					$html .= " selected=\"selected\"";
				}//end if
				$html .= ">".$files[$i]."</option>\n";
			}//end for
			return $html;
		}//end function
		
		private function options_from_query($param,$defvalue = NULL,$nodename = NULL){
			//Check first if there some conditional string	
			if(strpos($param,"custom_query") !== false){
				//In this case the param string should be like custrom_query?fieldvalue:fieldname and take the query from the node content
				$query = $this->xmlData->xmlform->{$nodename};
				//Check for query variables such as {module_id}
				if($this->module != NULL){
					$query = str_replace("{module_id}",$this->module,$query);
				}//end if			
				$splitted = explode("?",$param);
				$splitted = $splitted[1];//take everything after the ?
				$splitted = explode(":",$splitted);
				$table_id = $splitted[0];
				$table_value = $splitted[1];
			} else {
				//Now check if it was passed only the table name or also the other two definition fields			
				if(strpos($param,";") === false){
					//only the tablename
					$table = $param;
					$table_id = $param."_id";
					$table_value = $param."_name";
				} else {			
					//It found a separator
					$splitted = explode(";",$param);
					$table = $splitted[0];
					$table_id = $splitted[1];
					$table_value = $splitted[2];
				}//end if
				//Build query
				$query = "SELECT * FROM ".$table;	
			}//end if	
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){
				$data = $this->utils->get_result_array($result,false);	
				for($j = 0; $j < sizeof($data); $j++){
					$options .= "<option value=\"".$data[$j][$table_id]."\"";
					if($defvalue != NULL && $defvalue == $data[$j][$table_id]){
						$options.= " selected=\"selected\"";
					}//end if
					$options .= ">".$data[$j][$table_value]. "</option>\n";
				}//endfor
				return $options;
			} else {
				return false;
			}//end if
		}//end function
		
		private function checkboxes_from_query($type,$nodename,$param,$defvalue = NULL,$label){			
			//Check first if there some conditional string	
			if(strpos($param,"custom_query") !== false){
				//In this case the param string should be like custrom_query?fieldvalue:fieldname and take the query from the node content
				$query = $this->xmlData->xmlform->{$nodename};
				//Check for query variables such as {module_id}
				if($this->module != NULL){
					$query = str_replace("{module_id}",$this->module,$query);
				}//end if	
				$splitted = explode("?",$param);
				$splitted = $splitted[1];//take everything after the ?
				$splitted = explode(":",$splitted);
				$table_value = $splitted[0];
				$table_name = $splitted[1];
			} else {
				//Now check if it was passed only the table name or also the other two definition fields			
				if(strpos($param,";") === false){
					//only the tablename
					$table = $param;
					$table_value = $param."_value";
					$table_name = $param."_name";
				} else {			
					//It found a separator
					$splitted = explode(";",$param);
					$table = $splitted[0];
					$table_value = $splitted[1];
					$table_name = $splitted[2];
				}//end if
				$query = "SELECT * FROM ".$table;		
			}//end if	
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			//echo "received saved  value is ".$defvalue;
			if($defvalue != NULL){
				$saved_values = array();	
				//if($defvalue != 0){
					$defvalue = unserialize(base64_decode($defvalue));											
					//Parse the defvalues and creating an array				
					$i = 0;
					foreach($defvalue as $value){
						if(is_array($value)){
							foreach($value as $subvalue){
								$saved_values[$i] = $subvalue;
								$i++;
							}//end foreach
							//$i++;
						} else {
							$saved_values[$i] = $value;
							$i++;
						}//end if		
					}//end foreach
				//}//end if
			}//endif
		
			if($num > 0){
				$data = $this->utils->get_result_array($result,false);	
				for($j = 0; $j < sizeof($data); $j++){
					$start_label = "<label class=\"".$label['class']."\" for=\"".$nodename."_".$j."\">";
					$end_label = "<span>".$data[$j][$table_name]."</span>\n</label>";									
					$mycheckbox = "<input type=\"".$type."\" value=\"".$data[$j][$table_value]."\" name=\"".$nodename."[]\" id=\"".$nodename."_".$j."\"";	
					if($defvalue != NULL){
						$current_value = (string)$data[$j][$table_value];						
						if(in_array($current_value,$saved_values) != false){					
							$mycheckbox .= " checked=\"checked\"";
						}//end if
					}//end if
					$mycheckbox .= "/>\n";
					$input .= $this->apply_label('checkbox',$label,$mycheckbox,$start_label,$end_label);	
				}//end for
				return $input;
			} else {
				return false;
			}//end if
		}//end function
	}//end class
?>