<?php
	require_once('ewrite.class.php');

	class sidebars extends ewrite{
	
		public $utils;//utils class
		public $t;//prefix before each tablename
		public $uid;//userid
		public $pid;//post id	
		public $sid;//Sidebar ID
		public $module; //module
		public $errors;
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->module = "ewrite";
			$this->utils->read_params($this,$params);			
			$module_data = $this->utils->get_module($this->module);
			$this->force_module($this->module);
			$this->module_id = $module_data['module_id'];			
		}//endconstructor
		
	
		public function get_sidebars($module,$id){
			$module_data = $this->utils->get_module($module);
			$module_id = $module_data['module_id'];
			$query = "SELECT * FROM ".$this->t."sidebars WHERE sidebar_post_id = ".$id." AND sidebar_module_id = ".$module_id." ORDER BY sidebar_order";			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);						
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function get_sidebar($sid){
			$query = "SELECT * FROM ".$this->t."sidebars WHERE sidebar_id = ".$sid;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);						
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function save_sidebar($module_id,$id){
			//get the information
			$title = $this->utils->strtodb(trim($_POST['title']));
			$text = $this->utils->strtodb(trim($_POST['text']));
			$link = $this->utils->strtodb(trim($_POST['link']));
			$link_label = $this->utils->strtodb(trim($_POST['link_label']));
			
			//insert or edit
			if(is_null($_POST['sid'])){
				//is a new sidebar
				$query = "INSERT INTO ".$this->t."sidebars
						  (sidebar_title, sidebar_text, sidebar_link, sidebar_link_label, sidebar_post_id, sidebar_module_id)
						  VALUES
						  ('".$title."','".$text."','".$link."','".$link_label."',".$id.",".$module_id.")";
			} else {
				//edit a sidebar
				$query = "UPDATE ".$this->t."sidebars SET
						  sidebar_title = '".$title."',
						  sidebar_text = '".$text."',
						  sidebar_link = '".$link."',
						  sidebar_link_label = '".$link_label."',						
						  sidebar_post_id = ".$id.",
						  sidebar_module_id = ".$module_id."
						  WHERE sidebar_id = ".$_POST['sid'];
			}//end if
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			return true;
			
		}//end function
		
		//Sort the attachment sort
		public function save_sidebar_sorting($module_id,$id,$sidebar_ids){
			if(!is_numeric($module_id) && $module_id != NULL){
				$module_id = $this->utils->get_module($module_id);
				$module_id = $module_id['module_id'];
			}//end if			
			$sid = $sidebar_ids;			
			if(is_array($sid) && sizeof($sid) > 0){
				for($i = 0; $i < sizeof($sid); $i++){
					$query = "UPDATE ".$this->t."sidebars 
							  SET sidebar_order = ".$i."
							  WHERE sidebar_post_id = ".$id."
							  AND sidebar_module_id = ".$module_id." 
							  AND sidebar_id = ".$sid[$i];						
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));	
				}//end for
				return true;
			} else {
				return false;
			}//end if		
		}//end function
		
		//delete a sidebar
		public function delete_sidebar($sid){
			$query = "DELETE FROM ".$this->t."sidebars WHERE sidebar_id = ".$sid;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			return true;
		}//end function
	}//end class
		
?>    