<?php

	require_once('ewrite.class.php');

	class public_ewrite extends ewrite{
	
		private $utils;//utils class
		private $t;//prefix before each tablename
		private $uid;//userid
		private $pid;//post id	
		private $cid;//category id	
		private $fcid;//father category id
		private $gid;//group id
		public $module; //module
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->module = "ewrite";
			$this->utils->read_params($this,$params);				
			$module_data = $this->utils->get_module($this->module);
			$this->module_id = $module_data['module_id'];
		}//endconstructor
		
		
		public function get_posts($cid = NULL,$sort_by = "post_date", $sort_order = "DESC",$status = 1){
			$query = "SELECT ".$this->t."posts.*, ".$this->t."users.user_username, ".$this->t."categories.*, ".$this->t."categories_rel.*, UNIX_TIMESTAMP(".$this->t."posts.post_date) AS post_date, module_name AS folder
					  FROM (".$this->t."posts, ".$this->t."modules)
					  LEFT JOIN ".$this->t."users
					  ON (".$this->t."users.user_id = ".$this->t."posts.post_uid)
					  LEFT JOIN ".$this->t."categories_rel
					  ON ".$this->t."categories_rel.categories_rel_post_id = ".$this->t."posts.post_id
					  LEFT JOIN ".$this->t."categories
					  ON (".$this->t."categories.category_id = ".$this->t."categories_rel.categories_rel_category_id)
					  WHERE category_module_id = ".$this->module_id."
					  AND post_module_id = module_name";
			//check the status of the posts
			if($status != NULL){
				$query .= " AND ".$this->t."posts.post_status = ".$status;
			}//end if	
			if($cid != NULL){
				$query .= " AND ".$this->t."categories.category_id = ".$cid;
			}//end if
			if($this->uid != NULL){
				$query .= " AND ".$this->t."posts.post_uid = ".$this->uid;
			}//end if
			$query .= " GROUP BY ".$this->t."posts.post_id						
						ORDER BY ".$sort_by." ".$sort_order;
			if($this->utils->paging_isset()){
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			//echo $query;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				/*for($i = 0; $i < sizeof($data); $i++){				
					$addfield_data = $this->utils->get_additional_data($this->module,'new',$data[$i]['post_id']);						
					$data[$i] = array_merge($data[$i],$addfield_data);
				}//end for	*/						
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		//return a list with attachments
		//If the third paramether is equal to 1, will show only the images, if it's 0 will show the rest, if null will show both
		public function get_attachments($module_id = NULL,$ref_id = NULL,$is_img = NULL){
			if(!is_numeric($module_id) && $module_id != NULL){
				$module_id = $this->get_module($module_id);
				$module_id = $module_id['module_id'];
			}//end if
			$query = "SELECT * FROM ".$this->t."attachments, ".$this->t."modules";
			if($module_id != NULL){
				$query .= " WHERE attachment_ref_module_id = ".$module_id."
							AND module_id = attachment_ref_module_id";
			} else {
				$query .= " WHERE module_id = attachment_ref_module_id";
			}//end if
			$query .= " AND attachment_hidden = 0";
			if($module_id != NULL && $ref_id != NULL){
				$query .= " AND attachment_ref_id = ".$ref_id;
			}//end if
			if($module_id != NULL && $ref_id != NULL && $is_img != NULL){
				$query .= " AND attachment_is_img = ".$is_img;
			}//end if	
			if($module_id == NULL){
				$query .= " GROUP BY attachment_filename";
			}//end if
			$query .= " ORDER BY attachment_order ASC";
			//This is for the paging			
			if($this->paging_isset()){
				$this->set_unpaged_query($query);
				$query .= " LIMIT ".$this->p_start.", ".$this->p_limit;
			}//end if	
			$result = @mysql_query($query,$this->db);
			$this->error($result,__LINE__,get_class($this));
			$num = @mysql_num_rows($result);
			if($num > 0){		
				$data = $this->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
	}//end class
?>