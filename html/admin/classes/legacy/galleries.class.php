<?php
	require_once('ewrite.class.php');

	class galleries extends ewrite{
	
		protected $utils;//utils class
		protected $t;//prefix before each tablename
		protected $uid;//userid
		protected $pid;//post id	
		protected $cid;//category id	
		protected $fcid;//father category id
		protected $gid;//group id
		protected $reg_errors_data;
		protected $site_address;
		public $module; //module
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->module = "galleries";
			$this->utils->read_params($this,$params);			
			$module_data = $this->utils->get_module($this->module);
			$this->force_module($this->module);
			$this->module_id = $module_data['module_id'];
		}//endconstructor
		
		public function save_post(){
			$hidden_fields = array('action','step','submit','a','category_id','id','date_day','date_month','date_year','tags','category','file_upload','file_title','caption');
			//if exists the fields for the date, I create with these a new post field with the correct formatted data for mysql
			if($_POST['date_month'] != NULL && $_POST['date_day'] != NULL && $_POST['date_year'] != NULL){
				$_POST['date'] = date("Y-m-d H:i:s",mktime(1,0,0,$_POST['date_month']+1,$_POST['date_day'],$_POST['date_year']));				
			} else if($_POST['date'] != NULL){
				$_POST['date'] = $this->utils->convert_to_db_date($_POST['date']);
			}//end if
			
			if($_POST['date_end'] != NULL){
				$_POST['date_end'] = $this->utils->convert_to_db_date($_POST['date_end']);
			}//end if
			$count = $this->give_num_posts();		
			//now search in the additional fields which are the input to avoid
			$hidden_fields = $this->utils->merge_addfields_to_hiddens($this->module,'new',$hidden_fields);

			if($_REQUEST['id'] == NULL){
				//Is a new post
				$_POST['order'] = $count;
				$_POST['module_id'] = $this->module;
				$sql = $this->utils->build_sql_fields("gallery","INSERT","POST",$hidden_fields);				
				$query = "INSERT INTO ".$this->t."galleries ".$sql;	
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
				$this->pid = @mysql_insert_id();
		
				if($_REQUEST['tags'] != NULL){
					$this->utils->add_tags($_REQUEST['tags'],$this->pid,$this->module);
				}//end if
				if($_POST['category_id'] == NULL){
					$this->put_into_mainpages($this->pid);
				}//end if
			} else {
				//Editing an existant post
				$this->pid = (int)$_REQUEST['id'];	
				
				$sql = $this->utils->build_sql_fields("gallery","UPDATE","POST",$hidden_fields);				
				$query = "UPDATE ".$this->t."galleries SET ".$sql." WHERE gallery_id = ".$this->pid;				
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
				
				if($_REQUEST['tags'] != NULL){
					$this->utils->add_tags($_REQUEST['tags'],$this->pid,$this->module);
				}//end if
			}//endif		
			return true;
		}//end function
		
		public function get_post_id(){
			return $this->pid;
		}//end function
		
		//public function get_posts($cid = NULL,$sort_by = "post_date", $sort_order = "DESC",$status = NULL,$deleted = NULL){
		public function get_galleries(){			
			$params = func_get_args();
			if(!is_array($params[0])){
				$cid = $params[0];				
				$sort_by = $params[1];
				
				$sort_order = $params[2];
				
				$status = $params[3];
				$deleted = $params[4];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			if($sort_by == NULL){
				$sort_by = "gallery_date";
			}//end if
			if($sort_order == NULL){
				$sort_order = "DESC";
			}//end if
		
			$query = "SELECT ".$this->t."galleries.*, ".$this->t."users.*, UNIX_TIMESTAMP(".$this->t."galleries.gallery_date) AS gallery_date, 
					 module_name AS folder";			
			if($cid != NULL){
				$query .= ", ".$this->t."categories.*, ".$this->t."categories_rel.*";
			}//end if
			$query .= " FROM (".$this->t."galleries, ".$this->t."modules)
					  LEFT JOIN ".$this->t."users
					  ON (".$this->t."users.user_id = ".$this->t."galleries.gallery_uid) ";
			if($cid != NULL){
				$query .= "LEFT JOIN ".$this->t."categories_rel
					  ON ".$this->t."categories_rel.categories_rel_post_id = ".$this->t."galleries.gallery_id
					  LEFT JOIN ".$this->t."categories
					  ON (".$this->t."categories.category_id = ".$this->t."categories_rel.categories_rel_category_id) ";
			}//end if
			$query .= "WHERE gallery_module_id = module_name";
			if(!isset($all_modules)){
				if($cid != NULL){
					$query .= " AND category_module_id = ".$this->module_id."";
				} else {
					if(isset($module_list) && $module_list != NULL && is_string($module_list)){
						$module_list = explode(",",$module_list);
						$module_list = implode("','",$module_list);
						$query .= " AND gallery_module_id IN ('".$module_list."')";		
					} else {
						$query .= " AND gallery_module_id = '".$this->module."'";		
					}//end if
				}//end if	
			}//end if  
			/*
			if($module_group_id != NULL && is_numeric($module_group_id)){
				$query .= "";
			}//end if
			*/
			if($month != NULL && is_numeric($month)){
				$query .= " AND MONTH(gallery_date) = ".$month;
			}//end if
			
			if($year != NULL && is_numeric($year)){
				$query .= " AND YEAR(gallery_date) = ".$year;
			}//end if
			
			if($userid != NULL && isset($userid)){
				$query .= " AND user_id = ".$userid;
			}//end if
						
			if($tag != NULL && is_string($tag)){
				$query .= " AND gallery_tags LIKE '%".$tag."%'";
			}//end if
			if($this->search_key != NULL){
				$sql = $this->process_search();
				if($sql != false){
					$query .= $sql;					
				}//endif
			}//end if
			//check the status of the posts
			if($status != NULL && is_numeric($status)){
				$query .= " AND gallery_status = ".$status;
			}//end if	
	
			/*if($cid != NULL && $status != NULL && is_numeric($status)){
				$query .= " AND category_status = ".$status;
			}//end if*/
			//check if the posts is deleted or not	
			if($deleted != NULL || is_numeric($deleted)){
				$query .= " WHERE gallery_deleted = ".$deleted;
			}//end if
			
			if($cid != NULL){
				$query .= " AND (category_id = ".$cid;
				if(isset($show_subelements) && $show_subelements == true){
					$query .= " OR category_father_id = ".$cid;
				}//end if
				$query .= ")";
			}//end if
			
			if(sizeof($this->filters) > 0){
				$query .= $this->process_filters(false);
			}//end if
			$query .= " GROUP BY ".$this->t."galleries.gallery_id						
						ORDER BY ".$sort_by." ".$sort_order.", gallery_id DESC";				
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if			
		
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);

			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);				
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function get_items(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
		
			if($sort_by == NULL){
				$sort_by = "item_order";
			}//end if
			if($sort_order == NULL){
				$sort_order = "ASC";
			}//end if
			
			$query_posts = "SELECT gallery_id as id, gallery_title as label, user_username, module_item_order_order as item_order, gallery_status as status, UNIX_TIMESTAMP(gallery_date) as item_date
						  FROM ".$this->t."galleries
						  LEFT JOIN ".$this->t."users
						  ON (".$this->t."users.user_id = ".$this->t."galleries.gallery_uid) 
						  LEFT JOIN ".$this->t."categories_rel 
						  ON gallery_id = categories_rel_post_id
						  LEFT JOIN ".$this->t."modules_items_order
						  ON (module_item_order_module_id = ".$this->module_id."
						  AND module_item_order_type = 'post'
						  AND module_item_order_item_id = gallery_id)
						  WHERE gallery_module_id = '".$this->module."' ";	
			if($userid != NULL){
				$query_posts .= " AND gallery_uid = ".$userid;
			}//end if
			if($category_id == NULL){
				$query_posts .= " AND (categories_rel_id IS NULL OR module_item_order_order IS NOT NULL)
								  AND (module_item_order_father_id IS NULL OR module_item_order_father_id = 0)";
			} else {
				$query_posts .= " AND categories_rel_category_id = ".$category_id."
								  AND (module_item_order_father_id = ".$category_id." OR module_item_order_father_id IS NULL)";
			}//end if	
			if($status != NULL){
				if($status < 2){
					$query_posts .= " AND gallery_status = ".$status;
				} else {
					$query_posts .= " AND (gallery_status = 1 OR gallery_status = 2)";
				}//end if
			}//end if
			if($this->search_key != NULL){
				$sql = $this->process_search();
				if($sql != false){
					$query .= $sql;					
				}//endif
			}//end if				
	
			$query_categories .= "SELECT category_id as id, category_name as label, category_father_id, module_item_order_order as item_order, 
							   category_default as status, UNIX_TIMESTAMP(category_date) as item_date
							   FROM ".$this->t."categories
							   LEFT JOIN ".$this->t."modules_items_order
							   ON (module_item_order_module_id = category_module_id
							   AND module_item_order_type = 'category'
							   AND module_item_order_item_id = category_id)
							   WHERE category_module_id = '".$this->module_id."'";
			if($category_id == NULL){
				$query_categories .= " AND category_father_id = 0
									   AND (module_item_order_father_id IS NULL OR module_item_order_father_id = 0)";
			} else {
				$query_categories .= " AND category_father_id = ".$category_id."
								       AND (module_item_order_father_id = ".$category_id." OR module_item_order_father_id IS NULL)";
			}//end if		   
				
			//Fuse the two queries
			$query = "(".$query_posts.") UNION (".$query_categories.")"; 
			$query .= " ORDER BY ".$sort_by." ".$sort_order;			
				
			//echo $query;

			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);				
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function get_gallery($pid){
			return $this->get_post($pid);
		}//end function
		
		//Return the data of the id post passed
		public function get_post($pid){
			if(!is_numeric($pid)){
				return false;
			}//end if
			$this->pid = $pid;
			$query = "SELECT ".$this->t."galleries.*, ".$this->t."users.user_username, UNIX_TIMESTAMP(gallery_date) AS gallery_date, module_name AS folder
					  FROM (".$this->t."galleries, ".$this->t."modules)
					  LEFT JOIN ".$this->t."users
					  ON ".$this->t."users.user_id = ".$this->t."galleries.gallery_uid
					  WHERE gallery_id = ".$pid."
					  AND module_name = gallery_module_id";					 
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);	
				//Read the post categories
				////This may fix a problem, in case, take it out
				$this->module = $data['folder'];
				$post_cats = $this->get_post_categories($pid);	
				if($post_cats !== false){
					$cids = array();
					for($i = 0; $i < sizeof($post_cats); $i++){
						array_push($cids,$post_cats[$i]['category_id']);
					}//end for
					$data['category_id'] = base64_encode(serialize($cids));
				} else {
					$data['category_id'] = NULL;			
				}//end if
				
				/*$addfield_data = $this->utils->get_additional_data($this->module,'new',$pid);				
				$data = array_merge($data,$addfield_data);*/
				//These three fields are made only for the post editing
				$data['post_date_day'] = date("j",$data['post_date']);
				$data['post_date_month'] = date("n",$data['post_date']);	
				$data['post_date_year'] = date("Y",$data['post_date']);				
				return $data;
			} else {
				return false;
			}//endif
		}//end function
		
		public function delete_gallery($pid){
			//check if postid is an array or not
			if(!is_array($pid)){				
				$query = "DELETE FROM ".$this->t."galleries WHERE gallery_id = ".$pid;
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
				$this->delete_post_attachments($pid);	
				$this->delete_post_module_item_relation($pid);
			} else {
				for($i = 0; $i < sizeof($pid); $i++){				
					$query = "DELETE FROM ".$this->t."galleries WHERE gallery_id = ".$pid[$i];
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));	
					$this->delete_post_attachments($pid[$i]);	
					$this->delete_post_module_item_relation($pid[$i]);
				}//end for
			}//end if	
				
			return true;
		}//end function
		
		public function delete_post($pid){			
			return $this->delete_gallery($pid);
		}//end function	
		
		public function get_post_galleries($pid,$module_id){
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];
			}//end if
			
			$query = "SELECT * 
					  FROM attachments 
					  WHERE attachment_ref_id = ".$pid." 
					  AND attachment_ref_module_id = ".$module_id."
					  AND attachment_is_file = 0
					  AND attachment_data IS NOT NULL";
			$result = @mysql_query($query,$this->utils->db);
			$error == $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = @mysql_num_rows($result);
				if($num > 0){		
					$data = array();
					while($row = @mysql_fetch_array($result)){
						$enc_data = $row['attachment_data'];
						$enc_data = $this->utils->decrypt($enc_data);
						if($enc_data['type'] == 'gallery'){
							$gallery_id = $enc_data['item_id'];
							$gdata = $this->get_gallery($gallery_id);
							array_push($data,$gdata);
						}//end if						
					}//end while
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function update_gallery_field($postID,$fieldName,$fieldValue){
			$query = "UPDATE ".$this->t."galleries SET ".$fieldName." = '".$fieldValue."' WHERE gallery_id = ".$postID;
			$result = @mysql_query($query,$this->utils->db);
			if($this->utils->error($result,__LINE__,get_class($this))){
				return false;
			} else {
				return true;
			}//end if
		}//end if
		
	}//end class
?>