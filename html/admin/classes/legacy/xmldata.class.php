<?php
	class xmldata{
	
		public $utils;//utils class
		private $xmldata;//xml data var
		public $module; //module
		public $module_name;//module name
		private $addnodes; //Additional nodes
	
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;			
			$this->xmldata = "";
			$this->utils->read_params($this,$params);
		}//endconstructor
		
		private function init_xml_struct(){
			$data = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
			$data .= "<xml>";	
			return $data;
		}//end function
		
		public function set_single_nodes($nodes_data = array()){
			if(is_array($nodes_data) && sizeof($nodes_data) > 0){
				foreach($nodes_data as $key => $value){
					$data .= "\n	<".$key.">";
					if(is_string($value)){
						$data .= "<![CDATA[".$value."]]>";
					} else {
						$data .= $value;
					}//endif
					$data .= "</".$key.">";
				}//end foreach
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function set_module($module){
			$this->module = $this->utils->get_module_data($module);
			$this->module_name = $this->module['module_name'];
			$this->module = $this->module['module_id'];			
		}//end function
		
		public function get_xmls($module = NULL){
		
		}//end function
		
		public function add_data($data_array){
			//Try to  understand if it's multidimensinal or not
			if(is_array($data_array)){
				if(is_array($data_array[0])){
					//Is multidimensional
				} else {
					//Simple array
					$this->xmldata .= $this->set_single_nodes($data_array);
				}//end if
			} else {
				return false;
			}//endif
		}//end function
		
		public function create_xml($filename){
			$xmlcode = $this->init_xml_struct();
			$xmlcode .= $this->close_xml_struct();
			$this->save_xml($filename,$xmlcode);
		}//end function
		
		private function close_xml_struct(){
			$data = "\n</xml>";		
			return $data;
		}//end function
		
		//This function save the xml file
		private function save_xml($filename,$xml_struct){			
			/*
			$Settings = $gen->getSettings('egallery');
			$xml_path = $Settings['xml_path'];
			*/
			$xml_path = __SERVERPATH__."content/xml/";		
			echo ($xml_path);
			//Check if the dir exists
			if(!file_exists($xml_path)){
				//If doesn't exists, create it
				mkdir($xml_path,0777);
			}//end if
			$basedir = $xml_path;		
			//Check if the path finish with the slash			
			$basedir = $this->utils->check_slash($basedir);
			//Check if the folder is writable
			if(is_writable($basedir)){
				//Create the file on the server
				$xmlfile = fopen($basedir.$filename,"w+");				
				//Write the xml structure inside the file
				fwrite($xmlfile,$xml_struct);
				//Close the reference to the file
				fclose($xmlfile);	
			} else {
				return false;
			}//end if
		}//end function
	}//end class
?>