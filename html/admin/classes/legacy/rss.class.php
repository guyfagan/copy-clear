<?php
	require_once('xmldata.class.php');
	
	class rss extends xmldata{
	
		private $xml_path;
		private $xml_uri;
		private $filename;
		private $fid; //Feed ID
		private $cid; //Category ID
		private $classIstance;
		private $max_elements;
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);			
			$this->xml_path = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__."content/rss/";
			$this->xml_uri = "http://".$_SERVER['HTTP_HOST'].__SERVERPATH__;
			$this->max_elements = 10; //Default max elements showed in the RSS file
		}//endconstructor
		
		public function create_rss(){
			$this->filename = $_POST['filename'];
			if($this->filename == NULL){
				return false;
			}//end if
			$rss_header = $this->store_rss();
			if(!$rss_header){
				return false;
			}//end if
			$xmlcode = $this->init_xml_struct();
			$xmlcode .= $rss_header;
			$params = array("limit_text" => $_POST['limit_text']);
			$xmlcode .= $this->put_content($params);
			$xmlcode .= $this->close_xml_struct();
			$this->save_rss($this->filename,$xmlcode);
			return true;
		}//end function
		
		public function add_item_node($addnodes){
			$this->addnodes .= $addnodes;
		}//end function
		
		public function update_feeds($params = NULL){		
			$query = "SELECT ".$this->t."feeds.*, ".$this->t."modules.module_name, ".$this->t."modules.module_id
					  FROM (".$this->t."feeds)
					  LEFT JOIN ".$this->t."modules 
					  ON ".$this->t."feeds.feed_module = ".$this->t."modules.module_id
					  WHERE ".$this->t."feeds.feed_autoupdate = 1";	
					 			  
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){
				$data = $this->utils->get_result_array($result,false);
				for($i = 0; $i < sizeof($data); $i++){
					//Call the proper class
					$ci = $data[$i]['module_name']; //Class Istance
					if($ci != NULL){
						if(file_exists("classes/".$ci.".class.php")){
							${$ci} = $this->utils->call($ci);
						} else {
							$cparams = array("module" => $data[$i]['module_name']);
							${$ci} = $this->utils->call("ewrite",$cparams);
						}//end if
						$this->set_module($data[$i]['module_name']);
						$this->set_class(${$data[$i]['module_name']});
					}//end if
					//Build the xml structure
					$xmlcode = $this->init_xml_struct();
			
					$title = $data[$i]['feed_title'];			
					$link = $this->xml_uri."content/rss/".$data[$i]['feed_filename'];
					$description = html_entity_decode($data[$i]['feed_description']);
					//remove all useless tags
					$title = strip_tags($title,"<br><a><strong><em><b><i>");
					$description = strip_tags($description,"<br><a><strong><em><b><i>");
				
					$xmlcode .= $this->build_rss_header($title,$link,$description);
					$params = array("limit_text" => $data[$i]['limit_text']);
					$xmlcode .= $this->put_content($params);
					$xmlcode .= $this->close_xml_struct();
					$this->save_rss($data[$i]['feed_filename'],$xmlcode);
				}//end for
				return $num;
			} else {
				return false;
			}//end if
		}//end function
		
		public function force_feed_update($fid,$params = NULL){
			$query = "SELECT ".$this->t."feeds.*, ".$this->t."modules.module_name, ".$this->t."modules.module_id
					  FROM ".$this->t."feeds, ".$this->t."modules 
					  WHERE ".$this->t."feeds.feed_id = ".$fid."
					  AND ".$this->t."feeds.feed_module = ".$this->t."modules.module_id";					  
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num == 1){
				$data = $this->utils->get_result_array($result,true);			
				//Call the proper class
				$ci = $data['module_name']; //Class Istance
				${$data['module_name']} = $this->utils->call($data['module_name']);
				$this->set_module($data['module_name']);
				$this->set_class(${$data['module_name']});
				//Build the xml structure
				$xmlcode = $this->init_xml_struct();
		
				$title = $data['feed_title'];			
				$link = $this->xml_uri."content/rss/".$data['feed_filename'];
				$description = html_entity_decode($data['feed_description']);
				//remove all useless tags
				$title = strip_tags($title,"<br><a><strong><em><b><i>");
				$description = strip_tags($description,"<br><a><strong><em><b><i>");
				//Check if are presents some additional fields
				if($data['feed_additional_fields'] != NULL){
					$this->addnodes = unserialize(base64_decode($data['feed_additional_fields']));
				}//end if
				$xmlcode .= $this->build_rss_header($title,$link,$description);
				$xmlcode .= $this->put_content($params);
				$xmlcode .= $this->close_xml_struct();
				$this->save_rss($data['feed_filename'],$xmlcode);
				
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function preview_feed($fid,$params = NULL){
			$query = "SELECT ".$this->t."feeds.*, ".$this->t."modules.module_name, ".$this->t."modules.module_id
					  FROM ".$this->t."feeds, ".$this->t."modules 
					  WHERE ".$this->t."feeds.feed_id = ".$fid."
					  AND ".$this->t."feeds.feed_module = ".$this->t."modules.module_id";					  
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num == 1){
				$data = $this->utils->get_result_array($result,true);			
				//Call the proper class
				$ci = $data['module_name']; //Class Istance
				${$data['module_name']} = $this->utils->call($data['module_name']);
				$this->set_module($data['module_name']);
				$this->set_class(${$data['module_name']});
				//Build the xml structure
				$xmlcode = $this->init_xml_struct();
		
				$title = $data['feed_title'];			
				$link = $this->xml_uri."content/rss/".$data['feed_filename'];
				$description = html_entity_decode($data['feed_description']);
				//remove all useless tags
				$title = strip_tags($title,"<br><a><strong><em><b><i>");
				$description = strip_tags($description,"<br><a><strong><em><b><i>");
				//Check if are presents some additional fields
				if($data['feed_additional_fields'] != NULL){
					$this->addnodes = unserialize(base64_decode($data['feed_additional_fields']));
				}//end if
				$xmlcode .= $this->build_rss_header($title,$link,$description);
				$xmlcode .= $this->put_content($params);
				$xmlcode .= $this->close_xml_struct();	
				
				return $xmlcode;
			} else {
				return false;
			}//end if
		}//end function
		
		private function store_rss(){
			$title = $this->utils->dbtostr($_POST['title']);
			if($title == NULL){
				return false;
			}//end if
			$description = $this->utils->dbtostr($_POST['description']);
			if($description == NULL){
				return false;
			}//end if
			//remove all useless tags
			$title = strip_tags($title,"<br><a><strong><em><b><i>");
			$description = strip_tags(html_entity_decode($description),"<br><a><strong><em><b><i>");
			
			$link = $this->xml_uri."content/rss/".$this->filename;
			$category_id = $_POST['category_id'];
			if($category_id == NULL){
				$category_id = 0;
			} else {
				$this->cid = $category_id;
			}//end if
			$autoupdate = $_POST['autoupdate'];
			if($autoupdate == NULL){
				$autoupdate = 0;
			}//end if
			
			$limit_text = $_POST['limit_text'];
			
			if($_POST['additional_fields'] != NULL){
				$additional_fields = base64_encode(serialize($_POST['additional_fields']));
				$this->addnodes = $_POST['additional_fields'];			
			} else {
				$additional_fields = NULL;
			}//end if
			
			$module = $this->module;
			if($module == NULL){
				$module = "NULL";
			}//end if
			
			$query = "INSERT INTO ".$this->t."feeds
					  (feed_title, feed_description, feed_link, feed_filename, feed_module, feed_category, feed_autoupdate, feed_additional_fields, feed_text_limit)
					  VALUES
					  ('".$title."','".$description."','".$link."','".$this->filename."',".$module.",".$category_id.",".$autoupdate.",'".$additional_fields."',".$limit_text.")";							
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$this->fid = @mysql_insert_id();
			$xmlstruct .= $this->build_rss_header($title, $link, $description);
			return $xmlstruct;
		}//end function
		
		private function build_rss_header($title, $link, $description){
			$xmlstruct = "\n	<title><![CDATA[".$title."]]></title>";
			$xmlstruct .= "\n	<link><![CDATA[".$link."]]></link>";
			$xmlstruct .= "\n	<description><![CDATA[".$description."]]></description>";
			return $xmlstruct;
		}//end function
		
		private function init_xml_struct(){
			$data = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
			$data .= "\n<rss version=\"2.0\">";
			$data .= "\n	<channel>";	
			return $data;
		}//end function
		
		public function set_class($classIstance){
			$this->classIstance = $classIstance;
		}//end function
		
		public function get_rss_headers(){
			$query = "SELECT * FROM ".$this->t."feeds";
			//To finish
		}//end function
		
		private function put_content($params = NULL){			
			if($this->module_name != NULL){
				$params = array("module" => $this->module_name,"limit_result" => 10);	
				if(file_exists("classes/".$this->module_name.".class.php")){
					$this->classIstance = $this->utils->call($this->module_name,$params);
				} else {
					$this->classIstance = $this->utils->call("ewrite",$params);
				}//end if
			} else {
				$params = array("all_modules" => "true","limit_result" => 10);
				$this->classIstance = $this->utils->call("ewrite",$params);
			}//end if
			
			//Miss the category filter			
			if($params == NULL){
				$data = $this->classIstance->get_posts();								
			} else {
				$cid = $params['cid'];
				$sortfield = $params['sortfield'];
				$sortorder = $params['sortorder'];						
				$data = $this->classIstance->get_posts($cid,$sortfield,$sortorder);	
			}//end  if			
			
			for($i = 0; $i < sizeof($data); $i++){
				
				$title = $data[$i]['post_title'];
				$title = strip_tags(html_entity_decode($title),"<br><a><strong><em><b><i>");
			
				$description = html_entity_decode($data[$i]['post_message']);
				$description = str_replace("<p>&#160;</p>","<br /><br />",$description);						
				$description = strip_tags($description,"<br><a><strong><em><b><i>");
				$description = str_replace("\n\r","",$description);	
				$description = str_replace("\n","",$description);
				$description = str_replace("\r","",$description);	
				if($limit_text != NULL && is_numeric($limit_text)){
					$description = $this->utils->trunktext($description,$_POST['limit_text']);
				}//end if
				
				//Check if the string begin with <br />
				if(substr($description,0,6) == "<br />"){							
					$description = substr($description,6,strlen($description));
				}//end if				
			
				$xmlstruct .= "\n	<item>";
				$xmlstruct .= "\n		<title><![CDATA[".$title."]]></title>";
				$xmlstruct .= "\n		<link><![CDATA[".$this->xml_uri.$data[$i]['post_id']."]]></link>";
				$xmlstruct .= "\n		<description><![CDATA[".$description."]]></description>";					
				/*
				if(is_array($this->addnodes)){						
					//It's an array				
					for($j = 0; $j < sizeof($this->addnodes); $j++){
						$xmlstruct .= "\n		<".$this->addnodes[$j]."><![CDATA[".strip_tags($data[$i]["post_".$this->addnodes[$j]],"<p><a><b><strong><em>")."]]></".$this->addnodes[$j].">";
					}//end foreach
				} else {
					//It's a string
					$xmlstruct .= "\n		<".$this->addnodes."><![CDATA[".$data[$i]["post_".$this->addnodes]."]]></".$this->addnodes.">";
				}//end i f*/
				$xmlstruct .= "\n	</item>"; 					
			}//end for i
			return $xmlstruct;
		}//end function
		
		private function close_xml_struct(){
			$data = "\n	</channel>";
			$data .= "\n</rss>";		
			return $data;
		}//end function
		
		//This function save the xml file
		private function save_rss($filename,$xml_struct){	
			//Check if the dir exists
			if(!file_exists($this->xml_path)){
				//If doesn't exists, create it
				@mkdir($this->xml_path,0777);
			}//end if
			$basedir = $this->xml_path;		
			//Check if the path finish with the slash			
			$basedir = $this->utils->check_slash($basedir);
			
			//chmod ($basedir,0777);
			//Check if the folder is writable
			if(is_writable($basedir)){				
				//Create the file on the server
				$xmlfile = fopen($basedir.$filename,"w+");				
				//Write the xml structure inside the file
				fwrite($xmlfile,$xml_struct);
				//Close the reference to the file
				fclose($xmlfile);	
			} else {		
				return false;
			}//end if
		}//end function
		
		public function save_temp_file($filename,$xml_struct){
			$this->save_rss($filename,$xml_struct);
		}//end function
		
		public function remove_temp_file($filename){
			$basedir = $this->xml_path;		
			//Check if the path finish with the slash			
			$basedir = $this->utils->check_slash($basedir);
			if(file_exists($basedir.$filename)){
				if(@unlink($basedir.$filename)){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return true;
			}//end if
		}//end function
		

		
		public function read_rss($url,$use_curl = false){
			if($use_curl == false){
				$xmlFileData = file_get_contents($url);		
				$xmlData = new SimpleXMLElement($xmlFileData);			
				
				$data = array();
				$i = 0;	
				foreach($xmlData->channel->children() as $node){
					if($node->getName() == "item"){
						$data[$i] = array();
						foreach($node->children() as $key => $value){						
							$data[$i][$key] = (string)$value;										
						}//end foreach
						$i++;
					}//end if
				}//end foreach
				
			} else {
				$fm = @curl_init();				
				@curl_setopt($fm, CURLOPT_URL, $url);
			//	@curl_setopt($tw, CURLOPT_USERPWD, $this->login);
				@curl_setopt($fm, CURLOPT_RETURNTRANSFER, TRUE);
				$xmlFileData = @curl_exec($fm);
				$xmlData = new SimpleXMLElement($xmlFileData);					
				$data = array();
				$i = 0;	
				foreach($xmlData->channel->children() as $node){
					if($node->getName() == "item"){
						$data[$i] = array();
						foreach($node->children() as $key => $value){						
							$data[$i][$key] = (string)$value;										
						}//end foreach
						$i++;
					}//end if
				}//end foreach
			}//end if
			
			return $data;
		}//end function 
		
		public function get_feeds(){
			$query = "SELECT * FROM ".$this->t."feeds
					  ORDER BY feed_id ASC";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);					
				return $data;
			} else {
				return false;
			}//endif
		}//end function
	}//end class
?>	