<?php

	require_once('users.class.php');

	class public_users extends users{
	
		public $utils;//utils class
		public $t;//prefix before each tablename
		public $uid;//User ID
		public $gid;//Group ID
		public $lt; //defines the login type, sessions or cookies
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);		
			$this->utils->read_params($this,$params);
			$this->module = "users";
		}//endconstructor
		
		public function register(){
		
		}//end function		
	
	}//end class
?>