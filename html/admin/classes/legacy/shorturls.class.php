<?php
	class shorturls{
	
		private $utils;//utils class
		private $login;
		private $format;
		private $service;
		private $timeout;
		private $username;
		private $password;
		private $api_key;
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->service = "bitly";			
			$this->timeout = 5;
			$this->set_service($this->service);
		}//end function
		
		public function set_service($service_name,$format = NULL){
			if($format != NULL){
				$this->format = $format;
			}//end if
			
			switch($service_name){
				case "bitly":
				default:
					$this->username = __BITLY_USERNAME__;
					$this->password = "";
					$this->api_key = __BITLY_API_KEY__;
					if($format == NULL){
						$this->format = "txt";
					}//end if
					break;
				case "tinycc":
					$this->username = __TINYCC_USERNAME__;
					$this->password = "";
					$this->api_key = __TINYCC_API_KEY__;
					if($format == NULL){
						$this->format = "json";
					}//end if
					break;
				case "tinyurl":
					$this->username = "";
					$this->password = "";
					$this->api_key = "";
					if($format == NULL){
						$this->format = "txt";
					}//end if
					break;
			}//end switch
		}//end function 
		
		private function get_curl_direct_result($api_url){
			$ch = @curl_init();
			@curl_setopt($ch,CURLOPT_URL,$api_url);
			@curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			@curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$this->timeout);
			$data = @curl_exec($ch);
			@curl_close($ch);
			return $data;
		}//end function
		
		public function get_short_url($long_url){
			switch($this->service){
				case "bitly":
				default:
					$api_url = 'http://api.bit.ly/v3/shorten?login='.$this->username.'&apiKey='.$this->api_key.'&uri='.urlencode($long_url).'&format='.$this->format;
					break;
				case "tinycc":						
					$api_url = 'http://tiny.cc/?c=rest_api&m=shorten&version=2.0.1&format='.$this->format.'&longUrl='.urlencode($long_url).'&login='.$this->username.'&apiKey='.$this->api_key;								
					break;
				case "tinyurl":
					$api_url = 'http://tinyurl.com/api-create.php?url='.$long_url;
					break;
			}//end switch
			$result = $this->get_curl_direct_result($api_url);
			
			if($this->format == "json" && $this->service == "tinycc"){
				$result = json_decode($result,true);				
				$result = $result['results'][$long_url]['short_url'];				
			}//end if
			return $result;
		}//end function
		
		public function get_long_url($short_url){
			switch($this->service){
				case "bitly":
				default:
					$api_url = 'http://api.bit.ly/v3/shorten?login='.$this->username.'&apiKey='.$this->api_key.'&shortUrl='.urlencode($short_url).'&format='.$this->format;
					break;
				case "tinycc":	
					$hash = $this->get_tinycc_hash($short_url);					
					$api_url = 'http://tiny.cc/?c=rest_api&m=expand&version=2.0.1&format='.$this->format.'&hash='.urlencode($hash).'&login='.$this->username.'&apiKey='.$this->api_key;					
					return false;
					break;
				case "tinyurl":
					//tinyurl has not this service
					return false;
					break;
			}//end switch
			$result = $this->get_curl_direct_result($api_url);
			if($this->format == "json" && $this->service == "tinycc"){
				$result = json_decode($result,true);				
				$result = $result['results'][$hash]['longUrl'];				
			}//end if
			return $result;
		}//end function
		
		private function get_tinycc_hash($tinycc_url){
			$hash = str_replace("http://tiny.cc/",$tinycc_url);
			return $hash;
		}//end function
	}//end if
?>
		