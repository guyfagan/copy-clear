<?php
	class comments{
		private $utils;//utils class
		private $t;//prefix before each tablename
		private $uid;//userid
		private $pid;//Post id
		private $cid;//Comment ID
		public $show_addfields;
		private $default_status;
		public $reverse;
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->module = "comments";
			$this->utils->read_params($this,$params);
			$this->default_status = 1;
			$this->reverse = false;
		}//endconstructor	
		
		public function set_default_status($ds){
			$this->default_status = $ds;
		}//end function
		
		public function reverse_module($boole){
			//if it's set to true, on the get_comments function I'll avoid the selected module and I'll show all the others
			if($boole){
				$this->reverse = true;
			} else {
				$this->reverse = false;
			}//end if
		}//end function
		
		public function get_comments($module = NULL,$pid = NULL,$status = NULL){
			if($module != NULL){
				$module_data = $this->utils->get_module($module);
				$module_name = $module_data['module_name'];
				$post_table = $module_data['module_table'];
				$post_prefix = $this->utils->get_field_prefix_from_table($post_table);
			} else {
				$post_table = "posts";
				$post_prefix = "post";
			}//end if
		
			$query = "SELECT ".$this->t."comments.*, ".$this->t."modules.*, ".$post_prefix."_title, ".$post_prefix."_id, user_username, user_id, user_firstname, user_surname, UNIX_TIMESTAMP(comment_date) AS comment_date
					  FROM (".$this->t."comments, ".$this->t."modules, ".$this->t.$post_table.")
					  LEFT JOIN  ".$this->t."users
					  ON user_id = comment_uid
					  WHERE comment_module_id = module_name
					  AND ".$post_prefix."_id = comment_post_id		
					  AND module_name = ".$post_prefix."_module_id";
			if($status != NULL){
				$query .= " AND comment_status = ".$status;
			}//end if
			if($module != NULL && $this->reverse == false){
				$query .= " AND module_name = '".$module_name."'";
			}//end if
			if($module != NULL && $this->reverse == true){
				$query .= " AND module_name != '".$module_name."'";
			}//end if
			if($pid != NULL){
				$query .= " AND ".$post_prefix."_id = ".$pid;
			}//end if
			
			$query .= " ORDER BY comment_date DESC";
		
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
			
		}//end function
		
		public function get_public_comments($module,$pid){
			$status = 1;//Mean active
			if($module != NULL){
				$module_data = $this->utils->get_module($module);
				$module_name = $module_data['module_name'];
				$post_table = $module_data['module_table'];
				$post_prefix = $this->utils->get_field_prefix_from_table($post_table);
			} else {
				$post_table = "posts";
				$post_prefix = "post";
			}//end if
		
			$query = "SELECT ".$this->t."comments.*, ".$this->t."modules.*, ".$post_prefix."_title, ".$post_prefix."_id, user_username, user_id, UNIX_TIMESTAMP(comment_date) AS comment_date
					  FROM (".$this->t."comments, ".$this->t."modules, ".$this->t.$post_table.")
					  LEFT JOIN  ".$this->t."users
					  ON user_id = comment_uid
					  WHERE comment_module_id = module_name
					  AND ".$post_prefix."_id = comment_post_id		
					  AND module_name = ".$post_prefix."_module_id
					  AND comment_status = ".$status;	
			if($module != NULL){
				$query .= " AND module_name = '".$module_name."'";
			}//end if
			if($pid != NULL){
				$query .= " AND ".$post_prefix."_id = ".$pid;
			}//end if
			
			$query .= " ORDER BY comment_date ASC";		
		
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_user_comments($user_id,$status = NULL,$module = NULL,$limit = NULL){
			if($module != NULL){
				$module_data = $this->utils->get_module($module);
				$module_name = $module_data['module_name'];
				$post_table = $module_data['module_table'];
				$post_prefix = $this->utils->get_field_prefix_from_table($post_table);
			} else {
				$post_table = "posts";
				$post_prefix = "post";
			}//end if
			$query = "SELECT ".$this->t."comments.*, ".$this->t."modules.*, ".$post_prefix."_title, ".$post_prefix."_id, user_username, user_id, UNIX_TIMESTAMP(comment_date) AS comment_date
					  FROM (".$this->t."comments, ".$this->t."modules, ".$this->t.$post_table.")
					  LEFT JOIN ".$this->t."users
					  ON user_id = comment_uid
					  WHERE comment_module_id = module_name
					  AND comment_uid = ".$user_id."
					  AND ".$post_prefix."_id = comment_post_id					  
					  AND module_name = ".$post_prefix."_module_id";
			if($status != NULL){
				$query .= " AND comment_status = ".$status;
			}//end if
			if($module != NULL && $this->reverse == false){
				$query .= " AND ".$post_prefix."_module_id = '".$module_name."'";
			}//end if
			if($module != NULL && $this->reverse == true){
				$query .= " AND ".$post_prefix."_module_id != '".$module_name."'";
			}//end if
			$query .= " ORDER BY comment_date ASC";
			if($limit != NULL){
				$query .= " LIMIT ".$limit;
			}//end if
			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_comments_by_status($status,$module = NULL){
			if($module != NULL){
				$module_data = $this->utils->get_module($module);
				$module_name = $module_data['module_name'];
				$post_table = $module_data['module_table'];
				$post_prefix = $this->utils->get_field_prefix_from_table($post_table);
			} else {
				$post_table = "posts";
				$post_prefix = "post";
			}//end if
			$query = "SELECT ".$this->t."comments.*, ".$this->t."modules.*, ".$post_prefix."_title, ".$post_prefix."_id, user_username, user_id, UNIX_TIMESTAMP(comment_date) AS comment_date
					  FROM (".$this->t."comments, ".$this->t."modules, ".$this->t.$post_table.")
					  LEFT JOIN ".$this->t."users
					  ON user_id = comment_uid
					  WHERE comment_module_id = module_name
					  AND ".$post_prefix."_id = comment_post_id					  
					  AND module_name = ".$post_prefix."_module_id 
					  AND comment_status = ".$status;
			if($module != NULL && $this->reverse == false){
				$query .= " AND ".$post_prefix."_module_id = '".$module_name."'";
			}//end if
			if($module != NULL && $this->reverse == true){
				$query .= " AND ".$post_prefix."_module_id != '".$module_name."'";
			}//end if
			$query .= " ORDER BY comment_date ASC";
			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function process_activation(){
			$comments_action = $_REQUEST['comment_action'];	
			$comments_id = $_REQUEST['comment_id'];
			if(is_array($comments_id) && is_array($comments_action)){
				//Read all the elements
				for($i = 0; $i < sizeof($comments_id); $i++){
					//If is different from null or 0, execute the change				
					if($comments_action[$i] != NULL){
						if($comments_action[$i] != 3){
							$query = "UPDATE ".$this->t."comments SET 
									  comment_status = ".$comments_action[$i]."
									  WHERE comment_id = ".$comments_id[$i];	
						} else {
							$query = "DELETE FROM ".$this->t."comments
									  WHERE comment_id = ".$comments_id[$i];	
						}//end if							
						$result = @mysql_query($query,$this->utils->db);
						$this->utils->error($result,__LINE__,get_class($this));	
					}//end if
				}//end for
			} else {
				return false;
			}//end if
			return true;
		}//end function
		
		public function get_status_string($status_code){
			switch($status_code){
				case "0":
					return __STATUS_STANDBY__;
					break;
				case "1":
					return __STATUS_ACTIVE__;
					break;
				case "2":
					return __STATUS_SPAM__;
					break;
				case "3":
					return __STATUS_DELETED__;
					break;
			}//end switch
		}//end function
		
		public function get_post($id){
			return $this->get_comment($id);
		}//end function
		
		public function get_comment($cid){		
		
			$query = "SELECT ".$this->t."comments.*, ".$this->t."modules.*, post_title, post_id, 
					  user_username, user_id, UNIX_TIMESTAMP(comment_date) AS comment_date
					  FROM (".$this->t."comments, ".$this->t."modules, ".$this->t."posts)
					  LEFT JOIN ".$this->t."users
					  ON user_id = comment_uid
					  WHERE comment_module_id = module_name
					  AND post_id = comment_post_id					 
					  AND module_name = post_module_id
					  AND comment_id = ".$cid;
	
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);
				return $data;
			} else {
				return false;
			}//end if			
		}//end function
		
		public function get_post_comments_count($pid){
			$data = $this->get_comments(NULL,$pid);
			if($data == false){
				return "0";
			} else {
				return sizeof($data);
			}
		}//end function
		
		public function new_comment(){
			$params = func_get_args();
			foreach($params[0] as $key => $value){
				${$key} = $value;
			}//end if
			
			if($userid == NULL){
				//if in anonymous mode, check nickname and email fields
				if($nickname == NULL || $email == NULL){
					return false;
				}//end if
			}//end if
			
			//check if the message field is empty
			if(strlen(strip_tags($message)) == 0){
				return false;
			}//end if
			
			$active = $this->default_status;
			$message = nl2br($message);
			$message = strip_tags($message,"<br><a>");		
			//check if there are any link in the message
			if(eregi("(<a [a-zA-Z0-9])",$message)){
				$active = 0;
			}//end if
	
			//check for bad words
			$badwords = $this->utils->call("badwords");
			//check if the class exists
			/*
			if($badwords != false){
				//var_dump($badwords->check_bad_words($message));
				if($badwords->check_bad_words($message) == true){
					//put the comment under moderation
					$active = 0;
				}//end if
			}//end if
			*/
						
			$ip = $_SERVER['REMOTE_ADDR'];
			if($userid == NULL){
				$query = "INSERT INTO ".$this->t."comments
						  (comment_nickname, comment_email, comment_post_id, comment_title, comment_message, comment_ip, comment_module_id, comment_status)
						  VALUES
						  ('".$nickname."','".$email."',".$id.",'".$title."','".$message."','".$ip."','".$section."',".$active.")";
			} else {
				$query = "INSERT INTO ".$this->t."comments
						  (comment_uid, comment_post_id, comment_title, comment_message, comment_ip, comment_module_id, comment_status)
						  VALUES
						  (".$userid.",".$id.",'".$title."','".$message."','".$ip."','".$section."',".$active.")";
			}//end if			
			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));
			$this->cid = @mysql_insert_id();
			
			//Check if send an email to the administrator
			$settings = $this->utils->get_settings($this->module);
			if($settings['notify_on_comment'] == 1 && $settings['notify_email'] != NULL){
				$users = $this->utils->call("users");
				$ewrite = $this->utils->call("ewrite");
				$udata = $users->get_user_data($userid);//User data
				$pdata = $ewrite->get_post($id);
				$headers = "From: ".__NOREPLY_EMAIL__;
				$subject = "New comment on ".__DOMAIN_NAME__." from ".$udata['user_firstname']." ".$udata['user_surname'];
				$mail_message = $udata['user_firstname']." ".$udata['user_surname']." sent a new comment  on the page \"".$pdata['page_title']."\"\n\r";
				$mail_message .= "His comment was:\n\r\"".$message."\"\n\r";
				$mail_message .= "This comment is currently ";
				if($active == 1){
					$mail_message .= "active";
				} else {
					$mail_message .= "moderated as it contains a link or a bad word";
				}//endif
				@mail($settings['notify_email'],$subject,$mail_message,$headers);
			}//end if
			
			return true;	
		}//end function
		
		public function get_comment_id(){
			return $this->cid;
		}//end function
		
		public function delete_comment($cid){
			if(!is_array($cid)){
				$query = "DELETE FROM ".$this->t."comments WHERE comment_id = ".$cid;
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
			} else {
				for($i = 0; $i < sizeof($cid); $i++){
					$query = "DELETE FROM ".$this->t."comments WHERE comment_id = ".$cid[$i];
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));
				}//end for
			}//end if
			return true;
		}//end function
		
		//Alias of delete_comments
		public function delete_post($cid){
			return $this->delete_comment($cid);
		}//end function
	}//end class
?>