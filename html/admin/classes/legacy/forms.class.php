<?php
	class forms{
		private $utils;//utils class
		private $t;//prefix before each tablename
		private $fid;//form ID
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;
			$this->module = "forms";
			$this->utils->read_params($this,$params);
		}//endconstructor	
		
		public function save_form(){
			$fid = $_POST['id'];
			$nickname = $_POST['form_nickname'];
			$name = $_POST['form_name'];
			$recipient = $_POST['form_recipient'];
			$action_uri = $_POST['form_action_uri'];
			$descr = $_POST['form_description'];
			$tpl_file = $_POST['form_tpl_file'];
				
			if(!is_numeric($fid) && $fid == NULL){
				//Save the new form
				$query = "INSERT INTO ".$this->t."forms
						  (form_name,form_description,form_action_uri,form_tpl_file, form_recipient, form_nickname)
						  VALUES
						  ('".$this->utils->strtodb($name)."','".$this->utils->strtodb($descr)."','".$this->utils->strtodb($action_uri)."','".$this->utils->strtodb($tpl_file)."','".$this->utils->strtodb($recipient)."','".$this->utils->strtodb($nickname)."')";
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
				$this->fid = @mysql_insert_id();
			} else {
				//Update the current form
				$query = "UPDATE ".$this->t."forms
						  SET form_name = '".$this->utils->strtodb($name)."'
						  ,form_description = '".$this->utils->strtodb($descr)."'
						  ,form_action_uri = '".$this->utils->strtodb($action_uri)."'
						  ,form_tpl_file = '".$this->utils->strtodb($tpl_file)."'
						  ,form_nickname = '".$this->utils->strtodb($nickname)."'
						  ,form_recipient = '".$this->utils->strtodb($recipient)."'
						  WHERE form_id = ".$fid;
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
				$this->fid = $fid;
			}
			
			//echo $query;
			//now get the fields
			$fields = $_POST['newfield_data'];
				
			if(is_numeric($fid) && $fid != NULL){
				$this->remove_form_fields($fid);	
			}//end if		
						
			if(is_array($fields) && sizeof($fields) > 0){
				for($i = 0; $i < sizeof($fields); $i++){
					$field = unserialize(base64_decode(urldecode($fields[$i])));
					$value = $field['value'];
					$value_type = "string";
					if($value != NULL){
						$dec_value = unserialize(base64_decode($value));
						if(is_array($dec_value)){
							$value_type = "array";					
						}//end if
					}//end if
					//once we get the data, store them in the database
					$query = "INSERT INTO ".$this->t."forms_fields
							  (form_field_name, form_field_label, form_field_form_id, form_field_value, form_field_type, form_field_value_type, form_field_mandatory)
							  VALUES
							  ('".$field['name']."','".$field['label']."',".$this->fid.",'".$field['value']."','".$field['type']."','".$value_type."', '".$field['mandatory']."')";
							 // echo "<br />".$query;
					$result = @mysql_query($query,$this->utils->db);
					$this->utils->error($result,__LINE__,get_class($this));
					//var_dump($field);
					//echo '<br />--------<br />';
				}//end for i
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		
		public function remove_form_fields($fid){
				$query = "DELETE FROM ".$this->t."forms_fields
						  WHERE form_field_form_id = ".$fid;
				$result = @mysql_query($query,$this->utils->db);
				$this->utils->error($result,__LINE__,get_class($this));
			if($error == false){
				return true;
			} else {
				return false;
			}//end if
		}
		
		public function get_forms(){
			$query = "SELECT *, UNIX_TIMESTAMP(form_date) AS form_date FROM ".$this->t."forms ORDER BY form_name ASC";			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_form($fid){
			$query = "SELECT *, UNIX_TIMESTAMP(form_date) AS form_date FROM ".$this->t."forms WHERE form_id = ".$fid;			
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);
				return $data;
			} else {
				return false;
			}//end if
		}//end function	
		
		public function get_form_recipient($fid){
			$query = "SELECT form_recipient FROM ".$this->t."forms WHERE form_id = ".$fid." LIMIT 1";		
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);
				return $data['form_recipient'];
			} else {
				return false;
			}//end if
		}//end function	

		public function get_form_html($fid,$category,$custom_fields = NULL){
			if ($_GET['postdata']){ 
				$postdata = $_GET['postdata']; 
				$post_data = unserialize(base64_decode(urldecode($postdata))); 
			}
			$fdata = $this->get_form($fid);
			if($fdata['form_action_uri'] != NULL){ $action = $fdata['form_action_uri']; } else { $action = "process.php"; }
			//if($fdata['form_description'] != NULL){ $html .= '<h3>'.stripslashes(html_entity_decode($fdata['form_description'])).'</h3>'; }
			$html .= '<form action="'. __BASEURL__ . $action .'" method="post" name="newform_'.$fid.'" id="newform_'.$fid.'">';
			$html .= '<input type="hidden" name="fid" id="fid" value="'.$fid.'" />';
			//$html .= '<input type="hidden" name="from_page" id="from_page" value="" />';

			/*if($fdata['form_recipient'] != NULL){
				$html .= '<input type="hidden" name="recipient" id="recipient" value="'.$fdata['form_recipient'].'" />';
			}//end if*/
			
			if($fdata['form_tpl_file'] != NULL){
				///$handle = fopen(__BASEURL__.'content/forms/'.$fdata['form_tpl_file'],"r");
				$filename =  'content/forms/'.$fdata['form_tpl_file'];
				$filename =  $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.$filename;
				$file = fopen($filename,"r");
				$handle = fread($file,filesize($filename));
				
				//blow it up
				$kablooie = explode("{#", $handle);
				if (sizeof($kablooie)>0){
					for ($x=0;$x<sizeof($kablooie);$x++){
						//into tiny pieces
						$smidirini = explode("#}",$kablooie[$x]);
						if ($x>0){
							$smidirini[0] = stripslashes(html_entity_decode($post_data[$smidirini[0]]));
						}
						for ($j=0;$j<sizeof($smidirini);$j++){
							//we can rebuild it
							$newHTML .= $smidirini[$j];
						}
					}
				}
				
				$html .= $newHTML;
				fclose($file);
				
			}//end if
			//add hidden fields
			if($custom_fields != false){
				$html .= $custom_fields;
			}//end if
			//get the form fields
			$ff = $this->get_form_fields($fid);
			if($ff != false){
				for($i = 0; $i < sizeof($ff); $i++){
					//print_r($post_data);
					//echo $ff[$i]['form_field_name'];
					$html .= $this->get_field_html($ff[$i],$category,stripslashes($post_data[$ff[$i]['form_field_name']]));			
				}//end for i
			}//end if
			$html .= '<div class="req_text">* indicates required field</div>';

			$html .= '<input type="submit" value="Send" id="form_submit" />';
			$html .= '</form>';

			/*$html .= '<script type="text/javascript">
						$().ready(function(){
						$("#newform_'.$fid.'").validateForm({ error_class : "error", useImpromptu : true});
						});
					</script>';*/
			
			return $html;
		}//end function 
		
		private function get_field_html($field_data,$category,$post_value){
			if ($field_data['form_field_mandatory']==1) {
				$asterisk = "*";
				$req = " required";
				$html = '<input type="hidden" name="required_'.$field_data['form_field_name'].'" id="required_'.$field_data['form_field_name'].'" />';
			}
			//print_r($postdata);
			switch($field_data['form_field_type']){
				case "text":
					$html .= '<label for="'.$field_data['form_field_name'].'">'.$field_data['form_field_label'].$asterisk.'</label>';
					$html .= '<input value="'.$post_value.'" type="text" class="std'.$req.'" maxlength="255" name="'.$field_data['form_field_name'].'" id="'.$field_data['form_field_name'].'" />';
					
					break;
				case "textarea":
					$html .= '<label for="'.$field_data['form_field_name'].'">'.$field_data['form_field_label'].$asterisk.'</label>';
					$html .= '<textarea rows="2" class="'.$req.'" name="'.$field_data['form_field_name'].'" id="'.$field_data['form_field_name'].'">'.$post_value.'</textarea>';
					break;
				case "select":
					$html .= '<label for="'.$field_data['form_field_name'].'">'.$field_data['form_field_label'].$asterisk.'</label>';
					$html .= '<select class="'.$req.'" name="'.$field_data['form_field_name'].'" id="'.$field_data['form_field_name'].'">';			
					
					$html .= '<option value="#">Please Select</option>';
					//if the select name = product_list, get the list of products from the category, else
					if ($field_data['form_field_name']=="product_list"){
						$diatec = $this->utils->call("diatec");
						$diatec->force_module("cad");
						$options = $diatec->get_items(array("status" => 1, "category_id" => $category));
						if ($options!=NULL){
							for ($z=0;$z<sizeof($options);$z++){
								if ($options[$z]['label']!="Introduction"){
									$options[$z]['id'] == $post_value ? $sel = ' selected="selected"' : $sel=NULL;
									$label = $diatec->truncate($options[$z]['label'],35,$etc = "...");
									$html .= '<option value="'.$options[$z]['id'].'"'.$sel.'>'.$label.'</option>';
								}
							}
						}
					} else if ($field_data['form_field_name']=="category_list"){
						$diatec = $this->utils->call("diatec");
						$diatec->force_module("cad");
						$options = $diatec->get_items(array("status" => 1, "category_id" => $category));
						if ($options!=NULL){
							$options[0]['label']=="Introduction" ? $start = 1 : $start = 0;
							for ($z=$start;$z<sizeof($options);$z++){
								$subs = $diatec->get_items(array("status" => 1, "category_id" => $options[$z]['id']));
								if ($subs!=NULL){
									$subs[0]['label']=="Introduction" ? $sub_start = 1 : $sub_start = 0;
									$product_drop .= '<optgroup label="'.$diatec->truncate($options[$z]['label'],35,$etc = "...").'">';
									for ($x=$sub_start;$x<sizeof($subs);$x++){
										$subs[$x]['id'] == $post_value ? $sel = ' selected="selected"' : $sel=NULL;
										$product_drop .= '<option value="'.$subs[$x]['id'].'"'.$sel.'>'. $diatec->truncate($subs[$x]['label'],35,$etc = "...") . '</option>';
									}//end for x									
									$product_drop .= '</optgroup>';
								}//end if
							}//end for z
							$html .= $product_drop;
						}//end if	
					} else {					
						$options = $field_data['form_field_value'];
						$options = unserialize(base64_decode(urlencode($options)));
						foreach($options as $key => $value){
							$html .= '<option value="'.$value.'"';
							if($post_value == $value){
								$html .= ' selected="selected"';
							}//end if	
							$html .= '>'.stripslashes(html_entity_decode($key)).'</option>';
						}//end foreach					
					}//end if
				
					$html .= '</select>';		
					break;
			}//end switch
			return $html;
		}//end function
		
		public function get_form_fields_code($fid){
			$query = "SELECT * FROM ".$this->t."forms_fields WHERE form_field_form_id = ".$fid. " ORDER BY form_field_id ASC";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_form_fields($fid){
			$query = "SELECT * FROM ".$this->t."forms_fields WHERE form_field_form_id = ".$fid." ORDER BY form_field_id ASC";
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,false);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_form_field($field_id){
			$query = "SELECT * FROM ".$this->t."forms_fields WHERE form_field_id = ".$field_id;
			$result = @mysql_query($query,$this->utils->db);
			$this->utils->error($result,__LINE__,get_class($this));	
			$num = @mysql_num_rows($result);
			if($num > 0){							
				$data = $this->utils->get_result_array($result,true);
				return $data;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_encrypted_field($pdata = "POST"){
			//get the data			
			if(!is_array($pdata)){
				$pdata = $_POST;
				$type = $pdata['type'];
				$name = $pdata['newfield_name'];
				$label = $pdata['newfield_label'];
				$mandatory = $pdata['newfield_mandatory'];
			} else {
				$type = $pdata['form_field_type'];
				$name = $pdata['form_field_name'];
				$label = $pdata['form_field_label'];
				$mandatory = $pdata['form_field_mandatory'];
			}
			//create the array
			$data = array();
			//push the data in the array			
			$data['type'] = $type;
			$data['name'] = $name;
			$data['label'] = $label;
			$data['mandatory'] = $mandatory;
			//check the type
			switch($type){
				case "select":
					$select_values = $this->read_select_fields();
					if($select_values != NULL){
						$data['value'] = base64_encode(serialize($select_values));
					}//end if
					break;
			}//end switch
			//serialize and encrypt the array
			$data = urlencode(base64_encode(serialize($data)));
			return $data;
		}//end function
		
		private function read_select_fields(){
			$values = $_POST['newfield_select_value'];
			$labels = $_POST['newfield_select_label'];
			if(is_array($values) && sizeof($values) > 0){
				$data = array();
				for($i = 0; $i < sizeof($values); $i++){
					if(trim($values[$i]) != NULL){
						if($labels[$i] == NULL){
							$label = $this->utils->unifystring($values[$i]);
						} else {
							$label = $labels[$i];
						}//end if
						$data[$label] = $values[$i];
					}//end if
				}//end for i
				return $data;
			} else {
				return NULL;
			}//end if
		}//end function
		
		public function delete_form($fid){
			$query = "DELETE FROM ".$this->t."forms WHERE form_id = ".$fid;
			$result = @mysql_query($query,$this->utils->db);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_behaviors(){
			$query = "SELECT * FROM ".$this->t."forms_behaviors ORDER by form_behavior_label ASC";
			$result = @mysql_query($query,$this->utils->db);
			$error = $this->utils->error($result,__LINE__,get_class($this));
			if($error == false){
				$num = @mysql_num_rows($result);
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
	}//end class
?>