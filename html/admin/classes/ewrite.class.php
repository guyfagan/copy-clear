<?php
	#################################################################
	# Ewrite Class - 	last update 22/12/12						#
	#					created in 2008								#
	# This is a core class for the Old Hat CMS						#	
	#																#
	# 16/10/12 - Moved some old and useless functions to the legacy #
	#			 class, which extends this class in case we need it	#
	# 		   - Added more functions to support the meta class, 	#
	#			 like now it's possible to optionally get the meta  #
	#			 in the get_posts() method							#	
	#		   - Added support to meta class on get_post method as  #
	#			 well												#	
	# 21/11/12 - Added support for html meta tags					#	
	#																#
	# -------- IMPORTANT!	--------------------------------------- #	
	# From 06/12/12 this class fork to a new branch of development  #
	# and it will be no more compatible with old version of the cms #
	# so DO NOT USE IT on previous version of the cms				#
	# ------------------------------------------------------------- #
	#																#
	# 06/12/12 - Removed old functions, removed any reference to    #
	# 			 to categories_rel, and added the function 			#
	#			 get_category_id().									#
	#		   - Now the method get_items() returns if a row is post#
	#		     or a category with the field "is_category"			#	
	# 21/12/12 - Started converting to PDO							#	
	# 22/12/12 - End convertion to PDO								#	
	#################################################################

	class ewrite{
		protected $utils;//utils class
		protected $uid;//userid
		protected $pid;//post id	
		protected $cid;//category id	
		protected $fcid;//father category id
		protected $gid;//group id
		public $errors;
		public $module; //module
		public $module_id; //Module id number		
		protected $settings;
		protected $meta;
		public $table = 'posts';
		public $prefix = 'post';
		
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->module = "ewrite";			
			$this->utils->read_params($this,$params);	
			/*$module_data = $this->utils->get_module($this->module);		
			if($module_data !== false){
				$this->module_id = $module_data['module_id'];		
				$this->table = $module_data['module_table'];			
			}//end if		*/			
			//Meta init	
			$this->meta = $this->utils->call("meta");
			//$this->meta->set_meta_module($this->module);		
			//set the module
			$this->set_module($this->module);	
		}//endconstructor	
		
		public function set_module($module){
			$module_data = $this->utils->get_module($module);
			if($module_data !== false){
				$this->module = $module_data['module_name'];
				$this->module_id = $module_data['module_id'];
				$this->table = $module_data['module_table'];	
				if(isset($this->meta) && is_object($this->meta)){
					$this->meta->set_meta_module($this->module);
				}//end if
			}//end if
		}//end function
								
		public function get_active_months(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($sort)){
				$sort = "post_date DESC";	
			}//end if
			
			//Time split could be NULL, FUTURE, PAST
			$query = "SELECT MONTH(post_date) AS post_month, YEAR(post_date) AS post_year
					  FROM posts";			
			if($category_id != NULL){
				$query .= " LEFT JOIN modules_items_order
					  ON (module_item_order_item_id = post_id AND module_item_order_type = 'post')
					  LEFT JOIN categories
					  ON (category_id = module_item_order_father_id) ";
			}//end if
			$query .= " WHERE post_module_id = '".$this->module."'
						AND post_status != 0";
			if($category_id != NULL){
				$query .= " AND categories.category_id = ".$category_id;
			}//end if	
			if($time_split != NULL){
				if($time_split == "future"){
					$query .= " AND post_date >= NOW()";
				}//end if
				if($time_split == "past"){
					$query .= " AND post_date < NOW()";
				}//end if
			}//end if	  	
			$query .= " GROUP BY YEAR(post_date), MONTH(post_date)
					  ORDER BY ".$sort;	
						
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
	
		public function get_active_years($cid = NULL){
			$query = "SELECT YEAR(post_date) AS post_year
					  FROM posts";			
			if($cid != NULL){
				$query .= " LEFT JOIN modules_items_order
					  ON (module_item_order_item_id = post_id AND module_item_order_type = 'post')
					  LEFT JOIN categories
					  ON (category_id = module_item_order_father_id) ";
			}//end if
			$query .= " WHERE post_module_id = '".$this->module."'
						AND post_status != 0";
			if($cid != NULL){
				$query .= " AND categories.category_id = ".$cid;
			}//end if		  	
			$query .= " GROUP BY YEAR(post_date)
					  ORDER BY post_date DESC";	
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
				
		public function update_post_dependencies($id = NULL,$type = "post"){
			//Post update
			if($type == "post"){
				if($id == NULL){
					$id = $this->pid;
				}//end if
				$post_data = $this->get_post($id);			
				$module_id = $post_data['post_module_id'];
				$subpages = $this->get_post_subpages($id);
				//all read the subpages for this post
				if($subpages != false){
					//if exists, update all the module id
					for($i = 0; $i < sizeof($subpages); $i++){
						$this->utils->update_post_field(array('post_id' => $id, 'field' => 'post_module_id','value' => $module_id));					
					//	$this->synch_attachments($subpages[$i]['post_id'],$subpages[$i]['post_module_id'],$module_id);
					}//end for
				}//end if
			}//end if
			
			//Post category update
			
			if($type == "category"){
				if($id == NULL){
					$id = $this->cid;
				}//end if
				$cat_data = $this->get_category($id);		
				if($_REQUEST['module_id'] == NULL){	
					$module_id = $cat_data['category_module_id'];
				} else {
					$module_id = $_REQUEST['module_id'];
				}//end if
				$module_data = $this->utils->get_module($module_id);
				$module_name = $module_data['module_name'];
				//Check for the correlated posts
				$params = array("cid" => $id);
				$posts = $this->get_posts($params);				
				//For each post I update the module id
				
				if($posts != false){
					for($j = 0; $j < sizeof($posts); $j++){
						//$this->utils->update_post_field($posts[$j]['post_id'],"post_module_id",$module_name);
						//$this->synch_attachments($posts[$j]['post_id'],$posts[$j]['post_module_id'],$module_id);
					}//end for
				}//end if	
				
				$this->set_category_father_id($id);
				$subcats = $this->get_categories();
				//check if exists at least one sub category
				if($subcats != false){
					//Update the module of the selected category
					$query = "UPDATE categories SET category_module_id = ".$module_id." WHERE category_father_id = ".$id;
					$result = $this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));
					//Now update all the elements under this category
					for($i = 0; $i < sizeof($subcats); $i++){						
						$cid = $subcats[$i]['category_id'];		
						$params = array("cid" => $cid);
						$posts = $this->get_posts($params);
						
						//For each post I update the module id
						if($posts != false){
							for($j = 0; $j < sizeof($posts); $j++){
								//$this->utils->update_post_field($posts[$j]['post_id'],"post_module_id",$module_name);
							//	$this->synch_attachments($posts[$j]['post_id'],$posts[$j]['post_module_id'],$module_id);
							}//end for
						}//end if			
					}//end for
				}//end if				
			}//end if			
		}//end function		
		
		private function clean_from_sorting_duplicates($pid,$type,$category){
			$query = "SELECT * FROM modules_items_order
							  WHERE module_item_order_module_id = ".$this->module_id."
							  AND module_item_order_item_id = ".$pid."
							  AND module_item_order_type = '".$type."'
							  AND module_item_order_father_id = ".$category."
							  ORDER BY module_item_order_id DESC";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 1){
					$i = 0;
					while($row = $result->fetch(PDO::FETCH_ASSOC)){
						if($i > 0){
							$delete = "DELETE FROM modules_items_order WHERE module_item_order_id = ".$row['module_item_order_id'];
							$result = $this->utils->db->query($delete);
							$this->utils->error($result,__LINE__,get_class($this));
						}//end if
						$i++;
					}//end while
				}//end if
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function reset_module_sorting(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			//before doing anything, let's check what's the sorting order in the settings for this module
			
			$settings = $this->utils->get_settings(array("module_id" => $this->module_id));
			if($settings !== false && isset($settings['list_order'])){
				$order = $settings['list_order'];
			} else {
				$order = "ASC";
			}//end if
			
			$options = array("sort_by" => "post_date", "sort_order" => $order);
			
			if(!isset($parent_id)){
				$parent_id = "0";	
			} else {
				$options['category_id'] = $parent_id;
			}//end if
						
			$list = $this->get_posts($options);
			if($list !== false){
				$x = sizeof($list);
				for($i = 0; $i < sizeof($list); $i++){
					$query = "UPDATE modules_items_order SET
							  module_item_order_order = ".$x."
							  WHERE module_item_order_module_id = ".$this->module_id."
							  AND module_item_order_item_id = ".$list[$i]['post_id']."
							  AND module_item_order_type = 'post'
							  AND module_item_order_father_id = ".$parent_id;														
					$result = $this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));	
					$x--;
				}//end for i
			} else {
				return false;	
			}//end if
		}//end function
		
		public function save_module_sorting(){		
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($ids) || !isset($types)){
				return false;	
			}//end if			
			
			if(!isset($parent_id)){
				$parent_id = "0";	
			}//end if
			
			//we need to down by 1 point if it's set
			if(!isset($page)){
				$page = 0;
			} else {
				$page--;
			}//end if
			
			if(!isset($items_per_page)){
				$items_per_page = 20;
			}//end if
			
			if(!isset($total_items)){
				$total_items = 0;	
			}//end if
			
			//before doing anything, let's check what's the sorting order in the settings for this module
			$settings = $this->utils->get_settings(array("module_id" => $this->module_id));
			if($settings !== false && isset($settings['list_order'])){
				$order = $settings['list_order'];
			} else {
				$order = "ASC";
			}//end if		
			
			
			if(is_array($ids) && sizeof($ids) > 0){
				for($i = 0; $i < sizeof($ids); $i++){
					if($types[$i] == "1"){
						$type = "category";
					} else {
						$type = "post";
					}//end if
					//get the index according to the page number and the elements shown per page, plus the current value of $i
					if($order == "ASC"){
						$index = ($page * $items_per_page) + $i;					
					} else {					
						$index = $total_items - (($page * $items_per_page) + $i);
					}//end if
					//clean from duplicates
					$this->clean_from_sorting_duplicates($ids[$i],$type,$parent_id);
					//Check if exists
					$query = "SELECT * FROM modules_items_order
							  WHERE module_item_order_module_id = ".$this->module_id."
							  AND module_item_order_item_id = ".$ids[$i]."
							  AND module_item_order_type = '".$type."'
							  AND module_item_order_father_id = ".$parent_id."
							  ORDER BY module_item_order_id DESC
							  LIMIT 1";
								
					$result = $this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));
					$num = $result->rowCount();					
					if($num == 1){
						//Perform an update		  
						$query = "UPDATE modules_items_order SET
								  module_item_order_order = ".$index."
								  WHERE module_item_order_module_id = ".$this->module_id."
								  AND module_item_order_item_id = ".$ids[$i]."
								  AND module_item_order_type = '".$type."'
								  AND module_item_order_father_id = ".$parent_id;														
						$result = $this->utils->db->query($query);
						$this->utils->error($result,__LINE__,get_class($this));	
					} else if($num == 0){
						//If no record  was found in the db, a create a new one					
						$query = "INSERT INTO modules_items_order 
								 (module_item_order_module_id, module_item_order_item_id, module_item_order_type, module_item_order_order,
								  module_item_order_father_id)
								  VALUES
								  (".$this->module_id.",".$ids[$i].",'".$type."',".$index.",".$parent_id.")";					
						$result = $this->utils->db->query($query);
						$this->utils->error($result,__LINE__,get_class($this));	
					} else {											
						return false;
					}//end if
				
				}//end for
				return true;
			} else {
				return false;
			}//end if		
		}//end function
		
		public function get_items(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
		
			if($sort_by == NULL){
				$sort_by = "item_order";
			}//end if
			
			if($sort_order == NULL){					
				$this->settings = $this->utils->get_settings(array('module_id' => $this->module_id));			
				$sort_order = $this->settings['list_order'];
			}//end if	
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $this->module_id;
				$module_name = $this->module;	
			}//end if
			
			if(!isset($fields)){
				$query_posts_fields = "post_id as id, post_title as label, user_username, post_uid as user_id, module_item_order_order as item_order, 
								post_status as status, UNIX_TIMESTAMP(post_date) as item_date, post_lang as lang_id, post_permalink as permalink, @is_category := 0 AS is_category, post_type AS type, post_link_to AS link_to";
				$query_categories_fields = "category_id as id, category_name as label, category_father_id, category_uid as user_id, module_item_order_order as item_order, category_status as status, UNIX_TIMESTAMP(category_date) as item_date, category_lang as lang_id, category_permalink as permalink, @is_category := 1 AS is_category, @type := 'category' AS type, @link_to := NULL AS link_to";
			} else {
				if(is_array($fields['posts'])){				
					$fields['posts'] = implode(', ',$fields['posts']);	
				}//end if
				$query_posts_fields = $fields['posts'];
				if(is_array($fields['categories'])){
					$fields['categories'] = implode(', ',$fields['categories']);	
				}//end if
				$query_categories_fields = $fields['categories'];
			}//end if
			
			if(isset($group_id)){
				$query_posts_fields .= ", users_groups_rel_gid AS group_id";	
				$query_categories_fields .= ", users_groups_rel_gid AS group_id";
			}//end if
			
			$query_posts = "SELECT ".$query_posts_fields."
						  FROM (posts, modules_items_order)
						  LEFT JOIN users
						  ON (users.user_id = posts.post_uid) 					
						";						
			if($group_id){
				if(!is_array($group_id)){
					$query_posts .= " LEFT JOIN  users_groups_rel
									  ON (post_uid = users_groups_rel_uid
									  AND users_groups_rel_gid = ".$group_id.")";
				} else {
					$query_posts .= " LEFT JOIN  users_groups_rel
									  ON (post_uid = users_groups_rel_uid
									  AND users_groups_rel_gid IN (".implode(",",$group_id)."))";
				}
			}//end if
			if(isset($meta_filter)){
				$query_posts .= "LEFT JOIN meta
						   ON (meta_type = 'post' 
						   AND meta_ref_id = post_id 
						   AND meta_module_id = module_item_order_module_id 
						   AND meta_label = '".$meta_filter['label']."'
						   AND meta_value = '".$meta_filter['value']."') ";
			}//end if			  					  
			$query_posts .= " WHERE post_module_id = '".$module_name."' 
							  AND post_temp = 0 
							  AND module_item_order_module_id = ".$module_id."
							  AND module_item_order_type = 'post'
							  AND module_item_order_item_id = post_id ";	 
			if($category_id == NULL){
				$query_posts .= " AND (module_item_order_order IS NOT NULL)
								  AND (module_item_order_father_id IS NULL OR module_item_order_father_id = 0)";
			} else {
				$query_posts .= " AND (module_item_order_father_id = ".$category_id." OR module_item_order_father_id IS NULL)";
			}//end if	
			if($status != NULL){
				if($status < 2){
					$query_posts .= " AND post_status = ".$status;
				} else {
					$query_posts .= " AND (post_status = 1 OR post_status = 2)";
				}//end if
			}//end if
			
			if(isset($meta_filter)){
				$query_posts .= " AND meta_id IS NOT NULL";	
			}//end if
			
			if(isset($from_date)){
				$query_posts .= " AND DATE(post_date) >= DATE('".$from_date."')";
			}//end if
			
			if(isset($to_date)){
				$query_posts .= " AND DATE(post_date) <= DATE('".$to_date."')";	
			}//end if	
			
			if(isset($user_id)){
				if(!isset($group_id)){
					$query_posts .= " AND post_uid = ".(int)$user_id;	
				} else {
					if(!is_array($group_id)){
						$query_posts .= " AND (post_uid = ".(int)$user_id." OR (users_groups_rel_gid = ".$group_id;	
					} else {
						$query_posts .= " AND (post_uid = ".(int)$user_id." OR (users_groups_rel_gid IN (".implode(",",$group_id).")";	
					}//end if
					if(isset($group_status)){
						$query_posts .= " AND post_status = 1";
					}//end if
					$query_posts .= "))";
				}//end if
			} else if(!isset($user_id) && isset($group_id)){
				if(!is_array($group_id)){
					$query_posts .= " AND (users_groups_rel_gid = ".$group_id;	
				} else {
					$query_posts .= " AND (users_groups_rel_gid IN (".implode(",",$group_id).")";		
				}//end if		
				if(isset($group_status)){
					$query_posts .= " AND post_status = 1";
				}//end if
				$query_posts .= ")";
			}//end if
			
			if(isset($type)){
				if(is_array($type) && sizeof($type) > 0){
					$query_posts .= " AND post_type IN ('".implode("','",$type)."')";
				} else if(is_string($type)){
					$query_posts .= " AND post_type = '".$type."'";
				}//end if
			}//end if
			
			if($language != NULL){
				$query_posts .= " AND post_lang = ".$language;
			}//end if
			
			if(isset($search)){
				$search_key = str_replace(' ','%',trim($search));
				$query_posts .= " AND (post_title LIKE '%".$search_key."%' OR post_subtitle LIKE '%".$search_key."%' OR post_message LIKE '%".$search_key."%')";
			}//end if
			
			if($this->search_key != NULL){			
				$sql = $this->process_search();				
				if($sql != false){
					$query_posts .= $sql;					
				}//endif
			}//end if				
	
			$query_categories .= "SELECT ".$query_categories_fields."
							   FROM categories
							   LEFT JOIN modules_items_order
							   ON (module_item_order_module_id = category_module_id
							   AND module_item_order_type = 'category'
							   AND module_item_order_item_id = category_id)";
			if($group_id){
				if(!is_array($group_id)){
					$query_categories .= " LEFT JOIN  users_groups_rel
									  ON (category_uid = users_groups_rel_uid
									  AND users_groups_rel_gid = ".$group_id.")";
				} else {
					$query_categories .= " LEFT JOIN  users_groups_rel
									  ON (category_uid = users_groups_rel_uid
									  AND users_groups_rel_gid IN (".implode(",",$group_id)."))";
				}//end if
			}//end if
			if(isset($meta_filter)){
				$query_categories .= "LEFT JOIN meta
						   ON (meta_type = 'category' 
						   AND meta_ref_id = category_id 
						   AND meta_module_id = module_item_order_module_id 
						   AND meta_label = '".$meta_filter['label']."'
						   AND meta_value = '".$meta_filter['value']."') ";
			}//end if	
			$query_categories .= " WHERE category_module_id = '".$module_id."'";
			if($category_id == NULL){
				$query_categories .= " AND category_father_id = 0
									   AND (module_item_order_father_id IS NULL OR module_item_order_father_id = 0)";
			} else {
				$query_categories .= " AND category_father_id = ".$category_id."
								       AND (module_item_order_father_id = ".$category_id." OR module_item_order_father_id IS NULL)";
			}//end if		
			if($status != NULL){
				if($status < 2){
					$query_categories .= " AND category_status = ".$status;
				} else {
					$query_categories .= " AND (category_status = 1 OR category_status = 2)";
				}//end if
			}//end if   
			
			if(isset($meta_filter)){
				$query_categories .= " AND meta_id IS NOT NULL";	
			}//end if
			
			if($language != NULL){
				$query_categories .= " AND category_lang = ".$language;
			}//end if
			
			if(isset($user_id)){
				if(!isset($group_id)){
					$query_categories .= " AND category_uid = ".(int)$user_id;	
				} else {
					if(!is_array($group_id)){
						$query_categories .= " AND (category_uid = ".(int)$user_id." OR (users_groups_rel_gid = ".$group_id."";	
					} else {
						$query_categories .= " AND (category_uid = ".(int)$user_id." OR (users_groups_rel_gid IN (".implode(",",$group_id).")";		
					}
					if(isset($group_status)){
						$query_categories .= " AND category_status = 1";
					}//end if
					$query_categories .= "))";
				}//end if
			} else if(!isset($user_id) && isset($group_id)){
				if(!is_array($group_id)){
					$query_categories .= " AND (users_groups_rel_gid = ".$group_id;	
				} else {
					$query_categories .= " AND (users_groups_rel_gid IN (".implode(",",$group_id).")";		
				}//end if		
				if(isset($group_status)){
					$query_categories .= " AND category_status = 1";
				}//end if
				$query_categories .= ")";	
			}//end if
			
			if(isset($search)){
				$search_key = str_replace(' ','%',trim($search));
				$query_categories .= " AND (category_name LIKE '%".$search_key."%' OR category_message LIKE '%".$search_key."%')";
			}//end if
				
			//Fuse the two queries
			if(!isset($type)){
				$query = "(".$query_posts.") UNION (".$query_categories.")"; 
			} else {
				switch($type){
					case "posts":
						$query = $query_posts;
						break;
					case "categories":
						$query = $query_categories; 	
						break;
					default:
						$query = "(".$query_posts.") UNION (".$query_categories.")"; 	
						break;
				}//end switch
			}//end if
			$query .= " ORDER BY ".$sort_by." ".$sort_order;
						
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false,true,true);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if		
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if	
			
			//echo $query;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);						
					/* META SUPPORT */	
					if(isset($meta)){
						for($i = 0; $i < sizeof($data); $i++){
							if(!isset($html)){
								$html = false;
							}//end if
							//if we pass the value meta as a boolean, we get all the meta into the final data array
							if($data[$i]['is_category'] == "0"){
								$this->meta->set_meta_type('post');
							} else {
								$this->meta->set_meta_type('category');
							}
							if(isset($meta) && !is_array($meta) && (bool)$meta == true){
								//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
								if($data != false){
									for($i = 0; $i < sizeof($data); $i++){
										$meta_data = $this->meta->get_meta(array("id" => $data[$i]['id'], "html" => (bool)$html));
										if($meta_data != false){
											$data[$i] = array_merge($data[$i],$meta_data);
										}//end if
									}//end for i
								}//end if
							} else if(isset($meta) && is_array($meta)){
								//if it's an array, we just get the meta we asked for
								if($data != false){							
									for($i = 0; $i < sizeof($data); $i++){
										$meta_data = $this->meta->get_meta(array("id" => $data[$i]['id'],"search" => $meta, "html" => (bool)$html));								
										if($meta_data != false){
											$data[$i] = array_merge($data[$i],$meta_data);
										}//end if
									}//end for i
								}//end if
							}//end if	
						}//end for i
					}//end if
					/* END META SUPPORT */	
					/* ATTACHMENT SUPPORT */
					if(isset($default_photo) && $default_photo === true){
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$photo_options = array('post_id' => $data[$i]['id'],
													   'role' => 'default-image', 
													   'module_id' => $this->module_id, 
													   'limit' => 1, 
													   'status' => 'published',
													   'type' => 'image');
								if($data[$i]['is_category'] == "1"){
									$photo_options['attachment_post_type'] = 'category';									
								}//end if
								$photo = $this->utils->get_attachments($photo_options);															   
								if($photo !== false){
									$data[$i]['photo'] = $photo;
								}//end if
								//}//end if
							}//end if
						}//end if
					}//end if 			
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;	
			}//end if
		}//end function
		
		
		public function get_recent_posts(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($module_id)){
				if(!is_numeric($module_id)){
					$module_data = $this->utils->get_module(array('module' => $module_id));
					if($module_data !== false){
						$module_id = $module_data['module_id'];
					} else {
						unset($module_id);	
					}//end if
				}//end if
			}//end if					
			
			$query = "SELECT posts.*, user_username, user_firstname, user_surname, UNIX_TIMESTAMP(post_date) AS post_date, UNIX_TIMESTAMP(post_last_update) AS post_last_update, module_name
					  FROM (posts, modules)
					  LEFT JOIN users
					  ON user_id = post_uid
					  WHERE post_module_id = module_name
					  AND post_temp = 0";
			if(isset($status)){
				$query .= " AND post_status = ".(int)$status;	
			}//end if
			
			if(isset($module_id)){
				$query .= " AND module_id = ".$module_id;
			}//end if
			
			$query .= " ORDER BY post_last_update DESC";
			
			if(isset($limit)){
				$query .= " LIMIT ".$limit;	
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function search(){		
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $this->module_id;
				$module_name = $this->module;	
			}//end if
			
			//backward compatibility
			if(isset($userid)){
				$uid = $userid;	
			}//end if
			
			if(isset($cid)){
				$category_id = $cid;	
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "post_date";
			}//end if
			
			//Alias of limit_result
			if(isset($limit)){
				$limit_result = $limit;	
			}//end if
			
			if($sort_order == NULL){					
				$this->settings = $this->utils->get_settings(array('module_id' => $this->module_id));			
				$sort_order = $this->settings['list_order'];				
			}//end if	
			
			if(!isset($fields)){
				$query_fields = $this->table.".*, users.user_username, users.user_id, UNIX_TIMESTAMP(".$this->prefix."_date) AS ".$this->prefix."_date, module_name AS folder";
				if(isset($group_id)){
					$query_fields .= ", users_groups_rel_gid AS group_id";					
				}//end if	
			} else {
				if(is_array($fields)){
					$fields = implode(", ",$fields);
				}//end if
				$query_fields = $fields;
				if(isset($group_id)){
					$query_fields .= ", users_groups_rel_gid AS group_id";					
				}//end if		
			}//end if			
		
			$query = "SELECT ".$query_fields;			
			if(isset($category_id)){
				$query .= ", categories.*";
			}//end if
			if(isset($from_date) || isset($to_date)){
				$query .= ", DATE(".$this->prefix."_date) as short_date";
			}//end if
			
			if(isset($search)){
				if(!isset($search_fields)){
					$search_fields = array($this->prefix.'_title',$this->prefix.'_message');
				}//end if
				$query .= ", MATCH(".implode(",",$search_fields).") AGAINST ('".$search."' IN BOOLEAN MODE) AS search_total";	
				$sort_by = "search_total";
				$sort_order = "DESC";
			}//end if
			
			$query .= " FROM (".$this->table.", modules, modules_items_order";
			if(isset($tag)){
				$query .= ", tags";	
			}//end if
			$query .= ")
					  LEFT JOIN users
					  ON (user_id = ".$this->prefix."_uid) ";
			if($category != NULL){			
				$query .= "LEFT JOIN categories
					  ON (category_id = module_item_order_father_id) ";
			}//end if
			
			if(isset($meta_filter)){
				$query .= "LEFT JOIN meta
						   ON (meta_type = '".$this->prefix."' 
						   		AND meta_ref_id = ".$this->prefix."_id 
								AND meta_module_id = module_id 
								AND meta_label = '".$meta_filter['label']."'";
				if(!is_array($meta_filter['value'])){
					$query .= " AND meta_value = '".$meta_filter['value']."') ";
				} else {
					$query .= " AND meta_value IN ('".implode("','",$meta_filter['value'])."')) ";
				}//end if
			}//end if
			
			if($group_id){
				if(!is_array($group_id)){
					$query .= " LEFT JOIN  users_groups_rel
								  ON (".$this->prefix."_uid = users_groups_rel_uid
								  AND users_groups_rel_gid = ".$group_id.")";
				} else {
					$query .= " LEFT JOIN  users_groups_rel
								  ON (".$this->prefix."_uid = users_groups_rel_uid
								  AND users_groups_rel_gid IN (".implode(",",$group_id)."))";
				}//end if
			}//end if
			
			$query .= "WHERE post_module_id = module_name
					   AND ".$this->prefix."_temp = 0";
			if(isset($public)){
				if((bool)$public === true){
					$query .= " AND module_searchable = 1";	
				}
				$status = 1;//we force to show only published posts
			}
			if(!isset($all_modules)){				
				$query .= " AND ".$this->prefix."_module_id = '".$module_name."'";	
			}//end if  
			
			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						$query .= " AND ".$this->prefix."_".$key." = ".$value;
					}//end foreach
				}//end if
			}//end if
			
			if(isset($search)){
				if(!isset($search_fields)){
					$search_fields = array($this->prefix.'_title',$this->prefix.'_message');
				}//end if
				$query .= " AND MATCH(".implode(",",$search_fields).") AGAINST ('".$search."' IN BOOLEAN MODE)";	
			}//end if
			
			if(isset($meta_filter)){
				$query .= " AND meta_id IS NOT NULL";	
			}//end if
					
			if(isset($type)){
				if(is_array($type) && sizeof($type) > 0){
					$query .= " AND ".$this->prefix."_type IN ('".implode("','",$type)."')";
				} else if(is_string($type)){
					$query .= " AND ".$this->prefix."_type = '".$type."'";
				}//end if
			}//end if		
			
			if(isset($tag)){
				$query .= " AND tag_module_id = module_id AND tag_post_id = ".$this->prefix."_id";
				if(is_array($tag)){
					$query .= " AND tag_tag IN ('".implode("','",$tag)."')";
				} else {
					$query .= " AND tag_tag LIKE '".$tag."'";
				}//end if
			}//end if
					
			$query .= " AND module_item_order_type = '".$this->prefix."'
						AND module_item_order_item_id = ".$this->prefix."_id
						AND module_item_order_module_id = module_id";
		
			if(isset($user_id)){
				if(!isset($group_id)){
					$query .= " AND ".$this->prefix."_uid = ".(int)$user_id;	
				} else {
					if(!is_array($group_id)){
						$query .= " AND (".$this->prefix."_uid = ".(int)$user_id." OR (users_groups_rel_gid = ".$group_id;	
					} else {
						$query .= " AND (".$this->prefix."_uid = ".(int)$user_id." OR (users_groups_rel_gid IN (".implode(",",$group_id).")";	
					}//end if
					if(isset($group_status)){
						$query .= " AND ".$this->prefix."_status = 1";
					}//end if
					$query .= "))";
				}//end if
			} else if(!isset($user_id) && isset($group_id)){
				if(!is_array($group_id)){
					$query .= " AND (users_groups_rel_gid = ".$group_id;	
				} else {
					$query .= " AND (users_groups_rel_gid IN (".implode(",",$group_id).")";		
				}//end if		
				if(isset($group_status)){
					$query .= " AND ".$this->prefix."_status = 1";
				}//end if
				$query .= ")";
			}//end if
			
			if($month != NULL && is_numeric($month)){
				$query .= " AND MONTH(".$this->prefix."_date) = ".$month;
			}//end if
			
			if($year != NULL && is_numeric($year)){
				$query .= " AND YEAR(".$this->prefix."_date) = ".$year;
			}//end if
			
			if($day != NULL && is_numeric($day)){
				$query .= " AND DAY(".$this->prefix."_date) = ".$day;
			}//end if
			
			if(isset($from_date)){
				$query .= " AND DATE(".$this->prefix."_date) >= DATE('".$from_date."')";
			}//end if
			
			if(isset($to_date)){
				$query .= " AND DATE(".$this->prefix."_date) <= DATE('".$to_date."')";	
			}//end if
			
			if(isset($range)){
				if(is_array($range)){
					$query .= " AND ".$this->prefix."_id IN (".implode(',',$range).")";	
				} else {
					$query .= " AND ".$this->prefix."_id IN (".$range.")";	
				}//end if
			}//end if
			
			//check the status of the posts
			if(isset($status) && (is_bool($status) || is_numeric($status))){
				$query .= " AND ".$this->prefix."_status = ".(int)$status;
			}//end if	
					
			//check if the posts is deleted or not	
			if(isset($deleted) && (is_numeric($deleted) || is_bool($deleted))){
				$query .= " WHERE ".$this->prefix."_deleted = ".(int)$deleted;
			}//end if
			
			if(isset($category_id)){
				$query .= " AND (category_id = ".$category_id;
				if(isset($show_subelements) && $show_subelements == true){
					$query .= " OR category_father_id = ".$category_id;
				}//end if
				$query .= ")";
			} else {
				if(isset($show_subelements) && $show_subelements == false){
					$query .= " AND module_item_order_father_id = 0";
				}//end if
			}//end if
			
			if($use_system_order == NULL || $use_system_order == false){
				$query .= " GROUP BY ".$this->prefix."_id						
						    ORDER BY ".$sort_by." ".$sort_order;				
			} else {
				$query .= " GROUP BY ".$this->prefix."_id						
					    	ORDER BY module_item_order_order ".$sort_order;	
			}//end if
			
			if(isset($search)){
				$query .= ", post_id DESC";	
			}
								
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if	
			
			//echo $query;
						
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					$this->meta->set_meta_type('post');
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					
					/* ATTACHMENT SUPPORT */						
					if(isset($default_photo) && $default_photo === true){						
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$photo = $this->utils->get_attachments(array('post_id' => $data[$i]['post_id'],
																	   'role' => 'default-image', 
																	   'module' => $data[$i]['post_module_id'], 
																	   'limit' => 1, 
																	   'status' => 'published',
																	   'type' => 'image'));													   
								if($photo !== false){
									$data[$i]['photo'] = $photo;
								}//end if
							}//end if
						}//end if
					}//end if 
					
					
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function	
		
	
		public function get_posts(){			
			$params = func_get_args();
			if(!is_array($params[0])){
				$cid = $params[0];				
				$sort_by = $params[1];
				
				$sort_order = $params[2];
				
				$status = $params[3];
				$deleted = $params[4];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $this->module_id;
				$module_name = $this->module;	
			}//end if
			
			//backward compatibility
			if(isset($userid)){
				$uid = $userid;	
			}//end if
			
			if(isset($category_id)){
				$cid = $category_id;	
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "post_date";
			}//end if
			
			//Alias of limit_result
			if(isset($limit)){
				$limit_result = $limit;	
			}//end if
			
			if($sort_order == NULL){					
				$this->settings = $this->utils->get_settings(array('module_id' => $this->module_id));			
				$sort_order = $this->settings['list_order'];				
			}//end if	
			
			if(!isset($fields)){
				$query_fields = "posts.*, users.user_username, users.user_id, UNIX_TIMESTAMP(post_date) AS post_date, module_name AS folder";
				if(isset($group_id)){
					$query_fields .= ", users_groups_rel_gid AS group_id";					
				}//end if	
			} else {
				if(is_array($fields)){
					$fields = implode(", ",$fields);
				}//end if
				$query_fields = $fields;
				if(isset($group_id)){
					$query_fields .= ", users_groups_rel_gid AS group_id";					
				}//end if		
			}//end if			
		
			$query = "SELECT ".$query_fields;			
			if(isset($category_id)){
				$query .= ", categories.*";
			}//end if
			if(isset($from_date) || isset($to_date)){
				$query .= ", DATE(post_date) as short_date";
			}//end if
			$query .= " FROM (posts, modules, modules_items_order";
			if(isset($tag)){
				$query .= ", tags";	
			}//end if
			$query .= ")
					  LEFT JOIN users
					  ON (user_id = post_uid) ";
			if($cid != NULL){			
				$query .= "LEFT JOIN categories
					  ON (category_id = module_item_order_father_id) ";
			}//end if
			
			if(isset($meta_filter)){
				$query .= "LEFT JOIN meta
						   ON (meta_type = 'post' 
						   		AND meta_ref_id = post_id 
								AND meta_module_id = module_id 
								AND meta_label = '".$meta_filter['label']."'";
				if(!is_array($meta_filter['value'])){
					$query .= " AND meta_value = '".$meta_filter['value']."') ";
				} else {
					$query .= " AND meta_value IN ('".implode("','",$meta_filter['value'])."')) ";
				}//end if
			}//end if
			
			if($group_id){
				if(!is_array($group_id)){
					$query .= " LEFT JOIN  users_groups_rel
								  ON (post_uid = users_groups_rel_uid
								  AND users_groups_rel_gid = ".$group_id.")";
				} else {
					$query .= " LEFT JOIN  users_groups_rel
								  ON (post_uid = users_groups_rel_uid
								  AND users_groups_rel_gid IN (".implode(",",$group_id)."))";
				}//end if
			}//end if
			
			$query .= "WHERE post_module_id = module_name
					   AND post_temp = 0";
			if(!isset($all_modules)){
				if(isset($category_id)){
					$query .= " AND category_module_id = ".$module_id."";
				} else {
					$query .= " AND post_module_id = '".$module_name."'";		
				}//end if	
			}//end if  
			
			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND ".$this->prefix."_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND ".$this->prefix."_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if
			
			if(isset($date_check)){
				if($date_check === true){
					$query .= " AND post_date <= CURDATE() + INTERVAL 1 DAY";	
				}//end if
			}//end if
			
			if(isset($meta_filter)){
				$query .= " AND meta_id IS NOT NULL";	
			}//end if
					
			if(isset($type)){
				if(is_array($type) && sizeof($type) > 0){
					$query .= " AND post_type IN ('".implode("','",$type)."')";
				} else if(is_string($type)){
					$query .= " AND post_type = '".$type."'";
				}//end if
			}//end if		
			
			if(isset($tag)){
				$query .= " AND tag_module_id = module_id AND tag_post_id = post_id";
				if(is_array($tag)){
					$query .= " AND tag_tag IN ('".implode("','",$tag)."')";
				} else {
					$query .= " AND tag_tag LIKE '".$tag."'";
				}//end if
			}//end if
					
			$query .= " AND module_item_order_type = 'post'
						AND module_item_order_item_id = post_id
						AND module_item_order_module_id = module_id";
		
			if(isset($user_id)){
				if(!isset($group_id)){
					$query .= " AND post_uid = ".(int)$user_id;	
				} else {
					if(!is_array($group_id)){
						$query .= " AND (post_uid = ".(int)$user_id." OR (users_groups_rel_gid = ".$group_id;	
					} else {
						$query .= " AND (post_uid = ".(int)$user_id." OR (users_groups_rel_gid IN (".implode(",",$group_id).")";	
					}//end if
					if(isset($group_status)){
						$query .= " AND post_status = 1";
					}//end if
					$query .= "))";
				}//end if
			} else if(!isset($user_id) && isset($group_id)){
				if(!is_array($group_id)){
					$query .= " AND (users_groups_rel_gid = ".$group_id;	
				} else {
					$query .= " AND (users_groups_rel_gid IN (".implode(",",$group_id).")";		
				}//end if		
				if(isset($group_status)){
					$query .= " AND post_status = 1";
				}//end if
				$query .= ")";
			}//end if
			
			if($month != NULL && is_numeric($month)){
				$query .= " AND MONTH(post_date) = ".$month;
			}//end if
			
			if($year != NULL && is_numeric($year)){
				$query .= " AND YEAR(post_date) = ".$year;
			}//end if
			
			if($day != NULL && is_numeric($day)){
				$query .= " AND DAY(post_date) = ".$day;
			}//end if
			
			if(isset($from_date)){
				$query .= " AND DATE(post_date) >= DATE('".$from_date."')";
			}//end if
			
			if(isset($to_date)){
				$query .= " AND DATE(post_date) <= DATE('".$to_date."')";	
			}//end if
			
			if(isset($range)){
				if(is_array($range)){
					$query .= " AND post_id IN (".implode(',',$range).")";	
				} else {
					$query .= " AND post_id IN (".$range.")";	
				}//end if
			}//end if
			/*
			if($this->search_key != NULL){
				$sql = $this->process_search();
				if($sql != false){
					$query .= $sql;					
				}//endif
			}//end if		
			*/
			//check the status of the posts
			if(isset($status) && (is_bool($status) || is_numeric($status))){
				$query .= " AND post_status = ".(int)$status;
			}//end if	
			if(isset($temp) && (is_numeric($temp) || is_bool($temp))){
				$query .= " AND post_temp = ".(int)$temp;
			}//end if	
		
			//check if the posts is deleted or not	
			if(isset($deleted) && (is_numeric($deleted) || is_bool($deleted))){
				$query .= " AND post_deleted = ".(int)$deleted;
			}//end if
			
			if(isset($category_id)){
				$query .= " AND (category_id = ".$category_id;
				if(isset($show_subelements) && $show_subelements == true){
					$query .= " OR category_father_id = ".$category_id;
				}//end if
				$query .= ")";
			} else {
				if(isset($show_subelements) && $show_subelements == false){
					$query .= " AND module_item_order_father_id = 0";
				}//end if
			}//end if
			
			if($use_system_order == NULL || $use_system_order == false){
				$query .= " GROUP BY posts.post_id						
						    ORDER BY ".$sort_by." ".$sort_order;			
			} else {
				$query .= " GROUP BY posts.post_id						
					    	ORDER BY module_item_order_order ".$sort_order;	
			}//end if
								
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if	
						
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					$this->meta->set_meta_type('post');
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					/* ATTACHMENT SUPPORT */						
					if(isset($default_photo) && $default_photo === true){
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$photo = $this->utils->get_attachments(array('post_id' => $data[$i]['post_id'],
																	   'role' => 'default-image', 
																	   'module_id' => $this->module_id, 
																	   'limit' => 1, 
																	   'status' => 'published',
																	   'type' => 'image'));															   
								if($photo !== false){
									$data[$i]['photo'] = $photo;
								}//end if
							}//end if
						}//end if
					}//end if 
					
					/* TAGS SUPPORT */
					if(isset($tags) && $tags == true){
						if($data != false){
							$tools = $this->utils->call("tools");
							for($i = 0; $i < sizeof($data); $i++){
								$vars = array();
								$vars['post_id'] = $data[$i]['post_id'];
								if(is_null($this->module_id)){
									$vars['module_id'] = $data[$i]['post_module_id'];
								} else {
									$vars['module_id'] = $this->module_id;
								}//end if							
								$tags_list = $tools->get_tags($vars);						
								if($tags_list !== false){
									$data[$i]['tags'] = $tags_list;
								}//end if
							}//end for i
						}//end if
					}//end if	
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		//Return the data of the id post passed
		public function get_post(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$pid = $params[0];
				$this->pid = $pid;	
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($id)){
				$pid = $id;	
			}//end if
			
			if(!isset($fields)){
				$fields = 'posts.*, users.user_username, users.user_email, UNIX_TIMESTAMP(post_date) AS post_date, module_name AS folder,
					  UNIX_TIMESTAMP(post_last_update) AS post_last_update';	
			}//end if
			
			$query = "SELECT ".$fields."
					  FROM (posts, modules)
					  LEFT JOIN users
					  ON users.user_id = posts.post_uid
					  WHERE module_name = post_module_id ";	
			if(isset($pid)){
				if(is_numeric($pid)){
					$query .= " AND post_id = ".$pid;
				} else {
					return false;	
				}//end if
			}//end if
			
			if(isset($permalink)){
				$permalink = $this->utils->db->quote($permalink);
				$query .= " AND post_permalink = ".$permalink;
			}//end if
			
			$query .= " LIMIT 1";
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					//Read the post categories
					////This may fix a problem, in case, take it out
					if(isset($data['folder'])){
						$this->module = $data['folder'];	
					}//end if
					if(isset($data['post_id'])){				
						$this->pid = $data['post_id'];				
						$post_cats = $this->get_post_categories($this->pid);	
						if($post_cats !== false){
							$cids = array();
							for($i = 0; $i < sizeof($post_cats); $i++){
								array_push($cids,$post_cats[$i]['category_id']);
							}//end for
							$data['category_id'] = base64_encode(serialize($cids));
						} else {
							$data['category_id'] = NULL;			
						}//end if
					}//end if
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;	
					}//end if
					if(!isset($raw)){
						$raw = false;	
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$meta_data = $this->meta->get_meta(array("id" => $this->pid, "html" => (bool)$html, "raw" => (bool)$raw));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for					
						$meta_data = $this->meta->get_meta(array("id" => $this->pid,"search" => $meta, "html" => (bool)$html, "raw" => (bool)$raw));				
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					}//end if
					/* END META SUPPORT */	
					
					/* ATTACHMENT SUPPORT */
					if(isset($default_photo) && $default_photo === true){
						if($data != false){	
							$vars = array('post_id' => $data['post_id'],
																   'role' => 'default-image', 
																   'module_id' => $this->module_id, 
																   'limit' => 1, 
																   'status' => 'published',
																   'type' => 'image');																  
							$photo = $this->utils->get_attachments($vars);															   
							if($photo !== false){
								$data['photo'] = $photo;
							}//end if
						}//end if
					}//end if 
					
					/* AUTHOR SUPPORT */
					if(isset($author) && $author == true){
						if($data != false){
							$author_id = $data['post_uid'];
							$users = $this->utils->call("users");
							$author_data = $users->get_user(array("id" => $author_id));
							if($author_data !== false){
								$data['author'] = $author_data;
							}//end if
						}//end if
					}//end if
					
					/* TAGS SUPPORT */
					if(isset($tags) && $tags == true){
						if($data != false){
							$vars = array();
							$vars['post_id'] = $data['post_id'];
							if(is_null($this->module_id)){
								$vars['module_id'] = $data['post_module_id'];
							} else {
								$vars['module_id'] = $this->module_id;
							}//end if
							$tools = $this->utils->call("tools");
							$tags_list = $tools->get_tags($vars);
							if($tags_list !== false){
								$data['tags'] = $tags_list;
							}//end if
						}//end if
					}//end if		
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
							
		public function save_post(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			
			
			if(isset($revision)){
				if(is_bool($revision) && $revision == true && !is_null($_REQUEST['id'])){
					$revised = $this->save_revision(array("id" => (int)$_REQUEST['id'],"module_id" => $this->module_id));
				}//end if
			}//end if
			$hidden_fields = array('action','step','submit','category_id','id','date_day','date_month','date_year','tags','category','permatemp');
			//if exists the fields for the date, I create with these a new post field with the correct formatted data for mysql
			if($_POST['date_month'] != NULL && $_POST['date_day'] != NULL && $_POST['date_year'] != NULL){
				$_POST['date'] = date("Y-m-d H:i:s",mktime(1,0,0,$_POST['date_month']+1,$_POST['date_day'],$_POST['date_year']));				
			} else if($_POST['date'] != NULL){
				$_POST['date'] = $this->utils->convert_to_db_date($_POST['date']);
			}//end if
			
			//check if categories contains commas	
			if(!is_array($_POST['category_id'])){		
				if(strpos($_POST['category_id'],",") !== false){
					//Turn the category into an array
					$_POST['category_id'] = explode(",",$_POST['category_id']);
				}//end if		
			}//end if
			
			$seo = $this->utils->call("seo");
			$perma_options = array("type" => "post");
			
			if(!is_null($_POST['id'])){
				$perma_options['id'] = $_POST['id'];	
			}
			if ($_POST['permalink'] != NULL){	
				$perma_options["permalink"] = $_POST['permalink'];			
				$valid_link = $seo->return_valid_permalink($perma_options);
				$_POST['permalink'] = $valid_link;
			} else if($_POST['permalink'] == NULL && $_POST['id'] == NULL){
				$perma_options["permalink"] = $seo->clean_link($_POST['title']);
				$valid_link = $seo->return_valid_permalink($perma_options);
				$_POST['permalink'] = $valid_link;
			}//end if			
						
			if($_POST['date_end'] != NULL){
				$_POST['date_end'] = $this->utils->convert_to_db_date($_POST['date_end']);
			}//end if
			$count = $this->get_num_posts(array('type' => 'posts'));
					
			if($_REQUEST['id'] == NULL){
				//Is a new post
				$_POST['order'] = $count;
				$_POST['module_id'] = $this->module;
				//Add the user id of the post author
				$_POST['uid'] = $this->utils->get_uid();								
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "post","sql_type" => "INSERT", "method_data" => "POST", "hidden_fields" => $hidden_fields, "module" => $this->module));
				$query = "INSERT INTO posts ".$sql;	
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$this->pid = $this->utils->db->lastInsertId();				
					if($_POST['category_id'] == NULL){
						$this->put_into_mainpages($this->pid);
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				//Editing an existant post
				$this->pid = (int)$_REQUEST['id'];		
				$_POST['last_update'] = date("Y-m-d H:i:s",time());				
				//$sql = $this->utils->build_sql_fields("post","UPDATE","POST",$hidden_fields);	
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "post","sql_type" => "UPDATE", "method_data" => "POST", "hidden_fields" => $hidden_fields, "module" => $this->module));				
				$query = "UPDATE posts SET ".$sql." WHERE post_id = ".$this->pid;	
						
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					//$this->update_post_dependencies();
					
					$post_categories = $this->get_post_categories($this->pid);			
					if($post_categories === false){
						$this->put_into_mainpages($this->pid);
					} else {
						$this->remove_from_mainpages($this->pid);
					}//end if				
				} else {
					return false;	
				}//end if
			}//endif	
			//echo $query;
			/* META SUPPORT */			
			$this->meta->set_meta_id($this->pid);	
			$table_fields = $this->utils->get_table_fields(array("module" => $this->module,"field_prefix" => "post", "show_all" => true));			
			array_walk($table_fields,array($this->utils,'clean_prefix'),$table_prefix);		
			$this->meta->auto_add_meta(array("avoid" => $table_fields));
			/* END META SUPPORT */

			//Create the relation between the post and its categories					
			if($_POST['category_id'] != NULL && (is_numeric($_POST['category_id']) || is_array($_POST['category_id']))){			
				if($this->create_categories_relation($this->pid,$_POST['category_id'])){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return true;
			}//end if
			return true;
		}//end function
		
		public function get_intro(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($module)){
				return false;	
			}//end if
			
			if(!isset($force)){
				$force = false;	
			}//end if
			
			$intro = $this->get_posts(array(
				'module' => $module,
				'limit_result' => 1,
				'meta' => true,
				'use_system_order' => true,
				'type' => array('intro','redirect')
			));			
			
			if($intro === false && $force){
				$intro = $this->get_posts(array(
					'module' => $module,
					'limit_result' => 1,
					'use_system_order' => true,
					'meta' => true
				));	
			}//end if
			
			if($intro !== false){
				return $intro;
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function save_revision(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			//we have to know the post id and the module id, otherwise we return false
			if(!isset($id) && !isset($module_id)){
				return false;	
			}//end if
			$user_id = $this->utils->get_uid();
			
			//now we need to get the current data of the post, before we save the new informations!
			$data = $this->get_post(array("id" => $id, "meta" => true, "raw" => true));
			if($data !== false){
				try{
					$data = base64_encode(serialize($data));
					$query = "INSERT INTO posts_revisions
							  (post_revision_post_id, post_revision_user_id, post_revision_module_id, post_revision_data)
							  VALUES
							  (:post_id, :user_id, :module_id, :post_data)";
					$result = $this->utils->db->prepare($query);
					$result->bindParam(':post_id', $id, PDO::PARAM_INT);
					$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
					$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
					$result->bindParam(':post_data', $data , PDO::PARAM_STR);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors === false){
						$this->revisions_autoclean();
						return true;
					} else {
						return false;
					}//end if
				} catch(PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}
			} else {
				//no post found, so return false
				return false;
			}//end if
		}//end function
		
		public function revisions_autoclean(){
			$settings = $this->utils->get_settings();
			if(isset($settings['deleteOldRevisions'])){
				if($settings['deleteOldRevisions'] == 1){
					if(is_numeric($settings['deleteOldRevisionsDays']) && (int)$settings['deleteOldRevisionsDays'] > 0){
						//now we know that the system is set to remove the revisions after a specified amount of time
						$query = "DELETE FROM posts_revisions WHERE post_revision_date < CURDATE()";
						if((int)$settings['deleteOldRevisionsDays'] > 1){
							$query .= " - INTERVAL ".(int)$settings['deleteOldRevisionsDays']." DAY";						
						}//end if
						$result = $this->utils->db->query($query);
						$errors = $this->utils->error($result,__LINE__,get_class($this));
						if($errors === false){
							//optimize the table
							$query = "OPTIMIZE TABLE posts_revisions";
							$result = $this->utils->db->query($query);
							$errors = $this->utils->error($result,__LINE__,get_class($this));
							return true;
						} else {
							return false;	
						}//end if
					} else {
						return false;
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		private function get_lowerst_order_id($type,$f_category = NULL){
			//get the lowest number for order
			$query = "SELECT * FROM modules_items_order
					  WHERE module_item_order_module_id = ".$this->module_id."		
					  AND module_item_order_type = '".$type."'";
			if($f_category != NULL){
				$query .= " AND module_item_order_father_id = ".$f_category;
			} else {
				$query .= " AND module_item_order_father_id = 0";
			}//end if
			$query .= " ORDER BY module_item_order_order ASC LIMIT 1";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num != 0){
					$data = $this->utils->get_result_array($result,true);	
					return $data['module_item_order_order'];
				} else {
					return "1";
				}//end if
			} else {
				return "1";
			}//end if
		}//end function
		
		public function put_into_mainpages(){			
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$post_id = $params[0];
				$type = $params[1];
			}//end if
						
			if(!isset($post_id) || is_null($post_id)){
				return false;	
			}//end if
			
			if(!isset($type) || is_null($type)){
				$type = 'post';	
			}//end if
			
			if(isset($module)){
				$module_data = $this->utils->get_module(array('module' => $module));
				if($module_data !== false){
					$this->module_id = $module_data['module_id'];
				}//end if
			}//end if
			
			$f_category = $_REQUEST['category'];
			if($f_category == NULL){
				$f_category = $_REQUEST['father_id'];
			}//end if
			
			//get lowest number
			$order_num = $this->get_lowerst_order_id($type,$f_category);
			$order_num--;
			
			//check if already exists
			$query = "SELECT * FROM modules_items_order
					  WHERE module_item_order_module_id = ".$this->module_id."
					  AND module_item_order_item_id = ".$post_id."
					  AND module_item_order_type = '".$type."'";
			if($f_category != NULL){
				$query .= " AND module_item_order_father_id = ".$f_category;
			} else {
				$f_category = "0";
			}//end if
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num == 0){
					$query = "INSERT INTO modules_items_order
							  (module_item_order_module_id,module_item_order_item_id, module_item_order_type,module_item_order_father_id, module_item_order_order)
							  VALUES
							  (".$this->module_id.",".$post_id.",'".$type."',".$f_category.", ".$order_num.")";						
					$result = $this->utils->db->query($query);		
					$this->utils->error($result,__LINE__,get_class($this));
				} else {
					if($type == "category"){
						$row = $result->fetch(PDO::FETCH_ASSOC);
						$query = "UPDATE modules_items_order SET 
								  module_item_order_father_id = ".$f_category."
								  WHERE module_item_order_module_id = ".$this->module_id."
								  AND module_item_order_item_id = ".$post_id."
								  AND module_item_order_type = '".$type."'";
						$result = $this->utils->db->query($query);		
						$this->utils->error($result,__LINE__,get_class($this));
					}//end if
				}//end if
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function remove_from_mainpages($pid,$type = 'post'){
			$query = "DELETE FROM modules_items_order
					  WHERE module_item_order_module_id = ".$this->module_id."
					  AND module_item_order_item_id = ".$pid."
					  AND module_item_order_type = '".$type."'
					  AND module_item_order_father_id = 0";
			$result = $this->utils->db->query($query);
			$this->utils->error($result,__LINE__,get_class($this));
			return true;
		}//end function
		
		public function get_post_id(){
			return $this->pid;
		}//end function
		
		public function force_module($module){
			$this->module = $module;
			$data = $this->utils->get_module($module);
			$this->module_id = $data['module_id'];
		}//end function
		
		public function get_post_relationships(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if
			
			if(!isset($current_module) || is_null($current_module)){
				$current_module = false;	
			}//end if
			
			if($current_module){
				if($this->module != NULL){
					$module_data = $this->utils->get_module($this->module);			
					$module_id = $module_data['module_id'];
					$module_table = $module_data['module_table'];
				}//end if	
			}//end if
			
			$query = "SELECT * FROM modules_items_order
					  WHERE module_item_order_type = 'post'
					  AND module_item_order_item_id = ".$id;
			if($current_module){
				$query .= " AND module_item_order_module_id = ".$module_id;
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function check_post_relationship(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id) || !isset($module_id) || !isset($parent_id)){
				return false;	
			}//end if
			
			if(!isset($data)){
				$data = $this->get_post_relationships(array('id' => $id));
			}//end if
			
			if($data !== false){
				for($i = 0; $i < sizeof($data); $i++){
					if($data[$i]['module_item_order_module_id'] == $module_id && $data[$i]['module_item_order_father_id'] == $parent_id){
						return true;
					} else {
						return false;	
					}//end if
				}//end for i
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_post_path(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id) || !isset($module_id)){
				return false;
			}//end if			
			
			if(!is_numeric($module_id)){
				$module = $this->utils->get_module(array('module' => $module_id));
				
				if($module !== false){
					$module_id = $module['module_id'];	
				} else {
					return false;	
				}//end if
			}//end if
							
			$last_id = $id;
			$i = 0;
			$data = array();
			$result = true;			
			while($result !== false){			
				$type = 'post';
				if($i > 0){
					$type = 'category';	
				}//end if
				$options = array('id' => $last_id,'type' => $type,'module_id' => $module_id);				
				$result = $this->get_parent($options);				
				if($result !== false){
					$last_id = $result['module_item_order_father_id'];	
					$data[$i] = array();				
					$data[$i]['label'] = $result['category_name'];
					$data[$i]['id'] = $last_id;
				}				
				$i++;
			}
			return $data;
		}//end function
		
		protected function get_parent(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id) || !isset($module_id)){
				return false;	
			}//end if
			
			if(!is_numeric($module_id)){
				$module = $this->utils->get_module(array('module' => $module_id));
				if($module !== false){
					$module_id = $module['module_id'];	
				} else {
					return false;	
				}//end if
			}//end if
			
			if(!isset($type)){
				$type = 'post';	
			}//end if
			
			$query = "SELECT module_item_order_father_id, category_name 
					  FROM modules_items_order, categories 
					  WHERE module_item_order_item_id = ".$id." 
					  AND category_id = module_item_order_father_id
					  AND module_item_order_module_id = ".$module_id."
					  AND module_item_order_type = '".$type."'
					  LIMIT 1";					 
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					if($data['module_item_order_father_id'] > 0){
						return $data;
					} else {
						return false;	
					}
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		//Return an array of the categories ids linked to this post
		public function get_post_categories(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$post_id = $params[0];				
				$current_module = $params[1];			
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($id)){
				$post_id = $id;	
			}
			
			if(!isset($current_module) || is_null($current_module)){
				$current_module = true;	
			}//end if
			
			if($this->module != NULL){
				$module_data = $this->utils->get_module($this->module);			
				$module_id = $module_data['module_id'];
				$module_table = $module_data['module_table'];
			}//end if
			$query = "SELECT categories.*, modules.*
					  FROM modules_items_order, categories, modules, posts
					  WHERE module_item_order_item_id = ".$post_id."
					  AND module_item_order_item_id = post_id	
					  AND module_item_order_type = 'post'			
					  AND category_module_id = module_id					 
					  AND module_item_order_father_id = category_id";
			if($module_table != NULL){
				$query .= " AND module_table = '".$module_table."'";
			}//endif	
			if($current_module == true && !is_null($module_id)){
				$query .= " AND category_module_id = ".$module_id;
			}//endif	
				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type("category");
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['category_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$this->meta->set_meta_type("category");
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['category_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */		
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_post_categories_ids($pid){
			$module_id = $this->utils->get_module($this->module);
			$module_id = $module_id['module_id'];
			$query = "SELECT categories.category_id
					  FROM modules_items_order, categories
					  WHERE module_item_order_item_id = ".$pid."
					  AND module_item_order_type = 'post'
					  AND module_item_order_father_id = category_id";					  
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = array();
					while($row = $result->fetch(PDO::FETCH_ASSOC)){
						array_push($data,$row['category_id']);
					}//end if	
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		//return the number of the post for the passed category
		public function get_num_posts(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$parent_id = $params[0];				
				$type = $params[1];	
				$status = $params[2];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
						
			if(is_null($type) || !isset($type)){
				$type = 'category';						
			}//end if
			
			if($type == "category" || $type == "count-children"){
				if(!isset($parent_id) || is_null($parent_id)){
					return false;	
				}//end if
			}//end if
			
			switch($type){
				case "category":
					$query = "SELECT COUNT(*) AS counted 
							  FROM modules_items_order, posts
							  WHERE module_item_order_father_id = ".$parent_id."
							  AND post_id = module_item_order_item_id
							  AND module_item_order_type = 'post'
							  AND post_module_id = '".$this->module."'";
					if(isset($status)){
						if($status != NULL && is_numeric($status)){
							$query .= " AND post_status = ".$status;
						}//end if	 
					}//end if	
					
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors === false){
						$row = $result->fetch(PDO::FETCH_ASSOC);
						return $row['counted'];
					} else {
						return false;
					}//end if			
					break;	
				case "posts":
					if(isset($module)){
						$module_data = $this->utils->get_module($module);
						$module = $module_data['module_name'];
					} else {
						$module = $this->module;	
					}//end if
				
					$query = "SELECT COUNT(*) as counted
							  FROM (posts)					 			  
							  WHERE post_module_id = '".$module."'";
					if(isset($user_id) && is_numeric($user_id)){
						$query .= " AND post_uid = ".$user_id;
					}//end if	
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors === false){
						$row = $result->fetch(PDO::FETCH_ASSOC);
						return $row['counted'];
					} else {
						return false;
					}//end if		
					break;	
				case "count-children":	
					if(isset($module)){
						$module_data = $this->utils->get_module($module);
						$module_id = $module_data['module_id'];
					} else {
						$module_id = $this->module_id;	
					}//end if
					
					$meta_filter_enabled = false;
					if(isset($meta_filter)){
						if(is_array($meta_filter) && isset($meta_filter['label']) && isset($meta_filter['value'])){
							$meta_filter_enabled = true;							
						}//end if
					}//end if
								
					$query = "SELECT modules_items_order.*, post_temp";
					if($meta_filter_enabled){
						$query .= ",meta_label, meta_value";	
					}
					$query .= " FROM (modules_items_order)
							  LEFT JOIN posts
							  ON (
							  	post_id = module_item_order_item_id 
								AND module_item_order_type = 'post' 
								AND post_temp = 0
								AND module_item_order_module_id = ".$module_id."
							  )";
							  
					if($meta_filter_enabled){
						$query .= " LEFT JOIN meta
									ON (module_item_order_type = 'post'
										AND meta_ref_id = module_item_order_item_id
										AND meta_module_id = ".$module_id."
										AND meta_type = 'post'
										AND meta_label = '".$meta_filter['label']."'
									)";
						
					}//end if
					
					$query .= " WHERE module_item_order_father_id = ".$parent_id;	
					if($parent_id == 78){						
					//	echo $query;			
					}
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors === false){
						$count = 0;
						while($row = $result->fetch(PDO::FETCH_ASSOC)){
							if($row['module_item_order_type'] == 'post'){
								if($meta_filter_enabled === false){
									if($row['post_temp'] == '0'){
										$count++;	
									}
								} else {
									if($row['post_temp'] == '0' && $row['meta_label'] == $meta_filter['label'] && $row['meta_value'] == $meta_filter['value']){
										$count ++;	
									}
								}
							} else if($row['module_item_order_type'] == 'category'){
								$options = array('type' => 'count-children', 'parent_id' => $row['module_item_order_item_id']);
								if(isset($meta_filter)){
									$options['meta_filter'] = $meta_filter;	
								}//end if
								$count += $this->get_num_posts($options);
							}//end if
						}//end while
						return $count;
					}//end if
					break;		
			}//end switch	
			
		}//end function
		
		public function create_categories_relation($pid,$categories){
			$module_id = $this->utils->get_module($this->module);
			$module_id = $module_id['module_id'];			
			
			//Check if the post id is different from null, is numeric, and the categories are an array
			if($pid != NULL){
				if(is_array($categories) && sizeof($categories) > 0){	
					//Read the categories
					for($i = 0; $i < sizeof($categories); $i++){						
						$this->update_module_relationship($pid,$module_id,"post",$categories[$i]);
					}//end for
				} else {				
					$this->update_module_relationship($pid,$module_id,"post",$categories);
				}//end if		
				
				return true;
			} else {
				return false;
			}//end if
		}//end function	
		
		
		public function clean_module_relationships(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($type) || !isset($id)){
				return false;	
			}//end if
			
			if(!isset($module)){
				$module = $this->module;	
			}//end if
			
			$module_data = $this->utils->get_module($module);
			$module_id = $module_data['module_id'];
			
			$query = "DELETE FROM modules_items_order
					  WHERE module_item_order_module_id = ".$module_id."
					  AND module_item_order_item_id = ".$id."
					  AND module_item_order_type = '".$type."'";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;	
			} else {
				return false;
			}
		}//end function
		
		public function update_module_relationship(){	
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$id = $params[0];
				$module_id = $params[1];
				$type = $params[2];
				$category = $params[3];
			}//end if
			
			if(!isset($id) || !isset($module_id)){
				return false;	
			}//end if
			
			if(isset($parent_id)){
				$category = $parent_id;	
			}//end if			
			
			if(!isset($type) || is_null($type)){
				$type = 'post';	
			}//end if
			
			if(!is_numeric($module_id)){
				$module_data = $this->utils->get_module($module_id);
				$module_id = $module_data['module_id'];
			}//end if
			
			//First, check if the relationship with the module already exists
			$query = "SELECT * FROM modules_items_order
					  WHERE module_item_order_module_id = ".$module_id."
					  AND module_item_order_item_id = ".$id."
					  AND module_item_order_type = '".$type."'";
			if($category != NULL){
				$order = $this->get_num_posts($category);
				$order++;
				$query .= " AND module_item_order_father_id = ".$category;
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				//If doesn't exists, it will be created
				if($num == 0){
					$query = "INSERT INTO modules_items_order
							  (module_item_order_item_id,module_item_order_module_id, module_item_order_type, module_item_order_order";
					if($category != NULL){
						$query .= ",module_item_order_father_id";
					}//end if
					$query .= ") VALUES (".$id.",".$module_id.",'".$type."',".$order;
					if($category != NULL){
						$query .= ",".$category;
					}//end if
					$query .= ")";
					$this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
				
		//return true if the  user is the owner of the post passed
		public function is_post_owner($uid,$pid){
			$query = "SELECT * FROM posts WHERE post_id = ".$pid." AND posts.post_uid = ".$uid;				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num == 1){
					return true;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function	
		
		public function delete_post($post_id){	
			$tools = $this->utils->call("tools");
			if(!is_null($post_id)){	
				//check if postid is an array or not
				if(!is_array($post_id)){
					$data = $this->get_post($post_id);
					if($data !== false){
						$this->utils->db->beginTransaction();
						try{
							//$tags = $data['post_tags'];
							//$this->utils->delete_tags($tags,$this->module);
							$query = "DELETE FROM ".$this->table." WHERE ".$this->prefix."_id = ".$post_id;							
							$result = $this->utils->db->query($query);
							$this->utils->error($result,__LINE__,get_class($this));
							//Delete the relations with categories					
							$this->delete_post_attachments($post_id);	
							$this->delete_item_relation(array('id' => $post_id));
							//Delete the related tags
							$tools->delete_post_tags(array("post_id" => $post_id, "module_id" => $this->module_id));
							//Delete the related meta
							$this->meta->delete_meta(array('post_id' => $post_id, "module" => $this->module,'type' => 'post'));
						} catch(PDOException $ex){
							$this->utils->db->rollback();
							$this->errors = $ex->getMessage();
							return false;	
						}//end try	
						
						$this->utils->db->commit();
					}
				} else {
					for($i = 0; $i < sizeof($post_id); $i++){
						$data = $this->get_post($post_id[$i]);
						if($data !== false){
							$this->utils->db->beginTransaction();
							try{
								//$tags = $data['post_tags'];
								//$this->utils->delete_tags($tags,$this->module);
								$query = "DELETE FROM posts WHERE post_id = ".$post_id[$i];
								$result = $this->utils->db->query($query);
								$this->utils->error($result,__LINE__,get_class($this));
								//Delete the relations with categories			
								$this->delete_post_attachments($post_id[$i]);						
								$this->delete_item_relation(array('id' => $post_id[$i]));
								//Delete the related tags
								$tools->delete_post_tags(array("post_id" => $post_id[$i], "module_id" => $this->module_id));
								//Delete the related meta
								$this->meta->delete_meta(array('post_id' => $post_id[$i], "module" => $this->module,'type' => 'post'));
							} catch(PDOException $ex){
								$this->utils->db->rollback();
								$this->errors = $ex->getMessage();
								return false;	
							}//end try	
							
							$this->utils->db->commit();
						}//end if
					}//end for
				}//end if	
					
				return true;
			} else {
				return false;	
			}//end if
		}//end function			
		
		public function delete_item_relation(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if
			
			if(!isset($type)){
				$type = 'post';	
			}//end if
			
			$query = "DELETE FROM modules_items_order
					  WHERE module_item_order_module_id = ".$this->module_id."
					  AND module_item_order_type = '".$type."'
					  AND module_item_order_item_id = ".$id;
			if(isset($parent_id)){
				$query .= " AND module_item_order_father_id = ".$parent_id;
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function delete_post_attachments($pid){			
			$module_id = $this->utils->get_module($this->module);
			$module_id = $module_id['module_id'];
			$query = "SELECT * FROM attachments_posts 
					  WHERE attachment_post_post_id = ".$pid."
					  AND attachment_post_module_id = ".$module_id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$attachments = $this->utils->get_result_array($result,false);			
					for($i = 0; $i < sizeof($attachments); $i++){
						$aID = $attachments[$i]['attachment_post_file_id'];
						$this->utils->delete_attachment(array("file_id" => $aID));
					}//end for
				} else {
					return true;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function new_category(){
			$module_id = $this->utils->get_module($this->module);
			$module_id = $module_id['module_id'];
			if($_GET['parent_id'] != NULL){
				$_POST['parent_id'] = $_GET['parent_id'];
			}//end if
			
			$seo = $this->utils->call("seo");
			$perma_options = array("type" => "category");
			if(!is_null($_POST['id'])){
				$perma_options['id'] = $_POST['id'];	
			}
			if ($_POST['permalink'] != NULL){	
				$perma_options["permalink"] = $_POST['permalink'];			
				$valid_link = $seo->return_valid_permalink($perma_options);
				$_POST['permalink'] = $valid_link;
			} else if ($_POST['permalink'] == NULL && $_POST['id'] == NULL){	
				$perma_options["permalink"] = $seo->clean_link($_POST['name']);
				$valid_link = $seo->return_valid_permalink($perma_options);
				$_POST['permalink'] = $valid_link;
			}//end if
			
			$_POST['uid'] = $this->utils->get_uid();
			
			//These fields will be omitted 
			$hidden_fields = array('action','step','submit','id','permatemp','module');		
			if($_REQUEST['id'] == NULL){
				$_POST['module_id'] = $module_id;
				//create the insert sql script
				//$sql = $this->utils->build_sql_fields("category","INSERT","POST",$hidden_fields);			
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "category","sql_type" => "INSERT", "method_data" => "POST", "hidden_fields" => $hidden_fields, "module" => $this->module, "module_table" => "categories"));
				$query = "INSERT INTO categories ".$sql;
				$result = $this->utils->db->query($query);				
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$this->cid = $this->utils->db->lastInsertId();
					if($_POST['parent_id'] == NULL){
						$this->put_into_mainpages($this->cid,"category");
					} else {
						$this->update_module_relationship($this->cid,$module_id,"category",$_POST['parent_id']);
					}//end if
				} else {
					return false;
				}//end if
			} else {	
				//create the insert sql script
				$this->cid = $_REQUEST['id'];
				//$this->update_post_dependencies($this->cid,"category");
				//$sql = $this->utils->build_sql_fields("category","UPDATE","POST",$hidden_fields);	
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "category","sql_type" => "UPDATE", "method_data" => "POST", "hidden_fields" => $hidden_fields, "module" => $this->module, "module_table" => "categories"));		
				$query = "UPDATE categories SET ".$sql." WHERE category_id = ".$_REQUEST['id'];							
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors == false){
					if($_POST['parent_id'] != NULL){
						$this->remove_from_mainpages($this->cid,'category');
						$this->update_module_relationship($this->cid,$module_id,"category",$_POST['parent_id']);
					} else {
						$this->put_into_mainpages($this->cid,"category");
					//	$this->update_module_relationship($this->cid,$module_id,"category","0");	
					}//end if
				} else {
					return false;	
				}//end if
			}//end if
			
			/* META SUPPORT */	
			$this->meta->set_meta_type('category');		
			$this->meta->set_meta_id($this->cid);	
			$table_fields = $this->utils->get_table_fields(array("module" => $this->module,"field_prefix" => "category","module_table" => "categories", "show_all" => true));			
			array_walk($table_fields,array($this->utils,'clean_prefix'),$table_prefix);		
			$this->meta->auto_add_meta(array("avoid" => $table_fields));
			/* END META SUPPORT */
			
			return true;
		}//end function
		
		public function get_category_id(){
			return $this->cid;
		}//end function
		
		public function set_category_father_id($fcid){
			$this->fcid = $fcid;
		}//end function
		
		public function unset_category_father_id(){
			$this->fcid = NULL;
		}//end function
		
		public function force_category_posts_template(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($force_all)){
				$force_all = false;	
			}//end if
			
			if(!isset($parent_id)){
				return false;	
			}//end if
			
			if(!isset($template) || $template == ""){
				return false;
			}//end if
		
			$posts = $this->get_posts(array('category_id' => $parent_id));
			if($posts !== false){
				for($i = 0; $i < sizeof($posts); $i++){
					if($force_all == true || ($force_all == false && $posts[$i]['post_template'] == 'default')){
						$this->update_post_field(array('post_id' => $posts[$i]['post_id'],'field' => 'post_template', 'value' => $template));
					}//end if
				}//end if
				return true;
			} else {
				return true;	
			}//end if
		}//end function
		
		public function get_num_subcategories($cid){
			$query = "SELECT COUNT(*) AS subcats FROM categories WHERE category_father_id = ".$cid;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$row = $result->fetch(PDO::FETCH_ASSOC);
					return $row['subcats'];
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end if
		
		public function has_subcategories($cid){
			$num = $this->get_num_subcategories($cid);
			if($num == 0){
				return false;
			} else {
				return true;
			}//end if
		}//end function
		
		public function get_categories(){
			$params = func_get_args();		
			if(!is_array($params[0])){
				$hidden_childrend_categories = $params[0];				
				$sort_by = $params[1];				
				$sort_order = $params[2];				
				$gid = $params[3];
				$module_id = $params[4];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;									
				}//end if
			}//end if
		
			if(!isset($hidden_childrend_categories)){
				$hidden_childrend_categories = false;	
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "category_name";	
			}//end if
			
			if($sort_order == NULL){					
				$this->settings = $this->utils->get_settings(array('module_id' => $this->module_id));			
				$sort_order = $this->settings['list_order'];				
			}//end if		
			
			if(isset($parent_id) && !is_null($parent_id)){
				$this->fcid = $parent_id;	
				$hidden_childrend_categories = false;
			}//end if			
			
			if($module_id == NULL){
				$module_id = $this->utils->get_module($this->module);
				$module_id = $module_id['module_id'];
			} else {
				$module_id = $this->utils->get_module($module_id);
				$module_id = $module_id['module_id'];
			}//end if
			$query = "SELECT categories.*
					  FROM (categories,modules_items_order)";			
			if(isset($meta_filter)){
				$query .= "LEFT JOIN meta
						   ON (meta_type = 'category' 
						   		AND meta_ref_id = category_id 
								AND meta_module_id = category_module_id 
								AND meta_label = '".$meta_filter['label']."'";
				if(!is_array($meta_filter['value'])){
					$query .= " AND meta_value = '".$meta_filter['value']."') ";
				} else {
					$query .= " AND meta_value IN ('".implode("','",$meta_filter['value'])."')) ";
				}//end if
			}//end if
			$query .= " WHERE categories.category_module_id = ".$module_id;	
			$query .= " AND module_item_order_type = 'category'
						AND module_item_order_item_id = category_id
						AND module_item_order_module_id = category_module_id";
			if(isset($status)){
				$query .= " AND category_status = ".$status;
			}//end if
			if($hidden_childrend_categories == true){
				$query .= " AND category_father_id = 0";
			}//endif			
			if($hidden_childrend_categories == false && $this->fcid != NULL){
				$query .= " AND category_father_id = ".$this->fcid;
			} else if($hidden_childrend_categories == false && $this->fcid == NULL){
				$query .= " AND category_father_id = 0";
			}//endif
			if(isset($meta_filter)){
				$query .= " AND meta_id IS NOT NULL";	
			}//end if
			//$query .= " ORDER BY ".$sort_by." ".$sort_order;	
			if(!isset($use_system_order) || $use_system_order == false){
				$query .= " ORDER BY ".$sort_by." ".$sort_order;			
			} else {
				$query .= " ORDER BY module_item_order_order ".$sort_order;	
			}//end if
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if				
		
			//	echo $query;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);		
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type("category");
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['category_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$this->meta->set_meta_type("category");
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['category_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					/* ATTACHMENT SUPPORT */
					if(isset($default_photo) && $default_photo === true){
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$photo = $this->utils->get_attachments(array('post_id' => $data[$i]['category_id'],
																	   'role' => 'default-image', 
																	   'module_id' => $this->module_id, 
																	   'limit' => 1, 
																	   'status' => 'published',
																	   'type' => 'image',
																	   'attachment_post_type' => 'category'));															   
								if($photo !== false){
									$data[$i]['photo'] = $photo;
								}//end if
							}//end if
						}//end if
					}//end if 			
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function	
				
		public function delete_category(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$category_id = $params[0];	
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($category_id)){
				return false;	
			}//end if
			
			//Delete the category
			try {
				$this->utils->db->beginTransaction();
				//Before we delete the category we get its info
				$category_data = $this->get_category(array('id' => $category_id));
				if($category_data === false){
					throw new Exception("Cannot find the category to delete");	
					return false;
				} else {
					$parent_id = $category_data['category_father_id'];
					$query = "DELETE FROM categories WHERE category_id = ".$category_id;
					$this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));	
					//Delete the module relation with this category
					$query = "DELETE FROM modules_items_order WHERE module_item_order_module_id = ".$this->module_id." AND module_item_order_item_id = ".$category_id." AND module_item_order_type = 'category'";
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));	
					//Delete the relations with posts
					$query = "UPDATE modules_items_order SET module_item_order_father_id = ".$parent_id." WHERE module_item_order_father_id = ".$category_id." AND module_item_order_type = 'post' AND module_item_order_module_id = ".$this->module_id;
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));	
					//Delete the related meta
					$this->meta->delete_meta(array('post_id' => $category_id, "module" => $this->module,'type' => 'category'));
					$this->utils->db->commit();
					return true;
				}//end if
			} catch (PDOException $ex) {
				//Something went wrong rollback!
				$this->utils->db->rollBack();
				$this->errors = $ex->getMessage();
				return false;
			}//end if
		}//end function
		
		//Return the name of the category passed
		public function get_category(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$cid = $params[0];	
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($id)){
				$cid = $id;	
			}//end if
			
			$query = "SELECT categories.*, modules.* 
					  FROM categories, modules 
					  WHERE category_module_id = module_id ";
			if(isset($cid)){
				if(is_numeric($cid)){
					$query .= " AND category_id = ".$cid;	
				} else {
					return false;	
				}//end if
			}
			
			if(isset($permalink)){
				$permalink = $this->utils->db->quote($permalink);
				$query .= " AND category_permalink = ".$permalink;
			}//end if
			
			$query .= " LIMIT 1";
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){				
					$data = $this->utils->get_result_array($result,true);		
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;	
					}//end if
					if(!isset($raw)){
						$raw = false;	
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type('category');
						$meta_data = $this->meta->get_meta(array("id" => $cid, "html" => (bool)$html, "raw" => (bool)$raw));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for	
						$this->meta->set_meta_type('category');				
						$meta_data = $this->meta->get_meta(array("id" => $cid,"search" => $meta, "html" => (bool)$html, "raw" => (bool)$raw));				
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					}//end if
					/* END META SUPPORT */	
					/* ATTACHMENT SUPPORT */
					if(isset($default_photo) && $default_photo === true){
						if($data != false){	
							$vars = array('post_id' => $data['category_id'],
																   'role' => 'default-image', 
																   'module_id' => $this->module_id, 
																   'limit' => 1, 
																   'status' => 'published',
																   'type' => 'image',
																   'attachment_post_type' => 'category');																  
							$photo = $this->utils->get_attachments($vars);															   
							if($photo !== false){
								$data['photo'] = $photo;
							}//end if
						}//end if
					}//end if 	
					return $data;
				} else {
					return false;
				}//end if	
			} else {
				return false;	
			}//end if
		}//end function
		
		public function save_categories_sorting($id,$post_ids,$sort_group = true){
			$pi = $post_ids;
			if($sort_group == true){
				$gid = $id;
				if(is_array($pi) && sizeof($pi) > 0){
					for($i = 0; $i < sizeof($pi); $i++){
						$query = "UPDATE categories
								  SET category_order = ".$i."
								  WHERE category_gid = ".$gid."
								  AND category_id = ".$pi[$i];
						$result = $this->utils->db->query($query);
						$this->utils->error($result,__LINE__,get_class($this));	
					}//end for
					return true;
				} else {
					return false;
				}//end if	
			} else {
				//I sort the categories under a category father
				if($id != NULL){
					$cid = $id;
					if(is_array($pi) && sizeof($pi) > 0){
						for($i = 0; $i < sizeof($pi); $i++){
							$query = "UPDATE categories
									  SET category_order = ".$i."
									  WHERE category_father_id = ".$cid."
									  AND category_id = ".$pi[$i];
							$result = $this->utils->db->query($query);
							$this->utils->error($result,__LINE__,get_class($this));	
						}//end for
						return true;
					} else {
						return false;
					}//end if
				} else {
					if(is_array($pi) && sizeof($pi) > 0){
						for($i = 0; $i < sizeof($pi); $i++){
							$query = "UPDATE categories
									  SET category_order = ".$i."
									  WHERE category_id = ".$pi[$i];
							$result = $this->utils->db->query($query);
							$this->utils->error($result,__LINE__,get_class($this));	
						}//end for
						return true;
					} else {
						return false;
					}//end if
				}//end if
			}//end if	
		}//end function
				
		
		public function update_post_field(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value_array){
					${$key} = $value_array;
				}//end if
			}//end if
			
			if(!isset($post_id) || !isset($field) || !isset($value)){
				return false;	
			}//end if
			
			$query = "UPDATE ".$this->table." SET ".$field." = '".$value."' WHERE ".$this->prefix."_id = ".$post_id;					
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end if
		
		public function update_category_field(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value_array){
					${$key} = $value_array;
				}//end if
			}//end if
			
			if(!isset($category_id) || !isset($field) || !isset($value)){
				return false;	
			}//end if
			$query = "UPDATE categories SET ".$field." = '".$value."' WHERE category_id = ".$category_id;
			$result = $this->utils->db->query($query);
			if($this->utils->error($result,__LINE__,get_class($this))){
				return false;
			} else {
				return true;
			}//end if
		}//end if
		
		
	}//end class
?>