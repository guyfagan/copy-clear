<?php
	class help{
		
		protected $utils;//utils class	
		public $errors;	
		protected $settings;
	
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->utils->read_params($this,$params);	
		}//endconstructor	
		
		public static function get_library(){
			$params = func_get_args();
			if(is_array($params[0])){	
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			$library = array();
	
			if(file_exists("modules/help/library/")){						
				$path = __CMS_PATH__."modules/help/library/";
				$path = str_replace('//','/',$path);
				$abs_path = $_SERVER['DOCUMENT_ROOT'].$path;
				$files = scandir($abs_path);				
				$avoid_files = array('.','..','.AppleDouble','.DS_Store');				
				for($x = 0; $x < sizeof($files); $x++){	
					if(file_exists($abs_path.$files[$x]) && !is_dir($abs_path.$files[$x]) && !in_array($files[$x],$avoid_files) && strpos($files[$x],'default') === false){	
						$data = array();						
						$data['file'] = $files[$x];
						array_push($library,$data);
					}//end if
				}//end for i
				return $library;
			} else {
				return false;
			}//end if			
			
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
				
		private function init_settings(){			
			$this->settings = $this->utils->get_settings(array('module' => $this->module));
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>
