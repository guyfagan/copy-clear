<?php
	#################################################################
	# Users Class - 	last update 22/12/12						#
	#					created in 2008								#
	# This is a core class for the Old Hat CMS						#	
	#																#
	# 16/10/12 - Added more functions to support the meta class, 	#
	#			 like now it's possible to optionally get the meta  #
	#			 in the get_users() method							#	
	#		   - Added meta support to method get_user_data as well #	
	#		   - Added the support to enhanced module settings		#	
	# 17/10/12 - Fixed some issues on get_user method				#
	# 16/11/12 - Deleted the password from the get_user method,		#
	#			 also get_users now returns the users_group_is		#
	# 06/12/12 - Cleaned get_group_permissions and get_group methods#
	# 22/12/12 - Class conveted to PDO								#
	#################################################################
	class users{

		public $utils;//utils class		
		public $uid;//User ID
		public $gid;//Group ID
		public $lt; //defines the login type, sessions or cookies
		public $errors = array();
		protected $meta;
		protected $settings;
		
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;			
			$this->lt = __LOGIN_TYPE__;
			$this->utils->read_params($this,$params);
			$this->module = "users";
			//Meta init	
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);	
			//Set basic settings
			$this->init_settings();
		}//endconstructor		
		
		public function new_user(){
			if($_REQUEST['id'] == NULL){
				//Check first if the username is already used
				if($this->user_exists($_POST['username']) == true){
					array_push($this->errors,"User already exists");					
				}//end if
				//Check if the email is already used
				if($this->email_exists($_POST['email']) == true){
					array_push($this->errors,"Email already exists");					
				}//end if	
			}//end if		
			//Check if the password are identical
			if($_POST['password'] != $_POST['conf_password']){
				array_push($this->errors,"Passwords do not match");				
			}//end if	
			
			if(sizeof($this->errors) > 0){
				return false;	
			}//end if
			
			if($this->save_user()){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		protected function save_user(){	
			//These fields will be omitted 
			$hidden_fields = array('action','a','step','submit','id','conf_password','group_id','module');
			if(!isset($_POST['active'])){
				$_POST['active'] = 1;
			}//end if
			//create the insert sql script
			if($_REQUEST['id'] == NULL){
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "user","sql_type" => "INSERT", "method_data" => "POST", "hidden_fields" => $hidden_fields, "module" => $this->module, "module_table" => "users"));
				$query = "INSERT INTO users ".$sql;	
			} else {
				$this->uid = $_REQUEST['id'];
				array_push($hidden_fields,"password");
				$sql = $this->utils->build_sql_fields(array("table_prefix" => "user","sql_type" => "UPDATE", "method_data" => "POST", "hidden_fields" => $hidden_fields, "module" => $this->module, "module_table" => "users"));
				if($_POST['password'] != NULL){
					$sql .= ", user_password = '".sha1($_POST['password'])."'";
				}//end if
				$query = "UPDATE users SET ".$sql." WHERE users.user_id = ".$this->uid;
			}//end if	
			//echo $query;							
			$result = $this->utils->db->query($query);
			$this->utils->error($result,__LINE__,get_class($this));
			if($_REQUEST['id'] != NULL){
				$this->uid = $_REQUEST['id'];
			} else {
				$this->uid = $this->utils->db->lastInsertId();
			}//end if			
			//Create relation between groups and user
			if($_POST['group_id'] != NULL){
				$this->create_user_group_relation($this->uid,$_POST['group_id']);
			}//end if
			
			/* META SUPPORT */	
			$this->meta->set_meta_type('user');				
			$this->meta->set_meta_id($this->uid);	
			$table_fields = $this->utils->get_table_fields(array("module" => $this->module,"field_prefix" => "user", "show_all" => true));			
			array_walk($table_fields,array($this->utils,'clean_prefix'),$table_prefix);		
			$this->meta->auto_add_meta(array("avoid" => $table_fields));		
			/* END META SUPPORT */
			return true;
		}//end function
		
		public function create_user_group_relation($uid,$gid){
			//Delete previous relationship if exists
			$query = "DELETE FROM users_groups_rel WHERE users_groups_rel_uid = ".$uid;
			$result = $this->utils->db->query($query);
			$result = $this->utils->error($result,__LINE__,get_class($this));
			//Create a new relation between the user and the selected group
			$query = "INSERT INTO users_groups_rel
					  (users_groups_rel_uid, users_groups_rel_gid)
					  VALUES
					  (".$uid.",".$gid.")";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		//This function check if the passed username already exists
		public function user_exists($username){
			$query = "SELECT * FROM users WHERE user_username = '".$username."'";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				//if it found an user with same name, return true
				if($num >= 1){
					return true;
				} else {
					return false;
				}//endif
			} else {
				return false;	
			}//end if
		}//end function
		
		public function email_exists($email){
			$query = "SELECT * FROM users WHERE user_email = '".$email."' AND user_temp = 0";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				//if it found an email in the db, return true
				if($num >= 1){
					return true;
				} else {
					return false;
				}//endif
			} else {
				return false;	
			}//end if
		}//end function
		
		//Give the number of the group's members
		public function get_num_members($gid){
			$query = "SELECT count(*) AS members FROM users_groups_rel, users
					  WHERE users_groups_rel_gid = ".$gid."
					  AND users_groups_rel_uid = user_id
					  AND user_temp = 0";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$row = $result->fetch(PDO::FETCH_ASSOC);	
				return $row['members'];	
			} else {
				return false;	
			}//end if
		}//end function
		
		public function check_page_permission($module){
			$query = "SELECT modules.*, users_permissions.user_permission_permission
					  FROM (users_permissions, modules, users_groups_rel) 
					  WHERE users_groups_rel.users_groups_rel_uid = ".$this->uid."
					  AND users_permissions.user_permission_gid = users_groups_rel.users_groups_rel_gid
					  AND users_permissions.user_permission_module_id = modules.module_id
					  AND modules.module_name = '".$module."'";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$row = $result->fetch(PDO::FETCH_ASSOC);
					if($row['user_permission_permission'] > 0){
						return true;
					} else {
						//insufficient permission, not enabled to access to selected page
						return false;
					}//end if
				} else {
					//no records, not enabled to access to selected page
					return false;
				}//end if	
			} else {
				return false;
			}//end if
		}//end function
		
		public function set_default_module_permissions(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($module_id)){
				return false;	
			}//end if
			
			if(!isset($permission)){
				$permission = 7;	
			}//end if
			
			//first check if this module has already permissions
			$query = "SELECT * FROM users_permissions WHERE user_permission_module_id = ".$module_id;		
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){						
					//this module has already permission, so we don't touch it
					return false;
				} else {
					//this module is brand new with no permissions, so we create some 
					//Now we need to find the users groups marked as admins
					$groups = $this->get_groups(array("admin_only" => 1));					
					if($groups !== false){
						for($i = 0; $i < sizeof($groups); $i++){
							$query = "INSERT INTO users_permissions
									  (user_permission_gid, user_permission_module_id, user_permission_permission)
									  VALUES
									  (:group_id, :module_id, :permission)";									  
							$result = $this->utils->db->prepare($query);
							$result->bindParam(':group_id', $groups[$i]['users_group_id'], PDO::PARAM_INT);
							$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);
							$result->bindParam(':permission', $permission, PDO::PARAM_INT);
							$result->execute();
							$errors = $this->utils->error($result,__LINE__,get_class($this));
							if($errors === false){
								return true;
							} else {
								return false;
							}//end if
						}//end for i
					} else {
						//no groups found, so nothing to set
						return false;	
					}//end if
				}//end if
			} else {
				//something went wrong
				return false;	
			}//end if
		}//end function
		
		//return the user permissions in an array
		public function user_permissions($uid = NULL){
			//Check if the uid is passed or not
			if($uid != NULL){
				$this->uid = $uid;
			} else {
				$this->uid = $this->get_uid();
			}//end if
				
			//Check on the database the permission for this user
			$query = "SELECT users_permissions.*, modules.module_name
					  FROM (users_permissions, modules, users_groups_rel) 
					  WHERE users_groups_rel.users_groups_rel_uid = ".$this->uid."
					  AND users_permissions.user_permission_gid = users_groups_rel.users_groups_rel_gid
					  AND users_permissions.user_permission_module_id = modules.module_id";					 
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				//if it found something in the db, give the permissions
				if($num >= 1){
					$data = array();
					while($row = $result->fetch(PDO::FETCH_ASSOC)){
						$data[$row['module_name']] = $row['user_permission_permission'];					
					}//endwhile
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		//Return all the information of the passed  userid, or if it's null, return the data of the current user
		public function get_user(){	
			$params = func_get_args();		
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$uid = $params[0];				
			}//end if					
			
			if(isset($id)){
				$uid = $id;	
			}//end if
			
			if(!isset($show_password)){
				$show_password = false;	
			}//end if
					
			if($uid == NULL && !isset($username)){
				$uid = $this->get_uid();
			}//end if
			if($uid != NULL || isset($username)){
				//Find on the db all the userdata
				$query = "SELECT users.*, users_group_name, users_group_admin, 
						  users_groups.users_group_id as user_group_id, users_groups_rel_gid as group_id
						  FROM (users, users_groups_rel, users_groups)";
				if(isset($uid) && !isset($username)){
					$query .= " WHERE user_id = ".$uid;
				} else if(!isset($uid) && isset($username)){
					$query .= " WHERE user_username = '".$username.'"';
				}//end if
						  
				$query .= " AND users_groups_rel.users_groups_rel_uid = users.user_id
						  AND users_groups_rel.users_groups_rel_gid = users_groups.users_group_id
						  LIMIT 1";														  
				$result = $this->utils->db->query($query);			
				$errors = $this->utils->error($result,__LINE__,get_class($this));	
				if($errors === false){
					$data = $this->utils->get_result_array($result,true);
					
					if($data !== false){						
						//delete the password from the response						
						if($show_password == false){
							$data['user_password'] = NULL;			
						}//end if
						/* META SUPPORT */	
						$this->meta->set_meta_type('user');	
						//if we pass the value meta as a boolean, we get all the meta into the final data array
						if(isset($meta) && !is_array($meta) && (bool)$meta == true){
							$meta_data = $this->meta->get_meta(array("id" => $uid));
							if($meta_data != false){
								$data = array_merge($data,$meta_data);
							}//end if
						} else if(isset($meta) && is_array($meta)){
							//if it's an array, we just get the meta we asked for
							$meta_data = $this->meta->get_meta(array("id" => $uid,"search" => $meta));
							if($meta_data != false){
								$data = array_merge($data,$meta_data);
							}//end if
						}//end if
						/* END META SUPPORT */	
						return $data;
					} else {
						return false;
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
			$this->clear_uid();
		}//endfunction
		
		public function change_user_status(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				//legacy mode
				$id = $params[0];
				$status = $params[1];				
			}//end if
			
			if(!isset($id) || !isset($status)){
				return false;	
			}//end if
			$query = "UPDATE users SET user_active = ".$status." WHERE user_id = ".$id;
			$result = $this->utils->db->query($query);			
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function delete_user(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if
			
			if(!isset($real_delete)){
				$real_delete = false;	
			}//end if
			
			if($real_delete == true){
				try {
					$this->utils->db->beginTransaction();
					//Delete relations with groups
					$query = "DELETE FROM users_groups_rel WHERE users_groups_rel_uid = ".$id;
					$result = $this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));	
					//Delete user information
					$query = "DELETE FROM users WHERE user_id = ".$id;
					$result = $this->utils->db->query($query);
					$this->utils->error($result,__LINE__,get_class($this));	
					$this->utils->db->commit();
					return true;
				} catch(PDOException $ex) {
					//Something went wrong rollback!
					$this->utils->db->rollBack();
					$this->errors = $ex->getMessage();
					return false;
				}//end if
			} else {
				$this->change_user_status($id,0);
			}//end if
			return true;
		}//end function
		
		public function get_users(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$group_id = $params[0];				
				$sort_by = $params[1];				
				$sort_order = $params[2];				
				$active = $params[3];		
			} else {
				foreach($params[0] as $pkey => $value){
					${$pkey} = $value;
				}//end if
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "user_username";
			}//end if
			if($sort_order == NULL){
				$sort_order = "ASC";
			}//end if
			
			
			//query
			$query = "SELECT users.*, users_group_name, users_group_id
					  FROM (users)
					  LEFT JOIN users_groups_rel 
					  ON users_groups_rel.users_groups_rel_uid = users.user_id
					  LEFT JOIN users_groups
					  ON users_groups_rel.users_groups_rel_gid = users_groups.users_group_id
					  WHERE user_temp = 0";
			if(isset($active) && !is_null($active)){
				$query .= " AND user_active = ".(int)$active;
			}//end if
			if(!is_null($group_id)){
				if(is_array($group_id)){
					$query .= " AND users_groups_rel_gid IN (".implode(",",$group_id).")";
				} else {
					$query .= " AND users_groups_rel_gid = ".$group_id;
				}//end if
			}//end if
			
			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND user_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND user_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if
						
			if(isset($search) && !is_null($search)){
				$search = trim($search);
				$search = str_replace(" ","%",$search);
				$query .= " AND (user_username LIKE '%".$search."%' OR user_email LIKE '%".$search."%' OR user_firstname LIKE '%".$search."%' OR user_surname LIKE '%".$search."%')";
			}//end if
			$query .= " ORDER BY users.".$sort_by." ".$sort_order;
			//echo $query;
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);				
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;				
			}//end if	
			
			$result = $this->utils->db->query($query);			
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				
				$num = $result->rowCount();
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);	
					/* META SUPPORT */	
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type('user');	
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['user_id']));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$this->meta->set_meta_type('user');	
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['user_id'],"search" => $meta));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_uid(){
			if($_COOKIE['uid'] != NULL){
				return $_COOKIE['uid'];
			} else if($_SESSION['uid'] != NULL){
				return $_SESSION['uid'];				
			} else {
				return false;
			}//end if
		}//end function
		
		public function clear_uid(){
			$this->uid = NULL;
		}//end function
		/*
		public static function uid(){
			
			if($_COOKIE['uid'] != NULL){
				return $_COOKIE['uid'];
			} else if($_SESSION['uid'] != NULL){
				return $_SESSION['uid'];
			} else {
				return false;
			}//end if
		}//end function
		*/
		
		//This method return the list of the groups in the database
		public function get_groups(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$sort_by = $params[0];				
				$sort_order = $params[1];				
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(is_null($sort_by)){
				$sort_by = "users_group_name";	
			}//end if
			
			if(is_null($sort_order)){
				$sort_order = "ASC";	
			}//end if
			
			$query = "SELECT * FROM users_groups";
			if(isset($admin_only)){
				$query .= " WHERE users_group_admin = ".(int)$admin_only;
			}//end if
			$query .= " ORDER BY ".$sort_by." ".$sort_order;			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);			
					/* META SUPPORT */	
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type('group');	
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['users_group_id']));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$this->meta->set_meta_type('group');	
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['users_group_id'],"search" => $meta));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;	
			}//end if
		}//end function
		
		//Delete the passed group
		public function delete_group(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($id)){
				return false;	
			}//end if
			
			if(!isset($delete_users)){
				$delete_users = false;
			}//end if
			
			try {
				$this->utils->db->beginTransaction();
				if($delete_users){
					//get users
					$group_users = $this->get_users(array('group_id' => $id));
					if($group_users !== false){						
						for($i = 0; $i < sizeof($group_users); $i++){
							$options['id'] = $group_users[$i]['user_id'];							
							$query = "DELETE FROM users WHERE user_id = ".$group_users[$i]['user_id'];
							$this->utils->db->query($query);
							$this->utils->error($result,__LINE__,get_class($this));
						}//end for i
					}//end if
				}//end if
				//Delete the group
				$query = "DELETE FROM users_groups WHERE users_group_id = ".$id;
				$this->utils->db->query($query);
				$this->utils->error($result,__LINE__,get_class($this));	
				//Delete all the groups <-> users relationships
				$query = "DELETE FROM users_groups_rel WHERE users_groups_rel_gid = ".$id;
				$this->utils->db->query($query);
				$this->utils->error($result,__LINE__,get_class($this));
				$this->utils->db->commit();
				return true;
			} catch(PDOException $ex) {
				//Something went wrong rollback!
				$this->utils->db->rollBack();
				$this->errors = $ex->getMessage();
				return false;
			}//end if
		}//end function
		
		public function save_group(){
			//Catch the group name
			$group_name = addslashes(trim($_POST['name']));
			if($group_name == NULL){
				return false;
			}//end if
			//Check if is editing a group
			if($_REQUEST['id'] != NULL){
				$this->gid = $_REQUEST['id'];
			}//end if
		
			$admin = false;
			if(isset($_REQUEST['admin'])){
				if($_REQUEST['admin'] == 1){
					$admin = true;	
				}//end if
			}//end if
		
			if($this->gid == NULL){
				//Insert the group on the db
				$query = "INSERT INTO users_groups
						  (users_group_name, users_group_admin)
						  VALUES
						  ('".$group_name."',".(int)$admin.")";
				$result = $this->utils->db->query($query);
				$this->utils->error($result,__LINE__,get_class($this));	
				//Assign the new group id
				$this->gid = $this->utils->db->lastInsertId();;
			} else {
				//update
				$query = "UPDATE users_groups SET
						  users_group_name = '".$group_name."',
						  users_group_admin = ".(int)$admin."
						  WHERE users_group_id = ".$this->gid;
				$this->utils->db->query($query);
				$this->utils->error($result,__LINE__,get_class($this));		
				//After that we have to delete the old permission and ad new data
				$query = "DELETE FROM users_permissions WHERE user_permission_gid = ".$this->gid;		
				$result = $this->utils->db->query($query);
				$this->utils->error($result,__LINE__,get_class($this));			
			}//end if
			//Now we have to create the permissions for this group
			//catch all the permissions
			$modules = $this->utils->get_modules_list(NULL,true);
			for($i = 0; $i < sizeof($modules); $i++){
				$module_id = $modules[$i]['module_id'];
				$permission = $_POST[$modules[$i]['module_name']];
				//Check if permission is null
				if($permission == NULL){
					$permission = 0;
				}//endif
				$query = "INSERT INTO users_permissions
						  (user_permission_gid,user_permission_module_id,user_permission_permission)
						  VALUES
						  (".$this->gid.",".$module_id.",".$permission.")";
				$result = $this->utils->db->query($query);
				$this->utils->error($result,__LINE__,get_class($this));
			}//end for
			return true;
		}//end function
		
		public function check_old_pwd($uid,$oldpwd){
			$oldpwd = sha1($oldpwd);
			$query = "SELECT * FROM users WHERE user_id = ".$uid." AND user_password = '".$oldpwd."'";
			$result = $this->utils->db->query($query);
			$num = $result->rowCount();
			if($num != 1){
				return false;
			} else {
				return true;
			}//end if
		}//end function		
		
		public function get_group(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$gid = $params[0];				
			}//end if					
			
			if(isset($id)){
				$gid = $id;	
			}//end if
			
			
			if(!is_numeric($gid)){
				//Is string
				$sql = "users_group_name = '".strtolower($gid)."'";
			} else {
				//Is numeric
				$sql = "users_group_id = ".$gid;
			}//endif
			$query = "SELECT * FROM users_groups WHERE ".$sql;	
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					/* META SUPPORT */	
					$this->meta->set_meta_type('group');	
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$meta_data = $this->meta->get_meta(array("id" => $gid));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$meta_data = $this->meta->get_meta(array("id" => $gid,"search" => $meta));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					}//end if
					/* END META SUPPORT */			
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_group_permissions(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$gid = $params[0];				
			}//end if					
			
			if(isset($id)){
				$gid = $id;	
			}//end if
			$query = "SELECT users_permissions.user_permission_permission, modules.module_name
					  FROM (users_permissions, modules) 
					  WHERE users_permissions.user_permission_gid = ".$gid."
					  AND users_permissions.user_permission_module_id = modules.module_id";
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				//if it found something in the db, give the permissions
				if($num >= 1){
					$data = array();
					while($row = $result->fetch(PDO::FETCH_ASSOC)){
						$data[$row['module_name']] = $row['user_permission_permission'];					
					}//endwhile
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function generate_random_password($uid){
			if($uid == NULL && !is_numeric($uid)){
				return false;
			}//end if
			$newpass = rand(10000,99999);
			$this->change_password($uid,$newpass);
			$query = "UPDATE users SET user_temp_pwd = '".$newpass."' WHERE user_id = ".$uid;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return $newpass;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function change_password($uid,$password){
			$p = sha1($password);
			$query = "UPDATE users SET user_password = '".$p."' WHERE user_id = ".$uid;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_secure_id(){
			$udata = $this->get_user();
			if($udata != false){				
				$query = "SELECT HEX(".$udata['user_id'].") AS uidhex, HEX('".$udata['user_username']."') AS unamehex";				
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$row = $result->fetch(PDO::FETCH_ASSOC);
					$string = sha1($row['uidhex'].$row['unamehex']);				
					if($_COOKIE['secure_id'] != NULL){
						if($_COOKIE['secure_id'] == $string){
							return $string;
						} else {
							return false;
						}//end if
					} else {
						if($_SESSION['secure_id'] == $string){
							return $string;
						} else {
							return false;
						}//end if
					}//end if
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function check_secure_id(){		
			$udata = $this->get_user();
			//var_dump($udata);
			if($udata != false){
				//Check if this user has some permission for something
				$gid = $udata['user_group_id'];
				$query = "SELECT SUM(user_permission_permission) AS total FROM users_permissions
						  WHERE user_permission_gid = ".$gid;											 
				$result = $this->utils->db->query($query);
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$row = $result->fetch(PDO::FETCH_ASSOC);			
					if($row['total'] == "0"){
						return false;
					}//end if					
					$query = "SELECT HEX(".$udata['user_id'].") AS uidhex, HEX('".$udata['user_username']."') AS unamehex";
					$result = $this->utils->db->query($query);
					$errors = $this->utils->error($result,__LINE__,get_class($this));
					if($errors === false){
						$row = $result->fetch(PDO::FETCH_ASSOC);
						$string = sha1($row['uidhex'].$row['unamehex']);	
						if($this->lt == "cookie"){
							if($_COOKIE['secure_id'] == $string){
								return true;
							} else {
								return false;
							}//end if
						} else {
							if($_SESSION['secure_id'] == $string){
								return true;
							} else {
								return false;
							}//end if
						}//end if			
					} else {
						return false;
					}//end if
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function simple_check_secure_id(){		
			$udata = $this->get_user();
			//var_dump($udata);
			if($udata != false){									
				$query = "SELECT HEX(".$udata['user_id'].") AS uidhex, HEX('".$udata['user_username']."') AS unamehex";			
				$result = $this->utils->db->query($query);			
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					$row = $result->fetch(PDO::FETCH_ASSOC);
					$string = sha1($row['uidhex'].$row['unamehex']);	
					if($this->lt == "cookie"){
						if($_COOKIE['secure_id'] == $string){
							return true;
						} else {
							return false;
						}//end if
					} else {
						if($_SESSION['secure_id'] == $string){
							return true;
						} else {
							return false;
						}//end if
					}//end if
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function send_message(){
			$type = $_POST['sendtype'];
			switch($type){
				case "email":
					$emails = trim($_POST['recipient']);
					$emails = explode(";",$emails);
					break;
				case "users":
					$emails = $_POST['checked_users'];
					break;
				case "groups":
					$emails = array();
					$names = array();
					$groups = $_POST['checked_groups'];
					$users = $this->utils->call("users");
					if(is_array($groups)){
						for($i = 0; $i < sizeof($groups); $i++){
							$group_id = $groups[$i];
							$gusers = $users->get_users($group_id,"user_username","ASC","1");
							if($gusers != false){
								for($k = 0; $k < sizeof($gusers); $k++){
									array_push($emails,$gusers[$k]['user_email']);
								}//end for k
							}//end if
						}//end for i
					} else {
						$group_id = $groups;
						$gusers = $users->get_users($group_id,"user_username","ASC","1");
						if($gusers != false){
							for($k = 0; $k < sizeof($gusers); $k++){
								array_push($emails,$gusers[$k]['user_email']);
							}//end for k
						}//end if
					}//end if					
					break;
			}//end switch
			
			array_push($emails,"developer@oldhat.ie");
		
			//$emails = array("alessio@oldhat.ie","michael@oldhat.ie","sinead@oldhat.ie");
			if($_POST['testmsg'] == "1"){
				$emails = array();
				$test_emails = $_POST['testmsg_email'];
				$test_emails = str_replace(",",";",$test_emails);
				$emails = explode(";",$test_emails);
			}//end if
			
			$title = trim($_POST['title']);
			$message = '<body style="font-family: Arial, Helvetica, sans-serif;	font-size:12px;">';
			$message .= trim($_POST['message']);
			//$message .= '<br /><img src="" width="200" height="81" />';
			$message .= '</body>';
			
			$sender = __NOREPLY_EMAIL__;
						
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: $sender";
			if(is_array($emails)){
				for($i = 0; $i < sizeof($emails); $i++){
					$user_message = $message;
					$user_message = str_replace(htmlentities("<%email%>"),$emails[$i],$user_message);
					echo "Sending to ".$emails[$i]." the following message: ".stripslashes($user_message)." <br />From: ".$sender;
					echo "<br />----------------<br />";
					$this->utils->store_email(array("sender" => __NOREPLY_EMAIL__,"recipient" => $emails[$i],"subject" => $title,"message" => $user_message, "headers" => $headers));
					if(@mail($emails[$i],$title,stripslashes($user_message),$headers)){
						$this->utils->mark_mail_as_sent();	
					}//end if 
				}//end for i
			} else {
				$this->utils->store_email(array("sender" => __NOREPLY_EMAIL__,"recipient" => $emails,"subject" => $title,"message" => $message, "headers" => $headers));
				if(@mail($emails,$title,$message,$headers)){
					$this->utils->mark_mail_as_sent();	
				}//end if
			}//end if
		}//end function
		
		#################################################################
		# CUSTOM BREADCRUMBS PART										#		
		#################################################################
		
		public function get_breadcrumb(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if		
			$html = "";
			if(isset($parent_id)){
				$group_data = $this->get_group(array('id' => $parent_id));
				if($group_data !== false){
				$html .= '<li><a href="'.__CMS_PATH__.'users/list/'.$group_data['users_group_id'].'/">'.$group_data['users_group_name'].'</a> <span class="divider">/</span></li>';
				}//end if
			}//end if
			return $html;
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
				
		protected function init_settings(){
			$this->settings = array();	
			$this->settings['mail_charset'] = 'iso-8859-1';			
			//read the settings from the cms
			$cms_settings = $this->utils->get_settings($this->module);
			if($cms_settings !== false){
				//if we have something, merge the two arrays, bear in mind that the cms settings will override the previous settings
				$this->settings = array_merge($this->settings,(array)$cms_settings);	
			}//end if
			
			if($this->lt == "cms"){
				if(isset($this->settings['login_type'])){
					$accepted_types = array('cookie','session');
					if(in_array($this->settings['login_type'], $accepted_types)){
						$this->lt = $this->settings['login_type'];	
					} else {
						//fallback to cookie
						$this->lt = 'cookie';
					}//end if
				} else {
					//fallback to cookie
					$this->lt = 'cookie';	
				}//end if
			}//end if
			
			$this->cookie_expiry = time()+((3600*24)*31);
			if(isset($this->settings['cookie_expiry']) && is_numeric(($this->settings['cookie_expiry'])) && (int)$this->settings['cookie_expiry'] > 0){
				$this->cookie_expiry = time()+(int)$this->settings['cookie_expiry'];
			}//end if
			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//endclasss
?>