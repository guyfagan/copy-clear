<?php
	#################################################################
	# Formsmemory Class - last update 27/09/12						#
	#################################################################

	class formsmemory{
		private $utils;//utils class
		private $t;//prefix before each tablename
		private $fid;//form id
		private $error_output;
	
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->t = __TABLE_PREFIX__;					
			$this->utils->read_params($this,$params);
			$this->error_output = NULL;
		}//endconstructor
		
		public function store_error_output($err){
			$this->error_output = $err;
		}//end function
		
		public function save_form($fid = NULL, $comefrom = NULL){
			if($comefrom == NULL){
				$comefrom = "";	
			}//end if
			
			$error_output = "NULL";
			if($this->error_output != NULL){
				$error_output = addslashes($this->error_output);
			}//end if
			
			//serialize/encrypt the form 
			$post_data = $_POST;		
			$post_data = urlencode(base64_encode(json_encode($post_data)));
			if($fid != NULL){
				//store this form in the database
				$query = "UPDATE formsmemory SET
						  formmemory_data = '".$post_data."', 
						  formmemory_uri = '".$comefrom."',
						  formmemory_error = '".$error_output."'
						  WHERE formmemory_id = ".$fid;
			} else {
				//store this form in the database
				$query = "INSERT INTO formsmemory
						  (formmemory_data, formmemory_uri, formmemory_error)
						  VALUES
						  ('".$post_data."','".$comefrom."','".$error_output."')";
			}//end if				
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$this->fid = $this->utils->db->lastInsertId();;
				return $this->fid;
			} else {
				return false;
			}//end if
		}//end function
		
		public function get_form($fid){
			$query = "SELECT * FROM formsmemory WHERE formmemory_id = ".$fid;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function clear_form($fid){
			$query = "DELETE FROM formsmemory WHERE formmemory_id = ".$fid;
			$result = $this->utils->db->query($query);
			$error = $this->utils->error($result,__LINE__,get_class($this));	
			if($error == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
	}//end class
?>