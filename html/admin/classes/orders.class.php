<?php
	require_once('site.class.php');

	class orders extends site{
		
		public $order_id;
		public $meta;
		protected $code;
		protected $default_status;
		public $settings = array();
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);		
			$this->utils->read_params($this,$params);	
			$this->set_module("orders");
			//Meta init	
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);	
			$this->meta->set_meta_type('order');
			$this->default_status = 'draft';
			//Set basic settings
			$this->init_settings();	
		}//endconstructor
		
		public function get_order_id(){
			return $this->order_id;	
		}//end function
		
		public function create_order(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($post_id) || !isset($module)){
				return false;
			}//end if
			
			if(!is_numeric($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];	
			}//end if
			
			if(!isset($status)){
				$status = $this->default_status;	
			}//end if
			
			if(!isset($cart_id)){
				$use_cart = false;
				$cart_id = 0;	
			} else if($cart_id > 0 && is_numeric($cart_id)){
				$use_cart = true;
			}//end if
						
			//check if the user is logged
			$user_id = $this->utils->get_uid();
			if($user_id === false){
				return false;	
			}//end if
						
			//now we create a new order
			$query = "INSERT INTO orders 
					  (order_uid,order_module_id,order_status,order_cart_id)
					  VALUES
					  (".$user_id.",".$module_id.",'".$status."',".$cart_id.")";						  					  				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->order_id = $this->utils->db->lastInsertId();								
				
				/* META SUPPORT */			
				$this->meta->set_meta_id($this->order_id);	
				$table_fields = $this->utils->get_table_fields(array("module" => $this->module,"field_prefix" => "order", "show_all" => true));			
				array_walk($table_fields,array($this->utils,'clean_prefix'),$table_prefix);		
				$this->meta->auto_add_meta(array("avoid" => $table_fields));
				/* END META SUPPORT */
				
				if((bool)$use_cart == false){
					//now we have an order ID, so we are going to store the post_id in association with this order ID
					//force the post_id to be an array
					if(!is_array($post_id)){
						$post_id = array($post_id);	
					}//end if
					if(sizeof($post_id) > 0){
						for($i = 0; $i < sizeof($post_id); $i++){
							if(isset($prices)){
								$price = $prices[$i];
							} else {
								$price = "0";	
							}//end if
							if(isset($quantities)){
								$quantity = $quantities[$i];
							} else {
								$quantity = 1;	
							}//end if
							
							
							$query = "INSERT INTO orders_posts
									  (order_post_order_id, order_post_post_id, order_post_module_id, order_post_price, order_post_amount)
									  VALUES
									  (".$this->order_id.",".$post_id[$i].",".$module_id.",'".$price."',".$quantity.")";
							$result = $this->utils->db->query($query);
							$errors = $this->utils->error($result,__LINE__,get_class($this));						
						}//end for i									
						return true;
					} else {
						return false;	
					}//end function	
				} else {
					return true;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
			
		public function get_orders(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if		
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
			}//end if
			
			if(!isset($sort_by)){
				$sort_by = 'order_date';	
			}//end if
			
			if(!isset($sort_order)){
				$sort_order = 'DESC';
			}//end if
			
			if(!isset($fields)){
				$fields = "orders.*, user_id, user_firstname, user_surname, user_email, user_username,
			          UNIX_TIMESTAMP(order_date) AS order_date, module_name, module_label";	
			}//end if
			
			$query = "SELECT ".$fields."
					  FROM orders, users, modules
					  WHERE order_temp = 0
					  AND order_module_id = module_id
					  AND order_uid = user_id";
			if(isset($module)){
				$query .= " AND order_module_id = ".$module_id;
			}//end if
			
			if(isset($user_id)){
				$query .= " AND user_id = ".$user_id;	
			}//end if
			
			if(isset($from_date)){
				$query .= " AND DATE(order_date) >= DATE('".$this->utils->convert_to_db_date($from_date)."')";
			}//end if
			
			if(isset($to_date)){
				$query .= " AND DATE(order_date) <= DATE('".$this->utils->convert_to_db_date($to_date)."')";	
			}//end if
			
			$query .= " ORDER BY ".$sort_by." ".$sort_order;
			
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false,true,true);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if
			
			if($limit != NULL){
				$query .= " LIMIT ".$limit;
			}//end if			
						
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return true;
			}//end if
		}//end function
		
		public function get_order_prices(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if		
			
			if(!isset($id)){
				return false;
			}//end if	
			
			$data = array();
			
			$order_posts = $this->get_order_posts(array('id' => $id, 'meta' => true));
			//var_dump($order_posts);		
			if($order_posts !== false){
				$total = 0;
				$data['posts'] = array();
				for($i = 0; $i < sizeof($order_posts); $i++){
					$data['posts'][$i] = array();
					$data['posts'][$i]['post_id'] = $order_posts[$i]['post_id'];
					$data['posts'][$i]['price'] = $order_posts[$i]['order_post_price'];
					/*
					$post_total = 0;					
					$post_categories = $this->get_post_categories(array('post_id' => $order_posts[$i]['post_id'],'meta' => true));							
					if($post_categories !== false){
						$data['posts'][$i]['categories'] = array();
						for($x = 0; $x < sizeof($post_categories); $x++){
							$data['posts'][$i]['categories'][$x] = array();						
							$post_total += $post_categories[$x]['meta_category_price'];
							$data['posts'][$i]['categories'][$x]['category_name'] = $post_categories[$x]['category_name'];
							$data['posts'][$i]['categories'][$x]['price'] = $post_categories[$x]['meta_category_price'];							
						}//end for x
						$total += $post_total;
						$data['posts'][$i]['total'] = $post_total;
					}//end if	
					*/
					$post_total = $order_posts[$i]['order_post_price'];
					$total += $post_total;
				}//end for
				$data['total'] = $total;
				return $data;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_order(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if		
			
			if(!isset($id)){
				return false;
			}//end if			
			
			$query = "SELECT orders.*, user_id, user_firstname, user_surname, user_email, user_username,
			          UNIX_TIMESTAMP(order_date) AS order_date, module_name, module_label
					  FROM (orders, users, modules)
					  WHERE order_id = ".$id."
					  AND module_id = order_module_id
					  AND user_id = order_uid";
			if(isset($user_id)){
				$query .= " AND order_uid = ".$user_id;		
			}//end if
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;	
					}//end if
					if(!isset($raw)){
						$raw = false;	
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type('order');
						$meta_data = $this->meta->get_meta(array("id" => $id, "html" => (bool)$html, "raw" => (bool)$raw));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for	
						$this->meta->set_meta_type('order');				
						$meta_data = $this->meta->get_meta(array("id" => $id,"search" => $meta, "html" => (bool)$html, "raw" => (bool)$raw));				
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					}//end if
					/* END META SUPPORT */	
					return $data;
				} else {
					return false;
				}//end if				
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_order_cart_items(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if		
			
			if(!isset($id)){
				return false;
			}//end if
			
			$query = "SELECT posts.*, carts_items.* 
					  FROM (posts, carts_items, orders)
					  WHERE post_id = cart_item_post_id
					  AND order_id = ".$id."
					  AND order_cart_id = cart_item_cart_id";	
					//  echo $query;				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type("cart_item");
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['cart_item_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$this->meta->set_meta_type("cart_item");
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['cart_item_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					/* ATTACHMENT SUPPORT */
					if(isset($default_photo) && $default_photo === true){
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){							
								$photo = $this->utils->get_attachments(array('post_id' => $data[$i]['post_id'],
																	   'role' => 'default-image', 
																	   'module' => $data[$i]['post_module_id'], 
																	   'limit' => 1, 
																	   'status' => 'published',
																	   'type' => 'image'));															   
								if($photo !== false){
									$data[$i]['photo'] = $photo;
								}//end if
							}//end if
						}//end if
					}//end if 		
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function get_order_posts(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if		
			
			if(!isset($id)){
				return false;
			}//end if
			
			$fields = "post_title, post_id, post_uid, orders_posts.* ";
			if(isset($use_generic_labels)){
				if($use_generic_labels === true){
					$fields = "post_title AS title, post_id AS id, post_uid AS user_id, orders_posts.* ";		
				}//end if
			}//end if
			
			$query = "SELECT ".$fields."
					  FROM (posts, orders_posts)
					  WHERE post_id = order_post_post_id
					  AND order_post_order_id = ".$id."
					  GROUP BY post_id";	
					//  echo $query;				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function confirm_order(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($id) && !isset($module)){
				return false;
			}//end if
			
			$this->order_id = $id;
						
			$user_id = $this->utils->get_uid();
			
			if(!isset($status)){
				$status = 'pending';	
			}
			
			$query = "UPDATE orders SET
					  order_temp = 0";
			if(isset($vat)){
				$query .= ", order_vat_enabled = ".(int)$vat;	
			}//end if
			$query .= " WHERE order_id = ".$this->order_id."
					  AND order_uid = ".$user_id;
					  //echo $query;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				//now we create a unique ID for this order
				$this->create_order_code();
				//and then we change the status of the order
				$this->set_order_status(array('id' => $this->order_id,'status' => $status,'module' => $module));				
				//now that the order is in the system and it's confirmed, we send an email to the admin
				$this->send_order_notification_email();	
				//and we send the invoice to the user
				$this->send_invoice();				
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function send_invoice(){	
			//Message type, it could be html or text
			if(!isset($type)){
				$type = $this->settings['order_email_type'];	
			}//end if
			
			if($this->settings['order_send_invoice'] == 1){
			
				$genSettings = $this->utils->get_settings();
				
				$order_data = $this->get_order(array("id" => $this->order_id));			
				
				$email = $order_data['user_email'];				
						
				//create the mailer obj				
				$mailer = $this->utils->call("mailer");
				$mailer->set_sender($this->settings['order_no_reply']);	
				if(isset($this->settings['bcc_email']) && $this->settings['bcc_email'] != ""){
					$mailer->set_bcc($this->settings['bcc_email']);
				}//end if
				if($type == "html"){
					$mailer->defaultFormat = 'text/html';					
				} else {
					$mailer->defaultFormat = 'text/plain';					
				}//end if
								
				if($this->settings['order_email_subject'] == NULL){
					$subject = "Order notification";			
				} else {
					$subject = $this->settings['order_email_subject'];
					$subject = $this->parse_subject_mapping(array("subject" => $subject, "data" => $order_data));	
				}//end if
						
				if(isset($this->settings['order_email_signature']) && strlen(trim($this->settings['order_email_signature'])) > 0){
					$signature = $this->settings['order_email_signature'];
					if($type == "text"){
						$signature = strip_tags($signature);	
					}					
				}//end if
				
				$options = array("data" => $order_data, "type" => $type);
				if(isset($signature)){
					$options['signature'] = $signature;	
				}//end if
				$message = $this->get_invoice_message($options);
				
				$mailer->set_message(array('message' => $message));
				$mailresult = $mailer->send_mail(array('email' => array($email),'subject' => $subject));
				if($mailresult){					
					return true;
				} else {
					return false;	
				}//end if	
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function get_invoice_message(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
				
			if(!isset($data) && !is_array($data)){
				array_push($this->errors,"No data passed to the notification manager");
				return false;	
			}//end if
		
			if(!isset($type)){
				$type = "html";	
			}//end if
			
			switch($type){
				case "html":
					$genSettings = $this->utils->get_settings();					
					//now we check if we have to apply a template
					if((bool)$genSettings['enableEmailTemplates'] === true){
						//we get the template from the module settings
						$template = $this->settings['defaultModuleEmailTemplate'];
						if($template != ""){							
							//so if we have a template defined, we process the file
							$tpld = $data;//Template Data					
							if(isset($signature)){
								$tpld['signature'] = $signature;	
							}//end if
							$html = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
							if($html !== false){
								$html_message = $html;	
							}//end if
						} else {
							if(isset($signature)){
								$html_message .= $signature;
							}//end if
						}//end if
					} else {
						if(isset($signature)){
							$html_message .= $signature;
						}//end if
					}//end if
					$message = $html_message;
				break;
			}//end switch
			return $message;
		}//end function
		
		protected function send_order_notification_email(){
			//general settings
			$genSettings = $this->utils->get_settings();
			$noReply = $genSettings['noReply'];
			//get module settings
			$settings = $this->settings;
			//get the notification email
			$notification_email = trim($settings['order_notification_email']);				
			if(!is_null($notification_email)){
				//get the order details
				$order_data = $this->get_order(array('id' => $this->order_id));
				if($order_data !== false){				
					$message = 'A new order has just been submitted by ';
					$message .= $order_data['user_firstname'].' '.$order_data['user_surname'];
					
					//create the mailer obj
					$mailer = $this->utils->call("mailer");
					$mailer->set_sender($noReply);	
					$mailer->defaultFormat = 'text/plain';
					$mailer->set_message(array('type' => 'text/plain', 'message' => $message));
					$mailresult = $mailer->send_mail(array('email' => array($notification_email),'subject' => 'New order submitted'));
					if($mailresult){
						return true;
					} else {
						return false;	
					}//end if
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function send_paid_order_notification_email(){
			//general settings
			$genSettings = $this->utils->get_settings();
			$noReply = $genSettings['noReply'];
			//get order details
			$order_data = $this->get_order(array('id' => $this->order_id));
			/*
			if($order_data !== false){
				$padded_id = str_pad($this->order_id, 6, "0", STR_PAD_LEFT);
				$message = "Hi ".$order_data['user_firstname']."\n\rThis email is to confirm that we received the payment for the invoice no. #".$padded_id."\n\rYou'll find invoices in 'payment history' under the 'your account' tab in the top menu of the dashboard.\n\rBest Regards\n\rThe Kinsale Sharks Team";
				
				//create the mailer obj
				$mailer = $this->utils->call("mailer");
				$mailer->set_sender($noReply);	
				$mailer->defaultFormat = 'text/plain';
				$mailer->set_message(array('type' => 'text/plain', 'message' => $message));
				$mailresult = $mailer->send_mail(array('email' => array($order_data['user_email']),'subject' => 'Payment accepted'));
				if($mailresult){
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
			*/
			
		}//end function
		
		public function set_order_amount(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($id) || !isset($amount)){
				return false;
			}//end if
			
			$this->order_id = $id;
	
			$query = "UPDATE orders SET order_amount = '".$amount."' WHERE order_id = ".$id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function set_order_discount(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($id) || !isset($discount)){
				return false;
			}//end if
			
			$this->order_id = $id;
	
			$query = "UPDATE orders SET order_discount = '".$discount."' WHERE order_id = ".$id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function set_order_status(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($id) || !isset($status) || !isset($module)){
				return false;
			}//end if
			
			$this->order_id = $id;
			
			$module_data = $this->utils->get_module($module);
			$module_id = $module_data['module_id'];
			
			$query = "UPDATE orders SET order_status = '".$status."' WHERE order_id = ".$id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				//if we are setting the status as pending we execute this code							
				if($status == 'pending'){
					//get the order posts	
					$posts = $this->get_order_posts(array('id' => $id));
					if($posts != false){
						for($i = 0; $i < sizeof($posts); $i++){
							$this->meta->set_meta_module($module_id);
							$this->meta->add_meta(array('id' => $posts[$i]['post_id'],'label' => 'meta_process_status', 'value' => 'pending-payment'));	
						}//end for i
					}//end if
				}//end if
				//if we are setting the status as paid we execute this code							
				if($status == 'paid'){
					//get the order posts	
					$posts = $this->get_order_posts(array('id' => $id));
					if($posts != false){
						for($i = 0; $i < sizeof($posts); $i++){
							$this->meta->set_meta_module($module_id);
							$this->meta->add_meta(array('id' => $posts[$i]['post_id'],'label' => 'meta_process_status', 'value' => 'paid'));	
						}//end for i
					}//end if
					//we advise the user that his order has been accepted
					$this->send_paid_order_notification_email();
				}//end if
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		public function delete_order(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($id)){
				return false;
			}//end if
			
			$query = "DELETE FROM orders WHERE order_id = ".$id;
			$query = "UPDATE orders SET order_status = '".$status."' WHERE order_id = ".$id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
		
		protected function create_order_code(){
			if(is_numeric($this->order_id)){
				$tools = $this->utils->call('tools');
				$chars = strlen($this->order_id);
				$chars = 8-$chars;
				$this->code = "";
				if($chars > 0){
					for($k = 0; $k < $chars; $k++){
						$this->code .= $tools->get_random_alphanum();
					}//end for
				}//end if
				$this->code .= $this->order_id;
				
				//Update the booking with the code
				try{
					$query = "UPDATE orders SET order_code = :code WHERE order_id = :id";				
					$result = $this->utils->db->prepare($query);
					$result->bindParam(':id', $this->order_id, PDO::PARAM_INT);
					$result->bindParam(':code', $this->code, PDO::PARAM_STR);
					$result->execute();
					$errors = $this->utils->error($result,__LINE__,get_class($this));	
					if($errors === false){
						return true;
					} else {
						throw new PDOException("Cannot save the code for this order");
					}//end if
				} catch(PDOException $ex){
					$this->errors = $ex->getMessage();
					return false;	
				}//end if
			} else {
				return false;	
			}//end function
		}//end function
		
		public function set_order_payment(){
			$params = func_get_args();
			if(is_array($params) && $params != NULL){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if			
			}//end if	
			
			if(!isset($id) || !isset($payment_id)){
				return false;
			}//end if
			
			try{
				$query = "UPDATE orders SET order_payment_id = :payment_id WHERE order_id = :id";
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':id', $id, PDO::PARAM_INT);
				$result->bindParam(':payment_id', $payment_id, PDO::PARAM_INT);
				$result->execute();
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
					return true;
				} else {
					throw new PDOException("Cannot update the order payment ID");
				}//end if
			} catch(PDOException $ex){
				$this->errors = $ex->getMessage();
				return false;	
			}
		}//end function
		
		protected function parse_subject_mapping(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			if(!isset($subject) && !isset($data)){
				return false;	
			}//end if
			$this->subject_mapping = array();	
			$this->subject_mapping['day'] = date("j",$data['post_date']);
			$this->subject_mapping['ordinal'] = date("S",$data['post_date']);
			$this->subject_mapping['month'] = date("F",$data['post_date']);
			$this->subject_mapping['year'] = date("Y",$data['post_date']);
			$this->subject_mapping['title'] = $data["post_title"];
			
			foreach($this->subject_mapping as $key => $value){
				$subject = str_replace('['.$key.']',$value,$subject);
			}//end foreach
			
			return $subject;
		}//end if
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
		
		private function init_settings(){
			$this->settings = $this->utils->get_settings(array('module' => $this->module));						
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>