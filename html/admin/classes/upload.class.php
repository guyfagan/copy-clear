<?php 
	#################################################################
	# Upload Class - 	last update 11/12/12						#
	#					created in 2008								#
	# This is a core class for the Old Hat CMS						#	
	#																#
	# 11/12/12 - Cleaned the code from old unused code of lines		#
	#################################################################
	
	class upload{
	
		public $utils;//utils class
		private $mime;
		private $u_file;
		private $filename;
		private $u_address;
		private $u_add_date;
		private $error_message;
		private $file_organize;
		private $avoid_mime;
		private $is_image;

		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;		
			$this->utils->read_params($this,$params);
			$this->u_add_date = false;
			$this->error_message = "";
			$this->file_organize = true;
			$this->avoid_mime = false;//if set to true, it will use the file extension instead of mime type
		}//endconstructor
		
		//define the file istance to upload
		public function set_file($file){
			$this->u_file = $file;
		}//end function
		
		public function use_file_extension($avoid_mime){
			$this->avoid_mime = $avoid_mime;
		}//end function
		
		//This function declare if you want to add the unix-timestamp date ahead the filename
		public function add_date($u_add_date){
			$this->u_add_date = $u_add_date;
		}//end function
		
		//this function set the destination path
		public function set_destination($u_address,$uploadpath = __UPLOADPATH__){
			$this->u_address = $u_address;
			//Add the parent path			
			$this->u_address = $uploadpath.$this->u_address;						
			//check if the last slash is present, if missing add a slash at the end
			if(substr($this->u_address,(strlen($this->u_address)-1),1) != "/"){
				$this->u_address = $this->u_address."/";
			}//endif
			//clean for double slashes
			$this->u_address = $this->utils->clean_slashes($this->u_address);

			if(!@file_exists($this->u_address)){
				@mkdir($this->u_address);
			}//end if
		}//end function
		
		public function get_destination(){
			return $this->u_address;
		}//end function
		
		//this function set an new name for the uploaded file
		public function set_newName($newname){
			$this->new_filename = $newname;
			if(substr($this->new_filename,-4,1) != "."){
				$mime = $this->get_file_type();
				switch($mime){
					case "image/jpeg":
					case "image/pjpeg":
						$this->new_filename .= ".jpg";
						break;
					case "image/png":
						$this->new_filename .= ".png";
					case "image/gif":
						$this->new_filename .= ".gif";
				}//ends witch
			}//end if
		}//end function
		
		public function set_file_organize($file_organize){
			$this->file_organize = $file_organize;
		}//end functiton
		
		public function get_mime(){
			return $this->mime;
		}//end function
		
		private function get_mime_from_ext($filename){
			$filestring = strtolower($filename);
			$dot = substr($filestring,-5,5);
			if($dot == ".html"){
				return "text/html";
			} else {
				$dot = substr($filestring,-4,4);
				switch($dot){
					case ".jpg":
						return "image/jpeg";
						break;
					case ".gif":
						return "image/gif";
						break;
					case ".png":
						return "image/png";
						break;
					case ".pdf":
						return "application/pdf";
						break;
					case ".xml":
						return "text/xml";
						break;
					case ".css":
						return "text/css";
						break;
					case ".tif":
						return "image/tiff";
						break;
					case ".tif":
						return "video/mpeg";
						break;
					default:
						return "unknown";
				}//end switch
			}//end if
		}//end function
		
		#this function return the filetype of the uploaded file
		public function get_file_type($uploadID = NULL){
			if($uploadID == NULL){
				$file_type = $this->u_file['type'];
			} else {
				$file_type = $this->u_file['type'][$uploadid];
			}//end if

			if($file_type != NULL){
				return $file_type;
			} else {	
				return $this->get_mime_from_ext($file_type);	
			}//end if
		}//end function
		
		//return the error message
		public function get_error(){
			return $this->error_message;
		}//end function
		
		private function organize_path(){
			if($this->file_organize){	
				if($this->avoid_mime == false){				
					$this->is_image = $this->utils->is_img($this->mime);
				} else {
					$this->mime = $this->get_mime_from_ext($this->filename);
					$this->is_image = $this->utils->is_img($this->mime);
				}//end if		
				
				if($this->is_image){				
					$new_u_address = $this->u_address."photos/o/";				
				} else {
					$new_u_address = $this->u_address."files/";
				}//end if
				
				//Check if the path exist, if no it will create a new one
				if(!file_exists($new_u_address)){
					if($this->is_image){	
						mkdir($this->u_address."photos/",0777);
						mkdir($this->u_address."photos/o/",0777);
					} else {
						mkdir($new_u_address,0777);
					}//end if
				}//end if
				$this->u_address = $new_u_address;
			}//end if
		}//end function
		
		//execute the upload
		public function do_upload($uploadID = NULL){		
			//check if the upload is multiple or not
			if(is_array($this->u_file['name'])){
				$o_filename = $this->u_file['name'][$uploadID];
				$tmp_filename = $this->u_file['tmp_name'][$uploadID];
				$this->mime = $this->u_file['type'][$uploadID];
			} else {
				$o_filename = $this->u_file['name'];
				$tmp_filename = $this->u_file['tmp_name'];	
				$this->mime = $this->u_file['type'];		
			}//end if
			
			//check if the file is a php script, if yes, reject this file
			if(strpos($o_filename,".php") !== false){
				$this->error_message = "this file is not accepted";
				return false;
			}//end if
			
			//Check if the filename is empty
			if(trim($o_filename) == NULL){
				$this->error_message = __FILE_NODATA__;
				return false;
			}//end if
			
			//check if we have to change the filename
			if($this->new_filename != NULL){
				$this->filename = $this->new_filename;
			} else {
				$this->filename = $o_filename ;
			}//end if
			
			$this->filename = $this->utils->unifystring($this->filename);
			$this->filename = $this->utils->remove_slashes($this->filename);
			$this->filename = $this->force_ext($this->filename,$this->mime);
			/*
			if(!$this->utils->is_file_allowed($this->filename)){
				$this->error_message = __FILE_UPLOAD_FAILED__;
				return false;
			}//end if		
			*/	
			if($this->u_add_date == true){
				$date = time();
				$this->filename = $date."_".$this->filename;
			}//end if
			
			//Reorganize path
			$this->organize_path();
	
			//check if there are some non valid chars
			$this->filename = $this->utils->unifystring($this->filename);		
			//check if the directory is writable
			if(@is_writable($this->u_address)){
				if(@is_uploaded_file($tmp_filename)){				
					if(@move_uploaded_file($tmp_filename,$this->u_address.$this->filename)){			
						if(!@chmod($this->u_address.$this->filename,0755)){						
							$this->error_message = __FILE_CANT_CHMOD__;
						}//end if						
						return true;
					} else {					
						$this->error_message = __FILE_SAVE_UPLOAD_FAILED__;
						return false;
					}//end if					
				} else {				
					$this->error_message = __FILE_UPLOAD_FAILED__;
					return false;
				}//end if
			} else {				
				$this->error_message = __FILE_FOLDER_UNWRITABLE__;
				return false;
			}//end if			
		}//end function
		
		public function force_ext($filename,$mime){
			$fileext = substr($filename,-4);
			$fileext = strtolower($fileext);
			if($fileext != ".jpg" && $fileext != ".gif" && $fileext != ".png"){
				switch($mime){
					case "image/jpg":
					case "image/jpeg":
					case "image/pjpeg":
						$filename .= ".jpg";
						break;
					case "image/gif":
						$filename .= ".gif";
						break;
					case "image/png":
						$filename .= ".png";
						break;
					default:
						return $filename;
				}//endswitch
			}//end if
			return $filename;
		}//end function
		
		public function get_filename(){
			return $this->filename;
		}//end function
	}//end class
?>