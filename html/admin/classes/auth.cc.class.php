<?php
	require_once("auth.class.php");

	class auth_cc extends auth{

    //Constructor
		function __construct($utils){
			if(class_exists("auth")){
				parent::__construct($utils);
				$this->init_settings();
			}//end if
		}//endconstructor

		public function set_stay_logged($is_logged){
			if($is_logged){
				setcookie("persistent_login","1",$this->cookie_expiry,"/");
			} else {
				setcookie("persistent_login","",time()-3600,"/");
			}
		}// end function

		public function autologin(){
			if($_COOKIE['persistent_login'] && !is_null($_COOKIE['uid']) && !is_null($_COOKIE['secure_id'])){
				header('Location: '.__BASEPATH__.'/home/');
			}
		}

		public function user_agency_is_active(array $options = array()){
  		extract($options);

  		if(!isset($user_id) && isset($this->uid)){
    		$user_id = $this->uid;
  		}//end if

  		$query = "SELECT companies.*
  		          FROM companies, users
  		          WHERE user_company_id = company_id
  		          AND user_id = :user_id";
  		$result = $this->utils->db->prepare($query);
			$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
  			$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					if($data['company_status'] === 'active'){
  					return true;
  				} else {
    				return false;
  				}
				} else {
					return false;
				}//end if
			} else {
  			return false;
			}
		}//end function
  }
