<?php
	require_once('ewrite.class.php');

	class blog extends ewrite{
	
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);	
			$this->utils->read_params($this,$params);			
			$this->set_module("blog");
		}//endconstructor
		
		public function get_posts(){			
			$params = func_get_args();
			if(!is_array($params[0])){
				$cid = $params[0];				
				$sort_by = $params[1];
				
				$sort_order = $params[2];
				
				$status = $params[3];
				$deleted = $params[4];
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($module)){
				$module_data = $this->utils->get_module($module);
				$module_id = $module_data['module_id'];
				$module_name = $module_data['module_name'];
			} else {
				$module_id = $this->module_id;
				$module_name = $this->module;	
			}//end if
			
			//backward compatibility
			if(isset($userid)){
				$uid = $userid;	
			}//end if
			
			if(isset($category_id)){
				$cid = $category_id;	
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "post_date";
			}//end if
			
			if($sort_order == NULL){					
				$this->settings = $this->utils->get_settings(array('module_id' => $this->module_id));				
				$sort_order = $this->settings['list_order'];
			}//end if						
		
			$query = "SELECT posts.*, users.user_username, users.user_id, UNIX_TIMESTAMP(posts.post_date) AS post_date, module_name AS folder";			
			if($cid != NULL){
				$query .= ", categories.*";
			}//end if
			if(isset($tag)){
				$query .= ", tag_tag";	
			}//end if
			$query .= " FROM (posts, modules, modules_items_order ";
			if(isset($tag)){
				$query .= ", tags";	
			}//end if
			$query .= ")
					  LEFT JOIN users
					  ON (user_id = post_uid) ";
			if($cid != NULL){
				/*$query .= "LEFT JOIN modules_items_order
					  ON (module_item_order_item_id = post_id AND module_item_order_type = 'post')
					  LEFT JOIN categories
					  ON (category_id = module_item_order_father_id) ";
					  */
				$query .= "LEFT JOIN categories
					  ON (category_id = module_item_order_father_id) ";
			}//end if
			$query .= "WHERE post_module_id = module_name
					   AND post_temp = 0";
			if(!isset($all_modules)){
				if($cid != NULL){
					$query .= " AND category_module_id = ".$module_id."";
				} else {
					$query .= " AND post_module_id = '".$module_name."'";		
				}//end if	
			}//end if  
					
			$query .= " AND module_item_order_type = 'post'
						AND module_item_order_item_id = post_id
						AND module_item_order_module_id = module_id";
			
			if(isset($tag)){
				$query .= " AND tag_post_id = post_id
							AND tag_module_id = module_id
							AND tag_tag = ".$this->utils->db->quote($tag);	
			}//end if
			
			if($month != NULL && is_numeric($month)){
				$query .= " AND MONTH(post_date) = ".$month;
			}//end if
			
			if($year != NULL && is_numeric($year)){
				$query .= " AND YEAR(post_date) = ".$year;
			}//end if
			
			if($day != NULL && is_numeric($day)){
				$query .= " AND DAY(post_date) = ".$day;
			}//end if
			
			if($uid != NULL && isset($uid)){
				$query .= " AND user_id = ".$uid;
			}//end if
			
			if($this->search_key != NULL){
				$sql = $this->process_search();
				if($sql != false){
					$query .= $sql;					
				}//endif
			}//end if		
			//check the status of the posts
			if(isset($status) && (is_bool($status) || is_numeric($status))){
				$query .= " AND post_status = ".(int)$status;
			}//end if	
			if(isset($temp) && (is_numeric($temp) || is_bool($temp))){
				$query .= " AND post_temp = ".(int)$temp;
			}//end if	
			/*if($cid != NULL && $status != NULL && is_numeric($status)){
				$query .= " AND category_status = ".$status;
			}//end if*/
			//check if the posts is deleted or not	
			if(isset($deleted) && (is_numeric($deleted) || is_bool($deleted))){
				$query .= " WHERE post_deleted = ".(int)$deleted;
			}//end if
			
			if($cid != NULL){
				$query .= " AND (category_id = ".$cid;
				if(isset($show_subelements) && $show_subelements == true){
					$query .= " OR category_father_id = ".$cid;
				}//end if
				$query .= ")";
			} else {
				if(isset($show_subelements) && $show_subelements == false){
					$query .= " AND module_item_order_father_id = 0";
				}//end if
			}//end if
			
			if($use_system_order == NULL || $use_system_order == false){
				$query .= " GROUP BY posts.post_id						
						    ORDER BY ".$sort_by." ".$sort_order;			
			} else {
				$query .= " GROUP BY posts.post_id						
					    	ORDER BY module_item_order_order ".$sort_order;	
			}	
			if($sort_by !== "post_id"){
				$query .= ", post_id DESC";	 //if we sort by date and we have the same date, then we subsort by post_id			
			}//end if
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if	
			
			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if		
				
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors == false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);	
					if(isset($show_more) && $show_more === true){
						for($i = 0; $i < sizeof($data); $i++){
							$data[$i]['post_message'] = $this->truncate_to_more(array(
								"text" => $data[$i]['post_message'],
								"url" => __BASEPATH__.$data[$i]['post_permalink'].'.html'
							));
						}//end for i
					}//end if
					
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'], "html" => (bool)$html));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['post_id'],"search" => $meta, "html" => (bool)$html));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					
					/* ATTACHMENT SUPPORT */
					if(isset($default_photo) && $default_photo === true){
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$photo = $this->utils->get_attachments(array('post_id' => $data[$i]['post_id'],
																	   'role' => 'default-image', 
																	   'module_id' => $this->module_id, 
																	   'limit' => 1, 
																	   'status' => 'published',
																	   'type' => 'image'));															   
								if($photo !== false){
									$data[$i]['photo'] = $photo;
								}//end if
							}//end if
						}//end if
					}//end if 
					
					/* AUTHOR SUPPORT */
					if(isset($author) && $author == true){
						if($data != false){
							$users = $this->utils->call("users");
							for($i = 0; $i < sizeof($data); $i++){
								$author_id = $data[$i]['post_uid'];								
								$author_data = $users->get_user(array("id" => $author_id));
								if($author_data !== false){
									$data[$i]['author'] = $author_data;
								}//end if
							}//end for i
						}//end if
					}//end if
					
					/* TAGS SUPPORT */
					if(isset($tags) && $tags == true){
						if($data != false){
							$tools = $this->utils->call("tools");
							for($i = 0; $i < sizeof($data); $i++){
								$vars = array();
								$vars['post_id'] = $data[$i]['post_id'];
								$vars['module_id'] = $this->module_id;								
								$tags_list = $tools->get_tags($vars);						
								if($tags_list !== false){
									$data[$i]['tags'] = $tags_list;
								}//end if
							}//end for i
						}//end if
					}//end if			
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		//Return the data of the id post passed
		public function get_post(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$pid = $params[0];
				$this->pid = $pid;	
			} else {
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($id)){
				$pid = $id;	
			}//end if
			
			$query = "SELECT posts.*, users.user_username, users.user_email, UNIX_TIMESTAMP(post_date) AS post_date, module_name AS folder,
					  UNIX_TIMESTAMP(post_last_update) AS post_last_update
					  FROM (posts, modules)
					  LEFT JOIN users
					  ON users.user_id = posts.post_uid
					  WHERE module_name = post_module_id ";	
			if(isset($pid)){
				if(is_numeric($pid)){
					$query .= " AND post_id = ".$pid;
				} else {
					return false;	
				}//end if
			}//end if
			
			if(isset($permalink)){				
				$query .= " AND post_permalink = '".$permalink."'";
			}//end if
			
			$query .= " LIMIT 1";
			
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					//Read the post categories
					////This may fix a problem, in case, take it out
					$this->module = $data['folder'];
					$this->pid = $data['post_id'];				
					$post_cats = $this->get_post_categories($this->pid);	
					if($post_cats !== false){
						$cids = array();
						for($i = 0; $i < sizeof($post_cats); $i++){
							array_push($cids,$post_cats[$i]['category_id']);
						}//end for
						$data['category_id'] = base64_encode(serialize($cids));
					} else {
						$data['category_id'] = NULL;			
					}//end if
						
					/* META SUPPORT */	
					if(!isset($html)){
						$html = false;	
					}//end if
					if(!isset($raw)){
						$raw = false;	
					}//end if
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$meta_data = $this->meta->get_meta(array("id" => $this->pid, "html" => (bool)$html, "raw" => (bool)$raw));
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for					
						$meta_data = $this->meta->get_meta(array("id" => $this->pid,"search" => $meta, "html" => (bool)$html, "raw" => (bool)$raw));				
						if($meta_data != false){
							$data = array_merge($data,$meta_data);
						}//end if
					}//end if
					/* END META SUPPORT */	
					
					/* ATTACHMENT SUPPORT */
					if(isset($default_photo) && $default_photo === true){
						if($data != false){	
							$vars = array('post_id' => $data['post_id'],
																   'role' => 'default-image', 
																   'module_id' => $this->module_id, 
																   'limit' => 1, 
																   'status' => 'published',
																   'type' => 'image');																  
							$photo = $this->utils->get_attachments($vars);															   
							if($photo !== false){
								$data['photo'] = $photo;
							}//end if
						}//end if
					}//end if 
					
					/* AUTHOR SUPPORT */
					if(isset($author) && $author == true){
						if($data != false){
							$author_id = $data['post_uid'];
							$users = $this->utils->call("users");
							$author_data = $users->get_user(array("id" => $author_id));
							if($author_data !== false){
								$data['author'] = $author_data;
							}//end if
						}//end if
					}//end if
					
					/* TAGS SUPPORT */
					if(isset($tags) && $tags == true){
						if($data != false){
							$vars = array();
							$vars['post_id'] = $data['post_id'];
							$vars['module_id'] = $this->module_id;
							$tools = $this->utils->call("tools");
							$tags_list = $tools->get_tags($vars);						
							if($tags_list !== false){
								$data['tags'] = $tags_list;
							}//end if
						}//end if
					}//end if			
					return $data;
				} else {
					return false;
				}//endif
			} else {
				return false;
			}//end if
		}//end function
		
		public function truncate_to_more(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($text) || !isset($url)){
				return false;	
			}//end if
			
			if(!isset($more_html)){
				$more_html = '<a href="'.$url.'" class="read-more">Read More</a>';
			}//end if
			
			$tag = '<hr />';
			$tag_position = strpos($text,$tag);
			if($tag_position !== false){
				$text = substr($text,0,$tag_position);
				$text .= $more_html;
				return $text;
			} else {
				return $text;	
			}//end if
		}//end function
	}//end class
?>