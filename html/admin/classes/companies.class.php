<?php
	require_once('site.class.php');

	class companies extends site{

		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->utils->read_params($this,$params);
			$this->set_module("companies");
			//Meta init
			$this->meta = $this->utils->call("meta");
			$this->meta->set_meta_module($this->module);
		}//endconstructor

		public function get_companies(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($fields)){
				$fields = "*";
			}//end if

			$query = "SELECT ".$fields." FROM companies, companies_types
					  WHERE companies.company_type_id = companies_types.company_type_id
					  AND company_temp = 0";

			if(isset($status)){
				$query .= " AND company_status = '".$status."'";
			}

			if(isset($search)){
				$search = str_replace(" ","%",$search);
				$query .= " AND company_name LIKE '%".$search."%'";
			}//end if

			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND ".$this->prefix."_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND ".$this->prefix."_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if

			$query .= " ORDER BY company_name ASC";

			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			if($limit_result != NULL){
				$query .= " LIMIT ".$limit_result;
			}//end if

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_company(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			$query = "SELECT * FROM companies, companies_types
					  WHERE companies.company_type_id = companies_types.company_type_id
					  AND company_id = ".$id;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_company_users(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $pkey => $value){
					${$pkey} = $value;
				}//end if
			}//end if

			if($sort_by == NULL){
				$sort_by = "user_username";
			}//end if
			if($sort_order == NULL){
				$sort_order = "ASC";
			}//end if


			//query
			$query = "SELECT users.*, users_group_name, users_group_id, UNIX_TIMESTAMP(ad_submission_date) AS ad_submission_date
    					  FROM (users)
    					  LEFT JOIN ads_submissions
    					  ON ad_submission_uid = user_id
    					  LEFT JOIN users_groups_rel
    					  ON users_groups_rel.users_groups_rel_uid = users.user_id
    					  LEFT JOIN users_groups
    					  ON users_groups_rel.users_groups_rel_gid = users_groups.users_group_id
    					  WHERE user_temp = 0";
			if(isset($active) && !is_null($active)){
				$query .= " AND user_active = ".(int)$active;
			}//end if
			if(!is_null($group_id)){
				if(is_array($group_id)){
					$query .= " AND users_groups_rel_gid IN (".implode(",",$group_id).")";
				} else {
					$query .= " AND users_groups_rel_gid = ".$group_id;
				}//end if
			}//end if

			if(isset($company_id)){
  			$query .= " AND user_company_id = ".$company_id;
			}// end if

			if(isset($attributes)){
				if(is_array($attributes)){
					foreach($attributes as $key => $value){
						if(is_array($value)){
							$query .= " AND user_".$key." IN ('".implode("', '",$value)."')";
						} else {
							$query .= " AND user_".$key." = '".$value."'";
						}//end if
					}//end foreach
				}//end if
			}//end if

			if(isset($search) && !is_null($search)){
				$search = trim($search);
				$search = str_replace(" ","%",$search);
				$query .= " AND (user_username LIKE '%".$search."%' OR user_email LIKE '%".$search."%' OR user_firstname LIKE '%".$search."%' OR user_surname LIKE '%".$search."%')";
			}//end if
			$query .= " GROUP BY user_id";
			$query .= " ORDER BY users.".$sort_by." ".$sort_order.", ad_submission_date DESC";
			//echo $query;
			//This is for the paging
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;
			}//end if

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){

				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					/* META SUPPORT */
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type('user');
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['user_id']));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$this->meta->set_meta_type('user');
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['user_id'],"search" => $meta));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if
					/* END META SUPPORT */
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function


		public function reactivate_company(){
  		$users = $this->utils->call('users');
  		$user = $users->get_user();
  		if($user !== false){
        $company_id = $user['user_company_id'];
        $query = "UPDATE companies SET company_reactivated_by = :user_id, company_status = 'active' WHERE company_id = :company_id";
        $result = $this->utils->db->prepare($query);
  			$result->bindParam(':user_id', $user['user_id'], PDO::PARAM_INT);
  			$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);
  			$result->execute();
  			$errors = $this->utils->error($result,__LINE__,get_class($this));
  			if($errors === false){
          return $result;
        } else {
          return false;
        }// end if
  		} else {
    		return false;
  		}// end if
		}// end function

		public function update_company_status(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($company_id) || !isset($status)){
				return false;
			}//end if

			$query = "UPDATE companies SET company_status = '".$status."', company_temp = 0 WHERE company_id = ".$company_id;
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if

		}//end function

		public function get_companies_types(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			$query = "SELECT * FROM companies_types";

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function


		public function get_companies_type(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}

			$query = "SELECT * FROM companies_types WHERE company_type_id = ".$id;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function mark_brand_details_as_updated(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($company_id)){
				return false;
			}//end if
			$query = "UPDATE companies SET company_brands_associated = 1 WHERE company_id = :company_id";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
	}//end class
?>
