<?php
	require_once('users.class.php');	

	class registrations extends users{
	
		public $utils;//utils class
		public $errors;
		public $uid;
		protected $meta;
		public $settings = array();
		public $public_data = array();
		public $is_brand_owner = false;
		protected $default_users_group;
		
		//Constructor
		function __construct($utils,$params = array()){
			parent::__construct($utils,$params);
			$this->utils = $utils;
			$this->module = "registrations";
			$this->default_users_group = 4;//Members
			$this->errors = array();
			//Meta init	
			//$this->meta = $this->utils->call("meta");	
			//Set basic settings
			$this->init_settings();
		}//end function
		
		public function set_module($module){
			$module_data = $this->utils->get_module($module);
			if($module_data !== false){
				$this->module = $module_data['module_name'];
				$this->module_id = $module_data['module_id'];
				$this->table = $module_data['module_table'];	
				if(isset($this->meta) && is_object($this->meta)){
					$this->meta->set_meta_module($this->module);
				}//end if
			}//end if
		}//end function
		
		public function get_users(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$group_id = $params[0];				
				$sort_by = $params[1];				
				$sort_order = $params[2];				
				$active = $params[3];		
			} else {
				foreach($params[0] as $pkey => $value){
					${$pkey} = $value;
				}//end if
			}//end if
			
			if($sort_by == NULL){
				$sort_by = "user_username";
			}//end if
			if($sort_order == NULL){
				$sort_order = "ASC";
			}//end if			
			
			//query
			$query = "SELECT users.*, users_group_name, users_group_id, company_name
					  FROM (users, companies)
					  LEFT JOIN users_groups_rel 
					  ON users_groups_rel.users_groups_rel_uid = users.user_id
					  LEFT JOIN users_groups
					  ON users_groups_rel.users_groups_rel_gid = users_groups.users_group_id
					  WHERE user_temp = 0
					  AND company_id = user_company_id";
			if(isset($active) && !is_null($active)){
				$query .= " AND user_active = ".(int)$active;
			}//end if
			if(!is_null($group_id)){
				if(is_array($group_id)){
					$query .= " AND users_groups_rel_gid IN (".implode(",",$group_id).")";
				} else {
					$query .= " AND users_groups_rel_gid = ".$group_id;
				}//end if
			}//end if
			
			if(isset($search) && !is_null($search)){
				$search = trim($search);
				$search = str_replace(" ","%",$search);
				$query .= " AND (user_username LIKE '%".$search."%' OR user_email LIKE '%".$search."%' OR user_firstname LIKE '%".$search."%' OR user_surname LIKE '%".$search."%')";
			}//end if
			$query .= " ORDER BY users.".$sort_by." ".$sort_order;
			//echo $query;
			//This is for the paging			
			if($this->utils->paging_isset()){
				$this->utils->set_unpaged_query($query,false);				
				$query .= " LIMIT ".$this->utils->p_start.", ".$this->utils->p_limit;				
			}//end if	
			
			$result = $this->utils->db->query($query);			
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				
				$num = $result->rowCount();
				if($num > 0){	
					$data = $this->utils->get_result_array($result,false);	
					/* META SUPPORT */	
					//if we pass the value meta as a boolean, we get all the meta into the final data array
					if(isset($meta) && !is_array($meta) && (bool)$meta == true){
						$this->meta->set_meta_type('user');	
						//if it has  been asked to add the meta, we loop through the $data array and we add the meta to each element
						if($data != false){
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['user_id']));
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					} else if(isset($meta) && is_array($meta)){
						//if it's an array, we just get the meta we asked for
						$this->meta->set_meta_type('user');	
						if($data != false){							
							for($i = 0; $i < sizeof($data); $i++){
								$meta_data = $this->meta->get_meta(array("id" => $data[$i]['user_id'],"search" => $meta));								
								if($meta_data != false){
									$data[$i] = array_merge($data[$i],$meta_data);
								}//end if
							}//end for i
						}//end if
					}//end if	
					/* END META SUPPORT */	
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function create_company(){
			$company_name = $_POST['company_name'];
			if($company_name == "" || is_null($company_name)){
				return false;
			}//end id
			
			$company_type = $_POST['company_type'];
			if($company_type == "" || is_null($company_type)){
				return false;
			}//end id
			
			$brand_owner = "0";
			if($company_type == 6){
				$brand_owner = 1;	
			}//end if
			
			$contact_name = $_POST['firstname'].' '.$_POST['lastname'];
			
			$this->public_data['company_name'] = $company_name;
			
			$query = "INSERT INTO companies
					  (company_name, company_type_id, company_contact_name, company_brand_owner, company_brands_associated)
					  VALUES
					  (:company_name, :company_type, :contact_name, :brand_owner,1)";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':company_name', $company_name, PDO::PARAM_STR);
			$result->bindParam(':company_type', $company_type, PDO::PARAM_INT);
			$result->bindParam(':contact_name', $contact_name, PDO::PARAM_STR);
			$result->bindParam(':brand_owner', $brand_owner, PDO::PARAM_INT);
			$result->execute();						
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){					
				$company_id = $this->utils->db->lastInsertId();					
				return $company_id;
			} else {
				return false;
			}//end if
		}//end function
		
		public function register_user(){	
						
			//Check if the email is already used
			if($this->email_exists($_POST['email']) == true){		
				array_push($this->errors,"Email address already in use");	
				return false;
			}//end if	
			
			//Check if the password are identical
			if($_POST['password'] != $_POST['conf_password']){
				array_push($this->errors,"The passwords do not match");
				return false;
			}//end if	
			
			$post = array();
			$email = $_POST['email'];
			$firstname = $_POST['firstname'];
			$surname = $_POST['lastname'];
			$password = sha1($_POST['password']);		
			$active = 0;
			$temp = 1;
			$username = $_POST['email'];
			
			try{
				$this->utils->db->beginTransaction();
				$new_company = false;
				//now we check if the user selected a company
				if(!is_null($_POST['company_id']) && is_numeric($_POST['company_id'])){
					$company_id = $_POST['company_id'];
				} else {
					$company_id = $this->create_company();
					$new_company = true;
					if($company_id === false){
						throw new PDOException("Cannot save the company");	
					}//end if
				}//end if
				
				$this->public_data['company_id'] = $company_id;
				
				$query = "INSERT INTO users 
						  (user_username, user_password, user_email, user_firstname, user_surname, user_active, user_temp, user_company_id)
						  VALUES
						  (:username, :password, :email, :firstname, :surname, :active, :temp, :company_id)";		
				$result = $this->utils->db->prepare($query);
				$result->bindParam(':username', $username, PDO::PARAM_STR);
				$result->bindParam(':password', $password, PDO::PARAM_STR);
				$result->bindParam(':email', $email, PDO::PARAM_STR);
				$result->bindParam(':firstname', $firstname, PDO::PARAM_STR);	
				$result->bindParam(':surname', $surname, PDO::PARAM_STR);			
				$result->bindParam(':active', $active, PDO::PARAM_INT);
				$result->bindParam(':temp', $temp, PDO::PARAM_INT);	
				$result->bindParam(':company_id', $company_id, PDO::PARAM_INT);		
				$result->execute();						
				$errors = $this->utils->error($result,__LINE__,get_class($this));
				if($errors === false){
				
					$this->uid = $this->utils->db->lastInsertId();	
					
					//check if we need to create a new brand
					if($new_company){
						$brand_added = $this->intercept_new_brand_owner(array('company_id' => $company_id));
						if($brand_added){
							$this->is_brand_owner = true;	
						}//end if
					}//end if
				
					//now create the Activation code					
					$activation_code = sha1($this->uid.":".$email);					
					$ac_options = array(
						'uid' => $this->uid,
						'email' => $email,
						'activation_code' => $activation_code
					);
					//$this->send_activation_request($ac_options);					
							
					//Create relation between groups and user			
					$this->create_user_group_relation($this->uid,$this->default_users_group);				
					
					/* META SUPPORT */	
					$this->meta->set_meta_type('user');				
					$this->meta->set_meta_id($this->uid);	
					$table_fields = $this->utils->get_table_fields(array("module" => $this->module,"field_prefix" => "user", "show_all" => true));			
					array_walk($table_fields,array($this->utils,'clean_prefix'),$table_prefix);		
					$this->meta->auto_add_meta(array("avoid" => $table_fields));		
					/* END META SUPPORT */
					$this->utils->db->commit();
					return true;
				} else {
					throw new PDOException("Cannot save the user");				
				}//end if
			} catch(PDOException $ex){
				$this->utils->db->rollBack();
				array_push($this->errors, $ex->getMessage());
				return false;	
			}//end try
		}//end function
		
		public function intercept_new_brand_owner(){
			//read back the info of the company
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($company_id) || !isset($this->uid)){
				return false;	
			}//end if
			
			$brands = $this->utils->call('brands');
			$companies = $this->utils->call('companies');
			
			$company_data = $companies->get_company(array('id' => $company_id));
			if($company_data !== false){
				if($company_data['company_brand_owner'] == 1){
					//it's a brand owner, so we create a brand for the current user
					$brand_options = array('active' => '1','temp' => 0,'user_id' => $this->uid, 'company_id' => $company_id, 'name' => $company_data['company_name']);
					$result = $brands->save_brand_owner($brand_options);
					return $result;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		public function complete_registration(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($user_id)){
				return false;	
			}//end if
			
			$query = "UPDATE users SET user_temp = 0 WHERE user_id = :user_id";
			$result = $this->utils->db->prepare($query);
			$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);		
			$result->execute();						
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function notify_new_registration(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($user_id)){
				return false;	
			}//end if
			
			//general settings
			$genSettings = $this->utils->get_settings();
			$settings = $this->utils->get_settings(array('module' => $this->module));
			$noReply = $genSettings['noReply'];
			
			$type = $genSettings['defaultFormat'];
							
			$user_data = $this->get_user(array('id' => $user_id));	
			if($user_data !== false){					
				$company_id = $user_data['user_company_id'];	
				$companies = $this->utils->call('companies');
				$company_data = $companies->get_company(array('id' => $company_id));
			
				$tpld = array();
				$tpld['user_firstname'] = $user_data['user_firstname'];
				$tpld['user_surname'] = $user_data['user_surname'];
				$tpld['company_name'] = $company_data['company_name'];
						
				if($settings['notifyAdminsNewRegistrations'] == 1){
					//we send an email to the admin to let them know a new user just registered
					$notification_email = explode(',',$settings['notifyAdminsNewRegistrationsEmail']);				
					$subject = $settings['notifyAdminsEmailSubject'];
					$message = $settings['notifyAdminsEmailMessage'];
					$message = $this->utils->parse_text(array('data' => $user_data,'text' => $message));
					$tpld['message'] = $message;
					//create the mailer obj
					$mailer = $this->utils->call("mailer");					
										
					if($settings['notifyAdminEmailTemplate'] != "" && $type == "text/html"){					
						$template = $settings['notifyAdminEmailTemplate'];						
						$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));					
					} else {						
						$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));;						
					}//end if
					
					$mailer->set_sender($noReply);	
					$mailer->defaultFormat = 'text/html';
					$mailer->set_message(array('type' => 'text/html', 'message' => $message));				
					$mailresult = $mailer->send_mail(array('email' => $notification_email,'subject' => $subject));
				}//end if
				
				
				if($settings['notifyUserRegistrations'] == 1){
					//we send an email to the admin to let them know a new user just registered								
					$subject = $settings['notifyUserEmailSubject'];
					$message = $settings['notifyUserEmailMessage'];
					$message = $this->utils->parse_text(array('data' => $user_data,'text' => $message));
					$tpld['message'] = $message;
					//create the mailer obj
					$mailer = $this->utils->call("mailer");
					
					if($settings['notifyUserEmailTemplate'] != "" && $type == "text/html"){		
						$template = $settings['notifyUserEmailTemplate'];						
						$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
					} else {
						$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));;						
					}//end if
					
					$mailer->set_sender($noReply);	
					$mailer->defaultFormat = 'text/html';
					$mailer->set_message(array('type' => 'text/html', 'message' => $message));
					$mailresult = $mailer->send_mail(array('email' => $user_data['user_email'],'subject' => $subject));		
				}//end if			
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		public function update_account_status(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($user_id) || !isset($active)){
				return false;	
			}//end if
			
			$active = (int)$active;				
			
			$query = "UPDATE users SET user_active = ".$active." WHERE user_id = ".$user_id;		
			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				if((int)$active == 1){
					$this->activate_company(array('user_id' => $user_id));
					$this->notify_account_activation(array('user_id' => $user_id));
				}//end if
				return true;
			} else {
				return false;	
			}//end if						
		}//end function
		
		public function activate_company(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
		
			if(!isset($user_id)){
				return false;	
			}//end if
			
			$companies = $this->utils->call('companies');	
			$user_data = $this->get_user(array('id' => $user_id));
			if($user_data !== false){
				$company_id = $user_data['user_company_id'];
				$companies->update_company_status(array('company_id' => $company_id,'status' => 'active'));
				return true;
			} else {
				return false;	
			}//end if
			
		}//end function
		
		public function notify_account_activation(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
		
			if(!isset($user_id)){
				return false;	
			}//end if
						
			//general settings
			$genSettings = $this->utils->get_settings();
			$settings = $this->utils->get_settings(array('module' => $this->module));
			$noReply = $genSettings['noReply'];
			
			$type = $genSettings['defaultFormat'];
						
			$user_data = $this->get_user(array('id' => $user_id));	
			if($user_data !== false){					
		
				$company_id = $user_data['user_company_id'];	
				$companies = $this->utils->call('companies');
				$company_data = $companies->get_company(array('id' => $company_id));
			
				$tpld = array();
				$tpld['user_firstname'] = $user_data['user_firstname'];
				$tpld['user_surname'] = $user_data['user_surname'];
				$tpld['company_name'] = $company_data['company_name'];				
				
				
				if($settings['notifyUserActivation'] == 1){
					//we send an email to the admin to let them know a new user just registered								
					$subject = $settings['notifyActivationEmailSubject'];
					$message = $settings['notifyActivationEmailMessage'];
					$tpld['message'] = $this->utils->parse_text(array('data' => $tpld,'text' => $message));
					//create the mailer obj
					$mailer = $this->utils->call("mailer");
					
					if($settings['notifyActivationEmailTemplate'] != "" && $type == "text/html"){		
						$template = $settings['notifyActivationEmailTemplate'];						
						$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
					} else {
						$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));;						
					}//end if
					
					$mailer->set_sender($noReply);	
					$mailer->defaultFormat = 'text/html';
					$mailer->set_message(array('type' => 'text/html', 'message' => $message));
					$mailresult = $mailer->send_mail(array('email' => $user_data['user_email'],'subject' => $subject));		
				}//end if			
				return true;
			} else {
				return false;	
			}//end if
		}//end function
		
		#################################################################
		# FORGOT PASSWORD PART											#		
		#################################################################	
		
		public function sent_reset_email(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
		
			if(!isset($email)){
				return false;	
			}//end if
			
			$email = trim($email);
			
			//first we check if the email exists
			$exists = $this->email_exists($email);
			if(!$exists){
				array_push($this->errors,"The passed email does not exists");
				return false;					
			}//end if
			
			//now we know that the email is in the database, so we get the details for that email
			$query = "SELECT * FROM users WHERE user_email = '".$email."' AND user_active = 1 AND user_temp = 0 LIMIT 1";
			$result = $this->utils->db->query($query);			
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					if($data !== false){
						//now we create the activation code	
						$code = sha1(__ENCRYPTION_PASSKEY__.':'.$email.':'.sha1($data['user_id']));
						$this->submit_reset_password_email(array('email' => $email, 'code' => $code, 'data' => $data));
						return true;
					} else {
						array_push($this->errors,"The passed email does not exists or the user is inactive");
						return false;	
					}//end if
				} else {
					array_push($this->errors,"The passed email does not exists or the user is inactive");
					return false;
				}//end if
			} else {
				array_push($this->errors,"The passed email does not exists or the user is inactive");
				return false;	
			}//end if						
		}//end function
		
		protected function submit_reset_password_email(){ 
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if			

			if(!isset($email) || !isset($code) || !isset($data)){
				return false;	
			}//end if
			
			$genSettings = $this->utils->get_settings();
			$settings = $this->utils->get_settings(array('module' => $this->module));
			$noReply = $genSettings['noReply'];			
			$type = $genSettings['defaultFormat'];
			$resetPwdURI = $settings['resetPasswordURI'];
			$template = $settings['resetPasswordEmailTemplate'];
			$subject = $settings['resetPasswordEmailSubject'];
			
			$encdata = array('email' => $email, 'code' => $code);
			$encdata = urlencode(base64_encode(serialize($encdata)));
			
			$tpld = array();
			$tpld['user_firstname'] = $data['user_firstname'];
			$tpld['activation_url'] = $resetPwdURI;
			$tpld['encdata'] = $encdata;
		
			$mailer = $this->utils->call("mailer");
						
			if($template != "" && $type == "text/html"){			
				$message = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
			} else {			
				if(isset($tpld['message'])){				
					$message = $this->utils->parse_text(array('text' => $tpld['message'],'data' => $tpld));;						
				} else {
					return false;	
				}//end if
			}//end if
			
			$mailer->set_sender($noReply);	
			$mailer->defaultFormat = 'text/html';
			$mailer->set_message(array('type' => 'text/html', 'message' => $message));
			$mailresult = $mailer->send_mail(array('email' => $email,'subject' => $subject));	
			if($mailresult){				
				return true;
			} else {				
				return false;	
			}//end if
		}//end function
		
		public function check_activation_code(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
		
			if(!isset($email) || !isset($code)){
				return false;	
			}//end if
			
			$email = trim($email);
			
			//first we check if the email exists
			$exists = $this->email_exists($email);
			if(!$exists){
				array_push($this->errors,"The passed email does not exists");
				return false;					
			}//end if
			
			//now we know that the email is in the database, so we get the details for that email
			$query = "SELECT * FROM users WHERE user_email = '".$email."' AND user_active = 1 AND user_temp = 0 LIMIT 1";
			$result = $this->utils->db->query($query);			
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){							
					$data = $this->utils->get_result_array($result,true);	
					if($data !== false){
						//now we create the activation code	
						$corret_code = sha1(__ENCRYPTION_PASSKEY__.':'.$email.':'.sha1($data['user_id']));
						if($corret_code == $code){
							$this->uid = $data['user_id'];
							return true;
						} else {
							array_push($this->errors,"The passed code is not valid");
							return false;	
						}//end if
					} else {
						array_push($this->errors,"The passed email does not exists or the user is inactive");
						return false;	
					}//end if
				} else {
					array_push($this->errors,"The passed email does not exists or the user is inactive");
					return false;
				}//end if
			} else {
				array_push($this->errors,"The passed email does not exists or the user is inactive");
				return false;	
			}//end if				
		}//end function
		
		public function reset_password(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if	
			
			if(!isset($password) || !isset($conf_password) || !isset($user_id)){
				return false;
			}//end if
			
			$password = trim($password);
			$conf_password = trim($conf_password);
			
			if(strlen($password) < 6){
				array_push($this->errors,'The password is too short');
				return false;	
			}//end if
			
			if($password != $conf_password){
				array_push($this->errors,'The two passwords do not match');
				return false;	
			}//end if
			
			$query = "UPDATE users SET user_password = '".sha1($password)."' WHERE user_id = ".$user_id;
			$result = $this->utils->db->query($query);			
			$errors = $this->utils->error($result,__LINE__,get_class($this));	
			if($errors === false){
				return true;
			} else {
				return false;
			}//end if
		}//end function
			
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################		
		
		protected function init_settings(){			
			//read the settings from the cms
			$this->settings = $this->utils->get_settings(array('module' => $this->module));			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>