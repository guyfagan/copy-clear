<?php	
	#################################################################
	# Core Class - 	last update 04/01/13							#
	#					created in 2008								#
	# This is a core class for the Old Hat CMS						#	
	#																#
	# 15/11/12 - Added UTF8 encoding on get_result_array method		#
	# 26/11/12 - Removed old and useless methods, added "addslashes"#
	#			 to get_result_array method							#
	# 20/12/12 - Started convertion to PDO 							#
	# 21/12/12 - Converted to PDO									#
	# 04/01/13 - Cleaned code on some methods						#
	#################################################################
	
	class core{
		
		public $db;//db connection
		public $pdoDB; //PDO db connection
		public $uid;//userid
		public $lt; //Login Type
		public $view;//View class
		public $errors = array();
		protected $pdo_err_mode = 'warning'; //it can be warning, error or silent
		protected $module; //module
		protected $cur_path;
		protected $settings_disabled_fields;		
		
		//constructor
		function __construct($current_path = NULL){
			$this->db_connect();	
			$this->lt = __LOGIN_TYPE__;
			$this->cur_path = $current_path;			
			//include the view class
			require_once($this->cur_path.'classes/view.class.php');
			$this->view = $this->call('view');
			$this->settings_disabled_fields = array("submit","action");
		}//end function
		
		private function db_connect(){		
			//$this->db = new PDO('mysql:host='.__DB_HOST__.';dbname='.__DB_NAME__.';charset=utf8', __DB_USER__, __DB_PASS__, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
			$this->db = new PDO('mysql:host='.__DB_HOST__.';dbname='.__DB_NAME__.';charset=utf8', __DB_USER__, __DB_PASS__, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));			
		}//end function
		
		public function set_db_errmode($mode){
			$allowed_modes = array('warning','error','silent');
			if(!in_array($mode,$allowed_modes)){
				return false;	
			}//end if
			switch($mode){
				case 'warning':
					$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
				break;
				case 'error':
					$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				break;
				case 'silent':
					$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
				break;	
			}
		}//end function 
		
		#Catch all the generated mysql errors
		public function error(){
			$params = func_get_args();
			if(!is_array($params[0])){
				$stmt = $params[0];				
				$error_line = $params[1];					
				$error_page = $params[2];
			} else {
				foreach($params[0] as $pkey => $value){
					${$pkey} = $value;
				}//end if
			}//end if
			
			if(is_null($error_line)){
				$error_line = 0;	
			}//end if
			
			if(is_null($error_page)){
				$error_page = "";	
			}//end if
			
			if(!isset($connection)){
				$connection = $this->db;	
			}//end if
			
			$iplog = $_SERVER['REMOTE_ADDR'];
			if($error_page == ""){
				$error_page = $_SERVER['PHP_SELF'];
			}//end if		
		
			//$error = $connection->errorCode();
			$error = $connection->errorInfo();			
			if($error[0] != "00000"){				
				$timeError = time();
				$query = "INSERT INTO errors_log
								(error_log_error_page,error_log_error,error_log_line,error_log_ip)
								VALUES
								('".addslashes($error_page)."','".addslashes($error[2])."',".$error_line.",'".$iplog."')";
				@$connection->query($query);	
				return true;
			} else {
				return false;						
			}//end if			
		}//end function
				
		public function set_module($m,$ma = NULL){
			//Module is a string like 'users', and I have to find the module id
			$this->module = $this->get_module_data($m);
			$this->module = $this->module['module_id'];		
		}//end function		
		
		public function read_params($class, $params){
			if(is_array($params)){
				foreach($params as $key => $value){
					$class->{$key} = $value;
				}//end foreach
			}//end if
		}//end function
		
		//Funzione che restituisce la struttura di una tabella
		public function get_table_struct($tablename){
			$query = "DESCRIBE ".$tablename;
			$result = @$this->db->query($query);			
			$this->error($result,__LINE__,get_class($this));
			$struct = array();
			while($row = $result->fetch(PDO::FETCH_ASSOC)){
				array_push($struct,$row['Field']);
			}//endwhile
			return $struct;
		}//endfunction
		
		public function table_exists($table_name){
			$query = "SHOW TABLES LIKE '".$table_name."'";
			$result = @$this->db->query($query);			
			$errors = $this->error($result,__LINE__,get_class($this));	
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					return true;
				} else {
					return false;
				}//end if 
			} else {
				return false;	
			}//end if
		}//end function
				
		public function get_result_array($result, $single_line = false){
			//echo $single_line;
			$num = $result->rowCount();		
			if($num >= 1){				
				$data = array();		
				//get a simple array with the information of only one record
				if($single_line){
					$row = $result->fetch(PDO::FETCH_ASSOC);				
					//Now check for all the fields of the resulting query		
					for($j = 0; $j < count($row); $j++){
						foreach($row as $key => $value){
							if(!is_int($row[$key])){
								$data[$key] = stripslashes(html_entity_decode($value));
							} else {
								$data[$key] = $value;
							}//end if
						}//end foreach
					}//end for	
				} else {
					//In case that the result could be more than one, or we need a multidimensional array, execute the following code
					$i = 0;		
					while($row = $result->fetch(PDO::FETCH_ASSOC)) {
						//Now check for all the fields of the resulting query									
						for($j = 0; $j < count($row); $j++){
							foreach($row as $key => $value){
								if(!is_int($row[$key])){
									$data[$i][$key] = stripslashes(html_entity_decode($value));
								} else {
									$data[$i][$key] = $value;
								}//end if
							}//end foreach							
						}//end for						
						$i++;
					}//end while
				}//end if
			} else {
				//nothing found
				return false;
			}//endif
			return $data;
		}//end function	
		
		//This function return the settings for the selected module
		function get_settings(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$module = $params[0];
			}//end if
			
			if(isset($module)){
				$module_id = $module;	
			}//end if
			
			if(!isset($module_id) || is_null($module_id)){			
				$module_id = 0;
			} else if(is_string($module_id)){
				$module_data = $this->get_module($module_id);
				$module_id = $module_data['module_id'];
			}//end if
				
			$query = "SELECT * FROM settings WHERE setting_module_id = '".$module_id."'";
			$result = @$this->db->query($query);
			$error = $this->error($result,__LINE__,get_class($this));	
			if($error === false){
				$num = @$result->rowCount();
				if($num > 0){
					$data = array();
					while($row = $result->fetch(PDO::FETCH_ASSOC)) {					
						$data[$row['setting_label']] = stripslashes($row['setting_value']);						
					}//endwhile
					return $data;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		//This method update the settings for the specific passed module
		function update_settings(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($module_id) || is_null($module_id)){			
				$module_id = 0;
			} else if(is_string($module_id)){
				$module_data = $this->get_module($module_id);
				$module_id = $module_data['module_id'];
			}//end if
			
			$post_array = $_POST;
			$data_settings = array();
			foreach($post_array as $field => $value){
				$data_settings[$field] = $value;
				//Check if the passed value is an array, in positive case trasform serialize the array
				if(is_array($value)){
					$data_settings[$field] = serialize($value);
				}//endif
			}//endforeach
			
			$searchable = false;			
			if($post_array['module_searchable'] == 1){
				$searchable = true;	
			}//end if
			$searchable = (int)$searchable;
			
			$this->db->beginTransaction();
			try{				
				//update the module searchable setting
				if($module_id != 0){
					$query = "UPDATE modules SET 
							  module_searchable = :searchable			
							  WHERE module_id = :module_id";															
					$result = $this->db->prepare($query);
					$result->bindParam(':searchable', $searchable, PDO::PARAM_INT);
					$result->bindParam(':module_id', $module_id, PDO::PARAM_INT);	
					$result->execute();
					$errors = $this->error($result,__LINE__,get_class($this));
					if($errors !== false){
						throw new PDOException("Cannot updated the module searchable option");		
					}
				}
				
				//Erase all the previous settings for this module
				$query = "DELETE FROM settings WHERE setting_module_id = '".$module_id."'";	
				$result = $this->db->query($query);
				$errors = $this->error($result,__LINE__,get_class($this));
				if($errors === false){				
					foreach($data_settings as $field => $value){			
						if(!in_array($field,$this->settings_disabled_fields)){				
							//If numeric put the content in the "int" field			
							$query = "INSERT INTO settings
									  (setting_label, setting_value,setting_module_id)
									  VALUES
									  ('".$field."','".addslashes($value)."','".$module_id."')";									 
							$result = $this->db->query($query);
							$errors = $this->error($result,__LINE__,get_class($this));	
							if($errors !== false){
								throw new PDOException("Cannot save the new settings");	
							}
						}//end if
					}//end foreach					
				} else {
					throw new PDOException("Cannot delete the previous settings");		
				}//end if
				
			} catch(PDOException $ex){
				$this->db->rollback();
				$this->errors = $ex->getMessage();
				return false;	
			}//end try
			$this->db->commit();	
			return true;
		}//end function
		
	}//end class
?>