<?php
	require_once('ewrite.class.php');

	class ccci{
		protected $utils;//utils class
		public $module; //module
		public $module_id; //Module id number
		public $errors = array();
		public $settings = array();
		protected $meta;

		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;
			$this->module = "ccci";
			$this->utils->read_params($this,$params);
			//Meta init
			$this->meta = $this->utils->call("meta");
			//setting the current module in the class
			$this->set_module($this->module);
			//Set basic settings
			$this->init_settings();
		}//endconstructor

		public function set_module($module){
			$module_data = $this->utils->get_module($module);
			if($module_data !== false){
				$this->module = $module_data['module_name'];
				$this->module_id = $module_data['module_id'];
				$this->table = $module_data['module_table'];
				if(isset($this->meta) && is_object($this->meta)){
					$this->meta->set_meta_module($this->module);
				}//end if
			}//end if
		}//end function

		protected function check_token($token){
			if(is_null($token)){
				return false;
			}//end if

			if(defined("__SALT__")){
				$users = $this->utils->call('users');
				$user_data = $users->get_user();
				if($user_data !== false){
					$good_token = sha1($user_data['user_id'].':'.$user_data['user_username'].'-'.__SALT__);
					if($token == $good_token){
						return true;
					} else {
						return false;
					}//end if
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_mediums(){
			$query = "SELECT * FROM ads_medium
					  ORDER BY ad_medium_order ASC, ad_medium_id ASC";

			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,false);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		public function get_medium(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if

			if(!isset($id)){
				return false;
			}//end if

			$query = "SELECT * FROM ads_medium WHERE ad_medium_id = ".$id."
					  ORDER BY ad_medium_order ASC, ad_medium_id ASC";

			$this->utils->cache = $query;

			$result = $this->utils->db->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();
				if($num > 0){
					$data = $this->utils->get_result_array($result,true);
					return $data;
				} else {
					return false;
				}//end if
			} else {
				return false;
			}//end if
		}//end function

		#################################################################
		# SETTINGS PART													#
		#################################################################

		private function init_settings(){
			//read the settings from the cms
			$this->settings = $this->utils->get_settings(array('module' => $this->module));
			return true;
		}//end function

		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function

		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>
