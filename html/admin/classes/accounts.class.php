<?php
	require_once("users.class.php");
	
	class accounts extends users{
		
		public $settings;
		
		protected $required_fields = array('username', 'email', 'password', 'conf_password');
		
		//Constructor
		function __construct($utils){
			if(class_exists("users")){
				parent::__construct($utils);					
			}//end if
			$this->module = "accounts";
			$this->init_settings();		
		}//end function
		
		public function register_user(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($fields)){
				array_push($this->errors,"You didn't specify any fields");
				return false;	
			}//end if
			
			//checking if we have all the required fields
			foreach($this->required_fields as $value){
				if(!isset($fields[$value]) || $fields[$value] == ""){
					array_push($this->errors,"Missing ".$value." field");
					return false;	
				}//end if
			}//end if
			
			//Check first if the username is already used
			if($this->user_exists($fields['username']) == true){
				array_push($this->errors,"User already exists");					
			}//end if
			//in case the username and the email are different
			if($fields['username'] != $fields['email']){
				//Check if the email is already used
				if($this->email_exists($fields['email']) == true){
					array_push($this->errors,"Email already exists");					
				}//end if
			}//end if
			
			if($fields['password'] != $fields['conf_password']){
				array_push($this->errors,"The passwords do not match");				
			}//end if			
			
			if(sizeof($this->errors) > 0){
				return false;	
			}//end if
			
			//we check if we have to active the user straight away or not
			if(isset($this->settings['account_auto_activation'])){
				if((bool)$this->settings['account_auto_activation']){
					$fields['active'] = 1;
					$fields['temp'] = "0";
				} else {
					$fields['active'] = "0";
					$fields['temp'] = "0";
				}//end if
			}//end if
			
			//check for the default group if not passed
			if(!isset($group_id) && isset($this->settings['default_users_group'])){
				if(is_numeric($this->settings['default_users_group'])){
					$group_id = (int)$this->settings['default_users_group'];	
				}//end if
			}//end if
			
			$users_fields = array();
			$insert_fields = array();
			$insert_param_fields = array();
			$meta_fields = array();
			foreach($fields as $key => $value){
				if($key !== 'conf_password'){
					if(strpos($key,"meta_") === false){
						if($key != "password"){
							$users_fields['user_'.$key] = $fields[$key];
						} else {
							$users_fields['user_'.$key] = sha1($fields[$key]);	
						}
						array_push($insert_fields, 'user_'.$key);
						array_push($insert_param_fields, ':user_'.$key);						
					} else {
						$meta_fields[$key] = $fields[$key];
					}//end if
				}//end if
				unset($fields[$key]);
			}//end foreach
					
			$query = "INSERT INTO users 
					  (".implode(', ',$insert_fields).")
					  VALUES
					  (".implode(', ',$insert_param_fields).")";					  
			$result = $this->utils->db->prepare($query);
			foreach($users_fields as $key => &$value){
				$result->bindParam(':'.$key, $value);	
				//echo ':'.$key.', '.$value;
			}//end foreach
			$result->execute();
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$this->uid = $this->utils->db->lastInsertId();
				if(isset($group_id)){
					$this->create_user_group_relation($this->uid,$group_id);	
				}//end if
				/* META SUPPORT */	
				if(sizeof($meta_fields) > 0){
					$this->meta->set_meta_type('user');				
					$this->meta->set_meta_id($this->uid);	
					foreach($meta_fields as $key => &$value){
						$this->meta->add_meta(array('label' => $key, 'value' => $value));
					}//end foreach
				}//end if
				
				//now we check if the user has to activate his account
				if(isset($this->settings['account_email_activation'])){
					if((bool)$this->settings['account_email_activation']){
						$this->send_activation_email();
					}//end if
				}//end if
				return true;
			} else {
				array_push($this->errors, "Cannot save this user");
				return false;	
			}//end function
		}//end function
		
		protected function send_activation_email(){
			//get general settings
			$genSettings = $this->utils->get_settings();
			//get the email type to send
			$type = $this->settings['account_email_type'];	
			//get user info
			$user_data = $this->get_user(array('id' => $this->uid));
			//double checking if we have all the user info
			if($user_data !== false){
				//we get the email address
				$email = $user_data['user_email'];
				if($email == ""){
					return false;	
				}//end if
				//create the email obj
				$mailer = $this->utils->call("mailer");
				//add the no-reply address
				if(!isset($this->settings['account_no_reply']) || $this->settings['account_no_reply'] == ""){
					//fallback to general no reply
					$no_reply = $genSettings['noReply'];
				} else {
					$no_reply = $this->settings['account_no_reply'];
				}//end if
				//set the sender in the mail obj
				$mailer->set_sender($no_reply);	
				//set eventual bcc
				if(isset($this->settings['account_bcc_on_notification']) && $this->settings['account_bcc_on_notification'] != ""){
					$mailer->set_bcc($this->settings['account_bcc_on_notification']);
				}//end if
				//generate the activation link
				$activation_code = urlencode(sha1(__ENCRYPTION_PASSKEY__.':'.$this->uid.':'.$user_data['user_email']));
				$activation_link = $this->settings['account_activation_uri'].'?u='.urlencode(base64_encode($this->uid)).'&ac='.$activation_code;
				
				//set the email type
				if($type == "html"){
					$mailer->defaultFormat = 'text/html';	
					$user_data['activation'] = '<a href="'.$activation_link.'">Click here to activate your account</a>';				
				} else {
					$mailer->defaultFormat = 'text/plain';	
					$user_data['activation'] = $activation_link;					
				}//end if
				if(!isset($this->settings['account_email_subject']) || $this->settings['account_email_subject'] == NULL){
					$subject = "New account activation";			
				} else {
					$subject = $this->settings['account_email_subject'];					
				}//end if
				//get the message
				$cms_message = $this->settings['account_new_email_message'];
				//parse the m essage
				$cms_message = $this->utils->parse_text(array('data' => $user_data,'text' => $cms_message));
				$options = array("data" => $user_data, "type" => $type,'message' => $cms_message);		
				//merge the message with the template	
				$message = $this->get_notification_message($options);				
				$mailer->set_message(array('message' => $message));
				$mailresult = $mailer->send_mail(array('email' => array($email),'subject' => $subject));
				if($mailresult){
					return true;	
				} else {
					return false;
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		public function check_activation(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if		
			}//end if
			
			if(!isset($user_id) || !isset($code)){
				return false;	
			}//end if
			
			$user_id = base64_decode(urldecode($user_id));
			
			$user_data = $this->get_user(array('id' => (int)$user_id));
			if($user_data !== false){
				$good_code = urlencode(sha1(__ENCRYPTION_PASSKEY__.':'.$user_id.':'.$user_data['user_email']));
				if($good_code == $code){
					$this->change_user_status(array('id' => $user_id, 'status' => 1));
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		protected function get_notification_message(){
			$params = func_get_args();
			if(is_array($params[0])){
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			} else {
				$data = $params[0];			
			}//end if
			
			if(!isset($data) && !is_array($data)){
				array_push($this->errors,"No data passed to the notification manager");
				return false;	
			}//end if
			
			if(!isset($message)){
				array_push($this->errors,"No message passed to the notification manager");
				return false;	
			}//end if
		
			if(!isset($type)){
				$type = "html";	
			}//end if	
		
			switch($type){
				case "html":
					$genSettings = $this->utils->get_settings();
					$html_message = $message;
					
					//now we check if we have to apply a template
					if((bool)$genSettings['enableEmailTemplates'] === true){
						//we get the template from the module settings
						$template = $this->settings['defaultModuleEmailTemplate'];
						if($template != ""){							
							//so if we have a template defined, we process the file
							$tpld = array();//Template Data
							//in this array we pass the data to the html email template
							$tpld['message'] = $html_message;	
							if(isset($signature)){
								$tpld['signature'] = $signature;	
							}//end if
							$html = $this->utils->parse_mail_template(array('template' => $template,'data' => $tpld));
							if($html !== false){
								$html_message = $html;	
							}//end if
						} else {
							if(isset($signature)){
								$html_message .= $signature;
							}//end if
						}//end if
					} else {
						if(isset($signature)){
							$html_message .= $signature;
						}//end if
					}//end if
					$message = $html_message;
				break;
			}//end switch
			return $message;
		}//end function
		
		public function reset_password(){
			
		}//end function
		
		public function reset_password_notification(){
			
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################
				
		protected function init_settings(){
			$this->settings = array();	
			$this->settings['mail_charset'] = 'iso-8859-1';			
			//read the settings from the cms			
			$cms_settings = $this->utils->get_settings($this->module);
			if($cms_settings !== false){
				//if we have something, merge the two arrays, bear in mind that the cms settings will override the previous settings
				$this->settings = array_merge($this->settings,(array)$cms_settings);	
			}//end if
			
			if($this->lt == "cms"){
				if(isset($this->settings['login_type'])){
					$accepted_types = array('cookie','session');
					if(in_array($this->settings['login_type'], $accepted_types)){
						$this->lt = $this->settings['login_type'];	
					} else {
						//fallback to cookie
						$this->lt = 'cookie';
					}//end if
				} else {
					//fallback to cookie
					$this->lt = 'cookie';	
				}//end if
			}//end if
			
			$this->cookie_expiry = time()+((3600*24)*31);
			if(isset($this->settings['cookie_expiry']) && is_numeric(($this->settings['cookie_expiry'])) && (int)$this->settings['cookie_expiry'] > 0){
				$this->cookie_expiry = time()+(int)$this->settings['cookie_expiry'];
			}//end if
			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>