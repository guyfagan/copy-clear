<?php
	class import{
		
		protected $utils;//utils class
		protected $separator;
		protected $map;	
		protected $import;
		protected $meta;
		public $settings;
		public $format;
		public $filename;
		public $stats;
		public $errors = array();
		protected $db2;	
				
		//Constructor
		function __construct($utils,$params = array()){
			$this->utils = $utils;			
			$this->utils->read_params($this,$params);	
			$this->format = "csv";
			$this->separator = ';';
			$this->settings = array();	
			//Meta init	
			$this->meta = $this->utils->call("meta");						
		}//end function
		
		public function set_mapping(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($map)){
				$this->map = $map;	
			}//end if
			
			if(isset($table)){
				$this->settings['table'] = $table;	
			}//end if
			
			if(isset($meta)){
				$this->settings['meta'] = $meta;	
			}//end if
		}//end if
		
		public function read(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(isset($filename)){
				$this->filename = $filename;					
			}//end if
			
			$this->import = array();
			
			$handle = fopen($this->filename, "r");
			$data = fread($handle,filesize($this->filename));
			$data = explode("\n",$data);
			
			if(isset($ignore_titles)){
				array_shift($data);
			}//end if
			
			for($i = 0; $i < sizeof($data); $i++){
				
				$line = explode(";",$data[$i]);
				$this->import[$i] = array();
				for($x = 0; $x < sizeof($line); $x++){
					$this->import[$i][$x] = trim(str_replace('"','',$line[$x]));	
				}//end for x
				
			}//end for i			
			fclose($handle);
				
			
			if(isset($save)){
				switch($save){
					case "database":
					 	$tools = $this->utils->call('tools');
						$users = $this->utils->call('users');
						$this->meta->set_meta_module($this->settings['module']);
						if(is_array($this->map) && isset($this->settings['table'])){
							for($i = 0; $i < sizeof($this->import); $i++){
								//var_dump($this->import[$i]);
								$sql_fields = array();
								$meta_fields = array();
								$sql_values = array();
								$meta_values = array();
								foreach($this->map as $key => $value){
									//here we get the fields
									$meta = false;
									if(strpos($key,'meta_') !== false){
										array_push($meta_fields,$key);
										$meta = true;
									} else {
										array_push($sql_fields,$key);
									}//end if
									
									if(isset($value['field'])){									
										//here we get the values
										if(!is_array($value['field'])){										
											if(is_numeric($value['field'])){
												$import_value = $this->import[$i][$value['field']];	
												if(isset($value['prefix'])){
													$import_value = $value['prefix'].$import_value;	
												}
											} else if(is_string($value['field'])){											
												switch($value['field']){
													case 'password':			
														$password = $tools->get_random_password();										
														$import_value = sha1($password);
														array_push($meta_fields,'meta_password');
														array_push($meta_values,$password);
														break;	
												}//end switch							
											}//end if
										} else{
											$import_value = array();
											foreach($value['field'] as $subvalue){
												if(substr($this->import[$i][$subvalue],-1) == ','){
													$this->import[$i][$subvalue] = substr($this->import[$i][$subvalue],0,strlen($this->import[$i][$subvalue])-1);	
												}//end if
												array_push($import_value,$this->import[$i][$subvalue]);
											}//end foreach
											$import_value = implode(', ',$import_value);
										}//end if
										
										if($meta == false){
											array_push($sql_values,$import_value);
										} else {
											array_push($meta_values,$import_value);
										}//end if									
									}//end if
								}//end foreach
								
								if($this->settings['table'] == 'users'){
									array_push($sql_fields,'user_active');
									array_push($sql_values,1);
									array_push($sql_fields,'user_temp');
									array_push($sql_values,0);
								}//end if
								
								$query = "INSERT INTO ".$this->settings['table']."
										  (".implode(", ",$sql_fields).")
										  VALUES
										  ('".implode("', '",$sql_values)."')";
								
								$result = $this->utils->db->query($query);
								$errors = $this->utils->error($result,__LINE__,get_class($this));
								if($errors === false){
									$id = $this->utils->db->lastInsertId();	
									if($this->settings['table'] == 'users'){
										$this->meta->set_meta_type('user');			
																		
										if(isset($this->settings['group_id'])){
											$users->create_user_group_relation($id,$this->settings['group_id']);
										}//end if
									}//end if
									
									/* META SUPPORT */										
									if(sizeof($meta_values) > 0){
										$this->meta->set_meta_id($id);										
										for($x = 0; $x < sizeof($meta_values); $x++){											
											$this->meta->add_meta(array('label' => $meta_fields[$x],'value' => $meta_values[$x]));										
										}//end for i
									}//end if
									/* END META SUPPORT */
								}//end if								
							}//end for i
							return true;
						} else {
							return false;	
						}
					break;
				}//end switch
			}//end if
		}//end function
		
		#################################################################
		# IMPORT FULL DB PART											#		
		#################################################################
		
		public function import_db(){
			//first thing, we create the database connection
			$options = array();
			if($_POST['location_db'] == "local"){
				$options['host'] = __DB_HOST__;
				$options['dbname'] = $_POST['source_db'];
				$options['user'] = __DB_USER__;
				$options['pass'] = __DB_PASS__;
			} else {
				$options['host'] = $_POST['remote_host'];
				$options['dbname'] = $_POST['remote_db'];
				$options['user'] = $_POST['remote_user'];
				$options['pass'] = $_POST['remote_password'];
			}//end if
			
			$connected = $this->create_secondary_db_conn($options);
			if($connected){
				$this->utils->db->beginTransaction();
				try{
					//now we have the database connection with the source database, next step is import the modules						
					$this->import_modules();
					
					//once the modules have been imported, we start to import al the posts
					$this->import_posts();
				
					//if everything went fine, we commit the edits
					$this->utils->db->commit();	
				
				} catch(Exception $ex){
					$this->utils->db->rollback();
					$this->errors = $ex->getMessage();
					return false;	
				}//end try	
			} else {
				return false;	
			}//end if
		}//end function
		
		protected function create_secondary_db_conn(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($host) || !isset($dbname) || !isset($user) || !isset($pass)){
				return false;
			}//end if
			
			try{
				$this->db2 = new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8', $user, $pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT, PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
				return true;
			} catch(PDOException $ex){
				$this->errors = $ex->getMessage();				
				return false;
			}//end try		
		}//end function
		
		private function import_modules(){
			$ignore_modules = array('users','generic','api','forms','stats','banners');
			//we do not import module_groups table, as we will force any module to go into the "site" group (which has the ID = 2)
			//we read the modules from the source database
			$query = "SELECT * FROM modules WHERE module_name NOT IN ('".implode("','",$ignore_modules)."')";
			$result = $this->db2->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){							
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							$module_table = $data[$i]['module_table'];
							//we force the galleries to use the posts table
							if($data['module_name'] == 'galleries'){
								$module_table = 'posts';
							}//end if	
							$insert = "INSERT INTO modules 
									   (module_id, module_name, module_label, module_table, module_hidden, module_order, module_public, module_gid, module_searchable)
									   VALUES 
									   (".$data[$i]['module_id'].", '".$data[$i]['module_name']."','".$data[$i]['module_label']."','".$module_table."',".$data[$i]['module_hidden'].",".$data[$i]['module_order'].",1,2,0)";	
							$result = $this->utils->db->query($insert);
							$this->utils->error($result,__LINE__,get_class($this));
							//we also add the user permissions to the admin for the current module
							$insert = "INSERT INTO users_permissions
									   (user_permission_gid, user_permission_module_id, user_permission_permission)
									   VALUES
									   (1, ".$data[$i]['module_id'].",7)";
							$result = $this->utils->db->query($insert);
							$this->utils->error($result,__LINE__,get_class($this));
						}//end for i
						return true;
					} else {
						throw new Exception('Cannot read the data.');
						return false;
					}//end if
				} else {
					throw new Exception('Cannot read the data.');
					return false;	
				}//end if
			} else {
				throw new Exception('Cannot read the data.');
				$this->errors = array("Cannot read source modules");
				return false;	
			}//end if
		}//end function
		
		private function import_posts(){
			$query = "SELECT * FROM modules WHERE module_name != 'users' AND module_name != 'generic' AND module_name != 'api'";
			$result = $this->db2->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){	
					while($row = $result->fetch(PDO::FETCH_ASSOC)){
						$options = array();
						$options['module_name'] = $row['module_name'];
						$options['module_id'] = $row['module_id'];
						$options['module_table'] = $row['module_table'];						
						//now that we have the basic info for the current module, we check the table name
						switch($options['module_table']){
							case "posts":
								//classic posts 
								$this->import_classic_posts($options);
								break;	
							case "galleries":
								$this->import_galleries($options);
								break;
						}//end switch
					}//end while
				} else {
					return false;	
				}//end if
			} else {
				return false;
			}//end if
		}//end function
		
		private function import_classic_posts(){
			
		}//end function
		
		private function import_galleries(){
			$params = func_get_args();
			if(is_array($params[0])){				
				foreach($params[0] as $key => $value){
					${$key} = $value;
				}//end if
			}//end if
			
			if(!isset($module_name) || !isset($module_id) || !isset($module_table)){
				return false;
			}//end if
			
			$this->meta->set_meta_module(array('module' => $module_name));
			
			$query = "SELECT * FROM ".$module_table;
			//echo $query;
			$result = $this->db2->query($query);
			$errors = $this->utils->error($result,__LINE__,get_class($this));
			if($errors === false){
				$num = $result->rowCount();	
				if($num > 0){				 	
					$data = $this->utils->get_result_array($result,false);
					if($data !== false){
						for($i = 0; $i < sizeof($data); $i++){
							//we create the post for the gallery with the basic info
							$insert = "INSERT INTO posts 
									   (post_title, post_message, post_date, post_uid, post_module_id, post_temp,
									   post_status)
									   VALUES
									   ('".$data[$i]['gallery_title']."','".$data[$i]['gallery_message']."','".$data[$i]['gallery_date']."',".$data[$i]['gallery_uid'].",'".$module_name."', 0,
									   ".$data[$i]['gallery_status'].")";	
							$result = $this->utils->db->query($insert);
							$errors = $this->utils->error($result,__LINE__,get_class($this));
							if($errors === false){   
								$post_id = $this->utils->db->lastInsertId();	
								//now we create the relation in the modules_items_order table
								$insert = "INSERT INTO modules_items_order
										   (module_item_order_module_id, module_item_order_item_id, module_item_order_type, module_item_order_order)
										   VALUES
										   (".$module_id.",".$post_id.",'post',".$i.")";
								$result = $this->utils->db->query($insert);
								$errors = $this->utils->error($result,__LINE__,get_class($this));
								//now that we have the post id, we can save the rest of the info as meta info										
								$this->meta->set_meta_id(array('id' => $post_id));
								$this->meta->add_meta(array("label" => "meta_flickr_uri", "value" => $data[$i]['gallery_flickr_uri']));							
								$this->meta->add_meta(array("label" => "meta_flickr_type", "value" => $data[$i]['gallery_flickr_type']));
								$this->meta->add_meta(array("label" => "meta_flickr_id", "value" => $data[$i]['gallery_flickr_id']));
								$this->meta->add_meta(array("label" => "meta_flickr_owner", "value" => $data[$i]['gallery_flickr_owner']));
								$this->meta->add_meta(array("label" => "meta_flickr_main_image", "value" => $data[$i]['gallery_flickr_main_image']));								
							}//end if
						}//end for i
					}//end if
					return true;
				} else {
					return false;	
				}//end if
			} else {
				return false;	
			}//end if
		}//end function
		
		#################################################################
		# SETTINGS PART													#		
		#################################################################		
		
		protected function init_settings(){			
			//read the settings from the cms
			$this->settings = $this->utils->get_settings(array('module' => $this->module));			
			return true;
		}//end function
		
		//pass an array with settings to set, eg.: array("param1" => "value1", "param2" => "value2")
		public function set_settings($settings){
			foreach($settings as $key => $value){
				$this->settings[$key] = $value;
			}//end foreach
			return true;
		}//end function
		
		public function get_settings(){
			return $this->settings;
		}//end function
	}//end class
?>