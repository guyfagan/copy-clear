<?php
	ob_start();
	session_start();
	ini_set("display_errors",1);
	include("config.inc.php");
	include("classes/utils.class.php");	
	include("inc/secure.inc.php");
	include("inc/init.cms.inc.php");	
		
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="<?php echo __CMS_CHARSET__ ?>">
<!--meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /-->
<title><?php echo $utils->view->settings['siteName'] ?> CMS</title>
<!-- Bootstrap -->
<link href="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap.css" rel="stylesheet" media="screen" type="text/css" />
<link href="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap-responsive.css" rel="stylesheet" media="screen" type="text/css" />
<!-- Font Awesome -->
<link href="<?php echo __CMS_PATH__ ?>inc/fontawesome/<?php echo __FONTAWESOME_VERSION__ ?>/css/font-awesome.min.css" rel="stylesheet" media="screen" type="text/css" />
<?php
if(!defined(__USE_CDN__) || __USE_CDN__ == false){
?>
<link href="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/css/ui-lightness/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.css" rel="stylesheet" media="screen" type="text/css" />
<script src="<?php echo __CMS_PATH__ ?>inc/jquery/jquery-<?php echo __JQUERY_VERSION__ ?>.min.js"></script>
<script src="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/js/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.js"></script>
<?php
} else {
?>
<!-- USING CDN FOR JQUERY UI CSS -->
<link href="http://code.jquery.com/ui/<?php echo __JQUERY_UI_VERSION__ ?>/themes/base/jquery-ui.css" rel="stylesheet" media="screen" type="text/css" />
<!-- USING CDN FOR JQUERY -->
<script src="http://code.jquery.com/jquery-<?php echo __JQUERY_VERSION__ ?>.js"></script>
<!-- USING CDN FOR JQUERY UI -->
<script src="http://code.jquery.com/ui/<?php echo __JQUERY_UI_VERSION__ ?>/jquery-ui.js"></script>
<?php
}//end if
?>
<link href="<?php echo __CMS_PATH__ ?>css/screen.css?v=<?php echo __CMS_VERSION__ ?>" rel="stylesheet" media="screen" type="text/css" />
<link href="<?php echo __CMS_PATH__ ?>css/media.css?v=<?php echo __CMS_VERSION__ ?>" rel="stylesheet" media="screen" type="text/css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/js/bootstrap.min.js"></script>
<script src="<?php echo __CMS_PATH__ ?>js/config.js.php?m=<?php echo $utils->view->module ?>&a=<?php echo $utils->view->action ?>&v=<?php echo __CMS_VERSION__ ?>"></script>
<script src="<?php echo __CMS_PATH__ ?>inc/jloader/jloader.jquery.js?v=<?php echo __CMS_VERSION__ ?>"></script>
<script src="<?php echo __CMS_PATH__ ?>js/common.js?v=<?php echo __CMS_VERSION__ ?>"></script>
<?php
	//load js needed
	$utils->view->autoload_js();
?>
</head>

<body>
	<?php
		include("inc/topnav.inc.php");
	?>
	<div class="container-fluid clearfix">
    	<div class="row-fluid clearfix">
            <div class="span3">
            	<?php
				include("inc/sidenav.inc.php");
				?>
            </div><!--/span-->
            <div class="span9">
            <?php
				echo '<div class="row-fluid clearfix">';
				echo '<div class="span7">';
				//print breadcrumbs
				$utils->view->print_breadcrumbs();
				echo '</div>';
				echo '<div class="span5">';
				//print search html
				$utils->view->get_search();
				echo '</div>';
				echo '</div>';
				
				$pageToLoad = $utils->view->build_page();
				if($pageToLoad != false){
					require_once($pageToLoad);
				}//end if
			?>  
            </div><!--/span-->
        </div><!--/row-->        
        <hr class="clearfix">        
        <?php
			include("inc/footer.inc.php");
		?>

    </div><!--/.fluid-container-->
</body>
</html>