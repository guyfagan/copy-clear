<?php
	ini_set('display_errors',1);
	include("config.inc.php");
	include("classes/utils.class.php");	
	$photos = view::get_backgrounds();
?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $utils->view->settings['siteName'] ?> CMS</title>
<!-- Bootstrap -->
<link href="inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
<link href="css/screen.css" rel="stylesheet" media="screen">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="inc/jquery/jquery-<?php echo __JQUERY_VERSION__ ?>.min.js"></script>
<script src="inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/js/bootstrap.min.js"></script>
<?php
if($photos !== false){
?>
<script src="inc/galleryFader/galleryFader.js"></script>
<script>
	$(document).ready(function(){
	$.login = {};
	$.login.images = <?php echo json_encode($photos) ?>;
	$(".bg-image").galleryFader({
		gallery: $.login.images,
		auto: true,
		delay: 10000,
		nextBtn: '.next',
		prevBtn: '.prev',
		fx: 'fade'
	});
});
</script>
<?php
}//end if
?>
<script src="js/login.js"></script>
</head>

<body id="login-page">
	<div class="bg-image"></div>
	<div class="container" id="login-container">
        <div id="login">   
        	      
            <form class="form-signin" method="post" id="login-form" name="login-form">
            	<input type="hidden" name="api_key" id="api_key" value="db098ccf23d7d5a88b030760b55c07044f9e745c" />
                <h2 class="form-signin-heading">Please sign in</h2>
                <input type="text" id="username" name="username" class="input-block-level" placeholder="Email address">
                <input type="password" id="password" name="password" class="input-block-level" placeholder="Password">
                <!--label class="checkbox">
                  <input type="checkbox" value="remember-me"> Remember me
                </label-->
                <button class="btn btn-large btn-primary" type="submit">Sign in</button>
              </form>               
        </div>
    </div>
</body>
</html>