/************************************************************/
/*	jQuery formdata 0.3 - 21/03/13							*/
/*															*/
/*	UPDATES													*/
/*	27/09/12	- 	Plug-in created							*/
/*	28/09/12	- 	Added the prefix						*/
/*	17/10/12	-	Added the support for meta elements		*/
/*  12/12/12 	-   Added support for checkboxes			*/
/*  15/12/12	-   Added support to radios					*/
/*  21/03/13    -   Now you can add a list of fields to 	*/
/*				    ignore									*/
/*															*/
/************************************************************/
$().ready(function(){
	$.fn.formdata = function(params){
		params = $.extend({
			data: {}, 
			prefix: "", 
			meta: false,
			ignore: []
		}, params);  		
		var $form = $(this);
		if(params.data.length > 0){
			params.data = $.parseJSON(params.data);	
		}	
		$(':input:not(:hidden,:submit,:image,[data-ignore-api]), select, textarea',$form).each(function(){
			var $tag = this.nodeName.toLowerCase();	
			//console.log($(this).attr('id'));
			ignored = false;
			if($.inArray($(this).attr('name'),params.ignore) >= 0){
				ignored = true;	
			}			
			switch($tag){
				case "input":
				case "select":
				case "textarea":				
					if(params.meta == true){
									
						if(params.data[params.prefix+$(this).prop('name')] != "" && $(this).prop('name').indexOf("meta_") == -1 && ignored == false){
							switch($(this).prop("type")){
								default:
								case "text":
									$(this).val(params.data[params.prefix+$(this).prop('name')]);		
									break;
								case "radio":
								case "checkbox":								
									if($(this).val() == params.data[params.prefix+$(this).prop('name')]){
										$(this).prop("checked","checked");	
									}
									break;	
							}
						} else if($(this).prop('name').indexOf("meta_") != -1 && params.data[$(this).prop('name')] != "" && ignored == false){						
							switch($(this).prop("type")){
								default:
								case "text":
									$(this).val(params.data[$(this).prop('name')]);	
									break;
								case "radio":
								case "checkbox":									
									if($(this).val() == params.data[$(this).prop('name')]){
										$(this).prop("checked","checked");	
									}
									break;	
							}
						}
					} else {
						if(params.data[params.prefix+$(this).prop('name')] != "" && ignored == false){
							switch($(this).prop("type")){
								default:
								case "text":
									$(this).val(params.data[params.prefix+$(this).prop('name')]);			
									break;
								case "radio":
								case "checkbox":
									if($(this).val() == params.data[params.prefix+$(this).prop('name')]){
										$(this).prop("checked","checked");	
									}									
									break;								
							}
						}
					}
					break;					
			}//end switch			
		});
	}
});