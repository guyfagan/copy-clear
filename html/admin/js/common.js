// JavaScript Document

$().ready(function(){
	$.config = $settings;
	$.cmsVars = {};
	$.cmsVars.formSaved = false;
	$.cmsVars.media = {};
});


/* SWITCHER */

$.fn.jSwicher = function(){
	$(this).each(function(){
		$(this).click(function(e){
			e.preventDefault();		
			var $ajaxCall = $.config.apipath + "update.category.status";	
			//alert($(this).parent().attr("data-type"));
			if($(this).parent().data("type") == "post"){
				$ajaxCall = $.config.apipath + "update.post.status";
			}
			if($(this).parent().data("type") == "user"){
				$ajaxCall = $.config.apipath + "update.user.status";;	
			}
			
			if(typeof($(this).parent().data("api-call")) != "undefined"){
				$ajaxCall = $.config.apipath + $(this).parent().data("api-call");
			}
			
			if($(this).hasClass("on")){				
			//	$(this).animate({'background-position-x': '100'},1000,function(){
					$(this).removeClass("on").addClass("off");					
				//});
				var $value = 0;				
			} else {
				//$(this).animate({backgroundPosition: "-72px"},1000,function(){
					$(this).removeClass("off").addClass("on");						
				//});
				var $value = 1;
			}
			var $id = $(this).parent().data('id');		
			$.get($ajaxCall+'?value='+$value+"&id="+$id+"&module="+$.config.module);		
		});
	});
}

$.fn.popMessage = function(msg,params){
	params = $.extend( {type: 'alert', incipit: '',when: 'before',scrollTopOnComplete: true,local: false,class: '',persistent: false, fadeDelay: 5000}, params);
	if(params.class != ''){
		params.local = true;
		params.class = params.class.replace('.','');	
	}
	
	var $alertObj;
	
	if(params.local){
		//removes the closest alert to the trigger
		if(params.class != ''){
			$alertObj = $('.'+params.class,$(this).parent());	
		} else {
			$alertObj = $('.alert',$(this).parent());	
		}//end if
	} else {
		//removes all the alerts
		$alertObj = $(".alert");	
	}
	
	$alertObj.remove();
		
	switch(params.type){
		case "alert":
			if(params.incipit == ''){
				params.incipit = 'Alert!';	
			}
			var $html = '<div class="alert '+params.class+'"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>'+params.incipit+'</strong> '+msg+'</div>';
			break;
		case "error":
			if(params.incipit == ''){
				params.incipit = 'Error!';	
			}
			var $html = '<div class="alert alert-error '+params.class+'"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>'+params.incipit+'</strong> '+msg+'</div>';
			break;	
		case "success":
			if(params.incipit == ''){
				params.incipit = 'Success!';	
			}
			var $html = '<div class="alert alert-success '+params.class+'"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>'+params.incipit+'</strong> '+msg+'</div>';
			break;	
		case "info":
			if(params.incipit == ''){
				params.incipit = 'Success!';	
			}
			var $html = '<div class="alert alert-info '+params.class+'"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>'+params.incipit+'</strong> '+msg+'</div>';
			break;			
	}
	if(params.when == 'before'){
		$(this).prepend($html);	
	} else if(params.when == 'in'){
		$(this).append($html);	
	} else {
		$(this).after($html);	
	}
	
	//check if we do not want a persistent alert
	if(params.persistent == false){
		//now we find again the alert obj as 
		if(params.local){
			//removes the closest alert to the trigger
			if(params.class != ''){
				$alertObj = $('.'+params.class,$(this).parent());	
			} else {
				$alertObj = $('.alert',$(this).parent());	
			}//end if
		} else {
			//removes all the alerts
			$alertObj = $(".alert");	
		}//end if	
	
		setTimeout(function(){			
			$alertObj.fadeOut('slow',function(){
				$(this).remove();	
			});
		},params.fadeDelay);
	}//end if
	
	if(params.scrollTopOnComplete){
		$(document).scrollTop(0);
	}//end if
}

//load a post data
$.fn.load_post = function(params){
	var $form = $(this);
	params = $.extend({prefix: "", meta: false, ignore: []},params);
	$form.jLoader({
		backdropOpacity: .7,
		backdropColor: '#FFF',
		content: '<span class="loading"><i class="icon-refresh icon-spin"></i> Loading data...</span>'	
	});
	$.ajax({
		url: $.config.apipath + "get.post?id="+params.id+"&module="+params.module+"&meta="+params.meta,
		type: "get",
		dataType:"json",
		scriptCharset: "utf-8",
		timeout: 30000,
		success: function(data){		
			if(data.result == "success"){
				$form.jLoader('closeLoader');
				var $post = data.post;
				$.cmsVars.currentPost = $post;
				$('.permalink').handlePermalink('fillPermalink',$post.post_permalink);
				$form.formdata({data: $post, prefix: params.prefix, meta: params.meta, ignore: params.ignore});
			}
		}
	});
};

//load the passed category data
$.fn.load_category = function(params){
	var $form = $(this);
	params = $.extend({prefix: "", meta: false},params);
	$form.jLoader({
		backdropOpacity: .7,
		backdropColor: '#FFF',
		content: '<span class="loading"><i class="icon-refresh icon-spin"></i> Loading data...</span>'	
	});
	$.ajax({
		url: $.config.apipath + "get.category?id="+params.id+"&module="+params.module+"&meta="+params.meta,
		type: "get",
		dataType:"json",
		scriptCharset: "utf-8",
		timeout: 30000,
		success: function(data){		
			if(data.result == "success"){
				$form.jLoader('closeLoader');
				var $category = data.category;		
				$('.permalink').handlePermalink('fillPermalink',$category.category_permalink);		
				$form.formdata({data: $category, prefix: params.prefix, meta: params.meta});
			}
		}
	});
};

//load a post data
$.fn.load_settings = function(params){
	var $form = $(this);
	params = $.extend({module: ""},params);
	$form.jLoader({
		backdropOpacity: .7,
		backdropColor: '#FFF',
		content: '<span class="loading"><i class="icon-refresh icon-spin"></i> Loading data...</span>'	
	});
	$.ajax({
		url: $.config.apipath + "get.settings?module="+params.module,
		type: "get",
		dataType:"json",
		scriptCharset: "utf-8",
		timeout: 30000,
		success: function(data){		
			if(data.result == "success"){				
				var $settings = data.settings;
				$form.formdata({data: $settings});
			}
			$form.jLoader('closeLoader');
		}
	});
};

//load a post data
$.fn.load_module = function(params){
	var $form = $(this);
	params = $.extend({module_id: 0},params);
	$.ajax({
		url: $.config.apipath + "get.module?module="+params.module_id,
		type: "get",
		dataType:"json",
		scriptCharset: "utf-8",
		timeout: 30000,
		success: function(data){		
			if(data.result == "success"){
				var $module_data = data.module;
				$form.formdata({data: $module_data});
			}
		}
	});
};

//Simple form validation
$.fn.validate = function(){
	var valid = true;
	$("input[required]").each(function(){
		$(this).closest('.control-group').removeClass("error");
		if($(this).is(":checkbox")){
			var className = $(this).prop('class');
			if($("."+className+":checked").length == 0){
				valid = false;	
			}
		} else {
			if($(this).val() == ""){
				$(this).closest('.control-group').addClass("error");
				valid = false;	
			} else if($(this).prop("type") == "email"){
				var validEmail = validateEmail($(this).val());
				if(!validEmail){
					$(this).closest('.control-group').addClass("error");				
					valid = false;			
				}		
			}//end if
		}
	});		
	return valid;
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

//parse the form and load ckeditor when found
$.fn.applyCKEditor = function(){
	var $form = $(this);
	$('textarea[ckeditor="true"], textarea[data-ckeditor="true"]',$form).each(function(){
		var $textarea = $(this);
		if(typeof($(this).data('ckeditor-height')) != 'undefined'){
			var $height = $(this).data('ckeditor-height'); 
		} else {
			var $height = 300;
		}
		
		if(typeof($(this).data('ckeditor-toolbar')) != 'undefined'){
			var $toolbar = $(this).data('ckeditor-toolbar'); 
		} else {
			var $toolbar = 'MessageToolbar';
		}
				
		$textarea.ckeditor({
			//extraPlugins : 'uicolor,customtags',
			width: '98%',
			height: $height,
			toolbar : $toolbar	
		});	
	});
	
	CKEDITOR.on( 'instanceReady', function( event ) {
		event.editor.on( 'focus', function() {				
			$.cmsVars.currentTextEditor = this.name;				
		});
	});
}

$.fn.findEmptyMeta = function(){
	$.cmsVars.emptyMetaCheckboxes = new Array;
	$('input[type="checkbox"][name^="meta"]').each(function(){
		if($(this).not(':checked')){
			$.cmsVars.emptyMetaCheckboxes.push($(this).prop('name'));			
		}
	});
	$('#deleted-meta').remove();
	$deleteMeta = $.cmsVars.emptyMetaCheckboxes;
	$(this).append('<input type="hidden" name="deleted_meta" id="deleted-meta" value="'+$deleteMeta+'">');
}

$.fn.findOptionalFields = function(){
	$("input[data-toggle=optional-field],select[data-toggle=optional-field]").each(function(){
		if($(this).prop('type') != 'checkbox' && $(this).prop('type') != 'radio'){	
			$(this).on('change',function(){
				var $id = $(this).prop('id');
				var $target = $(this);	
				$('div[data-type="template-optional-field"][data-template-target=#'+$id+']').each(function(){
					var $match = $(this).data('template-match');	
					updateTemplateStatus($(this), $target, $match);
				});
			});
		} else {
			$(this).on('click',function(){
				var $id = $(this).prop('id');
				var $target = $(this);	
				$('div[data-type="template-optional-field"][data-template-target=#'+$id+']').each(function(){
					var $match = $(this).data('template-match');	
					updateTemplateStatus($(this), $target, $match);
				});
			});
		}
		
		var $id = $(this).prop('id');
		var $target = $(this);	
		$('div[data-type="template-optional-field"][data-template-target=#'+$id+']').each(function(){
			var $match = $(this).data('template-match');	
			updateTemplateStatus($(this), $target, $match);
		});
	});
	/*
	$("input[data-toggle=optional-field],select[data-toggle=optional-field]").each(function(){
		var $id = $(this).prop('id');
		var $target = $(this);	
		$('div[data-type="template-optional-field"][data-template-target=#'+$id+']').each(function(){
			var $match = $(this).data('template-match');	
			updateTemplateStatus($(this), $target, $match);
		});
	});
	*/
	/*
	//we need to check if the input #template is in the document, otherwise this code would be useless
	$('div[data-type="template-optional-field"]',$(this)).each(function(){				
		var $target = $(this).data('template-target');
		var $match = $(this).data('template-match');		
			
		updateTemplateStatus($(this), $target, $match);	
			
		if($($target).prop('type') != 'checkbox' && $($target).prop('type') != 'radio'){			
			$($target).change(function(){			
				$('div[data-type=template-optional-field]').each(function(){
					updateTemplateStatus($(this), $target, $match);
				});				
			});
		} else {		
			$($target).click(function(){						
				$('div[data-type=template-optional-field]').each(function(){
					updateTemplateStatus($(this), $target, $match);
				});				
			});
		}
	});	
	*/
	function updateTemplateStatus($this,$target, $match){		
		if(typeof($target) !== 'undefined'){	
			//check if match is an array of elements
			if(typeof($match) == 'number'){
				$match = String($match);
			}//end if	
			if($match.indexOf(",") !== -1){
				$match = $match.split(",");
			} else {
				$match = new Array($match);	
			}//end if
			var $found = false;
			if($($target).prop('type') == 'checkbox'){			
				if($($target).is(':checked')){
					$found = true;
				}//end if
			} else {					
				if($match.indexOf($($target).val()) != -1){
					$found = true;
				}//end if
			}//end if		
			if($found){
				$this.show();
				$this.find('textarea, input').prop('disabled',false);
			} else {
				$this.hide();
				$this.find('textarea, input').prop('disabled',true);
			}
		}	
	}
	
}

$.fn.saveTableSorting = function(options){
	var params = $.extend({
				page: 1,
				itemsPerPage: 10,
				parentId : 0,
				totalItems : 1			
			},options);
			
	var $form = $(this);
	
	$.ajax({
		url : $.config.apipath+'items.save.sorting',
		data: $form.serialize(),
		dataType:"json",
		type: "post",
		timeout: 30000,
		success: function(data){
			if(data.result == "success"){
				$form.popMessage("Sorting saved",{type: "success"});
			} else {
				$form.popMessage("Cannot save the sorting",{type: "error"});
			}
		}
	});
}

$.fn.savePostAttachmentsSorting = function(options){
	var params = $.extend({},options);
			
	var $form = $(this);
	
	if($('#attachment-post-type').val() == 'category'){
		var $post_data = $form.serialize()+'&module='+$.config.module+'&post_id='+$.cmsVars.category_id+"&attachment_post_type=category";
	} else {	
		var $post_data = $form.serialize()+'&module='+$.config.module+'&post_id='+$.cmsVars.post_id;
	}
	
	$.ajax({
		url : $.config.apipath+'post.attachments.save.sorting',
		data: $post_data,
		dataType:"json",
		type: "post",
		timeout: 30000,
		success: function(data){
			if(data.result == "success"){
				$form.popMessage("Sorting saved",{type: "success"});
			} else {
				$form.popMessage("Cannot save the sorting",{type: "error"});
			}
		}
	});
}

$.fn.saveModuleSorting = function(options){
	var params = $.extend({},options);
			
	var $form = $(this);
		
	var $post_data = $form.serialize();	
	
	$.ajax({
		url : $.config.apipath+'modules.group.save.sorting',
		data: $post_data,
		dataType:"json",
		type: "post",
		timeout: 30000,
		success: function(data){
			if(data.result == "success"){
				$form.popMessage("Sorting saved",{type: "success"});
			} else {
				$form.popMessage("Cannot save the sorting",{type: "error"});
			}
		}
	});
}

$.fn.callSidebar = function(params){
	params = $.extend({
				target: "#",
				position: "after",
				collapsedClass: "",
				expandedClass: "",
				sidebarContainer: "",
				form: ""
			},params);
			
	var $trigger = $(this);
	var $target = $(params.target);
	
	$trigger.click(function(e){
		e.preventDefault();
		
		if($target.hasClass(params.expandedClass)){
			$target.removeClass(params.expandedClass);
			$target.addClass(params.collapsedClass);
			$(params.sidebarContainer).removeClass("sidebar-collapsed");
			$(params.sidebarContainer).addClass("sidebar-expanded");	
			$(params.form).autoSave();	
		} else {
			$target.removeClass(params.collapsedClass);
			$target.addClass(params.expandedClass);
			$(params.sidebarContainer).addClass("sidebar-collapsed");
			$(params.sidebarContainer).removeClass("sidebar-expanded");	
		}
	});
}