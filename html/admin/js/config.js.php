<?php
	header("content-type: application/x-javascript");
	include("../config.inc.php");
	$protocol = 'http://';
	if(__FORCE_SSL__){
		$protocol = 'https://';
	}//end if
	define("__PROTOCOL__",$protocol);
?>

    var $settings = {};
    $settings.basepath = '<?php echo __PROTOCOL__.$_SERVER["HTTP_HOST"].__SERVERPATH__ ?>';
    $settings.apipath = '<?php echo __CMS_PATH__ ?>api/';
    $settings.permalinkPathPattern = '{#basepath#}{#module#}/';
<?php
	if(isset($_GET['m']) && !is_null($_GET['m']) && strlen($_GET['m']) > 0){
		echo '$settings.module = "'.str_replace(".","-",$_GET['m']).'";
		';
	}//end if
	
	if(isset($_GET['a']) && !is_null($_GET['a']) && strlen($_GET['a']) > 0){
		echo '$settings.action = "'.str_replace(".","-",$_GET['a']).'";
		';
	}//end if
?>

//set the module and action as a class in the body element
$().ready(function(){
<?php
	if(isset($_GET['m']) && !is_null($_GET['m']) && strlen($_GET['m']) > 0){
		echo '$("body").addClass("module-'.str_replace(".","-",$_GET['m']).'");
		';
	}//end if
	
	if(isset($_GET['a']) && !is_null($_GET['a']) && strlen($_GET['a']) > 0){
		echo '$("body").addClass("action-'.str_replace(".","-",$_GET['a']).'");
		';
	}//end if
?>
});