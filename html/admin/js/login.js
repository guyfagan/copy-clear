// JavaScript Document

$().ready(function() {
    $("#login-form").submit(function(e){
		e.preventDefault();	
		$.ajax({
			url: 'api/login',
			type: 'POST',
			dataType: 'json',
			data: $(this).serialize(),
			timeout: 30000,
			success: function(data){						
				if(data.result == "pass"){
					login_granted();					
				} else {
					show_login_error();
				}				
			},
			complete: function(data, xhr){
				
			},
			error: function(request, status, err) {			
				show_login_error();
			}
		});
	});
	
	function show_login_error(){
		if($(".alert").length == 0){
			$("#login").prepend('<div class="alert alert-error"> \
    <button type="button" class="close" data-dismiss="alert">×</button> \
    <strong>Error!</strong> Login failed, please try again \
    </div>');	
			$(".alert").hide();
			$(".alert").fadeIn("slow");
			setTimeout(function(){ $('.alert').slideUp('slow',function(){$(this).remove()}); }, 10000);
		}
		
	}
	
	function login_granted(){	
		document.location.href = "dashboard/";		
	}
});