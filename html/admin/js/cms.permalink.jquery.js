/************************************************************/
/*	jQuery permalink 1.0 - 23/12/12							*/
/*															*/
/*	UPDATES													*/
/*	23/12/12	- 	Plug-in created							*/
/*															*/
/************************************************************/
$().ready(function(){
	(function( $ ){	
		var $permaMemory = "";
		var $perma;
		var $params;
		
		var methods = {
			init : function( options ) {
				params = $.extend({
					input: "#title",
					path: "",
					saveTo: "#permalink",
					checkDb: true,
					defaultExt: '.html',
					apiCall: "post.check.permalink",
					mirrorTarget : false,
					append : ''
				},options);				
				
				$perma = $(this);
				$params = params;
			
				var $permaPath = $('<span class="perma-container" />').html(params.path);
				$perma.append($permaPath);
				var $permaEdit = $('<span class="perma-edit" />');
				$perma.append($permaEdit);
				$perma.append(params.defaultExt+" ");
				
				$permaEdit.html($(params.saveTo).val());
				
				if(params.mirrorTarget == false){
					var $btnGroup = $('<div class="btn-group"></div>');
					$perma.append($btnGroup);
					var $permaEditBtn = $('<button type="button" class="btn btn-mini btn-edit-permalink">Edit</button>');
					var $permaUpdateBtn = $('<button type="button" class="btn btn-mini btn-update-permalink">Update</button>');
					$perma.find('.btn-group').append($permaUpdateBtn);
					$perma.find('.btn-group').append($permaEditBtn);
					$('.btn-edit-permalink',$perma).click(function(e){
						e.preventDefault();	
						$permaMemory = $('.perma-edit',$perma).text();
						$('.btn-edit-permalink',$perma).hide();
						var $html = '<input type="text" class="input-permalink" id="permalink-input-edit" value="'+$permaMemory+'" name="permalinkInputEdit" />';
						var $buttons = '<span class="perma-buttons"><button type="button" class="btn btn-mini" data-role="confirm-permalink">OK</button>\
						<a href="#" class="btn btn-link btn-mini" data-role="cancel-permalink">Cancel</a></span>';
						$('.perma-edit',$perma).html($html);
						$perma.append($buttons);
						//if the user choose to cancel the edit, bring back everything to the previous state
						$('a[data-role=cancel-permalink]',$perma).unbind("click");
						$('a[data-role=cancel-permalink]',$perma).click(function(e){
							e.preventDefault();	
							methods.cancelEdit();
						});
						//here we listen the confirm permalink button actions
						$('button[data-role=confirm-permalink]',$perma).unbind("click");
						$('button[data-role=confirm-permalink]',$perma).click(function(e){
							e.preventDefault();	
							methods.updatePermalink($("#permalink-input-edit",$perma).val());			
						});
					});
					
					$('.btn-update-permalink',$perma).click(function(e){
						e.preventDefault();	
						if($params.append != ''){
							methods.updatePermalink($(params.input).val()+'-'+$params.append);	
						} else {
							methods.updatePermalink($(params.input).val());	
						}
					});
					
					//here we listen to the title input
					$(params.input).on("blur",function(){
						if($('.perma-edit',$perma).text() == "" && $(this).val() != ""){
							if($params.append != ''){
								methods.updatePermalink($(this).val()+'-'+$params.append);	
							} else {
								methods.updatePermalink($(this).val());	
							}
						}
					});
				} else {
					$(params.input).on("blur",function(){
						if($params.append != ''){
							methods.updatePermalink($(this).val()+'-'+$params.append);	
						} else {
							methods.updatePermalink($(this).val());	
						}
					});
				}
			},
			
			updatePermalink : function(newPermalink){
				postData = "permalink="+encodeURIComponent(newPermalink);
				if($("#id").val() != "" && typeof($("#id").val()) != 'undefined'){
					postData += "&id="+$("#id").val();	
				}
					
				if($params.checkDb == true){
					$.ajax({
						url : $.config.apipath+$params.apiCall,
						type: "post",
						dataType:"json",
						data: postData,
						success: function(data){							
							if(data.result == "success"){
								$permaMemory = data.permalink;
								$($params.saveTo).val($permaMemory);
								methods.cancelEdit();
							}
								
						}
					});
				} else {
					$permaMemory = newPermalink;
					$($params.saveTo).val($permaMemory);
					methods.cancelEdit();
				}
			},
			
			fillPermalink : function( text ){
				$(this).find('.perma-edit').text(text);
				$($params.saveTo).val(text);
				$permaMemory = text;
			},
			
			cancelEdit : function(){
				$('.perma-buttons',$perma).remove();
				$('.input-permalink',$perma).remove();
				$('.perma-edit',$perma).text($permaMemory);
				$('.btn-edit-permalink',$perma).show();
			}
		}
		
		$.fn.handlePermalink = function( method ) {    
			if ( methods[method] ) {
			  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
			  return methods.init.apply( this, arguments );
			} else {
			  $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
			}      
		};
	})( jQuery );

});