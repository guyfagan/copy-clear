<?php
	$category_id = $_GET['id'];
	$params = array();
	$new_post_link = "/new/";
	$new_category_link = "/category/new/";
	$has_father_id = false;
	$father_id = 0;
	if(!is_null($category_id) && is_numeric($category_id)){		
		$has_father_id = true;	
		$params['category_id'] = $category_id;	
		$father_id = $category_id;
	}//end if
	$settings = $utils->view->get_module_settings();
	$show_thumbs = false;
	if(isset($settings['show_thumbnails']) && (bool)$settings['show_thumbnails']){
		$params['default_photo'] = true;
		$show_thumbs = true;
	}//end if

	$list = ${$module}->get_items($params);
?>
<form action="#" name="sort-form" id="sort-form" method="post">
	<input type="hidden" name="page" id="page" value="1" />
    <input type="hidden" name="total-items" id="total-items" value="<?php echo sizeof($list) ?>" />
    <input type="hidden" name="items-per-page" id="items-per-page" value="<?php echo sizeof($list) ?>" />
    <input type="hidden" name="parent-id" id="parent-id" value="<?php echo $father_id ?>" />
    <input type="hidden" name="module" id="module" value="<?php echo $module ?>" />
    <h4>Sorting tool</h4>
    <p>To sort the elements just drag them in the position you desire, bear in mind that the sort order start in a natural way, so from the top left to the bottom right</p>
    <div class="sort-tool-container clearfix">
<?php
	if($list !== false){
		echo '<ul>';
		$x = 0;
		for($i = 0; $i < sizeof($list); $i++){
			$x++;
			//Switcher
			$switcher = "on";		
			if($list[$i]['status'] == "0"){
				$switcher = "off";
			}//end if
			$switch_type = "post";
			$id = $list[$i]['id'];
			$is_category = (bool)$list[$i]['is_category'];
			//Vars in case of Post
			$link_edit = __MODULE_PATH__."/edit/".$list[$i]['id'];
			//if we are showing the content of a category, we add the category_id in the edit link
			if(!$is_category && !is_null($category_id)){
				$link_edit .= "/in/".$category_id;	
			}//end if
			$link_delete = __CMS_PATH__."/api/delete.post?id=".$list[$i]['id']."&module=".$module;
			$label = $list[$i]['label'];		
			$move_path = "/move/post/".$list[$i]['id'];
			if($is_category){				
				$link_edit = __MODULE_PATH__."/category/edit/".$list[$i]['id'];	
				if(!is_null($category_id)){
					$link_edit .= "/in/".$category_id;
				}//end if
				$link_delete = __CMS_PATH__."/api/delete.category?id=".$list[$i]['id']."&module=".$module;				
				$switch_type = "category";
			}//end if
			$li_type = "post with-tooltip";			
			if($is_category){
				$li_type = "category";	
			}//end if
			$has_thumb = false;
			if(isset($list[$i]['photo']) && $show_thumbs){
				$has_thumb = true;
				
				$thumb = '<img src="'.__SERVERPATH__.'upload/photos/t/'.$list[$i]['photo']['attachment_filename'].'" alt="'.$label.'" class="list-thumb-icon" />';
			}//end if
	?>
    	<li class="<?php echo $li_type ?>">
        	<a href="#" data-toggle="tooltip" title="<?php echo $label ?>">
                <span class="badge"><?php echo $x ?></span>
            <?php
                if($has_thumb){
					echo $thumb;	
				}//end if
				if($is_category){
					echo '<i class="icon-folder-open icon-2x"></i> '.$label;
				} else if(!$is_category && !$has_thumb){
					echo '<i class="icon-file-alt icon-2x"></i> <span class="text">'.$utils->trunktext($label,15).'</span>';
				}//end if
            ?>        
                <input type="hidden" name="table-row-id[]" id="table-row-id-<?php echo $id ?>" value="<?php echo $id ?>" />
                <input type="hidden" name="table-row-type[]" id="table-row-type-<?php echo $id ?>" value="<?php echo (int)$is_category ?>" />
            </a>
        </li>
    <?php
		}//end for
		echo '</ul>';
	}//end if
?>
	</div>
	<button class="btn btn-primary" id="save-sorting-btn">Save Sorting</button>
</form>