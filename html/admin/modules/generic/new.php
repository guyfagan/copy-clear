<?php
	$id = $_REQUEST['id'];	
	$has_category = false;
	if(!is_null($_REQUEST['parent_id'])){
		$has_category = true;
		$category_id = $_REQUEST['parent_id'];
	}//end if
	$edit_mode = false;
	$page_title = "New Post";
	$post_status = 0;
	if(!is_null($id)){
		$post = ${$module}->get_post(array('id' => $id, 'meta' => true));
		$post_status = $post['post_status'];
		$edit_mode = true;	
		$page_title = "Edit Post";
		if($_GET['revision_id'] !== NULL){
			$revision_mode = true;
			$tools = $utils->call("tools");
			$post_revision = $tools->get_post_revision(array('module_id' => $module, 'post_id' => $id, 'id' => $_GET['revision_id'], 'full_details' => true));
			$post = $post_revision['post_revision_data'];
			echo '<div class="alert alert-warning"><strong>Warning!</strong> You are watching a previous saved version of this post, by clicking save you will override the current content</div>';
		}//end if
	}//end if
?>
<div id="form-wrapper" class="clearfix">
    <h4><?php echo $page_title ?></h4>    
    <form action="<?php echo __CMS_PATH__ ?>api/save.post" method="post" name="save-post" id="save-post" enctype="multipart/form-data" class="clearfix">
    	<div class="compressed-form">
            <input type="hidden" name="module" id="module" value="<?php echo $module ?>" />  
            <input type="hidden" name="attachment_post_type" id="attachment-post-type" value="post" />         
            <?php
            if($edit_mode){
            ?>
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
            <?php
            }//end if
            if($has_category){
				#This has been disabled has the category will be  in the sidebar now
				/*				
            ?>
            <input type="hidden" name="category_id" id="category_id" value="<?php echo $category_id ?>" />
            <?php
				*/
            }//end if
            ?>
            <fieldset>
                <div class="control-group">
                    <div class="input-append">
                        <label>Date</label>
                        <input type="text" name="date" id="date" class="input-medium date-pick"<?php
						if(isset($post)){
							echo ' value="'.date('d/m/Y',$post['post_date']).'"';	
						}
						?> />
                        <span class="add-on"><i class="icon-calendar"></i></span>
                    </div>
                </div>
                <div class="control-group">
                    <label>Title</label>
                    <input type="text" name="title" id="title" class="input-block-level" placeholder="Title" required />
                    <input type="hidden" name="permalink" id="permalink" />
                    <div class="permalink"></div>
                </div>
                <label>Subtitle</label>
                <input type="text" name="subtitle" id="subtitle" class="input-block-level" placeholder="Subtitle" />
                <!--label>Abstract</label>
                <textarea name="abstract" id="abstract" class="input-block-level" placeholder="Abstract"></textarea-->
                <div class="control-group">
                    <label>Message</label>
                    <div class="form-toolbar">
                        <a href="#mediaModal" role="button" data-toggle="modal" data-textarea="message" class="btn btn-mini media-btn"><i class="icon-picture"></i> Upload Media</a>
                        <a href="#" id="sidebar-btn" class="btn btn-mini"><i class="icon-bookmark"></i> Sidebar</a>
                    </div>
                    <textarea name="message" id="message" class="input-block-level" data-ckeditor="true">
                    <?php
					if($edit_mode == true){
						echo $post['post_message'];
					}//end if
					?>
                    </textarea>
                </div>   
                <?php
				if($utils->view->settings['enableTemplates'] == 1){
					require_once("inc/components/templates/templates.php");
				}//end if
				?>                
            </fieldset>
            <div class="clearfix">
            	<button type="submit" id="submit-form" class="btn btn-primary">Save</button>
                <button type="button" id="save-and-close" class="btn">Save &amp; Close</button>
                <a hrer="#" class="btn" data-action="cancel">Cancel</a>  
                <?php				
				if($utils->view->module_settings['enablePostPreview'] == 1){
				?>  
                <a href="#" class="btn pull-right" data-action="preview" data-preview-height="<?php echo $utils->view->module_settings['previewIframeHeight'] ?>" data-preview-url="<?php echo $utils->view->module_settings['previewPath'] ?>">Preview</a> 
                <?php
				}//end if
				?>      
            </div>
        </div>
        <?php
		include("inc/components/sidebar.php");
		?>
    </form>
    <?php        
		include("inc/components/media/media.php");
		include("inc/components/categories/categories.php");
		if($utils->view->module_settings['enablePostPreview'] == 1){
			include("inc/components/previews/previews.php");
		}//end if
    ?>
</div>