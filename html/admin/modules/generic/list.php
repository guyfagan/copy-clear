<?php
	$category_id = $_GET['id'];
	$params = array();
	$new_post_link = "/new/";
	$new_category_link = "/category/new/";
	$has_father_id = false;
	$father_id = 0;
	if(!is_null($category_id) && is_numeric($category_id)){
		$new_post_link = "/new/in/".$category_id;
		$has_father_id = true;
		$new_category_link = "/category/new/in/".$category_id;
		$params['category_id'] = $category_id;	
		$father_id = $category_id;
	}//end if
	
	define("__NEW_CATEGORY_LABEL__","New Category");
	if(!isset($utils->view->config['new_post_label'])){
		define("__NEW_POST_LABEL__","New Post");	
	} else {
		define("__NEW_POST_LABEL__",$utils->view->config['new_post_label']);
	}//end if
		
	$settings = $utils->view->get_module_settings();
	$show_thumbs = false;
	if(isset($settings['show_thumbnails']) && (bool)$settings['show_thumbnails']){
		$params['default_photo'] = true;
		$show_thumbs = true;
	}//end if
	$elements_per_page = 25;
	if(isset($settings['items_per_page']) && is_numeric($settings['items_per_page'])){
		$elements_per_page = $settings['items_per_page'];
	}//end if
	
	$list_system = 'natural';
	if(isset($settings['list_system'])){
		$list_system = $settings['list_system'];
	}//end if
	
	if($list_system == 'date'){
		$params['sort_by'] = 'item_date';
	}//end if
	
	if(isset($_REQUEST['search']) && trim($_REQUEST['search']) != ""){
		$params['search'] = $_REQUEST['search'];
	}//end if
	
	if(isset($_REQUEST['status'])){
		$params['status'] = $_REQUEST['status'];
	}//end if
		
	if(isset($_REQUEST['fromdate']) && $_REQUEST['fromdate'] != ''){
		$params['from_date'] = $utils->convert_to_db_date($_REQUEST['fromdate']);	
	}//end if
	
	if(isset($_REQUEST['todate']) && $_REQUEST['todate'] != ''){
		$params['to_date'] = $utils->convert_to_db_date($_REQUEST['todate']);	
	}//end if
	
	if(!$is_admin){
		switch($settings['usersActionsAllowed']){
			case "himself-plus":
				$params['user_id'] = $users->get_uid();
				if(isset($settings['usersShowableGroups']) && !is_null($settings['usersShowableGroups'])){				
					$params['group_id'] = explode(',',$settings['usersShowableGroups']);
				}//end if
				if(isset($settings['usersShowableGroupsOnlyPublished']) && !is_null($settings['usersShowableGroupsOnlyPublished'])){
					$params['group_status'] = 1;
				}//end if
				break;
			case "only-himself":
				$params['user_id'] = $users->get_uid();
				break;
			case "same-group":
				$params['group_id'] = $user_data['user_group_id'];
				break;
			default:
			case "all":
				break;	
		}//end switch
		
		$can_post = true;
		if(isset($settings['usersOnlyAdminCanWrite'])){
			if($settings['usersOnlyAdminCanWrite'] == 1){
				$can_post = false;
				$post_allowed_groups = explode(',',$settings['usersWriteGroups']);
				if(in_array($user_data['user_group_id'],$post_allowed_groups)){
					$can_post = true;	
				}//end if
			}//end if
		}//end if
	} else {
		$can_post = true;
	}//end if
	
	$view_mode = "default";
	if(isset($settings['list_view_mode'])){
		$view_mode = $settings['list_view_mode'];		
	}//end if
	
	$paginated_results = false;
	if($elements_per_page > 0){
		$paginated_results = true;
		$utils->create_paging($_GET['page'],$elements_per_page);
	}//end if
	switch($view_mode){
		case "default":
		default:
			$list = ${$module}->get_items($params);
		break;
		case "wp-like":
			$list = ${$module}->get_posts($params);
		break;
	}//end switch
	
	# PAGINATION #
	if($paginated_results){
		$paging_stats = $utils->get_paging_stats();	
		$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
		//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
		$utils->unset_paging();
		$page_start = $paging_stats['start']+1;
		$page_end = $paging_stats['start']+10;
		if($page_end > $paging_stats['founds']){
			$page_end = $paging_stats['founds'];
		}//end if
		$page_total = $paging_stats['pages'];
		$page_current = $paging_stats['current_page'];
		$page_prev = $page_current-1;
		if($page_prev == "0"){
			$page_prev = 1;
		}//end if
		$page_next = $page_current+1;
		if($page_next > $page_total){
			$page_next = $page_total;
		}//end if
	}//end if
	# END PAGINATION #
	
	
	$table_type = "table-large-list";
	if(isset($settings['list_view_type'])){
		if($settings['list_view_type'] == "small"){
			$table_type = "table-small-list";
		}//end if
	}//end if
	
	$bulk_actions = false;
	if($settings['enable_bulk_actions'] == 1){
		$bulk_actions = true;	
	}//end if
	
	if(!$can_post){
		$bulk_actions = false;	
	}//end if
		
?>
<form action="#" name="table-form" id="table-form" method="post">
	<input type="hidden" name="page" id="page" value="<?php echo $page_current ?>" />
    <input type="hidden" name="total-items" id="total-items" value="<?php echo $paging_stats['founds'] ?>" />
    <input type="hidden" name="items-per-page" id="items-per-page" value="<?php echo $elements_per_page ?>" />
    <input type="hidden" name="parent-id" id="parent-id" value="<?php echo $father_id ?>" />
    <input type="hidden" name="module" id="module" value="<?php echo $module ?>" />
    <?php
	if(isset($_REQUEST['search']) && trim($_REQUEST['search']) != ""){
	?>
    <div class="alert alert-success clearfix"><i class="icon icon-find"></i> Search results for &quot;<strong><?php echo $_REQUEST['search'] ?></strong>&quot; <a href="<?php echo __MODULE_PATH__ ?>/list/" class="btn btn-link pull-right"><i class="icon icon-trash"></i> Clear Search</a></div>
    <?php
	}//end if
	if($settings['show_refine_toolbar'] == 1){
	?>
    <div class="well well-small">
        <div class="form-inline">
            <label>From Date</label>
            <div class="input-append date">
                <input type="text" name="fromdate" id="fromdate" class="input-small" value="<?php echo $_REQUEST['fromdate'] ?>" /><span class="add-on"><i class="icon-calendar"></i></span>
            </div>
            <label>To Date</label>
            <div class="input-append date">
                <input type="text" name="todate" id="todate" class="input-small" value="<?php echo $_REQUEST['todate'] ?>" /><span class="add-on"><i class="icon-calendar"></i></span>
            </div>  
            <label>Status</label>
            <select name="status" id="status" class="input-medium">
                <option value=""<?php if(!isset($params['status'])){ echo ' selected'; } ?>>Any</option>
                <option value="1"<?php if(isset($params['status']) && $params['status'] == '1'){ echo ' selected'; } ?>>Published</option>
                <option value="0"<?php if(isset($params['status']) && $params['status'] == '0'){ echo ' selected'; } ?>>Draft</option>
            </select>
            <button type="submit" class="btn btn-primary"><i class="icon icon-search"></i> Refine results</button>   
        </div>
    </div>
    <br />
    <?php
	}//end if
	switch($view_mode){
		case "default":
		default:
	?>
        <table class="table table-striped table-hover <?php echo $table_type ?>">
            <thead>
                <tr>
                <?php
				if($bulk_actions){
				?>
                	<td width="20">
                    	<input type="checkbox" value="1" name="check-all" id="check-all" />
                    </td>
                <?php
				}//end if
				?>
                    <th>Title</th>         
                    <th>Status</th>
                    <td>
                        <div class="btn-group pull-right" id="table-view-type">
                            <a href="#" data-table-type="large-list" class="btn btn-mini<?php if($table_type == "table-large-list"){ echo ' active'; } ?>"><i class="icon-th-list"></i></a>
                            <a href="#" data-table-type="small-list" class="btn btn-mini<?php if($table_type == "table-small-list"){ echo ' active'; } ?>"><i class="icon-list"></i></a>
                        </div>
                    </td>
                </tr>
            </thead>
            <?php
            if($paginated_results){
                if($page_total > 1){
                ?>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <div class="pagination">
                                <ul>
                                    <li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
                                    <?php
                                    if(is_array($paging_list) && sizeof($paging_list) > 0){
                                        for($i = 0; $i < sizeof($paging_list); $i++){
                                            echo '<li';
                                            if($paging_list[$i]['page'] == $page_current){
                                                echo ' class="active"';	
                                            }//end if
                                            echo '><a href="'.$paging_list[$i]['url'].'">'.$paging_list[$i]['page'].'</a></li>';
                                        }//end for i
                                    }//end if
                                    ?>                      
                                    <li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
                <?php
                }//end if
            }//end if
            ?>
            <tbody>
                <?php
                if($list !== false){
                    for($i = 0; $i < sizeof($list); $i++){
                    //Switcher
                    $switcher = "on";	
                    $switch_label = "Published";	
                    $switch_label_class = "label label-success";
                    if($list[$i]['status'] == "0"){
                        $switcher = "off";
                        $switch_label = "Draft";
                        $switch_label_class = "label label-important";
                    }//end if
                    $switch_type = "post";
                    $id = $list[$i]['id'];
                    $is_category = (bool)$list[$i]['is_category'];
                    //Vars in case of Post
                    $link_edit = __MODULE_PATH__."/edit/".$list[$i]['id'];
                    //if we are showing the content of a category, we add the category_id in the edit link
                    if(!$is_category && !is_null($category_id)){
                        $link_edit .= "/in/".$category_id;	
                    }//end if
                    $link_delete = __CMS_PATH__."/api/delete.post?id=".$list[$i]['id']."&module=".$module;
                    $label = $list[$i]['label'];
                    $icon = "icon-file";
                    $move_path = "/move/post/".$list[$i]['id'];
                    if($is_category){
                        $move_path = "/move/category/".$list[$i]['id'];
                        //Vars in case of Category
                        $label = '<a href="'.__MODULE_PATH__.'/list/'.$list[$i]['id'].'">'.$label.'</a>';
                        $link_edit = __MODULE_PATH__."/category/edit/".$list[$i]['id'];	
                        if(!is_null($category_id)){
                            $link_edit .= "/in/".$category_id;
                        }//end if
                        $link_delete = __CMS_PATH__."/api/delete.category?id=".$list[$i]['id']."&module=".$module;
                        $icon = "icon-th-list";
                        $switch_type = "category";
                    }//end if
        			$can_publish = false;
					if($is_category){
						$can_publish = true;
					} else {								
						if($perms[$module] > 3 || ($perms[$module] >= 3 && ${$module}->is_post_owner($user_data['user_id'],$list[$i]['id']))){
							$can_publish = true;	
						}//end if
					}//end if
                ?>
                <tr class="table-row"> 
                	<?php
                    if($bulk_actions && !$is_category){
                    ?> 
                	<td><input type="checkbox" value="<?php echo $id ?>" name="ids[]" id="id-<?php echo $id ?>" /></td>    
                    <?php
					} else if($bulk_actions && $is_category){
						echo '<td>&nbsp;</td>';	
					}//end if
					?>    	           	
                    <td>
                        <?php
                        if(isset($list[$i]['photo']) && $show_thumbs){
                            echo '<img src="'.__SERVERPATH__.'upload/photos/t/'.$list[$i]['photo']['attachment_filename'].'" alt="'.$list[$i]['label'].'" class="list-thumb-icon" />';
                        }//end if
                        ?>
                        <i class="<?php echo $icon ?>"></i> <?php 
                        echo '<span id="label-'.$id.'">'.$label;
                        if(!$is_category){
                            if($list[$i]['type'] !== 'post'){						
                                echo ' <em class="label">'.ucwords($list[$i]['type']).'</em>';							
                            }//end if
                        }//end if
                        echo '</span>'; 
                        if(!$is_category){
                            //show the post date
                            echo '<span class="table-info-line muted"><i class="icon-calendar"></i> '.date('d/m/Y',$list[$i]['item_date']).'</span>';
                            //show the user
                            echo '<span class="table-info-line muted"><i class="icon-user"></i> created by <strong>'.$list[$i]['user_username'].'</strong></span>';
                        }//end if
                        ?>
                    </td> 
                    <?php
                    if($can_publish){
                    ?>   
                    <td class="table-row-status"><a href="#" data-id="status_<?php echo $id ?>" data-type="<?php echo $switch_type ?>" class="switcher"><span class="switch <?php echo $switcher ?>"><?php echo $status ?></span></a></td> 
                    <?php
                    } else {
                    ?>   
                    <td class="table-row-status"><div class="<?php echo $switch_label_class ?>"><?php echo $switch_label ?></div></td>
                    <?php
                    }//end if
                    ?>
                    <td class="table-row-options">
                        <input type="hidden" name="table-row-id[]" id="table-row-id-<?php echo $id ?>" value="<?php echo $id ?>" />
                        <input type="hidden" name="table-row-type[]" id="table-row-type-<?php echo $id ?>" value="<?php echo (int)$is_category ?>" />
                        <div class="btn-group pull-right table-button-cta">
                            <?php
                            if($perms[$module] > 3 || ($perms[$module] <= 3 && ${$module}->is_post_owner($users->get_uid(),$id))){
                            ?>
                            <a href="<?php echo $link_edit ?>" class="btn"><i class="icon-edit"></i></a>
                            <?php
								if($list_system == 'natural'){
							?>
                            <a href="#" class="btn table-row-handle"><i class="icon-move"></i></a>
                            <?php
								}//end if
							?>
                            <a href="<?php echo $link_delete ?>" data-action="delete" data-ref-id="<?php echo $id ?>" class="btn btn-warning"><i class="icon-trash"></i></a>                        
                            <?php
								if($is_category){
							?>
                            <button class="btn dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <!--li><a href="<?php echo __CMS_PATH__.$module.$move_path ?>">Move this element</a></li-->                                
                                <li><a href="<?php echo __MODULE_PATH__ ?>/sort/tool/<?php echo $id ?>/">Sort the content of this category</a></li>
                            </ul>
                            <?php
								}//end if
                            }//end if
                            ?>
                        </div>            	
                    </td>
                </tr>
                <?php		
                    }//end for i
                }//end if
                ?>
            </tbody>
        </table>
    <?php
		break;
		case "wp-like":
	?>
    	<table class="table table-striped table-hover <?php echo $table_type ?>">
            <thead>
                <tr>
                    <th>Title</th> 
                    <th>Categories</th>        
                    <th>Status</th>
                    <td>
                        <div class="btn-group pull-right" id="table-view-type">
                            <a href="#" data-table-type="large-list" class="btn btn-mini<?php if($table_type == "table-large-list"){ echo ' active'; } ?>"><i class="icon-th-list"></i></a>
                            <a href="#" data-table-type="small-list" class="btn btn-mini<?php if($table_type == "table-small-list"){ echo ' active'; } ?>"><i class="icon-list"></i></a>
                        </div>
                    </td>
                </tr>
            </thead>
            <?php
            if($paginated_results){
                if($page_total > 1){
                ?>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <div class="pagination">
                                <ul>
                                    <li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
                                    <?php
                                    if(is_array($paging_list) && sizeof($paging_list) > 0){
                                        for($i = 0; $i < sizeof($paging_list); $i++){
                                            echo '<li';
                                            if($paging_list[$i]['page'] == $page_current){
                                                echo ' class="active"';	
                                            }//end if
                                            echo '><a href="'.$paging_list[$i]['url'].'">'.$paging_list[$i]['page'].'</a></li>';
                                        }//end for i
                                    }//end if
                                    ?>                      
                                    <li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
                <?php
                }//end if
            }//end if
            ?>
            <tbody>
                <?php
                if($list !== false){
                    for($i = 0; $i < sizeof($list); $i++){
                    //Switcher
                    $switcher = "on";	
                    $switch_label = "Published";	
                    $switch_label_class = "label label-success";
                    if($list[$i]['post_status'] == "0"){
                        $switcher = "off";
                        $switch_label = "Draft";
                        $switch_label_class = "label label-important";
                    }//end if
                    $switch_type = "post";
                    $id = $list[$i]['post_id'];
                    $is_category = false;
                    //Vars in case of Post
                    $link_edit = __MODULE_PATH__."/edit/".$id;
                    //if we are showing the content of a category, we add the category_id in the edit link
                    if(!$is_category && !is_null($category_id)){
                        $link_edit .= "/in/".$category_id;	
                    }//end if
                    $link_delete = __CMS_PATH__."/api/delete.post?id=".$id."&module=".$module;
                    $label = $list[$i]['post_title'];
                    $icon = "icon-file";
                    $move_path = "/move/post/".$list[$i]['post_id'];
					$post_categories = ${$module}->get_post_categories(array('current_module' => true, 'id' => $id));
                          
                ?>
                <tr class="table-row">            	
                    <td>
                        <?php
                        if(isset($list[$i]['photo']) && $show_thumbs){
                            echo '<img src="'.__SERVERPATH__.'upload/photos/t/'.$list[$i]['photo']['attachment_filename'].'" alt="'.$label.'" class="list-thumb-icon" />';
                        }//end if
                        ?>
                        <i class="<?php echo $icon ?>"></i> <?php 
                        echo '<span id="label-'.$id.'">'.$label;                       
						if($list[$i]['post_type'] !== 'post'){						
							echo ' <em class="label">'.ucwords($list[$i]['post_type']).'</em>';							
						}//end if                      
                        echo '</span>';                        
						//show the post date
						echo '<span class="table-info-line muted"><i class="icon-time"></i>'.date('d/m/Y',$list[$i]['post_date']).'</span>';
						//show the user
						echo '<span class="table-info-line muted"><i class="icon-user"></i> created by <strong>'.$list[$i]['user_username'].'</strong></span>';
                        ?>
                    </td> 
                    <td>
                    <?php
					if($post_categories !== false){
						$cats = array();
						for($x = 0; $x < sizeof($post_categories); $x++){							
							array_push($cats,'<span class="label">'.$post_categories[$x]['category_name'].'</span>');	
						}
						echo implode(", ",$cats);						
					} else {
						echo '&nbsp;';
					}//end if
					?>
                    </td>
                    <?php
                    if($perms[$module] > 3){
                    ?>   
                    <td class="table-row-status"><a href="#" data-id="status_<?php echo $id ?>" data-type="<?php echo $switch_type ?>" class="switcher"><span class="switch <?php echo $switcher ?>"><?php echo $status ?></span></a></td> 
                    <?php
                    } else {
                    ?>   
                    <td class="table-row-status"><div class="<?php echo $switch_label_class ?>"><?php echo $switch_label ?></div></td>
                    <?php
                    }//end if
                    ?>
                    <td class="table-row-options">
                        <input type="hidden" name="table-row-id[]" id="table-row-id-<?php echo $id ?>" value="<?php echo $id ?>" />
                        <input type="hidden" name="table-row-type[]" id="table-row-type-<?php echo $id ?>" value="<?php echo (int)$is_category ?>" />
                        <div class="btn-group pull-right table-button-cta">
                            <?php
                            if($perms[$module] > 3 || ($perms[$module] <= 3 && ${$module}->is_post_owner($users->get_uid(),$id))){
                            ?>
                            <a href="<?php echo $link_edit ?>" class="btn"><i class="icon-edit"></i></a>
                            <!--a href="#" class="btn table-row-handle"><i class="icon-move"></i></a-->
                            <a href="<?php echo $link_delete ?>" data-action="delete" data-ref-id="<?php echo $id ?>" class="btn btn-warning"><i class="icon-trash"></i></a>                        
                            <button class="btn dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo __CMS_PATH__.$module.$move_path ?>">Move this element</a></li>                                
                            </ul>
                            <?php
                            }//end if
                            ?>
                        </div>            	
                    </td>
                </tr>
                <?php		
                    }//end for i
                }//end if
                ?>
            </tbody>
        </table>
    <?php	
		break;
	}//end switch
	?>
    <div class="buttons-bar">
    	<div class="btn-group">
        	<?php
			if($can_post){
			?>
            <a href="<?php echo __MODULE_PATH__.$new_post_link ?>" class="btn btn-primary"><?php echo __NEW_POST_LABEL__ ?></a>
            <?php
			}//end if
			if($utils->view->module_settings['disable_categories'] != '1'){
			?>
            <a href="<?php echo __MODULE_PATH__.$new_category_link ?>" class="btn btn-primary"><?php echo __NEW_CATEGORY_LABEL__ ?></a>        
            <?php
			}//end if
			if($bulk_actions && $can_post){
			?>       
            <a href="#" class="btn btn-primary" data-action="bulk-delete">Delete Selected</a> 
            <?php
			}//end if
			?>   
        </div>       	                               
    </div>
</form>
<script>
	$().ready(function() {
        $(".switcher span").jSwicher();
    });
</script>

<div class="modal hide fade" id="delete-item-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Confirm Delete</h3>
    </div>
    <div class="modal-body">
       
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">No, forget it</a>
        <a href="#" class="btn btn-primary" id="confirm-delete">Yes delete it</a>
    </div>
</div>