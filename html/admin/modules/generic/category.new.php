<?php
	$id = $_REQUEST['id'];
	//check if it has a father category id
	$has_category = false;
	if(!is_null($_REQUEST['parent_id'])){
		$has_category = true;
		$category_id = $_REQUEST['parent_id'];
	}//end if
	$edit_mode = false;
	$page_title = "New Category";
	if(!is_null($id)){
		$category_data = ${$module}->get_category(array('id' => $id));
		$edit_mode = true;	
		$page_title = "Edit Category";
	}//end if
	$attachment_post_type = 'category';
?>
<h4><?php echo $page_title ?></h4>
<div id="form-wrapper" class="clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.category" method="post" name="save-category" id="save-category" enctype="multipart/form-data">
        <div class="compressed-form">
            <input type="hidden" name="module" id="module" value="<?php echo $module ?>" />
            <input type="hidden" name="attachment_post_type" id="attachment-post-type" value="category" />
            <?php
            if($edit_mode){
            ?>
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
            <?php
            }//end if
            if($has_category){
            ?>
            <input type="hidden" name="father_id" id="father_id" value="<?php echo $category_id ?>" />
            <?php
            }//end if
            ?>
            <fieldset>    	
                <div class="control-group">
                    <label>Category Name</label>
                    <input type="text" name="name" id="name" class="input-block-level" placeholder="Category Name" required />
                    <input type="hidden" name="permalink" id="permalink" />
                    <div class="permalink"></div>
                </div>        
                <div class="control-group">
                    <label>Message</label>            
                    <textarea name="message" id="message" class="input-block-level" ckeditor="true">
                    <?php
                    if($edit_mode == true){
                        echo $category_data['category_message'];
                    }//end if
                    ?>
                    </textarea>
                </div>     
                <?php
                if($utils->view->settings['enableTemplates'] == 1){
                    require_once("inc/components/templates/templates.php");
                }//end if
                ?>                    
            </fieldset>            
            <div class="clearfix">
            	<button type="submit" id="submit-form" class="btn btn-primary">Save</button>
                <a hrer="#" class="btn" data-action="cancel">Cancel</a>
            </div>
        </div>
        <?php
        include("inc/components/sidebar.category.php");
        ?>
    </form>
    <?php
	include("inc/components/media/media.php");
	?>
</div>