<?php
	$ewrite = $this->utils->call('ewrite');
	$recent_posts = $ewrite->get_recent_posts(array('limit' => 3));
	if($recent_posts !== false){
?>
<style>
	.title-no-pad{
		margin-bottom: 0 !important;	
	}
</style>
<h3 class="title-no-pad">Latest Posts</h3>
<table class="table table-stiped table-hover widget-bookings">
	<thead>
    	<tr>
        	<th>Title</th>
            <th>Date</th>
            <th class="to-right">User</th>
        </tr>
    </thead>
    <tbody>
<?php
		for($i = 0; $i < sizeof($recent_posts); $i++){
?>
	<tr>
    	<td><i class="icon-document"></i> <a href="<?php echo __CMS_PATH__ ?><?php echo $recent_posts[$i]['module_name'].'/edit/'.$recent_posts[$i]['post_id'] ?>"><?php echo $recent_posts[$i]['post_title'] ?></a></td>
        <td width="30%"><?php 
		echo date("d/m/Y",$recent_posts[$i]['post_date']);	
		if($recent_posts[$i]['post_last_update'] != "0"){
			echo '<br /><small class="muted">last update: <br />'.date("d/m/Y H:i",$recent_posts[$i]['post_last_update']).'</small>';	
		}
		?></td>
        <td class="to-right"><?php echo $recent_posts[$i]['user_firstname'].' '.$recent_posts[$i]['user_surname']; ?></td>
    </tr> 
<?php			
		}//end for i
?>
	</tbody>
</table>
<?php
	}//end if
?>