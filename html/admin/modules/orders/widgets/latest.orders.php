<?php
	$orders = $this->utils->call('orders');
	$params = array('limit' => 3);
	$recent_posts = $orders->get_orders($params);
	if($recent_posts !== false){
?>
<style>
	.title-no-pad{
		margin-bottom: 0 !important;	
	}
</style>
<h3 class="title-no-pad">Latest Orders</h3>
<table class="table table-stiped table-hover widget-bookings">
	<thead>
    	<tr>
        	<th>Title</th>  
            <th>User / Company</th>   
            <th>Date</th>    
            <th class="to-right">Status</th>         
        </tr>
    </thead>
    <tbody>
<?php
		for($i = 0; $i < sizeof($recent_posts); $i++){
			$status = ucwords(str_replace("-"," ",$recent_posts[$i]['order_status']));        
			$id = $recent_posts[$i]['order_id'];
			$label = "Order ID #".$id." - ".strtoupper($recent_posts[$i]['order_code']);
			$user_label = $list[$i]['user_firstname'].' '.$list[$i]['user_surname'];
			if(trim($recent_posts[$i]['user_firstname']) == "" && trim($recent_posts[$i]['user_surname']) == ""){
				$user_label = $recent_posts[$i]['user_username'];	
			}
?>
	<tr>
    	<td><i class="icon-document"></i> <a href="<?php echo __CMS_PATH__.'orders/view/'.$id ?>"><?php echo $label ?></a></td>
        <td><?php echo $user_label ?></td>
        <td width="30%"><?php echo date("d/m/Y H:i",$recent_posts[$i]['order_date']) ?></td>
        <td class="to-right"><?php echo $status; ?></td>
    </tr> 
<?php			
		}//end for i
?>
	</tbody>
</table>
<?php
	}//end if
?>