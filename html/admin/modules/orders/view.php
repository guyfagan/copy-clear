<?php
	$id = $_GET['id'];
	
	$use_cart = false;
	if(isset($utils->view->module_settings['orders_use_cart'])){
		if($utils->view->module_settings['orders_use_cart'] == 1){
			$use_cart = true;	
		}
	}
	
	if(isset($utils->view->module_settings['orders_engine']) && !is_null($utils->view->module_settings['orders_engine'])){
		${$module} = $utils->call($utils->view->module_settings['orders_engine']);
	}//end if
	
	$order_data = ${$module}->get_order(array('id' => $id));	
	
	if($use_cart){
		$items = ${$module}->get_order_cart_items(array('id' => $id,'meta' => true,'default_photo' => true, 'use_generic_labels' => true));
	} else {
		$items = ${$module}->get_order_posts(array('id' => $id,'meta' => true, 'use_generic_labels' => true));
	}//end if
	
	$user_label = $order_data['user_firstname'].' '.$order_data['user_surname'];
	if(trim($order_data['user_firstname']) == "" && trim($order_data['user_surname']) == ""){
		$user_label = $order_data['user_username'];	
	}//end if
?>
<div class="order-view-container">
    <h4>Order Information</h4>
    <p>
        <strong>Order ID:</strong> #<?php echo $order_data['order_id'] ?><br />
        <strong>Date:</strong> <?php echo date("d/m/Y H:i:s",$order_data['order_date']) ?><br />
        <strong>Code:</strong> <?php echo strtoupper($order_data['order_code']) ?><br />
        <strong>Made by:</strong> <?php echo $user_label ?>
    </p>
    <h4>Order Details</h4>
  	<table class="table table-striped table-hover">
        <thead>
            <tr>
                <td>&nbsp;</td>
                <th>Product</th>
                <th><span class="pull-right">Quantity</span></th>
            </tr>
        </thead>
        <tbody>
        <?php
        for($i = 0; $i < sizeof($items); $i++){
            $size = $items[$i]['meta_size'];
            $item_id = array('item_id' => $items[$i]['cart_item_id']);
            $item_id = base64_encode(serialize($item_id));
			if($use_cart === true){
				$quantity = $items[$i]['cart_item_quantity'];	
			} else {
				$quantity = $items[$i]['order_post_amount'];	
			}//end if
            
        ?>
            <tr>
                <td class="thumb-column">
                <?php
                if(isset($items[$i]['photo'])){
                    echo '<img src="'.__SERVERPATH__.'upload/photos/t/'.$items[$i]['photo']['attachment_filename'].'" alt="" />';	
                }
                ?>
                </td>
                <td>
                    <strong><?php echo $items[$i]['title'] ?></strong>                   
                </td>
                <td>
                    <span class="pull-right">
                    <?php echo $quantity ?>
                    </span>
                </td>
            </tr>
        <?php	
        }//end for i
        ?>	
        </tbody>
        <tfoot>    	
            <tr>
                <td colspan="3">        
                    <form action="#" name="order-view" id="order-view" method="post"> 
                        <input type="hidden" name="module" id="module" value="<?php echo $order_data['module_name'] ?>" />
                        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />      
                        <select name="order_status" id="order_status">
                        	<option value="submitted">Submitted</option>
                        	<option value="processing-order">Processing Order</option>
                            <option value="processed">Processed</option>
                            <option value="dispatched">Dispatched</option>
                            <option value="pending-payment">Pending Payment</option>
                            <option value="paid">Paid</option>
                        </select>
                        <a href="#" class="btn btn-primary" id="set-order-status-btn">Set order status</a>                        
                    </form>
                </td> 
            </tr>
        </tfoot>
    </table>
<?php
	if($order_data['order_cart_id'] > 0){
		$files = $utils->get_attachments(array(
			'post_id' => $order_data['order_cart_id'],
			'attachment_post_type' => 'cart',
			'module' => 'cart',
			'filesize' => true,
			'upload_path' => '../'
		));	
		if($files !== false){
		?>
		<h4>Uploaded files</h4>
		<table class="table table-striped" id="order-files">
			<thead>
				<tr>
					<th>Thumb</th>
					<th>Filename</th>
					<th class="to-right">Size</th>
				</tr>
			</thead>
			<tbody>
			<?php
			for($i = 0; $i < sizeof($files); $i++){
				$is_image = false;
				$path = __SERVERPATH__.'upload/files/'.$files[$i]['attachment_filename'];
				if($files[$i]['attachment_type'] == 'image'){
					$path = __SERVERPATH__.'upload/photos/o/'.$files[$i]['attachment_filename'];
					$is_image = true;	
				}//end if
			?>
				<tr>
					<td class="table-thumb">
					<?php
					if($is_image){
					?>
					<img src="<?php echo __SERVERPATH__ ?>upload/photos/t/<?php echo $files[$i]['attachment_filename'] ?>" alt="Preview" />
					<?php	
					}//end if
					?>
					</td>
					<td>
						<a href="<?php echo $path ?>" target="_blank"><?php echo $files[$i]['attachment_friendly_name'] ?> <i class="icon icon-external-link"></i></a>
					</td>
					<td class="to-right"><?php echo $files[$i]['filesize'] ?></td>
				</tr>
			<?php	
			}//end for i
			?>
			</tbody>
		</table>
		<?php
		}//end if
	}//end if
?>
</div>
<script>
	$().ready(function(){
		$("#order_status").val('<?php echo $order_data['order_status'] ?>');
	});
</script>