<?php
	$category_id = $_GET['id'];
	$params = array();
	$new_post_link = "/new/";
	$has_father_id = false;
	$father_id = 0;
	
	$settings = $utils->view->module_settings;

	if(isset($utils->view->module_settings['orders_engine']) && !is_null($utils->view->module_settings['orders_engine'])){
		${$module} = $utils->call($utils->view->module_settings['orders_engine']);
	}//end if

	$elements_per_page = 25;
	if(isset($settings['items_per_page']) && is_numeric($settings['items_per_page'])){
		$elements_per_page = $settings['items_per_page'];
	}//end if
	/*
	if(isset($_GET['search'])){
		$params['search'] = $_GET['search'];
	}//end if
	*/
	
	if(isset($_GET['user_id'])){
		$params['user_id'] = $_GET['user_id'];
	}//end if
	
	$paginated_results = false;
	if($elements_per_page > 0){
		$paginated_results = true;
		$utils->create_paging($_GET['page'],$elements_per_page);
	}//end if
	$list = ${$module}->get_orders($params);
		
	# PAGINATION #
	if($paginated_results){
		$paging_stats = $utils->get_paging_stats();	
		$paging_list = $utils->get_paging(array('range' => 10));
		//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
		$utils->unset_paging();
		$page_start = $paging_stats['start']+1;
		$page_end = $paging_stats['start']+10;
		if($page_end > $paging_stats['founds']){
			$page_end = $paging_stats['founds'];
		}//end if
		$page_total = $paging_stats['pages'];
		$page_current = $paging_stats['current_page'];
		$page_prev = $page_current-1;
		if($page_prev == "0"){
			$page_prev = 1;
		}//end if
		$page_next = $page_current+1;
		if($page_next > $page_total){
			$page_next = $page_total;
		}//end if
	}//end if
	# END PAGINATION #
	
	
	$table_type = "table-large-list";
	if(isset($settings['list_view_type'])){
		if($settings['list_view_type'] == "small"){
			$table_type = "table-small-list";
		}//end if
	}//end if
	
?>
<form action="#" name="table-form" id="table-form" method="post">
	<input type="hidden" name="page" id="page" value="<?php echo $page_current ?>" />
    <input type="hidden" name="total-items" id="total-items" value="<?php echo $paging_stats['founds'] ?>" />
    <input type="hidden" name="items-per-page" id="items-per-page" value="<?php echo $elements_per_page ?>" />
    <input type="hidden" name="parent-id" id="parent-id" value="<?php echo $father_id ?>" />
    <input type="hidden" name="module" id="module" value="<?php echo $module ?>" />
    <table class="table table-striped table-hover <?php echo $table_type ?>">
        <thead>
            <tr>
                <th>Title</th>  
                <th>User / Company</th>   
                <th>Type</th>    
                <th>Status</th>
                <th>Amount</th>
                <!--td>
                	<div class="btn-group pull-right" id="table-view-type">
                    	<a href="#" data-table-type="large-list" class="btn btn-mini<?php if($table_type == "table-large-list"){ echo ' active'; } ?>"><i class="icon-th-list"></i></a>
                        <a href="#" data-table-type="small-list" class="btn btn-mini<?php if($table_type == "table-small-list"){ echo ' active'; } ?>"><i class="icon-list"></i></a>
                    </div>
                </td-->
            </tr>
        </thead>
        <?php
		if($paginated_results){
			if($page_total > 1){
			?>
			<tfoot>
				<tr>
					<td colspan="3">
						<div class="pagination">
							<ul>
								<li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
								<?php
								if(is_array($paging_list) && sizeof($paging_list) > 0){
									for($i = 0; $i < sizeof($paging_list); $i++){
										echo '<li';
										if($paging_list[$i]['page'] == $page_current){
											echo ' class="active"';	
										}//end if
										echo '><a href="?page='.$paging_list[$i]['page'].'">'.$paging_list[$i]['page'].'</a></li>';
									}//end for i
								}//end if
								?>                      
								<li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
							</ul>
						</div>
					</td>
				</tr>
			</tfoot>
			<?php
			}//end if
		}//end if
		?>
        <tbody>
            <?php
		
			$revenue = 0;
            if($list !== false){
                for($i = 0; $i < sizeof($list); $i++){             
					$status = ucwords(str_replace("-"," ",$list[$i]['order_status']));        
					$id = $list[$i]['order_id'];
					//Vars in case of Post
					$link_edit = __MODULE_PATH__."/edit/".$list[$i]['order_id'];
					//if we are showing the content of a category, we add the category_id in the edit link
					if(!$is_category && !is_null($category_id)){
						$link_edit .= "/in/".$category_id;	
					}//end if
					$link_delete = __CMS_PATH__."/api/delete.post?id=".$list[$i]['order_id']."&module=".$module;
					$label = "Order ID #".$id." - ".strtoupper($list[$i]['order_code']);
					$icon = "icon-file";
					
					$company_data = $users->get_user(array('id' => $list[$i]['order_uid'],'meta' => true));			
					
					$user_label = $list[$i]['user_firstname'].' '.$list[$i]['user_surname'];
					if(trim($list[$i]['user_firstname']) == "" && trim($list[$i]['user_surname']) == ""){
						$user_label = $list[$i]['user_username'];	
					}
										
					$options = array('id' => $list[$i]['order_id'],'module' => $list[$i]['module_name']);				
					
					//$total = $amount['total'];
					$total = $list[$i]['order_amount'];
					$revenue += $total;
            ?>
            <tr class="table-row">            	
                <td>                	
                    <i class="<?php echo $icon ?>"></i> <?php 
                    echo '<a href="'.__MODULE_PATH__.'/view/'.$id.'" id="label-'.$id.'">'.$label.'</a>'; 
                    if(!$is_category){
                        //show the post date
                        echo '<span class="table-info-line muted"><i class="icon-time"></i> '.date('d/m/Y H:i:s',$list[$i]['order_date']).'</span>';
                        //show the user
                        //echo '<span class="table-info-line muted"><i class="icon-user"></i> created by <strong>'.$list[$i]['user_firstname'].' '.$list[$i]['user_surname'].'</strong></span>';
                    }//end if
                    ?>
                </td>  
                <td>
                <?php
					echo $user_label;
				?>
                </td>  
                <td>
                <?php
					echo $list[$i]['module_label'];
				?>
                </td>				
                <td class="table-row-status">
                	<span id="post-status-<?php echo $id ?>"><?php echo $status ?></span>
                </td>  
                <td>
               	<?php
					echo '<strong>&euro;'.number_format($total,2).'</strong>';
				?>
                </td>
            </tr>
            <?php		
                }//end for i
            }//end if
            ?>
            <tr>
            	<td colspan="5">
                	<?php
						echo '<span class="pull-right"><strong>&euro;'.number_format($revenue,2).'</strong></span>';
					?>
                </td>
            </tr>
        </tbody>
    </table>   
    <a href="#" class="btn btn-primary" id="export-btn">Export as a CSV file</a>
</form>
<script>
	$().ready(function() {
        $(".switcher span").jSwicher();
    });
</script>

<div class="modal hide fade" id="delete-item-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Confirm Delete</h3>
    </div>
    <div class="modal-body">
       
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">No, forget it</a>
        <a href="#" class="btn btn-primary" id="confirm-delete">Yes delete it</a>
    </div>
</div>