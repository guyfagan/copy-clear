<?php
	$module_data = $utils->get_module($module);
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <div class="form-block">
            <fieldset>
                <legend>Module Status</legend>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-public" value="public" />
                    Public                
                </label>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-private" value="private" />
                    Private                
                </label>
            </fieldset>
            <fieldset>
            	<legend>Site Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_searchable" id="module_searchable">
                    Make the module searchable in the public search             
                </label>
            </fieldset>
        </div>     
        <div class="form-block">
        	<fieldset>
            	<legend>Notifications</legend>
                <label>Send notification emails with the following address</label>
                <input type="text" name="order_no_reply" id="order_no_reply" class="input-xxlarge" />
                <label>Once a new order is generated submit an email to</label>                
                <input type="text" name="order_notification_email" id="order_notification_email" class="input-xxlarge"/>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="order_send_invoice" id="order-send-invoice"> Send an invoice to the user once the order is complete
                </label>
                <label>Email type</label>
                <select name="order_email_type" id="order_email_type">
                	<option value="text">Plain Text</option>
                    <option value="html">Html</option>
                </select>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					require_once('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>Order email subject</label>
                <input type="text" name="order_email_subject" id="order_email_subject" class="input-xxlarge" />
                <label>Order email signature</label>
                <textarea data-ckeditor="true" data-ckeditor-height="100" id="order_email_signature" name="order_email_signature">
                <?php
				if(isset($settings['order_email_signature'])){
					echo $settings['order_email_signature'];	
				}//end if
				?>
                </textarea>
            </fieldset>
        </div>  
        <div class="form-block">
        	<fieldset>
            	<legend>Orders</legend>
                <label>
                	General Sort Order
                    <select id="list_order" name="list_order">
                    	<option value="ASC">Older first</option>
                        <option value="DESC">Newer first</option>
                    </select>
                </label>
                <br />
                <label class="checkbox">
                    <input type="checkbox" value="1" name="show_thumbnails" id="show_thumbnails">
                    Show posts thumbnails if available          
                </label>
                <br />
                <label>
                	List view type
                    <select id="list_view_type" name="list_view_type">
                    	<option value="large">Expanded (shows thumbnails, author and date)</option>
                        <option value="small">Compressed (show minimal information about the post)</option>
                    </select>
                </label>
                <br />
                <label>
                	Elements per page
                    <select id="items_per_page" name="items_per_page">
                    	<option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="0">Don't paginate and show everything</option>
                    </select>
                </label>
                <label>                	
                	Engine              
                    <select name="orders_engine" id="orders-engine">
                   	<?php	
					$tools = $utils->call('tools');								
					$files = $utils->get_folder_files(array('path' => 'admin/classes/','filter' => 'orders'));
					if($files !== false){
						for($i = 0; $i < sizeof($files); $i++){
							$class_name = tools::file_get_php_classes($_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.'admin/classes/'.$files[$i]);		
							echo '<option value="'.$class_name[0].'">'.$class_name[0].'</option>';
						}//end for i
					} else {
						echo '<option>No engine found</option>';
					}//end if
					?>
                    </select>
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="orders_use_cart" id="orders-use-cart"> Use the cart system to store the order items
                </label>
        	</fieldset>
        </div>
        <?php
			$sys_image_formats = $utils->get_attachments_formats(array("default" => false));
			if($sys_image_formats !== false){
		?>
        <div class="form-block">
            <fieldset>
                <legend>Images</legend>
                <?php
					$saved_image_settings = $utils->get_settings(array("module_id" => $utils->view->module));
					$saved_image_settings = $saved_image_settings['moduleDefaultImageFormats'];
					if(!is_null($saved_image_settings)){
						$saved_formats = explode(",",$saved_image_settings);
					}//end if					
					
				?>
                	<input type="hidden" name="moduleDefaultImageFormats" id="module-default-formats"<?php 
					if(isset($saved_image_settings) && !is_null($saved_image_settings)){
						echo ' value="'.$saved_image_settings.'"';	
					}//end if
					?> />
                	<p>                    	
                        By default there are two formats: Thumbnail and Large, which are system defauls formats and cannot be deactived. But you can always add new formats by adding them in the <a href="<?php echo __CMS_PATH__ ?>media/settings/">Media Settings</a> and then activate them here.<br />
                        Please select the image formats you want to activate for this module when a picture is uploaded.
                    </p>  
                    <?php
					echo '<div class="settings-format-container">';
					for($i = 0; $i < sizeof($sys_image_formats); $i++){
						$format_class = '';
						$format_status = 'Disabled';
						if(isset($saved_formats) && is_array($saved_formats)){
							if(in_array($sys_image_formats[$i]['attachment_setting_id'],$saved_formats)){
								$format_class = ' btn-success active';	
								$format_status = 'Enabled';
							}
						}//end if
						$width = (int)$sys_image_formats[$i]['attachment_setting_width'];
						if(is_numeric($width) && $width > 0){
							$width .= "px";	
						} else {
							$width = "auto";
						}//end if
						
						$height = (int)$sys_image_formats[$i]['attachment_setting_height'];
						if(is_numeric($height) && $height > 0){
							$height .= "px";	
						} else {
							$height = "auto";
						}//end if
					?> 
                    <div class="setting-format-box">
                    	 
                        <p><strong><?php echo $sys_image_formats[$i]['attachment_setting_label'] ?></strong><br />Width: <?php echo $width ?>, Height: <?php echo $height ?>, Processing: <?php echo ucwords($sys_image_formats[$i]['attachment_setting_processing']) ?>
                        </p>
                        <button type="button" class="btn<?php echo $format_class ?>" data-toggle="button" data-format-id="<?php echo $sys_image_formats[$i]['attachment_setting_id'] ?>" data-btn-action="toggle-formats"><?php echo $format_status ?></button> 
					</div>	
					<?php
					}//end for i
					echo '</div>';			
					?>                                     
            		              
            </fieldset>
        </div>
        <?php
			}//end if
		?>
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
<script>
	$().ready(function(){
		$('button[data-btn-action=toggle-formats]').click(function(e){
			if($(this).hasClass('active')){
				$(this).removeClass('btn-success');	
				$(this).text("Disabled");			
			} else {				
				$(this).addClass('btn-success');	
				$(this).text("Enabled");
			}
		
			$selectedIds = new Array();
			
			$('button[data-btn-action=toggle-formats]').each(function(){
				if($(this).hasClass("btn-success")){
					$selectedIds.push($(this).attr("data-format-id"));
				}
			});
			
			console.log($selectedIds);
			$("#module-default-formats").val($selectedIds);
			
		});	
	});
</script>