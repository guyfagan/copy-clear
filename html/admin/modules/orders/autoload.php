<?php
	$js_libs_autoload = array();
	$js_libs_autoload['settings'] = array(
									"js/cms.formdata.jquery.js",
									"inc/ckeditor/".__CKEDITOR_VERSION__."/ckeditor.js",
									"inc/ckeditor/".__CKEDITOR_VERSION__."/adapters/jquery.js",
									array(
										"type" => "css", 
										"path" => "inc/tokenfield/".__BOOTSTRAP_TOKENFIELD__."/bootstrap-tokenfield.css"
									),	
									"inc/tokenfield/".__BOOTSTRAP_TOKENFIELD__."/bootstrap-tokenfield.js"											
									);
	$js_libs_autoload['view'] = array(
									array(
										"type" => "css", 
										"path" => "modules/orders/css/orders.css"
									)
								);
	$js_libs_autoload['new'] = array(
									"js/cms.formdata.jquery.js",
									"js/cms.permalink.jquery.js",
									"inc/components/media/media-editor.js",
									"inc/components/media/photo-editor.js",
									"inc/jcrop/".__JCROP_VERSION__."/jquery.Jcrop.min.js",
									array(
										"type" => "css", 
										"path" => "inc/jcrop/".__JCROP_VERSION__."/css/jquery.Jcrop.min.css"
									),
									"inc/components/media/media.js",
									"inc/components/categories/categories.js",
									array(
										"type" => "css", 
										"path" => "inc/components/tags/tags.css"
									),
									"inc/components/tags/tags.js",
									array(
										"type" => "css", 
										"path" => "inc/components/revisions/revisions.css"
									),
									array(
										"type" => "css",
										"path" => "inc/mediaElement/".__MEDIAELEMENT_VERSION__."/mediaelementplayer.css"
									),
									"inc/mediaElement/".__MEDIAELEMENT_VERSION__."/mediaelement-and-player.min.js",
									"inc/components/revisions/revisions.js",
									array(
										"type" => "js",
										"path" => "inc/ckeditor/".__CKEDITOR_VERSION__."/ckeditor.js",
										"charset" => "utf-8"											
									),
									"inc/ckeditor/".__CKEDITOR_VERSION__."/adapters/jquery.js"
									);
?>