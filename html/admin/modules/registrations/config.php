<?php
	$config = array();
	$config['default_action'] = "list";
	$config['alias'] = 'users';
	$config['subnav'] = array();
	$config['subnav']['list'] = 'Full List';
	$config['subnav']['pending'] = 'Pending';
	//custom breadcrumb
	$config['breadcrumb'] = array();	
	$users = $this->utils->call('users');
	$config['breadcrumb']['new'] = array('class' => $users,'method' => 'get_breadcrumb', 'options' => array('parent_id' => $_REQUEST['parent_id']));
	$config['breadcrumb']['list'] = array('class' => $users,'method' => 'get_breadcrumb', 'options' => array('parent_id' => $_REQUEST['id']));
?>