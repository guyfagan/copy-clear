<?php
	if($method == "update.registration.status"){
		$value = $_REQUEST['value'];
		$underscore = strpos($_REQUEST['id'],"_");		
		$newid = substr($_REQUEST['id'],7,strlen($_REQUEST['id']));	
		$result = $users->change_user_status(array('id' => $newid, 'status' => $value));
		$data = array();
		if($result === true){
			$data['result'] = "pass";
			$data['value'] = $value;
		} else {
			$data['result'] = "error";
		}//end if
		echo json_encode($data);
	}//end if
?>