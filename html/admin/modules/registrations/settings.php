<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->view->module_settings;
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <input type="hidden" name="module-status" id="module-status-public" value="private" />
        <div class="form-block">         
            <fieldset>
            	<legend>Notifications</legend>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="notifyAdminsNewRegistrations" id="notify-new-registrations"> Notify admins about new registrations
                </label>
                <label>Send a notification email on new registration to the following address</label>
                <input type="text" name="notifyAdminsNewRegistrationsEmail" id="notify-admin-new-registrationsEmail" class="input-xxlarge" />
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'notifyAdminEmailTemplate';					
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>Admins notification email subject</label>
                <input type="text" name="notifyAdminsEmailSubject" id="notify-admins-email-subject" class="input-xxlarge">
                <label>Admins notification email message</label>
                <textarea name="notifyAdminsEmailMessage" id="notify-admins-email-message" data-ckeditor="true" data-ckeditor-height="150"><?php
				if(isset($settings['notifyAdminsEmailMessage'])){
					echo $settings['notifyAdminsEmailMessage'];	
				}//end if
				?></textarea>
                <hr />
                <label class="checkbox">
                	<input type="checkbox" value="1" name="notifyUserRegistrations" id="notify-user-registrations"> Send a message back to the user about the registration
                </label>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'notifyUserEmailTemplate';					
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>User notification email subject</label>
                <input type="text" name="notifyUserEmailSubject" id="notify-user-email-subject" class="input-xxlarge">
                <label>User notification email message</label>
                <textarea name="notifyUserEmailMessage" id="notify-user-email-message" data-ckeditor="true" data-ckeditor-height="150"><?php
				if(isset($settings['notifyUserEmailMessage'])){
					echo $settings['notifyUserEmailMessage'];	
				}//end if
				?></textarea>
                <hr />
                <label class="checkbox">
                	<input type="checkbox" value="1" name="notifyUserActivation" id="notify-user-activation"> Send a message to the user once it gets activated by an admin
                </label>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'notifyActivationEmailTemplate';					
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>User activation email subject</label>
                <input type="text" name="notifyActivationEmailSubject" id="notify-activation-email-subject" class="input-xxlarge">
                <label>User activation email message</label>
                <textarea name="notifyActivationEmailMessage" id="notify-activation-email-message" data-ckeditor="true" data-ckeditor-height="150"><?php
				if(isset($settings['notifyActivationEmailMessage'])){
					echo $settings['notifyActivationEmailMessage'];	
				}//end if
				?></textarea>                
            </fieldset>
            <fieldset>
            	<legend>Reset Password</legend>
                <label>Activation URL</label>
                <input type="text" class="input-xxlarge" id="resetPasswordURI" name="resetPasswordURI" placeholder="http://">
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'resetPasswordEmailTemplate';
					$emailTemplateLabel = 'Use the following template for reset password';					
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>Reset password email subject</label>
                <input type="text" name="resetPasswordEmailSubject" id="reset-password-email-subject" class="input-xxlarge">
            </fieldset>            
            <fieldset>
            	<legend>Users List</legend>
                <label>
                	Elements per page
                    <select id="items_per_page" name="items_per_page">
                    	<option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="0">Don't paginate and show everything</option>
                    </select>
                </label>
            </fieldset>
            
        </div>           
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
