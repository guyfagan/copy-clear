// JavaScript Document

$().ready(function(){
	interceptor();
});

//load a post data
$.fn.load_user = function(params){
	var $form = $(this);
	params = $.extend({prefix: "", meta: false},params);
	$.ajax({
		url: $.config.apipath + "get.user?id="+params.id+"&module="+$.config.module+"&meta="+params.meta,
		type: "get",
		dataType:"json",
		scriptCharset: "utf-8",
		timeout: 30000,
		success: function(data){		
			if(data.result == "success"){
				var $user = data.user;				
				$form.formdata({data: $user, prefix: params.prefix, meta: params.meta});
			}
		}
	});
};

$.fn.load_group = function(params){
	var $form = $(this);
	params = $.extend({prefix: "", meta: false},params);
	$.ajax({
		url: $.config.apipath + "get.users.group?id="+params.id+"&module="+$.config.module,
		type: "get",
		dataType:"json",
		scriptCharset: "utf-8",
		timeout: 30000,
		success: function(data){		
			if(data.result == "success"){				
				var $group = data.group;					
				$form.formdata({data: $group, prefix: params.prefix, meta: params.meta});				
			}//end if
		}
	});
};

$.fn.load_group_permissions = function(params){
	var $form = $(this);
	params = $.extend({prefix: "", meta: false},params);
	$.ajax({
		url: $.config.apipath + "get.users.group.permissions?id="+params.id+"&module="+$.config.module,
		type: "get",
		dataType:"json",
		scriptCharset: "utf-8",
		timeout: 30000,
		success: function(data){		
			if(data.result == "success"){				
				var $group = data.permissions;					
				for(var key in $group){
					$("select[name="+key+"]").val($group[key]);	
					console.log(key + " > " + $("select[name="+key+"]").val());				
				}
			}//end if
		}
	});
}


function interceptor(){
	//there is no space for Mel Gibson here
	var $bodyAttrs = $("body").attr("class").split(" ");
	for(i = 0; i < $bodyAttrs.length; i++){		
		if(parseInt($bodyAttrs[i].indexOf("action-")) != -1){
			$action = $bodyAttrs[i];			
			$action = $action.replace("action-","");
		}
	}
	switch($action){
		case "profile":
		case "new":
			new_user_actions();			
			break;
		case "list":
			users_table_actions();
			break;
		case "settings":
			settings_actions();			
			break;
		case "group-new":
			new_group_actions();
			break;
		case "groups":
			groups_table_actions();
			break;
	}//end switch
}

//Specific actions
function new_user_actions(){
	
	//now check if we have the id of the post, if yes load the data about it
	if($("#id").length > 0){
		$("#save-user").load_user({id: $("#id").val(), module: $.config.module, prefix: "user_", meta: true});
		$(".breadcrumb li:last a").text("Edit");
	}//end if
	
	$('input[data-enable-tokenfield=true]').tokenfield();	
	
	$('form').findOptionalFields();
	
	if($("#save-user").length > 0){
		
		$('a[data-action=cancel]').click(function(e){
			e.preventDefault();
			window.history.back()	
		});	
		
		$("#save-user").applyCKEditor();
		
		$("#save-user input, #save-user textarea").change(function(){
			$.cmsVars.formSaved = false;	
		});
		
		$("#save-user").submit(function(e){
			e.preventDefault();	
			var $url = $(this).attr("action");
			$.ajax({
				url: $url,
				type: "post",
				data: $(this).serialize(),
				dataType:"json",
				success: function(data){
					if(data.result == "success"){
						$("#save-user").popMessage("The user has been saved successfully",{type: "success"});
						$.cmsVars.formSaved = true;
						add_post_id(data.id);
						location.href = $.config.basepath+'admin/'+$.config.module+'/list/'+$('#group_id').val();
					} else {
						$("#save-user").popMessage("The user has not been saved due to a server error",{type: "error"});
					}
				}	
			});
		});
		
		function add_post_id(id){
			if($("#id").length == 0){
				$("#save-user").append('<input type="hidden" id="id" name="id" value="'+id+'" />');
			}//end if
		}//end function
		
		$("#submit-form").click(function(e){
			e.preventDefault();
			var isValidated = $("#save-user").validate();
			if(isValidated){
				$("#save-user").submit();	
			} else {
				$("#save-user").popMessage("Please fill the fields in red first!",{type: "error"});
			}
		});
	}//end if
}//end function

//Specific actions
function new_group_actions(){
	
	$(".breadcrumb li:last").prev().after('<li><a href="'+$.config.basepath+'admin/'+$.config.module+'/groups/">Groups</a><span class="divider">/</span></li>');
	//now check if we have the id of the post, if yes load the data about it
	if($("#id").length > 0){
		//$("#save-group").load_group({id: $("#id").val(), prefix: "users_group_", meta: true});
		//$("#save-group").load_group_permissions({id: $("#id").val()});
		$(".breadcrumb li:last a").text("Edit");
	}//end if
	
	
	if($("#save-group").length > 0){
			
		$("#save-group input, #save-group textarea").change(function(){
			$.cmsVars.formSaved = false;	
		});
		
		$("#save-group").submit(function(e){
			e.preventDefault();	
			var $url = $(this).attr("action");
			$.ajax({
				url: $url,
				type: "post",
				data: $(this).serialize(),
				dataType:"json",
				success: function(data){
					if(data.result == "success"){
						$("#save-group").popMessage("The group has been saved successfully",{type: "success"});
						$.cmsVars.formSaved = true;
						add_group_id(data.id);
						location.href = $.config.basepath + 'admin/'+$.config.module+'/list/'+data.id;
					} else {
						$("#save-group").popMessage("The group has not been saved due to a server error",{type: "error"});
					}
				}	
			});
		});
		
		function add_group_id(id){
			if($("#id").length == 0){
				$("#save-group").append('<input type="hidden" id="id" name="id" value="'+id+'" />');
			}//end if
		}//end function
		
		$("#submit-form").click(function(e){
			e.preventDefault();
			var isValidated = $("#save-group").validate();
			if(isValidated){
				$("#save-group").submit();	
			} else {
				$("#save-group").popMessage("Please fill the fields in red first!",{type: "error"});
			}
		});
	}//end if
}//end function

function users_table_actions(){
	
	//capture the delete action
	if($('a[data-action="delete"]').length > 0){
		$('a[data-action="delete"]').click(function(e){
			e.preventDefault();
			var $url = $(this).attr("href");
			var $tableLine = $(this).closest("tr");
			var $id = $(this).data('ref-id');
			var $label = $("#label-"+$id).text();
			$('#delete-item-modal').unbind('show');
			$('#delete-item-modal').on('show',function(){
				$('.modal-body',$(this)).html('Are you sure that you want to delete this user <strong>&quot;'+$label+'&quot;</strong>?');
				$('#confirm-delete').unbind('click');
				$('#confirm-delete').click(function(e){
					e.preventDefault();					
					$.ajax({
						url : $url,
						type: "get",
						dataType: "json",
						success: function(data){					
							if(data.result == "success"){
								$('#delete-item-modal').modal('hide');
								$tableLine.fadeOut("slow",function(){
									$(this).remove();										
								});	
							}
						}
					});
				});
			}).modal({ backdrop: true });
		});
	}
}//end function

function settings_actions(){
	
	$('input[data-enable-tokenfield=true]').tokenfield();
	
	$("#module-settings").applyCKEditor();
	
	$("#module-settings").load_settings({module: $.config.module});
	
	$('form').findOptionalFields();
	
	$("#module-settings").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				if(data.result == "success"){
					$("#module-settings").popMessage("The settings has been saved successfully",{type: "success"});
					$.cmsVars.formSaved = true;
				
				} else {
					$("#module-settings").popMessage("The settings has not been saved due to a server error",{type: "error"});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#module-settings").validate();
		if(isValidated){
			$("#module-settings").submit();	
		} else {
			$("#module-settings").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
}

function groups_table_actions(){
	
	//capture the delete action
	if($('a[data-action="delete"]').length > 0){
		$('a[data-action="delete"]').click(function(e){
			e.preventDefault();
			var $url = $(this).attr("href");
			var $tableLine = $(this).closest("tr");
			var $id = $(this).data('ref-id');
			var $label = $("#label-"+$id).text();
			$('#delete-item-modal').unbind('show');
			$('#delete-item-modal').on('show',function(){
				$('.modal-body .modal-label',$(this)).html($label);
				$('#confirm-delete').unbind('click');
				$('#confirm-delete').click(function(e){
					e.preventDefault();	
					console.log($("#delete-users").is(':checked'));
					if($("#delete-users").is(':checked')){
						$url += "&delete_users=1";
					}
					$.ajax({
						url : $url,
						type: "get",
						dataType: "json",
						success: function(data){					
							if(data.result == "success"){
								$('#delete-item-modal').modal('hide');
								$tableLine.fadeOut("slow",function(){
									$(this).remove();										
								});	
							}
						}
					});
				});
			}).modal({ backdrop: true });
		});
	}
}//end function