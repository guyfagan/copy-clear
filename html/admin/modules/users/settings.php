<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->get_settings(array('module' => $module));
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <input type="hidden" name="module-status" id="module-status-public" value="private" />
        <div class="form-block">         
            <fieldset>
            	<legend>Users Options</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="soft-delete" id="soft-delete">
                    Don't delete users, just deactivate them        
                </label>
            </fieldset>
            <fieldset>
            	<legend>Users List</legend>
                <label>
                	Elements per page
                    <select id="items_per_page" name="items_per_page">
                    	<option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="0">Don't paginate and show everything</option>
                    </select>
                </label>
            </fieldset>
            <fieldset>
            	<legend>CMS Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_local_search" id="module_local_search">
                    Enable local search<br />
                    <small class="muted">It will search only on the current module</small>         
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="show_refine_toolbar" id="show_refine_toolbar">
                    Show refine search toolbar
                </label>
                <label>Local search result page</label>
                <select name="module_local_search_view" id="module_local_search_view">
                <?php
				$files = $utils->get_folder_files(array('path' => '/admin/modules/'.$utils->view->module.'/'));
				if($files === false){
					$files = $utils->get_folder_files(array('path' => '/admin/modules/generic/'));	
				}//end if
				if($files !== false){
					for($i = 0; $i < sizeof($files); $i++){
						$search_action = str_replace(".php","",$files[$i]);
						$search_action = str_replace(".","/",$search_action);
						echo '<option value="'.$search_action.'">'.$search_action.'</option>';
					}//end for i
				}//end if
				?>
                </select>
                <br />
                <small class="muted">The search results will be displayed in the selected view</small>
            </fieldset>
            <fieldset>
            	<legend>Authentification</legend>
                <label>Auth Storage</label>
                <select name="login_type"  id="login_type" data-toggle="optional-field">
                	<option value="cookie"<?php if($settings['login_type'] == "cookie"){ echo ' selected'; } ?>>Cookies</option>
                    <option value="session"<?php if($settings['login_type'] == "session"){ echo ' selected'; } ?>>Sessions</option>
                </select>
                <div data-type="template-optional-field" data-template-target="#login_type" data-template-match="cookie">
                	<label>Cookie expires in</label>
                    <select id="cookie_expiry" name="cookie_expiry">
                    	<option value="<?php echo(60*30) ?>">30 Minutes</option>
                        <option value="<?php echo(60*60) ?>">1 Hour</option>
                        <option value="<?php echo(60*60*6) ?>">6 Hours</option>
                        <option value="<?php echo(60*60*24) ?>">1 Day</option>
                        <option value="<?php echo(60*60*24*7) ?>">1 Week</option>
                        <option value="<?php echo(60*60*24*31) ?>">1 Month</option>
                    </select>
                </div>
            </fieldset>
        </div>           
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
