<?php
	$id = $_REQUEST['id'];	
	$group_id = $_REQUEST['parent_id'];
	if(is_null($group_id)){
		$user_data = $users->get_user(array('id' => $id));
		$group_id = $user_data['group_id'];
	}	
	$edit_mode = false;
	$page_title = "New User";
	if(!is_null($id)){
		$edit_mode = true;	
		$page_title = "Edit User";
	}//end if
?>
<h4><?php echo $page_title ?></h4>
<form action="<?php echo __CMS_PATH__ ?>api/save.user" method="post" name="save-user" id="save-user" enctype="multipart/form-data">
	<input type="hidden" name="module" id="module" value="users" />
    <input type="hidden" name="group_id" id="group_id" value="<?php echo $group_id ?>" />
    <?php
	if($edit_mode){
	?>
    <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
    <?php
	}//end if
	?>
    <div class="control-group"> 
    	<label class="control-label" for="username">Username</label>   	
        <div class="controls">        	
      		<input type="text" class="input-large" id="username" placeholder="Username" name="username" required />            
        </div>
    </div>
    <div class="control-group"> 
   		<label class="control-label" for="email">Email</label>
        <div class="controls">  
            <input type="text" class="input-large" id="email" placeholder="Email" name="email" required />
    	</div>
   	</div>
    <div class="control-group"> 
    	<label class="control-label" for="firstname">Firstname</label>   	
        <div class="controls">        	
      		<input type="text" class="input-large" id="firstname" placeholder="Firstname" name="firstname" />            
        </div>
    </div>
    <div class="control-group"> 
   		<label class="control-label" for="surname">Surname</label>
        <div class="controls">  
            <input type="text" class="input-large" id="surname" placeholder="Surname" name="surname" />
    	</div>
   	</div>
    <div class="control-group"> 
   		<label class="control-label" for="password">Password</label>
        <div class="controls">  
            <input type="password" class="input-large" id="password" placeholder="Password" name="password" />
            <?php
			if($edit_mode){
				echo '<span class="help-block"><small>Leave these two fields blank if you don\'t want to change the password</small></span>';
			}//end if
			?>
    	</div>
   	</div>
    <div class="control-group"> 
   		<label class="control-label" for="conf_password">Confirm Password</label>
        <div class="controls">  
            <input type="password" class="input-large" id="conf_password" placeholder="Confirm Password" name="conf_password" />
    	</div>
   	</div>
    <button type="submit" id="submit-form" class="btn btn-primary">Save</button>
    <a hrer="#" class="btn" data-action="cancel">Cancel</a>
</form>