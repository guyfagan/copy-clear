<?php
	$list = $users->get_groups();
?>
<table class="table table-striped table-hover">
	<thead>
        <tr>
            <th>Group</th>         
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tbody>
    	<?php
		if($list !== false){
			for($i = 0; $i < sizeof($list); $i++){			
				$id = $list[$i]['users_group_id'];
		?>
        <tr>
        	<td><i class="icon-list-alt"></i> <a href="<?php echo __MODULE_PATH__."/list/".$id ?>" id="label-<?php echo $id ?>"><?php echo $list[$i]['users_group_name'] ?></a></td>      
            <td class="table-row-options">
            	<div class="btn-group">                   
                	<a href="<?php echo __MODULE_PATH__."/group/edit/".$id ?>" class="btn"><i class="icon-pencil"></i></a>
                    <a href="<?php echo __CMS_PATH__."/api/delete.user.group?id=".$id ?>&module=<?php echo $module ?>" data-action="delete" data-ref-id="<?php echo $id ?>" class="btn btn-warning"><i class="icon-trash"></i></a>                    
                </div>
            </td>
        </tr>
        <?php		
			}//end for i
		}//end if
		?>
    </tbody>
</table>
<a href="<?php echo __MODULE_PATH__ ?>/group/new/" class="btn btn-primary">New Group</a>
<script>
	$().ready(function(e) {
        $('.table tr td:first-child a').tooltip({title: "Click to view the list of<br /> users for this group", html: true});
    });
</script>
<div class="modal hide fade" id="delete-item-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Confirm Delete</h3>
    </div>
    <div class="modal-body">
       <p>Are you sure that you want to delete this user <strong>&quot;<span class="modal-label"></span>&quot;</strong>?</p>
       <p><input type="checkbox" value="1" name="delete-users" id="delete-users" /> Delete users in this group as well</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">No, forget it</a>
        <a href="#" class="btn btn-primary" id="confirm-delete">Yes delete it</a>
    </div>
</div>