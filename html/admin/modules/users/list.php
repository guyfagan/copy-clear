<?php
	$settings = $utils->view->get_module_settings();
	
	$elements_per_page = 25;
	if(isset($settings['items_per_page']) && is_numeric($settings['items_per_page'])){
		$elements_per_page = $settings['items_per_page'];
	}//end if

	$paginated_results = false;
	if($elements_per_page > 0){
		$paginated_results = true;
		$utils->create_paging($_GET['page'],$elements_per_page);
	}//end if

	$group_id = $_REQUEST['id'];	
	if(is_null($group_id)){
		if(isset($utils->view->module_settings['default_users_group'])){
			$group_id = $utils->view->module_settings['default_users_group'];	
		}//end if
	}//end if
	$options = array('group_id' => $group_id);
	if(isset($_REQUEST['active'])){
		$options['active'] = $_REQUEST['active'];
	}//end if
	
	$searched = false;
	if(isset($_REQUEST['search']) && trim($_REQUEST['search']) != ""){
		$searched = true;
		$options['search'] = $_REQUEST['search'];	
	}//end if
	
	$list = $users->get_users($options);
	
	# PAGINATION #
	if($paginated_results){
		$paging_stats = $utils->get_paging_stats();	
		$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
		//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
		$utils->unset_paging();
		$page_start = $paging_stats['start']+1;
		$page_end = $paging_stats['start']+10;
		if($page_end > $paging_stats['founds']){
			$page_end = $paging_stats['founds'];
		}//end if
		$page_total = $paging_stats['pages'];
		$page_current = $paging_stats['current_page'];
		$page_prev = $page_current-1;
		if($page_prev == "0"){
			$page_prev = 1;
		}//end if
		$page_next = $page_current+1;
		if($page_next > $page_total){
			$page_next = $page_total;
		}//end if
	}//end if
	# END PAGINATION #
	if($searched){
?>
<div class="alert alert-success clearfix"><i class="icon icon-search"></i> Search results for &quot;<strong><?php echo $_REQUEST['search'] ?></strong>&quot; <a href="<?php echo __MODULE_PATH__ ?>/list/" class="btn btn-success pull-right"><i class="icon icon-trash"></i> Clear search</a></div>
<?php		
	}//end if
?>
<table class="table table-striped table-hover">
	<thead>
        <tr>
            <th>User</th>         
          	<th>Status</th>
            <td>&nbsp;</td>
        </tr>
    </thead>
    <?php
	if($paginated_results){
		if($page_total > 1){
		?>
		<tfoot>
			<tr>
				<td colspan="3">
					<div class="pagination">
						<ul>
							<li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
							<?php
							if(is_array($paging_list) && sizeof($paging_list) > 0){
								for($i = 0; $i < sizeof($paging_list); $i++){
									echo '<li';
									if($paging_list[$i]['page'] == $page_current){
										echo ' class="active"';	
									}//end if
									echo '><a href="'.$paging_list[$i]['url'].'">'.$paging_list[$i]['page'].'</a></li>';
								}//end for i
							}//end if
							?>                      
							<li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
						</ul>
					</div>
				</td>
			</tr>
		</tfoot>
		<?php
		}//end if
	}//end if
	?>
    <tbody>
    	<?php
		if($list !== false){
			for($i = 0; $i < sizeof($list); $i++){
			//Switcher
			$switcher = "on";		
			if($list[$i]['user_active'] == "0"){
				$switcher = "off";
			}//end if
			if($list[$i]['user_active'] == 1){
				$status = "Active";
			} else {
				$status = "Not Active";
			}//end if
			$switch_type = "user";
			$id = $list[$i]['user_id'];
		?>
        <tr>
        	<td><i class="icon-user"></i> <?php echo $list[$i]['user_username'].'<span class="table-info-line muted" id="label-'.$id.'">'.$list[$i]['user_firstname']." ".$list[$i]['user_surname'].'</span>' ?></td>    
            <td class="table-row-status"><a href="#" data-id="status_<?php echo $id ?>" data-type="user" class="switcher"<?php
            if(defined('__SWITCH_CUSTOM_API_CALL__')){
				echo ' data-api-call="'.__SWITCH_CUSTOM_API_CALL__.'"';	
			}
			?>><span class="switch-user <?php echo $switcher ?>"><?php echo $status ?></span></a></td>    
            <td class="table-row-options">
            	<div class="btn-group">
                   <a href="<?php echo __MODULE_PATH__."/edit/".$list[$i]['user_id']."/in/".$list[$i]['users_group_id'] ?>" class="btn"><i class="icon-edit"></i></a>
                   <a href="<?php echo __CMS_PATH__."/api/delete.user?id=".$list[$i]['user_id'] ?>&module=<?php echo $module ?>"  data-action="delete" data-ref-id="<?php echo $id ?>" class="btn btn-warning"><i class="icon-trash"></i></a>                   
                </div>
            </td>
        </tr>
        <?php		
			}//end for i
		}//end if
		?>
    </tbody>
</table>
<a href="<?php echo __MODULE_PATH__ ?>/new/in/<?php echo $group_id ?>" class="btn btn-primary">New User</a>
<script>
	$().ready(function() {
        $(".switcher span").jSwicher();
    });
</script>
<div class="modal hide fade" id="delete-item-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Confirm Delete</h3>
    </div>
    <div class="modal-body">
       
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">No, forget it</a>
        <a href="#" class="btn btn-primary" id="confirm-delete">Yes delete it</a>
    </div>
</div>