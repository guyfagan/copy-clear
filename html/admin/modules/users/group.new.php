<?php
	$id = $_REQUEST['id'];	
	if(!is_null($id)){
		$group_data = $users->get_group(array('id' => $id));		
		$group_permissions = $users->get_group_permissions(array('id' => $id));
	}	//end if
	$edit_mode = false;
	$page_title = "New Group";
	if(!is_null($id)){
		$edit_mode = true;	
		$page_title = "Edit Group";
	}//end if
?>
<h4><?php echo $page_title ?></h4>
<form action="<?php echo __CMS_PATH__ ?>api/save.users.group" method="post" name="save-group" id="save-group" enctype="multipart/form-data">
	<input type="hidden" name="module" id="module" value="users" />
    <?php
	if($edit_mode){
	?>
    <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
    <?php
	}//end if
	?>
    <div class="control-group"> 
    	<label class="control-label" for="name">Group Name</label>   	
        <div class="controls">        	
      		<input type="text" class="input-large" id="name" placeholder="Group Name" name="name"<?php 
			if($edit_mode){
				echo ' value="'.$group_data['users_group_name'].'"';	
			}//end if
			?> required />            
        </div>
    </div>
    <label class="checkbox">
    	<input type="checkbox" value="1" id="admin" name="admin"<?php
		if($edit_mode){
			if($group_data['users_group_admin'] == 1){
				echo ' checked';
			}//end if
		}//end if
		?> /> Administrator group
    </label>
    <fieldset>
    	<legend>Modules Permissions</legend>
        <div class="controls controls-row">
        <?php
            $modules = $utils->get_modules();
            if($modules !== false){
                for($i = 0; $i < sizeof($modules); $i++){
            ?>
                <div class="input-prepend clearfix">
                    <span class="add-on"><?php echo $modules[$i]['module_label'] ?></span>
                    <select name="<?php echo $modules[$i]['module_name'] ?>" id="module-perms-<?php echo $modules[$i]['module_name'] ?>">
                        <option value="0"<?php
                        if(isset($group_permissions)){
							if($group_permissions[$modules[$i]['module_name']] == "0" || is_null($group_permissions[$modules[$i]['module_name']])){
								echo ' selected';	
							}//end if
						}//end if
						?>>No Access</option>
                        <option value="1"<?php
                        if(isset($group_permissions)){
							if($group_permissions[$modules[$i]['module_name']] == "1"){
								echo ' selected';	
							}//end if
						}//end if
						?>>Can't Publish</option>
                        <option value="3"<?php
                        if(isset($group_permissions)){
							if($group_permissions[$modules[$i]['module_name']] == "3"){
								echo ' selected';	
							}//end if
						}//end if
						?>>Can Publish Only Own Posts</option>
                        <option value="7"<?php
                        if(isset($group_permissions)){
							if($group_permissions[$modules[$i]['module_name']] == "7"){
								echo ' selected';	
							}//end if
						}//end if
						?>>Full Control</option>
                    </select>
                </div>
                <br />
            <?php		
                }//end for i
            } else {
                
            }//end if
        ?>
        </div>
    </fieldset>
    <button type="submit" id="submit-form" class="btn btn-primary">Save</button>
    <a hrer="#" class="btn">Cancel</a>
</form>