<?php
	$id = $_REQUEST['id'];	
	$has_category = false;
	if(!is_null($_REQUEST['parent_id'])){
		$has_category = true;
		$category_id = $_REQUEST['parent_id'];
	}//end if
	$edit_mode = false;
	$page_title = "New Post";
	$post_status = 0;
	if(!is_null($id)){
		$post = ${$module}->get_post($id);
		$post_status = $post['post_status'];
		$edit_mode = true;	
		$page_title = "Edit Post";
	}//end if
?>
<div id="form-wrapper" class="clearfix">
    <h4><?php echo $page_title ?></h4>    
    <form action="<?php echo __CMS_PATH__ ?>api/save.api.key" method="post" name="save-key" id="save-key" enctype="multipart/form-data" class="clearfix">
    	<div class="compressed-form">
            <input type="hidden" name="module" id="module" value="<?php echo $module ?>" />           
            <?php
            if($edit_mode){
            ?>
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
            <?php
            }//end if           
            ?>
            <fieldset>                
                <div class="control-group">
                    <label>Title</label>
                    <input type="text" name="title" id="title" class="input-block-level" placeholder="Title" required />                  
                </div>
                <label>Subtitle</label>
                <input type="text" name="subtitle" id="subtitle" class="input-block-level" placeholder="Subtitle" />                 
                <button type="submit" id="submit-form" class="btn btn-primary">Save</button>
                <a hrer="#" class="btn">Cancel</a>        
            </fieldset>
        </div>     
    </form>   
</div>