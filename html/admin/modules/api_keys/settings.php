<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->get_settings(array('module' => $module));
	$groups_list = $users->get_groups();
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <div class="form-block">
            <fieldset>
                <legend>Errors handling</legend>
                <label>Database Errors Mode</label>
                <select name="database_error_mode" id="database-error-mode">
                	<option value="error">Return Errors</option>
                    <option value="warning">Return Warnings</option>
                    <option value="silent">Silent Mode</option>
                </select>
                <small class="muted">In Silent mode no errors will be returned</small>
            </fieldset>
            
        </div>            
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
