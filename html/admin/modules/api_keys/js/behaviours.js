// JavaScript Document

$().ready(function(){
	interceptor();
});

function interceptor(){
	//there is no space for Mel Gibson here
	var $bodyAttrs = $("body").attr("class").split(" ");
	for(i = 0; i < $bodyAttrs.length; i++){		
		if(parseInt($bodyAttrs[i].indexOf("action-")) != -1){
			$action = $bodyAttrs[i];			
			$action = $action.replace("action-","");
		}
	}
	
	switch($action){
		case "new":
			new_post_actions();			
			break;
		case "category-new":
			new_category_actions();
			break;
		case "list":
			posts_table_actions();
			break;
		case "settings":
			settings_actions();			
			break;
		case "sort-tool":
			sort_tool_actions();			
			break;
	}//end switch
}

//Specific actions
function new_post_actions(){
	
	/* DISABLE THE ENTER */
	$('input').bind("keyup", function(e) {
		var code = e.keyCode || e.which; 
	 	if (code  == 13) {               
			e.preventDefault();
			return false;
		}
	});
	
	//now check if we have the id of the post, if yes load the data about it
	if($("#id").length > 0){
		$.cmsVars.post_id = $("#id").val();
		$("#save-api").load_post({id: $("#id").val(), module: $.config.module, prefix: "post_", meta: true, ignore: ['message']});
		$(".breadcrumb li:last").text("Edit");
	}//end if
	
	
	if($("#save-post").length > 0){
	
		
		$("#save-post").submit(function(e){
			e.preventDefault();	
			var $url = $(this).attr("action");			
			$.ajax({
				url: $url,
				type: "post",
				data: $(this).serialize(),
				dataType:"json",
				success: function(data){
					if(data.result == "success"){
						location.href = $.config.basepath + 'admin/'+$.config.module+'/';
					} else {
						$("#save-post").popMessage("The key has not been saved due to a server error",{type: "error"});
					}
				}	
			});
			$(window).scrollTop(0);
		});
		
	
		
		$("#submit-form").click(function(e){
			e.preventDefault();
			var isValidated = $("#save-api").validate();
			if(isValidated){
				$("#save-post").submit();	
			} else {
				$("#save-post").popMessage("Please fill the fields in red first!",{type: "error"});
			}
		});
				
		function getPostID(){
			var $id = $("#id").val();
			return $id;
		}
	}//end if
}//end function



function posts_table_actions(){
	//clone the buttons at the bottom of the table on the top of the table
	$('.table').before($('.buttons-bar').clone());
	
	//capture the delete action
	if($('a[data-action="delete"]').length > 0){
		$('a[data-action="delete"]').click(function(e){
			e.preventDefault();
			var $url = $(this).attr("href");
			var $tableLine = $(this).closest("tr");
			var $id = $(this).data('ref-id');
			var $label = $("#label-"+$id).text();
			$('#delete-item-modal').unbind('show');
			$('#delete-item-modal').on('show',function(){
				$('.modal-body',$(this)).html('Are you sure that you want to delete the entry <strong>&quot;'+$label+'&quot;</strong>');
				$('#confirm-delete').unbind('click');
				$('#confirm-delete').click(function(e){
					e.preventDefault();					
					$.ajax({
						url : $url,
						type: "get",
						dataType: "json",
						success: function(data){					
							if(data.result == "success"){
								$('#delete-item-modal').modal('hide');
								$tableLine.fadeOut("slow",function(){
									$(this).remove();										
								});	
							}
						}
					});
				});
			}).modal({ backdrop: true });
	
		});
	}
}//end function

function settings_actions(){
	
	$("#module-settings").load_settings({module: $.config.module});
	
	$("#module-settings").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				if(data.result == "success"){
					$("#module-settings").popMessage("The settings has been saved successfully",{type: "success"});
					$.cmsVars.formSaved = true;
				
				} else {
					$("#module-settings").popMessage("The settings has not been saved due to a server error",{type: "error"});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#module-settings").validate();
		if(isValidated){
			$("#module-settings").submit();	
		} else {
			$("#module-settings").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
}