<?php
	$list = ${$module}->get_keys();
?>
<table class="table table-striped table-hover">
	<thead>
        <tr>
            <th>Key</th>
            <th>Description</th>
            <th>Domains</th>
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tbody>
    	<?php
		if($list !== false){
			for($i = 0; $i < sizeof($list); $i++){
		?>
        <tr>
        	<td><?php echo $list[$i]['api_key_sha1'] ?></td>
            <td><?php echo $list[$i]['api_key_descr'] ?></td>
            <td><?php echo $list[$i]['api_key_domain_allowed'] ?></td>
            <td class="table-row-options">
            	<div class="btn-group pull-right table-button-cta">
                    <a href="<?php echo __MODULE_PATH__."/".$list[$i]['id'] ?>/edit/" class="btn"><i class="icon-pencil"></i></a>
                    <a href="<?php echo __MODULE_PATH__."/".$list[$i]['id'] ?>/delete/" class="btn btn-danger"><i class="icon-trash"></i></a>      
                </div>             
            </td>
        </tr>
        <?php		
			}//end for i
		}//end if
		?>
    </tbody>
</table>
<div class="buttons-bar">
    <a href="#" class="btn btn-primary">New Key</a>
</div>

<div class="modal hide fade" id="delete-item-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Confirm Delete</h3>
    </div>
    <div class="modal-body">
       
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">No, forget it</a>
        <a href="#" class="btn btn-primary" id="confirm-delete">Yes delete it</a>
    </div>
</div>