<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->get_settings(array('module' => $module));
	$groups_list = $users->get_groups();
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>    
        <div class="form-block">
        	<fieldset>
            	<legend>Notifications</legend>
                <label>Send notification emails with the following address</label>
                <input type="text" name="submission_no_reply" id="submission_no_reply" class="input-xxlarge" />
                <label>Once a new submission is created submit an email to</label>
                <input type="text" name="submission_notification_email" id="submission_notification_email" class="input-xxlarge" />
                <label>Also Bcc the following addresses</label>
                <input type="text" name="submission_bcc_on_notification" id="submission_bcc_on_notification" class="input-xxlarge" />
                <label>Email type</label>
                <select name="submission_email_type" id="submission_email_type">
                	<option value="text">Plain Text</option>
                    <option value="html">Html</option>
                </select>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'submission_template';
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>New submission email subject</label>
                <input type="text" name="submission_email_subject" id="submission_email_subject" class="input-xxlarge" />
                <div class="control-group">
                    <label>New submission email message</label>
                    <textarea data-ckeditor="true" data-ckeditor-height="200" id="submission_email_message" name="submission_email_message"></textarea>
                </div>
                <label class="checkbox">
                	<input type="checkbox" name="submission_notify_user" id="submission_notify_user" value="1"> Also notify the user
                </label>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'submission_user_template';
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>New submission user notification subject</label>
                <input type="text" name="submission_user_email_subject" id="submission_user_email_subject" class="input-xxlarge" />
                <div class="control-group">
                    <label>New submission user notification message</label>
                    <textarea data-ckeditor="true" data-ckeditor-height="200" id="submission_user_email_message" name="submission_user_email_message"></textarea>
                </div>  
                <label class="checkbox">
                	<input type="checkbox" name="submission_notify_status_change" id="submission_notify_status_change" value="1"> Also notify the user when the status of his submission changes
                </label>   
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'submission_status_change_template';
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'submission_status_finalapprove_template';
					$emailTemplateLabel = 'In case of Final Approve status, use the following template instead';
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>Submission status change notification subject</label>
                <input type="text" name="submission_status_email_subject" id="submission_status_email_subject" class="input-xxlarge" />
                <div class="control-group">
                    <label>Submission status change notification message</label>
                    <textarea data-ckeditor="true" data-ckeditor-height="200" id="submission_status_email_message" name="submission_status_email_message"></textarea>
                </div>            
            </fieldset>
            <fieldset>
            	<legend>Comments</legend>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="notify_new_comments" id="notify-new-comments"> Notify admin users about new comments
                </label>
                <label>Notify the following users group</label>
                <select name="notify_new_comments_users" id="notify-new-comments-users">
                <?php
					$users_groups = $users->get_groups();
					if($users_groups !== false){
						for($i = 0; $i < sizeof($users_groups); $i++){
				?>
                	<option value="<?php echo $users_groups[$i]['users_group_id'] ?>"><?php echo $users_groups[$i]['users_group_name'] ?></option>
                <?php			
						}//end for i
					}//end if
				?>
                </select>                
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'notify_new_comment_email_template';					
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>New comments notification email subject</label>
                <input type="text" name="notify_new_comments_email_subject" id="notify-new-comments-email-subject" class="input-xxlarge">
                <label>New comments notification email message</label>
                <textarea name="notify_new_comments_email_message" id="notify-new-comments-email-message" data-ckeditor="true" data-ckeditor-height="150"><?php
				if(isset($settings['notify_new_comments_email_message'])){
					echo $settings['notify_new_comments_email_message'];	
				}//end if
				?></textarea>
            
            </fieldset>
        </div>
        <div class="form-block">
            <fieldset>
                <legend>Module Status</legend>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-public" value="public" />
                    Public                
                </label>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-private" value="private" />
                    Private                
                </label>
            </fieldset>
            <fieldset>
            	<legend>Site Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_searchable" id="module_searchable">
                    Make the module searchable in the public search             
                </label>
            </fieldset>
            <fieldset>
            	<legend>CMS Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_local_search" id="module_local_search">
                    Enable local search<br />
                    <small class="muted">It will search only on the current module</small>         
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="show_refine_toolbar" id="show_refine_toolbar">
                    Show refine search toolbar
                </label>
                <label>Local search result page</label>
                <select name="module_local_search_view" id="module_local_search_view">
                <?php
				$files = $utils->get_folder_files(array('path' => '/admin/modules/'.$utils->view->module.'/'));
				if($files === false){
					$files = $utils->get_folder_files(array('path' => '/admin/modules/generic/'));	
				}//end if
				if($files !== false){
					for($i = 0; $i < sizeof($files); $i++){
						$search_action = str_replace(".php","",$files[$i]);
						$search_action = str_replace(".","/",$search_action);
						echo '<option value="'.$search_action.'">'.$search_action.'</option>';
					}//end for i
				}//end if
				?>
                </select>
                <br />
                <small class="muted">The search results will be displayed in the selected view</small>
            </fieldset>
        </div>     
        <div class="form-block">
        	<fieldset>
            	<legend>Posts</legend>
                <label>
                	General Sort Order
                    <select id="list_order" name="list_order">
                    	<option value="DESC">Newer first</option>
                        <option value="ASC">Older first</option>
                    </select>
                </label>
                <br />
                <label>
                	General Sort System
                    <select id="list_system" name="list_system">
                    	<option value="natural">Natural (default system)</option>
                        <option value="date">Order by Date</option>
                    </select>
                </label>
                <br />
                <label class="checkbox">
                    <input type="checkbox" value="1" name="show_thumbnails" id="show_thumbnails">
                    Show posts thumbnails if available          
                </label>
                <br />
                <label class="checkbox">
                    <input type="checkbox" value="1" name="enable_bulk_actions" id="enable_bulk_actions">
                    Enable bulk actions   
                </label>
                <br />
                <label class="checkbox">
                    <input type="checkbox" value="1" name="disable_categories" id="disable_categories">
                    Disable categories 
                </label>
                <br />
                <label>
                	List view type
                    <select id="list_view_type" name="list_view_type">
                    	<option value="large">Expanded (shows thumbnails, author and date)</option>
                        <option value="small">Compressed (show minimal information about the post)</option>
                    </select>
                </label>
                <br />
                <label>
                	List view mode <small class="muted">Experimental, do not use in modules already filled with posts</small>
                    <select id="list_view_mode" name="list_view_mode">
                    	<option value="default">Default mode (suggested)</option>
                        <option value="wp-like">Wordpress-like</option>
                    </select>
                </label>
                <br />
                <label>
                	Elements per page
                    <select id="items_per_page" name="items_per_page">
                    	<option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="0">Don't paginate and show everything</option>
                    </select>
                </label>
                <br />
                <?php
				require_once('inc/components/templates/templates.settings.php');
				?>
        	</fieldset>
            <fieldset>
            	<legend>Users Restrictions</legend>
                <p>Here it can be set what the users see and what they can't.</p>
                <label>What posts the user can see?</label>
                <select name="usersActionsAllowed" id="users-actions-allowed" data-toggle="optional-field" class="input-xxlarge">
                	<option value="all"<?php if($settings['usersActionsAllowed'] == 'all'){ echo ' selected'; } ?>>All</option>
                    <option value="same-group"<?php if($settings['usersActionsAllowed'] == 'same-group'){ echo ' selected'; } ?>>Only posts made by users of the same group</option>
                    <option value="only-himself"<?php if($settings['usersActionsAllowed'] == 'only-himself'){ echo ' selected'; } ?>>Only the posts made by the current user</option>
                    <option value="himself-plus"<?php if($settings['usersActionsAllowed'] == 'himself-plus'){ echo ' selected'; } ?>>Only the posts made by the current user and by the users of the following groups</option> 
                </select>       
               
                <div data-type="template-optional-field" data-template-target="#users-actions-allowed" data-template-match="himself-plus">	
                	<label>Show the posts from the users of the following groups</label>                    
                    <input type="text" name="usersShowableGroups" id="users-showable-groups" class="input-xxlarge" />
                    <label class="checkbox">
                    	<input type="checkbox" name="usersShowableGroupsOnlyPublished" id="users-showable-groups-only-published" value="1" />
                        Show only the posts marked as "Published" from these groups
                    </label>                    
                </div>
                <label class="checkbox">
                    <input type="checkbox" name="usersOnlyAdminCanWrite" id="users-only-admin-can-write" value="1"  data-toggle="optional-field"<?php if($settings['usersOnlyAdminCanWrite'] == '1'){ echo ' checked="checked"'; } ?> />
                    Only admins or any of the groups below can add new content
                </label>
                <div data-type="template-optional-field" data-template-target="#users-only-admin-can-write" data-template-match="1">
                	<label>Give writing permissions to the following groups:</label>                    
                    <input type="text" name="usersWriteGroups" id="users-write-groups" class="input-xxlarge" />
                </div>
            </fieldset>
        </div>
        <?php
			$sys_image_formats = $utils->get_attachments_formats(array("default" => false));
			if($sys_image_formats !== false){
		?>
        <div class="form-block">
            <fieldset>
                <legend>Images</legend>
                <?php
					$saved_image_settings = $utils->get_settings(array("module_id" => $utils->view->module));
					$saved_image_settings = $saved_image_settings['moduleDefaultImageFormats'];
					if(!is_null($saved_image_settings)){
						$saved_formats = explode(",",$saved_image_settings);
					}//end if					
					
				?>
                	<input type="hidden" name="moduleDefaultImageFormats" id="module-default-formats"<?php 
					if(isset($saved_image_settings) && !is_null($saved_image_settings)){
						echo ' value="'.$saved_image_settings.'"';	
					}//end if
					?> />
                	<p>                    	
                        By default there are two formats: Thumbnail and Large, which are system defauls formats and cannot be deactived. But you can always add new formats by adding them in the <a href="<?php echo __CMS_PATH__ ?>media/settings/">Media Settings</a> and then activate them here.<br />
                        Please select the image formats you want to activate for this module when a picture is uploaded.
                    </p>  
                    <?php
					echo '<div class="settings-format-container">';
					for($i = 0; $i < sizeof($sys_image_formats); $i++){
						$format_class = '';
						$format_status = 'Disabled';
						if(isset($saved_formats) && is_array($saved_formats)){
							if(in_array($sys_image_formats[$i]['attachment_setting_id'],$saved_formats)){
								$format_class = ' btn-success active';	
								$format_status = 'Enabled';
							}
						}//end if
						$width = (int)$sys_image_formats[$i]['attachment_setting_width'];
						if(is_numeric($width) && $width > 0){
							$width .= "px";	
						} else {
							$width = "auto";
						}//end if
						
						$height = (int)$sys_image_formats[$i]['attachment_setting_height'];
						if(is_numeric($height) && $height > 0){
							$height .= "px";	
						} else {
							$height = "auto";
						}//end if
					?> 
                    <div class="setting-format-box">
                    	 
                        <p><strong><?php echo $sys_image_formats[$i]['attachment_setting_label'] ?></strong><br />Width: <?php echo $width ?>, Height: <?php echo $height ?>, Processing: <?php echo ucwords($sys_image_formats[$i]['attachment_setting_processing']) ?>
                        </p>
                        <button type="button" class="btn<?php echo $format_class ?>" data-toggle="button" data-format-id="<?php echo $sys_image_formats[$i]['attachment_setting_id'] ?>" data-btn-action="toggle-formats"><?php echo $format_status ?></button> 
					</div>	
					<?php
					}//end for i
					echo '</div>';			
					?>                                     
            		              
            </fieldset>
        </div>
        <?php
			}//end if
		?>
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
<script>
	$().ready(function(){
		$('button[data-btn-action=toggle-formats]').click(function(e){
			if($(this).hasClass('active')){
				$(this).removeClass('btn-success');	
				$(this).text("Disabled");			
			} else {				
				$(this).addClass('btn-success');	
				$(this).text("Enabled");
			}
		
			$selectedIds = new Array();
			
			$('button[data-btn-action=toggle-formats]').each(function(){
				if($(this).hasClass("btn-success")){
					$selectedIds.push($(this).attr("data-format-id"));
				}
			});
			
			//console.log($selectedIds);
			$("#module-default-formats").val($selectedIds);			
		});	
		
		$('#users-showable-groups').tokenfield({
		  autocomplete: {
			source: <?php			
			if($groups_list !== false){
				$groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){
					array_push($groups,array('value' => $groups_list[$i]['users_group_id'], 'label' => $groups_list[$i]['users_group_name']));
				}//end for i
				echo json_encode($groups);
			}
			?>,
			delay: 100
		  },
		  <?php
			if(isset($settings['usersShowableGroups'])){
				$saved_groups = explode(",",$settings['usersShowableGroups']);	
				$show_groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){							
					if(in_array((int)$groups_list[$i]['users_group_id'],$saved_groups)){
						array_push($show_groups,array('value' => $groups_list[$i]['users_group_id'],'label'=> $groups_list[$i]['users_group_name']));	
					}//end if
				}//end if
				echo 'tokens:';
				echo json_encode($show_groups);
				echo ',';
			}//end if
			?>
		  showAutocompleteOnFocus: true,
		  allowDuplicates: false
		});
		
		$('#users-write-groups').tokenfield({
		  autocomplete: {
			source: <?php			
			if($groups_list !== false){
				$groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){
					array_push($groups,array('value' => $groups_list[$i]['users_group_id'], 'label' => $groups_list[$i]['users_group_name']));
				}//end for i
				echo json_encode($groups);
			}
			?>,
			delay: 100
		  },
		  <?php
			if(isset($settings['usersWriteGroups'])){
				$saved_groups = explode(",",$settings['usersWriteGroups']);	
				$show_groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){							
					if(in_array((int)$groups_list[$i]['users_group_id'],$saved_groups)){
						array_push($show_groups,array('value' => $groups_list[$i]['users_group_id'],'label'=> $groups_list[$i]['users_group_name']));	
					}//end if
				}//end if
				echo 'tokens:';
				echo json_encode($show_groups);
				echo ',';
			}//end if
			?>
		  showAutocompleteOnFocus: true,
		  allowDuplicates: false
		})

	});
</script>