<?php
	$import = $utils->call('ccci_import');
	
	if($method == "ccci.import.users"){
		$data = array();
		$result = $import->do_import();
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "ccci.import.brands"){
		$data = array();
		$result = $import->do_import('brands');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if

	if($method == "ccci.import.advertisers"){
		$data = array();
		$result = $import->do_import('advertisers');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "ccci.import.campaigns"){
		$data = array();
		$result = $import->do_import('campaigns');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "ccci.import.comments"){
		$data = array();
		$result = $import->do_import('comments');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "ccci.import.submissions"){
		$data = array();
		$result = $import->do_import('submissions');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "ccci.import.ads"){
		$data = array();
		$result = $import->do_import('ads');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "ccci.import.codes"){
		$data = array();
		$result = $import->do_import('codes');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "ccci.generate.relations"){
		$data = array();
		$result = $import->do_import('relations');
		if($result){
			$data['result'] = 'success';	
		} else {
			$data['result'] = 'error';
		}//end if
		echo json_encode($data);
	}//end if
?>