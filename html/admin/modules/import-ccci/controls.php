<h4>Import CCCI</h4>
<h5>Users & Companies</h5>
<div class="clearfix" id="import-users-container">
<a href="#" class="btn btn-danger" id="import-users">Import Users</a>
</div>
<hr />
<h5>Brands</h5>
<div class="clearfix" id="import-brands-container">
<a href="#" class="btn btn-danger" id="import-brands">Import Brands</a>
</div>
<hr />
<h5>Advertisers (Brand Owners)</h5>
<div class="clearfix" id="import-advertisers-container">
<a href="#" class="btn btn-danger" id="import-advertisers">Import Advertisers</a>
</div>
<hr />
<h5>Campaigns</h5>
<div class="clearfix" id="import-campaigns-container">
<a href="#" class="btn btn-danger" id="import-campaigns">Import Campaigns</a>
</div>
<hr />
<h5>Comments</h5>
<div class="clearfix" id="import-comments-container">
<a href="#" class="btn btn-danger" id="import-comments">Import Comments</a>
</div>
<hr />
<h5>Submissions</h5>
<div class="clearfix" id="import-submissions-container">
<a href="#" class="btn btn-danger" id="import-submissions">Import Submissions</a>
</div>
<hr />
<h5>Ads</h5>
<div class="clearfix" id="import-ads-container">
<a href="#" class="btn btn-danger" id="import-ads">Import Ads</a>
</div>
<hr />
<h5>Approval Codes</h5>
<div class="clearfix" id="import-codes-container">
<a href="#" class="btn btn-danger" id="import-codes">Import Codes</a>
</div>
<hr />
<h5>Generate relationships between Brands/Brands Owner and Companies</h5>
<div class="clearfix" id="import-relations-container">
<a href="#" class="btn btn-danger" id="import-relations">Generate</a>
</div>
<script>
	$(document).ready(function(e) {
        $("#import-users").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.users?module=import-ccci',
				dataType:"json",
				success: function(data){
					console.log(data);
					if(data.result == 'success'){
						$('#import-users-container').append('<div class="alert alert-success"><strong>Success!</strong> The users and companies have been imported correctly</div>');
					} else {
						$('#import-users-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-brands").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.brands?module=import-ccci',
				dataType:"json",
				success: function(data){
					console.log(data);
					if(data.result == 'success'){
						$('#import-brands-container').append('<div class="alert alert-success"><strong>Success!</strong> The brands have been imported correctly</div>');
					} else {
						$('#import-brands-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-advertisers").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.advertisers?module=import-ccci',
				dataType:"json",
				success: function(data){					
					if(data.result == 'success'){
						$('#import-advertisers-container').append('<div class="alert alert-success"><strong>Success!</strong> The advertisers have been imported correctly</div>');
					} else {
						$('#import-advertisers-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-campaigns").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.campaigns?module=import-ccci',
				dataType:"json",
				success: function(data){				
					if(data.result == 'success'){
						$('#import-campaigns-container').append('<div class="alert alert-success"><strong>Success!</strong> The campaigns have been imported correctly</div>');
					} else {
						$('#import-campaigns-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-comments").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.comments?module=import-ccci',
				dataType:"json",
				success: function(data){					
					if(data.result == 'success'){
						$('#import-comments-container').append('<div class="alert alert-success"><strong>Success!</strong> The comments have been imported correctly</div>');
					} else {
						$('#import-comments-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-submissions").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.submissions?module=import-ccci',
				dataType:"json",
				success: function(data){					
					if(data.result == 'success'){
						$('#import-submissions-container').append('<div class="alert alert-success"><strong>Success!</strong> The submissions have been imported correctly</div>');
					} else {
						$('#import-submissions-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-ads").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.ads?module=import-ccci',
				dataType:"json",
				success: function(data){					
					if(data.result == 'success'){
						$('#import-ads-container').append('<div class="alert alert-success"><strong>Success!</strong> The ads have been imported correctly</div>');
					} else {
						$('#import-ads-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-codes").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.import.codes?module=import-ccci',
				dataType:"json",
				success: function(data){					
					if(data.result == 'success'){
						$('#import-codes-container').append('<div class="alert alert-success"><strong>Success!</strong> The approval codes have been imported correctly</div>');
					} else {
						$('#import-codes-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
		
		$("#import-relations").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'ccci.generate.relations?module=import-ccci',
				dataType:"json",
				success: function(data){					
					if(data.result == 'success'){
						$('#import-relations-container').append('<div class="alert alert-success"><strong>Success!</strong> The relations have been generated correctly</div>');
					} else {
						$('#import-relations-container').append('<div class="alert alert-danger"><strong>Error!</strong> Something went in the wrong direction.</div>');
					}//end if
				}
			});
		});
    });
</script>