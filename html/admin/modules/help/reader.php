<div class="help-library">
<?php
	$book = $_GET['book'];
	
	require_once('inc/markDown/1.3/Markdown.php');
	use \Michelf\Markdown;
	
	if(file_exists('modules/help/library/'.$book.'.md')){	
		$my_text = file_get_contents('modules/help/library/'.$book.'.md');
		$my_html = Markdown::defaultTransform($my_text);		
		echo $my_html;
	}//end if
?>
	<hr />
    <a href="<?php echo __MODULE_PATH__ ?>/" class="btn">&lt; Back to index</a>
</div>