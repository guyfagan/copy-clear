<h2>Help</h2>
<?php
	$help = $utils->call('help');
	$library = help::get_library();
	if($library !== false){
		echo '<ul>';
		for($i = 0; $i < sizeof($library); $i++){
			$book = $library[$i]['file'];
			$book = substr($book,0,-3);
			echo '<li><a href="'.__MODULE_PATH__.'/reader/?book='.$book.'">'.ucwords($book).'</a></li>';
		}//end for i
		echo '</ul>';
	}//end if
?>