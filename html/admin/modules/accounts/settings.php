<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->get_settings(array('module' => $module));
	$groups_list = $users->get_groups();
	
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <div class="form-block">
            <fieldset>
                <legend>Module Status</legend>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-public" value="public" />
                    Public                
                </label>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-private" value="private" />
                    Private                
                </label>
            </fieldset>                     
            <fieldset>
            	<legend>Site Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_searchable" id="module_searchable">
                    Make the module searchable in the public search             
                </label>
            </fieldset>
            <fieldset>
            	<legend>CMS Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_local_search" id="module_local_search">
                    Enable local search<br />
                    <small class="muted">It will search only on the current module</small>         
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="show_refine_toolbar" id="show_refine_toolbar">
                    Show refine search toolbar
                </label>
                <label>Local search result page</label>
                <select name="module_local_search_view" id="module_local_search_view">
                <?php
				$files = $utils->get_folder_files(array('path' => '/admin/modules/'.$utils->view->module.'/'));
				if($files === false){
					$files = $utils->get_folder_files(array('path' => '/admin/modules/generic/'));	
				}//end if
				if($files !== false){
					for($i = 0; $i < sizeof($files); $i++){
						$search_action = str_replace(".php","",$files[$i]);
						$search_action = str_replace(".","/",$search_action);
						echo '<option value="'.$search_action.'">'.$search_action.'</option>';
					}//end for i
				}//end if
				?>
                </select>
                <br />
                <small class="muted">The search results will be displayed in the selected view</small>
            </fieldset>
        </div> 
        <div class="form-block">
        	<fieldset>
            	<legend>Accounts</legend>
                <label>Default users group for accounts</label>
                <select name="default_users_group" id="users-group">
               	<?php			
				$users_groups = ${$module}->get_groups();
				if($users_groups !== false){
					for($i = 0; $i < sizeof($users_groups); $i++){
						echo '<option value="'.$users_groups[$i]['users_group_id'].'">'.$users_groups[$i]['users_group_name'].'</option>';
					}//end for i
				}//end if
				?>
                </select>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="account_auto_activation" id="auto-activation"> Active the user straight after the registration
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="account_email_activation" id="email-activation"  data-toggle="optional-field" <?php
                    if($settings['account_email_activation'] == 1){
						echo ' checked';	
					}//end if
					?>> New users must validate their account before login
                </label>
                <div data-type="template-optional-field" data-template-target="#email-activation" data-template-match="1">
                	<label>Activation address</label>
                    <input type="text" name="account_activation_uri" id="activation-uri" class="input-xxlarge" placeholder="<?php echo $utils->view->settings['siteAddress'] ?>">
                </div>
            </fieldset>   
            <fieldset>
            	<legend>Notifications</legend>
                <label>Send notification emails with the following address <small class="muted">(If left black the default system email will be used)</small></label>
                <input type="text" name="account_no_reply" id="account-no-reply" class="input-xxlarge" />
                <label>Once a new account is created submit an email to</label>
                <input type="text" name="account_notification_email" id="account_notification_email" class="input-xxlarge" />
                <label>Also Bcc the following addresses</label>
                <input type="text" name="account_bcc_on_notification" id="account_bcc_on_notification" class="input-xxlarge" />
                <label>Email type</label>
                <select name="account_email_type" id="account_email_type">
                	<option value="text">Plain Text</option>
                    <option value="html">Html</option>
                </select>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					require_once('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <hr />
                <h4>Email sent once a user create a new account</h4>
                <label>New account email subject</label>
                <input type="text" name="account_email_subject" id="account_email_subject" class="input-xxlarge" />
                <div class="control-group">
                    <label>New account email</label>
                    <textarea data-ckeditor="true" data-ckeditor-height="100" id="new-account-email-message" name="account_new_email_message">
                    <?php
                    if(isset($settings['account_new_email_message'])){
                        echo $settings['account_new_email_message'];	
                    }//end if
                    ?>
                    </textarea>         
                </div>       
                <small>Use the tag <span class="label">{#activation#}</span> to add the activation link in the email. The activation link <strong>MUST</strong> be present in the email text if the user is not activated straight after the registration. <button type="button" class="btn btn-mini" id="add-activation-link" data-target="#new-account-email-message" data-tag="{#activation#}">Add to the text</button></small>
            </fieldset>
        </div>
            
        <div class="form-block">
        	<fieldset>
            	<legend>View Options</legend>
                <label>
                	General Sort Order
                    <select id="list_order" name="list_order">
                    	<option value="DESC">Newer first</option>
                        <option value="ASC">Older first</option>
                    </select>
                </label>
                <br />
                <label>
                	General Sort System
                    <select id="list_system" name="list_system">
                    	<option value="natural">Natural (default system)</option>
                        <option value="date">Order by Date</option>
                    </select>
                </label>
                <br />
                <label class="checkbox">
                    <input type="checkbox" value="1" name="show_thumbnails" id="show_thumbnails">
                    Show posts thumbnails if available          
                </label>
                <br />
                <label class="checkbox">
                    <input type="checkbox" value="1" name="enable_bulk_actions" id="enable_bulk_actions">
                    Enable bulk actions   
                </label>
                <br />
                <label class="checkbox">
                    <input type="checkbox" value="1" name="disable_categories" id="disable_categories">
                    Disable categories 
                </label>
                <br />
                <label>
                	List view type
                    <select id="list_view_type" name="list_view_type">
                    	<option value="large">Expanded (shows thumbnails, author and date)</option>
                        <option value="small">Compressed (show minimal information about the post)</option>
                    </select>
                </label>
                <br />
                <label>
                	List view mode <small class="muted">Experimental, do not use in modules already filled with posts</small>
                    <select id="list_view_mode" name="list_view_mode">
                    	<option value="default">Default mode (suggested)</option>
                        <option value="wp-like">Wordpress-like</option>
                    </select>
                </label>
                <br />
                <label>
                	Elements per page
                    <select id="items_per_page" name="items_per_page">
                    	<option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="0">Don't paginate and show everything</option>
                    </select>
                </label>                
        	</fieldset>           
        </div>       
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
<script>
	$().ready(function(){	
		$("#add-activation-link").click(function(e){
			e.preventDefault();
			var $target = $(this).data('target');
			var $tag = $(this).data('tag');
			$($target).val($($target).val()+'<p>'+$tag+'</p>');	
		});

	});
</script>