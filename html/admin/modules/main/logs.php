<?php
	$paginated_results = true;
	if(!isset($_GET['tab'])){
		$tab = 'mail';
	} else {
		$tab = $_GET['tab'];	
	}
?>
<div class="logs">
    <ul class="nav nav-tabs" id="myTab">
        <li<?php
        if($tab == 'mail'){
			echo ' class="active"';	
		}//end if
		?>><a href="#mail" data-toggle="tab">Mail Log</a></li>
        <!--li<?php
        if($tab == 'errors'){
			echo ' class="active"';	
		}//end if
		?>><a href="#errors" data-toggle="tab">Errors Log</a></li-->
        <li<?php
        if($tab == 'system'){
			echo ' class="active"';	
		}//end if
		?>><a href="#system" data-toggle="tab">System Log</a></li>   
        <li<?php
        if($tab == 'blacklist'){
			echo ' class="active"';	
		}//end if
		?>><a href="#blacklist" data-toggle="tab">Blacklist Log</a></li>     
    </ul>
    <div class="tab-content">
    	<div class="tab-pane<?php
        if($tab == 'mail'){
			echo ' active';	
		}//end if
		?>" id="mail">			
            <table class="table table-striped table-hover" id="mail-log-table" data-api-call="get.mail.logs">
                <thead>
                    <tr>
                        <th>Sent from</th>  
                        <td width="15">&nbsp;</td>  
                        <th>Sent to</th>     
                        <th>Subject</th>
                    </tr>
                </thead> 
                <tfoot class="pages">
                    <tr>
                        <td colspan="4">
                            <div class="pagination">
                                <ul>
                                    <!--li><a href="?page={#prev_page#}" data-type="prev">&laquo;</a></li-->
                                    <li><a href="?page={#pageN#}" data-type="counter" data-page="{#pageN#}">{#pageN#}</a></li>
                                    <!--li><a href="?page={#next_page#}" data-type="next">&raquo;</a></li-->                                   
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>                       
                <tbody class="table-template">                
                	<tr>
                        <td>{#mail_log_sender#}<br /><small class="muted">{#mail_log_date#}</small></td>
                        <td width="15"><i class="icon-long-arrow-right"></i></td>
                        <td>{#recipients#}</td>
                        <td><a href="#" data-msg-id="{#mail_log_id#}" data-role="load-mail">{#mail_log_subject#}</a></td>
                    </tr>
                </tbody>
            </table>           
		</div><!-- /#mail -->
        <div class="tab-pane<?php
        if($tab == 'mail'){
			echo ' errors';	
		}//end if
		?>" id="errors">
        </div><!-- /#errors -->
        <div class="tab-pane<?php
        if($tab == 'system'){
			echo ' active';	
		}//end if
		?>" id="system">   
        	<a href="#system" class="btn btn-success" id="refresh-system-log"><i class="icon icon-refresh"></i> Refresh</a>     	
            <table class="table table-striped table-hover" id="system-logs-table" data-api-call="get.system.logs">
                <thead>
                    <tr>
                    	<th>Date</th>
                        <th>Call</th> 
                        <th>IP</th>     
                        <th>User</th>
                    </tr>
                </thead>                
                <tfoot class="pages">
                    <tr>
                        <td colspan="4">
                            <div class="pagination">
                                <ul>
                                    <!--li><a href="?page={#prev_page#}" data-type="prev">&laquo;</a></li-->
                                    <li><a href="?page={#pageN#}" data-type="counter" data-page="{#pageN#}">{#pageN#}</a></li>
                                    <!--li><a href="?page={#next_page#}" data-type="next">&raquo;</a></li-->                                   
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>   
                <tbody class="table-template">               
                	<tr>
                    	<td><i class="icon icon-calendar"></i> {#system_log_date#}</td>
                        <td><a href="#" data-msg-id="{#system_log_id#}" data-role="load" data-api-call="get.log.details">{#system_log_type#}</a></td>
                        <td><i class="icon icon-globe"></i> {#system_log_ip#}</td>
                        <td>{#user#}</td>
                    </tr>
                </tbody>
            </table>
            <a href="<?php echo __CMS_PATH__ ?>api/empty.system.logs" class="btn btn-danger" id="empty-sy-logs-btn"><i class="icon-trash"></i> Empty System Logs</a>      
        </div><!-- /#system -->
        <div class="tab-pane<?php
        if($tab == 'mail'){
			echo ' errors';	
		}//end if
		?>" id="errors">
        </div><!-- /#errors -->
        <div class="tab-pane<?php
        if($tab == 'blacklist'){
			echo ' active';	
		}//end if
		?>" id="blacklist">        	
            <table class="table table-striped table-hover" id="blacklist-logs-table" data-api-call="get.blacklist.logs">
                <thead>
                    <tr>
                    	<th>Date</th>
                        <th>IP</th>                            
                    </tr>
                </thead>                
                <tfoot class="pages">
                    <tr>
                        <td colspan="4">
                            <div class="pagination">
                                <ul>
                                    <!--li><a href="?page={#prev_page#}" data-type="prev">&laquo;</a></li-->
                                    <li><a href="?page={#pageN#}" data-type="counter" data-page="{#pageN#}">{#pageN#}</a></li>
                                    <!--li><a href="?page={#next_page#}" data-type="next">&raquo;</a></li-->                                   
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>   
                <tbody class="table-template">               
                	<tr>
                    	<td><i class="icon icon-calendar"></i> {#blacklist_date#}</td>                                    
                        <td><i class="icon icon-globe"></i> {#blacklist_ip#}</td>                        
                    </tr>
                </tbody>
            </table>
            <a href="<?php echo __CMS_PATH__ ?>api/empty.blacklist.logs" class="btn btn-danger" id="empty-bl-logs-btn"><i class="icon-trash"></i> Empty Blacklist</a>      
        </div><!-- /#system -->
	</div>
</div>

<script>
	$().ready(function(){			
		$('#myTab li.active a[data-toggle="tab"]').loadTab();		
		$('a[data-toggle="tab"]').on('shown', function (e) {
			//e.target; // activated tab
			//e.relatedTarget; // previous tab		
			$(this).loadTab();			
		});			
		$("#refresh-system-log").click(function(e){
			e.preventDefault();
			$(this).loadTab();
		});
	});	
</script>

<div class="modal hide fade" id="log-detail-modal" style="width: 80%; margin-left: -40%;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Log Details</h3>
    </div>
    <div class="modal-body" style="min-height: 40%; max-height: 80%;">
       <iframe frameborder="0" width="100%" height="100%" id="iframe-content"></iframe>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">Close</a>
    </div>
</div>