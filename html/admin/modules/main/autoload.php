<?php
	$js_libs_autoload = array();
	$js_libs_autoload['dashboard'] = array(
										"inc/chart/".__CHARTJS_VERSION__."/Chart.min.js"																	
									);
	$js_libs_autoload['settings'] = array(
										"js/cms.formdata.jquery.js",
										array(
											"type" => "css", 
											"path" => "inc/tokenfield/".__BOOTSTRAP_TOKENFIELD__."/bootstrap-tokenfield.css"
										),	
										"inc/tokenfield/".__BOOTSTRAP_TOKENFIELD__."/bootstrap-tokenfield.js"											
									);
	$js_libs_autoload['logs'] = array(
										"inc/jloader/jloader.jquery.js"									
									);									
	$js_libs_autoload['module.new'] = array(
										"js/cms.formdata.jquery.js",
										"js/cms.permalink.jquery.js"							
									);
?>