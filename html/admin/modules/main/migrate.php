<form action="<?php echo __CMS_PATH__ ?>api/migrate.cms" method="post" name="migration-form" id="migration-form">
	<h2>Migration Tool</h2> 
    <label>Database Location</label>
    <select name="location_db" id="location-db" data-toggle="optional-field">
    	<option value="local">Local Database</option>
        <option value="remote">Remote Database</option>
    </select>
    <div data-type="template-optional-field" data-template-target="#location-db" data-template-match="local">
        <fieldset>
        	<legend>Local Database</legend>
            <div class="control-group">
                <label>Source Database</label>
                <select name="source_db" id="source-db">
                <?php
                    $query = "SHOW DATABASES";
					$result = $utils->db->query($query);
					$errors = $utils->error($result,__LINE__,get_class($utils));	
					if($errors == false){
						$num = $result->rowCount();	
						if($num > 0){							
							$data = $utils->get_result_array($result,false);
							if($data !== false){
								for($i = 0; $i < sizeof($data); $i++){
									echo '<option value="'.$data[$i]['Database'].'">'.$data[$i]['Database'].'</option>';
								}//end for i
							} else {
								echo '<option>Cannot load the databases</option>';		
							}//end if
						} else {
							echo '<option>Cannot load the databases</option>';		
						}//end if
					} else {
						echo '<option>Cannot load the databases</option>';	
					}//end if
                ?>
                </select>
            </div>
        </fieldset>
    </div>
    <div data-type="template-optional-field" data-template-target="#location-db" data-template-match="remote">
        <fieldset id="remote">
        	<legend>Remote Database</legend>            
            <label>Host</label>
            <input type="text" name="remote_host" id="remote-host" class="input-large" />                
            <label>User</label>
            <input type="text" name="remote_user" id="remote-user" class="input-large" />       
            <label>Pass</label>
            <input type="password" name="remote_password" id="remote-password" class="input-large" />      
            <label>Database Name</label>
            <input type="text" name="remote_db" id="remote-db" class="input-large" />    
            <div id="db-status" style="width:300px;"></div>
        </fieldset>
    </div>
    <button type="button" class="btn btn-primary" id="import-btn">Import database data</button>
</form>
<script>
	$(document).ready(function(e) {
        $('form').findOptionalFields();
		$("#remote input").on("blur",function(){
			var allFilled = true;
			$("#remote input").each(function(){
				if($(this).val() == ""){
					allFilled = false;
				}//end if
			});	
			
			if(allFilled){
				$.ajax({
					type: 'post',
					url: $.config.apipath + 'test.remote.db',
					dataType:"json",
					data: 'remote_host='+$("#remote-host").val()+'&remote_user='+$("#remote-user").val()+'&remote_password='+$("#remote-password").val()+'&remote_db='+$("#remote-db").val(),
					success: function(data){
						if(data.result == 'success'){
							$("#db-status").html('<div class="alert alert-success">Connection successfull</div>');
						} else {
							$("#db-status").html('<div class="alert alert-error">Can\'t connect to the database</div>');
						}
					}
				});
			}
		});
		
		$("#migration-form").submit(function(e){
			e.preventDefault();
			var $form = $(this);
			$.ajax({
				url: $.config.apipath + 'import.db',
				data: $form.serialize(),
				dataType:"json",
				type:'post',
				success: function(data){
					console.log(data);	
				}				
			});	
		});
		
		$("#import-btn").click(function(){
			$("#migration-form").submit();	
		});
    });
</script>