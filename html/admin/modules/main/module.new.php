<?php
	$id = $_REQUEST['id'];
	if(!is_null($id) && is_numeric($id)){
		$data = $utils->get_module($id);
		$group_id = $data['module_gid'];
	} else {
		$group_id = $_REQUEST['parent_id'];
	}//end if
	$edit_mode = false;
	$page_title = "New Module";
	if(!is_null($id) && is_numeric($id)){
		$edit_mode = true;	
		$page_title = "Edit Module";
	}//end if
	
	$groups = $utils->get_modules_groups();
?>
<div id="form-wrapper">
    <h4><?php echo $page_title ?></h4>    
    <form action="<?php echo __CMS_PATH__ ?>api/save.module" method="post" name="save-module" id="save-module">    	
    	 <?php
            if($edit_mode){
            ?>
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
            <?php
            }//end if
		?>
    	<fieldset>
        	<div class="control-group">
            	<label for="label">Module Label</label>
        		<input type="text" name="label" id="label" required<?php
				if($edit_mode){
					echo ' value="'.$data['module_label'].'"';	
				}
				?> />                
            </div>
            <div class="control-group">
            	<label for="name">Module Name</label>
        		<input type="text" name="name" id="name" required<?php
				if($edit_mode){
					echo ' readonly';	
					echo ' value="'.$data['module_name'].'"';	
				}//end if
				?> />
            </div>
            <div class="control-group">
            	<label for="table">Module Table <small class="muted">Leave the default value if you are not going to use a different table</small></label>
        		<input type="text" name="table" id="table" <?php
				if($edit_mode){
					echo ' value="'.$data['module_table'].'"';	
				} else {
					echo ' value="posts"';	
				}//end if
				?> />                
            </div>
            <?php
				if($groups !== false){
			?>
            <div class="control-group">
            	<label>Module Group</label>
            	<select name="group_id" id="group_id">                
			<?php
					for($i = 0; $i < sizeof($groups); $i++){
			?>
            		<option value="<?php echo $groups[$i]['module_group_id'] ?>"<?php
					if($groups[$i]['module_group_id'] == $group_id){
						echo ' selected="selected"';	
					}//end if
					?>
                    ><?php echo $groups[$i]['module_group_name'] ?></option>
            <?php			
					}//end for i
			?>	
                </select>
            </div>
            <?php	
				} else {
				?>
                <input type="hidden" name="group_id" id="group_id" value="<?php echo $group_id ?>" />
                <?php	
				}
			?>
            <button type="submit" id="submit-form" class="btn btn-primary">Save</button>
            <a hrer="#" class="btn" data-action="cancel">Cancel</a>   
    	</fieldset>
    </form>
</div>