<?php
	$tools = $utils->call('tools');
	$settings = $utils->get_settings();	
?>
<div class="page-view clearfix">
	<form action="<?php echo __CMS_PATH__ ?>api/save.settings" method="post" id="general-settings" name="settings">
    	<div class="row-fluid">
        	<div class="span9">
                <h2>General</h2>
                <div class="form-block">
                    <fieldset>
                        <legend>Basic</legend>
                        <label for="site-name">Site Name</label>
                        <input type="text" name="siteName" id="site-name" class="input-large" />
                        <label for="site-address">Site Address</label>
                        <input type="text" name="siteAddress" id="site-address" class="input-xxlarge" />
                    </fieldset> 
                    <fieldset>
                        <legend>Dashboard</legend>
                        <label class="checkbox">
                        	<input type="checkbox" value="1" name="showDashboardWidgets" id="show-dashboard-widgets" />
                        	Show widgets in the Dashboard
                       	</label>
                    </fieldset> 
                    <fieldset>
                        <legend>Emails</legend>
                        <label for="send-from">By default send emails as this address</label>
                        <input type="text" name="sendFrom" id="send-from" class="input-xxlarge" />
                        <label for="no-reply">No-Reply address</label>
                        <input type="text" name="noReply" id="no-reply" class="input-xxlarge" />
                        <label for="default-format">Email type</label>
                        <select name="defaultFormat" id="default-format">
                        	<option value="text/plain">Plain Text</option>
                            <option value="text/html">Html</option>
                        </select>                        
                        <label for="mail-delivery">Deliver emails with</label>
                        <select name="mailDelivery" id="mail-delivery" class="input-large" data-toggle="optional-field">
                            <option value="local"<?php if($settings['mailDelivery'] == 'local'){ echo ' selected'; } ?>>Local Server</option>
                            <option value="mandrill"<?php if($settings['mailDelivery'] == 'mandrill'){ echo ' selected'; } ?>>Mandrill</option>
                        </select>
                        <div data-type="template-optional-field" data-template-target="#mail-delivery" data-template-match="mandrill">
                            <h4>Mandrill Login Details</h4>
                            <div class="control-group">
                                <label>Mandrill Username</label>
                                <input type="text" name="mandrillUsername" id="mandrill-username" class="input-xxlarge" />
                                <label>Mandrill API Key</label>
                                <input type="password" name="mandrillApiKey" id="mandrill-apikey" class="input-xxlarge" />
                            </div>
                            <div class="mandrill-test"></div>
                        </div>
                        <label for="test-environment-enabled">Enable Test Environment</label>
                        <select name="testEnvironmentEnabled" id="test-environment-enabled">
                        	<option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                        </select>
                        <!--div data-type="template-optional-field" data-template-target="#test-environment-enabled" data-template-match="1"-->
                            <h4>Test Environment Details</h4>
                            <div class="control-group">
                                <label for="test-environment-notifications-email">Route all the email to this address</label>
                                <input type="text" name="testEnvironmentNotificationsEmail" id="test-environment-notifications-email" class="input-xxlarge" />
                            </div>
                        <!--/div-->
                    </fieldset>                              
                </div> 
                <h2>Templates</h2>
                <div class="form-block">
                    <fieldset>
                        <legend>Templates Settings</legend>
                        <label class="checkbox">
                            <input type="checkbox" name="enableTemplates" id="enableTemplates" value="1" />
                            Enable Templates                
                        </label>              
                        <div class="control-group">  
                            <label for="templates-path">Files Path</label>
                            <input type="text" name="templatesPath" id="templates-path" class="input-xxlarge" value="<?php echo $settings['templatesPath'] ?>" />
                        </div>
                        <label for="protected-permalinks">Protected Permalinks<br /><small class="muted">Enter here the list of permalinks you want to set as protected by the system, one permalink per line.</small></label>
                        <textarea name="protectedPermalinks" id="protected-permalinks" rows="4" cols="60" class="input-xxlarge"></textarea>
                        
                    </fieldset>   
                    <fieldset>
                    	<legend>Email Templates</legend>
                        <label class="checkbox">
                            <input type="checkbox" name="enableEmailTemplates" id="enableEmailTemplates" value="1" />
                            Enable HTML Email Templates                
                        </label>                
                        <label for="email-templates-path">Email Templates Files Path</label>
                        <input type="text" name="emailTemplatesPath" id="email-templates-path" class="input-xxlarge" />
                    </fieldset>
                </div> 
                <h2 data-toggle="collapse" data-target="#social">Social Media <small class="muted">(click to expand)</small></h2>
                <div class="collapse" id="social">
                	<div class="form-block">
                        <fieldset>
                            <legend>Facebook</legend>                      
                            <label for="facebook-app-id">App ID</label>
                            <input type="text" name="facebookAppId" id="facebook-app-id" class="input-xxlarge" />
                            <label for="facebook-app-secret">App Secret</label>
                            <input type="text" name="facebookAppSecret" id="facebook-app-secret" class="input-xxlarge" />
                            <label for="facebook-app-namespace">App Namespace</label>
                            <input type="text" name="facebookAppNamespace" id="facebook-app-namespace" class="input-xxlarge" />   
                            <label for="facebook-sdk-code">SDK Code</label>
                            <textarea name="facebookSDKCode" id="facebook-sdk-code" class="input-xxlarge" rows="5"></textarea>                
                        </fieldset>
                        <fieldset>
                            <legend>Twitter</legend>                      
                            <label for="twitter-consumer-key">Consumer Key</label>
                            <input type="text" name="twitterConsumerKey" id="twitter-consumer-key" class="input-xxlarge" />
                            <label for="twitter-consumer-secret">Consumer Secret</label>
                            <input type="text" name="twitterConsumerSecret" id="twitter-consumer-secret" class="input-xxlarge" />
                            <label for="twitter-user-token">User Token</label>
                            <input type="text" name="twitterUserToken" id="twitter-user-token" class="input-xxlarge" />
                            <label for="twitter-user-secret">User Secret</label>
                            <input type="text" name="twitterUserSecret" id="twitter-user-secret" class="input-xxlarge" />                  
                        </fieldset>
                        <fieldset>
                        	<legend>Instagram</legend>
                            <label for="instagram-client-id">Client ID</label>
                            <input type="text" name="instagramClientID" id="instagram-client-id" class="input-xxlarge">
                            <label for="instagram-client-secret">Client Secret</label>
                            <input type="text" name="instagramClientSecret" id="instagram-client-secret" class="input-xxlarge">
                            <label for="instagram-user-id">User ID</label>
                            <input type="text" name="instagramUserID" id="instagram-user-id" class="input-xxlarge">
                        </fieldset>
                	</div>
                </div>  
                <h2 data-toggle="collapse" data-target="#analytics">Analytics <small class="muted">(click to expand)</small></h2>  
                <div class="collapse" id="analytics">
                    <div class="form-block">
                        <fieldset>
                            <legend>Google Analytics</legend>
                            <label>Please paste the Google Analytics code in the textarea below <small class="muted">Include the &lt;script&gt; tags as well</small></label>
                            <textarea name="googleAnalyticsCode" id="google-analytics-code" rows="10" cols="60" class="input-block-level">
                            <?php
                            if(isset($settings['googleAnalyticsCode'])){
                                echo $settings['googleAnalyticsCode'];
                            }
                            ?>
                            </textarea>
                        </fieldset>
                	</div>
                </div>                 
                <h2 data-toggle="collapse" data-target="#maintenance">Maintenance <small class="muted">(click to expand)</small></h2>
                <div class="collapse" id="maintenance">
                    <div class="form-block">
                        <fieldset>
                            <legend>Revisions</legend>     
                            <?php
                            $size = $tools->get_table_size('posts_revisions');
                            $warning = false;
                            if($size > 1){
                                $warning = true;
                                $size_label = $size.'MB <i class="icon-warning-sign"></i>';
                            } else {
                                $size_label = $size.'MB';	
                            }
                            ?>
                            <div class="alert<?php
                            if(!$warning){
                                echo ' alert-info';
                            }
                            ?> ">
                                Current size of the posts revisions table: <strong><?php
                                echo $size_label;
                                ?></strong>
                            </div>           
                            <label class="checkbox">
                                <input type="checkbox" value="1" name="deleteOldRevisions" id="deleteOldRevisions" />
                                Delete old revisions
                            </label>
                            <label>Auto-remove revisions after</label>
                            <select name="deleteOldRevisionsDays" id="deleteOldRevisionsDays">
                                <option value="7" selected>One week</option>
                                <option value="14">Two Weeks</option>
                                <option value="30">One Month</option>
                                <option value="90">Three Months</option>
                                <option value="365">One Year</option>
                            </select>               
                        </fieldset>
                        <fieldset>
                            <legend>Logs</legend>   
                            <div class="clearfix">             
                                <h4>System Logs</h4>
                                <label>Remove system logs after</label>
                                <select name="deleteOldLogsDays" id="deleteOldLogsDays">
                                    <option value="1" selected>One Day</option>
                                    <option value="7" selected>One week</option>
                                    <option value="14">Two Weeks</option>
                                    <option value="30">One Month</option>
                                    <option value="90">Three Months</option>
                                    <option value="365">One Year</option>
                                </select>
                                <p class="muted">This logs contains all the calls made to the system API and public APIs.</p>
                                <?php
                                $size = $tools->get_table_size('system_logs');
                                $warning = false;
                                if($size > 1){
                                    $warning = true;
                                    $size_label = $size.'MB <i class="icon-warning-sign"></i>';
                                } else {
                                    $size_label = $size.'MB';	
                                }
                                ?>
                                <div class="alert<?php
                                if(!$warning){
                                    echo ' alert-info';
                                }
                                ?> ">
                                    Current size of the system logs table: <strong><?php
                                    echo $size_label;
                                    ?></strong>
                                </div>
                                <div id="system-logs">
                                    <a href="<?php echo __CMS_PATH__ ?>api/empty.system.logs" class="btn btn-danger" id="empty-sy-logs-btn"><i class="icon-trash"></i> Empty System Logs</a>
                                </div>
                            </div>
                            <hr />
                            <div class="clearfix">
                                <h4>Mail Logs</h4>
                                <p class="muted">This records contains all the emails sent using the system mail delivery.</p>
                                <?php
                                $size = $tools->get_table_size('mail_log');
                                $warning = false;
                                if($size > 1){
                                    $warning = true;
                                    $size_label = $size.'MB <i class="icon-warning-sign"></i>';
                                } else {
                                    $size_label = $size.'MB';	
                                }
                                ?>
                                <div class="alert<?php
                                if(!$warning){
                                    echo ' alert-info';
                                }
                                ?> ">
                                    Current size of the mail logs table: <strong><?php
                                    echo $size_label;
                                    ?></strong>
                                </div>
                                <div id="mail-logs">
                                    <a href="<?php echo __CMS_PATH__ ?>api/empty.mail.logs" class="btn btn-danger" id="empty-mail-logs-btn"><i class="icon-trash"></i> Empty Mail Logs</a>
                                </div>
                            </div>
                            <hr />
                            <div class="clearfix">
                                <h4>Blacklist Logs</h4>
                                <p class="muted">This records contains all the blacklisted ips recorded by the system.</p>                    
                                <?php
                                $size = $tools->get_table_size('blacklist');
                                $warning = false;
                                if($size > 1){
                                    $warning = true;
                                    $size_label = $size.'MB <i class="icon-warning-sign"></i>';
                                } else {
                                    $size_label = $size.'MB';	
                                }
                                ?>
                                <div class="alert<?php
                                if(!$warning){
                                    echo ' alert-info';
                                }
                                ?> ">
                                    Current size of the blacklist logs table: <strong><?php
                                    echo $size_label;
                                    ?></strong>
                                </div>
                                <div id="blacklist-logs">
                                    <a href="<?php echo __CMS_PATH__ ?>api/empty.blacklist.logs" class="btn btn-danger" id="empty-bl-logs-btn"><i class="icon-trash"></i> Empty Blacklist Log</a>
                                </div>
                                <br />
                                <div class="control-group">
                                    <label>Whitelist</label>
                                    <input type="text" name="whitelistIPs" id="whitelistIPs" class="input-xxlarge" <?php
                                    if(isset($settings['whitelistIPs'])){
                                        echo ' value="'.$settings['whitelistIPs'].'"';	
                                    }//end if
                                    ?> />
                                    <br />
                                    <small class="muted">The above IP will be ignored by the logger system</small>
                                </div>
                            </div>
                        </fieldset>
                	</div><!-- /.form-block -->
                </div><!-- /#maintenance -->
                <h2 data-toggle="collapse" data-target="#advanced">Advanced <small class="muted">(click to expand)</small></h2>
                <div class="collapse" id="advanced">
                    <div class="form-block">
                        <fieldset>
                            <legend>Advanced</legend>   
                            <label>Charset</label>
                            <select name="siteCharset" id="site-charset">
                            	<option value="uft-8">UTF-8</option>
                                <option value="iso-8859-1">ISO-8859-1</option>
                            </select>
                            <label>Force Protocol</label>
                            <select name="forceProtocol" id="force-protocol">
                            	<option value="">Don't force</option>
                                <option value="http">Force to HTTP</option>
                                <option value="https">Force to secure HTTPS</option>
                            </select>
                        </fieldset>
                	</div>
                </div><!-- /#advanced -->   
        	</div>
            <div class="span3">
            	<div data-spy="affix" data-offset-top="50">
	            	<button type="submit" class="btn btn-primary">Save Settings</button>                   
                </div>
            </div>
    	</div>
    </form>
</div>