<h1><?php echo $utils->view->settings['siteName'] ?> Dashboard</h1>
<hr />
<?php

	if(isset($utils->view->settings['showDashboardWidgets'])){
		if((bool)$utils->view->settings['showDashboardWidgets']){
			$utils->view->widgets();
		}//end if
	}//end if
?>
<script>
	$().ready(function(){	
		$('body').addClass('dashboard');
		$('.dashboard-widget').each(function(){
			if($(this).html().trim() == ""){
				$(this).hide();	
			}
		});
	});
</script>