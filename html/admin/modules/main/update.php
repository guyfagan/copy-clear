<h4>Database updater</h4>
<div id="response"></div>
<button type="button" class="btn btn-important" id="update-db-btn">Update</button>
<script>
	$(document).ready(function() {
        $("#update-db-btn").click(function(e){
			e.preventDefault();
			$(this).addClass('disabled');	
			$.ajax({
				url: $.config.apipath + 'update.db',
				dataType:"json",
				success: function(data){
					if(data.result == 'success'){
						$("#response").html('<div class="alert alert-success"><strong>Success!</strong> The database is now up to date</div>');						
					} else {
						$html = '<div class="alert alert-error"><strong>Error!</strong> Can\'t update the database as the system throw these errors:';
						$html += '<ul>';
						for($i = 0; $i < data.errors.length; $i++){
							$html += '<li>'+data.errors[$i]+'</li>';	
						}
						$html += '<ul></div>';
						$("#response").html($html);
					}
				}
			});
		});
    });
</script>