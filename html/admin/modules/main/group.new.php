<?php
	$id = $_REQUEST['id'];
	if(!is_null($id) && is_numeric($id)){
		$data = $utils->get_modules_group(array('id' => $id));
	}//end if
	$edit_mode = false;
	$page_title = "New Modules Group";
	if(!is_null($id)){
		$edit_mode = true;	
		$page_title = "Edit Modules Group";
	}//end if
?>
<div id="form-wrapper">
    <h4><?php echo $page_title ?></h4>    
    <form action="<?php echo __CMS_PATH__ ?>api/save.module.group" method="post" name="save-module-group" id="save-module-group">    	
    	 <?php
            if($edit_mode){
            ?>
            <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
            <?php
            }//end if
		?>
    	<fieldset>
        	<div class="control-group">
            	<label for="label">Group Name</label>
        		<input type="text" name="name" id="name"<?php
				if($edit_mode){
					echo ' value="'.$data['module_group_name'].'"';	
				}
				?> required />                
            </div>
         	<label class="checkbox">
            	<input type="checkbox" value="1" name="hidden" id="hidden"<?php
				if($edit_mode){
					if($data['module_group_hidden'] == 1){
						echo ' checked';	
					}//end if
				}//end if
				?> /> 
                Tick here to set as an hidden group
            </label>
            <button type="submit" id="submit-form" class="btn btn-primary">Save</button>
            <a hrer="#" class="btn" data-action="cancel">Cancel</a>   
    	</fieldset>
    </form>
</div>