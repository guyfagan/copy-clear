<h1>Search Results</h1>
<?php
	$elements_per_page = 25;
	$paginated_results = true;
	$show_thumbs = true;
	$generic = $utils->call('ewrite');
	$utils->create_paging($_GET['page'],$elements_per_page);
	$results = $generic->search(array('all_modules' => true, 'default_photo' => true, 'search' => $_REQUEST['search'],'fields' => array('post_title', 'post_module_id', 'module_label', 'post_id','module_id')));
	
	# PAGINATION #
	if($paginated_results){
		$paging_stats = $utils->get_paging_stats();	
		$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
		//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
		$utils->unset_paging();
		$page_start = $paging_stats['start']+1;
		$page_end = $paging_stats['start']+10;
		if($page_end > $paging_stats['founds']){
			$page_end = $paging_stats['founds'];
		}//end if
		$page_total = $paging_stats['pages'];
		$page_current = $paging_stats['current_page'];
		$page_prev = $page_current-1;
		if($page_prev == "0"){
			$page_prev = 1;
		}//end if
		$page_next = $page_current+1;
		if($page_next > $page_total){
			$page_next = $page_total;
		}//end if
	}//end if
	# END PAGINATION #

	if(isset($_REQUEST['search'])){
		echo '<div class="alert alert-info" id="search-alert"><i class="icon-search"></i> Search results for &quot;<strong>'.$_REQUEST['search'].'</strong>&quot;<button type="button" class="close" data-dismiss="alert">&times;</button></div>';
	}//end if
?>
<table class="table table-striped table-hover">
	<thead>
    	<th>Title</th>
        <th>Module</th>        
    </thead>
    <?php
	if($paginated_results){
		if($page_total > 1){
		?>
		<tfoot>
			<tr>
				<td colspan="3">
					<div class="pagination">
						<ul>
							<li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
							<?php
							if(is_array($paging_list) && sizeof($paging_list) > 0){
								for($i = 0; $i < sizeof($paging_list); $i++){
									echo '<li';
									if($paging_list[$i]['page'] == $page_current){
										echo ' class="active"';	
									}//end if
									echo '><a href="'.$paging_list[$i]['url'].'">'.$paging_list[$i]['page'].'</a></li>';
								}//end for i
							}//end if
							?>                      
							<li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
						</ul>
					</div>
				</td>
			</tr>
		</tfoot>
		<?php
		}//end if
	}//end if
	?>
	<tbody>
<?php	
	if($results !== false){
		for($i = 0; $i < sizeof($results); $i++){
			$id = $results[$i]['post_id'];
			$title = $results[$i]['post_title'];
			$module = $results[$i]['post_module_id'];
			$link = __CMS_PATH__.$module.'/edit/'.$id;
			$path = $generic->get_post_path(array('id' => $id,'module_id' => $results[$i]['module_id']));				
			if(sizeof($path) > 0){
				$link .= "/in/".$path[0]['id'];	
			}
		?>
        <tr>
        	<td>
            	<?php
				if(isset($results[$i]['photo']) && $show_thumbs){
					echo '<img src="'.__SERVERPATH__.'upload/photos/t/'.$results[$i]['photo']['attachment_filename'].'" alt="'.$results[$i]['label'].'" class="list-thumb-icon list-thumb-icon-mini" />';
				}//end if
				?>
            	<a href="<?php echo $link ?>"><?php echo $title ?></a>
                <br /><span class="label">
                <?php
				echo $results[$i]['module_label'].' / ';
				if($path !== false && sizeof($path) > 0){
					
					for($x = (sizeof($path)-1); $x >= 0; $x--){
						echo $path[$x]['label'];						
						echo ' / ';							
					}//end for x					
				}//end if
				?>
                </span>
            </td>
            <td><a href="<?php echo __CMS_PATH__.$module.'/'; ?>"><?php echo $results[$i]['module_label'] ?></a></td>
        </tr>
        <?php	
		}//end for i
	} else {
		echo '<tr><td colspan="5">Nothing found with this search criteria</td></tr>';
	}//end if
?>
	</tbody>
</table>