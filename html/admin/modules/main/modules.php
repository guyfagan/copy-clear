<?php
	$groups = $utils->get_modules_groups();
?>
<h1>Manage Modules</h1>
<table class="table table-striped table-hover">
	<thead>
        <tr>
            <th>Modules Group</th>         
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tfoot>
    	<tr>
        	<td colspan="2">
            	<a href="<?php echo __CMS_PATH__ ?>main/group/new/" class="btn btn-primary">New Group</a>
            </td>
        </tr>
    </tfoot>
    <tbody>
    <?php
		if($groups !== false){
			for($i = 0; $i < sizeof($groups); $i++){
		?>
        <tr>
        	<td><a href="<?php echo __CMS_PATH__ ?>main/group/<?php echo $groups[$i]['module_group_id'] ?>/"><?php echo $groups[$i]['module_group_name'] ?></a></td>
            <td class="table-row-options">
                <div class="btn-group pull-right table-button-cta">
                	<a href="<?php echo __CMS_PATH__ ?>main/group/edit/<?php echo $groups[$i]['module_group_id'] ?>/" class="btn"><i class="icon-edit"></i></a>
                </div>
            </td>
        </tr>
        <?php
			}//end for i
		} else {
			echo '<tr>
					<td colspan="2">
						<i class="icon-warning-sign"></i> No groups found
					</td>
				</tr>';		
		}//end if
	?>
    </tbody>
</table>