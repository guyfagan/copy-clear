<?php
	$id = $_GET['id'];
	$modules_list = $utils->get_modules(array("group_id" => $id));
	$group = $utils->get_modules_group(array('id' => $id));
?>
<h1><?php echo ucwords($group['module_group_name']) ?></h1>
<form action="#" id="table-form" name="table-form" method="post">
	<input type="hidden" name="group_id" id="group_id" value="<?php echo $id ?>">
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Modules</th>         
                <td>&nbsp;</td>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="2">
                    <a href="<?php echo __CMS_PATH__ ?>main/module/new/in/<?php echo $id ?>" class="btn btn-primary">New Module</a>
                </td>
            </tr>
        </tfoot>
        <tbody>
        <?php
            if($modules_list !== false){
                for($i = 0; $i < sizeof($modules_list); $i++){
                    $id = $modules_list[$i]['module_id'];
            ?>
            <tr>
                <td><a href="<?php echo __CMS_PATH__.'/'.$modules_list[$i]['module_name'] ?>/"><?php echo $modules_list[$i]['module_label'] ?></a></td>
                <td class="table-row-options">
                    <input type="hidden" name="table_row_id[]" id="table-row-id-<?php echo $id ?>" value="<?php echo $id ?>" />
                    <div class="btn-group pull-right table-button-cta">
                    	<a href="#" class="btn table-row-handle"><i class="icon-move"></i></a>
                        <a href="<?php echo __CMS_PATH__ ?>main/module/edit/<?php echo $modules_list[$i]['module_id'] ?>/" class="btn"><i class="icon-edit"></i></a>
                    </div>
                </td>
            </tr>
            <?php
                }//end for i
            } else {
                echo '<tr>
                        <td colspan="2">
                            <i class="icon-warning-sign"></i> No groups found
                        </td>
                    </tr>';		
            }//end if
        ?>
        </tbody>
    </table>
</form>