<?php
	$templates = $utils->get_templates();
?>
<h1>Templates</h1>
<table class="table table-striped table-hover">
	<thead>
        <tr>
            <th>Template Name</th>         
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tfoot>
    	<tr>
        	<td colspan="2">
            	
            </td>
        </tr>
    </tfoot>
    <tbody>
    <?php
		if($templates !== false){
			for($i = 0; $i < sizeof($templates); $i++){
		?>
        <tr>
        	<td><?php echo $templates[$i]['label'].' <em class="muted">('.$templates[$i]['folder'].$templates[$i]['file'].')</em>'; ?></td>
            <td class="table-row-options"></td>
        </tr>
        <?php
			}//end for i
		} else {
			echo '<tr>
					<td colspan="2">
						<i class="icon-warning-sign"></i> No templates found
					</td>
				</tr>';		
		}//end if
	?>
    </tbody>
</table>
<hr />
<?php
	$templates = $utils->get_templates(array('type' => 'email'));
?>
<h1>Email Templates</h1>
<table class="table table-striped table-hover">
	<thead>
        <tr>
            <th>Template Name</th>         
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tfoot>
    	<tr>
        	<td colspan="2">
            	
            </td>
        </tr>
    </tfoot>
    <tbody>
    <?php
		if($templates !== false){
			for($i = 0; $i < sizeof($templates); $i++){
		?>
        <tr>
        	<td><?php echo $templates[$i]['label'].' <em class="muted">('.$templates[$i]['file'].')</em>'; ?></td>
            <td class="table-row-options"></td>
        </tr>
        <?php
			}//end for i
		} else {
			echo '<tr>
					<td colspan="2">
						<i class="icon-warning-sign"></i> No templates found
					</td>
				</tr>';		
		}//end if
	?>
    </tbody>
</table>