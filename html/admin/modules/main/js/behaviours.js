// JavaScript Document

$().ready(function(){
	interceptor();
});

function interceptor(){
	//there is no space for Mel Gibson here	
	if($("body").prop("class") != ''){
		var $bodyAttrs = $("body").prop("class").split(" ");	
		for(i = 0; i < $bodyAttrs.length; i++){		
			if(parseInt($bodyAttrs[i].indexOf("action-")) != -1){
				$action = $bodyAttrs[i];			
				$action = $action.replace("action-","");
			}
		}
		
		switch($action){
			case "settings":
				settings_actions();			
				break;
			case "module-new":
				new_module_actions();
				break;
			case "group-new":
				new_modules_group_actions();
				break;
			case "group":
				group_actions();
				break;
			case "logs":
				logs_actions();
				break;
		}//end switch
	}//end if
}

function settings_actions(){

	$('#whitelistIPs').tokenfield();
	$('#templates-path').tokenfield();

	$("#general-settings").load_settings();
	
	$('form').findOptionalFields();
	
	//listen the mandrill fields
	$('#mandrill-apikey').on('blur',function(){
		$('.mandrill-test').html('<div class="alert">Checking Mandrill API Key</div>').find('div').hide().fadeIn(400);
		var $field = $(this);
		$.ajax({
			url: $.config.apipath+'test.mandrill',
			type: 'post',
			data: 'key='+$field.val(),
			dataType:"json",
			success: function(data){
				if(data.result == 'success'){
					$('.mandrill-test .alert').fadeOut('slow',function(){
						$(this).remove();	
						$('.mandrill-test').html('<div class="alert alert-success"><i class="icon-ok"></i> Mandrill API Key is working fine</div>').find('div').hide().fadeIn('slow');
					});
				} else {
					$('.mandrill-test .alert').fadeOut('slow',function(){
						$(this).remove();	
						$('.mandrill-test').html('<div class="alert alert-error"><i class="icon-remove"></i> Please ensure that you have the correct Mandrill API Key.</div>').find('div').hide().fadeIn('slow');
					});
				}
			}
		});
	});
	
	$("#general-settings").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				if(data.result == "success"){
					$("#general-settings").popMessage("The settings has been saved successfully",{type: "success",class: 'gen-settings-alert'});
					$.cmsVars.formSaved = true;
				
				} else {
					$("#general-settings").popMessage("The settings has not been saved due to a server error",{type: "error",class: 'gen-settings-alert'});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#general-settings").validate();
		if(isValidated){
			$("#general-settings").submit();	
		} else {
			$("#general-settings").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
	
	$("#empty-sy-logs-btn").click(function(e){
		e.preventDefault();
		var $path = $(this).prop('href');
		$.ajax({
			url : $path,
			type: 'post',
			dataType:"json",
			success: function(data){
				if(data.result == 'success'){
					$("#system-logs").popMessage("The system logs have been emptied",{type: "success",when: 'before', scrollTopOnComplete: false,local: true,persistent:true});
				} else {
					$("#system-logs").popMessage("Something went wrong: "+data.error,{type: "error",when: 'before', scrollTopOnComplete: false,local: true,persistent:true});
				}
			}
		});	
	});
		
	$("#empty-mail-logs-btn").click(function(e){
		e.preventDefault();
		var $path = $(this).prop('href');
		$.ajax({
			url : $path,
			type: 'post',
			dataType:"json",
			success: function(data){
				if(data.result == 'success'){
					$("#mail-logs").popMessage("The mail logs have been emptied",{type: "success",when: 'before', scrollTopOnComplete: false,local: true,persistent:true});
				} else {
					$("#mail-logs").popMessage("Something went wrong: "+data.error,{type: "error",when: 'before', scrollTopOnComplete: false,local: true,persistent:true});
				}
			}
		});	
	});
	
	$("#empty-bl-logs-btn").click(function(e){
		e.preventDefault();
		var $path = $(this).prop('href');
		$.ajax({
			url : $path,
			type: 'post',
			dataType:"json",
			success: function(data){
				if(data.result == 'success'){
					$("#blacklist-logs").popMessage("The blacklist logs have been emptied",{type: "success",when: 'before', scrollTopOnComplete: false,local: true,persistent:true});
				} else {
					$("#blacklist-logs").popMessage("Something went wrong: "+data.error,{type: "error",when: 'before', scrollTopOnComplete: false,local: true,persistent:true});
				}
			}
		});	
	});
}

function new_module_actions(){
	if($("#id").length > 0){
		$("#save-module").load_module({module_id : $("#id").val()});		
	}//end if
	
	$('a[data-action=cancel]').click(function(e){
		e.preventDefault();
		window.history.back()	
	});	

	if($("#id").length == 0){
		$(".permalink").handlePermalink({path: "", input: "#label", defaultExt: "",apiCall: "clean.link", saveTo: "#name"});
	}
	
	$("#save-module").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				if(data.result == "success"){
					//$("#save-module").popMessage("The module has been created successfully",{type: "success"});
					//$.cmsVars.formSaved = true;				
					var $group_id = $('#group_id').val();
					location.href = $.config.basepath+'admin/main/group/'+$group_id+'/';
				} else {
					$("#save-module").popMessage("The module has not been saved due to a server error",{type: "error"});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#save-module").validate();
		if(isValidated){
			$("#save-module").submit();	
		} else {
			$("#save-module").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
}//end function

function new_modules_group_actions(){
	
	$('a[data-action=cancel]').click(function(e){
		e.preventDefault();
		window.history.back()	
	});	

	
	$("#save-module-group").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				//console.log(data);
				if(data.result == "success"){					
					location.href = $.config.basepath+'admin/main/group/'+data.data.module_group_id+'/';
				} else {
					$("#save-module-group").popMessage("The module has not been saved due to a server error",{type: "error"});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#save-module").validate();
		if(isValidated){
			$("#save-module-group").submit();	
		} else {
			$("#save-module-group").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
}

function group_actions(){
	/* THIS CODE MAKES THE TABLE SORTABLE */
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	$("table tbody").sortable({ 
		handle: ".table-row-handle", 
		axis: "y",
		helper: fixHelper,
		stop: function( event, ui ) {
			$('#table-form').saveModuleSorting();
		}
	}).disableSelection();	
}

function logs_actions(){
	$("#empty-sy-logs-btn").click(function(e){
		e.preventDefault();
		var $path = $(this).prop('href');
		$.ajax({
			url : $path,
			type: 'post',
			dataType:"json",
			success: function(data){
				if(data.result == 'success'){
					$("#system-logs-table").popMessage("The system logs have been emptied",{type: "success",when: 'after', scrollTopOnComplete: false,persistent:true});
				} else {
					$("#system-logs-table").popMessage("Something went wrong: "+data.error,{type: "error",when: 'after', scrollTopOnComplete: false,persistent:true});
				}
			}
		});	
	});
	
	$("#empty-bl-logs-btn").click(function(e){
		e.preventDefault();
		var $path = $(this).prop('href');
		$.ajax({
			url : $path,
			type: 'post',
			dataType:"json",
			success: function(data){
				if(data.result == 'success'){
					$("#blacklist-logs-table").popMessage("The blacklist logs have been emptied",{type: "success",when: 'after', scrollTopOnComplete: false,persistent:true});
				} else {
					$("#blacklist-logs-table").popMessage("Something went wrong: "+data.error,{type: "error",when: 'after', scrollTopOnComplete: false,persistent:true});
				}
			}
		});	
	});
}

$.fn.loadTab = function(params){
	params = $.extend({page: 1},params);
	var $target = $(this).attr('href');
	var $tab = $(this);	
	$('table tbody tr:eq(0)',$target).hide();
	$('table tbody',$target).jLoader({
		backdropOpacity: .7,
		backdropColor: '#FFF',
		content: '<span class="loading"><i class="icon-refresh icon-spin"></i> Loading...</span>'	
	});
	$.ajax({
		url: $.config.apipath + $('table',$target).data('api-call')+'?page='+params.page,
		dataType:"json",
		success: function(data){						
			$('table tbody',$target).jLoader('closeLoader');
			$('table',$target).populate_table(data.data);	
			update_pagination(data);
			update_views($('table tbody',$target));
		}					
	});
	
	function update_views($target){
		
		$('a[data-role=load-mail]',$target).click(function(e){
			e.preventDefault();
			var $this = $(this);
			var $id = $this.data('msg-id');
			$('#log-detail-modal').unbind('show');
			$('#log-detail-modal').on('show',function(){				
				//$('.modal-body',$(this)).html('<iframe src="'+$.config.apipath + 'get.mail.html?id='+$id+'" border="></iframe>');	
				$('.modal-body #iframe-content',$(this)).prop('src',$.config.apipath + 'get.mail.html?id='+$id);	
			}).modal({ backdrop: true });
			/*		
			$.ajax({
				url: $.config.apipath + 'get.mail.details',
				data: 'id=' + $this.data('msg-id'),
				dataType:"json",
				type:'post',
				success: function(data){
					console.log(data.message);
					if(data.result == 'success'){
						$('#log-detail-modal').unbind('show');
						$('#log-detail-modal').on('show',function(){
							//$('.modal-body',$(this)).html('<div class="well">'+data.message+'</div>');	
							$('.modal-body',$(this)).html('<div class="well">'+data.message+'</div>');	
						}).modal({ backdrop: true });
					}
				}
			});
			*/
		});
		/*
		$('#log-detail-modal').unbind('show');
		$('#log-detail-modal').on('show',function(){
			$('.modal-body',$(this)).html('Are you sure that you want to delete the entry <strong>&quot;'+$label+'&quot;</strong>');	
		});
		*/
	}

	
	function listen_pagination(){
		$('.pagination li',$target).off('click','a');
		$('.pagination li',$target).on('click','a',function(e){
			e.preventDefault();	
			//console.log($(this).data('page'));
			$tab.loadTab({page: parseInt($(this).data('page'))});
		});
	}
	
	function update_pagination(data){
		if(typeof($.cmsVars.pagination) == 'undefined'){
			$.cmsVars.pagination = {};	
		}//end if
		
		if(typeof($.cmsVars.pagination[$target]) == 'undefined'){		
			var $template = $('.pagination li:eq(0)',$target).html();
			$.cmsVars.pagination[$target] = $template;
		} else {
			var $template = $.cmsVars.pagination[$target];
		}
		//console.log($template);
		
		$('.pagination li:eq(0)',$target).remove();		
		if(typeof(data.paging) != 'undefined'){
			for(var i = 0; i < data.paging.pages_shown; i++){
				var page = parseInt(data.paging.list[i].page);				
				$html = $template.replace(/{#pageN#}/g,page);			
				if(page == data.paging.current_page){
					$html = '<li class="active">'+$html+'</li>';
				} else {
					$html = '<li>'+$html+'</li>';
				}
				$('.pagination ul',$target).append($html);
			}//end for i
			$('.pagination li',$target).not(':eq(0)').show().addClass('cloned');
			$('.pagination').show();
			listen_pagination();
		} else {
			$('.pagination').hide();
		}
		
	}
}

$.fn.populate_table = function(jsonData,params){
	params = $.extend({tableTemplate: '.table-template'},params);
	var $table = $(this);
	$('.cloned',$table).remove();//remove the previously generated rows, if they exist
	var $template = $(params.tableTemplate,$table).html();//this is a tbody		
	$('tbody tr:eq(0)',$table).hide();
	if(jsonData.length > 0){	
		for(var i = 0; i < jsonData.length; i++){
			$html = $template;
			for(key in jsonData[i]){
				$html = $html.replace("{#"+key+"#}",jsonData[i][key]);	
			}		
			$('tbody',$table).append($html);		
		}//end for i		
	} else {
		var $colspan = $('tbody tr:eq(0) td',$table).length;
		$('tbody',$table).append('<tr><td colspan="'+$colspan+'"><div class="alert">Nothing found</div></td></tr>');	
	}//end if
	$('tbody tr',$table).not(':eq(0)').show().addClass('cloned');
}