<?php
	$settings = $utils->view->module_settings;
	$elements_per_page = 25;
	if(isset($settings['items_per_page']) && is_numeric($settings['items_per_page'])){
		$elements_per_page = $settings['items_per_page'];
	}//end if
	$paginated_results = false;
	if($elements_per_page > 0){
		$paginated_results = true;
		$utils->create_paging($_GET['page'],$elements_per_page);
	}//end if
	$items = ${$module}->get_messages(array('to' => $utils->get_uid()));
	# PAGINATION #
	if($paginated_results){
		$paging_stats = $utils->get_paging_stats();	
		$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
		//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
		$utils->unset_paging();
		$page_start = $paging_stats['start']+1;
		$page_end = $paging_stats['start']+10;
		if($page_end > $paging_stats['founds']){
			$page_end = $paging_stats['founds'];
		}//end if
		$page_total = $paging_stats['pages'];
		$page_current = $paging_stats['current_page'];
		$page_prev = $page_current-1;
		if($page_prev == "0"){
			$page_prev = 1;
		}//end if
		$page_next = $page_current+1;
		if($page_next > $page_total){
			$page_next = $page_total;
		}//end if
	}//end if
	# END PAGINATION #
?>
<table class="table table-hover table-striped">
	<thead>
    	<tr>
        	<th>Date</th>            
            <th>Subject</th>
            <th>From</th>
        </tr>
    </thead>
    <?php
	if($paginated_results){
		if($page_total > 1){
		?>
		<tfoot>
			<tr>
				<td colspan="3">
					<div class="pagination">
						<ul>
							<li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
							<?php
							if(is_array($paging_list) && sizeof($paging_list) > 0){
								for($i = 0; $i < sizeof($paging_list); $i++){
									echo '<li';
									if($paging_list[$i]['page'] == $page_current){
										echo ' class="active"';	
									}//end if
									echo '><a href="'.$paging_list[$i]['url'].'">'.$paging_list[$i]['page'].'</a></li>';
								}//end for i
							}//end if
							?>                      
							<li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
						</ul>
					</div>
				</td>
			</tr>
		</tfoot>
		<?php
		}//end if
	}//end if
	?>
    <tbody>
    <?php
	if($items !== false){
		for($i = 0; $i < sizeof($items); $i++){
	?>
    	<tr>
        	<td><i class="icon-calendar"></i> <?php echo date('d/m/Y H:i:s',$items[$i]['message_date']) ?></td>                        
            <td><a href="<?php echo __MODULE_PATH__ ?>/view/<?php echo $items[$i]['message_id'] ?>"><?php echo $items[$i]['message_subject'] ?></a></td>
            <td><i class="icon-user"></i> <?php echo $items[$i]['sender_firstname'].' '.$items[$i]['sender_surname'] ?></td>
        </tr>
    <?php		
		}//end for i
	}//end if 
	?>
    </tbody>
</table>