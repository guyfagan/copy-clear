<?php
	$messages = $this->utils->call('messages');
	$recent_messages = $messages->get_messages(array('limit' => 5, 'to' => $this->utils->get_uid()));
	if($recent_messages !== false){		
?>
<style>
	.title-no-pad{
		margin-bottom: 0 !important;	
	}
</style>
<h3 class="title-no-pad">Latest Messages</h3>
<table class="table table-stiped table-hover widget-bookings">
	<thead>
    	<tr>
        	<th>Title</th>
            <th>Date</th>
            <th class="to-right">From</th>
        </tr>
    </thead>
    <tbody>
    <?php
		for($i = 0; $i < sizeof($recent_messages); $i++){
?>
	<tr>
    	<td><i class="icon-mail"></i> <a href="<?php echo __CMS_PATH__ ?>messages/view/<?php echo $recent_messages[$i]['message_id'] ?>"><?php echo $recent_messages[$i]['message_subject'] ?></a></td>
        <td width="30%"><?php 
		echo date("d/m/Y H:i:s",$recent_messages[$i]['message_date']);			
		?></td>
        <td class="to-right"><?php echo $recent_messages[$i]['receiver_firstname'].' '.$recent_messages[$i]['receiver_surname']; ?></td>
    </tr> 
<?php			
		}//end for i
?>
	</tbody>
</table>
<?php
	}//end if
?>