<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->get_settings(array('module' => $module));
	$groups_list = $users->get_groups();
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <div class="form-block">
            <fieldset>
                <legend>Notifications</legend>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="notifyConversation" id="notify-conversation"> Notify the users of a conversation
                </label>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'notifyConversationEmailTemplate';					
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>Conversation notification email subject</label>
                <input type="text" name="conversationEmailSubject" id="conversation-email-subject" class="input-xxlarge">
                <label>Conversation notification email message</label>
                <textarea name="conversationEmailMessage" id="conversation-email-message" data-ckeditor="true" data-ckeditor-height="150"><?php
				if(isset($settings['conversationEmailMessage'])){
					echo $settings['conversationEmailMessage'];	
				}//end if
				?></textarea>
            </fieldset>
        </div>
        <div class="form-block">
            <fieldset>
                <legend>Module Status</legend>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-public" value="public" />
                    Public                
                </label>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-private" value="private" />
                    Private                
                </label>
            </fieldset>
            <fieldset>
            	<legend>Site Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_searchable" id="module_searchable">
                    Make the module searchable in the public search             
                </label>
            </fieldset>
            <fieldset>
            	<legend>CMS Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_local_search" id="module_local_search">
                    Enable local search<br />
                    <small class="muted">It will search only on the current module</small>         
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="show_refine_toolbar" id="show_refine_toolbar">
                    Show refine search toolbar
                </label>
                <label>Local search result page</label>
                <select name="module_local_search_view" id="module_local_search_view">
                <?php
				$files = $utils->get_folder_files(array('path' => '/admin/modules/'.$utils->view->module.'/'));
				if($files === false){
					$files = $utils->get_folder_files(array('path' => '/admin/modules/generic/'));	
				}//end if
				if($files !== false){
					for($i = 0; $i < sizeof($files); $i++){
						$search_action = str_replace(".php","",$files[$i]);
						$search_action = str_replace(".","/",$search_action);
						echo '<option value="'.$search_action.'">'.$search_action.'</option>';
					}//end for i
				}//end if
				?>
                </select>
                <br />
                <small class="muted">The search results will be displayed in the selected view</small>
            </fieldset>
        </div>     
        <div class="form-block">
        	<fieldset>
            	<legend>Posts</legend>
                <label>
                	General Sort Order
                    <select id="list_order" name="list_order">
                    	<option value="DESC">Newer first</option>
                        <option value="ASC">Older first</option>
                    </select>
                </label>
                <br />
                <label>
                	General Sort System
                    <select id="list_system" name="list_system">
                    	<option value="natural">Natural (default system)</option>
                        <option value="date">Order by Date</option>
                    </select>
                </label>
                <br />
                
                <label class="checkbox">
                    <input type="checkbox" value="1" name="enable_bulk_actions" id="enable_bulk_actions">
                    Enable bulk actions   
                </label>
                <br />
                <label>
                	Elements per page
                    <select id="items_per_page" name="items_per_page">
                    	<option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="0">Don't paginate and show everything</option>
                    </select>
                </label>
                <br />
                <?php
				require_once('inc/components/templates/templates.settings.php');
				?>
        	</fieldset>            
        </div>        
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
<script>
	$().ready(function(){
		$('button[data-btn-action=toggle-formats]').click(function(e){
			if($(this).hasClass('active')){
				$(this).removeClass('btn-success');	
				$(this).text("Disabled");			
			} else {				
				$(this).addClass('btn-success');	
				$(this).text("Enabled");
			}
		
			$selectedIds = new Array();
			
			$('button[data-btn-action=toggle-formats]').each(function(){
				if($(this).hasClass("btn-success")){
					$selectedIds.push($(this).attr("data-format-id"));
				}
			});
			
			//console.log($selectedIds);
			$("#module-default-formats").val($selectedIds);			
		});	
		
		$('#users-showable-groups').tokenfield({
		  autocomplete: {
			source: <?php			
			if($groups_list !== false){
				$groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){
					array_push($groups,array('value' => $groups_list[$i]['users_group_id'], 'label' => $groups_list[$i]['users_group_name']));
				}//end for i
				echo json_encode($groups);
			}
			?>,
			delay: 100
		  },
		  <?php
			if(isset($settings['usersShowableGroups'])){
				$saved_groups = explode(",",$settings['usersShowableGroups']);	
				$show_groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){							
					if(in_array((int)$groups_list[$i]['users_group_id'],$saved_groups)){
						array_push($show_groups,array('value' => $groups_list[$i]['users_group_id'],'label'=> $groups_list[$i]['users_group_name']));	
					}//end if
				}//end if
				echo 'tokens:';
				echo json_encode($show_groups);
				echo ',';
			}//end if
			?>
		  showAutocompleteOnFocus: true,
		  allowDuplicates: false
		});
		
		$('#users-write-groups').tokenfield({
		  autocomplete: {
			source: <?php			
			if($groups_list !== false){
				$groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){
					array_push($groups,array('value' => $groups_list[$i]['users_group_id'], 'label' => $groups_list[$i]['users_group_name']));
				}//end for i
				echo json_encode($groups);
			}
			?>,
			delay: 100
		  },
		  <?php
			if(isset($settings['usersWriteGroups'])){
				$saved_groups = explode(",",$settings['usersWriteGroups']);	
				$show_groups = array();
				for($i = 0; $i < sizeof($groups_list); $i++){							
					if(in_array((int)$groups_list[$i]['users_group_id'],$saved_groups)){
						array_push($show_groups,array('value' => $groups_list[$i]['users_group_id'],'label'=> $groups_list[$i]['users_group_name']));	
					}//end if
				}//end if
				echo 'tokens:';
				echo json_encode($show_groups);
				echo ',';
			}//end if
			?>
		  showAutocompleteOnFocus: true,
		  allowDuplicates: false
		})

	});
</script>