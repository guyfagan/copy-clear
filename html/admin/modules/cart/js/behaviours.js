// JavaScript Document

$().ready(function(){
	interceptor();
});

function interceptor(){
	//there is no space for Mel Gibson here
	var $bodyAttrs = $("body").attr("class").split(" ");
	for(i = 0; i < $bodyAttrs.length; i++){		
		if(parseInt($bodyAttrs[i].indexOf("action-")) != -1){
			$action = $bodyAttrs[i];			
			$action = $action.replace("action-","");
		}
	}
	
	switch($action){
		case "new":
			new_post_actions();			
			break;
		
		case "list":
			posts_table_actions();
			break;
		case "settings":
			settings_actions();			
			break;		
	}//end switch
}

//Specific actions
function new_post_actions(){
	
	/* DISABLE THE ENTER */
	$('input').bind("keyup", function(e) {
		var code = e.keyCode || e.which; 
	 	if (code  == 13) {               
			e.preventDefault();
			return false;
		}
	});
	
	$('#revisions-manager').revisionsManager();
	
	$(".date-pick").datepicker({
		dateFormat: "dd/mm/yy",
		minDate: new Date(2000, 1 - 1, 1)
	});	
	if($("#date").val() == ""){
		var $day = new Date().getDate();
		if($day < 10){
			$day = "0"+$day;	
		}
		var $month = new Date().getMonth()+1;
		if($month < 10){
			$month = "0"+$month;	
		}
		$('#date').val($day+ "/" + $month + "/" + new Date().getFullYear());
	}
	
	if($("#type").length > 0){		
		$("#type").on('change',function(){
			if($(this).val() == 'redirect'){
				$("#redirect-to").show();
			} else {
				$("#redirect-to").hide();
			}//end if
		});
	}//end if
	
	$('form').findOptionalFields();
	
	//now check if we have the id of the post, if yes load the data about it
	if($("#id").length > 0){
		$.cmsVars.post_id = $("#id").val();
		$("#save-post").load_post({id: $("#id").val(), module: $.config.module, prefix: "post_", meta: true, ignore: ['message']});
		$(".breadcrumb li:last").text("Edit");
	}//end if
	
	$(".permalink").handlePermalink({path: $.config.basepath});
		
	
	if($("#save-post").length > 0){
		//detecting if the user close the window before saving the changes
		/*$(window).unload(function(){
			if($.cmsVars.formSaved === false){
				if(confirm("You didn't save the changes, do you want to save it first?")){
					return false;	
				} else {
					return true;	
				}
			}
				
		});*/
		
		$("#save-post").applyCKEditor();
		
		$("#save-post input, #save-post textarea").change(function(){
			$.cmsVars.formSaved = false;	
		});
		
		$("#save-post").submit(function(e){
			e.preventDefault();	
			var $url = $(this).attr("action");			
			$.ajax({
				url: $url,
				type: "post",
				data: $(this).serialize()+"&temp=0&revision=1",
				dataType:"json",
				success: function(data){
					if(data.result == "success"){
						$("#save-post").popMessage("The post has been saved successfully",{type: "success"});
						$.cmsVars.formSaved = true;
						add_post_id(data.id);
					} else {
						$("#save-post").popMessage("The post has not been saved due to a server error",{type: "error"});
					}
				}	
			});
			$(window).scrollTop(0);
		});
		
		function add_post_id(id){
			if($("#id").length == 0){
				$("#save-post").append('<input type="hidden" id="id" name="id" value="'+id+'" />');
				$.cmsVars.post_id = id;
			}//end if
		}//end function
		
		$("#submit-form").click(function(e){
			e.preventDefault();
			var isValidated = $("#save-post").validate();
			if(isValidated){
				$("#save-post").submit();	
			} else {
				$("#save-post").popMessage("Please fill the fields in red first!",{type: "error"});
			}
		});
			
		if($("#save-and-close").length > 0){
			$("#save-and-close").click(function(e){
				e.preventDefault();	
				var isValidated = $("#save-post").validate();			
				if(isValidated){
					var $url = $("#save-post").attr("action");			
					$.ajax({
						url: $url,
						type: "post",
						data: $("#save-post").serialize()+"&temp=0&revision=1",
						dataType:"json",
						success: function(data){
							if(data.result == "success"){
								var $goto = $.config.basepath+'admin/'+$.config.module+'/';
								if($("#category_id").length > 0){
									$goto += 'list/'+$("#category_id").val()+'/';
								} else if($('input[name=category_id\\[\\]]').length > 0){
									$goto += 'list/'+$("input[name=category_id\\[\\]]:eq(0)").val()+'/';								
								}//end if								
								location.href = $goto;
							} else {
								$("#save-post").popMessage("The post has not been saved due to a server error",{type: "error"});
							}
						}	
					});
					$(window).scrollTop(0)
				} else {
					$("#save-post").popMessage("Please fill the fields in red first!",{type: "error"});
				}
			});
		}//end if
		
				
		//This function calls the media library
		$(".media-btn").click(function(e){
			e.preventDefault();			
			var $textarea = $(this).attr("data-textarea");
			$("#save-post").autoSave(function(){				
				$.cmsVars.post_id = $("#id").val();				
				//Once is saved, call the media manager					
				$("#media-library").mediaManager({textarea: $textarea, post_id: $.cmsVars.post_id});	
			});	
		});		
		
		$(".side-attachments-list").on("click","li a[data-btn-role=edit-attachment]",function(e){
			e.preventDefault();
			var $textarea = $(this).attr("data-textarea");
			$("#save-post").autoSave(function(){				
				$.cmsVars.post_id = $("#id").val();
				//Once is saved, call the media manager					
				$("#media-library").mediaManager({textarea: $textarea, post_id: $.cmsVars.post_id});	
			});	
		});
		
		
		$("#media-library").mediaManager('sidebarManager');
		
		//This function calls the sidebar
		$("#sidebar-btn").callSidebar({
			target: "#save-post div:first",
			form: "#save-post",
			collapsedClass: "compressed-form",
			expandedClass: "expanded-form",
			sidebarContainer: "#sidebar"
		});
		
		$('#mediaModal').on('hide', function() {
			$("#media-library").mediaManager('updateSidebar');
		});
		
		//Autosave function
		$.fn.autoSave = function(callback){
			var $form = $(this);
			var $url = $(this).attr("action");
			var $data = $form.serialize();
			var $alert_msg = "This post has been auto saved";
			if($("#id").length == 0){
				$data += "&temp=1"	
				$alert_msg += ", but is marked as temporary until you will not actually save the post with the Save button."
			}
			$data += "&revision=false";
			$.ajax({
				url: $url,
				type: "post",
				data: $data,
				dataType:"json",
				success: function(data){
					if(data.result == "success"){
						$form.popMessage($alert_msg,{type: "info"});
						$.cmsVars.formSaved = true;					
						add_post_id(data.id);
						//Callback
						if (typeof callback == 'function') { // make sure the callback is a function
							callback.call(this); // brings the scope to the callback
						}
					} else {
						$form.popMessage("The post has not been saved due to a server error",{type: "error"});
					}
				}	
			});
		}//end function
		
		//this prevent the accordion links to move the page up
		$('#sidebar h3 a').click(function(e){
			e.preventDefault();	
		});
		
		function getPostID(){
			var $id = $("#id").val();
			return $id;
		}
	}//end if
}//end function

function posts_table_actions(){
	//clone the buttons at the bottom of the table on the top of the table
	$('.table').before($('.buttons-bar').clone());
	
	/* THIS CODE MAKES THE TABLE SORTABLE */
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	$("table tbody").sortable({ 
		handle: ".table-row-handle", 
		axis: "y",
		helper: fixHelper,
		stop: function( event, ui ) {
			$('#table-form').saveTableSorting();
		}
	}).disableSelection();
	
	/* SEARCH */
	//This code is now useless as the search is now in the entire cms
	/*
	if($('#search-alert').length > 0){
		$('#search-alert').bind('closed',function(){
			var $href = location.href;
			$href = $href.split('?');
			$href = $href[0];
			location.href = $href;		
		});
	}//end if
	
	if($('.btn-search').length > 0){	
		$('.btn-search').click(function(e){
			e.preventDefault();
			var $key = $('.search-post').val();
			var $href = location.href;
			$href = $href.split('?');
			$href = $href[0];
			location.href = $href + '?search='+encodeURIComponent($key);	
		});
	}//end if
	*/
	/* THIS CODE CHANGE THE TYPE OF VIEW OF THE TABLE */	
	$("#table-view-type a").click(function(e){
		e.preventDefault();
		$("#table-view-type a").removeClass('active');
		$(this).addClass('active');	
		var $type = $(this).data('table-type');
		if($type == 'large-list'){
			$('.table').removeClass("table-small-list");
			$('.table').addClass("table-large-list");
			//$('.table-button-cta a.btn').removeClass("btn-mini");
		} else {
			$('.table').addClass("table-small-list");
			$('.table').removeClass("table-large-list");
			//$('.table-button-cta a.btn').addClass("btn-mini");
		}
		
	});
	
	//capture the delete action
	if($('a[data-action="delete"]').length > 0){
		$('a[data-action="delete"]').click(function(e){
			e.preventDefault();
			var $url = $(this).attr("href");
			var $tableLine = $(this).closest("tr");
			var $id = $(this).data('ref-id');
			var $label = $("#label-"+$id).text();
			$('#delete-item-modal').unbind('show');
			$('#delete-item-modal').on('show',function(){
				$('.modal-body',$(this)).html('Are you sure that you want to delete the entry <strong>&quot;'+$label+'&quot;</strong>');
				$('#confirm-delete').unbind('click');
				$('#confirm-delete').click(function(e){
					e.preventDefault();					
					$.ajax({
						url : $url,
						type: "get",
						dataType: "json",
						success: function(data){					
							if(data.result == "success"){
								$('#delete-item-modal').modal('hide');
								$tableLine.fadeOut("slow",function(){
									$(this).remove();										
								});	
							}
						}
					});
				});
			}).modal({ backdrop: true });
		});
	}
}//end function

function settings_actions(){
	
	$('form').findOptionalFields();
	
	$("#module-settings").load_settings({module: $.config.module});
	
	$("#module-settings").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				if(data.result == "success"){
					$("#module-settings").popMessage("The settings has been saved successfully",{type: "success"});
					$.cmsVars.formSaved = true;
				
				} else {
					$("#module-settings").popMessage("The settings has not been saved due to a server error",{type: "error"});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#module-settings").validate();
		if(isValidated){
			$("#module-settings").submit();	
		} else {
			$("#module-settings").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
}