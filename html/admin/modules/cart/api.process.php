<?php
	if($method == "update.cart.status"){
		$cart_engine = $_POST['cart_engine'];
		if(!isset($_POST['cart_engine'])){
			$cart_engine = 'cart';
		}//end if
		
		$status = $_POST['status'];
		$cart_id = $_POST['cart_id'];
		
		$cart = $utils->call($cart_engine);
		$data = array();
		$result = $cart->set_cart_status(array(
			'status' => $status,
			'cart_id' => $cart_id
		));
		if($result){
			$data['result'] = 'success';
		} else {
			$data['result'] = 'error';	
		}//end if
		
		echo json_encode($data);
	}//end if
?>