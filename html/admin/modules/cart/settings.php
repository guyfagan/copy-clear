<?php
	$module_data = $utils->get_module($module);
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <div class="form-block">
            <fieldset>
                <legend>Module Status</legend>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-public" value="public" />
                    Public                
                </label>
                <label class="radio inline">
                    <input type="radio" name="module-status" id="module-status-private" value="private" />
                    Private                
                </label>
            </fieldset>
            <fieldset>
            	<legend>Site Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_searchable" id="module_searchable">
                    Make the module searchable in the public search             
                </label>
            </fieldset>
            <fieldset>
            	<legend>CMS Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_local_search" id="module_local_search">
                    Enable local search<br />
                    <small class="muted">It will search only on the current module</small>         
                </label>
                <label>Local search result page</label>
                <select name="module_local_search_view" id="module_local_search_view">
                <?php
				$files = $utils->get_folder_files(array('path' => '/admin/modules/'.$utils->view->module.'/'));
				if($files === false){
					$files = $utils->get_folder_files(array('path' => '/admin/modules/generic/'));	
				}//end if
				if($files !== false){
					for($i = 0; $i < sizeof($files); $i++){
						$search_action = str_replace(".php","",$files[$i]);
						$search_action = str_replace(".","/",$search_action);
						echo '<option value="'.$search_action.'">'.$search_action.'</option>';
					}//end for i
				}//end if
				?>
                </select>
                <br />
                <small class="muted">The search results will be displayed in the selected view</small>
            </fieldset>
        </div>  
        <div class="form-block">
            <fieldset>
                <legend>Cart Settings</legend>            
                <label>Cart Storage</label>
                <select name="cart_storage_type"  id="cart_storage_type" data-toggle="optional-field">
                	<option value="cookie"<?php if($settings['login_type'] == "cookie"){ echo ' selected'; } ?>>Cookies</option>
                    <option value="session"<?php if($settings['login_type'] == "session"){ echo ' selected'; } ?>>Sessions</option>
                </select>
                <div data-type="template-optional-field" data-template-target="#cart_storage_type" data-template-match="cookie">
                	<label>Cookie expires in</label>
                    <select id="cart_cookie_expiry" name="cart_cookie_expiry">
                    	<option value="<?php echo(60*30) ?>">30 Minutes</option>
                        <option value="<?php echo(60*60) ?>">1 Hour</option>
                        <option value="<?php echo(60*60*6) ?>">6 Hours</option>
                        <option value="<?php echo(60*60*24) ?>">1 Day</option>
                        <option value="<?php echo(60*60*24*7) ?>">1 Week</option>
                        <option value="<?php echo(60*60*24*31) ?>">1 Month</option>
                    </select>
                </div>         
                <label>Session/Cookie prefix</label>
                <input type="text" class="input-large" name="session_prefix" id="session_prefix" />
                <label>Credit Card managed by</label>
                <select name="credit_system" id="credit_system">
                	<option value="none">None</option>
                    <option value="paypal">PayPal</option>
                    <option value="realex">Realex</option>
                    <option value="sagepay">Sagepay</option>
                </select>
                <label>Identification String based on</label>
                <select name="cart_unique_id_base" id="cart_unique_id_base" data-toggle="optional-field">
                	<option value="session_id"<?php if($settings['cart_unique_id_base'] == "session_id"){ echo ' selected'; } ?>>Session ID</option>
                    <option value="user_id"<?php if($settings['cart_unique_id_base'] == "user_id"){ echo ' selected'; } ?>>User ID</option>
                </select>
                <div data-type="template-optional-field" data-template-target="#cart_unique_id_base" data-template-match="user_id">
                	<div class="alert"><strong>Warning!</strong> The identification based on the User ID will work only if the cart is protected by user authentification and it will not work anonymously.
                </div>
            </fieldset>
        </div>
        <div class="form-block">
            <fieldset>
                <legend>Cart Emails</legend>   
                <label>No-Reply Email</label>
                <input type="text" class="input-xxlarge" name="cart_noreply_email" id="cart-noreply-email">
                <label>Notification Email</label>
                <input type="text" class="input-xxlarge" name="cart_notification_email" id="cart-notification-email">
                <label class="checkbox">
                	<input type="checkbox" value="1" name="cart_send_invoice" id="cart-send-invoice"> Send an invoice by email
                </label>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					$emailTemplateName = 'cart_invoice_template';					
					include('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
            </fieldset>
        </div>
        <input type="hidden" name="list_order" id="list_order" value="DESC" />
        <input type="hidden" name="list_system" id="list_system" value="natural" />        
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
<script>
	$().ready(function(){
		$('button[data-btn-action=toggle-formats]').click(function(e){
			if($(this).hasClass('active')){
				$(this).removeClass('btn-success');	
				$(this).text("Disabled");			
			} else {				
				$(this).addClass('btn-success');	
				$(this).text("Enabled");
			}
		
			$selectedIds = new Array();
			
			$('button[data-btn-action=toggle-formats]').each(function(){
				if($(this).hasClass("btn-success")){
					$selectedIds.push($(this).attr("data-format-id"));
				}
			});
			
			console.log($selectedIds);
			$("#module-default-formats").val($selectedIds);
			
		});	
	});
</script>