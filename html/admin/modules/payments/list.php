<?php
	$category_id = $_GET['id'];
	$params = array();
	$new_post_link = "/new/";
	$new_category_link = "/category/new/";
	$has_father_id = false;
	$father_id = 0;
	if(!is_null($category_id) && is_numeric($category_id)){
		$new_post_link = "/new/in/".$category_id;
		$has_father_id = true;
		$new_category_link = "/category/new/in/".$category_id;
		$params['category_id'] = $category_id;	
		$father_id = $category_id;
	}//end if
		
	$settings = $utils->view->get_module_settings();
	$show_thumbs = false;
	if(isset($settings['show_thumbnails']) && (bool)$settings['show_thumbnails']){
		$params['default_photo'] = true;
		$show_thumbs = true;
	}//end if
	$elements_per_page = 25;
	if(isset($settings['items_per_page']) && is_numeric($settings['items_per_page'])){
		$elements_per_page = $settings['items_per_page'];
	}//end if
	
	$list_system = 'natural';
	if(isset($settings['list_system'])){
		$list_system = $settings['list_system'];
	}//end if
	
	if($list_system == 'date'){
		$params['sort_by'] = 'item_date';
	}//end if
	
	if(isset($_REQUEST['search'])){
		$params['search'] = $_REQUEST['search'];
	}//end if
		
	$view_mode = "default";
	if(isset($settings['list_view_mode'])){
		$view_mode = $settings['list_view_mode'];		
	}//end if
	
	$paginated_results = false;
	if($elements_per_page > 0){
		$paginated_results = true;
		$utils->create_paging($_GET['page'],$elements_per_page);
	}//end if
	
	$list = ${$module}->get_payments($params);
		
	# PAGINATION #
	if($paginated_results){
		$paging_stats = $utils->get_paging_stats();	
		$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
		//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
		$utils->unset_paging();
		$page_start = $paging_stats['start']+1;
		$page_end = $paging_stats['start']+10;
		if($page_end > $paging_stats['founds']){
			$page_end = $paging_stats['founds'];
		}//end if
		$page_total = $paging_stats['pages'];
		$page_current = $paging_stats['current_page'];
		$page_prev = $page_current-1;
		if($page_prev == "0"){
			$page_prev = 1;
		}//end if
		$page_next = $page_current+1;
		if($page_next > $page_total){
			$page_next = $page_total;
		}//end if
	}//end if
	# END PAGINATION #
	
	
	$table_type = "table-large-list";
	if(isset($settings['list_view_type'])){
		if($settings['list_view_type'] == "small"){
			$table_type = "table-small-list";
		}//end if
	}//end if
		
?>
<form action="#" name="table-form" id="table-form" method="post">
	<input type="hidden" name="page" id="page" value="<?php echo $page_current ?>" />
    <input type="hidden" name="total-items" id="total-items" value="<?php echo $paging_stats['founds'] ?>" />
    <input type="hidden" name="items-per-page" id="items-per-page" value="<?php echo $elements_per_page ?>" />
    <input type="hidden" name="parent-id" id="parent-id" value="<?php echo $father_id ?>" />
    <input type="hidden" name="module" id="module" value="<?php echo $module ?>" />
    <table class="table table-striped table-hover <?php echo $table_type ?>">
        <thead>
            <tr>
                <th>Payments</th>         
                <th>Description</th>
                <th>Method</th>
                <th>Status</th>
                <td>
                    <div class="btn-group pull-right" id="table-view-type">
                        <a href="#" data-table-type="large-list" class="btn btn-mini<?php if($table_type == "table-large-list"){ echo ' active'; } ?>"><i class="icon-th-list"></i></a>
                        <a href="#" data-table-type="small-list" class="btn btn-mini<?php if($table_type == "table-small-list"){ echo ' active'; } ?>"><i class="icon-list"></i></a>
                    </div>
                </td>
            </tr>
        </thead>
        <?php
        if($paginated_results){
            if($page_total > 1){
            ?>
            <tfoot>
                <tr>
                    <td colspan="5">
                        <div class="pagination">
                            <ul>
                                <li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
                                <?php
                                if(is_array($paging_list) && sizeof($paging_list) > 0){
                                    for($i = 0; $i < sizeof($paging_list); $i++){
                                        echo '<li';
                                        if($paging_list[$i]['page'] == $page_current){
                                            echo ' class="active"';	
                                        }//end if
                                        echo '><a href="'.$paging_list[$i]['url'].'">'.$paging_list[$i]['page'].'</a></li>';
                                    }//end for i
                                }//end if
                                ?>                      
                                <li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </tfoot>
            <?php
            }//end if
        }//end if
        ?>
        <tbody>
            <?php
            if($list !== false){
                for($i = 0; $i < sizeof($list); $i++){               
					$id = $list[$i]['payment_id'];					
					//Vars in case of Post
					$link_edit = __MODULE_PATH__."/edit/".$id;		
					$link_view = __MODULE_PATH__."/view/".$id;					
					$link_delete = __CMS_PATH__."/api/delete.payment?id=".$id."&module=".$module;
					$link_related_view = __CMS_PATH__.$list[$i]['module_name'].'/view/'.$list[$i]['payment_rel_id'];
					$label = "Payment #".$id." - ".$list[$i]['payment_code'];
					$icon = "icon-money";
					if($list[$i]['payment_method'] == 'credit_card'){
						$icon = 'icon-credit-card ';	
					}//end if					
					$move_path = "/move/post/".$list[$i]['payment_id'];  
            ?>
            <tr class="table-row">            	
                <td>                       
                    <i class="<?php echo $icon ?>"></i> <?php 
                    echo '<span id="label-'.$id.'">'.$label;                  
                    echo '</span>'; 
                    
					//show the post date
					echo '<span class="table-info-line muted"><i class="icon-calendar"></i> '.date('d/m/Y',$list[$i]['payment_date']).'</span>';
					//show the user
					//echo '<span class="table-info-line muted"><i class="icon-user"></i> created by <strong>'.$list[$i]['user_username'].'</strong></span>';
                    
                    ?>
                </td> 
                <td><?php 
				echo $list[$i]['payment_description'];
				if($list[$i]['payment_rel_id'] > 0){
					echo '<br /><small><a href="'.$link_related_view.'">View details</a></small>';	
				}
				?></td>
                <td>
                <?php
					echo ucwords(str_replace('_',' ',$list[$i]['payment_method']));
				?>
                </td>
                <td class="table-row-status"><?php 
					switch($list[$i]['payment_status']){
						case "denied":
							echo '<span class="label label-important">'.ucwords($list[$i]['payment_status']).'</span>';
							break;
						case "pending":
							echo '<span class="label label-warning">'.ucwords($list[$i]['payment_status']).'</span>';
							break;
						default:	
							echo '<span class="label label-success">'.ucwords($list[$i]['payment_status']).'</span>';
					}//end switch
				?></td>
                <td class="table-row-options">
                    <input type="hidden" name="table-row-id[]" id="table-row-id-<?php echo $id ?>" value="<?php echo $id ?>" />
                    <input type="hidden" name="table-row-type[]" id="table-row-type-<?php echo $id ?>" value="<?php echo (int)$is_category ?>" />
                    <div class="btn-group pull-right table-button-cta">
                    	<a href="<?php echo $link_view ?>" class="btn"><i class="icon-eye-open"></i></a>
                        <?php
                        if($perms[$module] > 3){
							/*
                        ?>
                        <a href="<?php echo $link_edit ?>" class="btn"><i class="icon-edit"></i></a>                           
                        <a href="<?php echo $link_delete ?>" data-action="delete" data-ref-id="<?php echo $id ?>" class="btn btn-warning"><i class="icon-trash"></i></a>
                        <?php
						*/
                        }//end if
                        ?>
                    </div>            	
                </td>
            </tr>
            <?php		
                }//end for i
            }//end if
            ?>
        </tbody>
    </table>  
</form>
<script>
	$().ready(function() {
        $(".switcher span").jSwicher();
    });
</script>

<div class="modal hide fade" id="delete-item-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Confirm Delete</h3>
    </div>
    <div class="modal-body">
       
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">No, forget it</a>
        <a href="#" class="btn btn-primary" id="confirm-delete">Yes delete it</a>
    </div>
</div>