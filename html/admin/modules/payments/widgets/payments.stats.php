<?php
	$payments = $this->utils->call('payments');
	$stats = $payments->get_stats();	
	$palette = array("#004158","#038B8B","#F09979","#F1EEC9","#026675");
	if($stats !== false){
		$data = array();		
		//we count the total
		$total = 0;
		for($i = 0; $i < sizeof($stats); $i++){
			$total += $stats[$i]['total'];
		}//end for i
		for($i = 0; $i < sizeof($stats); $i++){
			$data[$i] = array('value' => (($stats[$i]['total'] * 100) / $total),'color' => $palette[$i]);
			$stats[$i]['perc'] = $data[$i]['value'];
		}//end for i
?>
	<style>
        #payments-chart{
            float: left;
        }
        
        .chart-legend{
            margin: 0;
            padding: 0;
            float: left;
        }
        
        .chart-legend li{
            margin: 0;
            padding: 0;
            list-style-type: none;
        }
        
        .dot{
            float: left;
            width: 15px;
            height: 15px;
            margin: 0 10px 0 0;	
        }
    </style>
	<h3>Payments</h3>
    <canvas id="payments-chart" width="100" height="100" data-type="Doughnut"></canvas>
    <ul class="chart-legend">
    <?php
		for($i = 0; $i < sizeof($stats); $i++){
			echo '<li><span class="dot" style="background-color: '.$palette[$i].'"></span>'.$stats[$i]['module_label'].' - '.number_format($stats[$i]['perc'],1).'%</li>';
		}//end for
	?>
    </ul>
    <script>
        var ctx = document.getElementById("payments-chart").getContext("2d");
        new Chart(ctx).Doughnut(<?php echo json_encode($data) ?>);
    </script>
<?php
	}//end if
?>