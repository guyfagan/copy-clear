<?php
	$id = $_GET['id'];
	if(!is_null($id) && is_numeric($id)){
		$payment_data = ${$module}->get_payment(array('pay_id' => $id));	
		if($payment_data !== false){			
?>
<div class="payment-view clearfix">
	<h2>Payment ID #<?php echo $id ?></h2>
    <hr />
    <h4>Payment Details</h4>
    <p>
    	<strong>Date:</strong> <?php echo date('d/m/Y H:i:s',$payment_data['payment_date']); ?><br />
        <strong>Method:</strong> <?php echo ucwords(str_replace("_"," ",$payment_data['payment_method'])) ?><br />
        <strong>Amount:</strong> &euro;<?php echo number_format($payment_data['payment_amount'],2) ?><br />   
        <strong>Description:</strong> <?php echo $payment_data['payment_description'] ?><br />   
        <strong>Code:</strong> <?php echo $payment_data['payment_code'] ?><br />
        <strong>Payment System:</strong> <?php echo ucwords($payment_data['payment_system']) ?><br />
        <strong>Status:</strong> <?php 
		switch($payment_data['payment_status']){
			case "denied":
				echo '<span class="label label-important">'.ucwords($payment_data['payment_status']).'</span>';
				break;
			case "pending":
				echo '<span class="label label-warning">'.ucwords($payment_data['payment_status']).'</span>';
				break;
			default:	
				echo '<span class="label label-success">'.ucwords($payment_data['payment_status']).'</span>';
		}
		?>        
    </p>
    <?php
	if($payment_data['payment_method'] == 'credit_card'){
		$response = unserialize($payment_data['payment_response']);		
	?>
    <hr />
    <h4>Credit Card Details</h4>
    <p>
    	<strong>Billing Name:</strong> <?php echo $payment_data['payment_billing_name'] ?><br />
        <strong>Card Type:</strong> <?php echo $payment_data['payment_card'] ?><br />
        <strong>Card Expiry:</strong> <?php echo $payment_data['payment_card_expiry'] ?><br />
    </p>
    <?php
		if(is_array($response)){
	?>
    <hr />
    <h4><i class="icon icon-credit-card"></i> Response from <?php echo ucwords($payment_data['payment_system']) ?></h4>
    <table class="table table-striped">
    	<thead>
        	<tr>
            	<th>Key</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
        <?php
		foreach($response as $key => $value){
		?>
        	<tr>
            	<td><?php echo $key ?></td>
                <td><?php 
				if(!is_array($value)){
					echo $value;
				} else {
					foreach($value as $error){
						echo $error.'<br />';	
					}//end foreach
				}//end if
				?></td>
            </tr>
        <?php
		}//end foreach
		?>
        </tbody>
    </table>
    <?php	
		}//end if
	}//end if
	?>
</div>
<?php		
		} else {
?>
	<div class="alert alert-error"><strong>Error:</strong> No payment found with the passed ID</div>
<?php			
		}//end if
	} else {
?>
	<div class="alert alert-error"><strong>Error:</strong> No payment ID passed</div>
<?php
	}//end if	
?>