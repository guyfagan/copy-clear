<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->get_settings(array('module' => $module));
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <div class="form-block">
            <fieldset>
            	<legend>Merchant Account</legend>
                <div class="control-group">
                	<label>Payment System</label>
                    <select name="paymentSystem" id="payment-system" data-toggle="optional-field">
                    	<option value="none"<?php if($settings['paymentSystem'] == 'none'){ echo ' selected'; } ?>>None</option>
                        <option value="realex"<?php if($settings['paymentSystem'] == 'realex'){ echo ' selected'; } ?>>Realex</option>
                        <option value="sagepay"<?php if($settings['paymentSystem'] == 'sagepay'){ echo ' selected'; } ?>>Sagepay</option>
                    </select>
                </div>
                <div data-type="template-optional-field" data-template-target="#payment-system" data-template-match="realex">
                    <div class="control-group">
                        <label>Merchant ID</label>
                        <input type="text" name="paymentMerchantID" id="merchant-id" class="input-large" />
                    </div>                    
                    <div class="control-group">
                        <label>Account</label>
                        <input type="text" name="paymentAccount" id="account" class="input-large" />
                    </div>
                    <div class="control-group">
                        <label>Secret</label>
                        <input type="text" name="paymentSecret" id="secret" class="input-large" />
                    </div>
                    <div class="control-group">
                    	<label>Mode</label>
                        <select name="realexMode" id="realex-mode" class="input-large">
                        	<option value="live">Live</option>
                            <option value="test">Test</option>
                        </select>
                    </div> 
                </div> 
                <div data-type="template-optional-field" data-template-target="#payment-system" data-template-match="sagepay">
               		<div class="control-group">
                        <label>Vendor</label>
                        <input type="text" name="paymentVendor" id="vendor" class="input-large" />
                        <small class="muted">Only if you are using SagePay</small>
                    </div>       
                    <div class="control-group">
                    	<label>Mode</label>
                        <select name="sagepayMode" id="sagepay-mode" class="input-large">
                        	<option value="live">Live</option>
                            <option value="test">Test</option>
                            <option value="simulator">Simulator</option>
                        </select>
                    </div>        
                </div>
                <div class="control-group">
                	<label class="checkbox">
                    	<input type="checkbox" value="1" name="paymentSecureServer" id="secure-server" /> SSL Enabled
                        <br />
                        <small class="muted">Please tick this option if you are using a secure server.</small>
                    </label>
                </div>
                <div class="control-group">
                	<label>Currency</label>
                    <select name="paymentCurrency" id="currency">
                    	<option value="EUR">Euro</option>
                        <option value="GBP">British Pound</option>
                        <option value="USD">US Dollar</option>
                        <option value="AUD">Australian Dollar</option>
                        <option value="CAD">Canadian Dollar</option>
                    </select>
                </div>
            </fieldset>
            <fieldset>
            	<legend>Site Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_searchable" id="module_searchable">
                    Make the module searchable in the public search             
                </label>
            </fieldset>
            <fieldset>
            	<legend>CMS Search</legend>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="module_local_search" id="module_local_search">
                    Enable local search<br />
                    <small class="muted">It will search only on the current module</small>         
                </label>
                <label>Local search result page</label>
                <select name="module_local_search_view" id="module_local_search_view">
                <?php
				$files = $utils->get_folder_files(array('path' => '/admin/modules/'.$utils->view->module.'/'));
				if($files === false){
					$files = $utils->get_folder_files(array('path' => '/admin/modules/generic/'));	
				}//end if
				if($files !== false){
					for($i = 0; $i < sizeof($files); $i++){
						$search_action = str_replace(".php","",$files[$i]);
						$search_action = str_replace(".","/",$search_action);
						echo '<option value="'.$search_action.'">'.$search_action.'</option>';
					}//end for i
				}//end if
				?>
                </select>
                <br />
                <small class="muted">The search results will be displayed in the selected view</small>
            </fieldset>
        </div>  
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
<script>
	$().ready(function(){
		$('button[data-btn-action=toggle-formats]').click(function(e){
			if($(this).hasClass('active')){
				$(this).removeClass('btn-success');	
				$(this).text("Disabled");			
			} else {				
				$(this).addClass('btn-success');	
				$(this).text("Enabled");
			}
		
			$selectedIds = new Array();
			
			$('button[data-btn-action=toggle-formats]').each(function(){
				if($(this).hasClass("btn-success")){
					$selectedIds.push($(this).attr("data-format-id"));
				}
			});
			
			console.log($selectedIds);
			$("#module-default-formats").val($selectedIds);
			
		});	
	});
</script>