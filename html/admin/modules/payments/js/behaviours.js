// JavaScript Document

$().ready(function(){
	interceptor();
});

function interceptor(){
	//there is no space for Mel Gibson here
	var $bodyAttrs = $("body").attr("class").split(" ");
	for(i = 0; i < $bodyAttrs.length; i++){		
		if(parseInt($bodyAttrs[i].indexOf("action-")) != -1){
			$action = $bodyAttrs[i];			
			$action = $action.replace("action-","");
		}
	}
	
	switch($action){		
		case "list":
			posts_table_actions();
			break;
		case "settings":
			settings_actions();			
			break;		
	}//end switch
}




function posts_table_actions(){
	//clone the buttons at the bottom of the table on the top of the table
	$('.table').before($('.buttons-bar').clone());
	
	/* THIS CODE MAKES THE TABLE SORTABLE */
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	$("table tbody").sortable({ 
		handle: ".table-row-handle", 
		axis: "y",
		helper: fixHelper,
		stop: function( event, ui ) {
			$('#table-form').saveTableSorting();
		}
	}).disableSelection();
	
	
	/* THIS CODE CHANGE THE TYPE OF VIEW OF THE TABLE */	
	$("#table-view-type a").click(function(e){
		e.preventDefault();
		$("#table-view-type a").removeClass('active');
		$(this).addClass('active');	
		var $type = $(this).data('table-type');
		if($type == 'large-list'){
			$('.table').removeClass("table-small-list");
			$('.table').addClass("table-large-list");
			//$('.table-button-cta a.btn').removeClass("btn-mini");
		} else {
			$('.table').addClass("table-small-list");
			$('.table').removeClass("table-large-list");
			//$('.table-button-cta a.btn').addClass("btn-mini");
		}
		
	});
	
	//capture the delete action
	if($('a[data-action="delete"]').length > 0){
		$('a[data-action="delete"]').click(function(e){
			e.preventDefault();
			var $url = $(this).attr("href");
			var $tableLine = $(this).closest("tr");
			var $id = $(this).data('ref-id');
			var $label = $("#label-"+$id).text();
			$('#delete-item-modal').unbind('show');
			$('#delete-item-modal').on('show',function(){
				$('.modal-body',$(this)).html('Are you sure that you want to delete the entry <strong>&quot;'+$label+'&quot;</strong>');
				$('#confirm-delete').unbind('click');
				$('#confirm-delete').click(function(e){
					e.preventDefault();					
					$.ajax({
						url : $url,
						type: "get",
						dataType: "json",
						success: function(data){					
							if(data.result == "success"){
								$('#delete-item-modal').modal('hide');
								$tableLine.fadeOut("slow",function(){
									$(this).remove();										
								});	
							}
						}
					});
				});
			}).modal({ backdrop: true });
		});
	}
}//end function


function settings_actions(){
	
	$("#module-settings").load_settings({module: $.config.module});
	
	$('form').findOptionalFields();
	
	$("#module-settings").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				if(data.result == "success"){
					$("#module-settings").popMessage("The settings has been saved successfully",{type: "success"});
					$.cmsVars.formSaved = true;
				
				} else {
					$("#module-settings").popMessage("The settings has not been saved due to a server error",{type: "error"});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#module-settings").validate();
		if(isValidated){
			$("#module-settings").submit();	
		} else {
			$("#module-settings").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
}