<?php
	error_reporting(E_ERROR);
	$formats = $utils->get_attachments_formats();
	$mediaSettings = $utils->get_settings(array('module' => 'media'));
?>	
<div class="page-view clearfix">
	<form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="media-settings" name="settings">
    	<h2>Media Settings</h2>
        <div class="form-block">
            <fieldset>
                <legend>Basic</legend>
                <div class="form-inline">
                	<div class="control-group">
                    	<label class="checkbox">
                            <input type="checkbox" name="hideMediaLibraryOnNav" id="hide-media-library-on-nav" value="1" />
                            Hide the Media section in the navigation
                        </label>
                    </div>
        			<div class="control-group">                    	
                        <label class="checkbox">
                            <input type="checkbox" name="resizeLargeImages" id="resize-large-images" value="1" />
                            Resize images larger than
                        </label>
                        <div class="input-append">
                            <input type="text" name="resizeImagesWidth" id="resize-images-width" placeholder="Width" class="input-mini" /> 
                            <span class="add-on">px</span>
                        </div>
                        X
                        <div class="input-append">
                            <input type="text" name="resizeImagesHeight" id="resize-images-height" placeholder="Height" class="input-mini" /> 
                            <span class="add-on">px</span>
                        </div>
                        <label>to the following format</label>
                        <select id="resize-images-format" name="resizeImagesFormat">
                        <?php
                        $formats = $utils->get_attachments_formats();
                        if($formats !== false){
                            for($i = 0; $i < sizeof($formats); $i++){
                                echo '<option value="'.$formats[$i]['attachment_setting_id'].'">'.$formats[$i]['attachment_setting_label'].'</option>';
                            }//end for i
                        }//end if
                        ?>
                        </select>
                	</div>                    
                </div>   
        	</fieldset>   
            <fieldset>
                <legend>Media Library</legend>
                <div class="form-inline">
        			<div class="control-group">
                        <label>Use the following format for the Media Library's thumbnails</label>
                        <select name="mediaGalleryThumsFormat" id="media-gallery-thumbs-format">
                        <?php
							if($formats !== false){
								for($i = 0; $i < sizeof($formats); $i++){
									echo '<option value="'.$formats[$i]['attachment_setting_folder'].'">',$formats[$i]['attachment_setting_label'].'</option>';
								}//end for
							}//end  if
						?>
						</select>
                    </div>
                </div>  
                <div class="form-inline">
        			<div class="control-group">
                        <label>Paginate attachments by</label>
                        <select name="mediaGalleryItemsPerPage" id="media-gallery-items-per-page">
                        	<option value="16">16</option>
                            <option value="32">32</option>
                            <option value="48">48</option>
                            <option value="64">64</option>
						</select>
                    </div>
                </div>           
            </fieldset> 
            <fieldset>
            	<legend>Users Restrictions</legend>
                <label class="checkbox">
                	<input type="checkbox" name="mediaGalleryIsolateUsers" id="media-gallery-isolate-users" value="1" /> Isolate users <small class="muted">The users will see only the photos they have uploaded</small>
                </label>
                <label>Isolation Type</label>
                <select name="mediaGalleryIsolatationType" id="media-gallery-isolation-type">
                	<option value="hard">Hard</option>
                    <option value="soft">Soft</option>
                </select>
            </fieldset>
            <fieldset>
            	<legend>Media Roles</legend>
                <p>There are two default roles for attachments, <em>"generic"</em> and <em>"default-image"</em>, but if you want to specify other roles, please add in the field below:</p>                
                <div class="control-group">
                    <label>Additional Roles <small class="muted">No spaces, or accents, or any special caracthers, if you want to separate two words, use the "-" symbol</small></label>
                    <input type="text" name="mediaRoles" id="mediaRoles" class="input-xxlarge"<?php
					if(isset($mediaSettings['mediaRoles'])){
						echo ' value="'.$mediaSettings['mediaRoles'].'"';	
					}//end if
					?> />
                </div>
               	<label>Default Role</label>
                <select name="mediaDefaultRole" id="mediaDefaultRole">
                	<option value="generic"<?php
                    if(!isset($mediaSettings['mediaDefaultRole']) || (isset($mediaSettings['mediaDefaultRole']) && $mediaSettings['mediaDefaultRole'] == 'generic')){
						echo ' selected';	
					}//end if
					?>>Generic (Default)</option>
                    <?php
					if(isset($mediaSettings['mediaRoles']) && $mediaSettings['mediaRoles'] != ""){
						$extraRoles = explode(",",$mediaSettings['mediaRoles']);
						if(is_array($extraRoles) && sizeof($extraRoles) > 0){
							for($x = 0; $x < sizeof($extraRoles); $x++){
								echo '<option value="'.trim($extraRoles[$x]).'"';
								if(isset($mediaSettings['mediaDefaultRole'])){
									if($mediaSettings['mediaDefaultRole'] == trim($extraRoles[$x])){
										echo ' selected';	
									}//end if
								}//end if
								echo '>'.ucwords($extraRoles[$x]).'</options>';
							}//end for x
						}//end if
					}//end if
					?>
                </select>
            </fieldset>              
    	</div>
        <h2>Amazon S3 Settings</h2>
        <div class="form-block">
        	<fieldset>
            	<legend>Amazon S3 Auth</legend>
                <label>Access Key</label>
                <input type="text" name="mediaS3AccessKey" id="media-s3-access-key" class="input-block-level">
                <label>Secret Key</label>
                <input type="text" name="mediaS3SecretKey" id="media-s3-secret-key" class="input-block-level">
            </fieldset>  
            <fieldset>
            	<legend>Amazon S3 Bucket Options</legend>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="mediaS3ForceCopy" id="media-s3-force-copy"> Copy the video files on S3
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="mediaS3RemoveOriginal" id="media-s3-remove-original"> Once uploaded on S3, remove the original file
                </label>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="mediaS3DontRemoveFile" id="media-s3-dont-remove-file"> Don't remove the file on S3 on attachment delete
                </label>
                <label>Copy the files in the following bucket</label>
                <input type="text" name="mediaS3DestinationBucket" id="media-s3-destination-bucket" class="input-block-level">
                <label>Amazon S3 Region</label>
                <select id="media-s3-region" name="mediaS3Region">
                	<option value="s3-eu-west-1">EU (Ireland) Region</option>
                    <option value="s3-us-east-1">US Standard *</option>
                    <option value="s3-us-west-2">US West (Oregon) Region</option>
                    <option value="s3-us-west-1">US West (Northern California) Region</option>                  
                </select>                
            </fieldset>    
            <fieldset>
            	<legend>Amazon Elastic Transcoder Options</legend>
                <?php
				if(is_null($mediaSettings['mediaS3AccessKey']) || is_null($mediaSettings['mediaS3SecretKey'])){
				?>
                <div class="alert alert-warning">Please fill the fields in the Amazon S3 Auth first</div>
                <?php
				} else {
					require_once("inc/elasticTranscoder/ElasticTranscoder.php");
					$awsRegion = $mediaSettings['mediaS3Region'];
					$awsRegion = str_replace('s3-','',$awsRegion);					
					$et = new AWS_ET($mediaSettings['mediaS3AccessKey'], $mediaSettings['mediaS3SecretKey'], $awsRegion);
					//var_dump($et);					
					$list = $et->listPipelines();					
					$list = $list['Pipelines'];
					?>                    
                    <label>Select Pipeline</label>
                    <select name="mediaS3Pipeline" id="media-s3-pipeline">
                    <?php
					if(is_array($list)){
						if(sizeof($list) > 0){
							for($i = 0; $i < sizeof($list); $i++){
								echo '<option value="'.$list[$i]['Id'].'">'.$list[$i]['Name'].'</option>';
							}//end for i
						}//end if
					}//end if
					?>
                    </select>
                    <div class="control-group">
                        <label>Save the transcoded files in the following subfolder</label>
                        <input type="text" name="mediaS3TranscodedDestFolder" id="media-s3-transcoded-dest-folder" class="input-xlarge">
                    </div>	
                    <?php
					echo '<h4>Select which presets to use</h4>';
					echo '<div class="control-group">';
					$presets = $et->listPresets();
					$presets = $presets['Presets'];					
					$saved_presets = unserialize($mediaSettings['mediaS3ElasticPreset']);			
					if(is_array($presets)){
						echo '<div class="row-fluid">';
						for($i = 0; $i < sizeof($presets); $i++){
							$id = $presets[$i]['Id'];
						?>
                        <label class="checkbox inline span3">
                        	<input type="checkbox" id="preset-<?php echo $id ?>" name="mediaS3ElasticPreset[]" value="<?php echo $id ?>"<?php
                            if(in_array($id,$saved_presets)){
								echo ' checked';	
							}//end if
							?>> <?php echo $presets[$i]['Name'] ?>
                        </label>
                        <?php
							if($i > 0 && $i%3 == 0){
								echo '</div><div class="row-fluid">';	
							}//end if						
						}//end for i
						echo '</div>';
					}//end if
					echo '</div>';				
				}//end if
				?>                
            </fieldset>
        </div>
        <button type="submit" class="btn btn-primary">Save Settings</button>
    </form>
</div>

<div class="page-view clearfix">
	<h2>Media Formats</h2>
    <div class="settings-image-formats clearfix">        
        <?php		
		if($formats !== false){
			for($i = 0; $i < sizeof($formats); $i++){
				$format_data = $formats[$i];	
				echo '<div class="settings-image-format" id="format-view-'.$format_data['attachment_setting_id'].'">';
				include("inc/components/media/media.format.view.php");
				echo '</div>';
				if($i % 2 && $i != (sizeof($formats)-1)){
					echo '</div><div class="settings-image-formats clearfix">';	
				}//end if
				
			}//end for i
		}//end if
		?>       
	</div>
    <a href="#newFormat" id="add-format-btn" data-toggle="modal" data-keyboard="false" class="btn btn-primary btn-small">Add a format</a>
</div>

<!-- Modals -->
<div id="newFormat" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form method="post" name="newFormatForm" id="new-format-form" action="<?php echo __CMS_PATH__?>api/save.media.format"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="myModalLabel">Modal header</h3>
        </div>
        <div class="modal-body">
        	<div id="new-format-actual-form">
                <div class="form-inline">
                    <div class="control-group">
                        <label>Label</label>
                        <input type="text" id="label" name="attachment_label" class="input-large" placeholder="Format name" required />
                    </div>
                </div>
                <div class="form-inline">
                	<div class="control-group">
                        <label>Folder</label>
                        <input type="text" id="folder" name="attachment_folder" class="input-small" placeholder="Folder name" required />
                        <p>
                            <small class="muted">It's better to use a single letter folder, for shorter links to file</small>
                        </p>
                	</div>
                </div>
                <div class="form-inline">
                    <div class="control-group">
                        <label>Image sizes</label>
                        <div class="input-append">
                            <input type="text" name="attachment_width" id="width" placeholder="Width" class="input-mini" /> 
                            <span class="add-on">px</span>
                        </div>
                        X
                        <div class="input-append">
                            <input type="text" name="attachment_height" id="height" placeholder="Height" class="input-mini" /> 
                            <span class="add-on">px</span>
                        </div>       
                        <p>
                            <small class="muted">If you want the image to scale, just enter the fixed width OR height, if both sizes are entered the image will be cropped.</small>
                        </p>
                    </div>             
                </div>  
                <label class="checkbox">
                    <input type="checkbox" value="1" name="attachment_default" id="default">
                    Apply as a default                
                </label>
                <p>
                    <small class="muted">Thick this box if you want to set this size as a default format, which will be applied from now on in all the modules.</small>
                </p>
        	</div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" id="new-format-btn-save">Save changes</button>
        </div>
    </form>
</div>

<div id="modal-delete" class="modal hide fade">
    <div class="modal-header">
    	<a href="#" class="close">&times;</a>
    	<h3>Delete Format</h3>
    </div>
    <div class="modal-body">
    	<p>You are about to delete an image format, this procedure is irreversible.</p>
    	<p>Do you want to proceed?</p>
    </div>
    <div class="modal-footer">
      <a href="<?php echo __CMS_PATH__ ?>api/delete.media.format" class="btn btn-danger" btn-data-role="confirm-delete">Yes</a>
      <button class="btn" data-dismiss="modal" type="button">No</button>
    </div>
</div>