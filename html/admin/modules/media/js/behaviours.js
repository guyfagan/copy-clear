// JavaScript Document

$().ready(function(){
	interceptor();
});

function interceptor(){
	//there is no space for Mel Gibson here
	var $bodyAttrs = $("body").attr("class").split(" ");
	for(i = 0; i < $bodyAttrs.length; i++){		
		if(parseInt($bodyAttrs[i].indexOf("action-")) != -1){
			$action = $bodyAttrs[i];			
			$action = $action.replace("action-","");
		}
	}
	
	switch($action){
		case "settings":
			settings_actions();			
			break;
		
	}//end switch
}

function settings_actions(){
	
	$("a[data-btn-role=edit]").mediaFormatEditBtn();
	$("a[data-btn-role=delete]").mediaFormatDeleteBtn();		
	
	$("#media-settings").load_settings({module: $.config.module});
	
	$('#mediaRoles').tokenfield();
	
	$("#media-settings").submit(function(e){
		e.preventDefault();	
		var $url = $(this).attr("action");
		$.ajax({
			url: $url,
			type: "post",
			data: $(this).serialize(),
			dataType:"json",
			success: function(data){
				if(data.result == "success"){
					$("#media-settings").popMessage("The settings has been saved successfully",{type: "success"});
					$.cmsVars.formSaved = true;
				
				} else {
					$("#media-settings").popMessage("The settings has not been saved due to a server error",{type: "error"});
				}
			}	
		});
		$(window).scrollTop(0)
	});		
	
	$("#submit-form").click(function(e){
		e.preventDefault();
		var isValidated = $("#media-settings").validate();
		if(isValidated){
			$("#media-settings").submit();	
		} else {
			$("#media-settings").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
	
	$("#new-format-form").submit(function(e){
		e.preventDefault();	
		$form = $(this);
		$.ajax({
			url : $form.attr("action"),
			dataType:"json",
			type: "post",
			data: $form.serialize(),
			success: function(data){
				if(data.result == "success"){
					//console.log('create id '+data.id);
					$.get($.config.basepath+"admin/inc/components/media/media.format.view.php?id="+data.id+"&load_cms=1",function(html){						
						var $div = '<div class="settings-image-format" id="format-view-'+data.id+'">'+html+'</div>';
						//console.log($div);
						$('.settings-image-formats:last').append($div);
						$("#newFormat").modal('hide');					
					});
				} else {
					$("#new-format-actual-form").popMessage("Is not possible to write on the upload folder",{type: "error", when:'before', incipit: 'Error'});
				}//end if
			}
		});
	});
	
	$("#new-format-btn-save").click(function(e){
		e.preventDefault();
		var isValidated = $("#new-format-form").validate();
		if(isValidated){
			$("#new-format-form").submit();	
		} else {
			$("#new-format-actual-form").popMessage("Please fill the fields in red first!",{type: "error"});
		}
	});
	
	(function(){
		$.ajax({
			url : $.config.apipath + "get.media.formats",
			dataType:"json",
			type: "post",
			success: function(data){				
				if(data.result == "success"){	
					$("#folder").keyup(function(){
						var $fieldValue = $(this).val();
						var $trigger = $(this);
										
						var found = false;
						for(i = 0; i < data.formats.length; i++){
							if(data.formats[i].attachment_setting_folder == $fieldValue){
								found = true;
							}
						}//end for
						if(found == true){
							$trigger.popover({
								content: "<strong>Oooops!</strong> This folder is already in use by another image format",
								html: true	
							});
							$trigger.popover('show');
						} else {
							$trigger.popover('hide');
						}//end if	
					});
				}	
			}
		});
	})();
	
	
}

$.fn.mediaFormatEditBtn = function(){
	$(this).click(function(e){		
		e.preventDefault();		
		var $target = $(this).attr("data-target");		
		
		$.get($(this).attr("href"),function(data){
			$(".settings-image-format-view",$($target)).fadeOut("slow",function(){
				$(this).remove();
				$($target).append(data).hide().fadeIn("slow");	
			});
		});	
	});	
}	

$.fn.mediaFormatDeleteBtn = function(){
	$(this).click(function(e){		
		e.preventDefault();		
		var id = $(this).data('id');	
		var target = $(this).data('target');	
		$('#modal-delete').unbind('show');
		$('#modal-delete').on('show', function() {
			var id = $(this).data('id');	
			var target = $(this).data('target');				
			var $delete = $(this).find('.btn-danger');
			var $modal = $(this);
			$delete.attr('data-id',id);
			$delete.unbind('click');
			$delete.click(function(e){
				e.preventDefault();				
				$.ajax({
					url : $delete.attr("href"),
					type: "post",
					dataType:"json",
					data: 'as_id='+id,
					success: function(data){
						if(data.result == "success"){
							$('.settings-image-formats').parent().popMessage("The format has been deleted",{type: "success"});		
							$(target).fadeOut("slow",function(){
								$(this).remove();	
							});					
						} else {
							$('.settings-image-formats').parent().popMessage("Couldn't delete the format.",{type: "error"});	
						}
					}
				});
				$modal.modal('hide');
			});
			
		}).modal({ backdrop: true });
		
		$('#modal-delete').data('id', id).data('target',target).modal('show');
	});	
}	



