<?php
	$id = $_GET['id'];
	if(!is_null($id) && is_numeric($id)){
		$booking_data = ${$module}->get_booking(array('id' => $id));	
		if($booking_data !== false){	
?>
<div class="booking-view clearfix">
	<h2>Booking ID #<?php echo $id ?></h2>
    <hr />
    <h4>Booking Details</h4>
    <p>
        <strong>Date:</strong> <?php echo date('d/m/Y H:i:s',$booking_data['booking_date']); ?><br />        
        <strong>Name:</strong> <?php echo $booking_data['booking_firstname'].' '.$booking_data['booking_surname'] ?><br />
        <strong>Email:</strong> <a href="mailto:<?php echo $booking_data['booking_email'] ?>"><?php echo $booking_data['booking_email']; ?></a><br />
        <strong>Code:</strong> <?php echo $booking_data['booking_code'] ?><br />
        <strong>Quantity:</strong> <?php echo $booking_data['booking_quantity'] ?><br />
        <strong>Price:</strong> &euro;<?php echo number_format($booking_data['booking_price'],2) ?><br />  
        <?php
		if($booking_data['booking_rate_label'] != ""){
			echo '<strong>Booking Rate:</strong> '.$booking_data['booking_rate_label'].'<br />';
		}//end if
		?>
        <strong>Total paid:</strong> &euro;<?php echo number_format($booking_data['booking_amount'],2) ?><br />  
        <strong>Status:</strong> <?php 
            switch($booking_data['booking_status']){
                case "denied":
                    echo '<span class="label label-important">'.ucwords($booking_data['booking_status']).'</span>';
                    break;
                case "pending":
                    echo '<span class="label label-warning">'.ucwords($booking_data['booking_status']).'</span>';
                    break;
                default:	
                    echo '<span class="label label-success">'.ucwords($booking_data['booking_status']).'</span>';
            }
            ?>       
    </p>
    <?php
	if($booking_data['booking_post_id'] > 0){
	?>
    <hr />
    <h4>Related Post</h4>
    <p>
    	<strong>Title:</strong> <?php echo $booking_data['post_title'] ?> <a href="<?php echo __MODULE_PATH__ ?>/view/event/<?php echo $booking_data['booking_post_id'] ?>" class="btn btn-mini">Event's Bookings</a><br />
        <strong>Held on:</strong> <?php echo date("d/m/Y",$booking_data['post_date']) ?><br />
    </p>
    <?php	
	}//end if
	//check if we have a payment
	if($booking_data['booking_payment_id'] > 0){
		define("__PAYMENT_ID__",$booking_data['booking_payment_id']);
		require_once("modules/payments/snippets/view.php");
	}//end if
	?>
</div>    
<?php		
		} else {
?>
	<div class="alert alert-error"><strong>Error:</strong> No booking found with the passed ID</div>
<?php			
		}//end if
	} else {
?>
	<div class="alert alert-error"><strong>Error:</strong> No booking ID passed</div>
<?php
	}//end if	
?>