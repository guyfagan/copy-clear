<?php	
	$list = ${$module}->get_booking_rates();
?>

<table class="table table-striped table-hover <?php echo $table_type ?>">
    <thead>
        <tr>
        	<th>Rate</th>
            <th>Meta Field</th>
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tbody>
	<?php
    if($list !== false){
        for($i = 0; $i < sizeof($list); $i++){
			$id = $list[$i]['booking_rate_id'];
			$label = $list[$i]['booking_rate_label'];
			$is_category = false;
			
			$link_edit = __MODULE_PATH__."/rate/new/?id=".$id;
			$link_delete = __CMS_PATH__."/api/delete.booking.rate?id=".$id."&module=".$module;
    ?>
    	<tr>
        	<td><?php echo '<span id="label-'.$id.'">'.$label.'</span>' ?></td>
            <td><?php echo $list[$i]['booking_rate_meta_field'] ?></td>
            <td class="table-row-options">
            	<div class="btn-group pull-right table-button-cta">
                	<a href="<?php echo $link_edit ?>" class="btn"><i class="icon-edit"></i></a>
            		<a href="<?php echo $link_delete ?>" data-action="delete" data-ref-id="<?php echo $id ?>" class="btn btn-warning"><i class="icon-trash"></i></a>
                </div>
            </td>
        </tr>
    <?php        
        }//end for i
	} else {
	?>
    	<tr>
        	<td colspan="3"><i class="icon-warning-sign"></i> Nothing found</td>
        </tr>
    <?php
    }//end if
    ?>
    </tbody>
</table>
<div class="buttons-bar">
    <div class="btn-group">    	
        <a href="<?php echo __MODULE_PATH__.'/rate/new/' ?>" class="btn btn-primary">New Rate</a>       
    </div>       	                               
</div>
<div class="modal hide fade" id="delete-item-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Confirm Delete</h3>
    </div>
    <div class="modal-body">
       
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id="dismiss-delete" data-dismiss="modal">No, forget it</a>
        <a href="#" class="btn btn-primary" id="confirm-delete">Yes delete it</a>
    </div>
</div>