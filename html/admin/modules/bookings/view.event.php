<?php
	$id = $_GET['id'];
	if(!is_null($id) && is_numeric($id)){		
		$event = ${$module}->get_post(array('id' => $id,'meta' => true));			
		if($event !== false){			
			$booked_seats = ${$module}->get_booked_seats(array('post_id' => $id));
			$max_seats = ${$module}->get_max_bookable_seats(array('data' => $event));
			
?>
<div class="event-view clearfix">
	<h2>Bookings for &quot;<?php echo $event['post_title'] ?>&quot; </h2>
    <a href="<?php echo __CMS_PATH__.$event['post_module_id'].'/edit/'.$id ?>" class="btn btn-mini">Edit Event</a>
    <hr />
    <p>
    	The event will be held on <strong><?php echo date("jS F Y",$event['post_date']) ?></strong><br />
        <strong>Audience:</strong> <?php 
		if($max_seats !== false && !is_null($max_seats)){
			echo $booked_seats."/".$max_seats;
			if($booked_seats >= $max_seats){
				echo ' <span class="label label-important">Sold Out</span>';
			}//end if
		} else {
			echo $booked_seats.' persons are attending this event';
		}//end if
		?>
    </p>
    <?php
		define("__POST_ID__",$id);
		require_once('snippets/bookings.php');
	?>
</div>
<?php
		}//end if
	}//end if
?>    