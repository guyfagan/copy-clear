<?php
	$bookings = $this->utils->call('bookings');
	$list_bookings = $bookings->get_bookings(array('limit' => 3));
	if($list_bookings !== false){
?>
<style>
	.title-no-pad{
		margin-bottom: 0 !important;	
	}
</style>
<h3 class="title-no-pad">Latest Bookings</h3>
<table class="table table-stiped table-hover widget-bookings">
	<thead>
    	<tr>
        	<th>Person</th>
            <th>Event</th>
            <th><span class="pull-right">Quantity</span></th>
        </tr>
    </thead>
    <tbody>
<?php
		for($i = 0; $i < sizeof($list_bookings); $i++){
?>
	<tr>
    	<td><i class="icon-ticket"></i> <a href="<?php echo __CMS_PATH__ ?>bookings/view/<?php echo $list_bookings[$i]['booking_id'] ?>"><?php echo $list_bookings[$i]['booking_firstname']." ".$list_bookings[$i]['booking_surname']; ?></a></td>
        <td><?php echo $list_bookings[$i]['post_title'] ?></td>
        <td><span class="pull-right"><?php echo $list_bookings[$i]['booking_quantity']; ?></span></td>
    </tr> 
<?php			
		}//end for i
?>
	</tbody>
</table>
<?php
	}//end if
?>