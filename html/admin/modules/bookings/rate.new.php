<?php
	$id = $_REQUEST['id'];	
	$edit_mode = false;
	$page_title = "New Rate";
	if(!is_null($id)){
		$edit_mode = true;
		$page_title = "Edit Rate";
		$rate_data = ${$module}->get_booking_rate(array('id' => $id));
	}//end if
?>
<div id="form-wrapper" class="clearfix">
    <h4><?php echo $page_title ?></h4>    
    <form action="<?php echo __CMS_PATH__ ?>api/save.booking.rate?module=bookings" method="post" name="save-rate" id="save-rate" enctype="multipart/form-data" class="clearfix">
    	<?php		
		if($edit_mode){
		?>
        <input type="hidden" name="id" name="id" value="<?php echo $id ?>" />
        <?php	
		}//end if
		?>
        <div class="control-group">
        	<label>Label</label>
            <input type="text" name="label" id="label" class="input-xxlarge" required <?php
			if($edit_mode){
				echo ' value="'.$rate_data['booking_rate_label'].'"';	
			}//end if
			?>
            />
        </div>
        <div class="control-group">
        	<label>Related Meta Field</label>
            <input type="text" name="meta_field" id="meta-field" class="input-xxlarge" required <?php
			if($edit_mode){
				echo ' value="'.$rate_data['booking_rate_meta_field'].'"';	
			}//end if
			?>
            />
        </div>
        <div class="clearfix">
	    	<button type="submit" id="save-and-close" class="btn btn-primary">Save &amp; Close</button>
        </div>
	</form>
</div>
<script>
	$(document).ready(function(){
		$(".permalink").handlePermalink({path: "", input: "#label", defaultExt: "",apiCall: "clean.meta.field", saveTo: "#meta-field"});	
	});
</script>