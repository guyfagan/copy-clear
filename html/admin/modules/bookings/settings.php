<?php
	$module_data = $utils->get_module($module);
	$settings = $utils->get_settings(array('module' => $module));
?>
<div class="page-view clearfix">
    <form action="<?php echo __CMS_PATH__ ?>api/save.settings?module=<?php echo $utils->view->module ?>" method="post" id="module-settings" name="settings">
        <h2><?php echo $module_data['module_label'] ?> Settings</h2>
        <div class="form-block">           
            <fieldset>
            	<legend>Variables & Security</legend>
              	<label>Field for price</label>
                <input type="text" name="booking_price_field" id="booking_price_field" class="input-regular" />
                <label>Field for the number of seats <small>(you can indicate multiple fields with the same scope)</small></label>
                <input type="text" name="booking_seats_field" id="booking_seats_field" class="input-xxlarge" value="<?php echo $settings['booking_seats_field'] ?>" data-enable-tokenfield="true" />
                <label>Security token template</label>
                <input type="text" name="booking_token_template" id="booking_token_template" class="input-xxlarge" />
                <label>Secret Key</label>
                <input type="text" name="secret_key" id="secret_key" class="input-xxlarge" />
            </fieldset>
        </div>     
        <div class="form-block">
        	<fieldset>
            	<legend>Notifications</legend>
                <label>Send notification emails with the following address</label>
                <input type="text" name="booking_no_reply" id="booking_no_reply" class="input-xxlarge" />
                <label>Once a new booking is generated submit an email to</label>
                <input type="text" name="booking_notification_email" id="booking_notification_email" class="input-xxlarge" />
                <label>Also Bcc the following addresses</label>
                <input type="text" name="booking_bcc_on_notification" id="booking_bcc_on_notification" class="input-xxlarge" />
                <label>Email type</label>
                <select name="booking_email_type" id="booking_email_type">
                	<option value="text">Plain Text</option>
                    <option value="html">Html</option>
                </select>
                <?php
				if((bool)$utils->view->settings['enableEmailTemplates']){
					require_once('inc/components/templates/templates.email.settings.php');
				}//end if
				?>
                <label>Booking email subject</label>
                <input type="text" name="booking_email_subject" id="booking_email_subject" class="input-xxlarge" />
                <label>Booking email signature</label>
                <textarea data-ckeditor="true" data-ckeditor-height="100" id="booking_email_signature" name="booking_email_signature">
                <?php
				if(isset($settings['booking_email_signature'])){
					echo $settings['booking_email_signature'];	
				}//end if
				?>
                </textarea>
            </fieldset>
        </div>
        <div class="form-block">
        	<fieldset>
            	<legend>Accounts</legend>
                <label class="checkbox">
                	<input type="checkbox" value="1" name="booking_enable_accounts" id="enable-accounts"> Enable to create new accounts from bookings
                </label>
                <label>New accounts go to the following users group</label>
                <select name="booking_accounts_users_group" id="accounts-users-group">
               	<?php
				$users = $utils->call('users');
				$users_groups = $users->get_groups();
				if($users_groups !== false){
					for($i = 0; $i < sizeof($users_groups); $i++){
						echo '<option value="'.$users_groups[$i]['users_group_id'].'">'.$users_groups[$i]['users_group_name'].'</option>';
					}//end for i
				}//end if
				?>
                </select>
            </fieldset>
        </div>
        <div class="form-block">
        	<fieldset>
            	<legend>Bookings List View</legend>
                <input type="hidden" name="list_system" id="list_system" value="date" />
                <label>
                	General Sort Order
                    <select id="list_order" name="list_order">
                    	<option value="DESC">Newer first</option>
                        <option value="ASC">Older first</option>
                    </select>
                </label>                
                <br />
                <label>
                	List view type
                    <select id="list_view_type" name="list_view_type">
                    	<option value="large">Expanded (shows thumbnails, author and date)</option>
                        <option value="small">Compressed (show minimal information about the post)</option>
                    </select>
                </label>                
                <br />
                <label>
                	Elements per page
                    <select id="items_per_page" name="items_per_page">
                    	<option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="0">Don't paginate and show everything</option>
                    </select>
                </label>                
        	</fieldset>
        </div>       
        <button type="submit" class="btn btn-primary">Save Settings</button>  
    </form>
</div>
<script>
	$().ready(function(){
		$('button[data-btn-action=toggle-formats]').click(function(e){
			if($(this).hasClass('active')){
				$(this).removeClass('btn-success');	
				$(this).text("Disabled");			
			} else {				
				$(this).addClass('btn-success');	
				$(this).text("Enabled");
			}
		
			$selectedIds = new Array();
			
			$('button[data-btn-action=toggle-formats]').each(function(){
				if($(this).hasClass("btn-success")){
					$selectedIds.push($(this).attr("data-format-id"));
				}
			});
			
			console.log($selectedIds);
			$("#module-default-formats").val($selectedIds);
			
		});	
	});
</script>