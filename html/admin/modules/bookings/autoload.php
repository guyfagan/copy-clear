<?php
	$js_libs_autoload = array();
	$js_libs_autoload['settings'] = array(
									"js/cms.formdata.jquery.js",
									"inc/ckeditor/".__CKEDITOR_VERSION__."/ckeditor.js",
									"inc/ckeditor/".__CKEDITOR_VERSION__."/adapters/jquery.js",
									array(
										"type" => "css", 
										"path" => "inc/tokenfield/".__BOOTSTRAP_TOKENFIELD__."/bootstrap-tokenfield.css"
									),	
									"inc/tokenfield/".__BOOTSTRAP_TOKENFIELD__."/bootstrap-tokenfield.js"								
									);
	$js_libs_autoload['rate.new'] = array(									
									"js/cms.permalink.jquery.js"
									);
?>