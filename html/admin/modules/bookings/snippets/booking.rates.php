<?php	
	$bookings = $utils->call('bookings');
	$list = $bookings->get_booking_rates();
	if($list !== false){
		if(!defined("__BOOKING_RATES_LABEL__")){
			define("__BOOKING_RATES_LABEL__","Booking Rates");	
		}//end if
?>
<fieldset>
	<legend><?php echo __BOOKING_RATES_LABEL__ ?></legend>
   	<?php
	for($i = 0; $i < sizeof($list); $i++){
	?>
    	<label><?php echo $list[$i]['booking_rate_label'] ?></label>
        <input type="text" name="meta_<?php echo $list[$i]['booking_rate_meta_field'] ?>" id="<?php echo $list[$i]['booking_rate_meta_field'] ?>" class="input-medium" />
    <?php
	}//end for i
	?> 
</fieldset>
<?php
	}//end if
?>