<?php
	$params = array();	
	
	$list_system = 'natural';	
	if(defined('__POST_ID__')){
		$params['post_id'] = __POST_ID__;	
	}//end if	
		
	$view_mode = "default";	
	$table_type = "table-large-list";
	$list = ${$module}->get_bookings($params);			
?>
<table class="table table-striped table-hover <?php echo $table_type ?>">
    <thead>
        <tr>
            <th>Booking</th>         
            <th>Post</th>
            <th>Status</th>
            <td>&nbsp;</td>
        </tr>
    </thead>
    <tbody>
        <?php
        if($list !== false){
            for($i = 0; $i < sizeof($list); $i++){               
                $id = $list[$i]['booking_id'];					
                //Vars in case of Post   
                $link_view = __CMS_PATH__.'bookings/view/'.$id;			
                $label = $list[$i]['booking_firstname']." ".$list[$i]['booking_surname'];
                $icon = "icon-file";               
        ?>
        <tr class="table-row">            	
            <td>                       
                <i class="<?php echo $icon ?>"></i> <?php 
                echo '<span id="label-'.$id.'">'.$label;                  
                echo '</span>'; 
                
                //show the post date
                echo '<span class="table-info-line muted"><i class="icon-calendar"></i> '.date('d/m/Y H:i:s',$list[$i]['booking_date']).'</span>';
                //show the user
                echo '<span class="table-info-line muted"><i class="icon-ticket"></i> quantity: <strong>'.$list[$i]['booking_quantity'].'</strong>';
				if($list[$i]['booking_rate_label'] != ""){
					echo ' - Rate: <strong>'.$list[$i]['booking_rate_label'].'</strong>';	
				}//end if
				echo '</span>';
                
                ?>
            </td> 
            <td><?php echo $list[$i]['post_title'] ?></td>
            <td class="table-row-status"><?php 
            switch($list[$i]['booking_status']){
                    case "denied":
                        echo '<span class="label label-important">'.ucwords($list[$i]['booking_status']).'</span>';
                        break;
                    case "pending":
                        echo '<span class="label label-warning">'.ucwords($list[$i]['booking_status']).'</span>';
                        break;
                    default:	
                        echo '<span class="label label-success">'.ucwords($list[$i]['booking_status']).'</span>';
                }//end switch
            ?></td>
            <td class="table-row-options">              
                <div class="btn-group pull-right table-button-cta">
                    <a href="<?php echo $link_view ?>" class="btn"><i class="icon-eye-open"></i></a>
                    <?php
                    if($list[$i]['booking_payment_id'] > 0){
                        echo '<a href="'.__CMS_PATH__.'payments/view/'.$list[$i]['booking_payment_id'].'" class="btn"><i class="icon-credit-card"></i></a>';
                    }//end if                   
                    ?>
                </div>            	
            </td>
        </tr>
        <?php		
            }//end for i
        }//end if
        ?>
    </tbody>
</table>  
