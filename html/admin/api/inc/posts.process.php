<?php
	if($method == "save.post"){
		$id = $_REQUEST['id'];
		$category_id = $_REQUEST['category_id'];
		$data = array();
		$params = array();
		if(!isset($_REQUEST['revision'])){
			$params['revision'] = false;
		} else if(is_bool((bool)$_REQUEST['revision'])){
			$params['revision'] = (bool)$_REQUEST['revision'];
		}//end if
		
		if($perms[$module] > 1){
			if(${$module}->save_post($params)){
				$post_id = ${$module}->get_post_id();
				$data['result'] = 'success';	
				$data['id'] = $post_id;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the post';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.post"){
		//log event
		$logger->log_event(array("login_area" => "private","type" => "api.get.post","module_id" => $module,"post_id" => $_REQUEST['id']));	
		$data = array();
		if($perms[$module] > 0){
			$id = (int)$_REQUEST['id'];
			$meta = (bool)$_REQUEST['meta'];
			if(!is_null($id)){
				$result = ${$module}->get_post(array('id' => $id, 'meta' => $meta));
				if($result !== false){
					$data['result'] = "success";
					$result['post_date'] = date("d/m/Y",$result['post_date']);
					$data['post'] = $result;
				} else {
					$data['result'] = 'error';	
					$data['error'] = 'Nothing found for the sent ID';
				}
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
		
	if($method == "update.post.status"){
		//log event
		$logger->log_event(array("login_area" => "private","type" => "api.update.post.status","module_id" => $module,"post_id" => $_REQUEST['id']));	
		$data = array();
		if($perms[$module] > 0){
			$value = $_REQUEST['value'];	
			//Posts Class
			$posts = $utils->call("ewrite",array('module' => $module)); 	
			$underscore = strpos($_REQUEST['id'],"_");		
			$newid = substr($_REQUEST['id'],7,strlen($_REQUEST['id']));					
			$result = $posts->update_post_field(array(
				'post_id' => $newid,
				'field' => 'post_status',
				'value' => $value
			));	
			$data = array();
			if($result === true){
				$data['result'] = "pass";
				$data['value'] = $value;
			} else {
				$data['result'] = "error";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if	
	
	if($method == "update.post.field"){
		//log event
		$logger->log_event(array("login_area" => "private","type" => "api.update.post.field","module_id" => $module,"post_id" => $_REQUEST['id']));	
		$data = array();
		if($perms[$module] > 0){
			$value = $_REQUEST['value'];
			$field = $_REQUEST['field'];
			$id = $_REQUEST['id'];	
			//Posts Class
			$posts = $utils->call("ewrite",array('module' => $module)); 						
			$result = $posts->update_post_field(array(
				'post_id' => $id,
				'field' => $field,
				'value' => $value
			));	
			$data = array();
			if($result === true){
				$data['result'] = "success";
				$data['value'] = $value;
			} else {
				$data['result'] = "error";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if	
	
	if($method == "delete.post"){
		$id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);				
		$data = array();
		if(((!is_array($id) && $perms[$module] == 3 && ${$module}->is_post_owner($users->get_uid(),$id))) || $perms[$module] == 7){
			if(${$module}->delete_post($id)){			
				$data['result'] = 'success';
				$data['id'] = $id;
			} else {
				$data['result'] = 'error';
				$data['error'] = "No post to delete found";
			}//end if
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permissions";
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "delete.posts"){
		$ids = $_POST['ids'];
		
		$data = array();
		if(is_array($ids) && $perms[$module] >= 3){
			if(${$module}->delete_post($ids)){			
				$data['result'] = 'success';
				$data['ids'] = $ids;			
			} else {
				$data['result'] = 'error';
				$data['error'] = "No post to delete found";
			}//end if
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permissions";
		}//end if
		echo json_encode($data);
		
	}//end if
	
	if($method == "create.post.relation"){
		$id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);	
		$category_id = filter_var($_POST['category_id'], FILTER_SANITIZE_NUMBER_INT);	
		if(is_null($category_id)){
			$category_id  = 0;	
		}//end if
		$options = array();
		$options['type'] = 'post';
		$options['id'] = $id;
		$options['parent_id'] = $category_id;	
		$options['module_id'] = $module;	
		
		$data = array();
		if(((!is_array($id) && $perms[$module] == 3 && ${$module}->is_post_owner($users->get_uid(),$id))) || $perms[$module] == 7){
			if(${$module}->update_module_relationship($options)){			
				$data['result'] = 'success';
				$data['id'] = $id;
			} else {
				$data['result'] = 'error';
				$data['error'] = "Can't add a new relationship";
			}//end if
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permissions";
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "delete.post.relation"){
		$id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);	
		$category_id = filter_var($_POST['category_id'], FILTER_SANITIZE_NUMBER_INT);	
		if(is_null($category_id)){
			$category_id  = 0;	
		}//end if
		$options = array();
		$options['type'] = 'post';
		$options['id'] = $id;
		$options['parent_id'] = $category_id;		
		
		$data = array();
		if(((!is_array($id) && $perms[$module] == 3 && ${$module}->is_post_owner($users->get_uid(),$id))) || $perms[$module] == 7){
			if(${$module}->delete_item_relation($options)){			
				$data['result'] = 'success';
				$data['id'] = $id;
			} else {
				$data['result'] = 'error';
				$data['error'] = "No post relationship to delete found";
			}//end if
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permissions";
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "post.check.permalink"){
		$seo = $utils->call("seo");
		$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);		
		$permalink = filter_var($_REQUEST['permalink'], FILTER_SANITIZE_STRING);			
		$data = array();
		$params = array("permalink" => $permalink, "type" => "post");
		if(is_numeric($id) && !is_null($id)){
			$params['id'] = $id;
		}//end if
		if($permalink !== false){
			$result = $seo->return_valid_permalink($params);
			if($result !== false){
				$data['result'] = "success";
				$data['permalink'] = $result;
			} else {
				$data['result'] = 'error';
				$data['error'] = "Can't validate the permalink";
			}
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permalink passed";
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == 'category.posts.force.template'){			
		if($perms[$module] > 0){
			$parent_id = filter_var($_POST['parent_id'], FILTER_SANITIZE_NUMBER_INT);	
			$template = filter_var($_POST['template'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);	
			$force_all = (bool)$_POST['force_all'];
			$result = ${$module}->force_category_posts_template(array("parent_id" => $parent_id,'template' => $template,'force_all' => $force_all));
			if($result !== false){
				$data['result'] = "success";				
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot set the passed template";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';	
		}//end if		
		echo json_encode($data); 
	}//end if
	
	##############
	# TAGS CALLS #
	##############
	
	if($method == "post.add.tags"){
		$tools = $utils->call("tools");
		$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);		
		$tags = filter_var($_REQUEST['tags'], FILTER_SANITIZE_STRING);	
		$data = array();
		$params = array("post_id" => $id, "tags" => $tags, "module_id" => $module);
		if($perms[$module] > 0){
			$result = $tools->add_tags($params);		
			if($result !== false){
				$data['result'] = "success";
				$data['tags'] = $result;
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot add the tags passed";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data); 
	}//end if
	
	if($method == "post.get.tags"){
		$tools = $utils->call("tools");
		$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);		
		$data = array();
		$params = array("post_id" => $id, "module_id" => $module);
		if($perms[$module] > 0){
			$result = $tools->get_tags($params);		
			if($result !== false){
				$data['result'] = "success";
				$data['tags'] = $result;
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot get the tags for the passed post";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data); 
	}//end if
	
	if($method == "post.delete.tag"){
		$tools = $utils->call("tools");
		$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);		
		$tag = filter_var($_REQUEST['tag'], FILTER_SANITIZE_STRING);	
		$data = array();
		$params = array("post_id" => $id, "tag" => $tag, "module_id" => $module);
		if($perms[$module] > 0){
			$result = $tools->delete_tag($params);		
			if($result !== false){
				$data['result'] = "success";				
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot delete the tags passed";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data); 
	}//end if
	
	if($method == "rebuild.items.indexes"){
		if($perms[$module] > 0){
			$parent_id = filter_var($_GET['parent_id'], FILTER_SANITIZE_NUMBER_INT);	
			$result = ${$module}->reset_module_sorting(array("parent_id" => $parent_id));
			if($result !== false){
				$data['result'] = "success";				
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot delete the tags passed";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';	
		}//end if		
		echo json_encode($data); 
	}//end if
		
	if($method == "items.save.sorting"){
		if($perms[$module] > 0){
			$ids = $_POST['table-row-id'];
			$types = $_POST['table-row-type'];
			$page = filter_var($_POST['page'], FILTER_SANITIZE_NUMBER_INT);
			$total_items = filter_var($_POST['total-items'], FILTER_SANITIZE_NUMBER_INT);
			$items_per_page = filter_var($_POST['items-per-page'], FILTER_SANITIZE_NUMBER_INT);	
			$parent_id = filter_var($_POST['parent-id'], FILTER_SANITIZE_NUMBER_INT);	
			
			$params = array(
				'ids' => $ids,
				'types' => $types,
				'page' => $page,
				'total_items' => $total_items,
				'items_per_page' => $items_per_page,
				'parent_id' => $parent_id
			);
			$data = array();
			$result = ${$module}->save_module_sorting($params);
			
			if($result !== false){
				$data['result'] = "success";				
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot save this sorting";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data); 
	}//end if
	
	if($method == "explore"){
		$data = array();
		if(isset($module)){
			$options = array('type' => 'categories');
			if(isset($_REQUEST['status'])){
				$options['status'] = (int)$_REQUEST['status'];	
			}//end if
			if(isset($_REQUEST['parent_id'])){
				$options['category_id'] = (int)$_REQUEST['parent_id'];	
			}//end if
			if(isset($_REQUEST['module'])){
				$options['module'] = $_REQUEST['module'];	
			}//end if
			$result = ${$module}->get_items($options);
			$data['parent'] = (int)$_REQUEST['parent_id'];
		} else {
			$result = $utils->get_modules();
			$visible_results = array();
			if($result !== false){
				for($i = 0; $i < sizeof($result); $i++){
					if($perms[$result[$i]['module_name']] > 0){
						array_push($visible_results, $result[$i]);
					}
				}//end for i
			}//end if
			$result = $visible_results;
		}//end if
		
		$data['result'] = 'success';	
		$data['data'] = $result;
		
		echo json_encode($data);
	}//end if
?>