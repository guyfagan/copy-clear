<?php
	if($method == "save.booking.rate"){
		$id = $_REQUEST['id'];		
		$data = array();
		
		$bookings = $utils->call('bookings');
		
		if($perms[$module] > 1){
			if(${$module}->save_booking_rate()){
				$rate_id = ${$module}->get_booking_rate_id();
				$data['result'] = 'success';	
				$data['id'] = $rate_id;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the booking rate';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "delete.booking.rate"){
		$id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);		
		$bookings = $utils->call('bookings');
				
		$data = array();
		if($perms['bookings'] >= 3){
			if($bookings->delete_booking_rate(array('id' => $id))){			
				$data['result'] = 'success';
				$data['id'] = $id;
			} else {
				$data['result'] = 'error';
				$data['error'] = "No booking rate to delete found";
			}//end if
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permissions";
		}//end if
		echo json_encode($data);
	}//end if
?>