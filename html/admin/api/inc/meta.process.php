<?php
	if($method == 'update.meta.field'){
		$data = array();
		if($perms[$module] > 1){
			$label = filter_var($_POST['label'], FILTER_SANITIZE_STRING);
			$value = trim($_POST['value']);
			$type = filter_var($_POST['type'], FILTER_SANITIZE_STRING);	
			$id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);	
			
			$meta = $utils->call("meta");
			$meta->set_meta_module(array('module' => $module));
			$meta->set_meta_type($type);
			$meta->set_meta_id($id);
			$result = $meta->add_meta(array('label' => $label, 'value' => $value));	
			if($result){
				$data['result'] = 'success';		
			} else {
				$data['result'] = 'error';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
?>