<?php
	if($method == "save.module"){
		$id = $_REQUEST['id'];			
		$data = array();
		
		if($perms['users'] > 1){
			$params = array();
			$params['module_name'] = $_POST['name'];
			$params['module_label'] = $_POST['label'];
			$params['module_table'] = $_POST['table'];
			$params['group_id'] = $_POST['group_id'];
			$result = $utils->save_module($params);
			if($result !== false){				
				$data['result'] = 'success';	
				$data['data'] = $result;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the user';
			}//end if*/
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "save.module.group"){
		$id = $_REQUEST['id'];			
		$data = array();
		
		if($perms['users'] > 1){
			$params = array();
			$params['name'] = strtolower($_POST['name']);			
			$params['hidden'] = $_POST['hidden'];
			$result = $utils->save_modules_group($params);
			if($result !== false){				
				$data['result'] = 'success';	
				$data['data'] = $result;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the user';
			}//end if*/
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "modules.group.save.sorting"){			
		$data = array();		
		if($perms['users'] > 1){
			$params = array();		
			$params['group_id'] = $_REQUEST['group_id'];	
			$params['ids'] = $_POST['table_row_id'];
			$result = $utils->save_modules_group_sorting($params);
			if($result !== false){				
				$data['result'] = 'success';	
				$data['data'] = $result;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the sorting for this group';
			}//end if*/
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "clean.link"){
		$seo = $utils->call("seo");		
		$permalink = filter_var($_REQUEST['permalink'], FILTER_SANITIZE_STRING);
		$result = $seo->simple_clean_link(array('permalink' => $permalink));
		$data = array();
		$data['result'] = "success";
		$data['permalink'] = $result;
		echo json_encode($data);
	}//end if
	
	if($method == "clean.meta.field"){
		$seo = $utils->call("seo");		
		$permalink = filter_var($_REQUEST['permalink'], FILTER_SANITIZE_STRING);
		$result = $seo->simple_clean_link(array('permalink' => $permalink,'type' => 'meta'));
		$data = array();
		$data['result'] = "success";
		$data['permalink'] = $result;
		echo json_encode($data);
	}//end if
	
	if($method == 'get.mail.details'){
		$id = $_POST['id'];
		$data = array();
		if($perms['users'] > 1){
			$result = $utils->get_logged_email(array('id' => $id));
			if($result !== false){
				$data['result'] = 'success';
				$msg = unserialize($result['mail_log_text']);
				$data['message'] = $msg;	
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Unknown error';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == 'get.mail.html'){
		$id = $_GET['id'];
		$data = array();
		if($perms['users'] > 1){
			$result = $utils->get_logged_email(array('id' => $id));
			if($result !== false){				
				$msg = unserialize($result['mail_log_text']);
				echo $msg;	
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Unknown error';
				echo json_encode($data);
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
			echo json_encode($data);
		}//end if
		
	}//end if
	
	if($method == "empty.system.logs"){
		$data = array();
		if($perms['users'] > 1){
			$empty = $logger->empty_logs();
			if($empty){
				$data['result'] = 'success';	
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Unknown error';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "empty.blacklist.logs"){
		$data = array();
		if($perms['users'] > 1){
			$empty = $logger->empty_blacklist();
			if($empty){
				$data['result'] = 'success';	
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Unknown error';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
		
	if($method == "empty.mail.logs"){
		$data = array();
		if($perms['users'] > 1){
			$empty = $utils->delete_logged_emails();
			if($empty){
				$data['result'] = 'success';	
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Unknown error';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.mail.logs"){
		$data = array();
		if($perms['users'] > 1){
			$utils->create_paging($_GET['page'],25);
			$logs = $utils->get_logged_emails();
			
			# PAGINATION #
			
			$paging_stats = $utils->get_paging_stats();	
			$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
			//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
			$utils->unset_paging();
			$page_start = $paging_stats['start']+1;
			$paging_stats['list'] = $paging_list;
			$paging_stats['page_start'] = $page_start;
			$page_end = $paging_stats['start']+10;
			if($page_end > $paging_stats['founds']){
				$page_end = $paging_stats['founds'];
			}//end if
			$paging_stats['page_end'] = $page_end;
			$paging_stats['pages_shown'] = sizeof($paging_list);
			$page_total = $paging_stats['pages'];
			$page_current = $paging_stats['current_page'];
			$page_prev = $page_current-1;
			if($page_prev == "0"){
				$page_prev = 1;
			}//end if
			$page_next = $page_current+1;
			if($page_next > $page_total){
				$page_next = $page_total;
			}//end if
			
			# END PAGINATION #
					
			if($logs !== false){
				$data['result'] = 'success';
				for($i = 0; $i < sizeof($logs); $i++){
					$recipients = unserialize($logs[$i]['mail_log_recipient']);
					if(is_array($recipients)){
						$recipients = implode(", ",$recipients);
					} else {
						$recipients = $logs[$i]['mail_log_recipient'];
					}
					$logs[$i]['recipients'] = $recipients;
					$logs[$i]['mail_log_date'] = date('d/m/Y H:i',$logs[$i]['mail_log_date']);	
				}
				
				$data['data'] = $logs;
				$data['paging'] = $paging_stats;
			} else {
				$data['result'] = 'success';
				$data['data'] = '';
			}//end if
		
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		
		echo json_encode($data);
	}//end if
	
	if($method == "get.system.logs"){
		$data = array();
		if($perms['users'] > 1){
			$logger = $utils->call('logger');
			$utils->create_paging($_GET['page'],25);
			$logs = $logger->get_logs();
			
			# PAGINATION #
			
			$paging_stats = $utils->get_paging_stats();	
			$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
			$paging_stats['list'] = $paging_list;
			//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
			$utils->unset_paging();
			$page_start = (int)$paging_stats['start']+1;		
			$paging_stats['page_start'] = $page_start;
			$page_end = $paging_stats['start']+10;
			if($page_end > $paging_stats['founds']){
				$page_end = $paging_stats['founds'];
			}//end if
			$paging_stats['page_end'] = $page_end;
			$paging_stats['pages_shown'] = sizeof($paging_list);
			$page_total = $paging_stats['pages'];
			$page_current = $paging_stats['current_page'];
			$page_prev = $page_current-1;
			if($page_prev == "0"){
				$page_prev = 1;
			}//end if
			$page_next = $page_current+1;
			if($page_next > $page_total){
				$page_next = $page_total;
			}//end if
			
			# END PAGINATION #
			
			if($logs !== false){
				$data['result'] = 'success';
				for($i = 0; $i < sizeof($logs); $i++){				
					$logs[$i]['system_log_date'] = date('d/m/Y H:i',$logs[$i]['system_log_date']);	
					$user = $logs[$i]['user_username'];
					if($user == NULL){
						$user = "anonymous";	
					}//end if
					$logs[$i]['user'] = $user;
					unset($logs[$i]['system_log_message']);
				}//end for i
				$data['data'] = $logs;
				$data['paging'] = $paging_stats;
			} else {
				$data['result'] = 'success';
				$data['data'] = '';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		
		echo json_encode($data);
	}//end if
	
	if($method == "get.blacklist.logs"){
		$data = array();
		if($perms['users'] > 1){
			$logger = $utils->call('logger');
			$utils->create_paging($_GET['page'],25);
			$logs = $logger->get_blacklist();
			
			# PAGINATION #
			
			$paging_stats = $utils->get_paging_stats();	
			$paging_list = $utils->get_paging(array('range' => 10,'cms' => true));
			$paging_stats['list'] = $paging_list;
			//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
			$utils->unset_paging();
			$page_start = (int)$paging_stats['start']+1;		
			$paging_stats['page_start'] = $page_start;
			$page_end = $paging_stats['start']+10;
			if($page_end > $paging_stats['founds']){
				$page_end = $paging_stats['founds'];
			}//end if
			$paging_stats['page_end'] = $page_end;
			$paging_stats['pages_shown'] = sizeof($paging_list);
			$page_total = $paging_stats['pages'];
			$page_current = $paging_stats['current_page'];
			$page_prev = $page_current-1;
			if($page_prev == "0"){
				$page_prev = 1;
			}//end if
			$page_next = $page_current+1;
			if($page_next > $page_total){
				$page_next = $page_total;
			}//end if
			
			# END PAGINATION #			
					
			if($logs !== false){
				$data['result'] = 'success';
				for($i = 0; $i < sizeof($logs); $i++){				
					$logs[$i]['blacklist_date'] = date('d/m/Y H:i',$logs[$i]['blacklist_date']);	
				}//end for i
				$data['data'] = $logs;
				$data['paging'] = $paging_stats;
			} else {
				$data['result'] = 'success';
				$data['data'] = '';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		
		echo json_encode($data);
	}//end if
	
	if($method == "test.mandrill"){
		$data = array();		
		if($perms['users'] > 1){
			$key = $_POST['key'];
			if(is_null($key)){
				$settings = $utils->get_settings();		
				$key = $settings['mandrillApiKey'];	
			}//end if
				
			if(isset($key)){
				require_once('../inc/mandrill/'.__MANDRILL_VERSION__.'/Mandrill.php');
				try {
					$mandrill = new Mandrill($key);
					$result = $mandrill->users->info();
					//print_r($result);
					$data['result'] = 'success';					
    			} catch(Mandrill_Error $e) {
					// Mandrill errors are thrown as exceptions
					$data['result'] = 'error';	
					$data['error'] = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();					
					// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
					//throw $e;
				}
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'No API Key is set';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "test.remote.db"){
		$data = array();		
		if($perms['users'] > 1){
			$host = $_POST['remote_host'];
			$user = $_POST['remote_user'];
			$pass = $_POST['remote_password'];
			$dbname = $_POST['remote_db'];
			try{
				$db = new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8', $user, $pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT, PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
				$data['result'] = 'success';
			} catch(PDOException $ex){
				$errors = $ex->getMessage();
				$data['result'] = 'error';
				$data['errors'] = $errors;
			}//end try			
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "import.db"){
		$data = array();		
		if($perms['users'] > 1){
			$import = $utils->call('import');
			$result = $import->import_db();
			if($result){
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';	
				$data['error'] = $import->errors;
			}
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "update.db"){
		$data = array();	
		if($perms['users'] > 3){
			$tools = $utils->call('tools');
			$result = $tools->update_database(array('file_log' => '../modules/main/updates.log', 'skip_errors' => true));
			if($result){
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';	
				$data['errors'] = $tools->errors;
			}
		} else {
			$data['result'] = 'error';	
			$data['errors'] = array('No permission');
		}//end if
		echo json_encode($data);
	}//end if
?>