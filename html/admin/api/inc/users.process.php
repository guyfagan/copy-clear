<?php
	if($method == "save.user"){
		$id = $_REQUEST['id'];			
		$data = array();
		
		if($perms[$module] > 1){
			if($users->new_user()){
				$user_id = $users->uid;
				$data['result'] = 'success';	
				$data['id'] = $user_id;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the user';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.user"){
		$data = array();
		if($perms[$module] > 0){
			$id = (int)$_REQUEST['id'];
			$meta = (bool)$_REQUEST['meta'];
			if(!is_null($id)){
				$result = $users->get_user(array('id' => $id, 'meta' => $meta));
				if($result !== false){
					$data['result'] = "success";							
					$data['user'] = $result;
				} else {
					$data['result'] = 'error';	
					$data['error'] = 'Nothing found for the sent ID';
				}
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "delete.user"){
		$data = array();
		if($perms[$module] > 0){
			$id = (int)$_REQUEST['id'];
			if(!is_null($id)){				
				$options = array('id' => $id);	
				//get the settings
				$settings = $utils->get_settings(array("module_id" =>$module));
				if(isset($settings['soft-delete']) && $settings['soft-delete'] == '1'){
					$options['real_delete'] = false;	
				} else {
					$options['real_delete'] = true;	
				}//end if
				$result = $users->delete_user($options);
				if($result !== false){
					$data['result'] = "success";
				} else {
					$data['result'] = 'error';	
					$data['error'] = 'Nothing found for the sent ID';
				}//end if
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "update.user.status"){
		$value = $_REQUEST['value'];
		$underscore = strpos($_REQUEST['id'],"_");		
		$newid = substr($_REQUEST['id'],7,strlen($_REQUEST['id']));	
		$result = $users->change_user_status(array('id' => $newid, 'status' => $value));
		$data = array();
		if($result === true){
			$data['result'] = "pass";
			$data['value'] = $value;
		} else {
			$data['result'] = "error";
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "save.users.group"){
		$id = $_REQUEST['id'];			
		$data = array();
		
		if($perms[$module] > 1){
			if($users->save_group()){
				$group_id = $users->gid;
				$data['result'] = 'success';	
				$data['id'] = $group_id;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the group';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.users.group"){
		$data = array();
		if($perms[$module] > 0){
			$id = (int)$_REQUEST['id'];
			if(!is_null($id)){
				$result = $users->get_group(array('id' => $id));
				if($result !== false){
					$data['result'] = "success";							
					$data['group'] = $result;
				} else {
					$data['result'] = 'error';	
					$data['error'] = 'Nothing found for the sent ID';
				}
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "delete.user.group"){
		$data = array();
		if($perms[$module] > 0){
			$id = (int)$_REQUEST['id'];
			if(!is_null($id)){				
				$options = array('id' => $id);	
				if($_GET['delete_users'] == 1){
					$options['delete_users'] = true;	
				}//end if
				$result = $users->delete_group($options);
				if($result !== false){
					$data['result'] = "success";
				} else {
					$data['result'] = 'error';	
					$data['error'] = 'Nothing found for the sent ID';
				}//end if
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.users.group.permissions"){
		$data = array();
		if($perms[$module] > 0){
			$id = (int)$_REQUEST['id'];
			if(!is_null($id)){
				$result = $users->get_group_permissions(array('id' => $id));
				if($result !== false){
					$data['result'] = "success";							
					$data['permissions'] = $result;
				} else {
					$data['result'] = 'error';	
					$data['error'] = 'Nothing found for the sent ID';
				}
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
?>