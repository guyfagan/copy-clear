<?php
	if($method == "save.category"){
		$id = $_REQUEST['id'];
		$category_id = $_REQUEST['category_id'];
		$data = array();
		
		if($perms[$module] > 1){
			if(${$module}->new_category()){
				$category_id = ${$module}->get_category_id();
				$data['result'] = 'success';	
				$data['id'] = $category_id;
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the category';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.category"){
		$data = array();
		if($perms[$module] > 0){
			$id = (int)$_REQUEST['id'];
			$meta = (bool)$_REQUEST['meta'];		
			if(!is_null($id)){
				$result = ${$module}->get_category(array('id' => $id,'meta' => $meta));
				if($result !== false){
					$data['result'] = "success";							
					$data['category'] = $result;
				} else {
					$data['result'] = 'error';	
					$data['error'] = 'Nothing found for the sent ID';
				}
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "delete.category"){
		$id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);				
		$data = array();
		if(!is_array($id) && $perms[$module] >= 3){
			if(${$module}->delete_category($id)){			
				$data['result'] = 'success';
				$data['id'] = $id;
			} else {
				$data['result'] = 'error';
				$data['error'] = "No category to delete found";
			}//end if
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permissions";
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "update.category.status"){
		$value = $_REQUEST['value'];	
		//Posts Class
		$posts = $utils->call("ewrite"); 	
		$underscore = strpos($_REQUEST['id'],"_");		
		$newid = substr($_REQUEST['id'],7,strlen($_REQUEST['id']));	
		$result = $posts->update_category_field(array(
			'category_id' => $newid,
			'field' => 'category_status',
			'value' => $value
		));
		$data = array();
		if($result === true){
			$data['result'] = "pass";
			$data['value'] = $value;
		} else {
			$data['result'] = "error";
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "category.check.permalink"){
		$seo = $utils->call("seo");
		$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);		
		$permalink = filter_var($_REQUEST['permalink'], FILTER_SANITIZE_STRING);			
		$data = array();
		$params = array("permalink" => $permalink, "type" => "category");
		if(is_numeric($id) && !is_null($id)){
			$params['id'] = $id;
		}
		if($permalink !== false){
			$result = $seo->return_valid_permalink($params);
			if($result !== false){
				$data['result'] = "success";
				$data['permalink'] = $result;
			} else {
				$data['result'] = 'error';
				$data['error'] = "Can't validate the permalink";
			}
		} else {
			$data['result'] = 'error';
			$data['error'] = "No permalink passed";
		}//end if
		echo json_encode($data);
	}//end if
?>