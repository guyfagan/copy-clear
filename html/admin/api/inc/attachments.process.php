<?php
	if($method == "save.media.format"){		
		$data = array();		
		if($perms['users'] > 3){
			$params = array();
			if($_POST['id'] != NULL){
				$params['id'] = (int)$_POST['id'];	
			}//end if
			$result = $utils->save_attachment_format($params);					
			if($result !== false){						
				$data['result'] = 'success';
				$data['id'] = $utils->candybox['attachment_setting_id'];			
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot save the format passed';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "delete.media.format"){
		$data = array();
		if($perms['users'] > 1){
			$id = filter_var($_REQUEST['as_id'], FILTER_SANITIZE_NUMBER_INT);
			$result = $utils->delete_attachment_format(array("id" => $id));
			if($result !== false){						
				$data['result'] = 'success';
				$data['data'] = $result;			
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot delete the media format passed';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "get.media.formats"){
		$data = array();		
		if($perms['media'] > 0){
			$params = array();
			if(!is_null($module)){
				$params['module'] = $module;	
			}//end if
			if($_REQUEST['default'] != NULL){
				$params['default'] = (bool)$_REQUEST['default'];	
			}//end if
			
			$result = $utils->get_attachments_formats($params);					
			if($result !== false){						
				$data['result'] = 'success';
				$data['formats'] = $result;			
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot retrieve media formats';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "create.attachment.format"){
		$data = array();
		if($perms[$module] > 0){
			$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);	
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);	
			$format_id = filter_var($_REQUEST['format_id'], FILTER_SANITIZE_NUMBER_INT);	
			$image = $utils->call("image");
			$params = array(
				"file_id" => $file_id,
				"format_id" => $format_id,
				"module" => $module,
				"post_id" => $id,
				"upload_path" => "../../upload/"
			);		
			
			if(isset($_REQUEST['crop']) && (bool)$_REQUEST['crop'] == true){
				$crop = array();
				$crop['scale'] = $_REQUEST['scaleRatio'];
				$crop['x1'] = $_REQUEST['x'];
				$crop['y1'] = $_REQUEST['y'];
				$crop['x2'] = $_REQUEST['x2'];
				$crop['y2'] = $_REQUEST['y2'];
				$crop['width'] = $_REQUEST['w'];
				$crop['height'] = $_REQUEST['h'];
				$image->set_crop($crop);
			}//end if
					
			$result = $image->create_new_size($params);
			if($result !== false){						
				$data['result'] = 'success';
				$data['data'] = $result;			
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot create the media format passed';
			}//end if			
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "resize.original"){
		$data = array();
		if($perms[$module] > 0){

			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);	
			$image = $utils->call("image");
			$params = array(
				"file_id" => $file_id,				
				"upload_path" => "../../upload/"
			);		
			$result = $image->check_if_resize_image_needed($params);
			if($result !== false){						
				$data['result'] = 'success';
				$data['data'] = $result;			
			} else {
				$data['result'] = 'success';	
				//There is no error as in this case there just no need to resie anything
				//$data['error'] = 'Cannot create the media format passed';
			}//end if			
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
		
	//retrieve the attachments related to a post
	if($method == "get.post.attachments"){
		$data = array();
		if($perms[$module] > 0){
			$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);	
			if(!is_null($id)){
				$params = array("post_id" => $id,"module" => $module);
				$type = $_REQUEST['type'];
				if(!is_null($type)){
					$params['type'] = $type;	
				}//end if
				$limit = $_REQUEST['limit'];
				if(!is_null($limit)){
					$params['limit'] = $limit;
				}//end if
				$filesize = (bool)$_REQUEST['filesize'];		
				if(!is_null($filesize)){
					$params['filesize'] = $filesize;
					$params['upload_path'] = "../../";
				}//end if	
				
				if(isset($_REQUEST['attachment_post_type'])){
					$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
				}//end if
				
				$result = $utils->get_attachments($params);
				$data['result'] = 'success';
				if($result != false){
					$data['data'] = $result;
				} else {
					$data['data'] = NULL;
				}//end if
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "set.post.attachment.type"){
		$data = array();
		if($perms[$module] > 0){
			//get the settings for media
			$mediaSettings = $utils->get_settings(array('module' => 'media'));
			
			$post_id = filter_var($_REQUEST['post_id'], FILTER_SANITIZE_NUMBER_INT);
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);
			$post_file_id = filter_var($_REQUEST['post_file_id'], FILTER_SANITIZE_NUMBER_INT);
			$role = filter_var($_REQUEST['role'], FILTER_SANITIZE_STRING); 
			$params = array();
			$params['post_id'] = $post_id;
			$params['file_id'] = $file_id;
			$params['post_file_id'] = $post_file_id;
			$params['field'] = 'role';
			$params['field_value'] = $role;
			$params['module'] = $module;
			if($role == "default-image"){
				$params['unique'] = true;
				$params['unique_default'] = 'generic';
				if(isset($mediaSettings['mediaDefaultRole']) && !is_null($mediaSettings['mediaDefaultRole'])){
					$params['unique_default'] = $mediaSettings['mediaDefaultRole'];	
				}//end if
			}//end if
			if(isset($_REQUEST['attachment_post_type'])){
				$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
			}//end if		
			$result = $utils->update_post_attachment($params);
			if($result !== false){
				$data['result'] = 'success';
				//now we get the attachment info back
				$params = array("file_id" => $file_id,"post_id" => $post_id,"module" => $module);
				if(isset($_REQUEST['attachment_post_type'])){
					$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
				}//end if
				$file = $utils->get_attachment($params);
				$data['attachment'] = $file;
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot update the database';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	//retrieve the attachments related to a post
	if($method == "get.attachments"){
		$data = array();
		if($perms['media'] > 0){
			$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);	
			if(!is_null($id)){
				$params = array();
				$type = $_REQUEST['type'];
				if(!is_null($type)){
					$params['type'] = $type;	
				}//end if
				$limit = $_REQUEST['limit'];
				if(!is_null($limit)){
					$params['limit'] = $limit;
				}//end if	
				$filesize = (bool)$_REQUEST['filesize'];
				if(!is_null($filesize)){
					$params['filesize'] = $filesize;
					$params['upload_path'] = "../../";
				}//end if
				$data['is_search'] = false;
				if(!is_null($_REQUEST['search'])){
					$params['search'] = urldecode($_REQUEST['search']);	
					$data['is_search'] = true;
				}//end if
				
				if(isset($_REQUEST['attachment_post_type'])){
					$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
				}//end if
				
				if(isset($_REQUEST['page'])){								
					$params['support_pagination'] = true;		
					$utils->create_paging($_REQUEST['page'],$_REQUEST['items_per_page']);										
				}//end if
				$params['check_isolation'] = true;
				$result = $utils->get_attachments($params);
				$data['result'] = 'success';
				if($result != false){
					$data['data'] = $result;
					$data['paging_stats'] = $utils->get_paging_stats();		
					$utils->unset_paging();
				} else {
					$data['data'] = NULL;
				}//end if
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'You did\'t specify and ID';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.attachment"){
		$data = array();
		if($perms[$module] > 0){
			$params = array();
			//compulsory field					
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);	
			$params['file_id'] = $file_id;
			//the post_id is not compulsory
			$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);	
			if(!is_null($_REQUEST['id'])){
				$params['post_id'] = $id;
				$params['module'] = $module;
			}//end if
			if(isset($_REQUEST['attachment_post_type'])){
				$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
			}//end if
			$result = $utils->get_attachment($params);
			if($result !== false){						
				$data['result'] = 'success';
				$data['data'] = $result;			
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot retrieve the attachment';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "add.attachment.to.post"){
		if($perms["media"] > 0){
			$image = $utils->call("image");
			
			$post_id = filter_var($_REQUEST['post_id'], FILTER_SANITIZE_NUMBER_INT);
			$params['post_id'] = $post_id;	
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);
			$params['file_id'] = $file_id;		
			$params['module'] = $module;
			$params['upload_path'] = '../../upload/';
			if(isset($_REQUEST['attachment_post_type'])){
				$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
			}//end if
			
			$result = $utils->add_attachment_to_post($params);
			$data = array();
			if($result){
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "save.attachment.details"){
		$data = array();
		if($perms[$module] > 0){
			$params = array();
			//compulsory fields					
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);	
			$params['file_id'] = $file_id;				
			$id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
			$params['post_id'] = $id;
			$params['module'] = $module;
			$post_file_id = filter_var($_REQUEST['post_file_id'], FILTER_SANITIZE_NUMBER_INT);
			$params['post_file_id'] = $post_file_id;
			$params['field'] = array(
									"title" => $_REQUEST['title'],
									"caption" => $_REQUEST['caption']
								);
			if(isset($_REQUEST['role']) && !is_null($_REQUEST['role'])){
				$params['field']['role'] = $_REQUEST['role'];
			}//end if
			if(isset($_REQUEST['attachment_post_type'])){
				$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
			}//end if
			$result = $utils->update_post_attachment($params);		
			if($result !== false){						
				$data['result'] = 'success';							
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot retrieve the attachment';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "delete.attachment"){
		$data = array();
		if($perms["media"] > 0){
			$params = array();
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);
			$params['file_id'] = $file_id;
			//compulsory fields				
			if(isset($_REQUEST['post_id'])){
				$post_id = filter_var($_REQUEST['post_id'], FILTER_SANITIZE_NUMBER_INT);	
				$params['post_id'] = $post_id;
				$params['module'] = $module;
			}//end if				
			
			if(isset($_REQUEST['attachment_post_type'])){
				$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
			}//end if
			$result = $utils->delete_attachment($params);
			if($result !== false){						
				$data['result'] = 'success';							
			} else {
				$data['result'] = 'error';	
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);	
	}//end if
	
	if($method == "create.attachment.formats"){
		if($perms["media"] > 0){
			$image = $utils->call("image");
			
			$post_id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);	
			
			$data = array();
			
			//first get the formats
			$params = array("module" => $module, "default" => true);
			$formats = $utils->get_attachments_formats($params);
			if($formats !== false){
				//now for each format we create the image
				for($i = 0; $i < sizeof($formats); $i++){
					$params = array(
						"file_id" => $file_id,
						"format_id" => $formats[$i]['attachment_setting_id'],
						"module" => $module,
						"post_id" => $post_id,
						"upload_path" => "../../upload/"
					);	
					$image->create_new_size($params);	
				}
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot find the available formats for the passed module';	
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if		
		echo json_encode($data);
	}//end if
	
	if($method == "rotate.image"){
		if($perms["media"] > 0){
			$post_id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
			$file_id = filter_var($_REQUEST['file_id'], FILTER_SANITIZE_NUMBER_INT);
			$rotate_all = $_REQUEST['rotate_all'];
			if(!is_null($_REQUEST['format'])){	
				$format = filter_var($_REQUEST['format'], FILTER_SANITIZE_STRING);	
			} else {
				$format = 'o';//default to original image	
			}//end if
			
			if(is_null($rotate_all)){
				$rotate_all = false;	
			}//end if
			
			$rotate_all = (bool)$rotate_all;
			
			$rotation = filter_var($_REQUEST['rotation'], FILTER_SANITIZE_STRING);	
			$degrees = 270;
			if($rotation == "left"){
				$degrees = 90;
			}//end if
			
			$params = array("file_id" => $file_id);
			if(!is_null($post_id) && is_numeric($post_id)){
				$params['post_id'] = $post_id;
				$params['module'] = $module;
			}//end if
			
			//get the attachment info
			$attachment_data = $utils->get_attachment($params);			
			if($attachment_data !== false){		
				$data = array();			
				$image = $utils->call('image');
				if($rotate_all === false){
					$image_file = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.'upload/photos/'.$format.'/'.$attachment_data['attachment_filename'];
					$rotated = $image->rotate_image($image_file,$degrees);
					if($rotated){
						$data['result'] = 'success';			
					}//end if
				} else {
					$formats = $utils->get_attachments_formats(array('module' => $module, 'default' => true));
					if($formats !== false){
						for($i = 0; $i < sizeof($formats); $i++){
							$image_file = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.'upload/photos/'.$formats[$i]['attachment_setting_folder'].'/'.$attachment_data['attachment_filename'];
							$rotated = $image->rotate_image($image_file,$degrees);
						}//end if
						//we also rotate the original image
						$image_file = $_SERVER['DOCUMENT_ROOT'].__SERVERPATH__.'upload/photos/o/'.$attachment_data['attachment_filename'];
						$rotated = $image->rotate_image($image_file,$degrees);
						$data['result'] = 'success';		
					} else {
						$data['result'] = 'error';
						$data['message'] = 'Cannot get the attachments formats';	
					}
				}//end if
			} else {
				$data['result'] = 'error';
				$data['message'] = 'Cannot find the image';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if		
		echo json_encode($data);	
	}//end function
	
	if($method == "post.attachments.save.sorting"){
		if($perms[$module] > 0){
			$ids = $_POST['list-row-id'];
			$post_id = $_POST['post_id'];
			
			$params = array(
				'ids' => $ids,
				'post_id' => $post_id,
				'module' => $module
			);
			
			if(isset($_REQUEST['attachment_post_type'])){
				$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
			}//end if
			
			$data = array();
			$result = $utils->save_post_attachments_sorting($params);
			
			if($result !== false){
				$data['result'] = "success";				
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot save this sorting";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data); 
	}//end if
	
	if($method == "video.post.upload.actions"){
		$data = array();
		if($perms["media"] > 0){
			$file_id = $_POST['file_id'];
			$params = array('file_id' => $file_id);
			$result = $utils->post_upload_video_actions($params);
			if($result !== false){
				$data['result'] = "success";				
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot execute the post upload video actions";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if		
		echo json_encode($data);	
	}//end if
	
	if($method == "transcode.video"){
		$data = array();
		if($perms["media"] > 0){
			$file_id = $_POST['file_id'];
			$params = array('file_id' => $file_id);
			$result = $utils->transcode_video($params);
			if($result !== false){
				$data['result'] = "success";				
			} else {
				$data['result'] = 'error';
				$data['error'] = "Cannot transcode the passed video";
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if		
		echo json_encode($data);
	}//end if
?>