<?php
	if($method == "save.settings"){							
		$data = array();				
		if($perms['users'] > 3){
			if($utils->update_settings(array("module_id" => $module))){					
				$data['result'] = 'success';						
			} else {
				$data['result'] = 'error';	
				$data['error'] = $utils->errors;
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
	
	if($method == "get.settings"){							
		$data = array();				
		if($perms['users'] > 3){
			$result = $utils->get_settings(array("module_id" => $module));					
			if($result !== false){
				$user_id = $users->uid;
				$data['result'] = 'success';
				$data['settings'] = $result;						
			} else {
				$data['result'] = 'error';	
				$data['error'] = 'Cannot retrieve the settings for the module passed';
			}//end if
		} else {
			$data['result'] = 'error';	
			$data['error'] = 'No permission';
		}//end if
		echo json_encode($data);
	}//end if
?>