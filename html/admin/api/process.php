<?php
	ob_start('ob_gzhandler');
	session_start();
	ini_set("display_errors",1);
	include("../config.inc.php");
	$curpath = "../";
	include("../classes/utils.class.php");
	
	//API Class
	$api = $utils->call("api_keys");	
	//Users Class
	$users = $utils->call("users");	
	//Logger Class
	$logger = $utils->call("logger");
	
	//api settings
	$api_settings = $utils->get_settings(array('module' => 'api_keys'));
	if(isset($api_settings['database_error_mode'])){
		$utils->set_db_errmode($api_settings['database_error_mode']);
	}//end if
		
	//Params	
	$method = $_REQUEST['m'];
	$api_key = $_REQUEST['api_key'];
	
	if(!is_null($api_key)){
		$logger->set_api_key($api_key);
	}
	
	//check in advance if whoever is calling the api is already blacklisted
	if($logger->is_blacklisted($_SERVER['REMOTE_ADDR'])){
		$logger->log_event(array("login_area" => "private","type" => "blacklisted.logged","module_id" => "generic", "message" => "Goodbye Spammer"));			
		die();	
	}//end if
	
	$module = $_REQUEST['module'];	
	if(!is_null($module) && $module != "users"){
		$params = array("module" => $module);	
		//Check if exists a class for this module
		$module_class_path = $module;
		$module_class_path = str_replace("_",".",$module_class_path);
		define("__MODULE_PATH__",__CMS_PATH__.$module);		
		if(file_exists("classes/".$module_class_path.".class.php")){
			${$module} = $utils->call($module,$params);
		} else {
			${$module} = $utils->call("ewrite",$params);
		}//end if
	} else  if($module == "users"){
		define("__MODULE_PATH__",__CMS_PATH__."users");		
	}//end if	
	
		
	if($method != NULL){										
		if($method == "login"){
			$flood = $logger->check_flood(array('task' => "api.login", "threshold" => 10));	
			//var_dump($flood);		
			if($flood){
				//Spammer
				$logger->log_event(array("login_area" => "private","type" => "spammer.logged","module_id" => "users", "message" => "This is a spammer"));					
				$data = array();
				$data['result'] = 'error';
				$data['error'] = 'You failed to login too many times, sorry buddy but you can\'t login anymore';
				echo json_encode($data);
				die();	
			}//end if			
			
			if($api->check_key($api_key)){		
				$user = trim($_REQUEST['username']);	
				$pass = trim($_REQUEST['password']);
				
				$auth = $utils->call('auth');	
				if(defined(__SECURE_CMS__)){
					if(__SECURE_CMS__ == true){
						$auth->restrict_access(array("lock_time" => true, "allowed_ips" => array('89.204.221.25')));	
					}//end if
				}//end if
				$data = array();
				if($auth->do_login($user,$pass)){
					$logger->log_event(array("login_area" => "public","type" => "api.auth.login.successful","module_id" => "users"));	
					$data['result'] = "pass";
				} else {
					$logger->log_event(array("login_area" => "public","type" => "api.auth.login.failed","module_id" => "users"));	
					$data['result'] = "error";
				}//end if
				echo json_encode($data);
			} else {			
				//echo 'You are not allowed to see this page';
			}//end if
		}//end if
		
		$secured = $users->check_secure_id();
		$user_id = $users->get_uid();
		$logged = false;
		if($user_id !== false){
			$logged = true;	
		}//end if
		
		
		####################################################
		# PRIVATE CALLS ACCESSIBLE ONLY TO LOGGED IN USERS #
		####################################################
		if($secured && $logged){
			$perms = $users->user_permissions();
			//log any calls 
			$params = array("login_area" => "private","type" => "api.method.".$method,"module_id" => $module,"message" => base64_encode(serialize($_REQUEST)));
			if(isset($_REQUEST['id'])){
				$params['post_id'] = $_REQUEST['id'];	
			}//end if
			$logger->log_event($params);	
			
			#################################
			# POSTS CALLS 					#	
			#################################	
			
			require_once("inc/posts.process.php");
			
			#################################
			# META CALLS 					#	
			#################################	
			
			require_once("inc/meta.process.php");
			
			#################################
			# CATEGORIES CALLS				#	
			#################################	
			
			require_once("inc/categories.process.php");
			
			#################################
			# SETTINGS CALLS 				#	
			#################################	
			
			require_once("inc/settings.process.php");
			
			#################################
			# ATTACHMENTS CALLS				#	
			#################################	
			
			require_once("inc/attachments.process.php");
			
			#################################
			# USERS CALLS 					#	
			#################################	
			
			require_once("inc/users.process.php");
			
			#################################
			# MAIN CALLS 					#	
			#################################
			
			require_once("inc/main.process.php");
			
			#################################
			# BOOKINGS CALLS				#	
			#################################
			
			require_once("inc/bookings.process.php");
			
			#################################
			# CUSTOM CALLS 					#	
			#################################
			
			if(file_exists("inc/custom.process.php")){
				require_once("inc/custom.process.php");
			}//end if
	
			if(isset($module) && !is_null($module)){				
				if(file_exists("../modules/".$module."/api.process.php")){
					require_once("../modules/".$module."/api.process.php");
				}//end if
			}//end if
		} else {			
			/*$data = array();
			$data['error'] = "You must be logged in";
			echo json_encode($data);*/
		}//end if		
		
		
		#####################################
		# PUBLIC CALLS ACCESSIBLE TO ANYONE #
		#####################################
		//this methods don't require an API Key	
		if($method == "logout"){
			$auth = $utils->call("auth");
			$auth->do_logout();
			$logger->log_event(array("login_area" => "public","type" => "api.auth.logout.successful","module_id" => "users"));
			header("location: ".__CMS_PATH__);
		}//end if
		
		if($method == "test"){
			echo "hello world";	
		}//end if
	} else {
		echo 'You didn\'t specify any method.';	
	}//end if
?>