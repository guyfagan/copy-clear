<?php
	$users = $utils->call('users');
	if(__SECURE_CMS__ == true){
		$auth = $utils->call("auth_ext");
		$auth->restrict_access(array("lock_time" => true, "allowed_ips" => array('82.141.221.107','89.204.221.25')));
		if($auth->check_locks() === false){
			$logs = $utils->call("users_logs");
			$log_params = NULL;
			if($users->get_uid() !== false){
				$log_params = array("user_id" => $users->get_uid());
			}//end if
			$logs->log_access($log_params);
			header('location: '.__CMS_PATH__.'index.php');
		}//end if
	}//end if	
	
	$perms = $users->user_permissions();
	$utils->view->set_user_permissions($perms);
			
	$this_page = $_SERVER['PHP_SELF'];
	$this_page = basename($this_page);
	
	if($users->get_uid() == NULL){
		header('location: '.__CMS_PATH__.'index.php');
	}//end if

	if(!$users->check_secure_id()){
		header('location: '.__CMS_PATH__.'index.php');
	}//end if
	
	if(defined("__FORCE_SSL__") && ($utils->view->settings['forceProtocol'] == "" || is_null($utils->view->settings['forceProtocol']))){
		if(__FORCE_SSL__){
			if (!isset($_SERVER['HTTPS'])) {
			   header('Location: https://'.$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
			}//end if
		}//end if	
	}//end if

	if($utils->view->settings['forceProtocol'] != ""){
		if($utils->view->settings['forceProtocol'] == 'http'){
			if (isset($_SERVER['HTTPS'])) {
			   header('Location: http://'.$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
			}	
		} else {
			if (!isset($_SERVER['HTTPS'])) {
			   header('Location: https://'.$_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
			}
		}//end if
	}//end if
/*
	if($users->check_page_permission($_GET['module']) == false && $this_page != 'main.php'){
	//	header('location: '.__CMS_PATH__.'index.php');
	}//end if
*/
	
?>