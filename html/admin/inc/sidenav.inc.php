<?php
	$groups = $utils->get_modules_groups();
?>
<div class="well sidebar-nav">
    <ul class="nav nav-list">        
        <?php
		if($groups !== false){
			for($j = 0; $j < sizeof($groups); $j++){				
				if($groups[$j]['module_group_hidden'] == "0"){
					$group_name = $groups[$j]['module_group_name'];
					$group_id = $groups[$j]['module_group_id'];						
					$modules = $utils->get_modules($group_id);
					if($modules != false){
						echo '<li class="nav-header">'.$group_name.'</li>';
						for($i = 0; $i < sizeof($modules); $i++){
							$show_module = true;
							if($perms[$modules[$i]['module_name']] == 0){
								$show_module = false;	
							}//end if
							if($modules[$i]['module_name'] == 'media'){
								if($utils->view->settings['media']['hideMediaLibraryOnNav'] == 1){
									$show_module = false;	
								}//end if
							}//end if
							if($show_module){
								echo '<li';
								if($_REQUEST['m'] == $modules[$i]['module_name']){
									echo ' class="active"';	
								}//end if
								echo '><a href="'.__CMS_PATH__.$modules[$i]['module_name'].'/">'.$modules[$i]['module_label'].'</span></a></li>';
							}//end if
							if(!is_null($utils->view->subnav[$modules[$i]['module_name']]) && $_REQUEST['m'] == $modules[$i]['module_name']){
								echo '<ul class="subnav">';
								$subnav = $utils->view->subnav[$modules[$i]['module_name']];
								foreach($subnav as $key => $value){
									echo '<li';
									$subnav_icon = '';
									if($utils->view->action == $key){
										echo ' class="active"';	
										$subnav_icon = '<i class="icon-caret-right"></i>';
									}//end if
									$subnav_path = str_replace('.','/',$key);
									echo '>'.$subnav_icon.' <a href="'.__CMS_PATH__.$modules[$i]['module_name'].'/'.$subnav_path.'/">'.$value.'</span></a></li>';
								}//end if
								echo '</ul>';
							}//end if
						}//end for
					}//end if
				}//end if			
			}//end for j
		}//end if
		?>
    </ul>
</div><!--/.well -->
 <script type="text/javascript">
$().ready(function(){
	$(".module-nav").each(function(){
		if($("li",$(this)).length == 0){
			$(this).remove();	
		}
	});	
	
	$('.nav-header').each(function(){
		if($(this).next('li:not(.nav-header)').length == 0){
			$(this).remove();	
		}
	});
});
</script>