<?php 
	if(isset($this->module_settings['module_local_search']) && isset($this->module_settings['module_local_search_view'])){
		$local_search = (bool)$this->module_settings['module_local_search'];
	} else {
		$local_search = false;
	}//end if
	
	if($local_search){
		$searchpath = __CMS_PATH__.$this->module.'/'.$this->module_settings['module_local_search_view'].'/';
	} else {
		$searchpath = __CMS_PATH__.'main/search/';
	}//end if
?>
<form action="<?php echo $searchpath ?>" method="post" id="search-form" name="search-form" class="pull-right">              
    <input class="input-medium search-query search-post" id="search" name="search" type="text"<?php
    if(isset($_GET['search'])){
        echo ' value="'.$_REQUEST['search'].'"';
    }//end if
    ?>>
    <button class="btn btn-search" type="submit">Search</button>   
</form>     