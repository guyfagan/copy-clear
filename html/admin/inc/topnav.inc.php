	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#"><?php echo $utils->view->settings['siteName'] ?> CMS</a>            
          <div class="nav-collapse collapse">          	                   
            <ul class="nav pull-right">            	
            	<li class="dropdown">                   
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Logged in as <strong><?php echo $user_data['user_username'] ?></strong><b class="caret"></b></a>
                    <ul class="dropdown-menu">         	
                        <li><a href="<?php echo __CMS_PATH__ ?>profile/"><i class="icon-user"></i> Edit Profile</a></li>
                        <li><a href="<?php echo __CMS_PATH__ ?>api/logout"><i class="icon-signout"></i> Sign Out</a></li> 
                    </ul>                    
                </li>
            	<li><a href="<?php echo __CMS_PATH__ ?>dashboard/">Dashboard</a></li>
            	<!--li><a href="#help">Help</a></li-->
            	<li class="dropdown">
              		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
                	<ul class="dropdown-menu">
                    	<?php
						if($is_admin){
						?>
                        <li><a href="<?php echo __CMS_PATH__ ?>settings/"><i class="icon-briefcase"></i> Global settings</a></li>                       
                        <li><a href="<?php echo __CMS_PATH__ ?>main/modules/"><i class="icon-sitemap"></i> Manage Modules</a></li> 
                        <li><a href="<?php echo __CMS_PATH__ ?>media/settings/"><i class="icon-picture"></i> Media Settings</a></li>
                        <?php
							if($utils->view->settings['enableTemplates'] == 1){
						?>
                        <li><a href="<?php echo __CMS_PATH__ ?>main/templates/"><i class="icon-code"></i> Manage Templates</a></li>          
                        <?php	
							}//end if	
						?>
                        <li class="divider"></li>    
                        <?php
						}//end if
						?>                   
                        <?php						
						if(!is_null($module) && $module != "media" && $perms[$module] == 7){
						?>
                        <li><a href="<?php echo __CMS_PATH__.$module ?>/settings/"><i class="icon-wrench"></i> Module Settings</a></li>
                        <li class="divider"></li> 
                        <?php
						}//end if
						if($is_admin){			
						?>
                        <li><a href="<?php echo __CMS_PATH__ ?>main/logs/"><i class="icon-bug"></i> Logs</a></li>
                        <?php	
						}//end if
						if($perms['users'] == 7){
						?>
                        <li><a href="<?php echo __CMS_PATH__ ?>api_keys/"><i class="icon-key"></i> API</a></li>                             
                        <?php
						}//end if
						?>
                        <li><a href="#"><i class="icon icon-code-fork"></i> Version: <?php echo __CMS_VERSION__ ?></a></li>                  
                    </ul>
            	</li>            
            </ul>
            <!--p class="navbar-text pull-right">            	
            	Logged in as <a href="#" class="navbar-link"><?php echo $user_data['user_username'] ?></a>
            </p-->     
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>