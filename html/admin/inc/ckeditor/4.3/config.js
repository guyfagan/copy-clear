/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	
	config.language = 'en';
	
	// The toolbar groups arrangement, optimized for two toolbar rows.
	/*config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];
	*/
	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	
	config.toolbar = 'MessageToolbar';

    config.toolbar_MessageToolbar =
    [
	 	['Format','-','Styles','-','Image','-','Flash'],
        ['Bold','Italic','Underline','StrikeThrough','NumberedList','BulletedList','-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
		['Link','Unlink','Table','HorizontalRule','Iframe','MediaEmbed','Source','Maximize']
    ];
	
	config.toolbar = 'extBasic';

    config.toolbar_extBasic =
    [
        ['Bold','Italic','-','NumberedList','BulletedList','-','Undo','Redo','Table','-','Link','Unlink','-','About']
    ];
	
	config.toolbar = 'Basic';

    config.toolbar_Simple =
    [
        ['Bold','Italic','-','NumberedList','BulletedList','-','Link','Unlink','-','About']
    ];
		
	config.toolbar = 'Simple';

    config.toolbar_Simple =
    [
        ['Bold','Italic','SpecialChar','NumberedList','BulletedList','-','Link','Unlink']
    ];
    
	config.forcePasteAsPlainText = false;
	config.extraPlugins = 'magicline,horizontalrule,iframe,mediaembed';
	config.allowedContent = true;	
};