<?php
	$user_data = $users->get_user();
	$is_admin = false;
	if($user_data['users_group_admin'] == 1){
		$is_admin = true;	
	}//end if
	$module = $_REQUEST['m'];
	$action = $_REQUEST['a'];
	if(!is_null($module)){
		$params = array("module" => $module);
		$utils->view->set_module($module);
		//Check if exists a class for this module
		$module_class_path = $module;
		$module_class_path = str_replace("_",".",$module_class_path);
		define("__MODULE_PATH__",__CMS_PATH__.$_REQUEST['m']);		
		if(file_exists("classes/".$module_class_path.".class.php")){
			${$module} = $utils->call($module,$params);
		} else {
			${$module} = $utils->call("ewrite",$params);
		}//end if
	}//end if
		
	if($utils->view->settings['siteCharset'] != ""){
		define("__CMS_CHARSET__",$utils->view->settings['siteCharset']);	
	}//end if
	
	//checking if the charset is defined
	if(!defined(__CMS_CHARSET__)){
		//fallback in case is not  defined
		define("__CMS_CHARSET__","utf-8");	
	}//end if
?>