// JavaScript Document
$().ready(function(){
	(function( $ ){	
		
		var params;
		
		var methods = {
			init : function( options ) {
				params = $.extend({
					tagInput	 :	 "#tag-line",
					tagCloud	 :	 ""
				},options);				
				var $container = $(this);
								
				//autostart the function
				(function(){					
					$(params.tagInput).keydown(function (e){
						if(e.keyCode == 13){//the user pressed enter
							//get the current value
							if(typeof($.cmsVars.post_id) != "undefined"){
								methods.saveTags();
							} else {
								$("#save-post").autoSave(function(){				
									$.cmsVars.post_id = $("#id").val();
									//Once is saved, call the media manager					
									methods.saveTags();
								});	
							}
						}//end if
					});					
					
					if(typeof($.cmsVars.post_id) != "undefined"){
						methods.loadTags();
					}
				})();
			   
			},
			
			saveTags : function(){
				var $inputField = $(params.tagInput);					
				var $tagline = $inputField.val();							
				$.ajax({
					url:  $.config.apipath + 'post.add.tags',
					dataType:"json",
					data: "tags="+$tagline+"&id="+$.cmsVars.post_id+"&module="+$.config.module,
					type:"post",
					success: function(data){
						if(data.result == "success"){
							$(params.tagInput).val("");								
							if(typeof(data.tags) == "object"){
								for(var i = 0; i < data.tags.length; i++){
									$(params.tagCloud).append('<li><i class="icon-tag"></i> '+data.tags[i]+'</li>');												
								}//end if
							}//end if
						}//end if
					}
				});//end ajax	
			},
			
			loadTags : function(){
				$.ajax({
					url:  $.config.apipath + 'post.get.tags',
					dataType:"json",
					data: "id="+$.cmsVars.post_id+"&module="+$.config.module,
					type:"post",
					success: function(data){		
						if(data.result == "success"){										
							if(typeof(data.tags) == "object"){
								for(var i = 0; i < data.tags.length; i++){
									$(params.tagCloud).append('<li><i class="icon-tag"></i> '+data.tags[i].tag_tag+'<a href="#" data-role-btn="delete-tag" data-tag="'+data.tags[i].tag_tag+'">x</a></li>');												
								}//end if
								methods.loadActions();
							}//end if
						}//end if
					}//end function
				});//end ajax
			},
			
			loadActions : function(){
				$('a[data-role-btn=delete-tag]',$(params.tagCloud)).on('click',function(e){
					e.preventDefault();
					//console.log($(this).data('tag'));	
					var $tag = $(this).data('tag');	
					var $li = $(this).closest('li');
					$.ajax({
						url:  $.config.apipath + 'post.delete.tag',
						dataType:"json",
						data: "id="+$.cmsVars.post_id+"&module="+$.config.module+"&tag="+$tag,
						type:"post",
						success: function(data){	
							console.log(data);	
							if(data.result == "success"){										
								$li.fadeOut("slow",function(){
									$(this).remove();	
								});
							}//end if
						}//end function
					});//end ajax				
				});	
			}
		}
	
		$.fn.tagManager = function( method ) {    
			if ( methods[method] ) {
			  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
			  return methods.init.apply( this, arguments );
			} else {
			  $.error( 'Method ' +  method + ' does not exist on jQuery.tagManager' );
			}      
		};
	
	})( jQuery );

});