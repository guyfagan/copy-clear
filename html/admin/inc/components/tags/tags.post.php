<div id="tag-manager" class="clearfix">
	<div class="input-prepend">
		<span class="add-on"><i class="icon-tags"></i></span>
		<input type="text" name="tag-line" id="tag-line" placeholder="Separate tags by comma..." />
    </div>
    <small class="muted">Press enter to add the tags</small>
    <ul id="tag-box">
    
    </ul>
</div>
<script>
	$().ready(function(){
		$("#tag-manager").tagManager({tagCloud : "#tag-box"});
	});
</script>