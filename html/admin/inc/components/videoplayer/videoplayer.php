<?php
	if($_GET['load_cms'] == 1){
		ob_start();
		session_start();	
		include("../../../config.inc.php");
		$curpath = "../../../../admin/";
		include("../../../classes/utils.class.php");	
		include("../../secure.inc.php");
		include("../../init.cms.inc.php");			
		$id = $_GET['id'];	
	}//end if
	
	$file_id = base64_decode(urldecode($_GET['file_id']));
	$file_data = $utils->get_attachment(array('file_id' => $file_id));
	if($file_data !== false){
		$tools = $utils->call('tools');
		if($file_data['attachment_type'] !== "url"){
			$filepath = __UPLOADPATH__.'files/'.$file_data['attachment_filename'];			
			$mime = $tools->get_mime_from_file($_SERVER['DOCUMENT_ROOT'].$filepath);
		} else {
			$filepath = $file_data['attachment_url'];
			$mime = $tools->get_mime_from_filename($filepath);
		}//end if
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Video Player - <?php echo $file_data['attachment_filename'] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Video">
    <meta name="author" content="Old Hat - http://oldhat.ie">   
    <script src="<?php echo __CMS_PATH__ ?>inc/jquery/jquery-<?php echo __JQUERY_VERSION__ ?>.min.js"></script>   
    <script src="<?php echo __CMS_PATH__ ?>inc/mediaElement/<?php echo __MEDIAELEMENT_VERSION__ ?>/mediaelement.min.js"></script>
    <style>
		html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

	</style>
</head>

<body>	
	<video width="640" height="360" id="player1" src="<?php echo $filepath ?>" type="<?php echo $mime ?>" controls></video>    	
	<script>
    MediaElement('player1', {success: function(me) {
        
        me.play();
        /*
        me.addEventListener('timeupdate', function() {
            document.getElementById('time').innerHTML = me.currentTime;
        }, false);
        
        document.getElementById('pp')['onclick'] = function() {
            if (me.paused)
                me.play();
            else
                me.pause();
        };
    */
    }});
    </script>
</body>
</html>
<?php
	}//end if
?>