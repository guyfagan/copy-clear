<?php
	$templates = $utils->get_templates();
	if(isset($module)){
		$module_settings = $utils->get_settings(array('module_id' => $module));
		if(isset($module_settings['defaultModuleTemplate'])){
			$default_template = $module_settings['defaultModuleTemplate'];
		}//end if
	}//end if
?>
<div class="control-group show-on-saved" id="force-template-option">
    <label>Force Template</label>
    <small class="muted">Force all the children posts of this category with the current template set to "Default" to use this template</small>
    <select name="meta_force_template" id="force-template">
        <option value="default">Default</option>
        <?php
        if($templates !== false){
            for($i = 0; $i < sizeof($templates); $i++){
                echo '<option value="'.$templates[$i]['file'].'"';
				if($edit_mode == true){
					if($templates[$i]['file'] == $post['post_template']){
						echo ' selected="selected"';
					}//end if
				} else if($edit_mode === false && isset($default_template)){
					if($templates[$i]['file'] == $default_template){
						echo ' selected="selected"';
					}//end if
				}//end if
				echo '>'.$templates[$i]['label'].'</option>';
            }//end for i
        }//end if
        ?>
    </select> 
    <label class="checkbox">
    	<input type="checkbox" value="1" name="meta_force_template_all" id="force-template-all" />
        Force the template to ALL children
    </label>  
    <button type="button" class="btn btn-warning" id="set-children-template-btn">Set</button>
</div>
<script>
	$(document).ready(function(){
        $("#set-children-template-btn").click(function(e){
			e.preventDefault();
			$.ajax({
				url: $.config.apipath + 'category.posts.force.template?module='+$.config.module,
				data: 'parent_id='+$('#id').val()+'&template='+$('#force-template').val()+'&force_all='+$('#force-template-all').val(),
				dataType:"json",
				type: 'post',
				success: function(data){
					if(data.result == 'success'){
						$('#force-template-option').popMessage("The template has been set to children posts",{type: "success"});
					} else {
						$('#force-template-option').popMessage("Something went wrong, double check in the logs or contact the system administrator",{type: "error"});
					}
				}
			});
		});
    });
</script>