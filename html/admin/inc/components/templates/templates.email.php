<?php
	$templates = $utils->get_templates(array('type' => 'email'));
	if(isset($module)){
		$module_settings = $utils->get_settings(array('module_id' => $module));
		if(isset($module_settings['defaultModuleEmailTemplate'])){
			$default_template = $module_settings['defaultModuleEmailTemplate'];
		}//end if
	}//end if
?>
<div class="control-group">
    <label>Template</label>
    <select name="template" id="template" data-toggle="optional-field">
        <option value="default">Default</option>
        <?php
        if($templates !== false){
            for($i = 0; $i < sizeof($templates); $i++){
                echo '<option value="'.$templates[$i]['file'].'"';
				if($edit_mode == true){
					if($templates[$i]['file'] == $post['post_template']){
						echo ' selected="selected"';
					}//end if
				} else if($edit_mode === false && isset($default_template)){
					if($templates[$i]['file'] == $default_template){
						echo ' selected="selected"';
					}//end if
				}//end if
				echo '>'.$templates[$i]['label'].'</option>';
            }//end for i
        }//end if
        ?>
    </select>   
</div>