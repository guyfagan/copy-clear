<?php
	$templates = $utils->get_templates();
?>
<div class="control-group">
    <label>Default Template for this module</label>
    <select name="defaultModuleTemplate" id="default-module-template">
        <option value="default">Default</option>
        <?php
        if($templates !== false){
            for($i = 0; $i < sizeof($templates); $i++){
                echo '<option value="'.$templates[$i]['file'].'"';
				if($edit_mode == true){
					if($templates[$i]['file'] == $post['post_template']){
						echo ' selected="selected"';
					}//end if
				}//end if
				echo '>'.$templates[$i]['label'].'</option>';
            }//end for i
        }//end if
        ?>
    </select>   
</div>