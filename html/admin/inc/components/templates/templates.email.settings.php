<?php
	$templates = $utils->get_templates(array('type' => 'email'));
	if(!isset($emailTemplateName)){
		$emailTemplateName = 'defaultModuleEmailTemplate';	
		$emailTemplateID = 'default-module-email-template';
	} else {
		$emailTemplateID = $emailTemplateName;	
	}
	
	if(!isset($emailTemplateLabel)){
		$emailTemplateLabel = 'Html Email Template <small class="muted">This will work only if the email type is set to "Html"</small>';	

	}
?>
<div class="control-group">
    <label><?php echo $emailTemplateLabel ?></label>
    <select name="<?php echo $emailTemplateName ?>" id="<?php echo $emailTemplateID ?>">
        <option value="">None</option>
        <?php
        if($templates !== false){
            for($i = 0; $i < sizeof($templates); $i++){
                echo '<option value="'.$templates[$i]['file'].'"';
				if($edit_mode == true){
					if($templates[$i]['file'] == $settings[$emailTemplateName]){
						echo ' selected="selected"';
					}//end if
				}//end if
				echo '>'.$templates[$i]['label'].'</option>';
            }//end for i
        }//end if
        ?>
    </select>   
</div>