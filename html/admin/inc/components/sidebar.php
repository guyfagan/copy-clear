<aside id="sidebar" class="sidebar sidebar-expanded">	
<?php
	if(!isset($utils->view->config['enable_post_info_sidebar']) || (isset($utils->view->config['enable_post_info_sidebar']) && $utils->view->config['enable_post_info_sidebar'] === true)){
?>
	<h3><a href="#" data-toggle="collapse" data-target="#section-info">Post Info</a></h3>
    <div id="section-info" class="collapse in">
    	<div class="side-section">
    <?php
		$status = "draft";
		if(isset($post) && $edit_mode == true){
			if($post['post_status'] == 1){
				$status = "published";
			}//end if
			$last_update = $post['post_last_update'];
			if($last_update == 0){
				$last_update = $post['post_date'];	
			}//end if
			$last_update = date("l d/m/Y H:i",$last_update);
		}//end if
		if($perms[$module] > 3){
	?>
    		<strong>Status: </strong><select name="status" id="status" class="input-medium">
            	<option value="0"<?php 
				if(!isset($post) || (isset($post) && $post['post_status'] == "0")){
					echo ' selected="selected"';	
				}//end if
				?>>Draft</option>
                <option value="1"<?php
                if(isset($post) && $post['post_status'] == 1){
					echo ' selected="selected"';	
				}//end if
				?>>Published</option>
            </select><br />
    <?php
		} else {
	?>
    	<strong>Status: </strong><?php echo ucwords($status) ?><br /><br />
        <input type="hidden" name="status" id="status" value="<?php echo $post['post_status'] ?>" />
    <?php
		}//end if
	?>
            <strong>Type: </strong><select name="type" id="type" class="input-medium" data-toggle="optional-field">
            	<option value="post"<?php 
				if($edit_mode == true){
					if($post['post_type'] == 'post'){
						echo ' selected';	
					}//end if
				}//end if
				?>>Post</option>
                <option value="intro"<?php 
				if($edit_mode == true){
					if($post['post_type'] == 'intro'){
						echo ' selected';	
					}//end if
				}//end if
				?>>Parent Introduction</option>
                <option value="redirect"<?php 
				if($edit_mode == true){
					if($post['post_type'] == 'redirect'){
						echo ' selected';	
					}//end if
				}//end if
				?>>Redirect</option>
                <option value="download"<?php 
				if($edit_mode == true){
					if($post['post_type'] == 'download'){
						echo ' selected';	
					}//end if
				}//end if
				?>>Download File</option>
            </select><br />
            <div class="redirect-to" data-type="template-optional-field" data-template-target="#type" data-template-match="redirect">            
           	  <strong>Route to: </strong>
                <input type="text" name="link_to" id="link_to" class="input-block-level"<?php
				if(isset($post) && $edit_mode == true){
					echo ' value="'.$post['post_link_to'].'"';
				}//end if
				?> />
            </div>          
            <div class="download-file-options" data-type="template-optional-field" data-template-target="#type" data-template-match="download">
            <?php
				if($edit_mode){
					if(isset($id) && isset($module)){					
			?>
                <select name="meta_direct_download" id="direct-download">
                	<option>Select a file</option>
			<?php
						$attachments = $utils->get_attachments(array("post_id" => $id, "module" => $module, 'attachment_post_type' => $attachment_post_type));
						if($attachments != false){
							for($i = 0; $i < sizeof($attachments); $i++){
								echo '<option value="'.$attachments[$i]['attachment_post_id'].'">'.$attachments[$i]['attachment_friendly_name'].'</option>';
							}//end for i
						}//end if			
			?>	
            	</select>
            <?php			
					} else {
						echo '<div class="alert">No attachments found</div>';
					}//end if
				}//end if 
			?>            	
            </div>
          <?php
			if($edit_mode == true){
			?>
            <strong>Last Update: </strong><?php echo $last_update; 
			}//end if
			?>            
    	</div>
    </div>    
    <?php
	}//end if
	if(!isset($utils->view->config['enable_attachments_sidebar']) || (isset($utils->view->config['enable_attachments_sidebar']) && $utils->view->config['enable_attachments_sidebar'] === true)){
	?>
    <h3><a href="#" data-toggle="collapse" data-target="#section-attachments">Post Attachments</a></h3>
    <div id="section-attachments" class="collapse in">
    	<div class="side-section">
        	<h4>Default Image</h4>
            <small class="muted">Drag any image from the files list in the box below to set as default image</small>
            <div class="side-default-image-box" id="default-image-drop-in">            	
                <?php
				if($edit_mode == true){
					//getting the default image, if it's already set					
					$def_img_options = array("post_id" => $id, "module" => $module, "limit" => 1, "role" => "default-image");
					if(isset($attachment_post_type)){
						$def_img_options['attachment_post_type'] = $attachment_post_type;	
					}//end if
					$default_img = $utils->get_attachments($def_img_options);
					if($default_img !== false){	
						echo '<div class="default-image">';		
						echo '<img src="'.__SERVERPATH__.'upload/photos/t/'.$default_img['attachment_filename'].'" alt="'.$default_img['attachment_friendly_name'].'" /><span>';
						echo $default_img['attachment_friendly_name'];						
						echo '</span>';
						echo '<a hrer="#" class="btn btn-danger btn-mini" id="remove-default-image" data-role="remove-default-image" data-post-file-id="'.$default_img['attachment_post_id'].'" data-file-id="'.$default_img['attachment_id'].'"><i class="icon-trash"></i></a>';
						echo '</div>';
					} else {
						echo '<span class="drag-here-label">Drag the photo here</strong>';
					}//end if
				} else {
					echo '<span class="drag-here-label">Drag the photo here</strong>';
				}//end if
				?>                
            </div>
            <h4>Files</h4>
            <?php
            require_once("media/media.post.attachments.php");
            ?>
            <a href="#mediaModal" role="button" data-toggle="modal" data-textarea="message" class="btn btn-mini media-btn"><i class="icon-picture"></i> Upload</a>
            <!--a href="#" class="btn btn-mini"><i class="icon-th"></i> Create New Collection</a-->
            <a href="#" class="btn btn-mini" id="sort-attachments-btn"><i class="icon-sort-by-attributes-alt"></i> Sort</a>
        </div>
    </div>
    <?php
	}//end if
	if(!isset($utils->view->config['enable_categories_sidebar']) || (isset($utils->view->config['enable_categories_sidebar']) && $utils->view->config['enable_categories_sidebar'] === true)){
	?>
    <h3><a href="#" data-toggle="collapse" data-target="#section-categories">Categories</a></h3>
    <div id="section-categories" class="collapse in">
    	<div class="side-section">
        	<?php
			require_once("categories/categories.post.php");
			?>
        </div>
    </div>
    <?php
	}//end if
	if(!isset($utils->view->config['enable_seo_sidebar']) || (isset($utils->view->config['enable_seo_sidebar']) && $utils->view->config['enable_seo_sidebar'] === true)){
	?>
    <h3><a href="#" data-toggle="collapse" data-target="#section-seo">SEO</a></h3>
    <div id="section-seo" class="collapse">
    	<div class="side-section">
            <label>Meta Title</label>
            <input type="text" name="meta_title" id="meta-title" />
            <label>Meta Description</label>
            <textarea name="meta_description" id="meta-description"></textarea>
        </div>
    </div>
    <?php
	}//end if
	if(!isset($utils->view->config['enable_tags_sidebar']) || (isset($utils->view->config['enable_tags_sidebar']) && $utils->view->config['enable_tags_sidebar'] === true)){
	?>
    <h3><a href="#" data-toggle="collapse" data-target="#section-tags">Tags</a></h3>
    <div id="section-tags" class="collapse in">
    	<div class="side-section">
        	<?php
			require_once("tags/tags.post.php");
			?>
        </div>
    </div>
    <?php
	}//end if
	if(!isset($utils->view->config['enable_revisions_sidebar']) || (isset($utils->view->config['enable_revisions_sidebar']) && $utils->view->config['enable_revisions_sidebar'] === true)){
		if($edit_mode == true){
		?>
		<h3><a href="#" data-toggle="collapse" data-target="#section-revisions">Revisions</a></h3>
		<div id="section-revisions" class="collapse">
			<div class="side-section">
				<?php
				require_once("revisions/revisions.post.php");
				?>
			</div>
		</div>
		<?php
		}//end if
	}
	?>
</aside>