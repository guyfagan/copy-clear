<div class="categories-explorer">
<?php
	$tools = $this->utils->call('tools');
	$id = $_GET['id'];
	if(isset($id)){			
		$catManager = $this->utils->call('ewrite');
		
		$post_data = $catManager->get_post(array('id' => $id));
		if($post_data !== false){
			
			$relationships = $catManager->get_post_relationships(array('id' => $id));
			echo '<pre>';
			var_dump($relationships);
			echo '</pre>';
			
			if($relationships !== false){
				$current_post_modules = array();
				$current_post_categories = array();
				for($x = 0; $x < sizeof($relationships); $x++){
					array_push($current_post_modules,$relationships[$x]['module_item_order_module_id']);
					array_push($current_post_categories,$relationships[$x]['module_item_order_father_id']);
				}//end if
			}//end if
			
			$modules_list = $this->utils->get_modules(array('standard' => true));
			if(isset($current_module) && $current_module == true){
				$modules_list = array();
				$modules_list[0] = array();
				$modules_list[0] = $this->utils->get_module($this->module);
			}//end if
			
			//now we need to create the main index, in case of current_module equal to true, the main index will contain only the current module
			if($modules_list !== false){
				for($i = 0; $i < sizeof($modules_list); $i++){
					echo '<a href="#" data-toggle="collapse" data-target="#cm-module-'.$modules_list[$i]['module_id'].'">'.$modules_list[$i]['module_label'].'</a>';
					echo '<div id="cm-module-'.$modules_list[$i]['module_id'].'" class="collapse';
					if($modules_list[$i]['module_name'] == $this->module){
						echo ' in';	
					}//end if
					echo '">';
					$map = $tools->get_sitemap(array('module' => $modules_list[$i]['module_name']));
					if($map !== false){
						echo '<ul>';
						if($catManager->check_post_relationship(array('id' => $id, 'data' => $relationships,'module_id' => $modules_list[$i]['module_id'],'parent_id' => '0'))){
							echo '<li>Post sits here</li>';
						}//end if
						
						for($x = 0; $x < sizeof($map[$modules_list[$i]['module_name']]); $x++){							
							if($map[$modules_list[$i]['module_name']][$x]['is_category'] == true){
								echo '<li>'.$map[$modules_list[$i]['module_name']][$x]['label'].'</li>';
							}//end if
						}//end if
						echo '</ul>';
					}//end if
					//var_dump($map);
					echo '</div>';
				}//end for i
			}//end if
		} else {
		?>
        <div class="alert"><i class="icon icon-warning-sign"></i> The post you are looking to move it doesn't exist or cannot be found</div>
        <?php
		}//end if
	} else {
	?>
    <div class="alert"><i class="icon icon-warning-sign"></i> You didn't specify any Posts to move</div>
    <?php
	}//end if
?>
</div>