// JavaScript Document
// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "categoryManager",
        defaults = {
            local: false
        };

    // The actual plugin constructor
    function Plugin( element, options ) {
        this.element = element;

        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend( {}, defaults, options );

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init: function() {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.options
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.options).
			
			var plugin = this;
				
			this.options._dataObj = {};
			this.options._dataObj.path = new Array();
			
			if(this.options.local){
				this.options._dataObj.module = $.config.module;
				this.options._dataObj.depth = 0;
				this.options._dataObj.path.push(0);
			} else {
				this.options._dataObj.depth = -1;
			}//end if						
			
			this.populate($("#explore"),this.options);
			
			$("#categoryModal").on('hidden',function(){
				plugin.updateCategoriesList();
			});//end function
			
			$("#explore").off('click','a[data-type="module"],a[data-type="category"]');
			$("#explore").on('click','a[data-type="module"],a[data-type="category"]',function(e){
				e.preventDefault();	
				
				plugin.options._dataObj.depth++;
				switch($(this).data('type')){
					case 'module':
						plugin.options._dataObj.module = $(this).data('id');
						plugin.options._dataObj.path.push(0);
						plugin.populate($("#explore"),plugin.options);
						break;
					case 'category':						
						plugin.options._dataObj.path.push($(this).data('id'));
						plugin.options._dataObj.parent_id = $(this).data('id');					
						plugin.populate($("#explore"),plugin.options);
						break;	
				}		
			});
			
			$("#explore").off('click','a[data-type="back"]');
			$("#explore").on('click','a[data-type="back"]',function(e){
				e.preventDefault();	
				plugin.options._dataObj.depth--;	
					
				if(plugin.options._dataObj.depth < 0){
					if(plugin.options.local == false){
						delete plugin.options._dataObj.module;
						delete plugin.options._dataObj.parent_id;					
						plugin.populate($("#explore"),plugin.options);
					}//end if
				} else {					
					//plugin.options._dataObj.father_id = plugin.options._dataObj.parent_id;
					plugin.options._dataObj.parent_id = plugin.options._dataObj.path[plugin.options._dataObj.depth];
				
					plugin.populate($("#explore"),plugin.options);
				}
			});
			
			$("#explore").off('click','a.circle-btn');
			$("#explore").on('click','a.circle-btn',function(e){
				e.preventDefault();	
				if($(this).hasClass('active')){
					$(this).removeClass('active');	
					var delete_id = $(this).data('id');
					
					for(i = 0; i < $.cmsVars.postCategories.length; i++){
						if(delete_id == $.cmsVars.postCategories[i]['category_id']){
							plugin.deletePostCategory(delete_id);
							$.cmsVars.postCategories.splice(i,1);
						}//end if
					}//end for i
				} else {
					$(this).addClass('active');
					var newCategory = {};
					newCategory['category_id'] = $(this).data('id');
					newCategory['category_name'] = $(this).data('label');	
					//check if the id is already in the array
					var found = false;
					for(i = 0; i < $.cmsVars.postCategories.length; i++){
						if(newCategory['category_id'] == $.cmsVars.postCategories[i]['category_id']){
							found = true;	
						}//end if
					}//end for i
					if(!found){
						plugin.savePostCategory(newCategory['category_id']);
						$.cmsVars.postCategories.push(newCategory);
					}//end if							
				}
			});
        },
		
		savePostCategory: function(category_id){
			$.ajax({
				url: $.config.apipath + 'create.post.relation?module='+this.options._dataObj.module,
				dataType:"json",
				type:'post',
				data: 'id='+$.cmsVars.post_id+'&category_id='+category_id,
				success: function(data){
					//
				}
			});
		},
		
		deletePostCategory: function(category_id){
			$.ajax({
				url: $.config.apipath + 'delete.post.relation?module='+this.options._dataObj.module,
				dataType:"json",
				type:'post',
				data: 'id='+$.cmsVars.post_id+'&category_id='+category_id,
				success: function(data){
					//
				}
			});
		},
		
		updateCategoriesList: function(){
			var $ul = $('.sidebar-categories-list');
			var $html = '';
			if($.cmsVars.postCategories.length > 0){
				for(i = 0; i < $.cmsVars.postCategories.length; i++){
					$html += '<li>'+$.cmsVars.postCategories[i]['category_name'];
					if($.cmsVars.currentCategory == $.cmsVars.postCategories[i]['category_id']){
						$html += '<span class="label pull-right">Current</span>';	
					}//end if
					$html += '<input type="hidden" value="'+$.cmsVars.postCategories[i]['category_id']+'" name="category_id[]" id="category-id-'+$.cmsVars.postCategories[i]['category_id']+'" /></li>';	
				}
			}//end if
			$ul.html($html);
		},
		
		populate: function(el, options){
			var path = $.config.apipath+'explore';
			var viewType = 'modules';
			var plugin = this;
			
			this.getPostCategories();
			
			if(typeof(this.options._dataObj.module) != 'undefined'){
				path += '?module='+this.options._dataObj.module;
				viewType = 'categories';		
			}//end if
			
			if(typeof(this.options._dataObj.parent_id) != 'undefined'){
				path += '&parent_id='+this.options._dataObj.parent_id;
			}//end if
						
			$.ajax({
				url: path,
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						$html = $('<ul id="folders" />');
					
						if(viewType == 'categories' && (plugin.options.local == false || (plugin.options.local == true && plugin.options._dataObj.depth > 0))){
							$html.append('<li><a href="#" data-type="back"><i class="icon-level-up"></i> Level Up</a></li>');
						}//end if
						if(data.data !== false){							
							for(i = 0; i < data.data.length; i++){
								var circleCss = 'circle-btn pull-right';
								if(plugin.options._dataObj.currentCategoriesIDs !== false && viewType == 'categories'){
									if(plugin.options._dataObj.currentCategoriesIDs.indexOf(parseInt(data.data[i]['id'])) != -1){
										circleCss += ' active';	
									}
								}
								if(viewType == 'modules'){
									$html.append('<li><a href="#" data-id="'+data.data[i]['module_id']+'" data-type="module"><i class="icon-folder-open"></i> '+data.data[i]['module_label']+'</a> <a class="'+circleCss+'" data-id="'+data.data[i]['module_id']+'" href="#set-category" data-label="'+data.data[i]['module_label']+'"><i class="icon-ok"></i></a></li>');
								} else {
									$html.append('<li><a href="#" data-id="'+data.data[i]['id']+'" data-type="category" data-father="'+data.data[i]['category_father_id']+'"><i class="icon-folder-open"></i> '+data.data[i]['label']+'</a><a class="'+circleCss+'" data-id="'+data.data[i]['id']+'" href="#set-category" data-label="'+data.data[i]['label']+'"><i class="icon-ok"></i></a></li>');
								}//end if
							}//end for i
						} else {
							$html.append('<li>Empty</li>');	
						}
						
						el.html($html);
					}//end if
				}
			});
		},
		
		getPostCategories : function(){		
			if(typeof($.cmsVars.postCategories) == 'undefined'){
				this.options._dataObj.currentCategories = false;
			} else {
				this.options._dataObj.currentCategoriesIDs = new Array();	
				this.options._dataObj.currentCategories = new Array();	
				for(i = 0; i < $.cmsVars.postCategories.length; i++){
					this.options._dataObj.currentCategoriesIDs.push(parseInt($.cmsVars.postCategories[i]['category_id']));
					this.options._dataObj.currentCategories.push($.cmsVars.postCategories[i]);
				}				
			}
		}
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );