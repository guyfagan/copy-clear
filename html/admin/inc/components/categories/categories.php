<!-- Modal -->
<div id="categoryModal" class="modal modal-large hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel">Categories Manager</h3>
    </div>
    <div class="modal-body">
       <div id="category-manager">
       <ul id="current-categories">
       
       </ul>
       <div id="explore">
       
       </div>
       </div>
	</div> 
    <div class="modal-footer">
    	<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>  
</div>