<div class="sidebar-categories-manager clearfix">
    <ul class="sidebar-categories-list">
	<?php
    //it's a new post, so we check if we have a parent id
    if(isset($category_id)){
        //we get the category_data
        $sidebar_category_data = ${$module}->get_category(array('id' => $category_id));
        if($sidebar_category_data !== false){
            echo '<li>';
            echo $sidebar_category_data['category_name'];
            echo '<span class="label pull-right">Current</span>';
            echo '<input type="hidden" value="'.$category_id.'" name="category_id[]" id="category-id-'.$category_id.'" />';
            echo '</li>';
        }//end if
    }//end if
    
    if($edit_mode == true){
        $sidebar_post_categories = ${$module}->get_post_categories(array('post_id' => $id, 'current_module' => false));						
        if($sidebar_post_categories !== false){
            for($x = 0; $x < sizeof($sidebar_post_categories); $x++){
                if($sidebar_post_categories[$x]['category_id'] !== $category_id){							
                    echo '<li>';
                    echo $sidebar_post_categories[$x]['category_name'];						
                    echo '<input type="hidden" value="'.$sidebar_post_categories[$x]['category_id'].'" name="category_id[]" id="category-id-'.$sidebar_post_categories[$x]['category_id'].'" />';
                    echo '</li>';	
                }//end if
            }//end if
        }//end if   
    }//end if
    ?>
    </ul>
    <?php
    if($sidebar_post_categories === false || !isset($sidebar_post_categories)){
    	if(isset($category_id)){
			$category_data = ${$module}->get_category(array('id' => $category_id));			
			$sidebar_post_categories = array();
			$sidebar_post_categories[0] = $category_data;
		}//end if
    }//end if	
    ?>
    <script>
        $(document).ready(function(){
			<?php
			if(isset($category_id)){
			?>
			$.cmsVars.currentCategory = <?php echo $category_id ?>;
			<?php
			}//end if
			if($sidebar_post_categories !== false && !is_null($sidebar_post_categories)){		
			?>
			$.cmsVars.postCategories = $.parseJSON('<?php echo addslashes(json_encode($sidebar_post_categories)) ?>');
			<?php
			} else {
			?>
			$.cmsVars.postCategories = new Array();
			<?php	
			}//end if
			?>
			//This function calls the category manager			
			$("#add-post-category-btn").click(function(e){
				e.preventDefault();			
				
				$("#save-post").autoSave(function(){				
					$.cmsVars.post_id = $("#id").val();				
					//Once is saved, call the category manager	
							
					$("#category-manager").categoryManager({local: true});
				});	
			});		
        });
    </script>   
    <a href="#categoryModal" role="button" data-toggle="modal" id="add-post-category-btn" class="btn btn-small btn-link pull-right"><i class="icon icon-plus-sign"></i> Edit Categories</a>
</div>