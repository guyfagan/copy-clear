<aside id="sidebar" class="sidebar sidebar-expanded">	
	<h3><a href="#" data-toggle="collapse" data-target="#section-info">Category Info</a></h3>
    <div id="section-info" class="collapse in">
    	<div class="side-section">
    <?php
		$status = "draft";
		if(isset($category_data) && $edit_mode == true){
			if($category_data['category_status'] == 1){
				$status = "published";
			}//end if			
		}//end if
		if($perms[$module] > 3){
	?>
    		<strong>Status: </strong><select name="status" id="status" class="input-medium">
            	<option value="0"<?php 
				if(!isset($category_data) || (isset($category_data) && $category_data['category_status'] == "0")){
					echo ' selected="selected"';	
				}//end if
				?>>Draft</option>
                <option value="1"<?php
                if(isset($category_data) && $category_data['category_status'] == 1){
					echo ' selected="selected"';	
				}//end if
				?>>Published</option>
            </select><br />
    <?php
		} else {
	?>
    	<strong>Status: </strong><?php echo ucwords($status) ?><br /><br />
        <input type="hidden" name="status" id="status" value="<?php echo $category_data['category_status'] ?>" />
    <?php
		}//end if
	?>        
    	</div>
    </div>    
    <h3><a href="#" data-toggle="collapse" data-target="#section-attachments">Attachments</a></h3>
    <div id="section-attachments" class="collapse in">
    	<div class="side-section">
        	<h4>Default Image</h4>
            <small class="muted">Drag any image from the files list in the box below to set as default image</small>
            <div class="side-default-image-box" id="default-image-drop-in">            	
                <?php
				if($edit_mode == true){
					//getting the default image, if it's already set
					$default_img = $utils->get_attachments(array("post_id" => $id, "module" => $module, "limit" => 1, "role" => "default-image", 'attachment_post_type' =>'category'));
					if($default_img !== false){	
						echo '<div class="default-image">';		
						echo '<img src="'.__SERVERPATH__.'upload/photos/t/'.$default_img['attachment_filename'].'" alt="'.$default_img['attachment_friendly_name'].'" /><span>';
						echo $default_img['attachment_friendly_name'];						
						echo '</span>';
						echo '<a hrer="#" class="btn btn-danger btn-mini" id="remove-default-image" data-role="remove-default-image" data-post-file-id="'.$default_img['attachment_post_id'].'" data-file-id="'.$default_img['attachment_id'].'"><i class="icon-trash"></i></a>';
						echo '</div>';
					} else {
						echo '<span class="drag-here-label">Drag the photo here</strong>';
					}//end if
				} else {
					echo '<span class="drag-here-label">Drag the photo here</strong>';
				}//end if
				?>                
            </div>
            <h4>Files</h4>
            <?php
            require_once("media/media.post.attachments.php");
            ?>
            <a href="#mediaModal" role="button" data-toggle="modal" data-textarea="message" class="btn btn-mini media-btn"><i class="icon-picture"></i> Upload</a>
            <!--a href="#" class="btn btn-mini"><i class="icon-th"></i> Create New Collection</a-->
            <a href="#" class="btn btn-mini" id="sort-attachments-btn"><i class="icon-sort-by-attributes-alt"></i> Sort</a>
        </div>
    </div>
    <h3><a href="#" data-toggle="collapse" data-target="#section-posts">Related Posts Options</a></h3>
    <div id="section-posts" class="collapse">
    <?php		
		if($utils->view->settings['enableTemplates'] == 1){
			require_once("templates/category.force.template.php");
		}//end if	
	?>
    </div>
</aside>