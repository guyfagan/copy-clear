// JavaScript Document
$().ready(function(){
	(function( $ ){	
		
		var methods = {
			init : function( options ) {
				params = $.extend({
					listElement: "#revisions-history"	
				},options);				
				var $container = $(this);
				
				//autostart the function
				(function(){		
				console.log('revisions loaded');
					$(params.listElement).on('click','li > a',function(){
						var $href = location.href;
						if($href.indexOf('#') !== -1){
							$href = $href.split('#');
							$href = $href[0];	
						}
						$href = $href.split('?');
						$href = $href[0];
						var $revision_id = $(this).data('revision-id');
						location.href = $href + '?revision_id='+$revision_id;	
					});
				})();
			   
			}/*,
			
			loadPostRevs : function(){
				$.ajax({
					url:  $.config.apipath + 'post.get.revision',
					dataType:"json",
					data: "id="+$.cmsVars.post_id+"&module="+$.config.module,
					type:"post",
					success: function(data){		
						if(data.result == "success"){										
							if(typeof(data.tags) == "object"){
								for(var i = 0; i < data.tags.length; i++){
									$(params.tagCloud).append('<li><i class="icon-tag"></i> '+data.tags[i].tag_tag+'<a href="#" data-role-btn="delete-tag" data-tag="'+data.tags[i].tag_tag+'">x</a></li>');												
								}//end if
								methods.loadActions();
							}//end if
						}//end if
					}//end function
				});//end ajax
			}*/
		}
	
		$.fn.revisionsManager = function( method ) {    
			if ( methods[method] ) {
			  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
			  return methods.init.apply( this, arguments );
			} else {
			  $.error( 'Method ' +  method + ' does not exist on jQuery.tagManager' );
			}      
		};
	
	})( jQuery );

});