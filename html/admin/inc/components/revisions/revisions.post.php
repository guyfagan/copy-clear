<?php
	$tools = $utils->call("tools");	
	if(isset($id)){		
		$revs = $tools->get_post_revisions(array("post_id" => $id, "module_id" => $module,"full_details" => true));	
		
?>
<div id="revisions-manager">
	<ul id="revisions-history">
    <?php
		if($revs !== false){
			for($i = 0; $i < sizeof($revs); $i++){
				$backCounter = sizeof($revs) - $i;
				echo '<li><a href="#" data-revision-id="'.$revs[$i]['post_revision_id'].'">Revision #'.$backCounter.'</a><br />'.date("d/m/Y H:i:s",$revs[$i]['post_revision_date']).'</li>';
			}//end for i
		} else {
		}//end if
	?>	
    </ul>
</div>
<script>
	$().ready(function(){
		//$("#revisions-manager").revisionsManager();
	});
</script>
<?php
	}//end if
?>