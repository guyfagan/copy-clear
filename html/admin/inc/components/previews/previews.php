<!-- Modal -->
<div id="previewModal" class="modal modal-large hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel">Post Preview</h3>
    </div>
    <div class="modal-body">
       <iframe width="100%" height="100%" frameborder="0" id="preview-iframe"></iframe>
	</div> 
    <div class="modal-footer">
    	<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>  
</div>