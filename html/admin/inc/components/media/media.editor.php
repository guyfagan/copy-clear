<?php
	ob_start();
	session_start();	
	include("../../../config.inc.php");
	ini_set("display_errors",1);
	$curpath = "../../../../admin/";
	include("../../../classes/utils.class.php");	
	include("../../secure.inc.php");
	include("../../init.cms.inc.php");
	
	$tools = $utils->call('tools');
	
	$params = array();
	$file_id = $_GET['file_id'];
	$editor_id = $file_id;
	$params['file_id'] = $file_id;
	if(!is_null($_GET['module'])){
		$module = $_GET['module'];
		$params['module'] = $module;
		$formats = $utils->get_attachments_formats(array("module" => $module, 'default' => true));
	}//end if
	if(!is_null($_GET['post_id'])){
		$post_id = $_GET['post_id'];
		$params['post_id'] = $post_id;
		$editor_id .= '-'.$post_id;
	}//end if
	
	if(isset($_GET['attachment_post_type'])){
		$attachment_post_type = $_GET['attachment_post_type'];
		$params['attachment_post_type']	= $attachment_post_type;
	}//end if
		
	//media settings
	$mediaSettings = $utils->get_settings(array('module' => 'media'));	
		
	$file_data = $utils->get_attachment($params);
	if($file_data !== false){
		//generic for all the files
		
		//generic for all the files but with a post_id related
		if(isset($post_id)){
?>
<form action="#" method="post" name="attachment-form-info" id="attachment-form-info">
	<input type="hidden" name="module" id="module" value="<?php echo $module ?>" />
    <input type="hidden" name="id" id="id" value="<?php echo $post_id ?>" />
    <input type="hidden" name="file_id" id="file_id" value="<?php echo $file_id ?>" />
    <input type="hidden" name="attachment_post_type" id="attachment_post_type" value="<?php echo $attachment_post_type ?>" />
    <input type="hidden" name="post_file_id" id="post_file_id" value="<?php echo $file_data['attachment_post_id'] ?>" />
    <div class="attachment-info">
    	<div class="row-fluid">
            <div class="span6">
                <label>Title</label>
                <input type="text" name="title" id="title" value="<?php echo htmlentities($file_data['attachment_post_title']) ?>" />
                <label>Role</label>
                <select name="role" id="role">
                    <option value="generic"<?php 
                    if($file_data['attachment_post_role'] == "generic"){
                        echo ' selected';	
                    }//end if
                    ?>>Generic (system default)</option>
                    <option value="default-image"<?php 
                    if($file_data['attachment_post_role'] == "default-image"){
                        echo ' selected';	
                    }//end if
                    ?>>Default Image</option>
                <?php
                if(isset($mediaSettings['mediaRoles']) && $mediaSettings['mediaRoles'] != ""){
                    $extraRoles = explode(",",$mediaSettings['mediaRoles']);
                    if(is_array($extraRoles) && sizeof($extraRoles) > 0){
                        for($x = 0; $x < sizeof($extraRoles); $x++){
                            echo '<option value="'.trim($extraRoles[$x]).'"';
                            if($file_data['attachment_post_role'] == trim($extraRoles[$x])){
                                echo ' selected';	
                            }//end if
                            echo '>'.ucwords($extraRoles[$x]).'</options>';
                        }//end for x
                    }//end if
                }//end if
                ?>    
                </select> 
            </div>
            <div class="span6">
                <label>Caption</label>
                <textarea name="caption" id="caption" rows="3" cols="100"><?php echo htmlentities($file_data['attachment_post_caption']) ?></textarea>  
            </div>
        </div>
        <div class="clearfix">        	
           	<button class="btn btn-primary" type="button" id="attachment-form-submit">Save</button>              
    	</div>
    </div>	
</form>
<?		
		}//end if	
		//this is only for external urls
		if($file_data['attachment_type'] == "url"){
			echo '<h4>Attached video</h4>';
			//if it's an external url, we check which kind of service is
			switch($file_data['attachment_external_service']){
				case "vimeo":
					$vimeo = $utils->call("vimeo");
					$params = array(
						'url' => $file_data['attachment_url'],
						'width' => 640,
						'height' => 320
					);
					$video_pictures = $vimeo->get_video_thumbnails($file_data['attachment_url']);					
					$iframe = $vimeo->get_video_iframe($params);
				?>                
                <div class="video-player">
					<?php
                    echo $iframe;
                    ?>
                </div>
                <?php
					break;
				case "youtube":
					$youtube = $utils->call("youtube");
					$params = array(
						'url' => $file_data['attachment_url'],
						'width' => 640,
						'height' => 320
					);
					$video_pictures = $youtube->get_video_thumbnails($file_data['attachment_url']);						
					$iframe = $youtube->get_video_iframe($params);
				?>
                <div class="video-player">
					<?php
                    echo $iframe;
                    ?>
                </div>
                <?php
					break;
				case "amazonS3":
					$type = $utils->get_file_type(array('filename' => $file_data['attachment_filename']));
					$mime = $tools->get_mime_from_filename($file_data['attachment_filename']);
					$transcoded = (bool)$file_data['attachment_video_transcoded'];	
					if($type == "video"){					
                    ?>
                    <div class="row-fluid">
                    	<div class="span5" id="video-preview">
                            <video width="350" height="200" id="player" src="<?php echo $file_data['attachment_url'] ?>" type="<?php echo $mime ?>" controls></video>                        </div>
                        <div class="span7" id="transcode-content">
                        <?php						
                        if($transcoded){
							$transcoded_files = $utils->get_transcodes(array('file_id' => $file_id));							
							if($transcoded_files !== false){
							?>
                            <table class="table table-striped table-hover" id="transcodes-table">
                            	<thead>
                                	<tr>
                                    	<th>Thumb</th>
                                        <th>Filename</th>
                                    	<th>Format</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                            <?php	
								for($i = 0; $i < sizeof($transcoded_files); $i++){
                        ?>
                        			<tr>
                                    	<td><img src="<?php echo $transcoded_files[$i]['attachment_transcode_thumbnail'] ?>" alt="<?php echo $transcoded_files[$i]['attachment_transcode_filetype'] ?>"></td>
                                        <td><a href="#" data-mime="video/<?php echo $transcoded_files[$i]['attachment_transcode_filetype'] ?>" data-url="<?php echo $transcoded_files[$i]['attachment_transcode_uri'] ?>" data-rel="video-preview"><?php echo $transcoded_files[$i]['attachment_transcode_filename'] ?></a></td>
                                        <td><?php echo $transcoded_files[$i]['attachment_transcode_filetype'] ?></td>
                                    </tr>
                        <?php	
								}//end for i
						?>
	                        	</tbody>
                        	</table>
                        <?php
							} else {
						?>
                        	<div class="alert">This video has <strong>not</strong> been transcoded</div>
                            <button class="btn btn-primary" id="start-transcode" data-file-id="<?php echo $file_data['attachment_id'] ?>"><i class="icon icon-cogs"></i> Transcode this video now</button>                            
                        <?php		
							}//end if
                        } else {
                        ?>
                        	<div class="alert">This video has <strong>not</strong> been transcoded</div>
                            <button class="btn btn-primary" id="start-transcode" data-file-id="<?php echo $file_data['attachment_id'] ?>"><i class="icon icon-cogs"></i> Transcode this video now</button>                           
                        <?php	
                        }//end if
						?>
                        </div>
                    </div>  
                    <script>
						$(document).ready(function(){
							if($("#start-transcode").length > 0){
								$("#start-transcode").off('click');
								$("#start-transcode").on('click',function(e){
									e.preventDefault();									
									var $id = $(this).data('file-id');
									$.fn.mediaEditor('launchTranscode',$id);
								});
							}
							if($("#transcodes-table").length > 0){
								$("a[data-rel=video-preview]").off('click');
								$("a[data-rel=video-preview]").on('click',function(e){
									e.preventDefault();	
									var $uri = $(this).data('url');
									var $mime = $(this).data('mime');								
									$('#video-preview').html('<video width="350" height="200" id="player" src="'+$uri+'" type="'+$mime+'" controls></video> ');
									
								});
							}//end if
							$('audio,video').mediaelementplayer({success: function(me) {        
								me.pause();       
							},
							features:['playpause','progress','current','duration']
							});
						});
					</script>           
                    <?php	
					}//end if
					break;
			}//end switch
			if(isset($video_pictures)){
				if(is_array($video_pictures) && sizeof($video_pictures) > 0){
			
?>
<h3>Related thumbnail</h3>
<ul class="video-thumbs">
<?php
					foreach($video_pictures as $key => $value){						
						echo '<li><img src="'.$value.'" class="video-thumb-'.$key.'" alt="'.$key.'" /></li>';
					}//end foreach
?>
</ul>
<?php
				}//end if
			}//end if
		}//end if
		
		//this is only for images, and it shows all the different image formats
		if($file_data['attachment_type'] == "image"){
?>
<div id="photo-editor-<?php echo $editor_id ?>" class="photo-editor">
	<ul class="photo-tabs">
    	<!-- The first two are default format sizes -->
        <!--li><a href="#l" data-target="size-l">Large</a></li>
        <li><a href="#t" data-target="size-t">Thumbnail</a></li-->
        <!-- These are the enabled formats for the current module, if passed -->
        <?php
		if(isset($formats) && is_array($formats)){
			for($i = 0; $i < sizeof($formats); $i++){
				echo '<li><a href="#'.$formats[$i]['attachment_setting_folder'].'"';
				if($i == 0){
					echo ' class="active"';
				}//end if
				echo ' data-target="size-'.$formats[$i]['attachment_setting_folder'].'">'.$formats[$i]['attachment_setting_label'].'</a></li>';
			}//end for i
		}//end if
		?>
        <li><a href="#o" data-target="size-o">Original</a></li>
    </ul>
	<div class="photo-container">    	
        <?php
		if(isset($formats) && is_array($formats)){
			for($i = 0; $i < sizeof($formats); $i++){	
				$size = @getimagesize('../../../../upload/photos/'.$formats[$i]['attachment_setting_folder'].'/'.$file_data['attachment_filename']);	
				$showhide = 'hide';
				if($i == 0){
					$showhide = 'show';
				}//end if					
		?>
        	<div class="photo <?php echo $showhide ?> size-<?php echo $formats[$i]['attachment_setting_folder'] ?>">            	
                <div class="btn-group pull-right">
                	<a href="#rotateleft" class="btn btn-mini image-rotate" data-rotate="left" data-format="<?php echo $formats[$i]['attachment_setting_folder'] ?>"><i class="icon-undo"></i></a>
               		<a href="#rotateright" class="btn btn-mini image-rotate" data-rotate="right" data-format="<?php echo $formats[$i]['attachment_setting_folder'] ?>"><i class="icon-repeat"></i></a>
                    <!--a href="#move" class="btn btn-mini"><i class="icon-move"></i></a-->
                    <a href="#edit" class="btn btn-mini image-edit" data-format-id="<?php echo $formats[$i]['attachment_setting_id'] ?>" data-format="<?php echo $formats[$i]['attachment_setting_folder'] ?>"><i class="icon-edit"></i></a>
                </div>    
                <img src="<?php echo __SERVERPATH__ ?>upload/photos/<?php echo $formats[$i]['attachment_setting_folder'] ?>/<?php echo $file_data['attachment_filename'] ?>" alt="<?php echo $formats[$i]['attachment_setting_label'] ?>" data-real-width="<?php echo $size[0] ?>" data-real-height="<?php echo $size[1] ?>" />
                <br />
                <small class="alert alert-info"><i class="icon icon-picture"></i> <strong>Link: </strong><?php echo __SERVERPATH__ ?>upload/photos/<?php echo $formats[$i]['attachment_setting_folder'] ?>/<?php echo $file_data['attachment_filename'] ?></small>
                <span class="info"></span>
            </div>
        <?php
			}//end for i
		}//end if
		$size = @getimagesize('../../../../upload/photos/o/'.$file_data['attachment_filename']);
		?>
        <div class="photo hide size-o">            	
            <div class="btn-group pull-right">
                <a href="#rotateleft" class="btn btn-mini image-rotate" data-rotate="left" data-format="o"><i class="icon-undo"></i></a>
                <a href="#rotateright" class="btn btn-mini image-rotate" data-rotate="right" data-format="o"><i class="icon-repeat"></i></a>
                <!--a href="#move" class="btn btn-mini"><i class="icon-move"></i></a-->
                <!--a href="#edit" class="btn btn-mini image-edit" data-format-id="<?php echo $formats[$i]['attachment_setting_id'] ?>" data-format="o"><i class="icon-edit"></i></a-->
            </div>    
            <img src="<?php echo __SERVERPATH__ ?>upload/photos/o/<?php echo $file_data['attachment_filename'] ?>" alt="Original" data-real-width="<?php echo $size[0] ?>" data-real-height="<?php echo $size[1] ?>" />
            <br />
            <small class="alert alert-info"><i class="icon icon-picture"></i> <strong>Link: </strong><?php echo __SERVERPATH__ ?>upload/photos/o/<?php echo $file_data['attachment_filename'] ?></small>
            <span class="info"></span>
        </div>
    </div>
</div>
<div id="crop-tool-<?php echo $editor_id ?>" class="crop-tool clearfix">
</div>
<?php
		}//end if
	} else {
?>
<div class="alert alert-error">
    Cannot load the passed attachment.
</div>
<?php		
	}//end if
?>