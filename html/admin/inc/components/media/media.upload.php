<?php
	ob_start();
	session_start();	
	include("../../../config.inc.php");
	ini_set("display_errors",1);
	$curpath = "../../../../admin/";
	include("../../../classes/utils.class.php");	
	include("../../secure.inc.php");
	include("../../init.cms.inc.php");
?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CMS Component</title>
<!-- Bootstrap -->
<link href="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap.css" rel="stylesheet" media="screen" type="text/css" />
<link href="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap-responsive.css" rel="stylesheet" media="screen" type="text/css" />
<?php
if(!defined(__USE_CDN__) || __USE_CDN__ == false){
?>
<link href="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/css/ui-lightness/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.css" rel="stylesheet" media="screen" type="text/css" />
<script src="<?php echo __CMS_PATH__ ?>inc/jquery/jquery-<?php echo __JQUERY_VERSION__ ?>.min.js"></script>
<script src="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/js/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.js"></script>
<?php
} else {
?>
<!-- USING CDN FOR JQUERY UI CSS -->
<link href="http://code.jquery.com/ui/<?php echo __JQUERY_UI_VERSION__ ?>/themes/base/jquery-ui.css" rel="stylesheet" media="screen" type="text/css" />
<!-- USING CDN FOR JQUERY -->
<script src="http://code.jquery.com/jquery-<?php echo __JQUERY_VERSION__ ?>.js"></script>
<!-- USING CDN FOR JQUERY UI -->
<script src="http://code.jquery.com/ui/<?php echo __JQUERY_UI_VERSION__ ?>/jquery-ui.js"></script>
<?php
}//end if
?>
</head>
<body>
<?php
	//get the post data
	$module = $_POST['module'];
	$id = $_POST['id'];	
	$attachment_post_type = $_POST['attachment_post_type'];
	if($attachment_post_type == NULL){
		//default
		$attachment_post_type = 'post';	
	}//end if
	//get the module data
	if(!is_null($module)){
		$module_data = $utils->get_module($module);
		$module_id = $module_data['module_id'];
	}//end if
	
	if(!isset($_REQUEST['allowed'])){
		$allowed = '*';
	} else {
		$allowed = $_REQUEST['allowed'];
	}//end if
	
	//check if this file is allowed to be uploaded
	$can_upload = true;
	if($allowed != "*"){
		$filename = $_FILES['fileUpload']['name'];
		$ext = $utils->get_file_extension($filename);
		$allowed = explode(',',$allowed);
		if(!in_array($ext,$allowed)){
			$can_upload = false;	
		}//end if
	}//end if
	
	if($can_upload == true){
		//upload the file
		$upload = $utils->call('upload');
		$upload->add_date(true);
		$upload->set_file($_FILES['fileUpload']);
		$upload->set_destination("upload/","../../../../");
		if(!@file_exists("../../../../upload/")){
			@mkdir("../../../../upload/",0777);
		}//end if
		$uploaded = $upload->do_upload(0);
		if($uploaded){	
			$mime = $upload->get_mime();	
			$params = array("upload_class" => $upload);	
			$params['upload_path'] = "../../../../upload/";
			if(!is_null($id)){
				$params["post_id"] = $id;
				$params["module"] = $module_id;
				$params["attachment_post_type"] = $attachment_post_type;
			}//end if
			$params['size'] = $_FILES['fileUpload']['size'];
			$result = $utils->save_attachment($params);
			if($result === true){
				$filename = $upload->get_filename();
				$file_id = $utils->get_attachment_id();			
		?>
			<script>
				parent.uploadDone('<?php echo $file_id ?>');
			</script>    	
		<?php		
			} else {
		?>
		<div class="alert alert-error">
			Cannot upload the file on the server.
		</div>
		<a href="media.form.php" class="btn"><i class="icon-arrow-left"></i> Go Back</a>
		<?php	
			}//end if
		} else {
		?>
		<div class="alert alert-error">
			Cannot upload the file on the server.
		</div>
		<a href="media.form.php" class="btn"><i class="icon-arrow-left"></i> Go Back</a>
		<?php	
		}//end if
	} else {
		?>
		<div class="alert alert-error">
			The file you are trying to upload on the server is not allowed
		</div>
		<a href="media.form.php" class="btn"><i class="icon-arrow-left"></i> Go Back</a>
		<?php	
	}//end if
?>
</body>
</html>