<div id="sidebar-media-manager">
<?php
	if($_GET['load_cms'] == 1){
		session_start();
		ob_start();
		include("../../../config.inc.php");
		$curpath = "../../../../admin/";
		include("../../../classes/utils.class.php");	
		include("../../secure.inc.php");
		include("../../init.cms.inc.php");	
		
		$id = $_REQUEST['id'];
		$module = $_REQUEST['module'];
		$attachment_post_type = $_REQUEST['attachment_post_type'];
		if(is_null($attachment_post_type)){
			$attachment_post_type = 'post';	
		}
	}//end if
	
	//ensure we have a post id
	if(isset($id) && isset($module)){
		$attachments = $utils->get_attachments(array("post_id" => $id, "module" => $module, 'attachment_post_type' => $attachment_post_type));
		if($attachments != false){
			echo '<ul class="side-attachments-list">';
			for($i = 0; $i < sizeof($attachments); $i++){
				$attachment_title = $attachments[$i]['attachment_post_title'];
				if($attachment_title == ""){
					$attachment_title = $attachments[$i]['attachment_friendly_name']; 
				}//end if
				if($attachments[$i]['attachment_type'] == "image"){
					echo '<li class="clearfix" id="side-attachment-id-'.$attachments[$i]['attachment_id'].'" data-type="image"> 
					<img src="'.__SERVERPATH__.'upload/photos/t/'.$attachments[$i]['attachment_filename'].'" alt="Image" />';					
					echo $attachment_title;					
					echo '<div class="btn-group pull-right">
						<a href="#" title="Info" class="btn btn-mini" data-btn-role="info-attachment" data-toggle-id="'.$i.'"><i class="icon-info"></i></a>
						<a href="#" title="Edit" class="btn btn-mini" data-btn-role="edit-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-edit"></i></a>
						<a href="#" title="Delete" class="btn btn-mini btn-danger" data-btn-role="delete-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-trash icon-white"></i></a> 
					</div>';
					if(isset($attachments[$i]['attachment_post_role'])){
						echo '<div class="file-info" id="file-info-'.$i.'">';
						echo '<small class="muted">Type: '.$attachments[$i]['attachment_post_role'].'</small>';	
						echo '</div>';
					}
					echo '</li>';
				} else if($attachments[$i]['attachment_type'] == "file"){
					echo '<li class="clearfix" id="side-attachment-id-'.$attachments[$i]['attachment_id'].'" data-type="file"> 
					<i class="icon-file"></i>
					'.$attachment_title.'
					<div class="btn-group pull-right">
						<a href="'.__SERVERPATH__.'upload/files/'.$attachments[$i]['attachment_filename'].'" class="btn btn-mini" target="_blank"><i class="icon-share"></i></a>
						<a href="#" title="Edit" class="btn btn-mini" data-btn-role="edit-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-edit"></i></a>
						<a href="#" title="Delete" class="btn btn-mini btn-danger" data-btn-role="delete-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-trash icon-white"></i></a> 
					</div>
					</li>';
				} else if($attachments[$i]['attachment_type'] == "video"){
					echo '<li class="clearfix" id="side-attachment-id-'.$attachments[$i]['attachment_id'].'" data-type="video"> 
					<i class="icon-film"></i>
					'.$attachment_title.'
					<div class="btn-group pull-right">
						<a href="'.__SERVERPATH__.'upload/files/'.$attachments[$i]['attachment_filename'].'" class="btn btn-mini" target="_blank"><i class="icon-share"></i></a>
						<a href="#" title="Edit" class="btn btn-mini" data-btn-role="edit-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-edit"></i></a>
						<a href="#" title="Delete" class="btn btn-mini btn-danger" data-btn-role="delete-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-trash icon-white"></i></a>
						</div>
					</li>';
				} else if($attachments[$i]['attachment_type'] == "document"){
					echo '<li class="clearfix" id="side-attachment-id-'.$attachments[$i]['attachment_id'].'" data-type="document"> 
					<span class="pull-left">
					<i class="icon-file-text"></i>
					'.$attachment_title.'</span>
					<div class="btn-group pull-right">
						<a href="'.__SERVERPATH__.'upload/files/'.$attachments[$i]['attachment_filename'].'" title="View" class="btn btn-mini" target="_blank"><i class="icon-share"></i></a>
						<a href="#" title="Edit" class="btn btn-mini" data-btn-role="edit-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-edit"></i></a>
						<a href="#" title="Delete" class="btn btn-mini btn-danger" data-btn-role="delete-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-trash icon-white"></i></a> 
					</div>
					</li>';
				} else if($attachments[$i]['attachment_type'] == "url"){
					echo '<li class="clearfix" id="side-attachment-id-'.$attachments[$i]['attachment_id'].'" data-type="url"> 					
					<span class="file-label"><i class="icon-film"></i> '.$attachment_title.'
					<em class="muted">(source: '.ucwords($attachments[$i]['attachment_external_service']).')</em></span>
					<div class="btn-group pull-right">
						<a href="'.$attachments[$i]['attachment_url'].'" title="View" class="btn btn-mini" target="_blank"><i class="icon-share"></i></a>
						<a href="#" title="Edit" class="btn btn-mini" data-btn-role="edit-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-edit"></i></a>
						<a href="#" title="Delete" class="btn btn-mini btn-danger" data-btn-role="delete-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-trash icon-white"></i></a>
					</div>		
					</li>';
				} else {
					echo '<li class="clearfix" id="side-attachment-id-'.$attachments[$i]['attachment_id'].'" data-type="file"> 
					<i class="icon-file"></i>
					'.$attachment_title.'
					<div class="btn-group pull-right">
						<a href="'.__SERVERPATH__.'upload/files/'.$attachments[$i]['attachment_filename'].'" class="btn btn-mini" target="_blank"><i class="icon-share"></i></a>
						<a href="#" title="Edit" class="btn btn-mini" data-btn-role="edit-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-edit"></i></a>
						<a href="#" title="Delete" class="btn btn-mini btn-danger" data-btn-role="delete-attachment" data-file-id="'.$attachments[$i]['attachment_id'].'" data-post-file-id="'.$attachments[$i]['attachment_post_id'].'"><i class="icon-trash icon-white"></i></a> 
					</div>
					</li>';
				}//end if
				
			}//end for i
			echo '</ul>';
		?>
        <div class="modal hide fade" id="delete-file-modal">
            <div class="modal-header">
            	<strong>Confirm Delete</strong>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                
            </div>
            <div class="modal-body">
               <p><strong>Warning!</strong> The file will be delete from the system and it will not be available to the related posts</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-role="button" type="button" id="btn-side-confirm-delete">Yes, delete it</button>
           		<button class="btn" data-role="button" type="button" data-dismiss="modal" id="btn-cancel-delete">No, leave it</button>
            </div>
        </div>
        <script>
			$(document).ready(function() {
               $.cmsVars.postAttachments = JSON.parse('<?php echo addslashes(json_encode($attachments)) ?>'); 			  
            });			
		</script>
        <!--
        <div class="alert alert-block alert-error fade hide">  
        	<a class="close" data-dismiss="alert">×</a>  
        	<p><strong>Warning!</strong> The file will be delete from the system and it will not be available to the related posts</p>
            <button class="btn btn-danger" data-role="button" type="button" id="btn-side-confirm-delete">Yes, delete it</button>
            <button class="btn" data-role="button" type="button" data-dismiss="alert" id="btn-cancel-delete">No, leave it</button>
        </div>  
        -->
        <?php
		} else {
			echo '<div class="well well-small"><strong>Ooops!</strong> No attachments found</div>';
		}//end if
	}//end if
?>
</div>