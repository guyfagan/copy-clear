<?php
	if(!isset($mediaOptions)){
		$mediaOptions = array('upload' => true, 'library' => true,'allowed' => '*');	
	} else {
		//checking defaults
		if(!isset($mediaOptions['upload'])){
			$mediaOptions['upload'] = true;	
		}//end if
		if(!isset($mediaOptions['library'])){
			$mediaOptions['library'] = true;	
		}//end if
		if(!isset($mediaOptions['allowed'])){
			$mediaOptions['allowed'] = '*';	
		}//end if
	}//end if
?>
<div id="media-library" class="media-library media-library-collapsed">          
    <ul class="nav nav-tabs">
    	<?php
		if($mediaOptions['upload'] == true){
		?>
        <li class="active"><a href="#upload" data-toggle="tab">Upload</a></li>
        <?php
		}//end if
		if($mediaOptions['library'] == true){
		?>
        <li><a href="#library" data-toggle="tab">Media Library</a></li>    
        <?php
		}//end if
		?>
    </ul>
    <div class="tab-content">
    	<?php
		if($mediaOptions['upload'] == true){	
		?>
        <div class="tab-pane active" id="upload">
          	<div class="upload-section">
           		<!--iframe name="mediaUploadIframe" id="media-upload-iframe" src="<?php echo __CMS_PATH__ ?>inc/components/media/media.form.php?allowed=<?php echo $mediaOptions['allowed'] ?>" frameborder="0"></iframe-->  
            </div>
            <div class="upload-section">
            	<form action="#" name="upload-url" id="upload-url" method="post">
                	<label>Url</label>
                    <input type="text" class="input-xxlarge" name="url" id="url" />
                </form>
            </div>
        </div>
        <?php
		}//end if
		if($mediaOptions['library'] == true){
		?>
        <div class="tab-pane" id="library">
            <div class="gallery-box clearfix">
            	<div class="gallery-header clearfix">    
                	<strong>Filter:</strong>                
                    <div class="btn-group" data-toggle="buttons-radio">
                        <a href="#all" class="btn btn-mini active" data-btn-role="filter">Show All</a>
                        <a href="#image" class="btn btn-mini" data-btn-role="filter">Images</a>
                        <a href="#file" class="btn btn-mini" data-btn-role="filter">Files</a>          
                        <a href="#document" class="btn btn-mini" data-btn-role="filter">Documents</a> 
                        <a href="#video" class="btn btn-mini" data-btn-role="filter">Videos</a>                      
                        <a href="#url" class="btn btn-mini" data-btn-role="filter">Imported Urls</a> 
                    </div>
                    <p>&nbsp;</p>
                    <div class="input-append">
                        <input class="input-medium" id="search-media" name="search-media" type="text">
                        <button class="btn" type="button" id="btn-search-media">Search</button> 
                        <button class="btn" type="button" id="btn-reset-search">Reset</button>                               
                    </div>
                    <div class="btn-group pull-right" data-toggle="buttons-radio">
                    	<a href="#thumbs" class="btn active" data-btn-role="view-filter"><i class="icon-th-large"></i> Thumbs</a>
                        <a href="#list" class="btn" data-btn-role="view-filter"><i class="icon-th-list"></i> List</a>
                    </div>
                </div>
                <div class="gallery-container gallery-show-thumbs">
                
                </div>  
                <div class="gallery-side-info">                        	
                    
                </div>                 
            </div>                                                     
        </div>   
        <?php
		}//end if
		?>             
    </div>
</div>
<script>
	$().ready(function(){
		$settings.mediaSettings = {};
		<?php
		$mediaSettings = $utils->get_settings(array("module" => 'media'));
		echo '$settings.mediaSettings = '.json_encode($mediaSettings).';';		
		?>	
		$("#media-library").mediaManager({iframe_params : 'allowed=<?php echo $mediaOptions['allowed'] ?>'});
		$('.gallery-side-info').height($(".gallery-container").outerHeight(true));	
	});
</script>
