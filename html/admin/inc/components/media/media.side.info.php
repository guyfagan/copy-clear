<div class="attachment-file-info-container clearfix">
<?php
	if($_GET['load_cms'] == 1){
		/*ob_start();*/
		session_start();	
		include("../../../config.inc.php");
		$curpath = "../../../../admin/";
		include("../../../classes/utils.class.php");	
		include("../../secure.inc.php");
		include("../../init.cms.inc.php");		
		
	}//end if
	
	$post_id = $_GET['post_id'];
	$file_id = $_GET['file_id'];
	$module = $_GET['module'];	
	
	$settings = $utils->get_settings(array("module_id" => "media"));
	$img_preview_folder = $settings['mediaGalleryThumsFormat'];
	
	//$img_preview_folder = "t";
	$params = array("file_id" => $file_id);
	$params['filesize'] = true;
	$params['upload_path'] = "../../../../";
	$window_version = "mini";
	$params['get_posts'] = true;
	$params['get_categories'] = true;
	if(!is_null($post_id) && is_numeric($post_id)){
		//now that we have a post_id, we check if this attachment is owned by that post id
		$post_own_file = $utils->check_attachment_post_ownership(array("post_id" => $post_id, "file_id" => $file_id,"module" => $module));
		if($post_own_file){
			$params['post_id'] = $post_id;
			$params['module'] = $module;
			$params['get_posts'] = false;
			$params['get_categories'] = false;
			$window_version = "full";
		}//end if
	}//end if
	
	if(isset($_REQUEST['attachment_post_type'])){
		$params['attachment_post_type'] = $_REQUEST['attachment_post_type'];	
	}//end if
		
	$attachment = $utils->get_attachment($params);
	if($attachment == false){
		$params['attachment_post_type'] = 'category';
		$attachment = $utils->get_attachment($params);
	}//end if
	
	$is_img = false;
	if($attachment['attachment_type'] == "image"){
		$is_img = true;	
	}//end if
	
	if($window_version == "full"){
?>
<form action="<?php echo __CMS_PATH__ ?>api/save.attachment.details" method="post" name="attachmentDetails" id="attachment-details">
	<div class="attachment-details-header clearfix">
    	<h4>Attachment Details</h4>
        <div class="clearfix">
			<?php
                if($attachment['attachment_type'] == "image"){
            ?>
            <img src="<?php echo __SERVERPATH__ ?>upload/photos/<?php echo $img_preview_folder."/".$attachment['attachment_filename'] ?>" alt="<?php echo $attachment['attachment_friendly_name'] ?>" class="attachment-preview" />
            <?php	
				} else if($attachment['attachment_type'] == "url"){
			?>
            <img src="<?php echo $attachment['attachment_external_thumb'] ?>" alt="<?php echo $attachment['attachment_filename'] ?>" class="attachment-preview" />
            <?php	
                } else {
                    echo '<span class="attachment-document-icon"></span>';	
                }//end if
            ?>    	
            <h5><?php echo $attachment['attachment_friendly_name'] ?></h5>
            <h6><?php echo date("d/m/Y H:i",$attachment['attachment_date']) ?></h6>
            <h6><?php echo $attachment['filesize'] ?></h6>
        </div>
        <p><a href="#" data-btn-role="delete" class="btn btn-danger btn-mini">Delete this attachment</a></p>
        <div class="alert alert-block alert-error fade hide">  
        	<a class="close" data-dismiss="alert">×</a>  
        	<p><strong>Warning!</strong> The file will be delete from the system and it will not be available to the related posts</p>
            <button class="btn btn-danger" data-role="button" type="button" id="btn-confirm-delete">Yes, delete it</button>
            <button class="btn" data-role="button" type="button" data-dismiss="alert" id="btn-cancel-delete">No, leave it</button>
        </div>  
    </div>
    <div class="attachment-details-body clearfix">
    	<h4>Attachment Options</h4>
        <?php
			if($attachment['attachment_type'] == "image"){
		?>
        <select name="add-to-text-format" id="add-to-text-format" class="input-medium">
        	<option value="o">Original Image</option>
        <?php
			$formats = $utils->get_attachments_formats(array("module" => $module, 'default' => true));
			if($formats !== false){
				for($i = 0; $i < sizeof($formats); $i++){
					echo '<option value="'.$formats[$i]['attachment_setting_folder'].'">'.$formats[$i]['attachment_setting_label'];
					if($formats[$i]['attachment_setting_processing'] == 'crop'){
						echo ' ('.$formats[$i]['attachment_setting_width'].'px x '.$formats[$i]['attachment_setting_height'].'px)';
					} else {
						if($formats[$i]['attachment_setting_width'] != NULL){
							echo ' (width '.$formats[$i]['attachment_setting_width'].'px)';
						} else {
							echo ' (height '.$formats[$i]['attachment_setting_height'].'px)';
						}
					}
					echo '</option>';
				}//end for i
			}//end if
		?>	
        </select>       
        <?php
			}//end if
		?>
	    <button type="button" class="btn btn-active disabled" id="add-to-text" data-filename="<?php echo $attachment['attachment_filename'] ?>" data-type="<?php echo $attachment['attachment_type'] ?>">Add to text</button>
        <hr />
        <div id="side-role-config" class="clearfix">
            <h4>Attachment Role</h4>
            <label>Role</label>
            <select name="role" id="file-side-role">
                <option value="generic"<?php 
                if($attachment['attachment_post_role'] == "generic"){
                    echo ' selected';	
                }//end if
                ?>>Generic (system default)</option>
                <option value="default-image"<?php 
                if($attachment['attachment_post_role'] == "default-image"){
                    echo ' selected';	
                }//end if
                ?>>Default Image</option>
            <?php
            if(isset($settings['mediaRoles']) && $settings['mediaRoles'] != ""){
                $extraRoles = explode(",",$settings['mediaRoles']);
                if(is_array($extraRoles) && sizeof($extraRoles) > 0){
                    for($x = 0; $x < sizeof($extraRoles); $x++){
                        echo '<option value="'.trim($extraRoles[$x]).'"';
                        if($attachment['attachment_post_role'] == trim($extraRoles[$x])){
                            echo ' selected';	
                        }//end if
                        echo '>'.ucwords($extraRoles[$x]).'</options>';
                    }//end for x
                }//end if
            }//end if
            ?>    
            </select> 
            <button type="button" class="btn btn-active" id="change-role-btn" data-file-id="<?php echo $attachment['attachment_id'] ?>" data-post-file-id="<?php echo $attachment['attachment_post_id'] ?>" data-post-id="<?php echo $attachment['attachment_post_post_id'] ?>" data-module-id="<?php echo $attachment['attachment_post_module_id'] ?>" data-post-type="<?php echo $attachment['attachment_post_type'] ?>">Change Role</button>
    	</div>
    </div>
</form>
<?php
	} else {
?>
	<div class="attachment-details-header clearfix">
    	<h4>Attachment Details</h4>
        <div class="clearfix">
			<?php
                if($attachment['attachment_type'] == "image"){
            ?>
            <img src="<?php echo __SERVERPATH__ ?>upload/photos/<?php echo $img_preview_folder."/".$attachment['attachment_filename'] ?>" alt="<?php echo $attachment['attachment_friendly_name'] ?>" class="attachment-preview" />
            <?php	
				} else if($attachment['attachment_type'] == "url"){
			?>
            <img src="<?php echo $attachment['attachment_external_thumb'] ?>" alt="<?php echo $attachment['attachment_filename'] ?>" class="attachment-preview" />
            <?php						
                } else {
                    echo '<span class="attachment-document-icon"></span>';
                }//end if
            ?>    	
            <h5><?php echo $attachment['attachment_friendly_name'] ?></h5>
            <h6><?php echo date("d/m/Y H:i",$attachment['attachment_date']) ?></h6>
            <h6><?php echo $attachment['filesize'] ?></h6>
        </div>
        <p class="btn-group">        
        	<a href="#" data-btn-role="delete" class="btn btn-danger btn-mini">Delete</a>
        <?php   
            if(!is_null($post_id) && is_numeric($post_id)){
		?>    
			<a href="#" class="btn btn-active btn-mini" data-btn-role="add-to-post">Add to post</a>
            <a href="#" title="Edit" class="btn btn-mini" data-btn-role="edit-attachment" data-file-id="<?php echo $attachment['attachment_id'] ?>" data-post-file-id="<?php echo $attachment[$i]['attachment_post_id'] ?>"><i class="icon-edit"></i> Edit</a>
		<?php
			}//end if
		?>
        </p>
        <div class="alert alert-block alert-error fade hide" id="delete-alert">  
        	<a class="close" data-dismiss="alert">×</a>  
        	<p><strong>Warning!</strong> The file will be delete from the system and it will not be available to the related posts</p>
            <button class="btn btn-danger" data-role="button" type="button" id="btn-confirm-delete">Yes, delete it</button>
            <button class="btn" data-role="button" type="button" data-dismiss="alert" id="btn-cancel-delete">No, leave it</button>
        </div> 
        <div class="alert alert-block alert-info fade hide" id="add-post-alert">  
        	<a class="close" data-dismiss="alert">×</a>  
        	<p><strong>Info</strong> Do you want to add this attachment to the current post?</p>
            <button class="btn btn-primary" data-role="button" type="button" id="btn-confirm-add-post">Yes, add it</button>
            <button class="btn" data-role="button" type="button" data-dismiss="alert" id="btn-cancel-add-post">No, leave it</button>
        </div>   
    </div>
    <div class="attachment-details-body clearfix">
	<?php
		if($attachment['posts'] !== false){
			echo '<h4>Related posts</h4>';
			echo '<ul class="attachment-related-posts">';
			for($i = 0; $i < sizeof($attachment['posts']); $i++){
				$post_title = $attachment['posts'][$i]['post_title'];
				$post_id = $attachment['posts'][$i]['post_id'];			
				if(is_null($post_id)){
					$post_title = "Deleted post";	
					echo '<li>'.$post_title.' <a href="#" class="btn btn-mini btn-danger">Remove dead relation</a></li>';
				} else {
					echo '<li><i class="icon-file"></i> <a href="#">'.$post_title.'</a></li>';
				}//end ifs
				
			}//end if
			echo '</ul>';
		}//end if		
		if($attachment['categories'] !== false){
			echo '<h4>Related categories</h4>';
			echo '<ul class="attachment-related-posts">';
			for($i = 0; $i < sizeof($attachment['categories']); $i++){
				$category_name = $attachment['categories'][$i]['category_name'];
				$category_id = $attachment['categories'][$i]['category_id'];			
				if(is_null($category_id)){
					$category_name = "Deleted categories";	
					echo '<li>'.$category_name.' <a href="#" class="btn btn-mini btn-danger">Remove dead relation</a></li>';
				} else {
					echo '<li><i class="icon-th-list"></i> <a href="#">'.$category_name.'</a></li>';
				}//end ifs
				
			}//end if
			echo '</ul>';
		}//end if		
	?>
    </div>
<?php		
	}//end if
?>

<script>
	$().ready(function(){
		$.fn.mediaManager("deleteFile",'<?php echo $file_id ?>');	
		if($.cmsVars.post_id > 0){
			$.fn.mediaManager('addToPost','<?php echo $file_id ?>',$.cmsVars.post_id);
		}	
	});
</script>
</div>