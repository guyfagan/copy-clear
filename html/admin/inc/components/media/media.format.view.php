<?php
	if($_GET['load_cms'] == 1){
		ob_start();
		session_start();	
		include("../../../config.inc.php");
		$curpath = "../../../../admin/";
		include("../../../classes/utils.class.php");	
		include("../../secure.inc.php");
		include("../../init.cms.inc.php");	
		
		$id = $_GET['id'];
		$format_data = $utils->get_attachment_format(array("id" => $id));
	}//end if
	$width = (int)$format_data['attachment_setting_width'];
	if(is_numeric($width) && $width > 0){
		$width .= "px";	
	} else {
		$width = "auto";
	}//end if
	
	$height = (int)$format_data['attachment_setting_height'];
	if(is_numeric($height) && $height > 0){
		$height .= "px";	
	} else {
		$height = "auto";
	}//end if
	
	$default = $format_data['attachment_setting_default'];
	$default_label = "No";
	if($default == 1){
		$default_label = "Yes";
	}//end if
?>

	<div class="settings-image-format-view">
        <h5>
            <?php echo $format_data['attachment_setting_label'] ?> <span class="muted">/photos/<?php echo $format_data['attachment_setting_folder'] ?>/</span>
            <span class="pull-right">
                <a href="<?php echo __CMS_PATH__ ?>inc/components/media/media.format.edit.php?id=<?php echo $format_data['attachment_setting_id'] ?>&amp;module=media" class="btn btn-mini" data-btn-role="edit" data-target="#format-view-<?php echo $format_data['attachment_setting_id'] ?>">Edit</a> 
                <?php 
				if($format_data['attachment_setting_format_locked'] != "1"){
				?>
                <a href="#" class="btn btn-mini btn-danger" data-btn-role="delete" data-id="<?php echo $format_data['attachment_setting_id'] ?>" data-target="#format-view-<?php echo $format_data['attachment_setting_id'] ?>">Delete</a>
                <?php
				}//end if
				?>
            </span>
        </h5>
        <p>
            <strong>Width:</strong> <?php echo $width ?><br />
            <strong>Height:</strong> <?php echo $height ?><br />
            <strong>Processing:</strong> <?php echo ucwords($format_data['attachment_setting_processing']) ?><br />
            <strong>Default:</strong> <?php echo $default_label ?>
        </p>
    </div>
<?php
	if($_GET['load_cms'] == 1){
?>
<script>
	$().ready(function(){
		$("a[data-btn-role=edit]",$("#format-view-<?php echo $format_data['attachment_setting_id'] ?>")).mediaFormatEditBtn();		
		$("a[data-btn-role=delete]",$("#format-view-<?php echo $format_data['attachment_setting_id'] ?>")).mediaFormatDeleteBtn();		
	});
</script>
<?php		
	}//end if
?>