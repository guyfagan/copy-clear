<?php
	ob_start();
	session_start();	
	include("../../../config.inc.php");
	ini_set("display_errors",1);
	$curpath = "../../../../admin/";
	include("../../../classes/utils.class.php");	
	include("../../secure.inc.php");
	include("../../init.cms.inc.php");
	
	//Logger Class
	$logger = $utils->call("logger");
?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CMS Component</title>
<!-- Bootstrap -->
<link href="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap.css" rel="stylesheet" media="screen" type="text/css" />
<link href="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap-responsive.css" rel="stylesheet" media="screen" type="text/css" />
<?php
if(!defined(__USE_CDN__) || __USE_CDN__ == false){
?>
<link href="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/css/ui-lightness/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.css" rel="stylesheet" media="screen" type="text/css" />
<script src="<?php echo __CMS_PATH__ ?>inc/jquery/jquery-<?php echo __JQUERY_VERSION__ ?>.min.js"></script>
<script src="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/js/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.js"></script>
<?php
} else {
?>
<!-- USING CDN FOR JQUERY UI CSS -->
<link href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet" media="screen" type="text/css" />
<!-- USING CDN FOR JQUERY -->
<script src="http://code.jquery.com/jquery-<?php echo __JQUERY_VERSION__ ?>.js"></script>
<!-- USING CDN FOR JQUERY UI -->
<script src="http://code.jquery.com/ui/<?php echo __JQUERY_UI_VERSION__ ?>/jquery-ui.js"></script>
<?php
}//end if
?>
</head>
<body>
<?php
	//get the post data
	$module = $_POST['module'];
	$id = $_POST['id'];
	$attachment_post_type = $_POST['attachment_post_type'];
	if($attachment_post_type == NULL){
		//default
		$attachment_post_type = 'post';	
	}//end if
	//var_dump($id);
	//get the module data
	if(!is_null($module)){
		$module_data = $utils->get_module($module);
		$module_id = $module_data['module_id'];
	}//end if
	
	$url = filter_var($_POST['url'], FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
	$params = array();
	$params['url'] = $url;
	if(!is_null($id)){
		$params["post_id"] = $id;
		$params["module"] = $module_id;
		$params["attachment_post_type"] = $attachment_post_type;
	}//end if
	$result = $utils->attach_url($params);
	if($result !== false){
		$file_id = $utils->get_attachment_id();		
		$log_vars = array("login_area" => "private","type" => "media.import.url.successful","module_id" => $module,"type" => "media");
		if(!is_null($id)){
			$log_vars['post_id'] = $id;
		}//end if
		$logger->log_event($log_vars);
?>
	<script>
       parent.uploadDone('<?php echo $file_id ?>');
    </script>  
<?php
	} else {
		$log_vars = array("login_area" => "private","type" => "media.import.url.failed","module_id" => $module,"type" => "media");
		if(!is_null($id)){
			$log_vars['post_id'] = $id;
		}//end if
		$logger->log_event($log_vars);
?>
	<div class="alert alert-error">
    	Cannot Import the passed url.
    </div>
    <a href="media.form.php" class="btn"><i class="icon-arrow-left"></i> Go Back</a>
<?php		
	}//end if
?>
</body>
</html>