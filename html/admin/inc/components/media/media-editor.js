// JavaScript Document
$().ready(function(){
	(function( $ ){	
	
		var params;
		var $container;
		
		var methods = {
			init : function( options ) {
				params = $.extend({	
					saveDetailsForm : '#attachment-form-info'			
				},options);	
				$container = $(this);			
						
				//autostart the function
				(function(){				
					methods.saveDetailsInit();
				})();
			   
			},
			
			launchTranscode : function(file_id){
				    $("#transcode-content").html('<div class="alert">Processing...</div><br /><div class="progress progress-striped active"> \
												    <div class="bar" style="width: 100%;"></div>\
											      </div>');
				$.ajax({
					url: $.config.apipath + 'transcode.video?module='+$.config.module,
					data: 'file_id='+file_id,
					dataType:"json",
					type:'post',
					success: function(data){
						if(data.result == 'success'){
							$("#transcode-content").html('<div class="alert alert-info"><strong>Transcoding in progress:</strong> the transcode job has been launched successfully, but to transcode this file it might take a while, so please come back in a few minutes.</div>');
						}
					}
				});
			},
			
			saveDetailsInit : function(){
				if($(params.saveDetailsForm).length > 0){
					
					$(params.saveDetailsForm).submit(function(e){
						e.preventDefault();	
					});
					
					$("#attachment-form-submit").click(function(e){
						e.preventDefault();
						$.ajax({
							url : $.config.apipath + 'save.attachment.details',
							data: $(params.saveDetailsForm).serialize(),
							dataType:"json",
							type: "post",
							success: function(data){
								if(data.result == 'success'){
									$(params.saveDetailsForm + ' .attachment-info').popMessage("Attachment details saved",{type: "success"});	
								}
							}
						});	
					});
				}
			}
		}
		
		$.fn.mediaEditor = function( method ) {    
			if ( methods[method] ) {
			  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
			  return methods.init.apply( this, arguments );
			} else {
			  $.error( 'Method ' +  method + ' does not exist on jQuery.mediaEditor' );
			}      
		};
	
	})( jQuery );

});