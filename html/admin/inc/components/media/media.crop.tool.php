<?php
	ob_start();
	session_start();	
	include("../../../config.inc.php");
	ini_set("display_errors",1);
	$curpath = "../../../../admin/";
	include("../../../classes/utils.class.php");	
	include("../../secure.inc.php");
	include("../../init.cms.inc.php");
	
	
	$params = array();
	$file_id = $_GET['file_id'];
	$format_id = $_GET['format_id'];	
	$editor_id = $file_id;
	$params['file_id'] = $file_id;
	if(!is_null($_GET['module'])){
		$module = $_GET['module'];
		$params['module'] = $module;		
	}//end if
	$format = $utils->get_attachment_format(array("id" => $format_id));
	
	if($format['attachment_setting_processing'] == "crop"){
		$aspect_ratio = round($format['attachment_setting_width'] / $format['attachment_setting_height'],2);
	}//end if
	if(!is_null($_GET['post_id'])){
		$post_id = $_GET['post_id'];
		$params['post_id'] = $post_id;
		$editor_id .= '-'.$post_id;
	}//end if
	
	if(!isset($_GET['attachment_post_type'])){
		$params['attachment_post_type'] = 'post';
	} else {
		$params['attachment_post_type'] = $_GET['attachment_post_type'];
	}
		
	$file_data = $utils->get_attachment($params);
	if($file_data !== false){
		$size = @getimagesize('../../../../upload/photos/o/'.$file_data['attachment_filename']);	
		$max_width = 800;
		$max_height = 500;
		$image_screen_w = $size[0];
		$image_screen_h = $size[1];
		if($size[0] > $max_width){	
			$image_screen_w = $max_width;
			$image_screen_h = $max_width * ($size[1]/$size[0]);	
		} 
		
		if($size[1] > $max_height){		
			$image_screen_h = $max_height;
			$image_screen_w = $max_height * ($size[0]/$size[1]);		
		}//end if
		/*
		echo '<br />';
		echo $max_width;
		echo '<br />';
		echo $image_screen_w;
		echo '<br />';
		echo $image_screen_h;
		*/
?>
	<form action="#" name="crop-form" id="crop-form-<?php echo $file_id ?>" method="post" class="crop-form"> 
    	<input type="hidden" name="id" id="crop-form-val-id" value="<?php echo $post_id ?>" />
        <input type="hidden" name="file_id" id="crop-form-val-file-id" value="<?php echo $file_id ?>" />
        <input type="hidden" name="format_id" id="crop-form-val-format-id" value="<?php echo $format_id ?>" /> 
        <input type="hidden" name="module" id="crop-form-val-module" value="<?php echo $module ?>" /> 	
        <div class="crop-container clearfix">
            <div class="crop-preview">
                <img src="<?php echo __SERVERPATH__ ?>upload/photos/o/<?php echo $file_data['attachment_filename'] ?>" alt="Preview" width="<?php echo $image_screen_w ?>" height="<?php echo $image_screen_h ?>" data-real-width="<?php echo $size[0] ?>" data-real-height="<?php echo $size[1] ?>" class="crop-image" />
            </div>
        </div>
        <button type="submit" class="btn" id="save-crop">Save Crop</button>
        <button type="button" class="btn" id="discard-btn">Discard Changes</button>
    </form>
    <script>
		$().ready(function(){
			var imageData = {};
				
			$('.crop-image',$(this)).Jcrop({
				<?php
				if(isset($aspect_ratio)){
				?>
				aspectRatio : <?php echo $aspect_ratio ?>,	
				<?php
				}//end if
				?>
				onSelect : getCoords,
				onChange : getCoords
			});	
			
			function getCoords(coords){
				imageData.x = coords.x;
				imageData.y = coords.y;
				imageData.x2 = coords.x2;
				imageData.y2 = coords.y2;
				imageData.w = coords.w;
				imageData.h = coords.h;
				imageData.scaleRatio = parseInt($('.crop-preview img').data('real-width')) / parseInt($('.crop-preview img').width());
			
			}//end function
			
			$('.crop-form').submit(function(e){
				e.preventDefault();	
			});
			
			$('#save-crop').click(function(e){
				e.preventDefault();	
				if(imageData.length == 0){
					alert("Please select an area to crop first");	
				} else {
					var $format = '<?php echo $format['attachment_setting_folder'] ?>';
				//	console.log($('.crop-form').serialize() + '&crop=1&'+$.param(imageData));
					$.ajax({
						url : $.config.apipath + 'create.attachment.format',
						dataType:"json",
						type:'post',
						data: $('.crop-form').serialize() + '&crop=1&'+$.param(imageData),
						success: function(data){
							if(data.result == 'success'){
								$('.crop-tool').fadeOut("slow");
								$('.photo-editor').fadeIn("slow");	
								$('.attachment-info').fadeIn("slow");	
								$('.size-'+$format+' img').prop('src', $('.size-'+$format+' img').prop('src')+'?'+Math.random());
								$('.crop-tool').html('');
							}//end if
						}
					});
				}
			});
			
			$('#discard-btn').click(function(e){
				e.preventDefault();
				$('.crop-tool').fadeOut("slow");
				$('.photo-editor').fadeIn("slow");	
				$('.attachment-info').fadeIn("slow");	
				$('.crop-tool').html('');
			});
		});
	</script>
<?php
	} else {
		echo '<div class="alert alert-danger">Cannot find the image data</div>';
	}//end if
?>