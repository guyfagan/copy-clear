// JavaScript Document
$().ready(function(){
	
	/* THIS CODE MAKES THE TABLE SORTABLE */
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	(function( $ ){	
		
		var params;
		
		var methods = {
			init : function( options ) {
				params = $.extend({
					textarea: "message",
					post_id: 0,
					default_view : "thumbs",
					thumbs_view_class : 'gallery-show-thumbs',
					list_view_class : 'gallery-show-list',
					iframe_params : ''			
				},options);				
				var $container = $(this);
				var $formats;
				var $file_id;				
						
				//autostart the function
				(function(){
					
					var $currentTab = $(".nav-tabs .active a").attr("href");
					
					if(typeof($currentTab) == "undefined"){
						$currentTab = "#upload";	
					}
					
					params.attachmentPostType = $('#attachment-post-type').val();
					if(params.attachmentPostType == 'category'){
						params.post_id = $.cmsVars.category_id;
					} else {
						params.post_id = $.cmsVars.post_id;	
					}
					
					switch($currentTab){
						case "#upload":		
							$("#mediaModal .tab-pane").removeClass('active');
							//remove whoever was active
							$("#mediaModal .nav-tabs li").removeClass('active');	
							$("#mediaModal .nav-tabs li:eq(0)").addClass('active');	
							$("#upload").addClass("active");	
							methods.initUpload();						
							break;	
						case "#post-files":						
							methods.initGallery({related: true, tab: "#post-files"});	
							break;							
					}
					
					$(".nav-tabs a").click(function(e){
						e.preventDefault();
						switch($(this).attr("href")){
							case "#library":
								methods.initGallery({tab: "#library"});	
								methods.initSearchEngine();
								break;
							case "#post-files":						
								methods.initGallery({related: true, tab: "#post-files"});	
								break;										
						}
					});
					
				})();
			   
			},
			
			initUpload : function(){			
				$("#upload").html('<iframe name="mediaUploadIframe" id="media-upload-iframe" src="'+$.config.basepath+'admin/inc/components/media/media.form.php?'+params.iframe_params+'" frameborder="0"></iframe>');			
			},
			
			initSearchEngine : function(){
				if($('#btn-search-media').length > 0){
					$('#btn-search-media').unbind('click');
					$('#btn-search-media').click(function(e){
						e.preventDefault();
						var $key = $('#search-media').val();
						if($key !== ''){							
							methods.initGallery({tab: "#library", searchKey : $key});	
						} else {
							methods.initGallery({tab: "#library"});	
						}//end if
					});
				}
				
				if($('#btn-reset-search').length > 0){
					$('#btn-reset-search').click(function(e){
						e.preventDefault();	
						$('#search-media').val('');
						methods.initGallery({tab: "#library"});	
					});
				}
			},
					
			initGallery : function(options){
				var defaultStatus = $(".gallery-header a.active").attr("href").replace("#","");
				var galleryParams = $.extend({
										related: false, 
										tab:"#library", 
										show: defaultStatus,
										searchKey: "", 
										page: 1,
										onLoad : ''
									},options);			
				var $id = $.cmsVars.post_id;				
				var $module = $.config.module;	
				
				if(params.attachmentPostType != ""){
					if(params.attachmentPostType == "category"){
						var $id = $.cmsVars.category_id;	
					}
				}
							
				if(galleryParams.page == 1){						
					var $html = $('<ul id="gallery-list-wrapper" />');
				} else {				
					var $html = $('#gallery-list-wrapper');					
				}
				//reset whatever we have in the side
				$(".media-library .gallery-side-info").html("");
				
				$("#add-to-text").addClass('disabled');		
				

				
				if(galleryParams.related == false){
					var $apicall = $.config.apipath + 'get.attachments?filesize=1&page='+galleryParams.page+'&items_per_page='+$settings.mediaSettings.mediaGalleryItemsPerPage;
					if(galleryParams.show != "all"){
						$apicall += "&type="+galleryParams.show;
					}//end if
					if(galleryParams.searchKey != ""){
						$apicall += "&search="+encodeURIComponent(galleryParams.searchKey);
					}	
					/*
					if(typeof(params.attachmentPostType) != "undefined"){
						$apicall += "&attachment_post_type="+params.attachmentPostType;	
					} else {
						$apicall += "&attachment_post_type=all";	
					}
					*/
					$apicall += "&attachment_post_type=all";	
				} else {
					var $apicall = $.config.apipath + 'get.post.attachments?id='+$id+'&module='+$module+'&filesize=1'
					if(galleryParams.show != "all"){
						$apicall += "&type="+galleryParams.show;
					}//end if	
							
					if(typeof(params.attachmentPostType) != "undefined"){
						$apicall += "&attachment_post_type="+params.attachmentPostType;	
					}
				}//end if				
				
				$.ajax({
					//url: $.config.apipath + 'get.post.attachments?id='+$id+'&module='+$module,
					url: $apicall,
					dataType:"json",
					type: "POST",
					success: function(data){
						if(data.result == "success"){
							var $files = data.data;
							var $gallery_active = false;
							if($files != null){		
								$gallery_active = true;												
								for(var i = 0; i < $files.length; i++){
									//console.log(params.default_view);
									$li = $('<li />');
									$li.append('<input type="hidden" name="list-row-id[]" id="list-row-id-'+$files[i].attachment_id+'" value="'+$files[i].attachment_id+'" />');
									if($files[i].attachment_type == "image"){
										//this is an image
										$li.append('<a href="'+$.config.basepath+'upload/photos/o/'+$files[i].attachment_filename+'" id="attachment-link-'+$files[i].attachment_id+'" data-file-type="'+$files[i].attachment_type+'" data-file-id="'+$files[i].attachment_id+'"><img src="'+$.config.basepath+'upload/photos/t/'+$files[i].attachment_filename+'" /><span class="attachment-label">'+$files[i].attachment_friendly_name+'</span><span class="attachment-fileinfo">'+$files[i].filesize+'</span></a>');
									} else if($files[i].attachment_type == "file" || $files[i].attachment_type == "document" || $files[i].attachment_type == "video"){
										//anything else
										$li.append('<a href="'+$.config.basepath+'upload/files/'+$files[i].attachment_filename+'" id="attachment-link-'+$files[i].attachment_id+'" data-file-type="'+$files[i].attachment_type+'" data-file-id="'+$files[i].attachment_id+'" class="attachment-document-icon"><span>'+$files[i].attachment_friendly_name+'</span><span class="attachment-fileinfo">'+$files[i].filesize+'</span></a>');
									} else if($files[i].attachment_type == "url"){
										$li.append('<a href="'+$files[i].attachment_url+'" id="attachment-link-'+$files[i].attachment_id+'" data-file-type="'+$files[i].attachment_type+'" data-file-id="'+$files[i].attachment_id+'"><img src="'+$files[i].attachment_external_thumb+'" /><span class="attachment-label">'+$files[i].attachment_filename+' <em class="muted">(source: '+$files[i].attachment_external_service+')</em></span></a>');
									}
									$html.append($li);
								}//end for i						
							} else {		
								//if it's a normal list
								if(data.is_search == false){					
									$html.append('<li>No attachments found</li>');
								} else {
									//in case this is the result of a search, show this code
									$html.append('<li>No attachments found<br /> <button type="button" class="btn" data-role="reload-attachments"><i class="icon-refresh"></i> reload the gallery?</button></li>');	
								}
							}//end if
							
							if(typeof(galleryParams.onLoad) == "function"){
								galleryParams.onLoad.call(this);	
							}
							
							//clean up in case of previous loadings
							$('.file-loader-btn').parent().remove();
							
							if(typeof(data.paging_stats) != 'undefined'){								
								if(parseInt(data.paging_stats.current_page) < parseInt(data.paging_stats.pages)){
									var next_page = parseInt(data.paging_stats.current_page) + 1;
									$html.append('<li><a href="#loadmore" class="file-loader-btn" data-page="'+next_page+'"><i class="icon-plus-sign icon-3x"></i>Load More</li>');
								}
							}
						}//end if			
						//append the images to the gallery element
						$(galleryParams.tab+" .gallery-container").html($html);
						
						if(galleryParams.related == true && $('.gallery-show-list').length > 0){
							$(galleryParams.tab+" .gallery-container ul").wrap('<form action="#" name="file-form" id="file-form" method="post" />');
							$(galleryParams.tab+" .gallery-container ul").sortable({ 
								axis: "y",
							//	helper: fixHelper,
								stop: function( event, ui ) {
									$(galleryParams.tab+' .gallery-container #file-form').savePostAttachmentsSorting();
								}
							}).disableSelection();
						}
						
						//this code check if the button to reload the gallery exists, if so add the functionality
						if($(galleryParams.tab+" button[data-role=reload-attachments]").length > 0){
							$(galleryParams.tab+" button[data-role=reload-attachments]").unbind("click");
							$(galleryParams.tab+" button[data-role=reload-attachments]").click(function(e){
								e.preventDefault();	
								methods.initGallery({tab: "#library"});	
							});
						}
						
						//load more images button
						$('.file-loader-btn').unbind('click');
						$('.file-loader-btn').click(function(e){
							e.preventDefault();						
							var $trigger = $(this);
							methods.initGallery({tab: "#library",page: $(this).data('page'), onLoad: function(){
								$trigger.find('i').removeClass('icon-plus-sign').addClass('icon-refresh icon-spin');								
								$trigger.parent().remove();	
								$('.file-loader-btn').parent().remove();
							}});												
						});						
						
						//This code will filter the results
						$(galleryParams.tab+" a[data-btn-role=filter]").unbind('click');
						$(galleryParams.tab+" a[data-btn-role=filter]").click(function(e){
							e.preventDefault();
							var $filter = $(this).attr("href").replace("#","");
							$("#media-library").mediaManager("initGallery",{show: $filter});	
							/*if($filter != "all"){
								$(".gallery-container li a[data-file-type="+$filter+"]").parent().show();
								$(".gallery-container li a[data-file-type!="+$filter+"]").parent().hide();
							} else {
								$(".gallery-container li").show();
							}//end if	*/
							//methods.initGallery({tab: "#library", show: $filter});	
							if($(".gallery-container li:visible").length == 0){
								$(".gallery-container").popMessage('Nothing found',{type: 'info', incipit: 'Result:'})	
							} else {
								$(".gallery-container .alert").remove();
							}
							//reset whatever we have in the side
							$(".media-library .gallery-side-info").html("");		
						});
						
						//This code will show the different view modes for the gallery
						$(galleryParams.tab+" a[data-btn-role=view-filter]").unbind('click');
						$(galleryParams.tab+" a[data-btn-role=view-filter]").click(function(e){
							e.preventDefault();
							var $filter = $(this).attr("href").replace("#","");
							//$("#media-library").mediaManager("initGallery",{show: $(this).attr("href").replace("#","")});								
							if($filter == "thumbs"){
								$(galleryParams.tab+" .gallery-container").addClass(params.thumbs_view_class);
								$(galleryParams.tab+" .gallery-container").removeClass(params.list_view_class);
							} else if($filter == "list"){								
								$(galleryParams.tab+" .gallery-container").removeClass(params.thumbs_view_class);
								$(galleryParams.tab+" .gallery-container").addClass(params.list_view_class);
							}
							//reset whatever we have in the side
							$(".media-library .gallery-side-info").html("");		
						});
						
						if($gallery_active){
							//take control over the link of the images						
							$(galleryParams.tab+" li a").not('.file-loader-btn').unbind('click');
							$(galleryParams.tab+" li a").not('.file-loader-btn').click(function(e){
								e.preventDefault();
								var $offset = $(this).offset();	
								var $position = $(this).position();
								$(galleryParams.tab+" li a").removeClass('active');
								$(this).addClass('active');								
								var attachment_id = $(this).attr('data-file-id');									
								if(params.post_id > 0){	
									//we have a post_id so we show the full version of the preview			
									var $apicall = $.config.basepath+"admin/inc/components/media/media.side.info.php?module="+$.config.module+"&file_id="+attachment_id+"&post_id="+params.post_id+"&load_cms=1";
									if(galleryParams.related == true){
										$apicall += "&attachment_post_type="+params.attachmentPostType;	
									}//end if
									$(galleryParams.tab+" .gallery-side-info").load($apicall,
									function(){
										//console.log($("#mediaModal .modal-body").scrollTop());		
										var $sideoffset = $('.attachment-file-info-container').position();						
										if($("#mediaModal .modal-body").scrollTop() > $sideoffset.top){
											$('.attachment-file-info-container').css({top: parseInt($("#mediaModal .modal-body").scrollTop() - $sideoffset.top)});		
										}//end if	
										//$(galleryParams.tab).scrollTop();
										
										$(".attachment-details-header").off("click");
										$(".attachment-details-header").on("click","a[data-btn-role=edit-attachment]",function(e){
											e.preventDefault();										
											methods.editFile($(this));
										});
										
										if($("#add-to-text").hasClass('disabled')){
											$("#add-to-text").removeClass('disabled');	
										}
										$("#add-to-text").unbind('click');
										$("#add-to-text").click(function(e){
											e.preventDefault();
											//get the active image
											var $link = $(galleryParams.tab+" li .active");
											//if the type of the attachments is equal to an image, then we allow the user to add the image in the text
											if($(this).data('type') == "image"){
												var $imagePath = $.config.basepath + 'upload/photos/'+$("#add-to-text-format").val()+'/'+$(this).data('filename');
												if($link.length > 0){
													if($link.attr("data-file-type") == "image"){
														CKEDITOR.instances[params.textarea].insertHtml('<img src="'+$imagePath+'" data-format="'+$("#add-to-text-format").val()+'"/>');	
													} else {
														CKEDITOR.instances[params.textarea].insertHtml('<a href="'+$imagePath+'">'+$('span',$link).text()+'</a>');	
													}//end if
													$("#mediaModal").modal('hide');
												}
											}
										});
										
										$("#change-role-btn").unbind('click');
										$("#change-role-btn").click(function(e){
											e.preventDefault();
											//get the active image
											$file_id = $(this).data('file-id');
											$post_file_id = $(this).data('post-file-id');
											$post_id = $(this).data('post-id');
											$post_type = $(this).data('post-type');
											$module_id = $(this).data('module-id');
											$role = $('#file-side-role').val();
											$.ajax({
												url: $.config.apipath + 'set.post.attachment.type',
												dataType:"json",
												type:'post',
												data: 'post_id='+$post_id+'&file_id='+$file_id+'&post_file_id='+$post_file_id+'&role='+$role+'&attachment_post_type='+$post_type+'&module='+$.config.module,
												success: function(data){
													if(data.result == 'success'){
														$('#side-role-config').popMessage("Role saved!",{when: 'in',type: "success"});
													}
												}
											});
										});
									});
								} else {								
									//no post_id, so basic version
									$(".media-library .gallery-side-info").load($.config.basepath+"admin/inc/components/media/media.side.info.php?&file_id="+attachment_id+"&load_cms=1",function(){
										if($("#mediaModal .modal-body").length > 0){
											var $sideoffset = $('.attachment-file-info-container').offset();										
											if($("#mediaModal .modal-body").scrollTop() > $sideoffset.top){
												$('.attachment-file-info-container').css({top: parseInt($("#mediaModal .modal-body").scrollTop() - $sideoffset.top)});		
											}//end if
										} else {
											var $sideoffset = $('.attachment-file-info-container').position();										
											if($(document).scrollTop() > $sideoffset.top){
												$('.attachment-file-info-container').css({top: parseInt($(document).scrollTop() - $sideoffset.top)});		
											}//end if
										}
											
									});
								}//end if
							});		
							
						} else {
							$(galleryParams.tab+" .add-to-text").hide();
						}
					}
				});
			},//end initGallery
			
			postUploadFuncs : function(file_id){	
				$file_id = file_id;		
				var $loader = '<div class="progress progress-striped active">\
					<div class="bar" style="width: 0%;"></div>\
				</div><div class="alert alert-info" id="upload-progress-status">Processing...</div>';
				$("#upload").html($loader);
				//now we need to get some info about the file we just uploaded
				
				$.ajax({
					url : $.config.apipath+'get.attachment?module='+$.config.module+'&file_id='+file_id,
					dataType: "json",
					type: "post",
					success: function(data){
						if(data.result == "success"){
							//console.log(data.data.attachment_type);
							switch(data.data.attachment_type){
								case "image":
									//it is an image
									$("#upload-progress-status").html("Creating Thumbnails...");
									methods.resizeImage(file_id);
									break;
								case "video":
									//it is a video file
									$("#upload-progress-status").html("Saving video...");
									methods.postVideoUploadFuncs(file_id);	
									break;
								default:
									//it is not an image
									if(typeof(params.post_id) == "undefined"){								
										$('#media-library .nav-tabs a[href=#library]').tab('show');
										methods.initGallery();
									} else {							
										$('#media-library .nav-tabs a[href=#post-files]').tab('show');	
										methods.initGallery({related: true, tab: "#post-files"});	
									}
									
									methods.initUpload();
							}//end if
						} else {
							$('#media-library').popMessage("something went wrong while retrieving the attachment information",{type: "error"});
						}//end if
					}
				});
				
			},//end function
			
			postVideoUploadFuncs : function(file_id){
				//retrieve the formats
				$(".progress .bar").css({"width" : "50%"});	
				$.ajax({
					url : $.config.apipath+'video.post.upload.actions?module='+$.config.module,
					dataType:"json",
					type: "post",	
					data: 'file_id='+file_id,		
					success: function(data){
						if(data.result == "success"){	
							$(".progress .bar").css({"width" : "100%"});	
							//show the gallery
							window.setTimeout(function(){
								//$('#media-library .nav-tabs a[href=#library]').tab('show');											
								if(typeof(params.post_id) == "undefined"){							
									$('#media-library .nav-tabs a[href=#library]').tab('show');
									methods.initGallery();
								} else {
									$('#media-library .nav-tabs a[href=#post-files]').tab('show');	
									methods.initGallery({related: true, tab: "#post-files"});	
								}
								methods.initUpload();
							}, 1000);							
						}//end if
					}
				});	
			},
			
			resizeImage : function(file_id){
				//retrieve the formats
				$.ajax({
					url : $.config.apipath+'get.media.formats?module='+$.config.module+'&default=true',
					dataType:"json",
					type: "post",			
					success: function(data){
						if(data.result == "success"){
							methods.resizeOriginalIfNeeded(file_id);
							$formats = data.formats;						
							if($formats.length > 0){							
								for(i = 0; i < $formats.length; i++){
									$formats[i].sized = false;
									methods.createThumb(file_id,$formats[i],i);								
								}//end for i							
							}//end if
						}//end if
					}
				});	
			},
			
			resizeOriginalIfNeeded: function(file_id){
				$.ajax({
					url : $.config.apipath+"resize.original?module="+$.config.module,
					type: "post",
					dataType:"json",
					data: "file_id="+file_id,
					success: function(data){
						console.log(data);
					}
				});		
			},
			
			//call the API to create a copy of the image for the format passed
			createThumb : function(file_id, format, counter_id){		
				$.ajax({
					url : $.config.apipath+"create.attachment.format?module="+$.config.module,
					type: "post",
					dataType:"json",
					data: "file_id="+file_id+"&format_id="+format.attachment_setting_id+"&id="+$.cmsVars.post_id,
					success: function(data){
						//this should update a function that updated the status bar
						methods.updateStatusBar(counter_id);
					}
				});		
				return true;	
			},//end function
			
			updateStatusBar : function(counter_id){
				$formats[counter_id].sized = true;
				var itemPerc = 100/$formats.length;
				var totalToSize = $formats.length;
				var sized = 0;
				for(var i = 0; i < $formats.length; i++){
					if($formats[i].sized){
						sized++;
					}
				}			
				
				$(".progress .bar").css({"width" : (itemPerc*(sized))+"%"});
				
				if(totalToSize == sized){
					//at this point we assume that all the sizes for the uploaded image has been created, so we go to the next screen	
					//methods.showImageManager();		
					//show the gallery
					window.setTimeout(function(){
						//$('#media-library .nav-tabs a[href=#library]').tab('show');											
						if(typeof(params.post_id) == "undefined"){							
							$('#media-library .nav-tabs a[href=#library]').tab('show');
							methods.initGallery();
						} else {
							$('#media-library .nav-tabs a[href=#post-files]').tab('show');	
							methods.initGallery({related: true, tab: "#post-files"});	
						}
						methods.initUpload();
					}, 1000);
				}
			},//end function
			
			addToPost : function(file_id, post_id){
				$file_id = file_id;
				$post_id = post_id;
				$('.attachment-details-header a[data-btn-role=add-to-post]').click(function(){					
					$("#add-post-alert").fadeIn("slow").addClass('in');					
				});
				
				$("#btn-confirm-add-post").click(function(e){
					e.preventDefault();
				//	console.log("I'm going to add");
					$.ajax({
						url : $.config.apipath+"add.attachment.to.post",
						data: "file_id="+$file_id+"&post_id="+$post_id+"&module="+$.config.module,
						dataType:"json",
						type: "post",
						success: function(data){
							if(data.result == "success"){
								$('#media-library .nav-tabs a[href=#post-files]').tab('show');	
								methods.initGallery({related: true, tab: "#post-files"});	
							}
						}
					});	
				});
			},
			
			deleteFile : function(file_id){
				$file_id = file_id;
				$('.attachment-details-header a[data-btn-role=delete]').click(function(){
					$("#delete-alert").fadeIn("slow").addClass('in');
				});
				
				$("#btn-confirm-delete").click(function(){
					var $post_data = "file_id="+$file_id;
					//console.log(params.attachmentPostType);
					if(!isNaN(params.post_id)){
						$post_data += '&post_id='+params.post_id+'&module='+$.config.module+'&attachment_post_type='+params.attachmentPostType;				
					}
					
					$.ajax({
						url : $.config.apipath+"delete.attachment",
						data: $post_data,
						dataType:"json",
						type: "post",
						success: function(data){
							if(data.result == "success"){
								$("#media-library").popMessage("The file has been successfully deleted",{type: "success"});
								//empty the gallery side info
								$(".gallery-side-info").fadeOut("slow",function(){
									$(this).html("").show();										
								});
								//remove the thumbnail from the gallery
								$("#attachment-link-"+$file_id).fadeOut("slow",function(){
									$(this).remove();	
								});
							} else {
								$("#media-library").popMessage("Something went wrong, file not deleted",{type: "error"});
							}//end if
						}
					});
				});
			},
			
			sidebarManager : function(options){
				//this function will handle the requests from the sidebar attachments
				var sidebarParams = $.extend({},options);
				$(".side-attachments-list").off("click");
				$(".side-attachments-list").on("click","li a[data-btn-role=delete-attachment]",function(e){
					e.preventDefault();
					methods.deleteSideFile($(this).data('file-id'));
				});
				
				$("#default-image-drop-in").on('click','#remove-default-image',function(e){
					e.preventDefault();
					var $file_id = $(this).data('file-id');
					var $post_file_id = $(this).data('post-file-id');		
					
					var attachmentPostType = $('#attachment-post-type').val();				
					var post_id = $("#id").val();
							
					var $post_data = "file_id="+$file_id+"&post_id="+post_id+"&module="+$.config.module+"&post_file_id="+$post_file_id+"&role=generic";
					if(typeof(attachmentPostType) != "undefined"){
						$post_data += "&attachment_post_type="+attachmentPostType;	
					}//end if
					//console.log($post_data);
					$.ajax({
						url : $.config.apipath+"set.post.attachment.type",
						//data: "file_id="+$file_id+"&post_id="+$.cmsVars.post_id+"&module="+$.config.module+"&post_file_id="+$post_file_id+"&role=generic",
						data: $post_data,					
						dataType:"json",
						type: "post",
						success: function(data){
							if(data.result == "success"){ 								
								$('.default-image').fadeOut("slow",function(){
									$('.default-image').html('<span class="drag-here-label">Drag the photo here</strong>');
									$('.default-image').show();	
								});
							} else {
								//error handling
								console.log(data.error);
							}
						}
					});
				});
				
				$("#sort-attachments-btn").on('click',function(e){
					e.preventDefault();	
					
					$("#mediaModal").modal('show');	
					$("#mediaModal .tab-pane").removeClass('active');
					$("#mediaModal .tab-pane:last-child").addClass('active');
					//remove whoever was active
					$("#mediaModal .nav-tabs li").removeClass('active');
					$("#mediaModal .nav-tabs li.post-files-tab").addClass('active');
					methods.init();
				});
				
				$(".side-attachments-list").on("click","li a[data-btn-role=edit-attachment]",function(e){
					e.preventDefault();
					methods.editFile($(this));
				});
				
				$(".side-attachments-list").on("click","li a[data-btn-role=info-attachment]",function(e){
					e.preventDefault();					
					$("#file-info-"+$(this).data('toggle-id')).toggle();
				});
				
				
				$(".side-attachments-list li[data-type='image']").draggable({ 
					revert: true 
				});
				$("#default-image-drop-in").droppable({
					drop: function( event, ui ) {
						//console.log(ui.draggable.data('type'));
						var $file_id = ui.draggable.find('a[data-btn-role=delete-attachment]').data('file-id');
						var $post_file_id = ui.draggable.find('a[data-btn-role=delete-attachment]').data('post-file-id');
						var $thumb_path = ui.draggable.find('img').prop('src');
						var $drop_html = '<img src="'+$thumb_path+'" alt="Default Image" />';
						var $drop_area = $(this);
						
						var attachmentPostType = $('#attachment-post-type').val();				
						var post_id = $("#id").val();
						
						/*$( this )
						//.addClass( "ui-state-highlight" )
						.addClass('side-default-image-box-dropped')
						.html( $dropHtml  );*/
						if(attachmentPostType == 'category'){
							$post_data = "file_id="+$file_id+"&post_id="+$.cmsVars.category_id+"&module="+$.config.module+"&post_file_id="+$post_file_id+"&role=default-image&attachment_post_type=category";
						} else {
							if(typeof(attachmentPostType) != 'undefined'){
								$post_data = "file_id="+$file_id+"&post_id="+$.cmsVars.post_id+"&module="+$.config.module+"&post_file_id="+$post_file_id+"&role=default-image&attachment_post_type="+attachmentPostType;
							} else {
								$post_data = "file_id="+$file_id+"&post_id="+$.cmsVars.post_id+"&module="+$.config.module+"&post_file_id="+$post_file_id+"&role=default-image&attachment_post_type="+attachmentPostType;
							}
						}
						
						$.ajax({
							url : $.config.apipath+"set.post.attachment.type",
							data: $post_data,
							dataType:"json",
							type: "post",
							success: function(data){
								if(data.result == "success"){ 
									//console.log("confirmed");
									$drop_html = '<div class="default-image">' + $drop_html;
									$drop_html += '<span>'+data.attachment.attachment_friendly_name+'</span>';
									$drop_html += '<a hrer="#" class="btn btn-danger btn-mini" id="remove-default-image" data-role="remove-default-image" data-post-file-id="'+$post_file_id+'" data-file-id="'+$file_id+'"><i class="icon-trash"></i></a>';	
									$drop_html += '</div>';
									$drop_area.addClass('side-default-image-box-dropped').html( $drop_html);
								} else {
									//error handling
									console.log(data.error);
								}
							}
						});
					}
				});
			},
			
			editFile : function(trigger){
				methods.init();							
				if(params.attachmentPostType !== ""){
					if(params.attachmentPostType == 'category'){
						var $post_id = $.cmsVars.category_id;
						params.post_id = $post_id;
					} else {
						var $post_id = $.cmsVars.post_id;
						params.post_id = $post_id;	
					}
				} else {
					var $post_id = $.cmsVars.post_id;
					params.post_id = $post_id;	
				}
				
				var $file_id = trigger.data('file-id');
				var $post_file_id = trigger.data('post-file-id');					
				$("#mediaModal").modal('show');	
				$("#mediaModal .tab-pane").removeClass('active');
				//remove whoever was active
				$("#mediaModal .nav-tabs li").removeClass('active');
				var $newTab;
				if($('#file-tab-'+$post_file_id).length == 0){
					$newTab = $('<div class="tab-pane active" id="edit-tab-'+$post_file_id+'" />');									
					$("#mediaModal .tab-content").append($newTab);
					$("#mediaModal .nav-tabs").append('<li class="active"><a href="#edit-tab-'+$post_file_id+'" data-toggle="tab" id="file-tab-'+$post_file_id+'">Edit File</a></li>');
				} else {
					$newTab = $('#edit-tab-'+$post_file_id);
				}//end if
				
				$apicall = $.config.basepath+'admin/inc/components/media/media.editor.php?post_id='+$post_id+'&file_id='+$file_id+'&module='+$.config.module;
				if(params.attachmentPostType != ''){
					$apicall += '&attachment_post_type='+params.attachmentPostType;	
				}//end if
				
				$newTab.load($apicall,function(){
					$('#file-tab-'+$post_file_id).tab('show');
					$('#edit-tab-'+$post_file_id).mediaEditor();
					if($('#edit-tab-'+$post_file_id + ' .photo-editor').length > 0){
						$('#edit-tab-'+$post_file_id + ' .photo-editor').photoEditor();
					}//end if
				});	
				
				$("#mediaModal").on('hidden',function(){
					//console.log($post_file_id);
					if($('#edit-tab-'+$post_file_id + ' .photo-editor').length > 0){
						$('#edit-tab-'+$post_file_id + ' .photo-editor').photoEditor("destroy");
					}//end if
					$('#edit-tab-'+$post_file_id).remove();
					$('#file-tab-'+$post_file_id).parent().remove();
				});//end function	
			},
			
			deleteSideFile : function(file_id){
				$file_id = file_id;	
				
				var attachmentPostType = $('#attachment-post-type').val();				
				var post_id = $("#id").val();
				
		
				//$(".alert").fadeIn("slow").addClass('in');
				$("#delete-file-modal").modal('show');
				$("#btn-side-confirm-delete").off("click");
				$("#btn-side-confirm-delete").on("click",function(){
					var $data = "file_id="+$file_id;
									
					if(!isNaN(post_id)){
						$data += '&post_id='+post_id+'&module='+$.config.module;
					}
					
					if(attachmentPostType !== ""){
						$data += '&attachment_post_type='+attachmentPostType;	
					}
					
					$.ajax({
						url : $.config.apipath+"delete.attachment",
						data: $data,
						dataType:"json",
						type: "post",
						success: function(data){							
							if(data.result == "success"){
								//remove the thumbnail from the gallery
								$("#side-attachment-id-"+$file_id).fadeOut("slow",function(){
									$(this).remove();
									//$(".alert").hide();	
									$("#delete-file-modal").modal('hide');
								});
							} else {
								$("#sidebar-media-manager").popMessage("Something went wrong, file not deleted",{type: "error"});
							}//end if
						}
					});
				});
			},
			
			updateSidebar : function(options){
				//this function update the content of the sidebar attachments
				var sidebarParams = $.extend({},options);
				var $id = $.cmsVars.post_id;
				if(params.attachmentPostType != ""){
					if(params.attachmentPostType == "category"){
						var $id = $.cmsVars.category_id;	
					}
				}
				var $apicall = $.config.basepath+"admin/inc/components/media/media.post.attachments.php?id="+$id+"&module="+$.config.module+"&load_cms=1";
				if(params.attachmentPostType != ""){
					$apicall += "&attachment_post_type="+params.attachmentPostType;
				}
				$("#sidebar-media-manager").load($apicall,function(){
					methods.sidebarManager();
				});
				
			}
			
		}
	
		$.fn.mediaManager = function( method ) {    
			if ( methods[method] ) {
			  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
			  return methods.init.apply( this, arguments );
			} else {
			  $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
			}      
		};
	
	})( jQuery );

});

function uploadDone(id){	
	if(typeof($.cmsVars.media.postUploadCallback) == 'undefined'){
		$.fn.mediaManager('postUploadFuncs',id);
	} else {
		if($.isFunction($.cmsVars.media.postUploadCallback)){
			$.cmsVars.media.postUploadCallback(id);
		}		
	}
}