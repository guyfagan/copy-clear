<?php
	ob_start();
	//ini_set("display_errors",1);
	session_start();	
	include("../../../config.inc.php");
	$curpath = "../../../../admin/";
	include("../../../classes/utils.class.php");	
	include("../../secure.inc.php");
	include("../../init.cms.inc.php");	
	
	if(!isset($_REQUEST['allowed'])){
		$allowed = '*';	
	} else {
		$allowed = $_REQUEST['allowed'];//this should be a comma separated list of extensions, like "jpg,doc,gif"
		$allowed_list = explode(',',$allowed);
	}//end if
	
?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo __CMS_NAME__ ?></title>
<!-- Bootstrap -->
<link href="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<?php echo __CMS_PATH__ ?>css/screen.css" rel="stylesheet" media="screen">
<link href="<?php echo __CMS_PATH__ ?>css/media.css" rel="stylesheet" media="screen">
<link href="<?php echo __CMS_PATH__ ?>inc/fontawesome/<?php echo __FONTAWESOME_VERSION__ ?>/css/font-awesome.min.css" rel="stylesheet" media="screen" type="text/css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link href="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/css/ui-lightness/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.css" rel="stylesheet" media="screen" type="text/css" />
<script src="<?php echo __CMS_PATH__ ?>inc/jquery/jquery-<?php echo __JQUERY_VERSION__ ?>.min.js"></script>
<script src="<?php echo __CMS_PATH__ ?>inc/jqueryUI/<?php echo __JQUERY_UI_VERSION__ ?>/js/jquery-ui-<?php echo __JQUERY_UI_VERSION__ ?>.custom.min.js"></script>
<script src="<?php echo __CMS_PATH__ ?>inc/bootstrap/<?php echo __BOOTSTRAP_VERSION__ ?>/js/bootstrap.min.js"></script>
<script src="<?php echo __CMS_PATH__ ?>js/config.js.php?m=<?php echo $utils->view->module ?>&a=<?php echo $utils->view->action ?>"></script>
<script src="<?php echo __CMS_PATH__ ?>js/common.js"></script>
<script src="<?php echo __CMS_PATH__ ?>inc/jloader/jloader.jquery.js"></script>
<style>
	body{
		margin: 0;
		padding: 0;
		background-color: #FFF;	
	}
</style>
</head>

<body>
<form action="media.upload.php" method="post" enctype="multipart/form-data" name="media-upload" id="media-upload">	
    <input type="hidden" name="id" id="post-id" value="">
    <input type="hidden" name="module" id="post-module" value="">
    <input type="file" name="fileUpload" id="file-upload">   
    <input type="hidden" name="attachment_post_type" id="post-type" value="">
    <input type="hidden" name="allowed" id="allowed" value="<?php echo $allowed ?>">
    <h2>Upload a file from your computer</h2>
    <?php
	if(!isset($allowed_list)){
	?>
    <small class="muted">Supported images types: Jpeg, Png &amp; Gif</small>
    <?php
	} else {
	?>
    <small class="muted">Allowed files: <?php
    for($i = 0; $i < sizeof($allowed_list); $i++){
		$allowed_list[$i] = '.'.$allowed_list[$i];
	}//end for i
	echo implode(',',$allowed_list);
	?></small>
    <?php	
	}//end if
	?>
    <div class="input-append input-island">
        <input type="text" name="fileUploadMask" id="file-upload-mask" class="input-large" />
        <button type="button" id="browse-btn" class="btn">Browse</button>
    </div>
    <button type="submit" class="btn btn-primary" id="upload-btn">Upload</button>
</form>
<form action="media.import.php" method="post" name="media-import" id="media-import">
	<input type="hidden" name="id" id="post-id-2" value="" />
    <input type="hidden" name="module" id="post-module-2" value="" />
    <input type="hidden" name="attachment_post_type" id="post-type-2" value="" />
	<h2>Add an external link</h2>
    <small class="muted">Supported services: Vimeo, Youtube &amp; Amazon S3</small>
    <div class="input-append input-island">
    	<input type="text" name="url" id="url" class="input-xlarge" />
        <span class="add-on"><i class="icon-globe"></i></span>
    </div>
    <button type="submit" class="btn btn-primary" id="import-btn">Import</button>
</form>
<script>
	$().ready(function(){
		$("#browse-btn").click(function(e){
			e.preventDefault();
			$('input[id=file-upload]').click();	
		});
		
		$("#file-upload").hide();
		$("#file-upload").change(function(){
			$("#file-upload-mask").val($(this).val());	
		});
		
		$("#post-id").val(parent.$("#id").val());	
		$("#post-module").val(parent.$("#module").val());		
		$("#post-id-2").val(parent.$("#id").val());	
		$("#post-module-2").val(parent.$("#module").val());
		
		if(parent.$("#attachment-post-type").length > 0){
			$("#post-type").val(parent.$("#attachment-post-type").val());
			$("#post-type-2").val(parent.$("#attachment-post-type").val());
		} else {
			//default to post
			$("#post-type").val('post');
			$("#post-type-2").val('post');
		}//end if
		
		$("#upload-btn").click(function(e){
			e.preventDefault();			
			$("#media-upload").submit();
			$('#media-upload').click(function(e){
				e.preventDefault();	
				$(this).jLoader({
					backdropOpacity: .7,
					backdropColor: '#FFF',
					content: '<span class="loading"><i class="icon-refresh icon-spin"></i> Uploading your file...</span>'	
				});
			});
		});
		
		$("#import-btn").click(function(e){
			e.preventDefault();				
			$("#media-import").submit();
			$('#media-import').click(function(e){
				e.preventDefault();	
				$(this).jLoader({
					backdropOpacity: .7,
					backdropColor: '#FFF',
					content: '<span class="loading">Importing the remote file...</span>'	
				});
			});
		});
	});
</script>
</body>
</html>