// JavaScript Document
$().ready(function(){
	(function( $ ){	
	
		var params;
		var $container;
		
		var methods = {
			init : function( options ) {
				params = $.extend({
					formatsNav : '.photo-tabs',
					defaultFormat : 'l'					
				},options);	
				$container = $(this);			
						
				//autostart the function
				(function(){				
					
				})();
			   
			   methods.initNav();
			   methods.initTools();	
			},
			
			initNav : function(){				
				$(params.formatsNav, $container).on('click','a',function(e){
					e.preventDefault();	
					var $target = $(this).data('target');
					$target = $('.'+$target);
					
					$(params.formatsNav + ' a').removeClass("active");
					$(this).addClass("active");
					
					$('.photo').removeClass("show").addClass("hide");
					
					$target.addClass("show").removeClass("hide");
					methods.checkImageInfo($target);
				});
				methods.checkImageInfo($(".size-"+params.defaultFormat, $container));
			},
			
			initTools : function(){
				$('.image-rotate').click(function(e){
					e.preventDefault();
					//getting the vars
					var $post_id = $('#id').val();
					var $file_id = $('#file_id').val();
					var $module = $('#module').val();
					var $format = $(this).data('format');
					var $rotate = $(this).data('rotate');		
					
					//launch the loader
					/*
					$(".photo").jLoader({
						backdropOpacity: .7,
						backdropColor: '#FFF',
						content: '<span class="loading"><i class="icon-refresh icon-spin"></i> Rotating the image...</span>'	
					});
					*/					
					
					$.ajax({
						url : $.config.apipath + "rotate.image",
						data: 'post_id='+$post_id+'&file_id='+$file_id+'&module='+$module+'&rotation='+$rotate+'&format='+$format+'&rotate_all=true',
						type: 'post',
						dataType:"json",	
						success: function(data){										
							if(data.result == 'success'){
								//create_thumbs($encdata);
								//console.log('.size-'+$format+' img');
								//$('.size-'+$format+' img').prop('src', $('.size-'+$format+' img').prop('src')+'?'+Math.random());
								$('.photo img').each(function(){
									$(this).prop('src', $(this).prop('src')+'?'+Math.random());
								});
								
							}//end if
							
						}
					});
					
				});
				
				$('.image-edit').click(function(e){
					e.preventDefault();
					//get the vars
					var $post_id = $('#id').val();
					var $file_id = $('#file_id').val();
					var $module = $('#module').val();
					var $format_id = $(this).data('format-id');		
					var $attachment_post_type = $('#attachment_post_type').val();			
					/*
					$(".photo").jLoader({
						backdropOpacity: .7,
						backdropColor: '#FFF',
						blockLevel : true,
						content: '<span class="loading"><i class="icon-refresh icon-spin"></i> Rotating the image...</span>'	
					});
					*/
					
					//load the crop tool
					$('.crop-tool').load($.config.basepath+'admin/inc/components/media/media.crop.tool.php?post_id='+$post_id+'&file_id='+$file_id+'&module='+$module+'&format_id='+$format_id+'&attachment_post_type='+$attachment_post_type, function(){
						//hide current editor
						$('.photo-editor').fadeOut("slow");	
						$('.attachment-info').fadeOut("slow");						
						$(this).fadeIn("slow");													
					});
				});
			},
			
			checkImageInfo : function($target){				
				var $info = $('.info',$target);
				var $img = $('img',$target);
				var imgCurrentWidth = $img.width();
				var imgCurrentHeight = $img.height();
				var imgActualWidth = $img.data('real-width');
				var imgActualHeight = $img.data('real-height');
				if(imgCurrentWidth < imgActualWidth || imgCurrentHeight < imgActualHeight){
					//the image has been sized down
					$info.addClass('alert');
					$info.html('sized down to '+imgCurrentWidth+'px x '+imgCurrentHeight+'px from '+imgActualWidth+'px x '+imgActualHeight+'px');
				} else {
					//the image has the actual size
					$info.addClass('alert alert-info');
					$info.html(imgActualWidth+'px x '+imgActualHeight+'px');
				}//end if
					
			},
			
			destroy : function(){
				//destroy function	
				$("#mediaModal .nav-tabs li").eq(0).addClass("active");
				$container.parent().removeClass("active");
			}
		}
		
		$.fn.photoEditor = function( method ) {    
			if ( methods[method] ) {
			  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
			  return methods.init.apply( this, arguments );
			} else {
			  $.error( 'Method ' +  method + ' does not exist on jQuery.photoEditor' );
			}      
		};
	
	})( jQuery );

});