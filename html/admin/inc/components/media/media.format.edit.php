<?php
	ob_start();
	session_start();	
	include("../../../config.inc.php");
	$curpath = "../../../../admin/";
	include("../../../classes/utils.class.php");	
	include("../../secure.inc.php");
	include("../../init.cms.inc.php");
	
	$id = $_GET['id'];
	$format_data = $utils->get_attachment_format(array("id" => $id));
?>
<form action="<?php echo __CMS_PATH__ ?>api/save.media.format" method="post" id="format-save-<?php echo $format_data['attachment_setting_id'] ?>" name="formatForm">
	<input type="hidden" name="id" id="format-id-<?php echo $format_data['attachment_setting_id'] ?>" value="<?php echo $format_data['attachment_setting_id'] ?>" />
	<div class="format-edit-form-header">
        <div class="form-inline">
            <div class="input-prepend">
                <label class="add-on">Label</label>    
                <input type="text" class="input-medium" id="setting-label-<?php echo $format_data['attachment_setting_id'] ?>" name="attachment_label" value="<?php echo $format_data['attachment_setting_label'] ?>" /> 
            </div>
            <span class="pull-right">
                <a href="<?php echo __CMS_PATH__ ?>api/save.media.format" class="btn btn-primary btn-mini" data-btn-role="save" data-target="#format-view-<?php echo $format_data['attachment_setting_id'] ?>">Save</a> <a href="#" class="btn btn-danger btn-mini" data-btn-role="cancel" data-target="#format-view-<?php echo $id ?>">Cancel</a>
            </span>
        </div>	
    </div>
    <div class="form-inline">
        <div class="control-group">
            <label>Image sizes</label>
            <div class="input-append">
                <input type="text" name="attachment_width" id="attachment-width-<?php echo $format_data['attachment_setting_id'] ?>" placeholder="Width" class="input-mini" value="<?php echo $format_data['attachment_setting_width'] ?>" /> 
                <span class="add-on">px</span>
            </div>
            X
            <div class="input-append">
                <input type="text" name="attachment_height" id="attachment-height-<?php echo $format_data['attachment_setting_id'] ?>" placeholder="Height" class="input-mini" value="<?php echo $format_data['attachment_setting_height'] ?>" /> 
                <span class="add-on">px</span>
            </div>       
        </div>             
    </div>  
    <div class="form-inline">
        <!--div class="control-group">
            <label for="attachment-processing-<?php echo $id ?>">Processing</label>
            <select name="attachment_processing" id="attachment-processing-<?php echo $id ?>" class="input-small">
                <option value="crop">Crop</option>
                <option value="scale">Scale</option>
            </select>
        </div-->
        <div class="control-group">
            <label for="attachment-format-<?php echo $id ?>">Folder Name</label>
            <input type="text" name="attachment_format" id="attachment-format-<?php echo $id ?>" class="input-small" disabled="disabled" value="<?php echo $format_data['attachment_setting_folder'] ?>" />               
        </div>
        <div class="control-group">
        	<label class="checkbox">
           		<input type="checkbox" name="attachment_default" id="attachment-default-<?php echo $id ?>" value="1" <?php
                if($format_data['attachment_setting_default'] == "1"){
					echo ' checked="checked"';	
				}//end if
				?>> Automatically create the image in this format
            </label>
        </div>
        <div class="control-group">
        	<!--label class="checkbox">
           		<input type="checkbox" name="attachment_keep_ratio" id="attachment-keep-ratio-<?php echo $id ?>" value="1" <?php
                if($format_data['attachment_setting_keep_ratio'] == "1"){
					echo ' checked="checked"';	
				}//end if
				?>> Preserve the aspect ratio of the image
            </label-->
        </div>
    </div>    
</form>
<script>
	$("#attachment-processing-<?php echo $id ?>").val('<?php echo $format_data['attachment_setting_processing'] ?>');
	
	$("a[data-btn-role=save]",$("#format-view-<?php echo $format_data['attachment_setting_id'] ?>")).unbind("click");
	$("a[data-btn-role=save]",$("#format-view-<?php echo $format_data['attachment_setting_id'] ?>")).click(function(e){
		e.preventDefault();
		$("#format-save-<?php echo $format_data['attachment_setting_id'] ?>").submit();
	});	
	
	$("#format-save-<?php echo $format_data['attachment_setting_id'] ?>").submit(function(e){
		e.preventDefault();
		var $form = $(this);
		var $target = $("a[data-btn-role=save]",$form).attr("data-target");		
		$.ajax({
			url : $form.attr("action"),
			dataType:"json",
			type: "post",
			data: $form.serialize(),
			success: function(data){
				if(data.result == "success"){				
					$form.fadeOut("slow",function(){
						$(this).remove();						
						$($target).load("<?php echo __CMS_PATH__ ?>inc/components/media/media.format.view.php?id=<?php echo $id ?>&load_cms=1");
						$("a[data-btn-role=edit]").mediaFormatEditBtn();
					});
				}//end if
			}
		});
	});
	

	$("a[data-btn-role=cancel]",$("#format-view-<?php echo $format_data['attachment_setting_id'] ?>")).unbind("click");
	$("a[data-btn-role=cancel]",$("#format-view-<?php echo $format_data['attachment_setting_id'] ?>")).click(function(e){
		e.preventDefault();
		var $target = $(this).attr("data-target");
		$("form",$($target)).fadeOut("slow",function(){
			$(this).remove();		
			$($target).load("<?php echo __CMS_PATH__ ?>inc/components/media/media.format.view.php?id=<?php echo $id ?>&load_cms=1");
		});
	});	
</script>