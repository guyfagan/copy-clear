<!-- Modal -->
<div id="mediaModal" class="modal modal-large hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel">Media Library</h3>
    </div>
    <div class="modal-body">
        <div id="media-library" class="media-library media-library-collapsed">          
            <ul class="nav nav-tabs">
                <li class="active"><a href="#upload" data-toggle="tab">Upload</a></li>
                <li><a href="#library" data-toggle="tab">Media Library</a></li>    
                <li class="post-files-tab"><a href="#post-files" data-toggle="tab">Related Media</a></li>    
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="upload">
                    
                </div>
                <div class="tab-pane" id="library">
                	<div class="gallery-box clearfix">
                    	<div class="gallery-header clearfix">
                        	<strong>Filter:</strong>                
                            <div class="btn-group" data-toggle="buttons-radio">
                                <a href="#all" class="btn btn-mini active" data-btn-role="filter">Show All</a>
                                <a href="#image" class="btn btn-mini" data-btn-role="filter">Images</a>
                                <a href="#file" class="btn btn-mini" data-btn-role="filter">Files</a>          
                                <a href="#document" class="btn btn-mini" data-btn-role="filter">Documents</a> 
                                <a href="#video" class="btn btn-mini" data-btn-role="filter">Videos</a>                      
                                <a href="#url" class="btn btn-mini" data-btn-role="filter">Imported Urls</a> 
                            </div>
                            <p>&nbsp;</p>
                            <div class="input-append">
                                <input class="input-medium" id="search-media" name="search-media" type="text">
                                <button class="btn" type="button" id="btn-search-media">Search</button> 
                                <button class="btn" type="button" id="btn-reset-search">Reset</button>                               
                            </div>
                            <div class="btn-group pull-right" data-toggle="buttons-radio">
                                <a href="#thumbs" class="btn active" data-btn-role="view-filter"><i class="icon-th-large"></i> Thumbs</a>
                                <a href="#list" class="btn" data-btn-role="view-filter"><i class="icon-th-list"></i> List</a>
                            </div>                            
                        </div>
                        <div class="gallery-container gallery-show-thumbs">
                        
                        </div>  
                        <div class="gallery-side-info">                        	
                            
                        </div>                 
                    </div>                                                     
                </div> 
                <div class="tab-pane" id="post-files">
                	<div class="gallery-box clearfix">
                    	<div class="gallery-header clearfix">
                        	<small class="muted pull-left">Drag the files to change the sort order</small>
                            <div class="btn-group pull-right" data-toggle="buttons-radio">
                                <a href="#thumbs" class="btn" data-btn-role="view-filter"><i class="icon-th-large"></i> Thumbs</a>
                                <a href="#list" class="btn active" data-btn-role="view-filter"><i class="icon-th-list"></i> List</a>
                            </div>                            
                        </div>
                        <div class="gallery-container gallery-show-list">
                        
                        </div>  
                        <div class="gallery-side-info">                        	
                            
                        </div>                 
                    </div>                                                     
                </div>                 
            </div>
        </div>
	</div> 
    <div class="modal-footer">
    	<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>  
</div>
<script>
	$().ready(function(){
		$settings.mediaSettings = {};
		<?php
		$mediaSettings = $utils->get_settings(array("module" => 'media'));
		echo '$settings.mediaSettings = '.json_encode($mediaSettings).';';
		?>	
		
	});
</script>