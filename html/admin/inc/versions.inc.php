<?php
	//Libraries Versions
	define("__CMS_VERSION__","3.0.3.140509");
	define("__JQUERY_VERSION__","2.1.0");
	define("__JQUERY_UI_VERSION__","1.10.4");
	define("__CKEDITOR_VERSION__","4.3");
	define("__BOOTSTRAP_VERSION__","2.3.2");
	define("__JCROP_VERSION__","0.9.12");
	define("__MEDIAELEMENT_VERSION__","2.11.2");
	define("__FONTAWESOME_VERSION__","3.2.1");
	define("__BOOTSTRAP_TOKENFIELD__","0.7.1");
	define("__MANDRILL_VERSION__","1.0.52");
	define("__CHARTJS_VERSION__","0.2");
	define("__MARKDOWN_VERSION__","1.3");
	define("__BOOTSTRAP_EDITABLE_VERSION__","1.5.1");
	define("__GETID3_VERSION__","1.9.7");
	define("__USE_CDN__",false);
?>