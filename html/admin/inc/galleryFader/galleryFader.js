/*
 *  jQuery Boilerplate - v3.3.1
 *  A jump-start for jQuery plugins development.
 *  http://jqueryboilerplate.com
 *
 *  Made by Zeno Rocha
 *  Under MIT License
 */
// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {

		// undefined is used here as the undefined global variable in ECMAScript 3 is
		// mutable (ie. it can be changed by someone else). undefined isn't really being
		// passed in so we can ensure the value of it is truly undefined. In ES5, undefined
		// can no longer be modified.

		// window and document are passed through as local variable rather than global
		// as this (slightly) quickens the resolution process and can be more efficiently
		// minified (especially when both are regularly referenced in your plugin).

		// Create the defaults once
		var pluginName = "galleryFader",
				defaults = {
				gallery: "",
				delay: 5000,
				transitionSpeed: 1000,
				auto: true,
				nextBtn: '',
				prevBtn: '',
				fx: 'fadeIn',
				position: 'center'
		};
		
		var counter = 0;
	

		// The actual plugin constructor
		function Plugin ( element, options ) {
			this.element = element;
			// jQuery has an extend method which merges the contents of two or
			// more objects, storing the result in the first object. The first object
			// is generally empty as we don't want to alter the default options for
			// future instances of the plugin
			this.settings = $.extend( {}, defaults, options );
			this._defaults = defaults;
			this._name = pluginName;
			this.init();
		}

		Plugin.prototype = {
			init: function () {
					// Place initialization logic here
					// You already have access to the DOM element and
					// the options via the instance, e.g. this.element
					// and this.settings
					// you can add more functions like the one below and
					// call them like so: this.yourOtherFunction(this.element, this.settings).
				if(this.settings.auto){
					this.startTimer();
				}
				this.enableNavigation();
				this.loadImage('none');				
			},
			
			enableNavigation: function (){
				var plugin = this;	
				if(this.settings.nextBtn.length > 0 && this.settings.nextBtn != ''){
					$(this.settings.nextBtn).click(function(){
						plugin.nextImage();	
					});
				}
				if(this.settings.prevBtn.length > 0 && this.settings.prevBtn != ''){
					$(this.settings.prevBtn).click(function(){
						plugin.previousImage();	
					});
				}
			},
			
			test: function(){
				console.log(this.element);
			},
			
			loadImage: function (fx){
				
				if(typeof(fx) == 'undefined'){
					fx = this.settings.fx;	
				}
				
				//console.log(fx);
				
				var img = $('<img />').attr({
					src : this.settings.gallery[counter]
				}).addClass('fullscreen-bg');
				
				var callback = function(){
					$(this).prevAll().remove();	
				};
				
				switch(fx){
					case "fadeIn":	
					default:							
						img.hide();
						break;	
					case "slide":					
						img.css({
							left: 300,
							opacity: 0
						});
						img.animate({
							left: 0,
							opacity: 1
						},this.settings.transitionSpeed, callback);
						break;
					case "none":
						img.prevAll().remove();	
						break;
				}				
				$(this.element).append(img);
				console.log();	
				img.css({marginLeft: ($(window).width()/2) - (img.width()/2)}).fadeIn(this.settings.transitionSpeed,callback);
				
				img.prevAll().fadeOut(this.settings.transitionSpeed);
			},
			
			nextImage: function (){
				counter++;
				if(counter >= this.settings.gallery.length){
					counter = 0;	
				}
				this.loadImage();
				
			},
			
			previousImage: function (){
				counter--;
				if(counter < 0){
					counter = this.settings.gallery.length - 1;	
				}
				this.loadImage();
				
			},
			
			startTimer: function () {	
				var plugin = this;			
				var timer = setInterval(function(){
					plugin.nextImage()
					//$('img',element).prop('src',settings.gallery[counter]);
					
				},plugin.settings.delay);
			}			
			
		};

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function ( options ) {
			return this.each(function() {
				if ( !$.data( this, "plugin_" + pluginName ) ) {
					$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
				}
			});
		};

})( jQuery, window, document );
