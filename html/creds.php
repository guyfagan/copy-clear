<?php 
/* 
!!!Important!!! Plupload UI Widget here, is used only for demo purposes and is not required for uploading to S3.
*/

// important variables that will be used throughout this example
$bucket = 'copyclear';

// these can be found on your Account page, under Security Credentials > Access Keys
$accessKeyId = '1R9N9E3RJ19WXC2HT502';
$secret = 'P8b606n173iP1psQaZgvB+FOog3IRXJHEpm5qHk1';


// hash_hmac — Generate a keyed hash value using the HMAC method 
// (PHP 5 >= 5.1.2, PECL hash >= 1.1)
if (!function_exists('hash_hmac')) :
// based on: http://www.php.net/manual/en/function.sha1.php#39492
function hash_hmac($algo, $data, $key, $raw_output = false)
{
	$blocksize = 64;
    if (strlen($key) > $blocksize)
        $key = pack('H*', $algo($key));
    
	$key = str_pad($key, $blocksize, chr(0x00));
    $ipad = str_repeat(chr(0x36), $blocksize);
    $opad = str_repeat(chr(0x5c), $blocksize);
    $hmac = pack('H*', $algo(($key^$opad) . pack('H*', $algo(($key^$ipad) . $data))));
	
	return $raw_output ? $hmac : bin2hex($hmac);
}
endif;

// prepare policy
$policy = base64_encode(json_encode(array(
	// ISO 8601 - date('c'); generates uncompatible date, so better do it manually
	'expiration' => date('Y-m-d\TH:i:s.000\Z', strtotime('+1 day')),  
	'conditions' => array(
		array('bucket' => $bucket),
		array('acl' => 'public-read'),
		array('starts-with', '$key', ''),
		// for demo purposes we are accepting only images
		array('starts-with', '$Content-Type', ''),
		// "Some versions of the Adobe Flash Player do not properly handle HTTP responses that have an empty body. 
		// To configure POST to return a response that does not have an empty body, set success_action_status to 201.
		// When set, Amazon S3 returns an XML document with a 201 status code." 
		// http://docs.amazonwebservices.com/AmazonS3/latest/dev/HTTPPOSTFlash.html
		array('success_action_status' => '201'),
		// Plupload internally adds name field, so we need to mention it here
		array('starts-with', '$name', ''), 	
		// One more field to take into account: Filename - gets silently sent by FileReference.upload() in Flash
		// http://docs.amazonwebservices.com/AmazonS3/latest/dev/HTTPPOSTFlash.html
		//array('starts-with', '$Filename', ''), 
	)
)));

// sign policy
$signature = base64_encode(hash_hmac('sha1', $policy, $secret, true));

$response['bucket'] = $bucket;
$response['accessKeyId'] = $accessKeyId;
$response['policy'] = $policy;
$response['signature'] = $signature;

echo $_GET['callback'] . '('.json_encode($response).')';

?>