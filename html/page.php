<?php	
	ob_start();
	session_start();
	ini_set("display_errors",0);
	include("admin/config.inc.php");
	$curpath = "admin/";
	include("admin/classes/utils.class.php");	
	include("inc/config.inc.php");
	
	$site = $utils->call('site');
	$utils->view->dev_mode = false;
	
	$permalink = $utils->view->get_permalink();
	$section = $utils->view->get_section();
	
	$page_loaded = $utils->view->load_page(); //this function automatically redirect in case of failure
	$page = $utils->view->page;//shortcut
	if(isset($page['module'])){
		$site->set_module($page['module']);
	} else if(!is_null($utils->view->get_section())){
		$site->set_module($utils->view->get_section());
	}//end if
	switch ($page['type']){
		case "post":
			if ($page['category_id']!==false){
				$category_data = unserialize(base64_decode($page['category_id']));
				$category = $site->get_category(array('cid' => $category_data[0]));
				$active_cat = $category_data[0];
				$page_title = $page['post_title'];
			}
		break;
		
		case "category":
			$category_data = $site->get_category(array('cid' => $page['category_id']));
			$active_cat = $category_data['category_father_id'];
			$page_title = $page['category_name'];
		break;
		
	}
	require_once("inc/meta.inc.php");
?>
</head>
<body>
	<header>	
		<div class="container">
			<div class="row clearfix">
					<a href="index.php"><h1>Copyclear</h1></a>
                    <a id="showLeft" href="#" class="btn btn-navbar pull-right visible-xs">Menu</a>
                <div class="toplogo">
                    <div id="ccci-logo" class="hidden-xs">
                        <img alt="Central Copy Clearance Ireland" src="gr/ccci_small.png">
                    </div>
                    <!--<div>
                        <p class="byline">Central Copy Clearance Ireland is now trading as CopyClear</p>
                    </div>-->
                </div>
			</div>
		</div>
	</header>
    <?php	
	$nav = $site->get_items(array('status' => 1));
	if($nav !== false){
		$navlist = '<li><a class="nav-item" href="'.__BASEPATH__.'">Home</a>';
		for($i = 0; $i < sizeof($nav); $i++){
			$link = __BASEPATH__.$nav[$i]['permalink'].'.html';		
			$navlist .= '<li><a class="nav-item';
			if($permalink == $nav[$i]['permalink']){
				$navlist .= ' active';	
			} else if($active_cat == $nav[$i]['id'] && $nav[$i]['is_category'] == 1){
				$navlist .= ' active';	
			}
			
			$navlist .= '" href="'.$link.'">'.$nav[$i]['label'];
			$navlist .= '</a></li>';
		}//end for i
		
		require_once('inc/mobile.nav.inc.php');
		require_once('inc/nav.inc.php');
	}
	?>
    <?php
	$tpl = $utils->view->load_template(array('template_path' => 'inc/templates/'));	
		
	if($tpl !== false){
		require_once($tpl);
	}//end if
	
	require_once('inc/footer.inc.php');
	?>
  </body>
</html>