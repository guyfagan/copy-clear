<?php	
	ob_start();
	session_start();
	//ini_set("display_errors",1);
	include("admin/config.inc.php");
	$curpath = "admin/";
	include("admin/classes/utils.class.php");	
	include("inc/config.inc.php");
	
	$site = $utils->call('site');
	$utils->view->dev_mode = true;
	
	$page_title = 'Welcome';
	$page=$site->get_post(array('id'=>1));
	
	$news = $site->get_posts(array('module' => 'news', 'status' => 1, 'limit' => 1, 'order_by' => 'post_date', 'sort_order' => 'desc'));
	$promos = $site->get_posts(array('module' => 'hp-promo', 'status' => 1, 'limit' => 1, 'order_by' => 'post_date', 'sort_order' => 'desc'));
	
	require_once("inc/meta.inc.php");
?>
</head>
<body>
	<header>	
		<div class="container">
			<div class="row clearfix">
					<a href="index.php"><h1>Copyclear</h1></a>
                    <a id="showLeft" href="#" class="btn btn-navbar pull-right visible-xs">Menu</a>
                <div class="toplogo">
                    <div id="ccci-logo" class="hidden-xs">
                        <img alt="Central Copy Clearance Ireland" src="gr/ccci_small.png">
                    </div>
                </div>
			</div>
		</div>
	</header>
    <?php	
	$nav = $site->get_items(array('status' => 1,'module'=>'pages','sort_order' => 'DESC'));
	if($nav !== false){
		$navlist = '';
		$navlist = '<li><a class="nav-item active" href="'.__BASEPATH__.'">Home</a>';
		for($i = 0; $i < sizeof($nav); $i++){
			$link = __BASEPATH__.$nav[$i]['permalink'].'.html';		
			$navlist .= '<li><a class="nav-item';
			if($permalink == $nav[$i]['permalink']){
				$navlist .= ' active';	
			} else if($active_cat == $nav[$i]['id'] && $nav[$i]['is_category'] == 1){
				$navlist .= ' active';	
			}
			
			$navlist .= '" href="'.$link.'">'.$nav[$i]['label'];
			$navlist .= '</a></li>';
		}//end for i
		
		require_once('inc/mobile.nav.inc.php');
		require_once('inc/nav.inc.php');
	}
	?>
    
    <article class="main-content beige">
		<div class="container">
           	<div class="row tagline"><h1 class="copyline"><?php echo $page['post_subtitle']; ?></h1></div>
             <section class="row intro">
             <div class="col-md-3 subnav hidden-xs hidden-sm">&nbsp;</div>
             <div class="hometext col-xs-12 col-md-9">
                <?php
                echo utf8_encode($page['post_message']);
            ?>  
			</div>
			</section>
            <section class="row">
                <ul class="circs clearfix col-md-6 col-sm-6 col-xs-12 top-row">
                    <li class="circ-color who"><a href="who-we-are.html">Who We Are</a></li>
                   	<li class="circ-color remit"><a href="our-remit.html">Our Remit</a></li>
                </ul>
                <ul class="circs clearfix col-md-6 col-sm-6 col-xs-12 bottom-row">
                    <li class="circ-color mission"><a href="our-mission.html">Our Mission</a></li>
                    <li class="circ-color behaviour"><a href="our-behaviour.html">Our Behaviour</a></li>
                </ul>
            </section>
		</div>
	</article>
	<article class="white">
		<div class="container">
            <section class="row news">
            	<div class="col-md-6 col-sm-6 news-promo clearfix">
                <?php 
					if ($promos !== false){ ?>
							<h3><a href="<?php echo $promos[0]['post_link_to']; ?>" target="_blank"><?php echo $promos[0]['post_title']; ?></a></h3>
                     <?php
                echo utf8_encode($promos[0]['post_message']);
            ?>  
            	<a href="http://prezi.com/vyomixhvfclv/ccci/" class="btn btn-primary">Watch the Prezi Now</a>
				<?php	}
				?>
                </div>
                <div class="col-md-6 col-sm-6 news-story clearfix">
                <?php 
					if ($news !== false){ ?>
							<h3>News</h3>
                    		<h4><?php echo $news[0]['post_title']; ?></h4>
                    		<p><?php echo date('jS F Y', $news[0]['post_date']); ?></p>
                     <?php
                echo utf8_encode($news[0]['post_message']);
            ?>  
				<?php	}
				?>
                </div>
            </section>
		</div>
	</article>	
    
    <?php
	
	require_once('inc/footer.inc.php');
	?>
  </body>
</html>