// JavaScript Document

$.copyclear = $settings;

$(document).ready(function(e) {

	actions_interceptor();
});

function actions_interceptor(){
	if($("#login-form").length > 0){
		$("#login-btn").click(function(e){
			e.preventDefault();
			$("#login-form").submit();
		});

		$("#login-form").submit(function(e){
			//e.preventDefault();
			var $form = $(this);
			$('.alert').remove();
			$.ajax({
				url: $.copyclear.apipath + 'login',
				data: $form.serialize(),
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						location.href = $.copyclear.basepath + 'home/';
					} else {
						$form.prepend('<div class="alert alert-danger"><strong>Error</strong> Username and/or password are incorrect, try again.</div>');
					}
				}
			});
		});
	}

	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});

	if($("#new-submission").length > 0){
		init_new_submission_form();
	}//end if

	if($("#new-submission-final").length > 0){
		init_final_submission_form();
	}//end if

	if($("#new-submission-upload").length > 0){
		init_upload_submission_form();
	}

	if($("#comment-form").length > 0){
		init_comments();
	}

	if($("#new-submission-confirm").length > 0){
		$.copyclear.encdata = $("#encdata").val();
		$("#complete-submission-btn").click(function(e){
			e.preventDefault();
			complete_submission();
		});
		init_handle_submissions_list();
	}

	if($("#archive").length > 0){
		init_archive();
	}

	if($("#report").length > 0){
		init_archive();
	}
}

function init_handle_submissions_list(){
	$('.submissions-list a[data-role=delete-submission]').click(function(e){
		e.preventDefault();
		var $total = $('.submissions-list a[data-role=delete-submission]').length;
		if($total > 1){
			$.copyclear.submission_data = $(this).data('submission-data');
			$('#confirm-delete').modal('show');
		} else {
			$("#warning-delete").modal('show');
		}
	});

	$("#confirm-delete-btn").click(function(e){
		e.preventDefault();
		$.ajax({
			url: $.copyclear.apipath + 'delete.submission',
			data: 'encdata='+$.copyclear.submission_data,
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.success){
					$('#confirm-delete').modal('hide');
				} else {

				}
			}
		});
	});
}

function init_new_submission_form(){

	var $encdata = $('#encdata').val();
	$.copyclear.encdata = $("#encdata").val();

	$("#supporting_material").on('click', function(event){
		var isChecked = $("#supporting_material").is(':checked');
		if(isChecked){
			$('#substantiation-modal').modal('show');
		}
	});

	$("#brand-owner").on('change',function(){
		if($(this).val() != ""){
			var $value = $(this).val();
			$.ajax({
				url: $.copyclear.apipath + 'get.advertiser.brands',
				data: 'advertiser='+$value+'&encdata='+$encdata,
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						$("#brand").html('<option>Select a brand</option>');
						$("#brand").removeAttr('disabled');
						for(i = 0; i < data.list.length; i++){
							$option = $('<option value="'+data.list[i].brand_id+'">'+data.list[i].brand_name+'</option>');
							$("#brand").append($option);
						}
						reset_campaign();
						reset_ad();
					} else {
						$("#brand").html('<option>No brands found for the selected brand owner</option>');
					}
				}
			});
		}
	});

	$("#brand").on('change',function(){
		if($(this).val() != ""){
			var $brand = $(this).val();
			var $advertiser = $("#brand-owner").val();
			$.ajax({
				url: $.copyclear.apipath + 'get.brand.campaigns',
				data: 'advertiser='+$advertiser+'&brand='+$brand+'&encdata='+$encdata,
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						$("#campaign").html('<option>Select a campaign</option>');
						$("#campaign").removeAttr('disabled');
						for(i = 0; i < data.list.length; i++){
							$option = $('<option value="'+data.list[i].campaign_id+'">'+data.list[i].campaign_name+'</option>');
							$("#campaign").append($option);
						}
						reset_ad();
					} else {
						$("#campaign").html('<option>No campaigns found for the selected brand</option>');
					}
				}
			});
		}
	});

	$("#campaign").on('change',function(){
		if($(this).val() != ""){
			var $brand = $("#brand").val();
			var $advertiser = $("#brand-owner").val();
			var $campaign = $("#campaign").val();
			$.ajax({
				url: $.copyclear.apipath + 'get.campaign.ads',
				data: 'advertiser='+$advertiser+'&brand='+$brand+'&campaign='+$campaign+'&encdata='+$encdata,
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						$("#ad").html('<option>Select an AD</option>');
						$("#ad").removeAttr('disabled');
						for(i = 0; i < data.list.length; i++){
							$option = $('<option value="'+data.list[i].ad_id+'">'+data.list[i].ad_title+'</option>');
							$("#ad").append($option);
						}
					} else {
						$("#ad").html('<option>No Ads found for the selected brand</option>');
					}
				}
			});
		}
	});

	$("#save-brand-owner-btn").click(function(e){
		e.preventDefault();
		$.ajax({
			url: $.copyclear.apipath + 'private.save.brand.owner',
			data: 'name='+$('#nbo-name').val()+ '&encdata='+$encdata,
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					$("#brand-owner").append('<option value="'+data.id+'">'+data.name+'</option>');
					$("#brand-owner").val(data.id);
					$('#new-brand-owner-modal').modal('hide');
					reset_brand();
					reset_campaign();
					reset_ad();
				} else {
					$('#nbo-form .modal-body .alert').remove();
					$('#nbo-form .modal-body').prepend('<div class="alert alert-warning">Cannot save this brand owner</div>');
				}//end if
			}
		});
	});

	$("#save-brand-btn").click(function(e){
		e.preventDefault();
		$.ajax({
			url: $.copyclear.apipath + 'private.save.brand',
			data: $("#nb-form").serialize()+'&brand_owner_id='+$('#brand-owner').val()+ '&encdata='+$encdata,
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					$("#brand").append('<option value="'+data.id+'">'+data.name+'</option>');
					$("#brand").removeAttr('disabled');
					$("#brand").val(data.id);
					$('#new-brand-modal').modal('hide');
					reset_campaign();
					reset_ad();
				} else {
					$('#nb-form .modal-body .alert').remove();
					$('#nb-form .modal-body').prepend('<div class="alert alert-warning">Cannot save this brand</div>');
				}//end if
			}
		});
	});

	$("#save-campaign-btn").click(function(e){
		e.preventDefault();
		$.ajax({
			url: $.copyclear.apipath + 'save.campaign',
			data: $("#nc-form").serialize()+'&brand_id='+$('#brand').val()+ '&encdata='+$encdata,
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					$("#campaign").append('<option value="'+data.id+'">'+data.name+'</option>');
					$("#campaign").removeAttr('disabled');
					$("#campaign").val(data.id);
					$('#new-campaign-modal').modal('hide');
					reset_ad();
				} else {
					$('#nc-form .modal-body .alert').remove();
					$('#nc-form .modal-body').prepend('<div class="alert alert-warning">Cannot save this campaign</div>');
				}//end if
			}
		});
	});

	$("#na-medium").change(function(){
		if($(this).val() == 10){
			$(this).after('<div class="alert alert-warning">*Please note that in the case of Owned Online Activations responsibility for individual postings remains with the brand owner.</div>');
		} else {
			$("#na-form .alert").remove();
		}
	});

	$("#save-ad-btn").click(function(e){
		e.preventDefault();
		$.ajax({
			url: $.copyclear.apipath + 'save.ad',
			data: $("#na-form").serialize()+'&campaign_id='+$("#campaign").val()+'&brand_owner_id='+$("#brand-owner").val()+'&brand_id='+$('#brand').val()+ '&encdata='+$encdata,
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					$("#ad").append('<option value="'+data.id+'">'+data.name+'</option>');
					$("#ad").removeAttr('disabled');
					$("#ad").val(data.id);
					$('#new-ad-modal').modal('hide');
				} else {
					$('#na-form .modal-body .alert').remove();
					$('#na-form .modal-body').prepend('<div class="alert alert-warning">Cannot save this AD</div>');
				}//end if
			}
		});
	});

	$("#save-submission-btn").click(function(e){
		e.preventDefault();
		var $form = $('#new-submission');
		$.ajax({
			url: $.copyclear.apipath + 'save.submission',
			data: $form.serialize(),
			dataType:"json",
			type: 'post',
			success: function(data){
				if(data.result == 'success'){
					var encdata = data.encdata;
					location.href = $.copyclear.basepath + 'submission/upload/?encdata='+encdata;
				}
			}
		});
	});

}

function reset_brand(){
	$("#brand").html('<option>Select a brand owner first</option>').addClass('disabled').prop('disabled','disabled');
}

function reset_campaign(){
	$("#campaign").html('<option>Select one</option>').addClass('disabled').prop('disabled','disabled');
}

function reset_ad(){
	$("#ad").html('<option>Select one</option>').addClass('disabled').prop('disabled','disabled');
}


function init_upload_submission_form(){
	$.copyclear.encdata = $("#encdata").val();
	prepareUploadForm();

	$("#save-labels-next-btn").click(function(e){
		e.preventDefault();
		var $form = $("#edit-submission-labels");
		$.ajax({
			url: $.copyclear.apipath + 'save.submission.labels',
			data: $form.serialize()+'&encdata='+$.copyclear.encdata,
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					location.href = $.copyclear.basepath + 'submission/details/?encdata='+$.copyclear.encdata;
				}
			}
		});
	});

	$("#save-labels-addnew-btn").click(function(e){
		e.preventDefault();
		var $form = $("#edit-submission-labels");
		$.ajax({
			url: $.copyclear.apipath + 'save.submission.labels',
			data: $form.serialize()+'&encdata='+$.copyclear.encdata,
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					$("#add-files-view").fadeIn('slow');
					$("#edit-files-view").fadeOut('slow',function(){
						$(this).addClass('hide');
					});
					prepareUploadForm();
					$("#show-files-btn").removeClass('hide');
					$("#complete-uploads-btn").removeClass('hide');
				}
			}
		});
	});

	$("#add-url-btn").click(function(e){
		e.preventDefault();
		$("#main-content").jLoader({
			backdropOpacity: .7,
			backdropColor: '#FFF',
			content: '<span class="loading">Processing url...</span>'
		});
		import_url();
	});

	$("#show-files-btn").click(function(e){
		e.preventDefault();
		$("#add-files-view").fadeOut('slow');
		$("#edit-files-view").removeClass('hide').hide().fadeIn('slow');
		generate_submission_labels();
	});
}

function import_url(){
	$url = $("#url").val();
	$.ajax({
		url: $.copyclear.apipath + 'save.url',
		data: 'url='+$url+'&encdata='+$.copyclear.encdata,
		type:'post',
		dataType:"json",
		success: function(data){
			if(data.result == 'success'){
				$.ajax({
					url: $.copyclear.apipath+'reorganise.files',
					data: 'encdata='+$.copyclear.encdata,
					dataType:"json",
					type: 'post',
					success: function(data){
						if(data.result == 'success'){
							$("#add-files-view").fadeOut('slow');
							$("#edit-files-view").removeClass('hide').hide().fadeIn('slow');
							$("#url").val("");
							generate_submission_labels();
						}//end if
					}
				});
				$("#main-content").jLoader('closeLoader');
			}//end if
		}
	});
}

function init_final_submission_form(){
	var $encdata = $('#encdata').val();
	$.copyclear.encdata = $encdata;

	$("#new-submission-final").submit(function(e){
		e.preventDefault();
	});

	$("#review-submission-btn").click(function(e){
		e.preventDefault();
		var $form = $("#new-submission-final");
		$.ajax({
			url: $.copyclear.apipath + 'save.submission.details',
			data: $form.serialize(),
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					//complete_submission();
					location.href = $.copyclear.basepath+"submission/confirm/?encdata="+$.copyclear.encdata;
				}
			}
		});
	});
}

function complete_submission(){
	$.ajax({
		url: $.copyclear.apipath + 'complete.submission',
		data: 'encdata='+$.copyclear.encdata,
		dataType:"json",
		type:'post',
		success: function(data){
			if(data.result == 'success'){
				location.href = $.copyclear.basepath+"submission/finish/?encdata="+$.copyclear.encdata;
			}
		}
	});
}//end function

$.fn.validate = function(){
	var valid = true;
	$("input[required],select[required],textarea[required]").each(function(){
		$(this).closest('.control-group').removeClass("error");
		if($(this).is(":checkbox")){
			var className = $(this).prop('class');
			if($("."+className+":checked").length == 0){
				var $label = $(this).data("label");
				if($label == "undefined"){
					$label = $(this).parent().find("label").text();
				}
				$.copyclear.missingFields.push($label);
				valid = false;
			}
		} else {
			if($(this).val() == ""){
				$(this).closest('.control-group').addClass("error");
				var $label = $(this).data("label");
				if($label == "undefined"){
					$label = $(this).closest("label").text();
				}
				$.copyclear.missingFields.push($label);
				valid = false;
			} else if($(this).prop("type") == "email"){
				var validEmail = validateEmail($(this).val());
				if(!validEmail){
					$(this).closest('.control-group').addClass("error");
					var $label = "Please enter a valid email address";
					$.copyclear.missingFields.push($label);
					valid = false;
				}
			}//end if
		}
	});
	return valid;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function prepareUploadForm(){
	var timestamp = new Date().getTime();
	$.copyclear.timestamp = timestamp;
	$.copyclear.files_remaining = 0;
	var maxsize = '80mb';

	$.jsonp({
	  "url": "https://copyclear.ie/creds.php?callback=?",
      "success": function(response) {
			$("#uploader").pluploadQueue({
				runtimes : 'html5,flash,silverlight',
				//max_file_size : maxsize,
				url : 'https://'+response.bucket+'.s3-eu-west-1.amazonaws.com:443/',
				max_file_size : '200mb',
				multipart: true,
				//max_files: 1,
				multipart_params: {
					'key': timestamp + '_${filename}', // use filename as a key
					//'Filename': timestamp + '${filename}', // adding this to keep consistency across the runtimes
					'acl': 'public-read',
					'Content-Type': 'image/jpeg',
					'success_action_status': '201',
					'AWSAccessKeyId' : response.accessKeyId,
					'policy': response.policy,
					'signature': response.signature
				},

				// optional, but better be specified directly
				file_data_name: 'file',

				// re-use widget (not related to S3, but to Plupload UI Widget)
				multiple_queues: false,

				// Specify what files to browse for

				filters : {
					// Maximum file size
					max_file_size : '400mb',
					// Specify what files to browse for
					mime_types: [
						{title : "Image files", extensions : "jpg,jpeg,png,gif"},
						{title : "Video files", extensions : "mp4,mov,m4v,wmv,avi,mpg,mpeg"},
						{title : "Audio files", extensions : "mp3"},
						{title : "Flash files", extensions : "swf"},
						{title : "Text files", extensions : "txt,doc,docx,xls,csv"},
						{title : "PDF files", extensions : "pdf"}
					]
				},

				//flash_swf_url : 'http://adfx.ie/enter/js/Moxie.swf',
				//flash_swf_url : 'Moxie.swf',

				// Silverlight settings
				//silverlight_xap_url : 'Moxie.xap',

				flash_swf_url : 'https://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',

				// Silverlight settings
				silverlight_xap_url : 'https://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',

				// PreInit events, bound before any internal events
		preinit : {
			Init: function(up, info) {
				//log('[Init]', 'Info:', info, 'Features:', up.features);
			},

			UploadFile: function(up, file) {
				//log('[UploadFile]', file);

				// You can override settings before the file is uploaded
				// up.settings.url = 'upload.php?id=' + file.id;
				up.settings.multipart_params['Content-Type'] = file.type;
			}
		},

		// Post init events, bound after the internal events
		init : {
			Refresh: function(up) {
				// Called when upload shim is moved
				//log('[Refresh]');
			},

			StateChanged: function(up) {
				// Called when the state of the queue is changed
				//log('[StateChanged]', up.state == plupload.STARTED ? "STARTED" : "STOPPED");
				if(up.state == plupload.STARTED){
					//console.log('upload started');
					$("#main-content").jLoader({
						backdropOpacity: .7,
						backdropColor: '#FFF',
						content: '<span class="loading">Processing file...</span>'
					});
				} else {

				}

			},

			UploadComplete: function(up){
				$.ajax({
					url: $.copyclear.apipath+'reorganise.files',
					data: 'encdata='+$.copyclear.encdata,
					dataType:"json",
					type: 'post',
					success: function(data){
						if(data.result == 'success'){
							$("#add-files-view").fadeOut('slow');
							$("#edit-files-view").removeClass('hide').hide().fadeIn('slow');
							generate_submission_labels();
						}//end if
					}
				});
				$("#main-content").jLoader('closeLoader');
			},

			QueueChanged: function(up) {
				// Called when the files in queue are changed by adding/removing files
				//log('[QueueChanged]');

				$.copyclear.files_remaining = up.files.length;
			},

			UploadProgress: function(up, file) {
				// Called while a file is being uploaded
				$('.save-uploads-btn').addClass('hide');
				//log('[UploadProgress]', 'File:', file, "Total:", up.total);
			},

			FilesAdded: function(up, files) {
				// Callced when files are added to queue
				//log('[FilesAdded]');
				if (files.length>=2){
					//alert ('too many files');
				}
				//$.copyclear.files_remaining += files.length;
				//console.log('files '+files.length);
				plupload.each(files, function(file) {
					file.name = file.name.replace(/\s+/g, '');
					up.settings.multipart_params.key = timestamp+'_'+file.name;
					up.refresh(); // Reposition Flash/Silverlight
				});
			},

			FilesRemoved: function(up, files) {
				// Called when files where removed from queue
				//log('[FilesRemoved]');

				plupload.each(files, function(file) {
					//log('  File:', file);
				});
			},

			BeforeUpload: function(up, file){
				file.name = file.name.replace(/\s+/g, '');
				up.settings.multipart_params.key = timestamp+'_'+file.name;
				up.refresh(); // Reposition Flash/Silverlight
			},

			FileUploaded: function(up, file, info,role) {
				var xml = $.parseXML(info.response);
				var locationNode = $(xml).find("Location");
				var filename = locationNode.text();
				var upload = new Array();
				upload['filename'] = filename;
				upload['type'] = file.type;
				upload['size'] = file.size;
				moveFile(upload);
				//console.log($.copyclear.files_remaining);
				$.copyclear.files_remaining--;

			},

			ChunkUploaded: function(up, file, info) {
				// Called when a file chunk has finished uploading
				//log('[ChunkUploaded] File:', file, "Info:", info);
			},

			Error: function(up, args) {
				// Called when a error has occured
				//log('[error] ', args);
			}
		}

			});

      },
      "error": function(d,msg) {

      }
    });
}

function generate_submission_labels(){
	$("#show-files-btn").addClass('hide');
	$.ajax({
		url: $.copyclear.apipath + 'get.submission.group',
		data: 'encdata='+$.copyclear.encdata,
		dataType:"json",
		type:'post',
		success: function(data){
			if(data.result == 'success'){
				process_submissions_list(data.list);
			}
		}
	});
}//end function

function process_submissions_list(data){
 	$queue = $("#uploaded-files-queue");
	$queue.html("");
	for($i = 0; $i < data.length; $i++){
		if(data[$i].service == "vimeo" || data[$i].service == "youtube"){
			data[$i].filename = data[$i].filename_full;
		}
		$row = $('<div class="queue-file clearfix" />');
		if(data[$i].type == 'url-website'){
			$head = $('<strong>website &quot;'+data[$i].filename+'&quot;</strong>');
		} else {
			$head = $('<strong>file &quot;'+data[$i].filename+'&quot;</strong>');
		}
		$label = $('<label><small>Label</small></label>');
		$input = $('<input type="hidden" value="'+data[$i].id+'" id="submission-id-'+data[$i].id+'" name="submission_id[]"><input type="text" class="form-control" id="submission-label-'+data[$i].id+'" name="submission_label[]" value="'+data[$i].title+'">');
		$row.append($head);
		$row.append($label);
		$row.append($input);
		$label = $('<label><small>Comment</small></label>');
		$input = $('<textarea rows="2" class="form-control" id="submission-comment-'+data[$i].id+'" name="submission_comment[]">'+data[$i].comment+'</textarea>');
		$row.append($label);
		$row.append($input);
		//console.log($row);
		$queue.append($row);
	}//end for i
}//end function

function createThumbs($file_id,$encdata){
	$.ajax({
		url: $.copyclear.apipath + 'create.thumbs.ajax',
		dataType:"json",
		data: 'file_id='+$file_id+'&encdata='+$.copyclear.encdata,
		type:"post",
		success: function(data){
			//console.log('thumbs response ' + data);
			if(data.result == 'success'){
				//$("#main-content").jLoader('closeLoader');
			}
		}
	});
}

function moveFile(upload){
	$.ajax({
		url: $.copyclear.apipath + 'save.file',
		type:'post',
		dataType:"json",
		data: 'encdata='+$.copyclear.encdata+'&f='+encodeURIComponent(upload['filename'])+'&type='+upload['type']+'&size='+upload['size'],
		success:function(data){
			//$('#entry-files-list').append('<li id="temp-file-'+data.code+'"><i class="icon-cog"></i> Preparing file</li>');
			//$('#temp-file-'+data.code).hide().slideDown('slow');
			//console.log(response);
			if (data.result=="success"){
				if (data.image == true){
					var $file_id = data.file;
					createThumbs($file_id,$.copyclear.encdata);
				}
			} else {
				//$("#main-content").jLoader('closeLoader');
			}
		},
		error: function(e) {

		}
	});
}

function init_comments(){
	$("#submit-comment").click(function(e){
		e.preventDefault();

		$("#comment-form .alert").remove();

		$("#main-content").jLoader({
			backdropOpacity: .7,
			backdropColor: '#FFF',
			content: '<span class="loading">Processing conversation...</span>'
		});

		$.ajax({
			url: $.copyclear.apipath + 'submit.message',
			data: $("#comment-form").serialize(),
			dataType:"json",
			type:'post',
			timeout: function(){
				$("#main-content").jLoader('closeLoader');
				$("#comment-form").prepend('<div class="alert alert-warning"><strong>Error:</strong> Cannot deliver the message.');
			},
			success: function(data){
				$("#main-content").jLoader('closeLoader');
				if(data.result == 'success'){
					location.reload();
				} else {
					$("#comment-form").prepend('<div class="alert alert-warning"><strong>Error:</strong> Cannot deliver the message.');
				}
			}
		});
	});
}

$.fn.addComment = function(data){
	if($("#messages").length == 0){
		var $messages = $('<div id="messages" />');
		$(this).append($messages);
	} else {
		var $messages = $("#messages");
	}

	var $message = '<div class="message-bubble clearfix"> \
					<h4><span><i class="fa fa-user"></i> You just said</span></h4> \
                    <div class="message-content">'+data.message_message+'</div> \
                </div>';
	$messages.prepend($message);
}

function init_archive(){

	update_filter_groups();

	$(".filter-add").click(function(e){
		e.preventDefault();
		var $group = $(this).data('group');
		var $id = $(this).prop('id');
		var $ref_id = $(this).data('id');
		var $label = $(this).text();
		var $field = $(this).data('field');
		if($(this).hasClass('active')){
			//remove the filter
			$("#"+$group+'-'+$ref_id).remove();
			$(this).removeClass('active');
			$(this).find('.fa').removeClass('fa-minus-circle').removeClass('disabled').addClass('fa-plus-circle');
		} else {
			//add the filter
			$(this).addClass('active');
			$(this).find('.fa').removeClass('fa-plus-circle').addClass('fa-minus-circle').addClass('disabled');
			$("#"+$group+' ul').append('<li id="'+$group+'-'+$ref_id+'"><input type="hidden" name="'+$field+'" id="field-'+$group+'-'+$ref_id+'" value="'+$ref_id+'"><span class="label label-primary">'+$label+' <a href="#" class="label-remove" data-target="#'+$id+'"><i class="fa fa-minus-circle"></i></a></span></li>');
		}
		update_filter_groups();
	});

	$(".filter-add.active").each(function(){
		var $group = $(this).data('group');
		var $id = $(this).prop('id');
		var $ref_id = $(this).data('id');
		var $label = $(this).text();
		var $field = $(this).data('field');

		//add the filter
		$(this).addClass('active');
		$(this).find('.fa').removeClass('fa-plus-circle').addClass('fa-minus-circle').addClass('disabled');
		$("#"+$group+' ul').append('<li id="'+$group+'-'+$ref_id+'"><input type="hidden" name="'+$field+'" id="field-'+$group+'-'+$ref_id+'" value="'+$ref_id+'"><span class="label label-primary">'+$label+' <a href="#" class="label-remove" data-target="#'+$id+'"><i class="fa fa-minus-circle"></i></a></span></li>');

		update_filter_groups();
	});

	$('#seach-filters').on('click','a.label-remove',function(e){
		e.preventDefault();
		//console.log('a');
		var $target = $(this).data('target');
		$(this).parent().parent().remove();
		$($target).removeClass('active');
		$($target).find('.fa').removeClass('fa-minus-circle').removeClass('disabled').addClass('fa-plus-circle');
		update_filter_groups();
	});

	$("#search-btn").click(function(e){
		e.preventDefault();
		$("#form-search").submit();
	});

	if($('#year-only').is(':checked')){
		$('.datepicker').datepicker({
			format: "yyyy",
			minViewMode: 2,
			autoclose: true
		});
	} else {
		$('.datepicker').datepicker({
			format: "M yyyy",
			minViewMode: 1,
			autoclose: true
		});
	}

	$('#year-only').click(function(){
		$('.datepicker').datepicker('remove');
		if($(this).is(":checked")){
			$('.datepicker').datepicker({
				format: "yyyy",
				minViewMode: 2,
				autoclose: true
			});
		} else {
			$('.datepicker').datepicker({
				format: "M yyyy",
				minViewMode: 1,
				autoclose: true
			});
		}
		$('.datepicker').each(function(){
			$(this).val("");
			var $target = $(this).data('target');
			$($target).addClass('hide');
		});

	});

	$('.datepicker').datepicker().on('changeDate',function(e){
		var $target = $(this).data('target');
		$($target + ' > span').text($(this).val());
		if($(this).val() != ""){
			$($target).removeClass('hide');
		} else {
			$($target).addClass('hide');
		}
	});
}

function update_filter_groups(){
	$(".filter-group").each(function(){
		if($('ul li',this).length > 0){
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}
