/************************************************************/
/*	jQuery jLoader 0.1.1 - 15/04/13							*/
/*															*/
/*	UPDATES													*/
/*	11/04/13	- 	Plug-in created							*/
/*															*/
/************************************************************/

$().ready(function(){
	(function( $ ){	
		
		var $trigger;
		var $window = $('<div class="loader-window" />');	
		var $container = $('<div class="loader-container" />');			
		var params;
				
		var methods = {
			init : function( options ) {				
				params = $.extend({				
					backdropClass: 'backdrop',
					loadAttribute: 'href',
					backdropColor: '#000',
					backdropOpacity: .9,
					blockLevel : false,
					content: 'Loading...'				
				},options);	
				
				$trigger = $(this);
				
				//add the backdrop
				methods.createBackdrop();
				
				//add the window
				methods.addWindow();					
			},
			
			createBackdrop : function(){
				var $backdropCss = {
					'display' : 'block',
					'position' : 'fixed',	
					'overflow-y' : 'auto',	
					'overflow-x': 'hidden',				
					'opacity' : params.backdropOpacity,					
					'z-index' : 10000,
					'top' : 0,
					'left' : 0,
					'width' : $(window).width(),
					'height' : $(window).height(),
					'background-color' : params.backdropColor
				};
				
				var $backdrop = $('<div class="'+params.backdropClass+'" />').css($backdropCss).click(function(){
					methods.closePopup();
				});
				$backdrop.hide();
				if(params.blockLevel){
					$trigger.css({'position': 'relative'});
					$trigger.append($backdrop);
				} else {
					$('body').append($backdrop);
				}
				
				$backdrop.fadeIn("slow");	
				
				$(window).resize(function(){
					$('.'+params.backdropClass).css({
						'width' : $(window).width(),
						'height' : $(window).height()
					});	
				});
			},
			
			removeBackdrop : function(){
				if($('.'+params.backdropClass).length > 0){
					$('.'+params.backdropClass).fadeOut("slow",function(){
						$(this).remove();	
					});	
				}				
			},
			
			closeLoader : function(){
				methods.removeBackdrop();	
				
				$window.fadeOut("slow",function(){
					$(this).remove();	
					//console.log("removing window");	
				});
			},			
			
			addWindow : function(){				
				$window.hide();	
				$window.css({
					'display' : 'block',
					'position' : 'fixed',	
					'overflow-y' : 'auto',	
					'overflow-x': 'hidden',
					'z-index' : 10002,
					'top' : 0,
					'left' : 0,
					'width' : $(window).width(),
					'height' : $(window).height()
				});			
				
				$container.css({
					'display' : 'block',
					'height' : '30px',
					'width' : '100px',	
					'position' : 'absolute',
					'text-align' : 'center',
					'top' : '50%',
					'left' : '50%',
					'margin-top' : -15,
					'margin-left' : -50
				});
				
				//console.log(params.content);
				$container.html(params.content);
				
				$window.append($container);				
				$('body').append($window);
				$window.fadeIn("slow");		
			
			}
		}

		$.fn.jLoader = function( method ) {    
			if ( methods[method] ) {
			  return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
			  return methods.init.apply( this, arguments );
			} else {
			  $.error( 'Method ' +  method + ' does not exist on jQuery.pagePreloader' );
			}      
		};
	
	})( jQuery );

});