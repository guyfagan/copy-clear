// JavaScript Document

$.copyclear = $settings;
//$.copyclear.encdata = 'YTozOntzOjI6ImlkIjtzOjM6IjQ0OSI7czoxMjoiY29tcGFueV9kYXRhIjthOjI6e3M6MTI6ImNvbXBhbnlfbmFtZSI7czoxMzoiQ2hpcHN0ZXIgTXVuayI7czoxMDoiY29tcGFueV9pZCI7czozOiIxODkiO31zOjExOiJicmFuZF9vd25lciI7YjowO30';//Only for testing

$(document).ready(function(e) {
	actions_interceptor();

//	init_brands();//testing
});

function actions_interceptor(){
	//Login
	if($("#login-form").length > 0){
		$("#login-btn").click(function(e){
			e.preventDefault();
			$("#login-form").submit();
		});

		$("#login-form").submit(function(e){
			e.preventDefault();
			var $form = $(this);
			$('.alert').remove();
			$.ajax({
				url: $.copyclear.apipath + 'login',
				data: $form.serialize(),
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
  					if(!data.is_admin){
    					if(data.company_active){
    						if(data.brand_associated){
    							location.href = $.copyclear.basepath + 'home/';
    						} else {
    							location.href = $.copyclear.basepath + 'associate/';
    						}
      				} else {
        				location.href = $.copyclear.basepath + 'terms/';
    				  }//end if
    				} else {
              location.href = $.copyclear.basepath + 'home/';
    				}
					} else {
						$form.prepend('<div class="alert alert-danger"><strong>Error</strong> Username and/or password are incorrect, try again.</div>');
					}
				}
			});
		});

		//forgot password functionality
		$("#submit-email-fpw").click(function(e){
			e.preventDefault();
			$("#forgot-pwd-modal .modal-body .alert").remove();
			$.ajax({
				url: $.copyclear.apipath + 'forgot.pwd',
				data: 'email='+$("#email").val(),
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						$("#submit-email-fpw").addClass('disabled');
						$("#forgot-pwd-modal .modal-body form").fadeOut('slow',function(){
							$("#forgot-pwd-modal .modal-body").append('<div class="alert alert-success">We just sent you an email with the reset link!</div>');
							setTimeout(function(){
								$("#forgot-pwd-modal").modal('hide');
								$("#forgot-pwd-modal .modal-body .alert").remove();
								$("#forgot-pwd-modal .modal-body form").show();
								$("#email").val('');
								$("#submit-email-fpw").removeClass('disabled');
							},5000);
						});
					} else {
						$html = '<div class="alert alert-warning"><strong>Error:</strong> '+data.errors[0]+'</div>';
						$("#forgot-pwd-modal .modal-body form").prepend($html);
					}
				}
			});
		});
	}//end if //Login

  //Terms & Conditions
  if($('#accept-terms-btn').length > 0){
    $('#accept-terms-btn').click(function(e){
      e.preventDefault();
      console.log('aaa');
      $.ajax({
				url: $.copyclear.apipath + 'reactivate.company',
				dataType:"json",
				type: "get",
				success: function(data){
          if(data.result == 'success'){
						if(data.brand_associated){
							location.href = $.copyclear.basepath + 'home/';
						} else {
							location.href = $.copyclear.basepath + 'associate/';
						}
          } else {

          }
				}
      });
    });
  }

	//Reset password
	if($("#reset-password-form").length > 0){
		$("#reset-password-btn").click(function(e){
			e.preventDefault();
			var $form = $("#reset-password-form");
			$("#validated-form .alert").remove();
			$.ajax({
				url: $.copyclear.apipath + 'reset.pwd',
				data: $form.serialize(),
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						$("#validated-form").slideUp('slow',function(){
							var $div = $('<div class="alert alert-success">Your new password has been set! <a href="'+$.copyclear.basepath+'" class="btn btn-default  btn-xs pull-right">Login now</a></div>');
							$form.prepend($div);
							$div.hide().slideDown('slow');
						});
					} else {
						var $div = $('<div class="alert alert-warning"><strong>We found a problem:</strong> '+data.errors[0]+'</div>');
						$("#validated-form").prepend($div);
					}//end if
				}
			});
		});
	}//end if //Reset Password

	//Register
	if($("#register-form").length > 0){
		$("#info-2").hide();
		$("#info-1").show();
		$("#page-title").html("Find your company");
		$.copyclear.newcompany = true;
		$('#company-name').on('keyup',function(){
			var $search = $(this).val();
			$.ajax({
				url: $.copyclear.apipath + 'search.companies',
				data: 'search='+$search,
				dataType:"json",
				type:'post',
				success: function(data){
					$("#company-search > li").remove();
					if(data.result == 'success'){
						$("#company-search").removeClass('hide');
						for(i = 0; i < data.data.length; i++){
							$("#company-search").append('<li><label><input type="radio" id="company-id-'+data.data[i].id+'" data-label="'+data.data[i].name+'" name="company_selector" value="'+data.data[i].id+'"> '+data.data[i].name+'</label></li>');
						}
						$("#company-search li input").off('click');
						$("#company-search li input").on('click',function(e){
							//e.preventDefault();
							var $company_id = $(this).val();
							$("#company-id").val($company_id);
							$.copyclear.company_id = $company_id;
							$("#company-search li").removeClass('active');
							$(this).parent().addClass('active');
							$('#company-name').val($(this).data('label'));
							$.copyclear.newcompany = false;
						});
					} else {
						$("#company-search").addClass('hide');
					}//end if
				}
			});
		});

		$('#add-user-details').click(function(e){
			e.preventDefault();
			$('.alert').remove();
			if($('#company-name').val() != ""){
				$('#company-finder').addClass('hide').slideUp('slow');
				$('#basic-registration').removeClass('hide').hide().slideDown('slow');
				$("#info-1").fadeOut('slow',function(){
					$("#info-2").removeClass('hide').hide().fadeIn('slow');
				});
				$("#page-title").html("Your Company Details");
				if($.copyclear.newcompany == false){
					$('.new-company-opt').hide();
				} else {
					$('.new-company-opt').show();
				}
			} else {
				$("#company-name-box").prepend('<div class="alert alert-danger">Please enter a company name</div>');
			}
		});

		$('a[data-action="back"]').click(function(e){
			e.preventDefault();
			var $step = $(this).data('step');

			switch($step){
				case 1:
					$("#page-title").html("Find your company");
					$('#company-finder').removeClass('hide').show();
					$('#basic-registration').addClass('hide').hide();
					$("#step2").addClass('hide').hide();
					$("#step1").removeClass('hide').show();
					break;
				case 2:
					$("#page-title").html("Add Brand Owners");
					$("#step3").addClass('hide').hide();
					$("#step2").removeClass('hide').show();
					break;
			}
		});

		$('#save-user-btn').click(function(e){
			e.preventDefault();
			$('.alert').remove();
			var $form = $("#register-form");
			var isValid = $form.validate();
			if(isValid){
				$.ajax({
					url: $.copyclear.apipath + 'register.user',
					data: $form.serialize(),
					dataType:"json",
					type:'post',
					success: function(data){
						if(data.result == 'success'){
							$.copyclear.encdata = data.encdata;
							$("#step1").slideUp('slow',function(){
								$(this).addClass('hide');
							});
							$.copyclear.brands_owners = data.bo;
							$.copyclear.brands = data.b;
							$.copyclear.brand_owner = data.brand_owner;
							if(data.brand_owner == true){
								$("#step3").removeClass('hide').hide().slideDown('slow');
								//console.log($.copyclear.encdata);
								init_brands();
							} else {
								$("#step2").removeClass('hide').hide().slideDown('slow');
								//console.log($.copyclear.encdata);
								init_brands_owners();
							}
						} else {
							$errors = data.errors;
							$errors_list = $('<ul>');
							for(i = 0; i < $errors.length; i++){
								$errors_list.append('<li>'+$errors[i]+'</li>');
							}//end for
							$errors_html = $('<div class="alert alert-danger"><strong>Please fill in the required fields below:</strong></div>');
							$errors_html.append($errors_list);
							$("#basic-registration").prepend($errors_html);
						}//end if
					}
				});
			} else {
				$errors = $.copyclear.missingFields;
				$errors_list = $('<ul>');
				for(i = 0; i < $errors.length; i++){
					$errors_list.append('<li>'+$errors[i]+'</li>');
				}//end for
				$errors_html = $('<div class="alert alert-danger"><strong>Please fill in the required fields below:</strong></div>');
				$errors_html.append($errors_list);
				$("#basic-registration").prepend($errors_html);
			}//end if
		});
	}

	if($("#associate-brands").length > 0){
		init_association();
	}//end if

	if($("#associate-brands-details").length > 0){
		init_association_details();
	}
}

function init_brands(){

	$("#page-title").html("Add Your Brands");

	$.copyclear.savedBrands = [];

	var repos = new Bloodhound({
		datumTokenizer: function(d) { return d.tokens; },
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: 'api/ta.get.brands?search=%QUERY&encdata='+$.copyclear.encdata
		}
	});

	repos.initialize();

	if(typeof($.copyclear.brands) != 'undefined'){
		if($.copyclear.brands.length > 0){
			for(i = 0; i < $.copyclear.brands.length; i++){
				$("#brands-list").append('<li><input type="hidden" name="brands[]" id="brand-id-'+$.copyclear.brands[i].id+'" value="'+$.copyclear.brands[i].id+'">'+$.copyclear.brands[i].name+'</li>');
				$.copyclear.savedBrands.push($.copyclear.brands[i]);
			}
		}//end if
	}//end if

	$('#brand-name').typeahead(null, {
		name: 'brands',
		displayKey: 'name',
		source: repos.ttAdapter(),
		templates: {
			suggestion: Handlebars.compile([
				'<p class="repo-name">{{name}}</p>',
				].join(''))
		}
	});

	$('#brand-name').bind('keyup',function(){
		delete $.copyclear.currentBrand;
	});

	$('#brand-name').bind('typeahead:selected typeahead:autocompleted', function(obj, datum, name) {
		$.copyclear.currentBrand = datum;
	});

	populate_advertisers();

	$("#add-brand").click(function(e){
		e.preventDefault();
		if(typeof($.copyclear.currentBrand) == 'undefined'){
			$("#no-results-message").removeClass('hide').hide().slideDown('slow');
			var context = {query: $("#brand-name").val()};
			var source = $("#no-results-message-template").html();
			var template = Handlebars.compile(source);
			var html = template(context);
			$("#hb-message").html(html);
		} else {
			$("#no-results-message").addClass('hide');
			addBrandsOnSidebar();
		}
	});

	$("#save-brand-btn").click(function(e){
		e.preventDefault();
		var $form = $('#new-brand-form');
		var $name = $("#brand-name").val();
		$.ajax({
			url: $.copyclear.apipath + 'save.brand',
			dataType:"json",
			data: $form.serialize() + '&name='+$name+'&encdata='+$.copyclear.encdata,
			type: 'post',
			success: function(data){
				if(data.result == 'success'){
					$.copyclear.currentBrand = {};
					$.copyclear.currentBrand.id = data.id;
					$.copyclear.currentBrand.name = data.name;
					$("#no-results-message").addClass('hide');
					addBrandsOnSidebar();
					$("#new-brand-form input, #new-brand-form select").val("");
				}
			}
		});
	});

	$('#saved-brands').submit(function(e){
		e.preventDefault();
	});

	$("#save-brands-btn").click(function(e){
		e.preventDefault();
		var $form = $("#saved-brands");
		$.ajax({
			url: $.copyclear.apipath + 'save.brands.rel',
			dataType:"json",
			type:'post',
			data: $form.serialize() + '&encdata='+$.copyclear.encdata,
			success: function(data){
				if(data.result == 'success'){
					$("#step3").slideUp('slow',function(){
						$(this).addClass('hide');
					});
					$("#step4").removeClass('hide').hide().slideDown('slow');
					$("#page-title").html('Thank You');
				} else {
					//error
				}
			}
		});
	});
}

function populate_advertisers(){
	$.ajax({
		url: $.copyclear.apipath + 'get.company.brands.owners',
		data: 'encdata='+$.copyclear.encdata,
		dataType:"json",
		type:'post',
		success: function(data){
			if(data.result == 'success'){
				var $select = $("#company-brand-advetiser");
				$select.html('');
				for(i = 0; i < data.data.length; i++){
					var $option = '<option value="'+data.data[i].brand_owner_id+'">'+data.data[i].brand_owner_name+'</option>';
					$select.append($option);
				}//end for i
			}//end if
		}
	});
}

function addBrandsOnSidebar(){
	$("#brands-list").append('<li><input type="hidden" name="brands[]" id="brand-id-'+$.copyclear.currentBrand.id+'" value="'+$.copyclear.currentBrand.id+'">'+$.copyclear.currentBrand.name+'</li>');
	$.copyclear.savedBrands.push($.copyclear.currentBrand);
	$("#brand-name").val("");
	delete $.copyclear.currentBrand;
}

function init_brands_owners(){

	$("#page-title").html("Add Brand Owners");

	$.copyclear.savedBrandsOwners = [];

	var repos = new Bloodhound({
		datumTokenizer: function(d) { return d.tokens; },
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: 'api/ta.get.brands.owners?search=%QUERY'
		}
	});

	if(typeof($.copyclear.brands_owners) != 'undefined'){
		if($.copyclear.brands_owners.length > 0 && $.copyclear.brands_owners != false){
			for(i = 0; i < $.copyclear.brands_owners.length; i++){
				$("#advertiser-list").append('<li><input type="hidden" name="advertisers[]" id="advertiser-id-'+$.copyclear.brands_owners[i].id+'" value="'+$.copyclear.brands_owners[i].id+'">'+$.copyclear.brands_owners[i].name+'</li>');
				$.copyclear.savedBrandsOwners.push($.copyclear.brands_owners[i]);
			}
		}
	}//end if

	repos.initialize();

	$('#brand-owner-name').typeahead(null, {
		name: 'brands_owners',
		displayKey: 'name',
		source: repos.ttAdapter(),
		templates: {
			suggestion: Handlebars.compile([
				'<p class="repo-name">{{name}}</p>',
				].join(''))
		}
	});

	$('#brand-owner-name').bind('keyup',function(){
		delete $.copyclear.currentBrandOwner;
	});

	$('#brand-owner-name').bind('typeahead:selected typeahead:autocompleted', function(obj, datum, name) {
		$.copyclear.currentBrandOwner = datum;
	});

	$("#add-advertiser").click(function(e){
		e.preventDefault();
		if(typeof($.copyclear.currentBrandOwner) == 'undefined'){
			createNewAdvertiser();
		} else {
			addAdvertisersOnSidebar();
		}
	});

	$("#save-advertisers-btn").click(function(e){
		e.preventDefault();
		$.ajax({
			url: $.copyclear.apipath + 'save.advertiser.rel',
			dataType:"json",
			type:'post',
			data: $("#advertiser-registration").serialize() + '&encdata='+$.copyclear.encdata,
			success: function(data){
				if(data.result == 'success'){
					$("#step2").slideUp('slow',function(){
						$(this).addClass('hide');
					});

					$("#step3").removeClass('hide').hide().slideDown('slow');
					//console.log($.copyclear.encdata);
					init_brands()
				}//end if
			}
		});
	});

}

function createNewAdvertiser(){
	var $name = $("#brand-owner-name").val();
	$.ajax({
		url: $.copyclear.apipath + 'save.advertiser',
		data: 'name='+$name+'&encdata='+$.copyclear.encdata,
		type:'post',
		dataType:"json",
		success: function(data){
			if(data.result == 'success'){
				$.copyclear.currentBrandOwner = {};
				$.copyclear.currentBrandOwner.id = data.id;
				$.copyclear.currentBrandOwner.name = $name;
				addAdvertisersOnSidebar();
			} else {
				//error
			}
		}
	});

}

function addAdvertisersOnSidebar(){
	//console.log('adding the advertiser in the list');
	$("#advertiser-list").append('<li><input type="hidden" name="advertisers[]" id="advertiser-id-'+$.copyclear.currentBrandOwner.id+'" value="'+$.copyclear.currentBrandOwner.id+'">'+$.copyclear.currentBrandOwner.name+'</li>');
	$.copyclear.savedBrandsOwners.push($.copyclear.currentBrandOwner);
	$("#brand-owner-name").val("");
	delete $.copyclear.currentBrandOwner;
}

function init_association(){
	$("input[data-type=brand]").click(function(){
		var $group = $(this).data('target-group');
		var $groupMaxElements = $('input[data-target-group='+$group+']').length;
		if($('input[data-target-group='+$group+']:checked').length > 0){
			if($('input[data-target-group='+$group+']:checked').length >= $groupMaxElements){
				$($group).prop('checked',true);
				$($group).prop('indeterminate',false);
			} else {
				$($group).prop('checked',true);
				$($group).prop('indeterminate',true);
			}
		} else {
			$('#'+$group).prop('indeterminate',false);
			$('#'+$group).prop('checked',false);
		}
	});

	$('input[data-ref=brands-group]').click(function(){
		var $group = $(this).prop('id');
		if($(this).is(':checked')){
			$('input[data-target-group="#'+$group+'"]').prop('checked',true);
		} else {
			$('input[data-target-group="#'+$group+'"]').prop('checked',false);
		}
	});

	$("#save-associated-brands-btn").click(function(e){
		e.preventDefault();
		var $form = $("#associate-brands");

		$.ajax({
			url: $.copyclear.apipath + 'update.company.brands',
			data: $form.serialize(),
			dataType:"json",
			type:'post',
			success: function(data){
				if(data.result == 'success'){
					location.href = $.copyclear.basepath + "associate/details/";
				} else {
					//error
				}
			}
		});
	});

}

function init_association_details(){
	$("#save-associated-brands-details-btn").click(function(e){
		e.preventDefault();
		var $form = $("#associate-brands-details");
		var isValid = $form.validate();
		if(isValid){
			$.ajax({
				url: $.copyclear.apipath + 'update.brands.details',
				data: $form.serialize(),
				dataType:"json",
				type:'post',
				success: function(data){
					if(data.result == 'success'){
						location.href = $.copyclear.basepath + "home/";
					} else {
						//error
					}
				}
			});
		} else {
			$errors = $.copyclear.missingFields;
			$errors_list = $('<ul>');
			for(i = 0; i < $errors.length; i++){
				$errors_list.append('<li>'+$errors[i]+'</li>');
			}//end for
			$errors_html = $('<div class="alert alert-danger"><strong>Please fill in the required fields below:</strong></div>');
			$errors_html.append($errors_list);
			$("#associate-brands-details").prepend($errors_html);
			$('html,body').scrollTop(1);
		}//end if
	});
}

$.fn.validate = function(){
	var valid = true;
	$.copyclear.missingFields = new Array();
	$("input[required],select[required],textarea[required]").each(function(){
		$(this).closest('.input-group').removeClass("error");
		if($(this).is(":checkbox")){
			var className = $(this).prop('class');
			if($("."+className+":checked").length == 0){
				var $label = $(this).data("label");
				if($label == "undefined"){
					$label = $(this).parent().find("label").text();
				}
				$.copyclear.missingFields.push($label);
				valid = false;
			}
		} else {
			if($(this).val() == ""){
				$(this).closest('.input-group').addClass("error");
				var $label = $(this).data("label");
				if($label == "undefined"){
					$label = $(this).closest("label").text();
				}
				$.copyclear.missingFields.push($label);
				valid = false;
			} else if($(this).prop("type") == "email"){
				var validEmail = validateEmail($(this).val());
				if(!validEmail){
					$(this).closest('.input-group').addClass("error");
					var $label = "Please enter a valid email address";
					$.copyclear.missingFields.push($label);
					valid = false;
				}
			}//end if
		}
	});
	return valid;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}