<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CopyClear</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Old Hat - http://oldhat.ie">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <link href="/ccci/html/dashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/ccci/html/dashboard/css/bootstrap-theme.min.css" rel="stylesheet">	
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->
    <script src="/ccci/html/dashboard/js/handlebars.js"></script>
	<script src="/ccci/html/dashboard/js/jquery-1.11.0.min.js"></script>
    <script src="/ccci/html/dashboard/js/bootstrap.min.js"></script>
    <script src="/ccci/html/dashboard/inc/config.js.inc.php"></script>
    <script src="/ccci/html/dashboard/js/typeahead.bundle.js"></script>  
    <script>
		$(document).ready(function(e) {
			var repos = new Bloodhound({
				datumTokenizer: function(d) { return d.tokens; },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				remote: {
					url: 'api/ta.get.companies?search=%QUERY'
				}
			});
			 
			repos.initialize();
			 
			$('#test').typeahead(null, {
				name: 'companies',
				displayKey: 'name',
				source: repos.ttAdapter(),
				templates: {
					suggestion: Handlebars.compile([						
						'<p class="repo-name">{{name}}</p>',						
						].join(''))
				},
				selected: function(){
					console.log();	
				}
			});
			
			$('#test').bind('typeahead:selected', function(obj, datum, name) {      
        //alert(JSON.stringify(obj)); // object
        // outputs, e.g., {"type":"typeahead:selected","timeStamp":1371822938628,"jQuery19105037956037711017":true,"isTrigger":true,"namespace":"","namespace_re":null,"target":{"jQuery19105037956037711017":46},"delegateTarget":{"jQuery19105037956037711017":46},"currentTarget":
        //alert(JSON.stringify(datum)); // contains datum value, tokens and custom fields
        // outputs, e.g., {"redirect_url":"http://localhost/test/topic/test_topic","image_url":"http://localhost/test/upload/images/t_FWnYhhqd.jpg","description":"A test description","value":"A test value","tokens":["A","test","value"]}
        // in this case I created custom fields called 'redirect_url', 'image_url', 'description'   

       // alert(JSON.stringify(name)); // contains dataset name
        // outputs, e.g., "my_dataset"
		console.log(datum.id);

});
			
			/*
			$("#submit").click(function(e){
				e.preventDefault();
				$.ajax({
					url: 'api/test'
					
				});	
			});*/
			
        });
	</script>
    </head>
	<body>
    	<input type="text" class="form-control" name="test" id="test">
        <button type="button" id="submit">Send</button>
	</body>	
</html>