<?php
	$ads = $utils->call('ads');
	$encdata = $_REQUEST['encdata'];
	$encdata = unserialize(base64_decode(urldecode($encdata)));
	$submission_id = $encdata['submission_id'];
	$data = $ads->get_submission(array('id' => $submission_id,'detailed' => true));
?>
<h1>Submit New Ad</h1>
<section class="row">
    <div class="col-md-3">
        <h2>Steps</h2>
        <ul>
            <li class="step complete"><i class="fa fa-check"></i> 1 Submission Details</li>
            <li class="step complete"><i class="fa fa-check"></i> 2 Upload Material</li>
            <li class="step complete"><i class="fa fa-check"></i> 3 Approval Details</li>
            <li class="step current">4 Confirm &amp; Submit</li>
        </ul>
        <hr />
        <div class="helpbox">
        	<p>Please take a minute to review the details of your submission. If you are happy with it click 'Confirm and Submit'.</p>
        </div>
    </div>
    <!-- start right-->
    <div class="col-md-9">
        <h2 class="sub_section clearfix">Step 4 - Confirm Submission</h2>
        <div class="box databox">
            <form method="post" name="new-submission-confirm" id="new-submission-confirm" action="#">
            	<input type="hidden" name="encdata" id="encdata" value="<?php echo $_REQUEST['encdata'] ?>">
                <table>
                    <tr>
                        <td class="labels">Advertiser</td>
                        <td class="data"><?php echo $data['brand_owner_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Brand</td>
                        <td class="data"><?php echo $data['brand_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Campaign</td>
                        <td class="data"><?php echo $data['campaign_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Medium</td>
                        <td class="data"><?php echo $data['ad_medium_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Ad Title</td>
                        <td class="data"><?php echo $data['ad_title'] ?></td>
                    </tr>
                    <?php
					if($data['ad_submission_support'] != 1){
					?>
                    <tr>
                        <td class="labels">Aproval Sought</td>
                        <td class="data"><?php echo ucwords($data['ad_submission_approval_sought']) ?> Approval</td>
                    </tr>
                    <?php
					}//end if
					?>
                    <tr>
                        <td class="labels">Substantiation</td>
                        <td class="data"><?php
						if($data['ad_submission_support'] == 1){
							echo 'Yes';
						} else {
							echo 'No';
						}//end if
						?></td>
                    </tr>
                    <tr>
                        <td class="labels">Origin of Work</td>
                        <td class="data"><?php
                        	switch($data['ad_submission_origin']){
								default:
								case 'ireland':
									echo 'Created in Ireland';
								break;
								case 'adapted':
									echo 'Adaptation of international material';
								break;
								case 'elsewhere':
									echo 'Created elsewhere';
								break;
							}
						?></td>
                    </tr>
                    <tr>
                        <td class="labels">Comments</td>
                        <td class="data"><?php echo $data['ad_submission_comments'] ?></td>
                    </tr>
                </table>
            </form>
            <hr />
            <h3>Submission files</h3>
            <table class="table submissions-list">
            	<thead>
                	<tr>
                        <th>Title</th>
                        <th width="20%"><span class="pull-right">Download</span></th>
                        <!--th width="15%">&nbsp;</th-->
                    </tr>
                </thead>
                <tbody>
            <?php
			$tools = $utils->call('tools');
			$list = $ads->get_submission_group_items(array('group_id' => $encdata['submission_group_id']));
			if($list !== false){
				for($i = 0; $i < sizeof($list); $i++){
					$sub_encdata = array('id' => $list[$i]['ad_submission_id'], 'group_id' => $encdata['submission_group_id']);
					$sub_encdata = urlencode(base64_encode(serialize($sub_encdata)));
					$title = $list[$i]['ad_submission_title'];
					if($title == ""){
						$title = $list[$i]['attachment_filename'];
					}//end if
					if($list[$i]['attachment_type'] == 'url-website'){
						$download = '<a href="'.$tools->check_http($list[$i]['attachment_url']).'" target="_blank"><i class="fa fa-external-link"></i> Open Link</a>';
					} else {
						$download = '<a href="'.$list[$i]['attachment_url'].'" target="_blank"><i class="fa fa-arrow-circle-o-down"></i> Download</a>';
					}//end if
					if($list[$i]['attachment_type'] == 'image'){
						$download = '<a href="'.__UPLOADPATH__.'photos/o/'.$list[$i]['attachment_filename'].'" target="_blank"><i class="fa fa-arrow-circle-o-down"></i> Download</a>';
						$thumb = '<a href="'.__UPLOADPATH__.'photos/l/'.$list[$i]['attachment_filename'].'" rel="prettyPhoto"><img src="'.__UPLOADPATH__.'photos/t/'.$list[$i]['attachment_filename'].'" alt="'.$title.'" class="table-thumb"></a>';
						$title = $thumb.$title;
					}//end if
			?>
            		<tr>
                        <td><?php echo $title ?></td>
                        <td><span class="pull-right"><?php echo $download ?></span></td>
                        <!--td><a href="#" class="btn btn-danger btn-xs pull-right" data-role="delete-submission" data-submission-data="<?php echo $sub_encdata ?>">Delete</a></td-->
                    </tr>
            <?php
				}//end for i
			}//end if
			?>
            	</tbody>
            </table>
        </div><!-- /.end box-->
        <a href="<?php echo __BASEPATH__ ?>submission/details/?encdata=<?php echo $_REQUEST['encdata'] ?>" class="btn btn-default pull-left">Back to step 3</a>
	    <button type="button" class="btn btn-primary" id="complete-submission-btn">Confirm and Submit</button>
    </div><!-- /.end col-md-9-->
</section>
<div class="modal fade" id="warning-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Warning</h4>
      </div>
      <div class="modal-body">
        <p>You can't delete all the files.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="error-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Error</h4>
      </div>
      <div class="modal-body">
        <p>You can't delete all the files.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="confirm-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="fa fa-warning"></i> Warning</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete the selected file?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" id="confirm-delete-btn">Yes, delete it</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
