<?php
	#ignore template
	
	if(in_array($user_data['group_id'],$admins)){
		header("location: ".__BASEPATH__."admin/");
	}
	
	$ads = $utils->call('ads');
	$campaigns = $utils->call('campaigns');
	$ads_list = $ads->get_ads(array('company_id' => $company_id, 'limit' => 10,'order_by_submission' => true, 'days' => $user_days_range));
?>
<h1>Your Dashboard</h1>
<section class="row">
    <div class="col-md-3">
        <?php
		require_once('inc/snippets/keys.inc.php');
		?>
        <hr />
        <div class="helpbox">
        	<p>Your dashboard gives an overview of your submissions.<br />Submit new ads or monitor the progress of work already submitted.</p>
        </div>
    </div> 
	<div class="col-md-9">
        <div class="sub_section clearfix">
            <h2>Submit New Ad</h2>
            <div class="box">
                <div class="row">
                    <p class="col-md-9">There are four simple steps to adding a new submission. You’ll find handy hints on the left. To start, click ‘submit new ad’. </p>
                    <ul class="col-md-3">
                        <li><a class="btn btn-primary" href="<?php echo __BASEPATH__ ?>submission/new/">Submit New Ad</a></li>
                    </ul>
                </div>
            </div>
            <!-- end box-->
        </div>
        <!-- start accordion -->
        <div class="panel-group" id="accordion">
       		<h2>Latest Feedback</h2>
            <?php
			if($ads_list !== false){
                 for($x = 0; $x < sizeof($ads_list); $x++){
					 $submissions = $ads->get_submissions(array('company_id' => $company_id, 'ad_id' => $ads_list[$x]['ad_id'], 'days' => $user_days_range));
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-<?php echo $ads_list[$x]['ad_id'] ?>">
                        <?php echo $ads_list[$x]['brand_name']." / ".$ads_list[$x]['campaign_name']." / ".$ads_list[$x]['ad_title'] ?>
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-<?php echo $ads_list[$x]['ad_id'] ?>" class="panel-collapse collapse<?php
                    if($i == 0){
                        echo ' in';	
                    }//end if
                    ?>">
                    <div class="panel-body">                
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Date Modified</th>                                  
                                    <th>Submission Title</th>
                                    <th>Medium</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/user.panel.list.inc.php');
							?>
                            </tbody>                            
                        </table>
                    
                
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
           </div><!-- /.panel -->		
           <?php
				}//end for x
			} else {
			?>
            <div class="box clearfix">There are no submissions at this time</div>
            <?php
			}//end if
		?>        
        </div><!-- end accordion -->
        <ul>
            <li><a class="btn btn-primary" href="<?php echo __BASEPATH__ ?>campaigns/all/">View All Campaigns</a></li>
        </ul>
	</div>
</section>