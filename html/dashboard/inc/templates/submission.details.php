<?php
	$ads = $utils->call('ads');
	$encdata = $_REQUEST['encdata'];
	$encdata = unserialize(base64_decode(urldecode($encdata)));				
	$submission_id = $encdata['submission_id'];	
	$data = $ads->get_submission(array('id' => $submission_id,'detailed' => true));
	$supporting = false;
	if($data['ad_submission_support'] == 1){
		$supporting = true;	
	}//end if
?>
<h1>Submit New Ad</h1>
<section class="row">
    <div class="col-md-3">
        <h2>Steps</h2>   
        <ul>
            <li class="step complete"><i class="fa fa-check"></i> 1 Submission Details</li>
            <li class="step complete"><i class="fa fa-check"></i> 2 Upload Material</li>
            <li class="step current">3 Approval Details</li>
            <li class="step next">4 Confirm &amp; Submit</li>
        </ul>    
        <hr />
        <div class="helpbox">
        	<p>Select the type of approval sought and the origin of the work.<br />You can add general comment for the submission.</p>
        </div>    
    </div>
    <!-- start right-->
    <div class="col-md-9">
        <h2 class="sub_section clearfix">Step 3 - Approval Details</h2>
        <div class="box">
            <form method="post" name="new-submission-final" id="new-submission-final" action="#">     
            	<input type="hidden" name="encdata" id="encdata" value="<?php echo $_REQUEST['encdata'] ?>">    
                <?php
				if($supporting === false){
				?>
                <label>Approval Sought</label>
                <select id="approval-sought" name="approval_sought" class="form-control">
                	<option value="interim">Interim Approval</option>
                    <option value="final">Final Approval</option>
                </select>
                <?php
				} else {
				?>
                <input type="hidden" name="approval_sought" id="approval-sought" value="support">
                <?php
				}//end if
				?>
                <label>Origin of Work</label>
                <select id="origin" name="origin" class="form-control">
                	<option value="ireland">Created in Ireland</option>
                    <option value="adapted">Adaptation of international material</option>
                    <option value="elsewhere">Created elsewhere</option>
                </select>
                <label>Comments</label>
                <textarea name="comments" id="comments" class="form-control"></textarea>
            </form>
        </div><!-- /.end box-->
        <a href="<?php echo __BASEPATH__ ?>submission/upload/?encdata=<?php echo $_REQUEST['encdata'] ?>" class="btn btn-default pull-left">Back to step 2</a>
	    <button type="button" class="btn btn-primary" id="review-submission-btn">Review and Submit</button>   
    </div><!-- /.end col-md-9-->    
</section>