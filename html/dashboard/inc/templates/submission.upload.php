<?php
	#ignore template

	$ads = $utils->call('ads');
	$encdata = $_REQUEST['encdata'];
	$encdata = unserialize(base64_decode(urldecode($encdata)));				
	$submission_id = $encdata['submission_id'];	
	$data = $ads->get_submission(array('id' => $submission_id,'detailed' => true));
	$files = $utils->get_attachments(array(
		'attachment_post_type' => 'submission',
		'post_id' => $submission_id,
		'module' => 'ads'
	));
?>
<h1>Submit New Ad</h1>
<section class="row">
    <div class="col-md-3">
        <h2>Steps</h2>   
        <ul>
            <li class="step complete"><i class="fa fa-check"></i> 1 Submission Details</li>
            <li class="step current">2 Upload Material</li>
            <li class="step next">3 Approval Details</li>
            <li class="step next">4 Confirm &amp; Submit</li>
        </ul>  
        <hr />
        <div class="helpbox">
        	<p>Here's where you upload files for your submission.</p>
			<p>Add files, URLs or a combination of both.</p>
            <p>When you've uploaded all, click 'Save'.</p>
			<p>You can add labels and comments for each file at this stage. This is an optional step. You can add general comments for the submission at the final step.</p>
			<p>When you've added all your files, click 'Save' and move on to the next step.</p>
			<p>Click on the back button to go back to the previous step.</p>
        </div>      
    </div>
    <!-- start right-->
    <div class="col-md-9">
        <h2 class="sub_section clearfix">Step 2 - Upload Material</h2>
        <div class="box clearfix" id="add-files-view">
            <form method="post" name="new-submission-upload" id="new-submission-upload" action="#">     
            	<input type="hidden" name="encdata" id="encdata" value="<?php echo $_REQUEST['encdata'] ?>">    
                <div id="queue"></div>
                <div id="uploader">
                    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                </div>
                <hr />
                <label>Import URL</label>
                <div class="clearfix">
                    <div class="input-group">
                        <input type="text" name="url" id="url" class="form-control" placeholder="http://" />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="add-url-btn">Add</button>
                        </span>
                    </div><!-- /input-group -->
                </div>
            </form>
            <?php
			if($files !== false){
			?>
			<a href="#" class="btn btn-default btn-sm pull-left" id="show-files-btn"><i class="fa fa-list"></i> Show current files</a>
			<?php	
			}//end if
			?>
        </div><!-- /.end box-->
        <div class="box hide clearfix" id="edit-files-view">
        	<form method="post" action="#"  id="edit-submission-labels" name="edit-submission-labels">
            	<h4>You have uploaded the following files/urls</h4>
                <hr />
                <div id="uploaded-files-queue" class="clearfix"></div>  
            </form>
            <button type="button" class="btn btn-default" id="save-labels-addnew-btn">Save &amp; add more files</button>   
            <button type="button" class="btn btn-primary" id="save-labels-next-btn">Save &amp; go to next step</button>         
        </div>
        <a href="<?php echo __BASEPATH__ ?>submission/new/?encdata=<?php echo $_REQUEST['encdata'] ?>" class="btn btn-default pull-left">Back to step 1</a>
        
	    <a href="<?php echo __BASEPATH__ ?>submission/details/?encdata=<?php echo $_REQUEST['encdata'] ?>" class="btn btn-primary pull-right<?php
        if($files === false){
			echo ' hidden';
		}//end if
		?>" id="complete-uploads-btn">Next Step</a>
    </div><!-- /.end col-md-9-->       
</section>