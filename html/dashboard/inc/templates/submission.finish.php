<?php
	$encdata = $_REQUEST['encdata'];
?>
<h1>Submit New Ad</h1>
<section class="row">
    <div class="col-md-3">
        <h2>Steps</h2>   
        <ul>
            <li class="step complete"><i class="fa fa-check"></i> 1 Submission Details</li>
            <li class="step complete"><i class="fa fa-check"></i> 2 Upload Material</li>
            <li class="step complete"><i class="fa fa-check"></i> 3 Approval Details</li>
            <li class="step complete"><i class="fa fa-check"></i> 4 Confirm &amp; Submit</li>
        </ul>        
    </div>
    <!-- start right-->
    <div class="col-md-9">
        <h2 class="sub_section clearfix">Thank you for your submission</h2>      
        <p>Thank you for your submission to Copyclear. The CopyClear team will review your submission shortly and you will receive email updates of our decision in due course.</p> 
    </div><!-- /.end col-md-9-->    
</section>