<?php
	#ignore template
	ini_set('display_errors',1);

	require_once("inc/admin.secure.inc.php");

	$ads = $utils->call('ads');
	$campaigns = $utils->call('campaigns');
	$messages = $utils->call('messages');
	$sort_order = $_GET['sort_order'];
	$sort_by = $_GET['sort_by'];

	if(is_null($sort_order)){
		$sort_order = 'ASC';
	}// end if

	if(is_null($sort_by)){
		$sort_by = 'ad_submission_date_replied';
	}// end if

	$sort_date_link = "?sort_order=DESC";
	$sort_company_link = "?sort_by=company_name&sort_order=DESC";
	$sort_icon = 'fa-chevron-down';
	if($sort_order !== 'ASC'){
		$sort_date_link = "?sort_order=ASC";
		$sort_company_link = "?sort_by=company_name&sort_order=ASC";
		$sort_icon = 'fa-chevron-up';
	}// end if
?>
<h1>Admin Dashboard</h1>
<section class="row">
    <div class="col-md-12">
        <?php
		require_once('inc/snippets/keys.wide.inc.php');
		?>
    </div>
	<div class="col-md-12">
        <!-- start accordion -->
        <div class="panel-group" id="accordion">
        	<?php
			$total_ads = 0;
			$list = $ads->get_submissions(array('status' => 'new', 'days' => $admin_days_range,'sort_order' => $sort_order,'sort_by' => $sort_by));
			if($list !== false){
				$total_ads += sizeof($list);
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-new">
                        <span class="status new"></span> <?php echo sizeof($list) ?> New
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-new" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>
																			<a href="<?php echo $sort_date_link ?>">
																				Date
																				<?php
																				if($sort_by === 'ad_submission_date_replied'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>
																			<a href="<?php echo $sort_company_link ?>">
																				Company
																				<?php
																				if($sort_by === 'company_name'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>Ad</th>
                                    <th>Submission</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/admin.panel.list.inc.php');
							?>
                            </tbody>
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
            </div><!-- /.panel -->
			<?php
			}//end if
			$list = $ads->get_submissions(array('status' => 'viewed', 'days' => $admin_days_range,'sort_order' => $sort_order,'sort_by' => $sort_by));
			if($list !== false){
				$total_ads += sizeof($list);
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-viewed">
                        <span class="status viewed"></span> <?php echo sizeof($list) ?> Viewed but not decided
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-viewed" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>
																			<a href="<?php echo $sort_date_link ?>">
																				Date
																				<?php
																				if($sort_by === 'ad_submission_date_replied'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
                                    </th>
                                    <th>
																			<a href="<?php echo $sort_company_link ?>">
																				Company
																				<?php
																				if($sort_by === 'company_name'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
                                    </th>
                                    <th>Ad</th>
                                    <th>Submission</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/admin.panel.list.inc.php');
							?>
                            </tbody>
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
            </div><!-- /.panel -->
			<?php
			}//end if
			// New Messages in conversation
			$list = $messages->get_latest_messages(array('sort_order' => 'desc', 'sort_by' => $sort_by, 'status' => 'conversation')); // Added ib 04/03/17
			if($list !== false){
				$total_ads += sizeof($list);
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-conversation-2017">
                        <span class="status conversation"></span> <?php echo sizeof($list) ?> Conversation <?php
												// $new_comments = $ads->count_new_messages();
												// if($new_comments > 0){
												// 	echo '<small>('.$new_comments.' new comments)</small>';
												// }//end if
												?>
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-conversation-2017" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        	<thead>
                                <tr>
                                    <!-- <th></th> -->
                                    <th>
																			<a href="<?php echo $sort_date_link ?>">
																				Date
																				<?php
																				if($sort_by === 'ad_submission_date_replied'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>
																			<a href="<?php echo $sort_company_link ?>">
																				User / Company
																				<?php
																				if($sort_by === 'company_name'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>

                                    <th>Message</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/admin.panel.messages.list.inc.php');
							?>
                            </tbody>
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
           </div><!-- /.panel -->
           <?php
		   }//end if

			 // New messages in user comments
 			$list = $messages->get_latest_messages(array('sort_order' => 'desc', 'sort_by' => $sort_by, 'ignore_status' => 'conversation', 'days' => 60)); // Added ib 04/03/17

 			if($list !== false){
 				$total_ads += sizeof($list);
 			?>
             <div class="panel panel-default">
                 <div class="panel-heading">
                     <h4 class="panel-title">
                         <a data-toggle="collapse" data-parent="#accordion" href="#panel-comments-2017">
                         <span class="status conversation"></span> <?php echo sizeof($list) ?> User Comments <?php
 												// $new_comments = $ads->count_new_messages();
 												// if($new_comments > 0){
 												// 	echo '<small>('.$new_comments.' new comments)</small>';
 												// }//end if
 												?>
                         <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                         </a>
                     </h4>
                 </div>
                 <div id="panel-comments-2017" class="panel-collapse collapse">
                     <div class="panel-body">
                         <table class="table">
                         	<thead>
                                 <tr>
                                     <!-- <th></th> -->
                                     <th>
 																			<a href="<?php echo $sort_date_link ?>">
 																				Date
 																				<?php
 																				if($sort_by === 'ad_submission_date_replied'){
 																				?>
 																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
 																				<?php
 																				}// end if
 																				?>
 																			</a>
 																		</th>
                                     <th>
 																			<a href="<?php echo $sort_company_link ?>">
 																				User / Company
 																				<?php
 																				if($sort_by === 'company_name'){
 																				?>
 																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
 																				<?php
 																				}// end if
 																				?>
 																			</a>
 																		</th>

                                     <th>Message</th>
                                     <th class="last"></th>
                                 </tr>
                             </thead>
                             <tbody>
                             <?php
 							include('inc/snippets/admin.panel.messages.list.inc.php');
 							?>
                             </tbody>
                         </table>
                 	</div><!-- /.panel-body -->
                 </div><!-- /.panel-collapse -->
            </div><!-- /.panel -->
            <?php
 		   }//end if
			//$list = $ads->get_submissions_in_conversation(array('days' => $admin_days_range,'sort_order' => $sort_order, 'sort_by' => $sort_by, 'status' => 'conversation')); // As per pre 19/02/17
			/*$list = $ads->get_submissions_in_conversation(array('sort_order' => $sort_order, 'sort_by' => $sort_by, 'status' => 'conversation')); // Added ib 04/03/17
			if($list !== false){
				$total_ads += sizeof($list);
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-conversation">
                        <span class="status conversation"></span> <?php echo sizeof($list) ?> Conversation <?php
						$new_comments = $ads->count_new_messages();
						if($new_comments > 0){
							echo '<small>('.$new_comments.' new comments)</small>';
						}//end if
						?>
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-conversation" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>
																			<a href="<?php echo $sort_date_link ?>">
																				Date
																				<?php
																				if($sort_by === 'ad_submission_date_replied'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>
																			<a href="<?php echo $sort_company_link ?>">
																				Company
																				<?php
																				if($sort_by === 'company_name'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>Ad</th>
                                    <th>Submission</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/admin.panel.list.inc.php');
							?>
                            </tbody>
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
           </div><!-- /.panel -->
           <?php
		   }//end if
			 */
			 /*$list = $ads->get_submissions_by_user_comments(array('days' => 30,'sort_order' => $sort_order, 'sort_by' => $sort_by, 'ignore_status' => array('conversation')));
 			//$list = $ads->get_submissions_in_conversation(array('days' => $admin_days_range,'sort_order' => $sort_order, 'sort_by' => $sort_by, 'status' => 'conversation')); // As per pre 19/02/17
 			if($list !== false){
 				$total_ads += sizeof($list);
 			?>
             <div class="panel panel-default">
                 <div class="panel-heading">
                     <h4 class="panel-title">
                         <a data-toggle="collapse" data-parent="#accordion" href="#panel-comments">
                         <span class="status conversation"></span> <?php echo sizeof($list) ?> User Comments <?php
 						$new_comments = $ads->count_new_messages();
 						if($new_comments > 0){
 							echo '<small>('.$new_comments.' new comments)</small>';
 						}//end if
 						?>
                         <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                         </a>
                     </h4>
                 </div>
                 <div id="panel-comments" class="panel-collapse collapse">
                     <div class="panel-body">
                         <table class="table">
                         	<thead>
                                 <tr>
                                     <th></th>
                                     <th>
 																			<a href="<?php echo $sort_date_link ?>">
 																				Date
 																				<?php
 																				if($sort_by === 'ad_submission_date_replied'){
 																				?>
 																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
 																				<?php
 																				}// end if
 																				?>
 																			</a>
 																		</th>
                                     <th>
 																			<a href="<?php echo $sort_company_link ?>">
 																				Company
 																				<?php
 																				if($sort_by === 'company_name'){
 																				?>
 																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
 																				<?php
 																				}// end if
 																				?>
 																			</a>
 																		</th>
                                     <th>Ad</th>
                                     <th>Submission</th>
                                     <th class="last"></th>
                                 </tr>
                             </thead>
                             <tbody>
                             <?php
 							include('inc/snippets/admin.panel.list.inc.php');
 							?>
                             </tbody>
                         </table>
                 	</div><!-- /.panel-body -->
                 </div><!-- /.panel-collapse -->
            </div><!-- /.panel -->
            <?php
 		   }//end if
			 */
			$list = $ads->get_submissions(array('ignore_status' => array('conversation','new'),'viewed' => false, 'current_week' => true,'sort_order' => $sort_order,'sort_by' => $sort_by));
		//	$list = $ads->get_submissions(array('ignore_status' => array('conversation','new'),'viewed' => false, 'sort_order' => $sort_order));
			if($list !== false){
				$total_ads += sizeof($list);
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-decided">
                        <?php echo sizeof($list) ?> Decided - To be viewed by user
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-decided" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>
																			<a href="<?php echo $sort_date_link ?>">
																				Date
																				<?php
																				if($sort_by === 'ad_submission_date_replied'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
                                    </th>
                                    <th>
																			<a href="<?php echo $sort_company_link ?>">
																				Company
																				<?php
																				if($sort_by === 'company_name'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
                                    </th>
                                    <th>Ad</th>
                                    <th>Submission</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/admin.panel.list.inc.php');
							?>
                            </tbody>
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
           </div><!-- /.panel -->
           <?php
			}//end if
			$list = $ads->get_submissions(array('ignore_status' => array('conversation','new'),'viewed' => true, 'current_week' => true,'sort_order' => $sort_order,'sort_by' => $sort_by));
		//	$list = $ads->get_submissions(array('ignore_status' => array('conversation','new'),'viewed' => true, 'sort_order' => $sort_order));
			if($list !== false){
				$total_ads += sizeof($list);
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-view">
                        <?php echo sizeof($list) ?> Decided &amp; Viewed
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-view" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>
																			<a href="<?php echo $sort_date_link ?>">
																				Date
																				<?php
																				if($sort_by === 'ad_submission_date_replied'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
                                    </th>
                                    <th>
																			<a href="<?php echo $sort_company_link ?>">
																				Company
																				<?php
																				if($sort_by === 'company_name'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>Ad</th>
                                    <th>Submission</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/admin.panel.list.inc.php');
							?>
                            </tbody>
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
           </div><!-- /.panel -->
           <?php
			}//end if
			$list = $ads->get_submissions(array('status' => 'Pending', 'days' => 120,'sort_order' => $sort_order,'sort_by' => $sort_by));
			if($list !== false){
				$total_ads += sizeof($list);
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-pending">
                        <span class="status pending"></span> <?php echo sizeof($list) ?> Pending
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-pending" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>
																			<a href="<?php echo $sort_date_link ?>">
																				Date
																				<?php
																				if($sort_by === 'ad_submission_date_replied'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>
																			<a href="<?php echo $sort_company_link ?>">
																				Company
																				<?php
																				if($sort_by === 'company_name'){
																				?>
																				<span class="pull-right"><i class="fa <?php echo $sort_icon ?>"></i></span>
																				<?php
																				}// end if
																				?>
																			</a>
																		</th>
                                    <th>Ad</th>
                                    <th>Submission</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/admin.panel.list.inc.php');
							?>
                            </tbody>
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
            </div><!-- /.panel -->
			<?php
			}//end if
		   ?>
        </div><!-- end accordion -->
        <!--p>Total number of submissions for the past <?php echo $admin_days_range ?> days: <span class="label label-info"><?php echo $total_ads ?></span></p-->
        <ul>
            <li><a class="btn btn-primary" href="campaigns_list.php">View All Campaigns</a></li>
        </ul>
	</div>
</section>
