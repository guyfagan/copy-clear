<?php
	#ignore template

	$ads = $utils->call('ads');
	$brands = $utils->call('brands');
	$search_fields = array('from_month','to_month','from_year','to_year','chart_type');
	$do_search = false;
	foreach($search_fields as $value){
		if(isset($_REQUEST[$value])){
			$do_search = true;
		}//end if
	}//end foreach
  $template = 'chart';
	if($_REQUEST['chart_type'] == 'brand_owner'){
  	$template = 'brands';
	}
?>
<h1>Reports</h1>
<section class="row" id="report-charts">
	<form method="post" id="form-search" name="form-search" action="<?php echo __BASEPATH__ ?>admin/reports/charts/">
        <div class="col-md-4">
        	<h2>Refine results</h2>
        	<div class="box clearfix">
                <h4>Month range</h4>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>From</label>
                        <select name="from_month" id="from-month" class="form-control">
                        <?php
                            $utils->call('tools');
                            $months = tools::get_months();
                            for($i = 0; $i < 12; $i++){
                                $x = $i+1;
                                echo '<option value="'.$x.'"';
                                if(isset($_REQUEST['from_month'])){
                                    if($_REQUEST['from_month'] == $x){
                                        echo ' selected';
                                    }//end if
                                }//end if
                                echo '>'.$months[$i].'</option>';
                            }//end if
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>To</label>
                        <select name="to_month" id="to-month" class="form-control">
                        <?php
                            $utils->call('tools');
                            $months = tools::get_months();
                            for($i = 0; $i < 12; $i++){
                                $x = $i+1;
                                echo '<option value="'.$x.'"';
                                if(isset($_REQUEST['to_month'])){
                                    if($_REQUEST['to_month'] == $x){
                                        echo ' selected';
                                    }//end if
                                }//end if
                                echo '>'.$months[$i].'</option>';
                            }//end if
                        ?>
                        </select>
                    </div>
                </div>
                <h4>Year range</h4>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>From</label>
                        <select name="from_year" id="from-year" class="form-control">
                        <?php
                            for($i = 2009; $i <= date("Y"); $i++){
                                echo '<option value="'.$i.'"';
                                if(isset($_REQUEST['from_year'])){
                                    if($_REQUEST['from_year'] == $i){
                                        echo ' selected';
                                    }//end if
                                }//end if
                                echo '>'.$i.'</option>';
                            }//end if
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>To</label>
                        <select name="to_year" id="to-year" class="form-control">
                        <?php
                            for($i = 2009; $i <= date("Y"); $i++){
                                echo '<option value="'.$i.'"';
                                if(isset($_REQUEST['to_year'])){
                                    if($_REQUEST['to_year'] == $i){
                                        echo ' selected';
                                    }//end if
                                }//end if
                                echo '>'.$i.'</option>';
                            }//end if
                        ?>
                        </select>
                    </div><!-- /.form-group -->
                </div><!-- /.row -->
                <div class="form-group">
                	<label>Chart Type</label>
                	<select id="chart-type" name="chart_type" class="form-control">
                  	<option value="categories"<?php if($_REQUEST['chart_type'] == "categories"){ echo ' selected'; } ?>>Categories</option>
                    <option value="medium"<?php if($_REQUEST['chart_type'] == "medium"){ echo ' selected'; } ?>>Medium</option>
                    <option value="company_type"<?php if($_REQUEST['chart_type'] == "company_type"){ echo ' selected'; } ?>>Agency Type</option>
                    <option value="status"<?php if($_REQUEST['chart_type'] == "status"){ echo ' selected'; } ?>>Status</option>
                    <option value="origin"<?php if($_REQUEST['chart_type'] == "origin"){ echo ' selected'; } ?>>Origin</option>
                    <option value="brand_owner"<?php if($_REQUEST['chart_type'] == "brand_owner"){ echo ' selected'; } ?>>Brands Owners</option>
                  </select>
                </div>
                <a href="<?php echo __BASEPATH__ ?>admin/reports/" class="btn btn-default btn-top-margin pull-left">Back to basic reports</a>
                <button type="submit" class="btn btn-primary">Show Chart</button>
        	</div><!-- /.box -->
        </div><!-- /.col-md-4 -->
        <div class="col-md-8">
        <?php
          switch($template){
            case "chart":
              require_once 'inc/snippets/chart.results.inc.php';
              break;
            case "brands":
              require_once 'inc/snippets/brands.results.inc.php';
              break;
          }
        ?>
        </div>
    </form>
</section>
<script src="<?php echo __BASEPATH__ ?>js/jquery.peity.min.js"></script>
<script>
	$(document).ready(function(e) {

		$(".line").peity("line");
		$("span.pie").peity("pie");

        $("#from-month").on("change",function(){
			var $from = $("#from-month").val();
			if($from > $("#to-month").val()){
				$("#to-month").val($from);
			}
		});

		$("#from-year").on("change",function(){
			var $from = $("#from-year").val();
			if($from > $("#to-year").val()){
				$("#to-year").val($from);
			}
		});
    });
</script>
