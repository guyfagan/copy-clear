<?php
	#ignore template

	require_once("inc/admin.secure.inc.php");

	$ads = $utils->call('ads');
	$messages = $utils->call('messages');
	$encdata = $_REQUEST['encdata'];
	$encdata = base64_decode(urldecode($encdata));
	$submission_id = $encdata;
	$ads->mark_submission_as_viewed(array('id' => $submission_id,'admin' => true));
	$ads->log_activity(array('ref_id' => $submission_id,'type' => 'actions','value' => 'view'));
	$data = $ads->get_submission(array('id' => $submission_id,'detailed' => true));
	$title = trim($data['ad_submission_title']);
	if($title == ""){
		$title = $data['attachment_friendly_name'];
	}//end if
	$last_edit = date("jS M g:iA",$data['create_date']);
	if($data['reply_date'] > 0){
		$last_edit = date("jS M g:iA",$data['reply_date']);
	}//end if

	$user_data = $users->get_user(array('id' => $data['ad_submission_uid'],'meta' => true));
	$company_data = $companies->get_company(array('id' => $user_data['user_company_id']));

	$viewed_by = $users->get_user(array('id' => $data['ad_submission_viewed_by_admin']));

	$codes = $ads->get_codes();

?>
<ol class="breadcrumb">
  <li><?php echo $data['brand_name'] ?></li>
  <li><?php echo $data['campaign_name'] ?></li>
  <li class="active"><?php echo $data['ad_title'] ?></li>
</ol>
<h2><?php echo $title ?></h2>
<?php
if(trim($data['ad_submission_file_details']) != ""){
?>
<p><?php echo $data['ad_submission_file_details'] ?></p>
<?php
}//end if
?>
<section class="row">
    <div class="col-md-6 col-lg-6">
         <div class="box databox">
            <h3><div class="circle"><i class="fa fa-bars"></i></div> Submission Details <a href="#" class="pull-right expander" data-toggle="collapse" data-target="#details-container"><i class="fa fa-chevron-down"></i></a></h3>
            <div id="details-container" class="collapse in">
                <table>
                    <tr>
                        <td class="labels">Agency</td>
                        <td class="data"><?php echo $company_data['company_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Submitted by</td>
                        <td class="data"><?php
                        echo $user_data['user_firstname'].' '.$user_data['user_surname'];
                        ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Contact details</td>
                        <td class="data"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $user_data['user_email']; ?>"><?php echo $user_data['user_email']; ?></a><br /><?php
                        if($user_data['meta_mobile'] != ""){
                            echo ' <i class="fa fa-mobile"></i> <a href="tel:'.$user_data['meta_mobile'].'">'.$user_data['meta_mobile'].'</a>';
                        }

                        if($user_data['meta_phone'] != ""){
                            echo ' | <i class="fa fa-phone"></i> <a href="tel:'.$user_data['meta_phone'].'">'.$user_data['meta_phone'].'</a>';
                        }
                        ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Advertiser</td>
                        <td class="data"><?php echo $data['brand_owner_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Brand</td>
                        <td class="data"><?php echo $data['brand_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Campaign</td>
                        <td class="data"><?php echo $data['campaign_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Medium</td>
                        <td class="data"><?php echo $data['ad_medium_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Ad Title</td>
                        <td class="data"><?php echo $data['ad_title'] ?></td>
                    </tr>
                    <?php
                    if($data['ad_submission_support'] != 1){
                    ?>
                    <tr>
                        <td class="labels">Approval Sought</td>
                        <td class="data"><?php echo ucwords($data['ad_submission_approval_sought']) ?> Approval</td>
                    </tr>
                    <?php
                    }//end if
                    ?>
                    <tr>
                        <td class="labels">Substantiation</td>
                        <td class="data"><?php
                        if($data['ad_submission_support'] == 1){
                            echo 'Yes';
                        } else {
                            echo 'No';
                        }//end if
                        ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Origin of Work</td>
                        <td class="data"><?php
                            switch($data['ad_submission_origin']){
                                default:
                                case 'ireland':
                                    echo 'Created in Ireland';
                                break;
                                case 'adapted':
                                    echo 'Adaptation of international material';
                                break;
                                case 'elsewhere':
                                    echo 'Created elsewhere';
                                break;
                            }
                        ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Comments</td>
                        <td class="data"><?php echo $data['ad_submission_comments'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">File</td>
                        <td class="data long-words">
                        <?php
                            if($data['attachment_type'] == 'image'){
                        ?>
                            <a href="<?php echo __S3_PUBLIC_PATH__.$data['attachment_filename'] ?>" target="_blank" rel="prettyPhoto">
                                <img src="<?php echo __UPLOADPATH__ ?>photos/t/<?php echo $data['attachment_filename'] ?>" alt="<?php echo $data['attachment_friendly_name'] ?>">
                            </a>
                        <?php
                            } else {
                        ?>
                            <a href="<?php echo $data['attachment_url'] ?>" target="_blank"><?php echo $data['attachment_filename'] ?></a>
                        <?php
                            }//end if
                        ?>
                        </td>
                    </tr>
                </table>
            </div><!-- /.collapse -->
        </div><!-- /.end box-->
    </div>
    <!-- start right-->
    <div class="col-md-6 col-lg-6">
    	<form action="#" method="post" id="edit-submission" name="edit-submission">
            <input type="hidden" name="encdata" id="encdata" value="<?php echo $_GET['encdata'] ?>">
            <div class="box clearfix" id="edit-box">
            	<h3><div class="circle"><i class="fa fa-wrench"></i></div> Edit Submission <a href="#" class="pull-right expander" data-toggle="collapse" data-target="#edit"><i class="fa fa-chevron-down"></i></a></h3>
                <div id="edit" class="collapse in">
                	<p><strong>Last Edit:</strong> <?php echo $last_edit ?></p>
                  <p><strong>Viewed by:</strong> <?php echo $viewed_by['user_firstname'].' '.$viewed_by['user_surname'] ?></p>
                	<div class="form-group">
                        <label>Submission Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="final-approval"<?php if($data['ad_submission_status'] == "final-approval"){ echo ' selected'; } ?>>Final Approval</option>
                            <option value="interim-approval"<?php if($data['ad_submission_status'] == "interim-approval"){ echo ' selected'; } ?>>Interim Approval</option>
                            <option value="new"<?php if($data['ad_submission_status'] == "new"){ echo ' selected'; } ?>>Submitted/Undecided</option>
                            <option value="viewed"<?php if($data['ad_submission_status'] == "viewed"){ echo ' selected'; } ?>>Viewed</option>
                            <option value="not-approved"<?php if($data['ad_submission_status'] == "not-approved"){ echo ' selected'; } ?>>Not Approved</option>
														<option value="incomplete-submission"<?php if($data['ad_submission_status'] == "incomplete-submission"){ echo ' selected'; } ?>>Incomplete Submission</option>
                            <option value="not-valid"<?php if($data['ad_submission_status'] == "not-valid"){ echo ' selected'; } ?>>Not Valid</option>
                            <option value="pending"<?php if($data['ad_submission_status'] == "pending"){ echo ' selected'; } ?>>Pending</option>
                            <option value="sm-received"<?php if($data['ad_submission_status'] == "sm-received"){ echo ' selected'; } ?>>Substantiation Received</option>
                            <option value="conversation"<?php if($data['ad_submission_status'] == "conversation"){ echo ' selected'; } ?>>Conversation</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Codes</label>
                       	<div class="input-group">
	                        <input type="text" name="codes" id="codes" class="form-control">
                            <a href="#" class="input-group-addon" data-toggle="modal" data-target="#codes-library"><i class="fa fa-book"></i></a>
                        </div>
                    </div>
                    <div class="form-group">
                    	<label>CCCI Feedback</label>
                        <textarea name="comment" id="comment" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label data-toggle="collapse" data-target="#additional-notes"><i class="fa fa-plus-circle"></i> Additional Notes <small>(click to expand)</small></label>
                        <div class="collapse" id="additional-notes">
                            <textarea name="notes" id="notes" class="form-control" rows="3"><?php
                            if($data['ad_submission_notes'] != ""){
                                echo $data['ad_submission_notes'];
                            }//end if
                            ?></textarea>
                            <span class="help-block">This are internal notes and they are not visible to the users.</span>
                        </div>
                    </div>
                    <button class="btn btn-primary pull-right" type="button" id="update-submission">Save</button>
                </div>
            </div>
        </form>

    </div><!-- /.end col-md-9-->
</section>
<section class="row">
	<div class="col-md-12">
        <div class="box clearfix" id="conversation-container">
            <h3><span class="status conversation"></span> Conversation</h3>
            <div id="messages" class="clearfix">
        <?php
            $encdata = array('submission_id' => $submission_id);
            $encdata = urlencode(base64_encode(serialize($encdata)));
            $options = array('ref_id' => $submission_id,'module_id' => 'ads');
            $msg_list = $messages->get_messages($options);
            if($msg_list !== false){
                for($i = 0; $i < sizeof($msg_list); $i++){
        ?>
                <div class="message-bubble clearfix">
                    <h4><span><i class="fa fa-user"></i> From <?php echo $msg_list[$i]['sender_firstname'].' '.$msg_list[$i]['sender_surname'] ?></span> - <?php echo date('jS M Y, g:iA',$msg_list[$i]['message_date']) ?></h4>
                    <div class="message-content"><?php echo $msg_list[$i]['message_message'] ?></div>
                </div>
        <?php
                }//end for i
            }//end if
        ?>
            </div>
            <!--form action="#" method="post" id="comment-form" name="comment-form">
                <h4>Add your feedback</h4>
                <input type="hidden" name="encdata" id="encdata" value="<?php echo $encdata ?>">
                <textarea name="comment" id="comment" class="form-control" rows="4"></textarea>
                <button id="submit-comment" class="btn btn-primary">Submit <i class="fa fa-chevron-right"></i></button>
            </form-->
        </div><!-- /.end box-->
	</div>
</section>
<div class="modal fade" id="codes-library">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Codes</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                	<thead>
                    	<tr>
                        	<th width="10%">Code</th>
                            <th>Text</th>
                        </tr>
                    </thead>
                    <tbody>
                   	<?php
					if($codes !== false){
						for($i = 0; $i < sizeof($codes); $i++){
					?>
                    	<tr>
                        	<td><?php echo $codes[$i]['code_code'] ?></td>
                            <td><?php echo $codes[$i]['code_text'] ?></td>
                        </tr>
                    <?php
						}//for i
					}//end if
					?>
                    </tbody>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="status-change">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Status change</h4>
            </div>
            <div class="modal-body">
            	The type of approval you have selected does not match the type of approval sought.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Dismiss</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){

		$.copyclear.approvalSought = '<?php echo $data['ad_submission_approval_sought'] ?>';

		$('#codes').tokenfield({
		  autocomplete: {
			source: <?php

			if($codes !== false){
				$list_codes = array();
				for($i = 0; $i < sizeof($codes); $i++){
					array_push($list_codes,array('value' => $codes[$i]['code_id'], 'label' => $codes[$i]['code_code']));
				}//end for i
				echo json_encode($list_codes);
			}
			?>,
			delay: 100
		  },
		  <?php
			if($data['ad_submission_codes'] != ""){
				$saved_codes = explode(",",$data['ad_submission_codes']);
				$show_codes = array();
				for($i = 0; $i < sizeof($codes); $i++){
					if(in_array((int)$codes[$i]['code_id'],$saved_codes)){
						array_push($show_codes,array('value' => $codes[$i]['code_id'],'label'=> $codes[$i]['code_code']));
					}//end if
				}//end if
				echo 'tokens:';
				echo json_encode($show_codes);
				echo ',';
			}//end if
			?>
		  showAutocompleteOnFocus: true,
		  allowDuplicates: false
		});

		$("#status").change(function(){
			if(($(this).val() == 'final-approval' && $.copyclear.approvalSought == 'interim') || ($(this).val() == 'interim-approval' && $.copyclear.approvalSought == 'final')){
				$("#status-change").modal('show');
			}
		});

		$("#update-submission").click(function(e){
			e.preventDefault();
			var $form = $("#edit-submission");
			$('.alert').remove();
			$('#codes').val($('#codes').tokenfield('getTokensList'));
			//console.log($('#codes').tokenfield('getTokensList'));
			$.ajax({
				url: $.copyclear.apipath + 'admin.update.submission',
				data: $form.serialize(),
				dataType:"json",
				type:'post',
				success: function(data){
					console.log(data);
					if(data.result == 'success'){
						$("#comment").val('');
						$('#edit-box').prepend('<div class="alert alert-success"><strong>Success!</strong> This submission has been saved successfully</div>');
						//$('html,body').scrollTop(0);
						if(typeof(data.status.message_id) != 'undefined'){
							$("#conversation-container").addComment(data.status);
						}//end if
						location.href = $.copyclear.basepath + 'home/';
					}
				}
			});
		});

	});
</script>
