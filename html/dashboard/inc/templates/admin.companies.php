<?php
	#ignore template

	$companies = $utils->call('companies');
	$options = array();

	if(!isset($show)){
		$show = 'pending';
	}//end if

  if($show != "all"){
  	$options['status'] = $show;
  }// end if

  if(isset($_GET['search'])){
    $show = 'all';
    unset($options['status']);
    $search = $_GET['search'];
    $options['search'] = $search;
  } // end if

	$utils->create_paging($_GET['page'],20);
	$list = $companies->get_companies($options);
	# PAGINATION #
	$paging_stats = $utils->get_paging_stats();

	$paging_list = $utils->get_paging(array('range' => 10,'cms' => false));
	//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class
	$utils->unset_paging();
	$page_start = $paging_stats['start']+1;
	$page_end = $paging_stats['start']+10;
	if($page_end > $paging_stats['founds']){
		$page_end = $paging_stats['founds'];
	}//end if
	$page_total = $paging_stats['pages'];
	$page_current = $paging_stats['current_page'];
	$page_prev = $page_current-1;
	if($page_prev == "0"){
		$page_prev = 1;
	}//end if
	$page_next = $page_current+1;
	if($page_next > $page_total){
		$page_next = $page_total;
	}//end if
?>
<h1>Companies</h1>
<section class="row">
    <div class="col-md-3">
    	<ul>
            <li class="step<?php if($show == 'pending'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/companies/pending/">Pending Companies</a></li>
            <li class="step<?php if($show == 'active'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/companies/active/">Active Companies</a></li>
            <li class="step<?php if($show == 'inactive'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/companies/inactive/">Inactive Companies</a></li>
            <li class="step<?php if($show == 'all'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/companies/all/">All</a></li>
        </ul>
    </div>
	<div class="col-md-9">
        <!-- start accordion -->
        <div class="panel-group" id="accordion">
        	<?php
       			echo '<h2>'.ucwords($show).' Companies</h2>';
			?>
			      <div class="row">
  			      <div class="col-md-6 col-md-offset-6">
    			      <form class="form-inline" id="search-form" action="<?php echo __BASEPATH__.'admin/companies/'.$show ?>/" method="get">
      			      <div class="input-group">
                    <input type="text" name="search" id="search" class="form-control" placeholder="Search for..." value="<?php
                      if(isset($search)){
                        echo $search;
                      }
                      ?>">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                      <a href="<?php echo __BASEPATH__.'admin/companies/'.$show ?>/" class="btn btn-default">Reset</a>
                    </span>
                  </div><!-- /input-group -->
                </form>
  			      </div>
			      </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Companies
                    </h4>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Status</th>
                                <th class="last"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						if($list !== false){
							for($i = 0; $i < sizeof($list); $i++){
								$encdata = urlencode(base64_encode($list[$i]['company_id']));
								$link = __BASEPATH__.'admin/company/?encdata='.$encdata;
								$status = 'active';
								$btn_class = 'btn-success';
								if($list[$i]['company_status'] == 'pending'){
									$status = 'pending';
									$btn_class = 'btn-warning';
								}//end if
								if($list[$i]['company_status'] == 'inactive'){
									$status = 'inactive';
									$btn_class = 'btn-danger';
								}//end if
							?>
								<tr>
									<td><?php echo $list[$i]['company_name'] ?></td>
									<td><a class="btn <?php echo $btn_class ?> btn-xs editable editable-click" data-value="<?php echo $list[$i]['company_status'] ?>" data-pk="<?php echo $list[$i]['company_id'] ?>" href="#"><?php echo $status ?></a></td>
									<td class="last"><a href="<?php echo $link ?>" class="btn btn-primary btn-xs">View</a></td>
								</tr>
							<?php
							}//end for i
						} else {
						?>
                        	<tr>
                            	<td colspan="5"><div class="alert alert-warning">No companies found</div></td>
                            </tr>
                        <?php
						}//end if
						?>
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->

           </div><!-- /.panel -->
           <?php

		   if($page_total > 1){
			?>
                <ul class="pagination clearfix">
                    <li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
                    <?php
                    if(is_array($paging_list) && sizeof($paging_list) > 0){
                        for($i = 0; $i < sizeof($paging_list); $i++){
                            echo '<li';
                            if($paging_list[$i]['page'] == $page_current){
                                echo ' class="active"';
                            }//end if
                            $url_params = http_build_query($_REQUEST);

	                        echo '><a href="'.__BASEPATH__.'admin/companies/'.$show.'/?page='.$paging_list[$i]['page'].'">'.$paging_list[$i]['page'].'</a></li>';

                        }//end for i
                    }//end if
                    ?>
                    <li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
                </ul>
			<?php
			}//end if
		   ?>
        </div><!-- end accordion -->
	</div>
</section>
<script>
	$(document).ready(function() {
	<?php
	if($list !== false){
	?>
    	$('.editable').editable({
			type: 'select',
			showbuttons: false,
			url: $.copyclear.apipath+'update.company.status',
			source: [
				{value: 'active', text: 'Active'},
				{value: 'pending', text: 'Pending'},
				{value: 'inactive', text: 'Inactive'}
			]
		});
	<?php
	}//end if
	?>
    });
</script>