<h1>Reports</h1>
<section class="clearfix" id="report-charts">
<?php
	$ads = $utils->call('ads');
	$brands = $utils->call('brands');
	$companies = $utils->call('companies');

	$view = $_REQUEST['view'];

	$category_id = $_REQUEST['category_id'];
	$medium_id = $_REQUEST['medium_id'];
	$company_type_id = $_REQUEST['company_type_id'];
	$from_month = $_REQUEST['from_month'];
	$to_month = $_REQUEST['to_month'];
	$from_year = $_REQUEST['from_year'];
	$to_year = $_REQUEST['to_year'];
	$years = $to_year - $from_year;
	$years++;

	switch($view){
		case "category":
			require_once('inc/snippets/report.details.categories.inc.php');
		break;
		case "medium":
			require_once('inc/snippets/report.details.medium.inc.php');
		break;
		case "company_type":
			require_once('inc/snippets/report.details.agencytype.inc.php');
		break;
	}//end switch
?>
</section>
