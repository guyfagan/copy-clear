<?php
	#template name: Whatever

	$ads = $utils->call('ads');
	$brands = $utils->call('brands');
?>
<h1>Archive Search</h1>
<section class="row" id="archive">
	<form method="post" id="form-search" name="form-search" action="<?php echo __BASEPATH__ ?>archive/">
        <div class="col-md-4">
        <?php
		require_once('inc/snippets/archive.search.panel.inc.php');
		?>
        </div>
        <div class="col-md-8">
            <div class="sub_section clearfix">
                <h2>Search the Archive</h2>
                <div class="box clearfix">
                    <p>Use the filters on the left to refine your search.</p>

                    <input id="search" class="form-control" type="text" name="search" <?php
					if(isset($_REQUEST['search'])){
						echo ' value="'.$_REQUEST['search'].'"';
					}//end if
					?> placeholder="Enter your search term">
                    <div class="clearfix" id="seach-filters">
                        <h4>Search Filters</h4>
                        <div id="brand-category" class="filter-group clearfix">
                            <strong>Category</strong>
                            <ul></ul>
                        </div>
                        <div id="brand" class="filter-group clearfix">
                            <strong>Brand</strong>
                            <ul></ul>
                        </div>
                        <div id="company" class="filter-group clearfix">
                            <strong>Company</strong>
                            <ul></ul>
                        </div>
                        <div id="approval" class="filter-group clearfix">
                            <strong>Approval</strong>
                            <ul></ul>
                        </div>
                        <div id="status" class="filter-group clearfix">
                            <strong>Status</strong>
                            <ul></ul>
                        </div>
                        <div id="medium" class="filter-group clearfix">
                            <strong>Medium</strong>
                            <ul></ul>
                        </div>
                        <div id="origin" class="filter-group clearfix">
                            <strong>Origin</strong>
                            <ul></ul>
                        </div>
                        <div id="company-type" class="filter-group clearfix">
                            <strong>Company Type</strong>
                            <ul></ul>
                        </div>
                        <?php
						if(isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != ""){
							echo '<p><i class="fa fa-calendar"></i> From '.$_REQUEST['from_date'].'</p>';
						}//end if
						if(isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != ""){
							echo '<p><i class="fa fa-calendar"></i> To '.$_REQUEST['to_date'].'</p>';
						}//end if
						?>
                    </div>

                    <button type="button" class="btn btn-primary" id="search-btn">Search</button>
                </div><!-- /.box-->
            </div><!-- /.sub_section -->
            <div class="sub_section clearfix">
            <?php
			$options = array();
			$show_results = false;
			if(!$is_admin){
				$options['company_id'] = $company_id;
			}
			if(isset($_REQUEST['search']) && $_REQUEST['search'] != ""){
				$show_results = true;
				$options['search'] = $_REQUEST['search'];
			}//end if
			if(isset($_REQUEST['brand_category_id']) && is_array($_REQUEST['brand_category_id'])){
				$show_results = true;
				$options['brand_category_id'] = $_REQUEST['brand_category_id'];
			}//end if
			if(isset($_REQUEST['company_id']) && is_array($_REQUEST['company_id'])){
				$show_results = true;
				$options['company_id'] = $_REQUEST['company_id'];
			}//end if
			if(isset($_REQUEST['company_types']) && is_array($_REQUEST['company_types'])){
				$show_results = true;
				$options['company_type_id'] = $_REQUEST['company_types'];
			}//end if
			if(isset($_REQUEST['brand_id']) && is_array($_REQUEST['brand_id'])){
				$show_results = true;
				$options['brand_id'] = $_REQUEST['brand_id'];
			}//end if
			if(isset($_REQUEST['approval']) && is_array($_REQUEST['approval'])){
				$show_results = true;
				$options['approval'] = $_REQUEST['approval'];
			}//end if
			if(isset($_REQUEST['status']) && is_array($_REQUEST['status'])){
				$show_results = true;
				$options['status'] = $_REQUEST['status'];
			}//end if
			if(isset($_REQUEST['year_only']) && $_REQUEST['year_only'] != ""){
				$options['year_only'] = $_REQUEST['year_only'];
			}//end if
			if(isset($_REQUEST['from_date']) && $_REQUEST['from_date'] != ""){
				$show_results = true;
				$options['from_date'] = $_REQUEST['from_date'];
			}//end if
			if(isset($_REQUEST['to_date']) && $_REQUEST['to_date'] != ""){
				$show_results = true;
				$options['to_date'] = $_REQUEST['to_date'];
			}//end if
			if(isset($_REQUEST['origin']) && is_array($_REQUEST['origin'])){
				$show_results = true;
				$options['origin'] = $_REQUEST['origin'];
			}//end if
			if(isset($_REQUEST['medium_id']) && is_array($_REQUEST['medium_id'])){
				$show_results = true;
				$options['medium_id'] = $_REQUEST['medium_id'];
			}//end if
			$utils->create_paging($_GET['page'],25);
			$results = $ads->search_submission($options);
			//echo $utils->cache;
			# PAGINATION #
			$paging_stats = $utils->get_paging_stats();
			$paging_list = $utils->get_paging(array('range' => 10,'cms' => false));
			//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class
			$utils->unset_paging();
			$page_start = $paging_stats['start']+1;
			$page_end = $paging_stats['start']+10;
			if($page_end > $paging_stats['founds']){
				$page_end = $paging_stats['founds'];
			}//end if
			$page_total = $paging_stats['pages'];
			$page_current = $paging_stats['current_page'];
			$page_prev = $page_current-1;
			if($page_prev == "0"){
				$page_prev = 1;
			}//end if
			$page_next = $page_current+1;
			if($page_next > $page_total){
				$page_next = $page_total;
			}//end if
			//echo $utils->cache;
			if($show_results === false){
				$results = false;
				$paging_list = false;
			}//end if
			if($show_results){
			?>
                <h2>Search Results</h2>
                <div class="box clearfix">
                <?php
				if($results !== false){
				?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Status</th>
                            <th>Date Modified</th>
                            <th>Submission Title</th>
                            <th>Medium</th>
                            <th class="last"></th>
                        </tr>
                    </thead>
                    <?php
					if($page_total > 1){
					?>
					<tfoot>
						<tr>
							<td colspan="5">
								<ul class="pagination">
                                    <li><a href="?page=<?php echo $page_prev.'&'.$url_params ?>">&laquo;</a></li>
                                    <?php
                                    if(is_array($paging_list) && sizeof($paging_list) > 0){
										unset($_REQUEST['page']);
										$url_params = http_build_query($_REQUEST);
                                        for($i = 0; $i < sizeof($paging_list); $i++){
                                            echo '<li';
                                            if($paging_list[$i]['page'] == $page_current){
                                                echo ' class="active"';
                                            }//end if

                                            echo '><a href="'.__BASEPATH__.'archive/?page='.$paging_list[$i]['page'].'&'.$url_params.'">'.$paging_list[$i]['page'].'</a></li>';
                                        }//end for i
                                    }//end if
                                    ?>
                                    <li><a href="?page=<?php echo $page_next.'&'.$url_params ?>">&raquo;</a></li>
                                </ul>
							</td>
						</tr>
					</tfoot>
					<?php
					}//end if
					?>
                    <tbody>
                    <?php
                    for($i = 0; $i < sizeof($results); $i++){
						$last_edit = date("jS M Y g:iA",$results[$i]['create_date']);
						if($results[$i]['reply_date'] > 0){
							$last_edit = date("jS M Y g:iA",$results[$i]['reply_date']);
						}//end if
						$title = trim($results[$i]['ad_submission_title']);
						if($title == ""){
							$title = $results[$i]['attachment_friendly_name'];
						}//end if
						$encdata = urlencode(base64_encode($results[$i]['ad_submission_id']));
						if(!$is_admin){
							$link = __BASEPATH__.'submission/view/?encdata='.$encdata;
						} else {
							$link = __BASEPATH__.'admin/view/?encdata='.$encdata;
						}//end if
				?>
                	<!--tr>
                    	<td colspan="5">
                        	<ol class="breadcrumb">
                              <li><?php echo $results[$i]['brand_name'] ?></li>
                              <li><?php echo $results[$i]['campaign_name'] ?></li>
                              <li class="active"><?php echo $results[$i]['ad_title'] ?></li>
                            </ol>

                        </td>
                    </tr-->
					<tr class="<?php echo $results[$i]['ad_submission_status'] ?>">
						<td>
							<span class="status <?php echo $results[$i]['ad_submission_status'] ?>"></span>
						</td>
						<td><?php echo $last_edit?></td>
						<td><a href="<?php echo $link ?>" target="_blank"><strong><?php echo $title ?></strong></a></td>
						<td><a href="<?php echo $link ?>" target="_blank"><?php echo $results[$i]['ad_medium_name'] ?></a></td>
						<td class="last"><a href="<?php echo $link ?>" class="btn btn-primary btn-xs" target="_blank">View</a></td>
					</tr>
				<?php
					}//end for i
                    ?>
                    </tbody>
                </table>
                <?php
				} else {
				?>
                <div class="alert alert-warning"><i class="fa fa-warning"></i> Nothing found with the passed search criteria</div>
                <?php
				}//end if
				?>
                </div><!-- /.box -->
        <?php
			}//end if
		?>
            </div><!-- /.sub_section -->
        </div><!-- /.col-md-8 -->
    </form>
</section>
