<?php
	#ignore template

	$brands = $utils->call('brands');
	$brands_categories_list = $brands->get_brands_categories();

	$encdata = array('user_id' => $user_id,'company_id' => $company_id,'token' => $token);
	$encdata = urlencode(base64_encode(serialize($encdata)));
?>
<h1>Submit New Ad</h1>
<section class="row">
    <div class="col-md-3">
        <h2>Steps</h2>
        <ul>
            <li class="step current">1 Submission Details</li>
            <li class="step next">2 Upload Material</li>
            <li class="step next">3 Approval Details</li>
            <li class="step next">4 Confirm &amp; Submit</li>
        </ul>
        <hr />
        <div class="helpbox">
        	<p>Fill in the details of your submission in the form.</p>
			<p>Select from existing brand owners, brands, campaigns and ads from the dropdown menus or click &lsquo;Add new&rsquo; to create new ones.</p>
			<p>When creating a new brand you will be asked for Billing Company's details.</p>
            <p>When you've added all details, click &lsquo;Save&rsquo; and move on to the next step.</p>
            <p>Click on the back button to go back to the previous step.</p>
        </div>
    </div>
    <!-- start right-->
    <div class="col-md-9">
        <h2 class="sub_section clearfix">Step 1 - Campaign Details</h2>
        <div class="box">
            <form method="post" name="new-submission" id="new-submission" action="#">
            	<input type="hidden" name="encdata" id="encdata" value="<?php echo $encdata ?>">
                <label>Brand Owner</label>
	            <div class="form-group clearfix row">
                    <div class="col-sm-10">
                        <select class="form-control" id="brand-owner" name="brand_owner">
                            <option>Select One</option>
                        <?php
                            $advertisers_list = $brands->get_brands_owners(array('company_id' => $company_id));
                            if($advertisers_list !== false){
                                for($i = 0; $i < sizeof($advertisers_list); $i++){
                                    echo '<option value="'.$advertisers_list[$i]['brand_owner_id'].'">'.$advertisers_list[$i]['brand_owner_name'].'</option>';
                                }//end for i
                            }//end if
                        ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                    	<span class="pull-right">
                    	<a href="#new-brand-owner-modal" data-toggle="modal" class="btn btn-add btn-xs"><i class="fa fa-plus-circle"></i> Add New</a>
                        </span>
                    </div>
                </div>
                <label>Brand</label>
                <div class="form-group clearfix row">
                    <div class="col-sm-10">
                        <select class="form-control disabled"  id="brand" name="brand" disabled>
                            <option>Select a brand owner first</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                    	<span class="pull-right">
                    		<a href="#new-brand-modal" data-toggle="modal" class="btn btn-add btn-xs"><i class="fa fa-plus-circle"></i> Add New</a>
                        </span>
                    </div>
                </div>
                <label for="campaign">Campaign</label>
                <div class="form-group clearfix row">
                    <div class="col-sm-10">
                        <select class="form-control" id="campaign" name="campaign" disabled>
                            <option>Select one</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                    	<span class="pull-right">
                    		<a href="#new-campaign-modal" data-toggle="modal" class="btn btn-add btn-xs"><i class="fa fa-plus-circle"></i> Add New</a>
                        </span>
                    </div>
                </div>
                <label>AD</label>
                <div class="form-group clearfix row">
                    <div class="col-sm-10">
                        <select class="form-control" id="ad" name="ad" disabled>
                            <option>Select one</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                    	<span class="pull-right">
                    		<a href="#new-ad-modal" data-toggle="modal" class="btn btn-add btn-xs"><i class="fa fa-plus-circle"></i> Add New</a>
                        </span>
                    </div>
                </div>
                <strong>Substantiation</strong>
                <label class="checkbox">
                    <input type="checkbox" value="1" name="supporting_material" id="supporting_material"><small>Only tick box if this submission consists of supporting material (e.g. research evidence or other substantiation, casting, wardrobe, location, music, lyrics, etc.)</small>
								</label>
            </form>
        </div><!-- /.end box-->
    </div><!-- /.end col-md-9-->
    <button type="button" class="btn btn-primary" id="save-submission-btn">Next Step</button>
</section>
<!-- NEW BRAND OWNER MODAL -->
<div class="modal fade" id="new-brand-owner-modal" tabindex="-1" role="dialog" aria-labelledby="New Brand Owner" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="#" method="post" id="nbo-form" name="nbo-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add a new brand owner</h4>
                </div>
                <div class="modal-body">
                	<label>Brand Owner Name</label>
                    <input type="text" name="nbo_name" id="nbo-name" class="form-control" placeholder="Brand Owner Name">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-brand-owner-btn">Save Brand Owner</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW BRAND OWNER MODAL -->
<!-- NEW BRAND MODAL -->
<div class="modal fade" id="new-brand-modal" tabindex="-1" role="dialog" aria-labelledby="New Brand" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="#" method="post" id="nb-form" name="nb-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add a new brand</h4>
                </div>
                <div class="modal-body">
                	<label>Brand Name</label>
                    <input type="text" name="nb_name" id="nb-name" class="form-control" placeholder="Brand Name">
                	<label>Billing Company Name</label>
                    <input id="nb-company-brand-name" class="form-control" type="text" name="nb_company_brand_name" placeholder="Billing Company's Name">
                    <label>Billing Company Email</label>
                    <input id="nb-company-brand-email" class="form-control" type="text" name="nb_company_brand_email" placeholder="Billing Company's Email Address">
                    <label>Category</label>
                    <select name="nb_company_brand_category_id" id="nb-company-brand-category-id" class="form-control">
                    <?php
                        if($brands_categories_list !== false){
                            for($i = 0; $i < sizeof($brands_categories_list); $i++){
                                echo '<option value="'.$brands_categories_list[$i]['brand_category_id'].'">'.$brands_categories_list[$i]['brand_category_name'].'</option>';
                            }//end for i
                        }//end if
                    ?>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-brand-btn">Save Brand</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW BRAND MODAL -->
<!-- NEW CAMPAIGN MODAL -->
<div class="modal fade" id="new-campaign-modal" tabindex="-1" role="dialog" aria-labelledby="New Campaign" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="#" method="post" id="nc-form" name="nc-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add a new campaign</h4>
                </div>
                <div class="modal-body">
                	<label>Campaign Name</label>
                    <input type="text" name="nc_name" id="nc-name" class="form-control" placeholder="Campaign Name">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-campaign-btn">Save Campaign</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW CAMPAIGN MODAL -->
<!-- NEW AD MODAL -->
<div class="modal fade" id="new-ad-modal" tabindex="-1" role="dialog" aria-labelledby="New AD" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="#" method="post" id="na-form" name="na-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add a new AD</h4>
                </div>
                <div class="modal-body">
                	<label>AD Name</label>
                    <input type="text" name="na_name" id="na-name" class="form-control" placeholder="AD Name">
                    <label>Medium</label>
                    <select name="na_medium" id="na-medium" class="form-control" required>
                        <?php
                        $mediums = $brands->get_mediums();
                        if($mediums !== false){
                            for($i = 0; $i < sizeof($mediums); $i++){
                                echo '<option value="'.$mediums[$i]['ad_medium_id'].'">'.$mediums[$i]['ad_medium_name'].'</option>';
                            }//end for i
                        }//end if
                        ?>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-ad-btn">Save AD</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW AD MODAL -->
<!-- SUPPORTING MATERIAL MODAL -->
<div class="modal fade" id="substantiation-modal" tabindex="-1" role="dialog" aria-labelledby="Substantiation" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="#" method="post" id="na-form" name="na-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Substantiation</h4>
                </div>
                <div class="modal-body">
                	<strong>This submission will be viewed as substantiation and NOT as a piece of marketing communications.</strong>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END NEW AD MODAL -->
