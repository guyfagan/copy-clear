<?php
	#ignore template
	
	$ads = $utils->call('ads');
	$campaigns = $utils->call('campaigns');
	$list_campaigns = $campaigns->get_campaigns(array('company_id' => $company_id, 'limit' => 3));
?>
<h1>Your Dashboard</h1>
<section class="row">
    <div class="col-md-3">
        <h2>Key</h2>
        <div class="helpbox">
            <ul class="key">
                <li class="not-approved">Not Approved</li>
                <li class="not-valid">Not Valid</li>
                <li class="pending">Pending</li>
                <li class="interim">Interim Approval</li>
                <li class="final-approve">Final Approved</li>
                <li class="conversation">Conversation</li>
            </ul>
        </div>
    </div> 
	<div class="col-md-9">
        <div class="sub_section clearfix">
            <h2>Upload New Submission</h2>
            <div class="box">
                <div class="row">
                    <p class="col-md-8">There are three simple steps to uploading a new submission. You’ll find hand hints on the left of the form. Start by clicking the upload new submission button. </p>
                    <ul class="col-md-4">
                        <li><a class="btn btn-primary" href="<?php echo __BASEPATH__ ?>submission/new/">Upload New Submission</a></li>
                    </ul>
                </div>
            </div>
            <!-- end box-->
        </div>
        <!-- start accordion -->
        <div class="panel-group" id="accordion">
        <?php
		if($list_campaigns !== false){
			for($i = 0; $i < sizeof($list_campaigns); $i++){
				$ads_list = $ads->get_ads(array('campaign_id' => $list_campaigns[$i]['campaign_id']));
		?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#panel-<?php echo $list_campaigns[$i]['campaign_id'] ?>">
                    <?php echo $list_campaigns[$i]['campaign_name'] ?>
                    </a>
                </h4>
            </div>
            <div id="panel-<?php echo $list_campaigns[$i]['campaign_id'] ?>" class="panel-collapse collapse<?php
				if($i == 0){
					echo ' in';	
				}//end if
				?>">
            <?php
				if($ads_list !== false){
					for($x = 0; $x < sizeof($ads_list); $x++){
			?>
                <div class="panel-body">
                	<h5><?php echo $ads_list[$x]['ad_title'] ?></h5>
                    <table>
                        <tr>
                            <th>Status</th>
                            <th>Date Modified</th>
                            <th>Brand</th>
                            <th>Submission Title</th>
                            <th>Medium</th>
                            <th class="last"></th>
                        </tr>
                        <tr>
                            <td><ul class="key">
                                <li class="not_approve"></li>                 
                            </ul></td>
                            <td>24th Jan 2:30pm</td>
                            <td>Heineken</td>
                            <td>Odyssey TVC endframe Odyssey TVC</td>
                            <td>TV</td>
                            <td class="last"><a href="#">View</a></td>
                        </tr>
                        <tr>
                            <td><ul class="key">
                                <li class="pending"></li>                 
                            </ul></td>
                            <td>23th Jan 4:15pm</td>
                            <td>Heineken</td>
                            <td>Odyssey TVC endframe Odyssey TVC</td>
                            <td>Owned Online Activations</td>
                            <td class="last"><a href="#">View</a></td>
                        </tr>
                    </table>
                </div><!-- /.panel-body -->
            <?php
					}//end for x
				}//end if
			?>
            </div><!-- /.panel-collapse -->
        </div><!-- /.panel -->
		<?php				
			}//end for i
		}//end if
		?>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                  Collapsible Group Item #1
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body">
                <table>
                    <tr>
                        <th>Status</th>
                        <th>Date Modified</th>
                        <th>Brand</th>
                        <th>Submission Title</th>
                        <th>Medium</th>
                        <th class="last"></th>
                    </tr>
                    <tr>
                        <td><ul class="key">
                            <li class="not_approve"></li>                 
                        </ul></td>
                        <td>24th Jan 2:30pm</td>
                        <td>Heineken</td>
                        <td>Odyssey TVC endframe Odyssey TVC</td>
                        <td>TV</td>
                        <td class="last"><a href="#">View</a></td>
                    </tr>
                    <tr>
                        <td><ul class="key">
                            <li class="pending"></li>                 
                        </ul></td>
                        <td>23th Jan 4:15pm</td>
                        <td>Heineken</td>
                        <td>Odyssey TVC endframe Odyssey TVC</td>
                        <td>Owned Online Activations</td>
                        <td class="last"><a href="#">View</a></td>
                    </tr>
                </table>
              </div>
            </div>
          </div>  
        </div><!-- end accordion -->
        <ul>
            <li><a class="btn btn-primary" href="campaigns_list.php">View All Campaigns</a></li>
        </ul>
	</div>
</section>