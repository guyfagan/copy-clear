<?php
	#ignore template

	$ads = $utils->call('ads');
	$messages = $utils->call('messages');
	$encdata = $_REQUEST['encdata'];
	$encdata = base64_decode(urldecode($encdata));
	$submission_id = $encdata;
	$data = $ads->get_submission(array('id' => $submission_id,'detailed' => true));
	$ads->mark_submission_as_viewed(array('id' => $submission_id));
	$ads->log_activity(array('ref_id' => $submission_id,'type' => 'actions','value' => 'view'));
	$title = trim($data['ad_submission_title']);
	if($title == ""){
		$title = $data['attachment_friendly_name'];
	}//end if
	$last_edit = date("jS M g:iA",$data['create_date']);
	if($data['reply_date'] > 0){
		$last_edit = date("jS M g:iA",$data['reply_date']);
	}//end if
?>
<ol class="breadcrumb">
  <li><?php echo $data['brand_name'] ?></li>
  <li><?php echo $data['campaign_name'] ?></li>
  <li class="active"><?php echo $data['ad_title'] ?></li>
</ol>
<h2><?php echo $title ?></h2>
<?php
if(trim($data['ad_submission_file_details']) != ""){
?>
<p><?php echo $data['ad_submission_file_details'] ?></p>
<?php
}//end if
?>
<section class="row">
    <div class="col-md-3">
        <div class="helpbox">
        	<h4>Status</h4>
        	<span class="status <?php echo $data['ad_submission_status'] ?>"></span><?php echo ucwords($data['ad_submission_status']) ?><hr />
            <?php
			if($data['ad_submission_approval_code'] != ""){
				echo '<strong>Approval Code:</strong> <div class="clearfix"><span class="label label-success approval-code-label">'.$data['ad_submission_approval_code'].'</span></div><hr />';
			}//end if
			if($data['ad_submission_status'] == 'final-approval' || $data['ad_submission_status'] == 'interim-approval'){
				echo '<p>Brand owners are responsible for ensuring that everyone featured in marketing communications is over the age of 25 years and looks over the age of 25 years. Brands must be able to provide substantiation on request.</p>';
			}// end if
			if($data['ad_submission_codes'] != ""){
				$codes = explode(",",$data['ad_submission_codes']);
			?>
            	<strong>Codes in violation:</strong>
                <div class="clearfix error-codes">
                <?php
				for($i = 0; $i < sizeof($codes); $i++){
					$code_data = $ads->get_code(array('id' => (int)$codes[$i]));
					if($code_data !== false){
						echo '<button type="button" class="btn btn-warning btn-xs error-code" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="right" data-title="Code '.$code_data['code_code'].'" data-content="'.$code_data['code_text'].'" data-html="true">'.$code_data['code_code'].'</button>';
					}//end if
				}//end for i
				?>
                </div>
                <hr />
			<?php
			}//end if
			if($data['ad_submission_status'] == 'not-approved'){
				echo '<p>Please note that there is a new ASAI code which is valid from march 1st 2016. All submissions running after this date must have a 2016 approval code.  2015 codes will not be valid after february 29, 2016.</p>';
			}// end if
			?>
            <strong>Last Edit:</strong> <?php echo $last_edit ?>
        </div>
    </div>
    <!-- start right-->
    <div class="col-md-9">
        <div class="box databox">
        	<h3><div class="circle"><i class="fa fa-bars"></i></div> Submission Details <a href="#" class="pull-right expander" data-toggle="collapse" data-target="#details-container"><i class="fa fa-chevron-down"></i></a></h3>
            <div id="details-container" class="collapse">
                <table>
                    <tr>
                        <td class="labels">Advertiser</td>
                        <td class="data"><?php echo $data['brand_owner_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Brand</td>
                        <td class="data"><?php echo $data['brand_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Campaign</td>
                        <td class="data"><?php echo $data['campaign_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Medium</td>
                        <td class="data"><?php echo $data['ad_medium_name'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Ad Title</td>
                        <td class="data"><?php echo $data['ad_title'] ?></td>
                    </tr>
                    <?php
					if($data['ad_submission_support'] != 1){
					?>
                    <tr>
                        <td class="labels">Approval Sought</td>
                        <td class="data"><?php echo ucwords($data['ad_submission_approval_sought']) ?> Approval</td>
                    </tr>
                    <?php
					}//end if
					?>
                    <tr>
                        <td class="labels">Substantiation</td>
                        <td class="data"><?php
                        if($data['ad_submission_support'] == 1){
                            echo 'Yes';
                        } else {
                            echo 'No';
                        }//end if
                        ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Origin of Work</td>
                        <td class="data"><?php
                            switch($data['ad_submission_origin']){
                                default:
                                case 'ireland':
                                    echo 'Created in Ireland';
                                break;
                                case 'adapted':
                                    echo 'Adaptation of international material';
                                break;
                                case 'elsewhere':
                                    echo 'Created elsewhere';
                                break;
                            }
                        ?></td>
                    </tr>
                    <tr>
                        <td class="labels">Comments</td>
                        <td class="data"><?php echo $data['ad_submission_comments'] ?></td>
                    </tr>
                    <tr>
                        <td class="labels">File</td>
                        <td class="data">
                        <?php
                            if($data['attachment_type'] == 'image'){
                        ?>
                            <a href="<?php echo __S3_PUBLIC_PATH__.$data['attachment_filename'] ?>" target="_blank" rel="prettyPhoto">
                                <img src="<?php echo __UPLOADPATH__ ?>photos/t/<?php echo $data['attachment_filename'] ?>" alt="<?php echo $data['attachment_friendly_name'] ?>">
                            </a>
                        <?php
                            } else {
                        ?>
                            <a href="<?php echo $data['attachment_url'] ?>" target="_blank"><?php echo $data['attachment_filename'] ?></a>
                        <?php
                            }//end if
                        ?>
                        </td>
                    </tr>
                </table>
        	</div><!-- /.collapse -->

        </div><!-- /.end box-->
        <hr />
        <div class="box clearfix">
        	<h3><span class="status conversation"></span> Conversation</h3>
        <?php
			$encdata = array('submission_id' => $submission_id);
			$encdata = urlencode(base64_encode(serialize($encdata)));
			$options = array('ref_id' => $submission_id,'module_id' => 'ads');
			$msg_list = $messages->get_messages($options);
			if($msg_list !== false){
		?>
        	<div id="messages" class="clearfix">
        <?php
				for($i = 0; $i < sizeof($msg_list); $i++){
					$username = $msg_list[$i]['sender_firstname'].' '.$msg_list[$i]['sender_surname'];
					if(in_array($msg_list[$i]['sender_group_id'],$admins)){
						$username = "CopyClear";
					}//end if
		?>
        		<div class="message-bubble clearfix">
					<h4><span><i class="fa fa-user"></i> From <?php echo $username ?></span> - <?php echo date('jS M Y, g:iA',$msg_list[$i]['message_date']) ?></h4>
                    <div class="message-content"><?php echo $msg_list[$i]['message_message'] ?></div>
                </div>
        <?php
				}//end for i
		?>
        	</div>
            <hr />
        <?php
			}//end if
		?>
        	<form action="#" method="post" id="comment-form" name="comment-form">
            	<h4>Add your feedback</h4>
            	<input type="hidden" name="encdata" id="encdata" value="<?php echo $encdata ?>">
                <textarea name="comment" id="comment" class="form-control" rows="4"></textarea>
                <button id="submit-comment" class="btn btn-primary">Submit <i class="fa fa-chevron-right"></i></button>
            </form>
        </div><!-- /.end box-->
    </div><!-- /.end col-md-9-->
</section>
<script>
	$(document).ready(function(){
		$('.error-code').popover();
	});
</script>
