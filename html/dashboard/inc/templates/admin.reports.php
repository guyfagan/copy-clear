<?php
	#template name: Whatever

	$ads = $utils->call('ads');
	$brands = $utils->call('brands');
?>
<h1>Reports</h1>
<section class="row" id="report">
	<form method="post" id="form-search" name="form-search" action="<?php echo __BASEPATH__ ?>admin/reports/">
        <div class="col-md-4">
        <?php
		require_once('inc/snippets/archive.search.panel.inc.php');
		?>
        </div>
        <div class="col-md-8">
            <div class="sub_section clearfix">
                <h2>Search the Archive</h2>
                <div class="box clearfix">
                    <p>Use the filters on the left to refine your search.</p>

                    <input id="search" class="form-control" type="text" name="search" <?php
					if(isset($_REQUEST['search'])){
						echo ' value="'.$_REQUEST['search'].'"';
					}//end if
					?> placeholder="Enter your search term">
                    <div class="clearfix" id="seach-filters">
                        <h4>Search Filters</h4>
                        <div id="brand-category" class="filter-group clearfix">
                            <strong>Category</strong>
                            <ul></ul>
                        </div>
                        <div id="brand" class="filter-group clearfix">
                            <strong>Brand</strong>
                            <ul></ul>
                        </div>
                        <div id="company" class="filter-group clearfix">
                            <strong>Company</strong>
                            <ul></ul>
                        </div>
                        <div id="approval" class="filter-group clearfix">
                            <strong>Approval</strong>
                            <ul></ul>
                        </div>
                        <div id="status" class="filter-group clearfix">
                            <strong>Status</strong>
                            <ul></ul>
                        </div>
                        <div id="medium" class="filter-group clearfix">
                            <strong>Medium</strong>
                            <ul></ul>
                        </div>
                        <div id="origin" class="filter-group clearfix">
                            <strong>Origin</strong>
                            <ul></ul>
                        </div>
                        <div id="company-types" class="filter-group clearfix">
                            <strong>Company Type</strong>
                            <ul></ul>
                        </div>
                        <?php
						if(isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != ""){
							echo '<p class="from-date-block"><i class="fa fa-calendar"></i> From <span>'.$_REQUEST['from_date'].'</span></p>';
						} else {
							echo '<p class="from-date-block hide"><i class="fa fa-calendar"></i> From <span></span></p>';
						}//end if

						if(isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != ""){
							echo '<p class="to-date-block"><i class="fa fa-calendar"></i> To <span>'.$_REQUEST['to_date'].'</span></p>';
						} else {
							echo '<p class="to-date-block hide"><i class="fa fa-calendar"></i> To <span></span></p>';
						}//end if
						?>
                    </div>

                    <button type="button" class="btn btn-primary" id="search-btn">Search</button>
                </div><!-- /.box-->
            </div><!-- /.sub_section -->
            <div class="sub_section clearfix">
            <?php
			$options = array('ignore_status' => array('pending','sm-received','not-valid'));
			$show_results = false;
			if(!$is_admin){
				$options['company_id'] = $company_id;
			}
			if(isset($_REQUEST['search']) && $_REQUEST['search'] != ""){
				$show_results = true;
				$options['search'] = $_REQUEST['search'];
			}//end if
			if(isset($_REQUEST['brand_category_id']) && is_array($_REQUEST['brand_category_id'])){
				$show_results = true;
				$options['brand_category_id'] = $_REQUEST['brand_category_id'];
			}//end if
			if(isset($_REQUEST['company_id']) && is_array($_REQUEST['company_id'])){
				$show_results = true;
				$options['company_id'] = $_REQUEST['company_id'];
			}//end if
			if(isset($_REQUEST['company_types']) && is_array($_REQUEST['company_types'])){
				$show_results = true;
				$options['company_type_id'] = $_REQUEST['company_types'];
			}//end if
			if(isset($_REQUEST['brand_id']) && is_array($_REQUEST['brand_id'])){
				$show_results = true;
				$options['brand_id'] = $_REQUEST['brand_id'];
			}//end if
			if(isset($_REQUEST['approval']) && is_array($_REQUEST['approval'])){
				$show_results = true;
				$options['approval'] = $_REQUEST['approval'];
			}//end if
			if(isset($_REQUEST['status']) && is_array($_REQUEST['status'])){
				$show_results = true;
				$options['status'] = $_REQUEST['status'];
			}//end if
			if(isset($_REQUEST['year_only']) && $_REQUEST['year_only'] != ""){
				$options['year_only'] = $_REQUEST['year_only'];
			}//end if
			if(isset($_REQUEST['from_date']) && $_REQUEST['from_date'] != ""){
				$show_results = true;
				$options['from_date'] = $_REQUEST['from_date'];
			}//end if
			if(isset($_REQUEST['to_date']) && $_REQUEST['to_date'] != ""){
				$show_results = true;
				$options['to_date'] = $_REQUEST['to_date'];
			}//end if
			if(isset($_REQUEST['origin']) && is_array($_REQUEST['origin'])){
				$show_results = true;
				$options['origin'] = $_REQUEST['origin'];
			}//end if
			if(isset($_REQUEST['medium_id']) && is_array($_REQUEST['medium_id'])){
				$show_results = true;
				$options['medium_id'] = $_REQUEST['medium_id'];
			}//end if

			$results = $ads->search_report($options);
			//echo $utils->cache;

			?>
                <h2>Results</h2>
                <div class="box clearfix">
                	<div class="alert alert-success">
                <?php
					echo 'We found <strong>'.$results['found'].'</strong> results with the current search criteria';
				?>
                	</div>
                </div><!-- /.box -->
                <hr />
     			<div class="clearfix">
                	<a href="<?php echo __BASEPATH__ ?>admin/reports/charts/" class="btn btn-default"><i class="fa fa-bar-chart-o fa-2x"></i> Click here for chart reports</a>
                </div>
            </div><!-- /.sub_section -->
        </div><!-- /.col-md-8 -->
    </form>
</section>
