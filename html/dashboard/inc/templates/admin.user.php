<?php
	$user_id = base64_decode(urldecode($_GET['encdata']));
	$user_details = $users->get_user(array('id' => $user_id,'meta' => true));
	$company_details = $companies->get_company(array('id' => $user_details['user_company_id']));
	
	$status = 'Active';
	$btn_class = 'btn-success';
	if($user_details['user_active'] == '0'){
		$status = 'Inactive';	
		$btn_class = 'btn-warning';
	}//end if
?>
<h1><span class="circle"><i class="fa fa-user"></i></span> User Details</h1>
<section class="row">
    <div class="col-md-3">
    	<h2>Status</h2>
        <div class="helpbox">
        	<strong><a class="btn btn-link editable editable-click" data-value="<?php echo $user_details['user_active'] ?>" data-pk="<?php echo $user_id ?>" href="#"><?php echo $status ?></a></strong>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box clearfix">
            <form class="form-horizontal" action="#" role="form" name="update-user" id="update-user">                       
                <div class="form-group">
                    <label class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?php echo $user_details['user_firstname'].' '.$user_details['user_surname'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><a href="mailto:<?php echo $user_details['iser_email'] ?>"><?php echo $user_details['user_email'] ?></a></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Telephone</label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?php echo $user_details['meta_telephone'] ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mobile</label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?php echo $user_details['meta_mobile'] ?></p>
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Name</label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?php echo $company_details['company_name'] ?></p>
                    </div>
                </div>   
                <div class="form-group">
                <?php
					switch($company_details['company_status']){
						case 'pending':
							$company_status = '<span class="label label-warning">Pending</span>';
							break;	
						case 'active':
							$company_status = '<span class="label label-success">Active</span>';
							break;
					}
				?>
                    <label class="col-sm-3 control-label">Company Status</label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?php echo $company_status ?></p>
                    </div>
                </div>         
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Type</label>
                    <div class="col-sm-9">
                        <p class="form-control-static"><?php echo $company_details['company_type_label'] ?></p>
                    </div>
                </div>                         
            </form>
        </div>
    </div>
</section>
<script>
	$(document).ready(function(){
		$('.editable').editable({
			type: 'select',
			showbuttons: false,
			url: $.copyclear.apipath+'update.user.status',
			source: [
				{value: 1, text: 'Active'},
				{value: 0, text: 'Inactive'}
			],
			ajaxOptions: {			
				dataType: 'json'
			},
			mode: 'inline',
			success: function(response, newValue) {
				/*if(response.result == 'success'){
					if(newValue == 1){
						$(this).removeClass('btn-warning').addClass('btn-success');
					} else {
						$(this).removeClass('btn-success').addClass('btn-warning');
					}
				}*/
			}
		});		
	});
</script>