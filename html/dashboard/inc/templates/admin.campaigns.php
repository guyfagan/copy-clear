<?php
	#ignore template
	
	$ads = $utils->call('ads');
	$campaigns = $utils->call('campaigns');
	$options = array('order_by_submission' => true);
	
	if(!isset($show)){
		$show = 'recent';	
	}//end if
	
	if($show == 'recent'){
		$options['days'] = $admin_days_range;
	}//end if
	$utils->create_paging($_GET['page'],20);
	$ads_list = $ads->get_ads($options);	
	# PAGINATION #
	$paging_stats = $utils->get_paging_stats();		
	
	$paging_list = $utils->get_paging(array('range' => 10,'cms' => false));
	//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
	$utils->unset_paging();
	$page_start = $paging_stats['start']+1;
	$page_end = $paging_stats['start']+10;
	if($page_end > $paging_stats['founds']){
		$page_end = $paging_stats['founds'];
	}//end if
	$page_total = $paging_stats['pages'];
	$page_current = $paging_stats['current_page'];
	$page_prev = $page_current-1;
	if($page_prev == "0"){
		$page_prev = 1;
	}//end if
	$page_next = $page_current+1;
	if($page_next > $page_total){
		$page_next = $page_total;
	}//end if
?>
<h1>Campaigns</h1>
<section class="row">
    <div class="col-md-3">
    	<ul>
            <li class="step<?php if($show == 'recent'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/campaigns/">Active Campaigns</a></li>
            <li class="step<?php if($show == 'all'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/campaigns/all/">All Campaigns</a></li>
        </ul>
 		<hr />       
        <?php
		require_once('inc/snippets/keys.inc.php');
		?>
    </div> 
	<div class="col-md-9">        
        <!-- start accordion -->
        <div class="panel-group" id="accordion">
        	<?php
			if($show == 'recent'){			
       			echo '<h2>Active Campaigns</h2>';      
			} else {
				echo '<h2>All Campaigns</h3>';	
			}//end if
			if($ads_list !== false){
                 for($x = 0; $x < sizeof($ads_list); $x++){
					 $submissions = $ads->get_submissions(array('ad_id' => $ads_list[$x]['ad_id']));					
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-<?php echo $ads_list[$x]['ad_id'] ?>">
                        <?php echo $ads_list[$x]['brand_name']." / ".$ads_list[$x]['campaign_name']." / ".$ads_list[$x]['ad_title'] ?>
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-<?php echo $ads_list[$x]['ad_id'] ?>" class="panel-collapse collapse<?php
                    if($i == 0){
                        echo ' in';	
                    }//end if
                    ?>">
                    <div class="panel-body">                
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>Date Modified</th>                                  
                                    <th>Submission Title</th>
                                    <th>Medium</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							include('inc/snippets/user.panel.list.inc.php');
							?>
                            </tbody>                            
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
           </div><!-- /.panel -->		
           <?php
                        }//end for x
                    }//end if
                ?>   
           <?php
		   if($page_total > 1){
			?>			
                <ul class="pagination clearfix">
                    <li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
                    <?php
                    if(is_array($paging_list) && sizeof($paging_list) > 0){
                        for($i = 0; $i < sizeof($paging_list); $i++){
                            echo '<li';
                            if($paging_list[$i]['page'] == $page_current){
                                echo ' class="active"';	
                            }//end if
                            $url_params = http_build_query($_REQUEST);
                            echo '><a href="'.__BASEPATH__.'campaigns/?page='.$paging_list[$i]['page'].'&'.$url_params.'">'.$paging_list[$i]['page'].'</a></li>';
                        }//end for i
                    }//end if
                    ?>                      
                    <li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
                </ul>					
			<?php
			}//end if	
		   ?>
        </div><!-- end accordion -->        
	</div>
</section>