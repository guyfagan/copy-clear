<?php
	#ignore template

	require_once("inc/admin.secure.inc.php");

	$encdata = $_REQUEST['encdata'];
	$encdata = base64_decode(urldecode($encdata));
	$company_id = $encdata;
	$company_data = $companies->get_company(array('id' => $company_id));

	$status = 'Active';
	$btn_class = 'btn-success';
	if($company_data['company_status'] == 'pending'){
		$status = 'pending';
		$btn_class = 'btn-warning';
	}//end if
	if($company_data['company_status'] == 'inactive'){
		$status = 'inactive';
		$btn_class = 'btn-danger';
	}//end if
?>
<h1><span class="circle"><i class="fa fa-users"></i></span> Company Details</h1>
<section class="row">
    <div class="col-md-3">
    	<h2>Status</h2>
      <div class="helpbox">
      	<strong><a class="btn btn-link editable editable-click company-status" data-value="<?php echo $company_data['company_status'] ?>" data-pk="<?php echo $company_id ?>" href="#"><?php echo $status ?></a></strong>
      	<br />
      	<?php
        /*if($company_data['company_reactivated_by'] != ''){
          $users = $utils->call('users');
          $react_guy = $users->get_user(array('id' => $company_data['company_reactivated_by'])){
          if($react_guy !== false){
            echo 'Company reactivated by '.$react_guy['user_firstname'].' '.$react_guy['user_surname'];
          }//end if
        }//end if*/
        ?>
      </div>
    </div>
    <div class="col-md-9">
    	<h2>Basic Info</h2>
        <div class="box clearfix">
        	<form class="form-horizontal" action="#" role="form" name="update-user" id="update-user">
            <div class="form-group">
              <label class="col-sm-3 control-label">Name</label>
              <div class="col-sm-9">
                <p class="form-control-static"><?php echo $company_data['company_name'] ?></p>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Type</label>
              <div class="col-sm-9">
                <p class="form-control-static"><?php echo $company_data['company_type_label'] ?></p>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Contact Name</label>
              <div class="col-sm-9">
                <p class="form-control-static"><?php echo $company_data['company_contact_name'] ?></p>
              </div>
            </div>
          </form>
        </div>
        <hr />
        <h2>Users</h2>
        <div class="box clearfix">
        <?php
				$company_users = $companies->get_company_users(array('meta' => true, 'company_id' => $company_id));
				if($company_users !== false){
				?>
      	<table class="table">
        	<thead>
            	<tr>
              	<th>Name</th>
                <th>Contact Details</th>
                <th>Last Submission</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            <?php
						for($i = 0; $i < sizeof($company_users); $i++){
							$encdata = urlencode(base64_encode($company_users[$i]['user_id']));
							$link = __BASEPATH__.'admin/user/?encdata='.$encdata;
							$status = 'active';
							$btn_class = 'btn-success';
							if($company_users[$i]['user_active'] == '0'){
								$status = 'inactive';
								$btn_class = 'btn-warning';
							}//end if
						?>
							<tr>
								<td><?php echo $company_users[$i]['user_firstname'].' '.$company_users[$i]['user_surname'] ?></td>
                <td>
                	Email: <a href="mailto:<?php echo $company_users[$i]['user_email'] ?>"><?php echo $company_users[$i]['user_email'] ?></a>
                	<?php
									if($company_users[$i]['meta_telephone'] != ""){
									?>
                  <br />Landline: <?php echo $company_users[$i]['meta_telephone'] ?>
                  <?php
									}//end if
									if($company_users[$i]['meta_mobile'] != ""){
									?>
                  <br />Mobile: <?php echo $company_users[$i]['meta_mobile'] ?>
                  <?php
									}//end if
									?>
                </td>
                <td><?php
                if($company_users[$i]['ad_submission_date'] != ""){
                  echo date("d/m/Y H:i",$company_users[$i]['ad_submission_date']);
                }//end if
                ?></td>
								<td class="last">
                  <a class="btn <?php echo $btn_class ?> btn-xs editable editable-click user-status" data-value="<?php echo $company_users[$i]['user_active'] ?>" data-pk="<?php echo $company_users[$i]['user_id'] ?>" href="#"><?php echo $status ?></a>
                  <a href="<?php echo $link ?>" class="btn btn-primary btn-xs">View</a>
                </td>
							</tr>
						<?php
						}//end for i
						?>
                </tbody>
            </table>
        <?php
				} else {
				?>
        	<div class="alert">No users found for this company</div>
        <?php
				}//end if
				?>
    	</div>
    </div>
</section>
<script>
	$(document).ready(function(){
		$('.company-status').editable({
			type: 'select',
			showbuttons: false,
			url: $.copyclear.apipath+'update.company.status',
			source: [
				{value: 'active', text: 'Active'},
				{value: 'pending', text: 'Pending'},
				{value: 'inactive', text: 'Inactive'}
			],
			ajaxOptions: {
				dataType: 'json'
			},
			mode: 'inline',
			success: function(response, newValue) {
				/*if(response.result == 'success'){
					if(newValue == 1){
						$(this).removeClass('btn-warning').addClass('btn-success');
					} else {
						$(this).removeClass('btn-success').addClass('btn-warning');
					}
				}*/
			}
		});

		$('.user-status').editable({
			type: 'select',
			showbuttons: false,
			url: $.copyclear.apipath+'update.user.status',
			source: [
				{value: 1, text: 'Active'},
				{value: 0, text: 'Inactive'}
			],
			ajaxOptions: {
				dataType: 'json'
			},
			mode: 'inline',
			success: function(response, newValue) {
				/*if(response.result == 'success'){
					if(newValue == 1){
						$(this).removeClass('btn-warning').addClass('btn-success');
					} else {
						$(this).removeClass('btn-success').addClass('btn-warning');
					}
				}*/
			}
		});
	});

</script>