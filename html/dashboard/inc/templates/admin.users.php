<?php
	#ignore template
	
	$registrations = $utils->call('registrations');
	$options = array('group_id' => 4);
	
	if(!isset($show)){
		$show = 'pending';	
		$options['active'] = 0;
	}//end if	
	$options['sort_by'] = 'user_datareg';
	$options['sort_order'] = 'DESC';
	
	$utils->create_paging($_GET['page'],20);
	$list = $registrations->get_users($options);	
	# PAGINATION #
	$paging_stats = $utils->get_paging_stats();		
	
	$paging_list = $utils->get_paging(array('range' => 10,'cms' => false));
	//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
	$utils->unset_paging();
	$page_start = $paging_stats['start']+1;
	$page_end = $paging_stats['start']+10;
	if($page_end > $paging_stats['founds']){
		$page_end = $paging_stats['founds'];
	}//end if
	$page_total = $paging_stats['pages'];
	$page_current = $paging_stats['current_page'];
	$page_prev = $page_current-1;
	if($page_prev == "0"){
		$page_prev = 1;
	}//end if
	$page_next = $page_current+1;
	if($page_next > $page_total){
		$page_next = $page_total;
	}//end if
?>
<h1>Users</h1>
<section class="row">
    <div class="col-md-3">
    	<ul>
            <li class="step<?php if($show == 'pending'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/users/pending/">Pending</a></li>
            <li class="step<?php if($show == 'all'){ echo ' current'; } else { echo ' next'; } ?>"><a href="<?php echo __BASEPATH__ ?>admin/users/all/">All Users</a></li>
        </ul> 		
    </div> 
	<div class="col-md-9">        
        <!-- start accordion -->
        <div class="panel-group" id="accordion">
        	<?php
			if($show == 'pending'){			
       			echo '<h2>Pending</h2>';      
			} else {
				echo '<h2>All Users</h3>';	
			}//end if			           
			?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Users
                    </h4>
                </div> 
                <div class="panel-body">                
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Company</th>                                  
                                <th>Status</th>    
                                <th>Registration Date</th>                           
                                <th class="last"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						if($list !== false){     
							for($i = 0; $i < sizeof($list); $i++){
								$encdata = urlencode(base64_encode($list[$i]['user_id']));
								$link = __BASEPATH__.'admin/user/?encdata='.$encdata;
								$status = 'active';
								$btn_class = 'btn-success';
								if($list[$i]['user_active'] == '0'){
									$status = 'inactive';	
									$btn_class = 'btn-warning';
								}//end if
							?>
								<tr>
									<td><?php echo $list[$i]['user_firstname'].' '.$list[$i]['user_surname'] ?></td>
									<td><?php echo $list[$i]['company_name'] ?></td>
									<td><a class="btn <?php echo $btn_class ?> btn-xs editable editable-click" data-value="<?php echo $list[$i]['user_active'] ?>" data-pk="<?php echo $list[$i]['user_id'] ?>" href="#"><?php echo $status ?></a></td>
									<td><?php echo $list[$i]['user_datareg'] ?></td>
									<td class="last"><a href="<?php echo $link ?>" class="btn btn-primary btn-xs">View</a></td>
								</tr>
							<?php	
							}//end for i
						} else {
						?>
                        	<tr>
                            	<td colspan="5"><div class="alert alert-warning">No users found</div></td>
                            </tr>
                        <?php
						}//end if
						?>
                        </tbody>                            
                    </table>
                </div><!-- /.panel-body -->
               
           </div><!-- /.panel -->		  
           <?php
			
		   if($page_total > 1){
			?>			
                <ul class="pagination clearfix">
                    <li><a href="?page=<?php echo $page_prev ?>">&laquo;</a></li>
                    <?php
                    if(is_array($paging_list) && sizeof($paging_list) > 0){
                        for($i = 0; $i < sizeof($paging_list); $i++){
                            echo '<li';
                            if($paging_list[$i]['page'] == $page_current){
                                echo ' class="active"';	
                            }//end if
                            $url_params = http_build_query($_REQUEST);
							if($show == 'pending'){
								echo '><a href="'.__BASEPATH__.'admin/users/pending/?page='.$paging_list[$i]['page'].'">'.$paging_list[$i]['page'].'</a></li>';
							} else {
	                            echo '><a href="'.__BASEPATH__.'admin/users/all/?page='.$paging_list[$i]['page'].'">'.$paging_list[$i]['page'].'</a></li>';
							}
                        }//end for i
                    }//end if
                    ?>                      
                    <li><a href="?page=<?php echo $page_next ?>">&raquo;</a></li>
                </ul>					
			<?php
			}//end if	
		   ?>
        </div><!-- end accordion -->        
	</div>
</section>
<script>
	$(document).ready(function() {
	<?php
	if($list !== false){  
	?>
    	$('.editable').editable({
			type: 'select',
			showbuttons: false,
			url: $.copyclear.apipath+'update.user.status',
			source: [
				{value: 1, text: 'Active'},
				{value: 0, text: 'Inactive'}
			]	
		});	
	<?php
	}//end if
	?>
    });
</script>