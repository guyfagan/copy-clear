<div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#panel-<?php echo $panel_section ?>">
                        
                        <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                        </a>
                    </h4>
                </div>
                <div id="panel-<?php echo $panel_section ?>" class="panel-collapse collapse in">
                    <div class="panel-body">                
                        <table class="table">
                        	<thead>
                                <tr>
                                    <th></th>
                                    <th>Date</th>                                  
                                    <th>Company</th>
                                    <th>Ad</th>                                                              
                                    <th>Submission</th>
                                    <th class="last"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
							if($list !== false){
								for($i = 0; $i < sizeof($list); $i++){
									$last_edit = date("jS M g:iA",$list[$i]['create_date']);
									if($list[$i]['reply_date'] > 0){
										$last_edit = date("jS M g:iA",$list[$i]['reply_date']);
									}//end if
									$title = trim($list[$i]['ad_submission_title']);
									if($title == ""){
										$title = $list[$i]['attachment_friendly_name'];	
									}//end if
									$encdata = urlencode(base64_encode($list[$i]['ad_submission_id']));
									$link = __BASEPATH__.'admin/view/?encdata='.$encdata;
							?>
                            	<tr class="<?php echo $list[$i]['ad_submission_status'] ?>">
                                    <td>
	                                    <span class="status <?php echo $list[$i]['ad_submission_status'] ?>"></span>
                                    </td>
                                    <td><?php echo $last_edit?></td> 
                                    <td><?php echo $list[$i]['company_name']; ?></td>
                                    <td><?php echo '<strong>'.$list[$i]['ad_title'].'</strong><br />Media: '.$list[$i]['ad_medium_name']  ?></td>                                 
                                    <td class="long-words"><?php echo $title ?></td>                                  
                                    <td class="last"><a href="<?php echo $link ?>" class="btn btn-primary btn-xs">View</a></td>
                                </tr>
                            <?php		
								}//end for i
							}//end if
							?>
                            </tbody>                            
                        </table>
                	</div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
            </div><!-- /.panel -->	