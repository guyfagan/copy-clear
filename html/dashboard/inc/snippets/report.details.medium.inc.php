<?php
$medium = $brands->get_medium(array('id' => $medium_id));
echo '<h2>Details for '.$medium['ad_medium_name'].'</h2>';

$options = array('ignore_status' => array('pending','sm-received','not-valid'));
$options['medium_id'] = $medium_id;
$options['from_month'] = $from_month;
$options['to_month'] = $to_month;
$options['from_year'] = $from_year;
$options['to_year'] = $to_year;

$orig_options = $options;

require_once('report.details.viewer.inc.php');
