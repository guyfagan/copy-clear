<?php
$company_type = $companies->get_companies_type(array('id' => $company_type_id));
echo '<h2>Details for '.$company_type['company_type_label'].'</h2>';

$options = array('ignore_status' => array('pending','sm-received','not-valid'));
$options['company_type_id'] = $company_type_id;
$options['from_month'] = $from_month;
$options['to_month'] = $to_month;
$options['from_year'] = $from_year;
$options['to_year'] = $to_year;

$orig_options = $options;

require_once('report.details.viewer.inc.php');
