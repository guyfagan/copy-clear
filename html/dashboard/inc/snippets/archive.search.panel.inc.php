<h2>Select Filters</h2>
    <!-- start accordion -->
            <div class="panel-group filters" id="accordion">
     <!-- accordion 3 -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Date Range <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse in">
                        <div class="panel-body">
                        	<div class="form-padder">
                            	<div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">From</span>
                                        <input type="text" name="from_date" id="from-date" data-target=".from-date-block" class="datepicker form-control"<?php
                                        if(isset($_REQUEST['from_date'])){
											echo ' value="'.$_REQUEST['from_date'].'"';
										}//end if
										?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">To</span>
                                        <input type="text" name="to_date" id="to-date" data-target=".to-date-block" class="datepicker form-control"<?php
                                        if(isset($_REQUEST['to_date'])){
											echo ' value="'.$_REQUEST['to_date'].'"';
										}//end if
										?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="check">
                                        <input type="checkbox" value="1" id="year-only" name="year_only"<?php
                                            if(isset($_REQUEST['year_only'])){
                                                echo ' checked';
                                            }//end if
                                            ?>> Select the entire year
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <!-- end accordion 3 -->
    <!-- accordion 1 -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Category <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                            <?php
                            $categories = $brands->get_brands_categories();
                            if($categories !== false){
                                for($i = 0; $i < sizeof($categories); $i++){
                                  if(trim($categories[$i]['brand_category_name']) !== ''){
                  									$icon = 'fa-plus-circle';
                  									$class = 'filter-add';
                  									if(isset($_REQUEST['brand_category_id']) && is_array($_REQUEST['brand_category_id'])){
                  										if(in_array($categories[$i]['brand_category_id'],$_REQUEST['brand_category_id'])){
                  											$icon = 'fa-minus-circle';
                  											$class = 'filter-add active';
                  										}//end if
                  									}//end if
                              ?>
                                  <li><a class="<?php echo $class ?>" data-group="brand-category" data-field="brand_category_id[]" data-id="<?php echo $categories[$i]['brand_category_id'] ?>" id="panel-brand-category-<?php echo $categories[$i]['brand_category_id'] ?>" href="#"><i class="fa <?php echo $icon ?>"></i> <?php echo $categories[$i]['brand_category_name'] ?></a></li>
                              <?php
                                  }// end if
                                }//end for i
                            }//end if
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
    <!-- end accordion 1 -->
    <!-- accordion 2 -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Brand <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                            <?php
              							if($is_admin){
              	                            $brands_list = $brands->get_brands();
              							} else {
              								$brands_list = $brands->get_brands(array('company_id' => $company_id));
              							}
                            if($brands_list !== false){
                                for($i = 0; $i < sizeof($brands_list); $i++){
                                  if(trim($brands_list[$i]['brand_name']) !== ''){
                									$icon = 'fa-plus-circle';
                									$class = 'filter-add';
                									if(isset($_REQUEST['brand_id']) && is_array($_REQUEST['brand_id'])){
                										if(in_array($brands_list[$i]['brand_id'],$_REQUEST['brand_id'])){
                											$icon = 'fa-minus-circle';
                											$class = 'filter-add active';
                										}//end if
                									}//end if
                            ?>
                                <li><a class="<?php echo $class ?>" data-group="brand" data-field="brand_id[]" data-id="<?php echo $brands_list[$i]['brand_id'] ?>" id="panel-brand-<?php echo $brands_list[$i]['brand_id'] ?>" href="#"><i class="fa <?php echo $icon ?>"></i> <?php echo $brands_list[$i]['brand_name'] ?></a></li>
                            <?php
                                  }// end if
                                }//end for i
                            }//end if
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php
				if($is_admin){
				?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#agency-panel">Agency <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
                    </div>
                    <div id="agency-panel" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                            <?php
                            $companies_list = $companies->get_companies(array('status' => 'active'));
                            if($companies_list !== false){
                                for($i = 0; $i < sizeof($companies_list); $i++){
									$icon = 'fa-plus-circle';
									$class = 'filter-add';
									if(isset($_REQUEST['company_id']) && is_array($_REQUEST['company_id'])){
										if(in_array($companies_list[$i]['company_id'],$_REQUEST['company_id'])){
											$icon = 'fa-minus-circle';
											$class = 'filter-add active';
										}//end if
									}//end if
                            ?>
                                <li><a class="<?php echo $class ?>" data-group="company" data-field="company_id[]" data-id="<?php echo $companies_list[$i]['company_id'] ?>" id="panel-company-<?php echo $companies_list[$i]['company_id'] ?>" href="#"><i class="fa <?php echo $icon ?>"></i> <?php echo $companies_list[$i]['company_name'] ?></a></li>
                            <?php
                                }//end for i
                            }//end if
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php
				}//end if
				?>
  				<div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseApproval">Approval Sought <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
                    </div>
                    <div id="collapseApproval" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                            <?php
								$statuses = array();
								$statuses['final'] = 'Final Approval';
								$statuses['interim'] = 'Interim Approval';
								foreach($statuses as $key => $value){
									$icon = 'fa-plus-circle';
									$class = 'filter-add';
									if(isset($_REQUEST['approval']) && is_array($_REQUEST['approval'])){
										if(in_array($key,$_REQUEST['approval'])){
											$icon = 'fa-minus-circle';
											$class = 'filter-add active';
										}//end if
									}//end if
							?>
                                <li><a class="<?php echo $class ?>" data-group="approval" data-field="approval[]" data-id="<?php echo $key ?>" href="#" id="panel-approval-<?php echo $key ?>"><i class="fa <?php echo $icon ?>"></i> <?php echo $value ?></a></li>
                            <?php
								}//foreach
							?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Status <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                            <?php
								$statuses = array();
								$statuses['final-approval'] = 'Final Approval';
								$statuses['interim-approval'] = 'Interim Approval';
								$statuses['new'] = 'New';
								$statuses['viewed'] = "Viewed";
                $statuses['conversation'] = "Conversation";
								$statuses['not-approved'] = "Not Approved";
								$statuses['not-valid'] = "Not Valid";
                $statuses['incomplete-submission'] = "Incomplete Submission";
								if(!$is_admin){
									$statuses['pending'] = "Pending";
									$statuses['sm-received'] = "Substantiation Received";
								}//end if
								foreach($statuses as $key => $value){
									$icon = 'fa-plus-circle';
									$class = 'filter-add';
									if(isset($_REQUEST['status']) && is_array($_REQUEST['status'])){
										if(in_array($key,$_REQUEST['status'])){
											$icon = 'fa-minus-circle';
											$class = 'filter-add active';
										}//end if
									}//end if
							?>
                                <li><a class="<?php echo $class ?>" data-group="status" data-field="status[]" data-id="<?php echo $key ?>" href="#" id="panel-status-<?php echo $key ?>"><i class="fa <?php echo $icon ?>"></i> <?php echo $value ?></a></li>
                            <?php
								}//foreach
							?>
                            </ul>
                        </div>
                    </div>
                </div>
    <!-- end accordion 4 -->
    <!-- accordion 5 -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Origin of Creative <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                            <?php
								$origins = array();
								$origins['ireland'] = 'Created in Ireland';
								$origins['adapted'] = 'Adaptation of international material';
								$origins['elsewhere'] = 'Created elsewhere';
								foreach($origins as $key => $value){
									$icon = 'fa-plus-circle';
									$class = 'filter-add';
									if(isset($_REQUEST['origin']) && is_array($_REQUEST['origin'])){
										if(in_array($key,$_REQUEST['origin'])){
											$icon = 'fa-minus-circle';
											$class = 'filter-add active';
										}//end if
									}//end if
							?>
                                <li><a class="<?php echo $class ?>" data-group="origin" data-field="origin[]" data-id="<?php echo $key ?>" href="#" id="panel-origin-<?php echo $key ?>"><i class="fa <?php echo $icon ?>"></i> <?php echo $value ?></a></li>
                            <?php
								}//foreach
							?>
                            </ul>
                        </div>
                    </div>
                </div><!-- end accordion 5 -->

    <!-- accordion 6 -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        	<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Medium
                            <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php
                            $mediums = $brands->get_mediums();
                            if($mediums !== false){
                                for($i = 0; $i < sizeof($mediums); $i++){
									$icon = 'fa-plus-circle';
									$class = 'filter-add';
									if(isset($_REQUEST['medium_id']) && is_array($_REQUEST['medium_id'])){
										if(in_array($mediums[$i]['ad_medium_id'],$_REQUEST['medium_id'])){
											$icon = 'fa-minus-circle';
											$class = 'filter-add active';
										}//end if
									}//end if
                            ?>
                                <li><a class="<?php echo $class ?>" data-group="medium" data-field="medium_id[]" data-id="<?php echo $mediums[$i]['ad_medium_id'] ?>" id="panel-medium-<?php echo $mediums[$i]['ad_medium_id'] ?>" href="#"><i class="fa <?php echo $icon ?>"></i> <?php echo $mediums[$i]['ad_medium_name'] ?></a></li>
                            <?php
                                }//end for i
                            }//end if
                            ?>
                        </div>
                    </div><!-- /.panel-body -->
                </div><!-- /.accordion 6 -->
            <?php
			if($is_admin){
			?>
            	<div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        	<a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Agency Type
                            <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php
                            $company_types = $companies->get_companies_types();
                            if($company_types !== false){
                                for($i = 0; $i < sizeof($company_types); $i++){
									$icon = 'fa-plus-circle';
									$class = 'filter-add';
									if(isset($_REQUEST['company_types']) && is_array($_REQUEST['company_types'])){
										if(in_array($company_types[$i]['company_type_id'],$_REQUEST['company_types'])){
											$icon = 'fa-minus-circle';
											$class = 'filter-add active';
										}//end if
									}//end if
                            ?>
                                <li><a class="<?php echo $class ?>" data-group="company-types" data-field="company_types[]" data-id="<?php echo $company_types[$i]['company_type_id'] ?>" id="panel-company-type-<?php echo $company_types[$i]['company_type_id'] ?>" href="#"><i class="fa <?php echo $icon ?>"></i> <?php echo $company_types[$i]['company_type_label'] ?></a></li>
                            <?php
                                }//end for i
                            }//end if
                            ?>
                        </div>
                    </div><!-- /.panel-body -->
                </div><!-- /.accordion 6 -->
            <?php
			}//end
			?>
            </div><!-- /.accordion -->
