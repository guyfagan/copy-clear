<div class="helpbox wide clearfix">
	<h2>Key</h2>
    <ul class="key">
        <li class="new">New</li>
        <li class="viewed">Viewed</li>
        <li class="sm-received">Substantiation Received</li>
        <li class="not-approved">Not Approved</li>
				<li class="incomplete-submission">Incomplete Submission</li>
        <li class="not-valid">Not Valid</li>
        <li class="pending">Pending</li>
        <li class="interim">Interim Approval</li>
        <li class="final-approve">Final Approved</li>
    </ul>
</div>
