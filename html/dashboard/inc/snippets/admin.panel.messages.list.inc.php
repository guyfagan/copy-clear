<?php
	if($list !== false){
		for($i = 0; $i < sizeof($list); $i++){
			$last_edit = date("jS M g:iA",$list[$i]['message_date']);
			$title = trim($list[$i]['ad_submission_title']);

			$encdata = urlencode(base64_encode($list[$i]['ad_submission_id']));
			$link = __BASEPATH__.'admin/view/?encdata='.$encdata;
	?>
		<tr class="<?php echo $list[$i]['ad_submission_status'] ?>">
			<!-- <td>
				<span class="status <?php echo $list[$i]['ad_submission_status'] ?>"></span>
			</td> -->
			<td><?php echo $last_edit?></td>
			<td><?php echo $list[$i]['user_firstname'].' '.$list[$i]['user_surname'] ?><br /><?php echo $list[$i]['company_name']; ?></td>
			<td><?php echo '<a href="'.$link.'"><strong>'.$list[$i]['message_message'].'</strong></a>'  ?></td>
			<td class="last"><a href="<?php echo $link ?>" class="btn btn-primary btn-xs">View</a></td>
		</tr>
	<?php
		}//end for i
	}//end if
?>
