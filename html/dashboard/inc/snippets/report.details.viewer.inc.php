<?php
$fields = array();
$fields[0] = array();
$fields[0]['field_id'] = "ireland";
$fields[0]['field_name'] = "Created in Ireland";
$fields[1] = array();
$fields[1]['field_id'] = "adapted";
$fields[1]['field_name'] = "Adaptation of international materia";
$fields[2] = array();
$fields[2]['field_id'] = "elsewhere";
$fields[2]['field_name'] = "Created elsewhere";
$field_name = 'field_name';
$field_id = 'field_id';
$search_field = 'origin';

$data = array();
$data_info = array();
for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
  $data_info['year_'.$year] = array(array("Field A","Field B"));
}//end if
$year_totals = array();
for($i = 0; $i < sizeof($fields); $i++){
  $data[$i] = array();
  $options[$search_field] = $fields[$i][$field_id];
  $chart = $ads->get_chart($options);
  if($chart !== false){
    for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
      $data[$i][$year] = 0;
      for($x = 0; $x < sizeof($chart); $x++){
        if($chart[$x]['year'] == $year){
          $data[$i][$year] = $chart[$x]['found'];
          $year_totals[$year] += $chart[$x]['found'];
          $subdata = array($fields[$i][$field_name], (int)$chart[$x]['found']);
          array_push($data_info['year_'.$year],$subdata);
        }//end if
      }//end for x
    }//end for year
  } else {
    for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
      $data[$i][$year] = 0;
      $subdata = array($fields[$i][$field_name], 0);
      array_push($data_info['year_'.$year],$subdata);
    }//end if
  }//end if
}//end for i

$options = $orig_options;

$fields2 = array();
$fields2[0] = array();
$fields2[0]['field_id'] = "not-approved";
$fields2[0]['field_name'] = "Not Approved";
$fields2[1] = array();
$fields2[1]['field_id'] = "interim-approval";
$fields2[1]['field_name'] = "Interim Approval";
$fields2[2] = array();
$fields2[2]['field_id'] = "final-approval";
$fields2[2]['field_name'] = "Final Approval";
$search_field2 = 'status';

$data2 = array();
$data2_info = array();
for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
  $data2_info['year_'.$year] = array(array("Field A","Field B"));
}//end if
$year_totals2 = array();
for($i = 0; $i < sizeof($fields2); $i++){
  $data2[$i] = array();
  $options[$search_field2] = $fields2[$i][$field_id];
  $chart = $ads->get_chart($options);
  if($chart !== false){
    for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
      $data2[$i][$year] = 0;
      for($x = 0; $x < sizeof($chart); $x++){
        if($chart[$x]['year'] == $year){
          $data2[$i][$year] = $chart[$x]['found'];
          $year_totals2[$year] += $chart[$x]['found'];
          $subdata = array($fields2[$i][$field_name], (int)$chart[$x]['found']);
          array_push($data2_info['year_'.$year],$subdata);
        }//end if
      }//end for x
    }//end for year
  } else {
    for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
      $data2[$i][$year] = 0;
      $subdata = array($fields2[$i][$field_name], 0);
      array_push($data2_info['year_'.$year],$subdata);
    }//end if
  }//end if
}//end for i
?>
<ul class="nav nav-tabs">
<li class="active"><a href="#pie-charts" data-toggle="tab">Pie Charts</a></li>
<li><a href="#table-results" data-toggle="tab">Table</a></li>
</ul>
<div class="tab-content">
  <div class="tab-pane" id="table-results">
      <table class="table table-striped">
          <thead>
              <tr>
                  <th></th>
                  <?php
                  for($i = $_REQUEST['to_year']; $i >= $_REQUEST['from_year']; $i--){
                      echo '<th>YTD '.$i.'</th>';
                  }
                  ?>
                  <th>Graph</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <th colspan="<?php echo $years+2 ?>">
                      Origin
                  </th>
              </tr>
          <?php
              $log = array();
              for($i = 0; $i < sizeof($fields); $i++){

                  $label = $fields[$i][$field_name];
                  echo '<tr>';
                  echo '<td>'.$label.'</td>';
                  $linedata = array();

                  for($year = $to_year; $year >= $from_year; $year--){
                      $perc = round(($data[$i][$year]*100)/$year_totals[$year],2);
                      array_push($linedata,$data[$i][$year]);
                      $log_line = array("y" => $year,"a" => $perc);
                      array_push($log,$log_line);
                      echo  '<td><strong>'.$data[$i][$year].'</strong> <small>('.$perc.'%)</small></td>';
                  }
                  if($years > 1){
                      echo '<td><span class="line" data-width="64">'.implode(',',$linedata).'</span></td>';
                  } else {
                      echo '<td><span class="pie" data-diameter="48">'.$perc.'/100</span></span>';
                  }//end if
                  echo '</tr>';
              }//end for i
          ?>
          <tr>
              <th colspan="<?php echo $years+2 ?>">
                  Approval
              </th>
          </tr>
          <?php
              for($i = 0; $i < sizeof($fields2); $i++){

                  $label = $fields2[$i][$field_name];
                  echo '<tr>';
                  echo '<td>'.$label.'</td>';
                  $linedata = array();
                  for($year = $to_year; $year >= $from_year; $year--){
                      $perc = round(($data2[$i][$year]*100)/$year_totals2[$year],2);
                      array_push($linedata,$data2[$i][$year]);
                      echo  '<td><strong>'.$data2[$i][$year].'</strong> <small>('.$perc.'%)</small></td>';
                  }
                  if($years > 1){
                      echo '<td><span class="line" data-width="64">'.implode(',',$linedata).'</span></td>';
                  } else {
                      echo '<td><span class="pie" data-diameter="48">'.$perc.'/100</span></span>';
                  }//end if
                  echo '</tr>';
              }//end for i

          ?>
          </tbody>
      </table>
  </div>
  <div id="pie-charts" class="tab-pane active">
    <h3>Origin</h3>
      <div class="row">
  <?php
$x = 0;
      for($year = $to_year; $year >= $from_year; $year--){
  ?>
      <div id="piechart-<?php echo $year ?>-origin" class="col-md-6" style="height: 280px;"></div>
  <?php
  $x++;
  if($x%2 == 0 && $x > 0){
    echo '</div><hr /><div class="row">';
  }

      }//end for
  ?>
    </div>
    <h3>Approval</h3>
      <div class="row">
  <?php
$x = 0;
      for($year = $to_year; $year >= $from_year; $year--){
  ?>
      <div id="piechart-<?php echo $year ?>-approval" class="col-md-6" style="height: 280px;"></div>
  <?php
  $x++;
  if($x%2 == 0 && $x > 0){
    echo '</div><hr /><div class="row">';
  }
      }//end for
  ?>
    </div>
  </div>
</div>
<a href="<?php echo __BASEPATH__ ?>admin/reports/charts/?from_month=<?php echo $from_month ?>&to_month=<?php echo $to_month ?>&from_year=<?php echo $from_year ?>&to_year=<?php echo $to_year ?>&chart_type=categories" class="btn btn-primary pull-left">Go Back</a>
<script src="<?php echo __BASEPATH__ ?>js/jquery.peity.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});
</script>
<script>


$(document).ready(function(e) {
$(".line").peity("line");
$("span.pie").peity("pie");

  $("#from-month").on("change",function(){
var $from = $("#from-month").val();
if($from > $("#to-month").val()){
  $("#to-month").val($from);
}
});

$("#from-year").on("change",function(){
var $from = $("#from-year").val();
if($from > $("#to-year").val()){
  $("#to-year").val($from);
}
});

var myData = $.parseJSON('<?php echo json_encode($data_info) ?>');
var myData2 = $.parseJSON('<?php echo json_encode($data2_info) ?>');

function drawChart($myData,$year,$group) {
/*var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Work',     11],
  ['Eat',      2],
  ['Commute',  2],
  ['Watch TV', 2],
  ['Sleep',    7]
]);
*/

json = $myData['year_'+$year];

var data = google.visualization.arrayToDataTable(json);

var options = {
  title: $year
};

var chart = new google.visualization.PieChart(document.getElementById('piechart-'+$year+'-'+$group));
chart.draw(data, options);
}
<?php
for($year = $to_year; $year >= $from_year; $year--){
?>
drawChart(myData,'<?php echo $year ?>','origin');
drawChart(myData2,'<?php echo $year ?>','approval');

<?php
}//end for year
?>
});
</script>
