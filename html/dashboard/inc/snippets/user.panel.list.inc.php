<?php
	if($submissions !== false){
		for($i = 0; $i < sizeof($submissions); $i++){
			$last_edit = date("jS M Y g:iA",$submissions[$i]['create_date']);
			if($submissions[$i]['reply_date'] > 0){
				$last_edit = date("jS M Y g:iA",$submissions[$i]['reply_date']);
			}//end if
			$title = trim($submissions[$i]['ad_submission_title']);
			if($title == ""){
				if(in_array($sumissions[$i]['attachment_external_service'],array('youtube','vimeo'))){
					$title = $submissions[$i]['attachment_filename'];	
				} else {
					$title = $submissions[$i]['attachment_friendly_name'];	
				}
			}//end if
			$encdata = urlencode(base64_encode($submissions[$i]['ad_submission_id']));
			if(!$is_admin){
				$link = __BASEPATH__.'submission/view/?encdata='.$encdata;
			} else {
				$link = __BASEPATH__.'admin/view/?encdata='.$encdata;
			}//end if
	?>
		<tr class="<?php echo $submissions[$i]['ad_submission_status'] ?>">
			<td>
				<span class="status <?php echo $submissions[$i]['ad_submission_status'] ?>"></span>
			</td>
			<td><?php echo $last_edit?></td>                                  
			<td><a href="<?php echo $link ?>"><strong><?php echo $title ?></strong></a></td>
			<td><a href="<?php echo $link ?>"><?php echo $submissions[$i]['ad_medium_name'] ?></a></td>
			<td class="last"><a href="<?php echo $link ?>" class="btn btn-primary btn-xs">View</a></td>
		</tr>
	<?php		
		}//end for i
	}//end if
?>