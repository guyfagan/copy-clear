<h2>Chart</h2>
  <div class="box clearfix">
<?php
	if($do_search){
		$options = array('ignore_status' => array('pending','sm-received','not-valid'));
		$options['from_month'] = $_REQUEST['from_month'];
		$options['to_month'] = $_REQUEST['to_month'];
		$options['from_year'] = $_REQUEST['from_year'];
		$options['to_year'] = $_REQUEST['to_year'];
		$years = $options['to_year'] - $options['from_year'];
		$years++;

		switch($_REQUEST['chart_type']){
			case "categories":
			default:
				$fields = $brands->get_brands_categories();
				$field_name = 'brand_category_name';
				$field_id = 'brand_category_id';
				$search_field = 'brand_category_id';
				break;
			case "medium":
				$fields = $brands->get_mediums();
				$field_name = 'ad_medium_name';
				$field_id = 'ad_medium_id';
				$search_field = 'medium_id';
				break;
			case "company_type":
				$companies = $utils->call('companies');
				$fields = $companies->get_companies_types();
				$field_name = 'company_type_label';
				$field_id = 'company_type_id';
				$search_field = 'company_type_id';
				break;
			case "status":
				$fields = array();
				$fields[0] = array();
				$fields[0]['field_id'] = "not-approved";
				$fields[0]['field_name'] = "Not Approved";
				$fields[1] = array();
				$fields[1]['field_id'] = "interim-approval";
				$fields[1]['field_name'] = "Interim Approval";
				$fields[2] = array();
				$fields[2]['field_id'] = "final-approval";
				$fields[2]['field_name'] = "Final Approval";
				$field_name = 'field_name';
				$field_id = 'field_id';
				$search_field = 'status';
				break;
			case "origin":
				$fields = array();
				$fields[0] = array();
				$fields[0]['field_id'] = "ireland";
				$fields[0]['field_name'] = "Created in Ireland";
				$fields[1] = array();
				$fields[1]['field_id'] = "adapted";
				$fields[1]['field_name'] = "Adaptation of international material";
				$fields[2] = array();
				$fields[2]['field_id'] = "elsewhere";
				$fields[2]['field_name'] = "Created elsewhere";
				$field_name = 'field_name';
				$field_id = 'field_id';
				$search_field = 'origin';
				break;
		}//end switch

		$data = array();
		$year_totals = array();
		for($i = 0; $i < sizeof($fields); $i++){
			$data[$i] = array();
			$options[$search_field] = $fields[$i][$field_id];
			$chart = $ads->get_chart($options);
      //echo $utils->cache;
			if($chart !== false){
				for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
					$data[$i][$year] = 0;
					for($x = 0; $x < sizeof($chart); $x++){
						if($chart[$x]['year'] == $year){
							$data[$i][$year] = $chart[$x]['found'];
							$year_totals[$year] += $chart[$x]['found'];
						}//end if
					}//end for x
				}//end for year
			} else {
				for($year = $_REQUEST['to_year']; $year >= $_REQUEST['from_year']; $year--){
					$data[$i][$year] = 0;
				}//end if
			}//end if
		}//end for i

	?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th></th>
					<?php
					for($i = $_REQUEST['from_year']; $i <= $_REQUEST['to_year']; $i++){
						echo '<th>YTD '.$i.'</th>';
					}
					?>
          <th>Graph</th>
				</tr>
			</thead>
      <tfoot>
				<tr>
					<th>Total</th>
					<?php
					$linedata = array();
					for($i = $_REQUEST['from_year']; $i <= $_REQUEST['to_year']; $i++){
						echo '<th>'.$year_totals[$i].'</th>';
						array_push($linedata,$year_totals[$i]);
					}//end for i
					if($years > 1){
						echo '<td><span class="line" data-width="64">'.implode(',',$linedata).'</span></td>';
					} else {
						echo '<td></td>';
					}//end if
					?>
				</tr>
			</tfoot>
			<tbody>
			<?php

				for($i = 0; $i < sizeof($fields); $i++){

					switch($_REQUEST['chart_type']){
						case "categories":
							$label = '<a href="'.__BASEPATH__.'admin/reports/details/?view=category&category_id='.$fields[$i][$field_id].'&from_month='.$_REQUEST['from_month'].'&to_month='.$_REQUEST['to_month'].'&from_year='.$_REQUEST['from_year'].'&to_year='.$_REQUEST['to_year'].'">'.$fields[$i][$field_name].'</a>';
							break;
            case "medium":
							$label = '<a href="'.__BASEPATH__.'admin/reports/details/?view=medium&medium_id='.$fields[$i][$field_id].'&from_month='.$_REQUEST['from_month'].'&to_month='.$_REQUEST['to_month'].'&from_year='.$_REQUEST['from_year'].'&to_year='.$_REQUEST['to_year'].'">'.$fields[$i][$field_name].'</a>';
							break;
            case "company_type":
							$label = '<a href="'.__BASEPATH__.'admin/reports/details/?view=company_type&company_type_id='.$fields[$i][$field_id].'&from_month='.$_REQUEST['from_month'].'&to_month='.$_REQUEST['to_month'].'&from_year='.$_REQUEST['from_year'].'&to_year='.$_REQUEST['to_year'].'">'.$fields[$i][$field_name].'</a>';
							break;
					  default:
							$label = $fields[$i][$field_name];
					}
					echo '<tr>';
					echo '<td>'.$label.'</td>';
					$linedata = array();
					for($year = $_REQUEST['from_year']; $year <= $_REQUEST['to_year']; $year++){
						$perc = round(($data[$i][$year]*100)/$year_totals[$year],2);
						array_push($linedata,$data[$i][$year]);
						echo  '<td><strong>'.$data[$i][$year].'</strong> <small>('.$perc.'%)</small></td>';
					}
					if($years > 1){
						echo '<td><span class="line" data-width="64">'.implode(',',$linedata).'</span></td>';
					} else {
						echo '<td><span class="pie" data-diameter="48">'.$perc.'/100</span></td>';
					}//end if
					echo '</tr>';
				}//end for i

			?>
			</tbody>
		</table>
      <?php
	} else {
?>
      	<div class="alert alert-warning">Please click on search to see the results</div>
      <?php
	}//end if
?>
</div>
