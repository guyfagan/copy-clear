<?php
$brand_category = $brands->get_brand_category(array('category_id' => $category_id));
echo '<h2>Details for '.$brand_category['brand_category_name'].'</h2>';

$options = array('ignore_status' => array('pending','sm-received','not-valid'));
$options['brand_category_id'] = $category_id;
$options['from_month'] = $from_month;
$options['to_month'] = $to_month;
$options['from_year'] = $from_year;
$options['to_year'] = $to_year;

$orig_options = $options;

require_once('report.details.viewer.inc.php');
