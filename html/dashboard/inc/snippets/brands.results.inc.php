<h2>Brands</h2>
<table class="table">
  <thead>
    <tr>
      <th>Advertiser</th>
      <th>Brand</th>
      <th>Submissions</th>
    </tr>
  </thead>
  <tbody>
<?php
  $options['from_month'] = $_REQUEST['from_month'];
	$options['to_month'] = $_REQUEST['to_month'];
	$options['from_year'] = $_REQUEST['from_year'];
	$options['to_year'] = $_REQUEST['to_year'];

  $owners = $brands->get_brands_owners();
  if($owners !== false){
    for($i = 0; $i < sizeof($owners); $i++){
      $owner_name = $owners[$i]['brand_owner_name'];
      $options['brand_owner_id'] = $owners[$i]['brand_owner_id'];
      $brands_list = $brands->get_brands_report($options);
      $total_submissions = 0;
      if($brands_list !== false){
        for($x = 0; $x < sizeof($brands_list); $x++){
          $submissions = $brands_list[$x]['submissions'];
          $total_submissions += $submissions;
  ?>
    <tr>
      <th>
      <?php
        if($x == 0){
          echo $owner_name;
        } else {
          echo '&nbsp;';
        }
      ?>
      </th>
      <td><?php echo $brands_list[$x]['brand_name'] ?></td>
      <td><?php echo $submissions ?></td>
    </tr>
  <?php
        }//end for x
  ?>
    <tr>
      <th colspan="2">Total Submissions</th>
      <th><?php echo $total_submissions ?></th>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
  <?php
      }//end if

    }//end for i
  }//end if

?>
  </tbody>
</table>