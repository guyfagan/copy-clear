<div class="col-md-3 helpbox">
	<div class="clearfix" id="info-1">
        <p>Check to see if your company is already registered by typing the name into the search box.</p>
        <p>If your company is already registered it will appear it will appear in a list below the text box.</p>
        <p>Select your company from the list and click next step.</p>
        <p>If your company is not registered enter the company name and click next step.</p>
    </div>
    <div class="clearfix hide" id="info-2">
    	<p>Fill in the rest of your details and click &quot;Next Step&quot;.</p>
    </div>
</div>
<div class="col-md-9">
    <form method="post" action="#" id="register-form" name="register-form">
        <div id="company-finder">
            <div class="box" id="company-name-box">
                <p>Please enter your company name to check to see if you are already registered with us.</p>                        
                <input type="hidden" name="company_id" id="company-id">
                <label for="company_name">Company Name</label>
                <input id="company-name" class="form-control" type="text" name="company_name" placeholder="Your Company Name">                           
                <ul class="well hide" id="company-search"></ul>                                  
            </div>
            <div class="clearfix">
                <a class="btn btn-default btn-top-margin" href="index.php">Back to Log In</a>
                <a class="btn btn-primary" href="#" id="add-user-details">Next Step</a>
            </div>
        </div>
        <div id="basic-registration" class="hide">
            <div class="box" id="new-company"> 
                <div class="new-company-opt">                     
                    <label for="company-type">Company Type</label>
                    <select class="form-control" id="company-type" name="company_type">
                        <option>Select One</option>
                        <?php
                        $types = $companies->get_companies_types();
                        if($types !== false){
                            for($i = 0; $i < sizeof($types); $i++){
                                echo '<option value="'.$types[$i]['company_type_id'].'">'.$types[$i]['company_type_label'].'</option>';
                            }//end for i
                        }//end if
                        ?>
                    </select>
                </div>
                <div class="control-group">                
                    <label for="firstname">First Name</label>
                    <input id="firstname" class="form-control" type="text" name="firstname" placeholder="Your First Name" required data-label="First Name">
                </div>
                <div class="control-group">
                    <label for="lastname">Last Name</label>
                    <input id="lastname" class="form-control" type="text" name="lastname" placeholder="Your Last Name" required data-label="Last Name">   
                </div>
                <div class="control-group"> 
                    <label for="username">Email</label>
                    <input id="email" class="form-control" type="text" name="email" placeholder="Your Email Address" required data-label="Email">
                </div>
                <div class="control-group">
                    <label for="telephone">Telephone</label>
                    <input id="telephone" class="form-control" type="tel" name="meta_telephone" placeholder="Your Telephone Number" required data-label="Landline Phone Number">
                </div>
                <div class="control-group">
                    <label for="mobile">Mobile</label>
                    <input id="mobile" class="form-control" type="tel" name="meta_mobile" placeholder="Your Mobile Number" required data-label="Mobile Number">
                </div>
                <div class="control-group">
                    <label for="password">Password</label>
                    <input id="password" class="form-control" type="password" name="password" placeholder="Enter Password" required data-label="Password">
                </div>
                <div class="control-group">
                    <label for="password">Confirm Password</label>
                    <input id="conf-password" class="form-control" type="password" name="conf_password" placeholder="Re-Enter Password" required data-label="Confirm Password">                   
                </div>               
            </div>
            <div class="clearfix">
                <a class="btn btn-default btn-top-margin" href="index.php">Back to Log In</a>
                <a class="btn btn-primary" href="#" id="save-user-btn">Next Step</a>
            </div>
        </div>
    </form>
</div>                 
