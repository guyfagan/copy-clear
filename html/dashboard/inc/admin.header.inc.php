<header>	
    <div class="container">
        <div class="row clearfix ">
                <a href="<?php echo __BASEPATH__ ?>admin/"><h1>Copyclear</h1></a>
        </div>
    </div>
</header>
<?php
	if(isset($user) && $user !== false){
?>
<nav>
    <div class="container">
        <div class="row clearfix ">
            <ul class="navbar col-md-9">
                <li><a href="<?php echo __BASEPATH__ ?>admin/"<?php if($section == 'home'){ echo ' class="active"'; } ?>>Dashboard</a></li>
                <li><a href="<?php echo __BASEPATH__ ?>admin/campaigns/"<?php if($section == 'campaigns' || $permalink == 'campaigns.all'){ echo ' class="active"'; } ?>>Campaigns</a></li>
                <li><a href="<?php echo __BASEPATH__ ?>admin/archive/"<?php if($section == 'archive'){ echo ' class="active"'; } ?>>Archive</a></li>
                <li><a href="<?php echo __BASEPATH__ ?>admin/users/"<?php if($section == 'users'){ echo ' class="active"'; } ?>>Users</a></li>
                <li><a href="<?php echo __BASEPATH__ ?>admin/companies/"<?php if($section == 'companies'){ echo ' class="active"'; } ?>>Companies</a></li>
                <li>
                	<a href="<?php echo __BASEPATH__ ?>admin/reports/"<?php if($section == 'reports'){ echo ' class="active"'; } ?>>Reports</a>                	
                </li>
            </ul>  
            <div class="col-md-3">
                <p class="username pull-right">Signed in as <strong><?php echo $user['user_username'] ?></strong> <span class="navgrey">|</span><!-- <a href="<?php echo __BASEPATH__ ?>profile/">Your Profile</a> <span class="navgrey">|</span>--> <a href="<?php echo __BASEPATH__ ?>api/logout">Logout</a></p>    
            </div>
        </div>
    </div>
</nav>
<?php
	}//end if
?>