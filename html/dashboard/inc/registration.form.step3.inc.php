<?php 
	$brands_list = $brands->get_brands_categories();
?>
<div class="col-md-3 helpbox">
    <p>Type the name of the brand and click the plus (+) symbol.</p>
	<p>Brands will appear in the list on the right.</p>
	<p>You will need to fill in the billing contact details for each new brand you add.</p>
	<p>Repeat these steps until all your brands have been added.</p>
</div>
<div class="col-md-9">
    <div class="box">
        <div class="row">
            <div class="col-md-8">
                <form method="post" name="brand-registration" id="brand-registration" action="#">
                    <label>Enter Name of Brand</label>
                    <div class="form-group clearfix row">
                    	<div class="col-sm-11">
                    		<input id="brand-name" class="form-control brand_box" type="text" name="brand_name" placeholder="Your Brands">
                        </div>
                        <div class="col-sm-1">
                            <span class="pull-right">
                    			<a href="#" class="btn btn-add btn-xs" id="add-brand"><i class="fa fa-plus-circle"></i> Add</a>
                            </span>
                        </div>
                    </div>
                </form>   
                <div id="no-results-message" class="hide">                    
                    <div class="well clearfix">
                        <p id="hb-message"></p>
                        <hr>
                        <h3>Billing Contact for Brand</h3>
                        <p>Costs will be attributed to the client company.  Please provide details of the billing contact.</p>
                        <form method="post" name="new-brand-form" id="new-brand-form" action="#">
                            <label>Billing Company Name</label>
                            <input id="company-brand-name" class="form-control" type="text" name="company_brand_name" placeholder="Billing Company's Name">
                            <label>Billing Company Email</label>
                            <input id="company-brand-email" class="form-control" type="text" name="company_brand_email" placeholder="Billing Company's Email Address">
                            <label>Category</label>
                            <select name="company_brand_category_id" id="company-brand-category-id" class="form-control">
                            <?php
                                
                                if($brands_list !== false){
                                    for($i = 0; $i < sizeof($brands_list); $i++){
                                        echo '<option value="'.$brands_list[$i]['brand_category_id'].'">'.$brands_list[$i]['brand_category_name'].'</option>';
                                    }//end for i
                                }//end if
                            ?>
                            </select>
                            <label>Brand Owner</label>
                            <select name="company_brand_advertiser" id="company-brand-advetiser" class="form-control">
                            <?php
								$advertisers_list = $brands->get_brands_owners();
								if($advertisers_list !== false){
									for($i = 0; $i < sizeof($advertisers_list); $i++){
                                        echo '<option value="'.$advertisers_list[$i]['brand_owner_id'].'">'.$advertisers_list[$i]['brand_owner_name'].'</option>';
                                    }//end for i
								}//end if
							?>
                            </select>
                            <button type="button" id="save-brand-btn" class="btn btn-primary">Add this brand</button>
                        </form>
                     </div>
                </div>             
            </div>    
            <div class="col-md-4 list">
            	<form method="post" name="saved-brands" id="saved-brands" action="#">
            	<p><strong>Your Brands</strong></p>
                <ul id="brands-list"></ul>
                </form>
            </div>
        </div>
        <!-- end row -->
     </div>
    <!-- end box -->
    <a class="btn btn-default" href="#" data-action="back" data-step="2">Back to Previous Step</a>
    <a class="btn btn-primary" href="#" id="save-brands-btn">Complete Registration</a>
</div>			
<script id="no-results-message-template" type="text/x-handlebars-template">
	The brand “{{query}}” is not currently in the system. Fill in the details below to add a new brand.
</script>