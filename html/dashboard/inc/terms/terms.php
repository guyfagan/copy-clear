<h2>COPYCLEAR</h2>
<hr />
<h3>TERMS AND CONDITIONS FOR THE SUPPLY OF SERVICES</h3>
<hr />
<h4>INTRODUCTION</h4>
<p>These Conditions set out the terms on which CopyClear agree to provide the Services to the User. By making a Submission to CopyClear, the User accepts these Conditions in full. If you disagree with these Conditions, please contact CopyClear before making a Submission.</p>

<h4>INTERPRETATION</h4>

<p>Definitions: In these Conditions, the following definitions apply:</p>
<p>In these Conditions, the following rules apply:</p>
<ul class="text-list">
  <li>a person includes a natural person, corporate or unincorporated body (whether or not having separate legal personality);</li>
  <li>a reference to a party includes its employees, agents, directors, successors or permitted assigns;</li>
  <li>a reference to a statute, statutory provision, regulation, guideline or code of practice is a reference to such statute, statutory provision, regulation, guideline or code of practice as amended or re-enacted. A reference to a statute or statutory provision includes any subordinate legislation made under that statute or statutory provision, as amended or re-enacted;</li>
  <li>any phrase introduced by the terms including, include, in particular or any similar expression, shall be construed as illustrative and shall not limit the sense of the words preceding those terms; and</li>
  <li>a reference to writing or written includes faxes and e-mails.</li>
</ul>
<p>&nbsp;</p>
<h4>SUPPLY OF SERVICES</h4>

<p>CopyClear shall supply the Services to the User. In providing the Services, CopyClear shall review the Submission based on CopyClear’s understanding and interpretation of the Codes of Practice and understanding of assessing marketing communications and, to the extent CopyClear consider it appropriate to do so (at all times acting in its absolute discretion), provide Users with actionable advice where a Submission fails to meet the requirements of the Codes of Practice or is not in keeping with the spirit of the Codes of Practice.</p>
<p>To the extent that CopyClear are satisfied that the Submission complies in full with the Codes of Practice, CopyClear shall furnish the User with a Clearance Number in respect of that Submission. Once provided, that Clearance Number shall be valid for a period of 12 months from the date of issue, after which date the User shall be required to re-submit the Submission to CopyClear if that User continues to require a Clearance Number in respect of that Submission.</p>
<p>CopyClear shall have the right to make any changes to the Services which are necessary to comply with any applicable law or safety requirement, or which do not materially affect the nature or quality of the Services.</p>
<p>The User acknowledges and accepts that CopyClear shall be under no obligation to provide the Services in respect of any specific Submission and that CopyClear shall at all times have a discretion as to review any Submission submitted by any User.</p>
<p>Copyclear CopyClear shall not be liable to the User as a result of any delay or failure to perform the Services as a result of any circumstances beyond its control.</p>
<p>In the event of CopyClear deciding not to issue a Clearance Number in respect of any Submission, the User shall be entitled to appeal that decision, within a reasonable time, to the Executive of CopyClear in which case two members of the CopyClear Executive shall review the Submission in accordance with their understanding of the Codes of Practice and shall decide whether or not a Clearance Number should be issued in respect of that Submission. Any decision of the Executive of CopyClear shall be final and binding on all parties.</p>
<p>&nbsp;</p>
<h4>USER’S OBLIGATIONS AND UNDERTAKINGS</h4>
<p>The User shall:</p>
<ul class="text-list">
  <li>ensure that the Submission and any information it provides in relation to the Submission are complete and accurate;</li>
  <li>co-operate with CopyClear in all matters relating to the Services;</li>
  <li>provide CopyClear with such additional information and materials as CopyClear may reasonably require in order to supply the Services, and ensure that such information is accurate in all material respects; and</li>
  <li>ensure that all information and materials which are submitted to CopyClear are in a format which is acceptable to CopyClear and, where submitted in an electronic format, are free from all defects, viruses and bugs.</li>
</ul>
<p>The User hereby undertakes that it shall at all times submit to CopyClear all future Relevant Material for review and consideration by CopyClear. The User further undertakes that it shall not publish any Relevant Material in advance of CopyClear issuing a Clearance Number in respect of that specific material.</p>
<p>If CopyClear’s performance of the Services is prevented or delayed by any act or omission by the User or failure by the User to perform any relevant obligation (a “User Default”):</p>
<ul class="text-list">
  <li>CopyClear shall, without limiting its other rights or remedies, have the right to suspend performance of the Services until the User remedies the User Default, and to rely on the User Default to relieve it from the performance of any of its obligations to the extent the User Default prevents or delays CopyClear's performance of any of its obligations;</li>
  <li>CopyClear shall not be liable for any costs or losses sustained or incurred by the User arising directly or indirectly from CopyClear's failure or delay to perform any of its obligations as set out in this clause; and</li>
  <li>the User shall reimburse CopyClear on written demand for any costs or losses sustained or incurred by CopyClear arising directly or indirectly from the User Default.</li>
</ul>
<p>&nbsp;</p>
<h4>CHARGES AND PAYMENT</h4>
<p>The User shall pay all amounts due to CopyClear in respect of the performance of the Services in full without any set-off, counterclaim, deduction or withholding (except for any deduction or withholding required by law). CopyClear may at any time, without limiting its other rights or remedies, set off any amount owing to it by the User against any amount payable by CopyClear to the User.</p>
<p>&nbsp;</p>
<h4>CONFIDENTIALITY</h4>
<p>CopyClear shall keep in strict confidence all technical or commercial know-how, specifications, inventions, processes, designs, advertisements or initiatives which are of a confidential nature and have been disclosed to CopyClear by the User, its employees, agents or subcontractors, and any other confidential information concerning the User’s business, brands, products and services which CopyClear may obtain. CopyClear shall only disclose such confidential information to those of its employees, agents and subcontractors who need to know it for the purpose of discharging CopyClear’s obligations hereunder. CopyClear may also disclose such of the User’s confidential information as is required to be disclosed by law, any governmental or regulatory authority or by a court of competent jurisdiction.</p>
<p>&nbsp;</p>
<h4>INTELLECTUAL PROPERTY</h4>
<p>The User shall at all times retail full ownership of all Intellectual Property Rights associated with the Submission.</p>
<p>&nbsp;</p>
<h4>NO LIABILITY AND INDEMNITY</h4>
<p>Save in the case of gross negligence on the part of CopyClear, CopyClear (including, for the avoidance of doubt, all officers, managers, representatives, agents, employees and members of the executive of CopyClear)  shall under no circumstances whatsoever be liable to the User, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, for any loss of profit, or any indirect or consequential loss arising out of or in connection with the provision of the Services.</p>
<p>The User irrevocably acknowledges and accepts that all warranties, conditions, guarantees, representations and undertakings, express or implied, in tort or in equity, contractual, statutory or otherwise relating to the provision of the Services by CopyClear are excluded to the fullest extent possible.</p>
<p>The User shall indemnify and keep indemnified CopyClear against all losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by CopyClear to a third party in settlement of a claim or dispute on the advice of CopyClear’s legal advisers) which may be incurred or suffered by CopyClear and relating to the Submission, including, without limitation, any such losses, damages, costs, liabilities and expenses incurred as a result of any claim by any person or entity that the Submission was not suitable or appropriate for any reason whatsoever or that the Submission was in breach of any statute, regulation, directive and/or rules or regulations of any regulatory, professional and/or industry body or bodies.</p>
<p>&nbsp;</p>
<h4>BREACHES OF THESE CONDITIONS</h4>
<p>In the event of a breach by the User of its obligations and undertakings under these Conditions, CopyClear may take such action as CopyClear deems appropriate to deal with the breach, including suspending the User’s access to the Services.</p>
<p>&nbsp;</p>
<h4>VARIATION</h4>
<p>CopyClear may revise these Conditions from time-to-time.  Revised Conditions will apply as and from the date of the publication of the revised Conditions on CopyClear’s website.  The User agrees to check CopyClear’s website regularly to ensure it is familiar with the current version.</p>
<p>&nbsp;</p>
<h4>NO PARTNERSHIP</h4>
<p>Nothing contained herein is intended to, or shall be deemed to, establish any partnership or joint venture between CopyClear and the User, nor constitute either party the agent of the other for any purpose. Neither party shall have authority to act as agent for, or to bind, the other party in any way.</p>
<p>&nbsp;</p>
<h4>GENERAL</h4>
<p>In the event that any provision in these Conditions is held to be unenforceable or invalid, such provision shall be severed and the remaining provisions shall be enforceable to the fullest extent permitted by the laws Ireland.</p>
<p>These Conditions and all issues and disputes relating to these Conditions are governing by Irish law and are subject to the exclusive jurisdiction of the Irish courts.</p>
<p>We have reviewed, considered and understand all of the provisions of the above Terms and Conditions for the Supply of Services. We accept and agree to be bound by the said Terms and Conditions as and from the date of this acceptance insofar as they relate to the relationship between CopyClear and us as User.</p>
<p>We accept that the said Terms and Conditions are integral to the provision of services to us by CopyClear and undertake to comply at all times with all such terms and conditions.</p>