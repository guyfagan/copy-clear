<?php 
	$brands_list = $brands->get_brands_categories();
?>
<div class="col-md-3 helpbox">
    <p>Type the name of the brand owner and click the plus (+) symbol.</p>
	<p>Brand Owners will appear in the list on the right.</p>
	<p>Repeat these steps until all your brand owners have been added.</p>
</div>
<div class="col-md-9">
    <div class="box">
        <div class="row">
        	<form method="post" name="advertiser-registration" id="advertiser-registration" action="#">
                <div class="col-md-8">                  	                  
                    <label>Enter Name of Brand Owner</label>
                    <div class="form-group clearfix row">
                    	<div class="col-sm-11">
                            <input id="brand-owner-name" class="form-control brand_box" type="text" name="brand_owner_name" placeholder="Your Brand Owner">
                        </div>
                        <div class="col-sm-1">
                            <span class="pull-right">
                                <a href="#" class="btn btn-add btn-xs" id="add-advertiser"><i class="fa fa-plus-circle"></i> Add</a>        
                            </span>
                        </div>
                    </div>            
                </div>    
                <div class="col-md-4 list">
                <p><strong>Your Brand Owners</strong></p>
                    <ul id="advertiser-list"></ul>
                </div>
            </form> 
        </div>
        <!-- end row -->
     </div>
    <!-- end box -->
    <a class="btn btn-default" href="#" data-action="back" data-step="1">Back to Previous Step</a>
    <a class="btn btn-primary" href="#" id="save-advertisers-btn">Next Step</a>
</div>