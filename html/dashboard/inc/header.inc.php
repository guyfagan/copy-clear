<header>	
    <div class="container">
        <div class="row clearfix ">
                <a href="<?php echo __BASEPATH__ ?>home/"><h1>Copyclear</h1></a>
        </div>
    </div>
</header>
<?php
	if(isset($user) && $user !== false){
?>
<nav>
    <div class="container">
        <div class="row clearfix ">
            <ul class="navbar col-md-8">
                <li><a href="<?php echo __BASEPATH__ ?>home/"<?php if($section == 'home'){ echo ' class="active"'; } ?>>Dashboard</a></li>
                <li><a href="<?php echo __BASEPATH__ ?>campaigns/"<?php if($section == 'campaigns' || $permalink == 'campaigns.all'){ echo ' class="active"'; } ?>>Campaigns</a></li>
                <li><a href="<?php echo __BASEPATH__ ?>archive/"<?php if($section == 'archive'){ echo ' class="active"'; } ?>>Archive</a></li>
                <li><a href="http://copyclear.ie/downloads/Guidance_Notes_25_March_2014.pdf" target="_blank">Guidance Notes</a></li>  
            </ul>  
            <div class="col-md-4">
                <p class="username pull-right">Signed in as <strong><?php echo $user['user_username'] ?></strong> <span class="navgrey">|</span> <!--<a href="<?php echo __BASEPATH__ ?>profile/">Your Profile</a> <span class="navgrey">|</span>--> <a href="<?php echo __BASEPATH__ ?>api/logout">Logout</a></p>    
            </div>
        </div>
    </div>
</nav>
<?php
	}//end if
?>