<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CopyClear</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Old Hat - http://oldhat.ie">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <link href="<?php echo __BASEPATH__ ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="<?php echo __BASEPATH__ ?>css/screen.css?v=<?php echo __VERSION__ ?>" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>css/fluid.css?v=<?php echo __VERSION__ ?>" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>js/jqueryUI/1.10.4/css/ui-lightness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>js/tokenfield/css/bootstrap-tokenfield.min.css" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>js/tokenfield/css/tokenfield-typeahead.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>js/jquery.plupload.queue/css/jquery.plupload.queue.css" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>js/editable/css/bootstrap-editable.css" rel="stylesheet">
    <link href="<?php echo __BASEPATH__ ?>css/datepicker3.css?v=<?php echo __VERSION__ ?>" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo __BASEPATH__ ?>js/html5shiv.js"></script>
    <![endif]-->
	<script src="<?php echo __BASEPATH__ ?>js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/bootstrap.min.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/jqueryUI/1.10.4/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="<?php echo __BASEPATH__ ?>inc/config.js.inc.php?v=<?php echo __VERSION__ ?>"></script>
    <script src="<?php echo __BASEPATH__ ?>js/plupload.full.min.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/jquery.jsonp.2.4.0.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/jloader.jquery.js?v=<?php echo __VERSION__ ?>"></script>
    <script src="<?php echo __BASEPATH__ ?>js/tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/editable/js/bootstrap-editable.min.js"></script>
    <script src="<?php echo __BASEPATH__ ?>js/copyclear.js?v=<?php echo __VERSION__ ?>"></script>    