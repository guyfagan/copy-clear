<?php 
	require_once("inc/config.inc.php");
	require_once('inc/init.cms.inc.php');
	require_once('inc/meta.public.inc.php');
	
	$registrations = $utils->call('registrations');
	$encdata = $_GET['encdata'];
	//decrypt encdata
	$encdata = unserialize(base64_decode(urldecode($encdata)));
	//check if the activation code is valid
	$valid = $registrations->check_activation_code(array('email' => $encdata['email'], 'code' => $encdata['code']));
	if($valid){
		$user_id = $registrations->uid;
		$encdata['user_id'] = $user_id;
	}//end if
	//re-encrypt encdata
	$encdata =  urlencode(base64_encode(serialize($encdata)));
?>
  </head>
	<body>
	<?php
	require_once('inc/header.public.inc.php');
	?>	
    <article class="main-content">
		<div class="container">
           	<h1>Reset Your Password</h1>            
            <section class="row" id="step1">
				<div class="col-md-3 helpbox">
                    <p>Haven't received an email?</p>
                    <p> Try checking your spam folder.</p>                        
                </div>
                <div class="col-md-9">
                	<?php
						if(!$valid){
					?>
                    <div class="alert alert-warning"><strong>Warning!</strong> The activation code passed is not valid.</div>
                    <?php		
						}//end if
					?>
                    <div class="box clearfix">
                		<form action="#" method="post" id="reset-password-form" name="reset-password-form" class="disabled">
                    	<?php
						if($valid){
						?>
                        	<input type="hidden" name="encdata" id="encdata" value="<?php echo $encdata ?>">
                        <?php		
						}//end if
						?>
                            <fieldset<?php
                            if(!$valid){
                                echo ' disabled';	
                            }//end if
                            ?> id="validated-form">
                            
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Type your new password</label>
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Your password">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Re-type your new password</label>
                                        <input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Your password">
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary pull-right" id="reset-password-btn">Reset your password</button>                            
                            </fieldset>
                        </form>
                    </div>
                </div>
            </section>
            <section class="row hide" id="step2">
            	<div class="col-md-3 helpbox">
                    <p>Haven't received an email?</p>
                    <p> Try checking your spam folder.</p>                        
                </div>
                <div class="col-md-9">
                    <div class="box">
                        <h3>Thanks for registering with CopyClear.</h3>
                    	<p>The CopyClear team will review your registration shortly and you will receive an email informing you when your account has been activated.</p>
						<p>Once activated and you can log in to your dashboard and begin submitting work for approval.</p>
                     </div>
                </div>
            </section>
		</div>
	</article>	
</body>
</html>