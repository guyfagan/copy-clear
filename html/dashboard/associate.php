<?php 
	require_once("inc/config.inc.php");
	require_once('inc/init.cms.inc.php');
	require_once('inc/meta.public.inc.php');
	
	$users = $utils->call('users');
	$user_data = $users->get_user();
	if($user_data === false){
		header("location: ".__BASEPATH__);	
	}//end if
	$company_id = $user_data['user_company_id'];	
	$user_id = $user_data['user_id'];
	
	$companies = $utils->call('companies');
	$company_data = $companies->get_company(array('id' => $company_id));
	
	$brands = $utils->call('brands');
	
	$encdata = array('user_id' => $user_id,'company_id' => $company_id);
	$encdata = urlencode(base64_encode(serialize($encdata)));
	
?>
</head>
<body>
	<?php
	require_once('inc/header.public.inc.php');
	?>	
    <article class="main-content">
		<div class="container">
           	<h1>Identify your current brands</h1>
            <div class="row">         
                <div class="col-md-9">
                	<h4>Welcome to the New CopyClear Site</h4>
                    <p>As you are logging in for the first time we'd like you take a minute to identify your company's current brands. Please tick your active brands from the list below and click next step.</p>
					<p>You will only need to do this once. The next time you log in you will be taken straight to your dashboard.</p>
                </div>
            </div>
            <section class="row">
            	<div class="boxes col-md-9">
                	<form action="#" id="associate-brands" name="associate-brands" method="post">
                    	<input type="hidden" name="encdata" id="encdata" value="<?php echo $encdata ?>">
            <?php
				$brands_owners = $brands->get_brands_owners(array('show_legacy' => true,'company_id' => $company_id));
				if($brands_owners){
					for($i = 0; $i < sizeof($brands_owners); $i++){
						$brands_list = $brands->get_brands(array('show_legacy' => true,'company_id' => $company_id, 'brand_owner_id' => $brands_owners[$i]['brand_owner_id']));						
						if($brands_list !== false){														
							?>
                            <div class="box clearfix">
                                <label class="checkbox">
                                    <h3><input type="checkbox" data-ref="brands-group" value="<?php echo $brands_owners[$i]['brand_owner_rel_id'] ?>" id="group-<?php echo $brands_owners[$i]['brand_owner_id'] ?>" name="brand_owner[]"> <?php echo $brands_owners[$i]['brand_owner_name'] ?></h3>
                                </label>
                            <?php       
							for($x = 0; $x < sizeof($brands_list); $x++){
							?>
                                <label class="checkbox">
                                    <input type="checkbox" value="<?php echo $brands_list[$x]['brand_rel_id'] ?>" id="brand-rel-<?php echo $brands_list[$x]['brand_rel_id'] ?>" name="brand[]" data-type="brand" data-target-group="#group-<?php echo $brands_owners[$i]['brand_owner_id'] ?>"> <?php echo $brands_list[$x]['brand_name'] ?>
                                </label>
                            <?php	
							}//end for x
							?>
                            </div>
                            <hr />
                            <?php
						}//end if
					}//end for i
				}//end if
			?>
           				<button type="button" class="btn btn-primary pull-right" id="save-associated-brands-btn">Next Step</button>
            		</form>
            	</div>
            </section>           
		</div>
	</article>	
</body>
</html>