<?php
	require_once("inc/config.inc.php");
	require_once('inc/init.cms.inc.php');
	require_once('inc/meta.public.inc.php');

	$users = $utils->call('users');
	$user_data = $users->get_user();
	if($user_data === false){
		header("location: ".__BASEPATH__);
	}//end if
	$company_id = $user_data['user_company_id'];
	$user_id = $user_data['user_id'];

	$companies = $utils->call('companies');
	$company_data = $companies->get_company(array('id' => $company_id));

	$brands = $utils->call('brands');

	$encdata = array('user_id' => $user_id,'company_id' => $company_id);
	$encdata = urlencode(base64_encode(serialize($encdata)));

?>
</head>
<body>
	<?php
	require_once('inc/header.public.inc.php');
	?>
    <article class="main-content">
		<div class="container">
     	<h1>New Terms &amp; Conditions</h1>
      <div class="row">
        <div class="col-md-12">
        	<h4>Please read carefully</h4>
          <div class="new-terms-box">
            <?php
              include 'inc/terms/terms.php';
            ?>
          </div>
        </div>
      </div>
      <section class="row">
      	<div class="boxes col-md-12">
          <form action="#" id="associate-brands" name="associate-brands" method="post">
            <input type="hidden" name="encdata" id="encdata" value="<?php echo $encdata ?>">
            <a href="<?php echo __BASEPATH__ ?>downloads/terms_and_conditions.pdf" target="_blank" class="btn btn-default btn-top-margin pull-left">Download the Terms &amp; Conditions</a>
     				<button type="button" class="btn btn-primary pull-right" id="accept-terms-btn">I accept the new Terms &amp; Conditions</button>
      		</form>
      	</div>
      </section>
		</div>
	</article>
</body>
</html>