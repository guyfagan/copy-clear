<?php
	ob_start('ob_gzhandler');
	session_start();
	ini_set("display_errors",1);
	include("../inc/config.inc.php");
	include("../../admin/config.inc.php");
	$curpath = "../../admin/";
	include("../../admin/classes/utils.class.php");

	//Users Class
	$users = $utils->call("users");
	//Logger Class
	$logger = $utils->call("logger");

	//Params
	$method = $_REQUEST['m'];

	//check in advance if whoever is calling the api is already blacklisted
	if($logger->is_blacklisted($_SERVER['REMOTE_ADDR'])){
		$logger->log_event(array("login_area" => "public","type" => "blacklisted.logged","module_id" => "applications", "message" => "Goodbye Spammer"));
		header('location: '.__BASEPATH__);
		die();
	}//end if

	if($method != NULL){

		$logger->log_event(array("login_area" => "public","type" => "api.public.method.".$method));

		$secured = $users->simple_check_secure_id();
		$user_id = $users->get_uid();
		$logged = false;
		if($user_id !== false){
			$logged = true;
		}//end if

		####################################################
		# PRIVATE CALLS ACCESSIBLE ONLY TO LOGGED IN USERS #
		####################################################
		if($secured && $logged){
			$user_data = $users->get_user();

			if($method == "get.advertiser.brands"){
				$brands = $utils->call('brands');
				$data = array();
				$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
				$user_id = $encdata['user_id'];
				$company_id = $encdata['company_id'];
				$brand_owner_id = $_POST['advertiser'];
				$options = array('brand_owner_id' => $brand_owner_id,'company_id' => $company_id);
				$list = $brands->get_brands($options);
				if($list !== false){
					$data['result'] = 'success';
					$data['list'] = $list;
				} else {
					$data['result'] = 'empty';
				}//end if

				echo json_encode($data);
			}//end if

			if($method == "get.brand.campaigns"){
				$campaigns = $utils->call('campaigns');
				$data = array();
				$brand_owner = $_POST['advertiser'];
				$brand = $_POST['brand'];
				$encdata = $_POST['encdata'];
				$options = array('advertiser_id' => $brand_owner,'brand_id' => $brand,'encdata' => $encdata);
				$list = $campaigns->get_campaigns($options);
				if($list !== false){
					$data['result'] = 'success';
					$data['list'] = $list;
				} else {
					$data['result'] = 'empty';
				}//end if

				echo json_encode($data);
			}//end if

			if($method == "private.save.brand.owner"){
				$data = array();
				$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
				$user_id = $encdata['user_id'];
				$company_id = $encdata['company_id'];
				$name = ucwords(strtolower($_POST['name']));
				$brands = $utils->call('brands');
				$brand_options = array('active' => '1','user_id' => $user_id, 'company_id' => $company_id, 'name' => $name);
				$result = $brands->save_brand_owner($brand_options);
				if($result){
					$data['result'] = 'success';
					$data['id'] = $brands->owner_id;
					$data['name'] = $name;
					//we also save the relation between the current user and the brand owner
					$brand_options = array('owner_id' => $brands->owner_id,'user_id' => $user_id, 'company_id' => $company_id,'active' => 1);
					$brands->save_brand_owner_rel($brand_options);
				} else {
					$data['result'] = 'error';
					$data['error'] = 'Cannot save the advertiser';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "private.save.brand"){
				$data = array();
				$brands = $utils->call('brands');
				$name = $_POST['nb_name'];
				$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
				$user_id = $encdata['user_id'];
				$company_id = $encdata['company_id'];
				$billing_name = $_POST['nb_company_brand_name'];
				$billing_email = $_POST['nb_company_brand_email'];
				$category_id = $_POST['nb_company_brand_category_id'];
				$owner_id = $_POST['brand_owner_id'];
				$options = array('temp' => "0", 'active' => 1,'name' => $name,'user_id' => $user_id, 'company_id' => $company_id,'billing_name' => $billing_name, 'billing_email' => $billing_email, 'category_id' => $category_id, 'owner_id' => $owner_id);
				$result = $brands->save_brand($options);
				if($result){
					$data['result'] = 'success';
					$data['id'] = $brands->id;
					$data['name'] = ucwords($name);
					$options = array('brand_id' => $brands->id, 'user_id' => $user_id, 'company_id' => $company_id);
					$brands->save_brand_rel($options);
				} else {
					$data['result'] = 'error';
					$data['errors'] = array('Cannot save the brand');
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "save.campaign"){
				$data = array();
				$campaigns = $utils->call('campaigns');
				$name = $_POST['nc_name'];
				$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
				$user_id = $encdata['user_id'];
				$company_id = $encdata['company_id'];
				$brand_id = $_POST['brand_id'];
				$options = array('temp' => "0", 'status' => 'active','name' => $name,'user_id' => $user_id, 'company_id' => $company_id,'brand_id' => $brand_id);
				$result = $campaigns->save_campaign($options);
				if($result){
					$data['result'] = 'success';
					$data['id'] = $campaigns->id;
					$data['name'] = ucwords($name);
				} else {
					$data['result'] = 'error';
					$data['errors'] = array('Cannot save the brand');
				}//end if
				echo json_encode($data);
			}//end if

			if($method == 'get.campaign.ads'){
				$ads = $utils->call('ads');
				$data = array();
				$brand_owner = $_POST['advertiser'];
				$campaign = $_POST['campaign'];
				$brand = $_POST['brand'];
				$encdata = $_POST['encdata'];
				$options = array('advertiser_id' => $brand_owner,'brand_id' => $brand, 'campaign_id' => $campaign,'encdata' => $encdata);
				$list = $ads->get_ads($options);
				$data['query'] = $utils->cache;
				if($list !== false){
					$data['result'] = 'success';
					$data['list'] = $list;
				} else {
					$data['result'] = 'empty';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "save.ad"){
				$data = array();
				$ads = $utils->call('ads');
				$name = ucwords(strtolower($_POST['na_name']));
				$medium_id = $_POST['na_medium'];
				$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
				$user_id = $encdata['user_id'];
				$company_id = $encdata['company_id'];
				$brand_id = $_POST['brand_id'];
				$brand_owner_id = $_POST['brand_owner_id'];
				$campaign_id = $_POST['campaign_id'];
				$options = array(
					'temp' => "0",
					'status' => 'active',
					'title' => $name,
					'user_id' => $user_id,
					'company_id' => $company_id,
					'brand_id' => $brand_id,
					'brand_owner_id' => $brand_owner_id,
					'campaign_id' => $campaign_id,
					'medium_id' => $medium_id
				);
				$result = $ads->save_ad($options);
				if($result){
					$data['result'] = 'success';
					$data['id'] = $ads->id;
					$data['name'] = $name;
				} else {
					$data['result'] = 'error';
					$data['errors'] = array('Cannot save the brand');
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "save.submission"){
				$data = array();
				$ads = $utils->call('ads');
				$result = $ads->save_submission();
				if($result){
					$data['result'] = 'success';
					$data['encdata'] = $ads->get_encdata();
				} else {
					$data['result'] = 'error';
					$data['errors'] = array('Cannot save the submission');
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "delete.submission"){
				$data = array();
				$ads = $utils->call('ads');
				$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
				$id = $encdata['id'];
				$group_id = $encdata['group_id'];
				$result = $ads->delete_submission(array('id' => $id,'group_id' => $group_id));
				if($result){
					$data['result'] = 'success';
				} else {
					$data['result'] = 'error';
					$data['errors'] = array('Cannot delete the submission');
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "save.url"){
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$submission_id = $encdata['submission_id'];
				$data = array();

				$url = filter_var($_POST['url'], FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
				$params = array();
				$params['url'] = $url;
				$params["post_id"] = $submission_id;
				$params["module"] = 'ads';
				$params["attachment_post_type"] = 'submission';
				$result = $utils->attach_url($params);
				if($result !== false){
					$file_id = $utils->get_attachment_id();
					$file_data = $utils->get_attachment(array('file_id' => $file_id));
					$data['result']='success';
					$data['file'] = urlencode(base64_encode($file_id));
					//$data['encdata'] = urlencode(base64_encode(serialize(array('file_id' => $file_id,'entry_id' => $entry_id))));
					$data['uri'] = $_POST['url'];
					$data['code'] = $file_id;
					$data['mime'] = $file_data['attachment_mime'];
					if($file_data['attachment_external_service'] == "amazonS3"){
						$data['friendly_name'] = $file_data['attachment_friendly_name'];
					} else {
						$data['friendly_name'] = $file_data['attachment_filename'];
					}
					$data['type'] = 'url';
					$data['thumb'] = $file_data['attachment_external_thumb'];
				} else {
					$data['result'] = 'error';
					$data['error'] = 'Cannot save the file';
				}//end if

				echo json_encode($data);
			}//end if

			if($method == "reorganise.files"){
				$ads = $utils->call('ads');
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$submission_id = $encdata['submission_id'];
				$submission_group_id = $encdata['submission_group_id'];
				$result = $ads->reorganise_files(array('source' => $submission_id,'group_id' => $submission_group_id));
				$data = array();
				if($result){
					$data['result'] = 'success';
				} else {
					$data['result'] = 'error';
					$data['error'] = 'Cannot reorganise the files';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "get.submission.group"){
				$ads = $utils->call('ads');
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$submission_id = $encdata['submission_id'];
				$submission_group_id = $encdata['submission_group_id'];
				$list = $ads->get_submission_group_items(array('fields' => "ad_submission_id AS id, IF(attachment_external_service IN ('none','amazonS3'),SUBSTRING(attachment_filename,12), attachment_filename) AS filename, attachment_filename AS filename_full, attachment_external_service AS service, ad_submission_title AS title, ad_submission_file_details AS comment, attachment_type AS type",'source' => $submission_id,'group_id' => $submission_group_id));
				$data = array();
				if($list !== false){
					$data['result'] = 'success';
					$data['list'] = $list;
				} else {
					$data['result'] = 'error';
					$data['error'] = 'Cannot get the files';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "save.submission.labels"){
				$ads = $utils->call('ads');
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$submission_id = $encdata['submission_id'];
				$submission_group_id = $encdata['submission_group_id'];
				$list = $ads->save_submission_labels(array('group_id' => $submission_group_id));
				$data = array();
				if($list !== false){
					$data['result'] = 'success';
					$data['list'] = $list;
				} else {
					$data['result'] = 'error';
					$data['error'] = 'Cannot save the labels';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "save.submission.details"){
				$ads = $utils->call('ads');
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$submission_id = $encdata['submission_id'];
				$submission_group_id = $encdata['submission_group_id'];
				$result = $ads->save_submission_details(array('group_id' => $submission_group_id));
				$data = array();
				if($result){
					$data['result'] = 'success';
				} else {
					$data['result'] = 'error';
					$data['error'] = 'Cannot save the details';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "complete.submission"){
				$ads = $utils->call('ads');
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$submission_id = $encdata['submission_id'];
				$submission_group_id = $encdata['submission_group_id'];
				$result = $ads->complete_submission(array('group_id' => $submission_group_id));
				$data = array();
				if($result){
					$data['result'] = 'success';
				} else {
					$data['result'] = 'error';
					$data['error'] = 'Cannot save the details';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "save.file"){

				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$submission_id = $encdata['submission_id'];
				$submission_group_id = $encdata['submission_group_id'];
				/*//now, here we need to understand if this is the first file or not
				$submissions = $ads->get_submission_group_items(array('id' => $submission_group_id));
				if(sizeof($submissions) > 1){
					//we need to clone the submission
					$ads->clone_submission(array('id' => $submission_id, 'group_id' => $submission_group_id));
					$submission_id = $ads->submission_id;
				}//end if*/
				$data = array();
				$filename = str_replace('https://copyclear.s3-eu-west-1.amazonaws.com/','',$_POST['f']);
				switch ($_POST['type']){
					case 'image/jpeg':
					case 'image/png':
						$data['image'] = true;
						$remotefile = __FILEPATH__ . 'upload/photos/o/'.$filename;
						if (copy($_POST['f'], $remotefile)) {
							$params = array(
								"filename" => $filename,
								"upload_path" => '../../upload/',
								"attachment_post_type" => 'submission',
								'module' => 'ads',
								'post_id' => $submission_id,
								'size' => $_POST['size'],
								'mime' => $_POST['type']
							);
							$result = $utils->save_attachment($params);
							$data['query'] = $utils->cache;
							if($result === true){
								$file_id = $utils->get_attachment_id();
								$file_data = $utils->get_attachment(array('file_id' => $file_id));
								$data['result']='success';
								$data['file'] = urlencode(base64_encode($file_id));
								//$data['encdata'] = urlencode(base64_encode(serialize(array('file_id' => $file_id,'entry_id' => $entry_id))));
								$data['code'] = $file_id;
								$data['uri'] = $_POST['f'];
								$data['mime'] = $file_data['attachment_mime'];
								$data['friendly_name'] = $file_data['attachment_friendly_name'];
							}
						}else{
							$data['result'] = 'error';
							$data['error'] = 'Cannot move the file';
						}
					break;

					default:
						$data['image'] = false;
						//import the URL
						$params = array();
						$params['url'] = $_POST['f'];
						if(!is_null($submission_id)){
							$params["post_id"] = $submission_id;
							$params["module"] = 'ads';
							$params["mime"] = $_POST['type'];
							$params["size"] = $_POST['size'];
							$params['attachment_post_type'] = 'submission';
						}//end if
						$result = $utils->attach_url($params);
						if($result !== false){
							$file_id = $utils->get_attachment_id();
							$file_data = $utils->get_attachment(array('file_id' => $file_id));
							$data['result']='success';
							$data['file'] = urlencode(base64_encode($file_id));
							//$data['encdata'] = urlencode(base64_encode(serialize(array('file_id' => $file_id,'entry_id' => $entry_id))));
							$data['uri'] = $_POST['f'];
							$data['code'] = $file_id;
							$data['mime'] = $file_data['attachment_mime'];
							$data['friendly_name'] = $file_data['attachment_friendly_name'];
						} else {
							$data['result'] = 'error';
							$data['error'] = 'Cannot save the file';
						}//end if
				}//end switch

				echo json_encode($data);
			}//end if

			if($method == "create.thumbs.ajax"){
				$image = $utils->call("image");
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$post_id = $encdata['entry_id'];
				$file_id = base64_decode(urldecode($_POST['file_id']));

				$data = array();

				$logger->log_event(array("login_area" => "public","type" => "api.create.thumbs.ajax","module_id" => "submissions"));

				//first get the formats
				$params = array("module" => "entries", "default" => true);
				$module = 'entries';
				$formats = $utils->get_attachments_formats($params);
				if($formats !== false){
					//now for each format we create the image
					for($i = 0; $i < sizeof($formats); $i++){
						$params = array(
							"file_id" => $file_id,
							"format_id" => $formats[$i]['attachment_setting_id'],
							"module" => $module,
							"post_id" => $post_id,
							"upload_path" => "../../upload/"
						);
						$result = $image->create_new_size($params);
					}//end for i

					$file_data = $utils->get_attachment(array('file_id' => $file_id));

					$data['result'] = 'success';

					$data['file'] = urlencode(base64_encode($file_id));
					$data['encdata'] = urlencode(base64_encode(serialize(array('file_id' => $file_id,'entry_id' => $post_id))));
					$data['code'] = $file_id;
					$data['uri'] = $file_data['attachment_filename'];
					$data['mime'] = $file_data['attachment_mime'];
					$data['friendly_name'] = $file_data['attachment_friendly_name'];
				} else {
					$data['result'] = 'error';
					$data['message'] = "Problem with the image, please try again";
				}//end if

				echo json_encode($data);
			}//end if

			if($method == "submit.message"){
				$data = array();
				$messages = $utils->call('messages');
				$encdata = $_POST['encdata'];
				$encdata = unserialize(base64_decode(urldecode($encdata)));
				$ref_id = $encdata['submission_id'];
				$message = $_POST['comment'];
				$options = array('ref_id' => $ref_id,'message' => $message,'status' => 'sent','module' => 'ads');
				$result = $messages->send_message($options);
				if($result){
					$data['result'] = 'success';
					$data['enc_id'] = urlencode(base64_encode($messages->message_id));
					$data['id'] = $messages->message_id;
					$ads = $utils->call('ads');
					$ads->notify_new_comment(array('id' => $ref_id));
				} else {
					$data['result'] = 'error';
					$data['errors'] = $messages->errors;
				}//end if
				echo json_encode($data);
			}//end if

			###########################################
			# PRIVATE CALLS ACCESSIBLE ONLY TO ADMINS #
			###########################################

			if(in_array($user_data['group_id'],$admins)){
				if($method == "admin.update.submission"){
					$data = array();
					$ads = $utils->call('ads');
					$encdata = $_POST['encdata'];
					$id = base64_decode(urldecode($encdata));
					$options = array('id' => $id);
					$result = $ads->update_submission($options);
					if($result){
						$data['result'] = 'success';
						$data['status'] = $ads->status;
					} else {
						$data['result'] = 'error';
						$data['errors'] = $ads->errors;
					}//end if
					echo json_encode($data);
				}//end if
			}//end if

			if($method == "update.user.status"){
				$data = array();
				$user_id = $_POST['pk'];
				$status = $_POST['value'];
				$registrations = $utils->call('registrations');
				$result = $registrations->update_account_status(array('user_id' => $user_id,'active' => $status));
				if($result){
					$data['result'] = 'success';
				} else {
					$data['result'] = 'error';
				}//end if
				echo json_encode($data);
			}//end if

			if($method == "update.company.status"){
				$data = array();
				$company_id = $_POST['pk'];
				$status = $_POST['value'];
				$companies = $utils->call('companies');
				$result = $companies->update_company_status(array('company_id' => $company_id,'status' => $status));
				if($result){
					$data['result'] = 'success';
				} else {
					$data['result'] = 'error';
				}//end if
				echo json_encode($data);
			}//end if

		}//end if

		#####################################
		# PUBLIC CALLS ACCESSIBLE TO ANYONE #
		#####################################
		//this methods don't require an API Key

		if($method == "login"){

			$user = trim($_REQUEST['username']);
			$pass = trim($_REQUEST['password']);
			$stay_logged = (bool)$_REQUEST['stay_logged'];

			$auth = $utils->call('auth_cc');
			$data = array();
			if($auth->do_login($user,$pass)){
				$logger->log_event(array("login_area" => "public","type" => "api.auth.login.successful","module_id" => "users"));

				$auth->set_stay_logged($stay_logged);
				$user_id = $auth->uid;
				$user_data = $auth->get_user(array('id' => $user_id));
				$company_id = $user_data['user_company_id'];
				$companies = $utils->call('companies');
				$company_data = $companies->get_company(array('id' => $company_id));
				$data['result'] = "success";
				if($user_data['group_id'] == 4){
  				$data['is_admin'] = false;
					$data['brand_associated'] = (bool)$company_data['company_brands_associated'];
					if($company_data['company_status'] === 'active'){
  					$data['company_active'] = true;
  				} else {
    				$data['company_active'] = false;
					}
				} else {
					$data['brand_associated'] = true;
					$data['is_admin'] = true;
				}
			} else {
				$logger->log_event(array("login_area" => "public","type" => "api.auth.login.failed","module_id" => "users"));
				$data['result'] = "error";
			}//end if
			echo json_encode($data);
		}//end if

		#####################################
		# TERMS & CONDITIONS METHODS	   		#
		#####################################

		if($method == "reactivate.company"){
  		$data = array();
  		$logger->log_event(array("login_area" => "public","type" => "api.reactivate.company","module_id" => "companies"));
  		$auth = $utils->call('auth');
  		$user_id = $auth->uid;
			$user_data = $auth->get_user(array('id' => $user_id));
			$company_id = $user_data['user_company_id'];
			$companies = $utils->call('companies');
			$company_data = $companies->get_company(array('id' => $company_id));
  		$result = $companies->reactivate_company();
  		if($result){
    		$data['result'] = 'success';
    		$logger->log_event(array("login_area" => "public","type" => "api.reactivate.company.successful","module_id" => "companies"));
    		if($user_data['group_id'] == 4){
      		$data['brand_associated'] = (bool)$company_data['company_brands_associated'];
        } else {
					$data['brand_associated'] = true;
				}
  		} else {
    		$data['result'] = 'error';
    		$logger->log_event(array("login_area" => "public","type" => "api.reactivate.company.failed","module_id" => "companies"));
  		}// end if
  		echo json_encode($data);
		}// end if

		#####################################
		# REGISTRATIONS METHODS		       		#
		#####################################

		if($method == "register.user"){
			$data = array();
			$registrations = $utils->call('registrations');

			$result = $registrations->register_user();
			if($result == true){
				$encdata = array('id' => $registrations->uid, 'company_data' => $registrations->public_data);
				$data['result'] = 'success';
				$data['brand_owner'] = false;
				$encdata['brand_owner'] = false;
				if($registrations->is_brand_owner == true){
					$data['brand_owner'] = true;
					$encdata['brand_owner'] = true;
				}//end if
				$user_data = $registrations->get_user(array('id' => $registrations->uid));
				if($user_data !== false){
					$companies = $utils->call('companies');
					$company_id = $user_data['user_company_id'];
					$company_data = $companies->get_company(array('id' => $company_id));
					if($company_data !== false){
						$brands = $utils->call('brands');
						$brands_owners = $brands->get_brands_owners(array('company_id' => $company_id,'fields' => 'brand_owner_id AS id, brand_owner_name AS name'));
						$data['bo'] = $brands_owners;
						$brands_list = $brands->get_brands(array('company_id' => $company_id,'fields' => 'brand_id AS id, brand_name AS name'));
						$data['b'] = $brands_list;
					}//end if
				}//end if
				$data['encdata'] = urlencode(base64_encode(serialize($encdata)));
			} else {
				$data['result'] = 'error';
				$data['errors'] = $registrations->errors;
			}//end if

			echo json_encode($data);
		}//end if

		if($method == "search.companies"){
			$data = array();
			$search = $_POST['search'];
			if(strlen($search) > 2){
				$companies = $utils->call('companies');
				$list = $companies->get_companies(array('search' => $search,'fields' => 'company_id AS id, company_name AS name'));
				if($list !== false){
					$data['result'] = 'success';
					$data['data'] = $list;
				} else {
					$data['result'] = 'empty';
				}//end if
			} else {
				$data['result'] = 'empty';
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "save.brand"){
			$data = array();
			$brands = $utils->call('brands');
			$name = $_POST['name'];
			$encdata = unserialize(base64_decode(urldecode($_GET['encdata'])));
			$user_id = $encdata['id'];
			$company_id = $encdata['company_data']['company_id'];
			$billing_name = $_POST['company_brand_name'];
			$billing_email = $_POST['company_brand_email'];
			$category_id = $_POST['company_brand_category_id'];
			$owner_id = $_POST['company_brand_advertiser'];
			$options = array('temp' => 0,'active' => 1, 'name' => $name,'user_id' => $user_id, 'company_id' => $company_id,'billing_name' => $billing_name, 'billing_email' => $billing_email, 'category_id' => $category_id, 'owner_id' => $owner_id);
			$result = $brands->save_brand($options);
			if($result){
				$data['result'] = 'success';
				$data['id'] = $brands->id;
				$data['name'] = ucwords($name);
			} else {
				$data['result'] = 'error';
				$data['errors'] = array('Cannot save the brand');
			}//end if

			echo json_encode($data);
		}//end if

		if($method == "save.brands.rel"){
			$data = array();
			$brands = $utils->call('brands');
			$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
			$user_id = $encdata['id'];
			$company_id = $encdata['company_data']['company_id'];
			$ids = $_POST['brands'];
			$options = array('brand_id' => $ids, 'user_id' => $user_id, 'company_id' => $company_id);
			$result = $brands->save_brand_rel($options);
			if($result){
				$registrations = $utils->call('registrations');
				$registrations->complete_registration(array('user_id' => $user_id));
				$registrations->notify_new_registration(array('user_id' => $user_id));
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot save the brands';
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "ta.get.companies"){
			$data = array();
			$search = $_GET['search'];
			if(strlen($search) > 2){
				$companies = $utils->call('companies');
				$list = $companies->get_companies(array('search' => $search,'fields' => 'company_id AS id, company_name AS name'));
				if($list !== false){
					$data = $list;
				}//end if
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "ta.get.brands"){
			$data = array();
			$search = $_GET['search'];
			if(strlen($search) > 2){
				$brands = $utils->call('brands');
				$encdata = unserialize(base64_decode(urldecode($_GET['encdata'])));
				$user_id = $encdata['id'];
				$company_id = $encdata['company_data']['company_id'];
				$list = $brands->get_brands(array('search' => $search,'fields' => 'brand_id AS id, brand_name AS name','user_id' => $user_id, 'company_id' => $company_id));
				if($list !== false){
					$data = $list;
				}//end if
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "ta.get.brands.owners"){
			$data = array();
			$search = $_GET['search'];
			if(strlen($search) > 2){
				$brands = $utils->call('brands');
				$list = $brands->get_brands_owners(array('search' => $search,'fields' => 'brand_owner_id AS id, brand_owner_name AS name'));
				if($list !== false){
					$data = $list;
				}//end if
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "get.company.brands.owners"){
			$data = array();
			$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
			$user_id = $encdata['id'];
			$company_id = $encdata['company_data']['company_id'];
			$brands = $utils->call('brands');
			$brand_options = array('user_id' => $user_id, 'company_id' => $company_id, 'show_inactive' => true);
			$result = $brands->get_brands_owners($brand_options);
			if($result !== false){
				$data['result'] = 'success';
				$data['data'] = $result;
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot get the advertisers';
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "save.advertiser"){
			$data = array();
			$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
			$user_id = $encdata['id'];
			$company_id = $encdata['company_data']['company_id'];
			$name = $_POST['name'];
			$brands = $utils->call('brands');
			$brand_options = array('active' => '1','user_id' => $user_id, 'company_id' => $company_id, 'name' => $name);
			$result = $brands->save_brand_owner($brand_options);
			if($result){
				$data['result'] = 'success';
				$data['id'] = $brands->owner_id;
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot save the advertiser';
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "save.advertiser.rel"){
			$data = array();
			$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
			$user_id = $encdata['id'];
			$company_id = $encdata['company_data']['company_id'];
			$ids = $_POST['advertisers'];
			$brands = $utils->call('brands');
			$brand_options = array('owner_id' => $ids,'user_id' => $user_id, 'company_id' => $company_id);
			$result = $brands->save_brand_owner_rel($brand_options);
			if($result){
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot save the advertiser';
			}//end if
			echo json_encode($data);
		}//end if

		#####################################
		# BRANDS ASSOCIATIONS METHODS		#
		#####################################

		if($method == "update.company.brands"){
			$data = array();
			$encdata = unserialize(base64_decode(urldecode($_POST['encdata'])));
			$user_id = $encdata['user_id'];
			$company_id = $encdata['company_id'];
			$brands = $utils->call('brands');
			$companies = $utils->call('companies');
			$result = $brands->update_brand_associations(array('company_id' => $company_id));
			if($result){
				$companies->mark_brand_details_as_updated(array('company_id' => $company_id));
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot update the associations';
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "update.brands.details"){
			$brands = $utils->call('brands');
			$result = $brands->update_brand_details(array('id' => $_POST['brand_id']));
			if($result){
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
				$data['error'] = 'Cannot update the brands';
			}//end if
			echo json_encode($data);
		}//end if

		#####################################
		# FORGOT PASSWORD METHODS			#
		#####################################

		if($method == "forgot.pwd"){
			$registrations = $utils->call('registrations');
			$email = $_POST['email'];
			$registrations->set_module('registrations');
			$result = $registrations->sent_reset_email(array('email' => $email));
			if($result){
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
				$data['errors'] = $registrations->errors;
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "reset.pwd"){
			$registrations = $utils->call('registrations');
			$encdata = $_POST['encdata'];
			$encdata = unserialize(base64_decode(urldecode($encdata)));
			$user_id = $encdata['user_id'];
			$registrations->set_module('companies');
			$result = $registrations->reset_password(array('user_id' => $user_id,'password' => $_POST['password'], 'conf_password' => $_POST['conf_password']));
			if($result){
				$data['result'] = 'success';
			} else {
				$data['result'] = 'error';
				$data['errors'] = $registrations->errors;
			}//end if
			echo json_encode($data);
		}//end if

		if($method == "logout"){
			$auth = $utils->call("auth");
			$auth->do_logout();
			$logger->log_event(array("login_area" => "public","type" => "api.auth.logout.successful","module_id" => "users"));
			header("location: ".__BASEPATH__);
		}//end if

		if($method == "test"){
			echo "hello world";
		}//end if
	} else {
		echo 'You didn\'t specify any method.';
	}//end if
?>
