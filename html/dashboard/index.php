<?php
	//require_once("inc/config.inc.php");
	require_once('inc/init.cms.inc.php');
	require_once('inc/meta.public.inc.php');
?>
</head>
<body>
	<?php
	require_once('inc/header.public.inc.php');
	$auth = $utils->call('auth_cc');
	$auth->autologin();
	?>
	<article class="main-content">
		<div class="container">
           	<div><h1>Welcome</h1></div>
<!--div class="alert alert-warning"><h4>Notice for users of Internet Explorer 8</h4>We have identified a bug in Internet Explorer 8 which is causing difficulties when uploading files. We are working to resolve this issue and expect to implement a solution shortly. In the short term we would ask users to use a more modern web browser if possible - Internet Explorer 9 or higher, Firefox, Chrome or Safari. If this is not an option please contact <a href="mailto:webmaster@copyclear.ie">webmaster@copyclear.ie</a> and we will assist you with your submission.</div-->
            <section class="row">
			<div class="col-md-6 col-sm-6 clearfix">
            	<h2>Log In</h2>
                <div class="box">
                <form role="form" id="login-form" name="login-form" method="post" action="#">
                  <div class="form-group">
                    <label for="username">Email Address</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  </div>
									<div class="form-group">
										<label class="checkbox">
											<input type="checkbox" value="1" id="stay-logged-in" name="stay_logged" <?php
												if($_COOKIE['persistent_login']){
													echo 'checked="checked"';
												}// end if
											?> /> Stay logged in
										</label>
									</div>
                  <a id="forgot_pwd" href="#" data-toggle="modal" data-target="#forgot-pwd-modal">Forgot Password?</a>
                </form>
                </div>
            	<button class="btn btn-primary" id="login-btn">Log In</button>
			</div>
			<div class="col-md-6 col-sm-6 clearfix">
            	<h2>Register</h2>
                <div class="box">
					<p><strong>Not registered with us?</strong></p>
					<p>Click on the register button to register your details and start submitting ads.</p>
                </div>
                <a class="btn btn-primary" href="register.php">Register</a>
            </div>
			</section>
		</div>
	</article>
    <div class="modal fade" id="forgot-pwd-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Forgot Password</h4>
                </div>
                <div class="modal-body">
                	<form action="#" method="post" id="forgot-pwd-form" name="forgot-pwd-form">
                    	<label>Enter your email address</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="youremail@domain.com">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submit-email-fpw">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
</html>
