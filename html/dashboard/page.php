<?php 
	//require_once("inc/config.inc.php");
	require_once('inc/init.cms.inc.php');	
	
	$utils->view->dev_mode = true;
	
	$permalink = $utils->view->get_permalink();
	$section = $utils->view->get_section();
	
	$page_loaded = $utils->view->load_page(); //this function automatically redirect in case of failure
	$page = $utils->view->page;//shortcut
	if(isset($page['module'])){
		$site->set_module($page['module']);
	} else if(!is_null($utils->view->get_section())){
		$site->set_module($utils->view->get_section());
	}//end if
	require_once('inc/meta.inc.php');
	/* CUSTOM CALLS */
	$users = $utils->call('users');
	$user_data = $users->get_user();
	if($user_data === false){
		header("location: ".__BASEPATH__);	
	}//end if
	$company_id = $user_data['user_company_id'];	
	$user_id = $user_data['user_id'];
	
	$companies = $utils->call('companies');
	$company_data = $companies->get_company(array('id' => $company_id));
	
	$token = sha1($user_id.':'.$user_data['user_username'].'-'.__SALT__);
	$is_admin = false;
	if(in_array($user_data['group_id'],$admins)){
		$is_admin = true;
	}//end if
	/* END CUSTOM CALLS */
?>
  </head>
	<body>
	<?php
	if(!$is_admin){
		require_once('inc/header.inc.php');
	} else {
		require_once('inc/admin.header.inc.php');	
	}
	?>	
	<article class="main-content">
		<div class="container">
        <?php
		$tpl = $utils->view->load_template(array('template_path' => 'inc/templates/'));		
		if($tpl !== false){
			require_once($tpl);
		}//end if
		?>
		</div>
	</article>	
	</body>
</html>