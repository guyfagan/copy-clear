<?php 
	require_once("inc/config.inc.php");
	require_once('inc/init.cms.inc.php');
	require_once('inc/meta.public.inc.php');
	
	$brands = $utils->call('brands');
	$companies = $utils->call('companies');
?>
  </head>
	<body>
	<?php
	require_once('inc/header.public.inc.php');
	?>	
    <article class="main-content">
		<div class="container">
           	<h1>Register</h1>
            <div class="row">         
                <div class="col-md-offset-3 col-md-9">
                	<h2 id="page-title">Find your Company</h2>
                </div>
            </div>
            <section class="row" id="step1">
            <?php
				require_once('inc/registration.form.step1.inc.php');
			?>
            </section>
            <section class="row hide" id="step2">
            <?php
				require_once('inc/registration.form.step2.inc.php');
			?>
            </section>
            <section class="row hide" id="step3">
            <?php
				require_once('inc/registration.form.step3.inc.php');
			?>
            </section>
            <section class="row hide" id="step4">
            	<div class="col-md-3 helpbox">
                    <p>Haven't received an email?</p>
                    <p> Try checking your spam folder.</p>                        
                </div>
                <div class="col-md-9">
                    <div class="box">
                        <h3>Thanks for registering with CopyClear.</h3>
                    	<p>The CopyClear team will review your registration shortly and you will receive an email informing you when your account has been activated.</p>
						<p>Once activated and you can log in to your dashboard and begin submitting work for approval.</p>
                     </div>
                </div>
            </section>
		</div>
	</article>	
</body>
</html>