<?php 
	require_once("inc/config.inc.php");
	require_once('inc/init.cms.inc.php');
	require_once('inc/meta.public.inc.php');
	
	$users = $utils->call('users');
	$user_data = $users->get_user();
	if($user_data === false){
		header("location: ".__BASEPATH__);	
	}//end if
	$company_id = $user_data['user_company_id'];	
	$user_id = $user_data['user_id'];
	
	$companies = $utils->call('companies');
	$company_data = $companies->get_company(array('id' => $company_id));
	
	$brands = $utils->call('brands');
	$brands_categories_list = $brands->get_brands_categories();
	
	$encdata = array('user_id' => $user_id,'company_id' => $company_id);
	$encdata = urlencode(base64_encode(serialize($encdata)));
	
?>
</head>
<body>
	<?php
	require_once('inc/header.public.inc.php');
	?>	
    <article class="main-content">
		<div class="container">
           	<h1>Associate your current brands</h1>
            <div class="row">         
                <div class="col-md-9">
                	<p>Please provide billing information for each of your brands.</p>
                    <p>You will only need to do this once. The next time you log in you will be taken straight to your dashboard.</p>
                </div>
            </div>
            <section class="row">
            	<div class="boxes col-md-9">
                	<form action="#" id="associate-brands-details" name="associate-brands-details" method="post" class="form-horizontal" role="form">
                    	<input type="hidden" name="encdata" id="encdata" value="<?php echo $encdata ?>">
            <?php
				$brands_owners = $brands->get_brands_owners(array('show_legacy' => false,'company_id' => $company_id));
				if($brands_owners){
					for($i = 0; $i < sizeof($brands_owners); $i++){
						$brands_list = $brands->get_brands(array('show_legacy' => false,'company_id' => $company_id, 'brand_owner_id' => $brands_owners[$i]['brand_owner_id']));						
						if($brands_list !== false){				
							echo '<h2>'.$brands_owners[$i]['brand_owner_name'].'</h2>';
							for($x = 0; $x < sizeof($brands_list); $x++){
							?>
                            <div class="panel panel-default">
                            	<input type="hidden" name="brand_id[]" id="brand-id-<?php echo $brands_list[$x]['brand_id'] ?>" value="<?php echo $brands_list[$x]['brand_id'] ?>">
                            	<div class="panel-heading"><?php echo $brands_list[$x]['brand_name'] ?></div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Billing Company Name</label>
                                        <div class="col-md-9">
                                            <input id="company-brand-name-<?php echo $brands_list[$x]['brand_id'] ?>" class="form-control" type="text" name="company_brand_name[]" placeholder="Billing Company's Name" value="<?php echo $brands_list[$x]['brand_billing_name'] ?>" required data-label="Billing Company Name for <?php echo $brands_list[$x]['brand_name'] ?>">	
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Billing Company Email</label>
                                        <div class="col-md-9">
                                            <input id="company-brand-email-<?php echo $brands_list[$x]['brand_id'] ?>" class="form-control" type="text" name="company_brand_email[]" placeholder="Billing Company's Email Address" value="<?php echo $brands_list[$x]['brand_billing_email'] ?>" required data-label="Billing Company Email for <?php echo $brands_list[$x]['brand_name'] ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Category</label>
                                        <div class="col-md-9">
                                            <select name="company_brand_category_id[]" id="company-brand-category-id-<?php echo $brands_list[$x]['brand_id'] ?>" class="form-control">
                                            <?php
                                                if($brands_categories_list !== false){
                                                    for($z = 0; $z < sizeof($brands_categories_list); $z++){
                                                        echo '<option value="'.$brands_categories_list[$z]['brand_category_id'].'"';
                                                        if($brands_list[$x]['brand_category_id'] == $brands_categories_list[$z]['brand_category_id']){
                                                            echo ' selected';	
                                                        }//end if
                                                        echo '>'.$brands_categories_list[$z]['brand_category_name'].'</option>';
                                                    }//end for i
                                                }//end if
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <?php		
							}//end for x
						}//end if
					}//end for i
				}//end if
					?>
                    	<button type="button" class="btn btn-primary pull-right" id="save-associated-brands-details-btn">Save and go to your dashboard</button>
                	</form>                	
            	</div>                
            </section>           
		</div>
	</article>	   
</body>
</html>