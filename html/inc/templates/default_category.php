<?php
	$section_data = $utils->view->get_section_data();
	$utils->create_paging($_GET['page'],10);
	$list = $site->get_posts(array('category_id' => $page['category_id'],'status' => 1));
	# PAGINATION #
	$paging_stats = $utils->get_paging_stats();	
	$paging_list = $utils->get_paging(array('range' => 10,'cms' => false));
	//now that we have all the info about the pagination, we reset the paging, so it doesn't interfere with other calls to the class	
	$utils->unset_paging();
	$page_start = $paging_stats['start']+1;
	$page_end = $paging_stats['start']+10;
	if($page_end > $paging_stats['founds']){
		$page_end = $paging_stats['founds'];
	}//end if
	$page_total = $paging_stats['pages'];
	$page_current = $paging_stats['current_page'];
	$page_prev = $page_current-1;
	if($page_prev == "0"){
		$page_prev = 1;
	}//end if
	$page_next = $page_current+1;
	if($page_next > $page_total){
		$page_next = $page_total;
	}//end if

	# END PAGINATION #
?>
<article class="row-fluid clearfix">
    <section class="span8 clearfix primary">
        <header>
            <h1><?php echo $section_data['module_label'] ?></h1>
        </header>
		<div class="primary-content">
        	<h2><?php echo $page['category_name'] ?></h2>
        <?php
			if($page['category_message'] != ""){
				echo $page['category_message'];
				echo '<hr />';
			}//end if
			
			if($list !== false){
				for($i = 0; $i < sizeof($list); $i++){
					$date = date('d.m.Y',$list[$i]['post_date']);
					$title = $list[$i]['post_title'];
					$abstract = $list[$i]['post_abstract'];
					$link = __BASEPATH__.$list[$i]['post_permalink'].'.html';
		?>
        	<section class="list clearfix">                
                <div class="span12">
                     <h3><?php echo $title ?></h3>  
                     <p><?php echo $list[$i]['post_abstract'] ?></p>    
                     <a href="<?php echo $link ?>" class="more-btn">Read More</a>
                </div>                
            </section>
        <?php
				}//end for i
		?>    
        	<div class="pagination pagination-centered">            
                <ul>
                    <li><a href="?page=<?php echo $page_prev ?>"><span>&#8592;</span></a></li>
                    <?php
                    if(is_array($paging_list) && sizeof($paging_list) > 0){
                        for($i = 0; $i < sizeof($paging_list); $i++){
                            echo '<li';
                            if($paging_list[$i]['page'] == $page_current){
                                echo ' class="active"';	
                            }//end if
                            echo '><a href="'.__BASEPATH__.'vacancies/'.$paging_list[$i]['url_clean'].'">'.$paging_list[$i]['page'].'</a></li>';
                        }//end for i
                    }//end if
                    ?>                      
                    <li><a href="?page=<?php echo $page_next ?>"><span>&#8594;</span></a></li>
                </ul>                    
           </div>
           <?php
			}//end if
		   ?>
    	</div>
    </section>
</article>