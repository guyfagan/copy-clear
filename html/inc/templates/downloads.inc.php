<?php
	#template name: Downloads Page
	
?>
<article class="main-content beige">
    <div class="container">
        <div class="category-title"><h1>Downloads</h1></div>
        <section class="row">
            <?php
			$list = $site->get_posts(array('category_id' => $page['category_id'], 'status' => 1,'default_photo' => true, 'use_system_order' => true));
			if($list !== false){
				for($i = 0; $i < sizeof($list); $i++){
					?>
                    	<div class="col-md-6 col-sm-6 download-col <?php echo $list[$i]['post_permalink']; ?>">
                            <h2><?php echo $list[$i]['post_title']; ?></h2><?php 
								$files = $utils->get_attachments(array('module' => 'pages', 'post_id' => $list[$i]['post_id'],'status' => 'published','ignore' => array('type' => array('image'))));
								if ($files!==false){
									echo '<ul>';
									foreach ($files as $file){
										echo '<li class="downloads"><a href="'.__BASEPATH__.'upload/files/'.$file['attachment_filename'].'" target="_blank">'.$file['attachment_post_title'].'</a></li>';
									}
									echo '</ul>';
								}
							?>
                         </div>
                    <?php 
				}//end for i
			}//end if
			?>    
        </div>
        </section>
    </div>
</article>
<!--

<div class="col-md-4 download-col">
            	<h2>Codes</h2>
                <ul>
                	<li class="downloads"><a href="downloads/ASAI_Code.pdf" target="_blank">ASAI</a></li>
                    <li class="downloads"><a href="downloads/Meas_Guidelines.pdf" target="_blank">MEAS</a></li>
                    <li class="downloads"><a href="downloads/alcohol_codes_practice.pdf" target="_blank">Alcohol Marketing, Comm’s and Sponsorship Codes of Practice</a></li>
                    <li class="downloads"><a href="downloads/BAI_General_Guidance_Notes.pdf" target="_blank">BAI General Guidance Notes</a></li>
                    <li class="downloads"><a href="downloads/BAI_General_Communications_Code.pdf" target="_blank">BAI General Advertising Code</a></li>
                </ul>
             </div>
             <div class="col-md-4 download-col">
            	<h2>CopyClear Facts & Figures</h2>
                <ul>
                	<li class="downloads"><a href="downloads/CCCI_Annual_Report_2010.pdf" target="_blank">Annual Report 2010</a></li>
                    <li class="downloads"><a href="#" target="_blank">Quarterly report July 2013</a></li>
                </ul>
             </div>
             <div class="col-md-4 download-col">
            	<h2>Guidelines</h2>
                <ul>
                	<li class="downloads"><a href="#" target="_blank">How we interpret the codes</a></li>
                    <li class="downloads"><a href="downloads/Social_Media_Activations_Draft_Guidelines_February_2014.docx" target="_blank">Guidance on social media</a></li>
                </ul>
                <h2 class="last-download">Useful Documents</h2>
                <ul>
                	<li class="downloads"><a href="downloads/ccci_waiver.doc" target="_blank">Waiver Form</a></li>
                    <!--<li class="downloads"><a href="#" target="_blank">FAQs</a></li>
                </ul>
             </div-->