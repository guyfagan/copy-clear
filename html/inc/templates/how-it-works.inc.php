<?php 
/*

	Get the first article.
	Use the info to update the nav highlight on the page

*/
	$active_page = 13;
	$page = $site->get_post(array('id' => $active_page));
$category_data = unserialize(base64_decode($page['category_id']));
				$category = $site->get_category(array('cid' => $category_data[0]));

	
?>
<article class="main-content beige">
    <div class="container">
        <div class="category-title"><h1><?php echo $category['category_name'] ?></h1></div>
        <section class="row">
         <div class="col-md-3 col-sm-3 subnav hidden-xs">
            <div >
                <?php
                    require_once('inc/sidenav.inc.php');
                ?>  
            </div>
         </div>
         <div class="col-md-9 col-sm-9">
            <h2><?php echo $page['post_title'] ?></h2>
            <?php
                echo utf8_encode($page['post_message']);
            ?>  
        </div>
<div class="col-xs-12 visible-xs">
            <div >
                <?php
                    include('inc/sidenav.inc.php');
                ?>  
            </div>
         </div>
        </section>
    </div>
</article>