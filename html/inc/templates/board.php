<?php
	#template name: Managers Page

	$father = $site->get_category(array('category_id' => $page['category_father_id']));
	//var_dump($father);
?>
<article class="main-content beige">
    <div class="container">
        <div class="category-title"><h1><?php echo $father['category_name'] ?></h1></div>
        <section class="row">
         <div class="col-md-3 subnav col-sm-3 hidden-xs">
            <div >
                <?php
	$sidenav = $site->get_items(array('status' => 1,'category_id'=>$page['category_father_id']));

	if($sidenav !== false){
?>
	<ul>
    <?php
	for($i = 0; $i < sizeof($sidenav); $i++){
		$link = __BASEPATH__.$sidenav[$i]['permalink'].'.html';
		echo '<li class="sidenav';
		if($permalink == $sidenav[$i]['permalink']){
			echo ' current';
		} else if (isset($active_page) && $active_page == $sidenav[$i]['id'] ) {
			echo ' current';
		}
		echo '"><a href="'.$link.'">'.$sidenav[$i]['label'];
		echo '</a></li>';
	}//end for i
	?>
    </ul>
<?php
	}//end if
?>
            </div>
         </div>
         <div class="col-md-9 managers col-sm-9">
            <h2><?php echo $page['category_name'] ?></h2>
            <?php
			$list = $site->get_posts(array('category_id' => $page['category_id'], 'status' => 1,'default_photo' => true, 'use_system_order' => true));
			if($list !== false){
				for($i = 0; $i < sizeof($list); $i++){
		?>
        		<h3><?php echo $list[$i]['post_title'] ?></h3>
               <div class="profile clearfix">
               <?php
					if(isset($list[$i]['photo'])){
					?>
                	<img class="pull-right" src="<?php echo __BASEPATH__?>upload/photos/t/<?php echo $list[$i]['photo']['attachment_filename'] ?>" alt="<?php echo $list[$i]['post_title'] ?>">
                <?php
					}
				?>
               <?php echo utf8_encode($list[$i]['post_message']) ?>
               </div>
        <?php
				}//end for i
			}//end if
		?>
        </div>
		<div class="col-xs-12 visible-xs">
            <div >
                <?php
	//$sidenav = $site->get_items(array('status' => 1,'category_id'=>$page['category_father_id']));

	if($sidenav !== false){
?>
	<ul>
    <?php
	for($i = 0; $i < sizeof($sidenav); $i++){
		$link = __BASEPATH__.$sidenav[$i]['permalink'].'.html';
		echo '<li class="sidenav';
		if($permalink == $sidenav[$i]['permalink']){
			echo ' current';
		} else if (isset($active_page) && $active_page == $sidenav[$i]['id'] ) {
			echo ' current';
		}
		echo '"><a href="'.$link.'">'.$sidenav[$i]['label'];
		echo '</a></li>';
	}//end for i
	?>
    </ul>
<?php
	}//end if
?>
            </div>
         </div>
        </section>
    </div>
</article>