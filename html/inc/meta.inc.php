<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CopyClear<?php echo ' | '  . $page_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Old Hat - http://oldhat.ie">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="css/screen.css" rel="stylesheet">
    <link href="css/fluid.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    	<link href="css/ie.css" rel="stylesheet">
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]--><script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>
	$().ready(function(){
		var menuTop = document.getElementById( 'cbp-spmenu-s1' ),
		showTop= document.getElementById( 'showLeft' ),
		closeMenu = document.getElementById( 'closeMenu' ),
		showLeftPush = document.getElementById( 'showLeftPush' ),
		body = document.body;
	
		showTop.onclick = function() {
			classie.toggle( this, 'active' );
			classie.toggle( menuTop, 'cbp-spmenu-open' );
		};
		
		closeMenu.onclick = function() {
			classie.toggle( menuTop, 'cbp-spmenu-closed' );
		};
		
		$('#closeMenu').click(function() {
			$('#cbp-spmenu-s1').toggleClass('cbp-spmenu-open');
			$('body').toggleClass('cbp-spmenu-no-scroll');
		}
	);
	});
	</script>