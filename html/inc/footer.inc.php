<article class="footer">
		<div class="container">
            <section>
                <p class="address">CopyClear, 12 Clanwilliam Square, Grand Canal Quay, Dublin 2.<span class="hidden-xs"> | </span><br class="visible-xs" />Phone: 01 - 6764876.<span class="hidden-xs"> | </span><br class="visible-xs" />E-mail: <a href="mailto:clearance@copyclear.ie">clearance@copyclear.ie</a><span class="hidden-xs"> | </span><br class="visible-xs" />Copyright © Central Copy Clearance Ireland 2003-<?php echo date ('Y'); ?></p>
                <div class="row">
                    <img class="ccci" alt="Central Copy Clearance Ireland" src="gr/ccci_logo.png">
                    <p>Central Copy Clearance Ireland is now trading as CopyClear</p>
                </div>
            </section>
		</div>
	</article>
	</body>
</html>