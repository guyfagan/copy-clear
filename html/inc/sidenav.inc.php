<?php
	$sidenav = $site->get_items(array('status' => 1,'category_id'=>$category_data[0]));
	
	if($sidenav !== false){
?>
	<ul>
    <?php
	for($i = 0; $i < sizeof($sidenav); $i++){
		$link = __BASEPATH__.$sidenav[$i]['permalink'].'.html';		
		echo '<li class="sidenav';
		if($permalink == $sidenav[$i]['permalink']){
			echo ' current';	
		} else if (isset($active_page) && $active_page == $sidenav[$i]['id'] ) {
			echo ' current';	
		}
		echo '"><a href="'.$link.'">'.$sidenav[$i]['label'];
		echo '</a></li>';
	}//end for i
	?>
    </ul>
<?php
	}//end if
?>